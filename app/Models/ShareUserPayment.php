<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShareUserPayment extends Model
{
    use HasFactory;
    use \Awobaz\Compoships\Compoships;

     public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
