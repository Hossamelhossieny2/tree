<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WheelMiss extends Model
{
    use HasFactory;

    public function quess()
    {
        return $this->belongsTo(WheelQues::class,'q_id','q_id');
    }

}
