<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Familycalendar extends Model
{
    use HasFactory;

    public function userEvents()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function joins_users()
    {
        return $this->hasMany(EventJoin::class,'event_id');
    }
}
