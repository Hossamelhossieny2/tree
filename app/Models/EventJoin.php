<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventJoin extends Model
{
    use HasFactory;

    public function events()
    {
        return $this->belongsTo(FamilyCalendar::class,'event_id','id');
    }

    public function user_join()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
