<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tafseer2 extends Model
{
	protected $table = 'tafseer2';
    use HasFactory;
}
