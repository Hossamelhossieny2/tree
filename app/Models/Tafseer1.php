<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tafseer1 extends Model
{
	protected $table = 'tafseer1';
    use HasFactory;
}
