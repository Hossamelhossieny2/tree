<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserMsg extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function from_user()
    {
        return $this->belongsTo(User::class,'to_user');
    }

    public function repliess()
    {
        return $this->hasMany(UserMsg::class,'replay')->whereNotNull('replay');
    }
}

