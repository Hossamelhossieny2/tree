<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MyFamily extends Model
{
    use HasFactory;
    public $tree = [];

    public static function familyDetails()
    {
        $father_fam_ca = FamilyName::find(auth()->user()->family_id);
                $finids = unserialize($father_fam_ca->fam_cache);

                if(isset(auth()->user()->family_2)){
                    $mother_fam_ca = FamilyName::find(auth()->user()->family_2);
                    $minids = unserialize($mother_fam_ca->fam_cache);
                    $res1 = array_merge($finids,$minids); 
                }else{
                        $res1 = $finids;
                   }


                if(isset(auth()->user()->family_m)){
                    $wife_fam_ca = FamilyName::find(auth()->user()->family_m);
                    $winids = unserialize($wife_fam_ca->fam_cache);
                    $res2 = array_merge($res1,$winids);
                }else{
                        $res2 = $res1;
                   }

        return User::where(function ($query) use($res2){
                       $query->whereIn('id',$res2);;
                    })->get();
    }


    public static function familyDetails_country()
    {
        $father_fam_ca = FamilyName::find(auth()->user()->family_id);
                $finids = unserialize($father_fam_ca->fam_cache);

                if(isset(auth()->user()->family_2)){
                    $mother_fam_ca = FamilyName::find(auth()->user()->family_2);
                    $minids = unserialize($mother_fam_ca->fam_cache);
                    $res1 = array_merge($finids,$minids); 
                }else{
                        $res1 = $finids;
                   }


                if(isset(auth()->user()->family_m)){
                    $wife_fam_ca = FamilyName::find(auth()->user()->family_m);
                    $winids = unserialize($wife_fam_ca->fam_cache);
                    $res2 = array_merge($res1,$winids);
                }else{
                        $res2 = $res1;
                   }

        return User::where(function ($query) use($res2){
                       $query->whereIn('users.id',$res2);;
                    })->leftJoin('countries','users.country_id','countries.id')->get();
    }

}
