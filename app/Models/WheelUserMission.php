<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WheelUserMission extends Model
{
    use HasFactory;
    use \Awobaz\Compoships\Compoships;

    public function quesWithType()
    {
        return $this->belongsTo(WheelQues::class,['ques','type'],['q_id','type'])->where('lang',app()->getLocale());
    }
}
