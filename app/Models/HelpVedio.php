<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HelpVedio extends Model
{
    use HasFactory;

    public function subs()
    {
        return $this->hasMany(static::class, 'sub' ,'cat')->where('lang',app()->getLocale());
    }
}
