<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class UserWife extends Pivot
{
    use HasFactory;

    public function isMarriedNow(): bool
    {
        return !$this->marriage_end_date;
    }

    public function scopeCurrent(Builder $builder): Builder
    {
        return $builder->whereNull('marriage_end_date')->whereNotNull('marriage_start_date');
    }

    public function getMarriageTextualDate(): string
    {
        if ($this->marriage_end_date) {
            $date = Carbon::parse($this->marriage_start_date)->diffInYears($this->marriage_end_date);
        } else {
            $date = Carbon::parse($this->marriage_start_date)->diffInYears(Carbon::now());
        }
        return $date == 0 ? __('same_year') : $date . __('years');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

}
