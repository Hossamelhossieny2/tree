<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    use HasFactory;
    use \Conner\Tagging\Taggable;
    
    protected $fillable = [ 
        'title_name', 
        'content' ,
        'pic' ,
        'cat' ,
        'user_id' ,
        'family_id'
    ];

    public function user_add()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
