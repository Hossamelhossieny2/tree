<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FamilyShare extends Model
{
    use HasFactory;
    protected $table='shares';

    public function user_share()
    {
        return $this->hasMany(ShareUser::class,'share_id');
    }


    public function payments()
    {
        return $this->hasMany(ShareUserPayment::class,'share_id');
    }

    public function desc()
    {
        return $this->belongsTo(ShareTypes::class,'type','type_id')->where('lang',app()->getLocale());
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
