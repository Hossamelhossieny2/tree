<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class FamilyName extends Model
{
    use HasFactory;

    protected $fillable = [
        'title','members','male','female','live','fam_cache'
    ];

    protected $withCount = [
        'members'
    ];

    public function members(): HasMany
    {
        return $this->hasMany(User::class,'family_id');
    }
    public function posts(): HasMany
    {
        return $this->hasMany(Post::class,'family_id')->orderBy('created_at','desc');
    }

    
}
