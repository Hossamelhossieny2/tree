<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WheelQues extends Model
{
    use HasFactory;
    use \Awobaz\Compoships\Compoships;
}
