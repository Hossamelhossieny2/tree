<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShareUser extends Model
{
    use HasFactory;
    use \Awobaz\Compoships\Compoships;

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function share_user()
    {
        return $this->belongsTo(User::class,'share_user');
    }

    public function part()
    {
        return $this->belongsTo(ShareUserPayment::class,'id');
    }

    public function share()
    {
        return $this->belongsTo(FamilyShare::class);
    }

     public function share_user_id()
    {
        return $this->belongsTo(User::class,'share_user');
    }

    public function pays()
    {
        return $this->HasMany(ShareUserPayment::class,['share_id','piece_id'],['share_id','piece_id']);
        
    }

}
