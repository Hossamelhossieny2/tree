<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WheelType extends Model
{
    use HasFactory;

    public function ques()
    {
        return $this->hasMany(WheelQues::class,'type','type_id')->where('lang',app()->getLocale());
    }
}
