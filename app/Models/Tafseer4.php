<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tafseer4 extends Model
{
	protected $table = 'tafseer4';
    use HasFactory;
}
