<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tafseer3 extends Model
{
	protected $table = 'tafseer3';
    use HasFactory;
}
