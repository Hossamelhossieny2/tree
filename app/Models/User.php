<?php

namespace App\Models;

use App\Traits\CustomZodiac;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Cache;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Jetstream\HasTeams;
use Laravel\Sanctum\HasApiTokens;
use App\Models\Session;
use App\Models\Country;
use App\Models\UserInfo;
use App\Models\UserEducation;
use App\Models\FollowUser;
use DB;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use HasTeams;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use CustomZodiac;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'gender' ,'family_id' ,'family_2' ,'family_m' ,'father_id' , 'mother_id' , 'birth_date' , 'phone_number' , 'profile_photo_path','country_id','family_mother', 'facebook_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'birth_date' => 'date:Y-m-d',
        'death_date' => 'date:Y-m-d',
        'wife'       => 'array',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    public function getFatherFamilyIds(): array
    {
        $minutes = Carbon::now()->addHour();

        return Cache::remember('user-father-family-id' . $this->id,$minutes,function () {
            $treeIds = [];

            $id = DB::table('users')->where('id',$this->id)->select('family_id as id')->first();

            $grand = DB::table("users")->where("family_id",$id->id)->where("father_id",NULL)->where("mother_id",NULL)->select('users.gender','users.name','users.id')->first();

            array_push($treeIds,$grand->id);

            return $treeIds;
        });
    }

    public function getMarraigeIds($person)
    {
        $minutes = Carbon::now()->addHour();

        return Cache::remember('user-marrige-family-id' . $this->id,$minutes,function () use ($person) {
            $treeIds = [];
            if($person->gender == "male"){
                $marriges = DB::table("user_wife")->leftJoin("users","users.id","=","wife_id")->select("wife_id as marrige_id","users.gender")->where("user_id",$person->id)->get();
            }else{
                $marriges = DB::table("user_wife")->leftJoin("users","users.id","=","user_id")->select("user_id as marrige_id","users.gender")->where("wife_id",$person->id)->get();
            }
            if(!empty($marriges)){
                foreach($marriges as $marrige){
                    array_push($treeIds,$marrige->marrige_id);
                    if($person->gender == "male"){
                        $children = DB::table("users")->where("father_id",$person->id)->where("mother_id",$marrige->marrige_id)->select('users.gender','users.id')->get();

                    }else{
                        $children = DB::table("users")->where("mother_id",$person->id)->where("father_id",$marrige->marrige_id)->select('users.gender','users.id')->get();
                    }
                    if(!empty($children)){
                        foreach($children as $child){
                            array_push($treeIds,$child->id);
                            $this->getMarraigeIds($child);
                        }
                    }
                }
            }
            return $treeIds;
        });
    }

    public function addNew($input)
    {
        $check = static::where('facebook_id',$input['facebook_id'])->first();


        if(is_null($check)){
            return static::create($input);
        }


        return $check;
    }
    
    public function channel()
    {
        return app()->isProduction() ? 'channel-' . $this->id : 'channel-debug-' . $this->id;
    }

    public function profile()
    {
        return $this->belongsTo(UserInfo::class,'id','user_id');
    }

    public function study()
    {
        return $this->hasMany(UserEducation::class);
    }

    public function family(): BelongsTo
    {
        return $this->belongsTo(FamilyName::class,'family_id');
    }

    public function family2(): BelongsTo
    {
        return $this->belongsTo(FamilyName::class,'family_2');
    }

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class,'country_id');
    }
    public function UserFamily(): BelongsTo
    {
        return $this->belongsTo(FamilyName::class,'family_id');
    }
    public function wife_children(): HasMany
    {
        return $this->hasMany(User::class,'mother_id','id');
    }

    public function husband_children()
    {
        return $this->hasMany(User::class,'father_id','id');
    }

    public function siblings(): HasMany
    {
        return $this->hasMany(User::class,'family_id','family_id')
            ->where('id','!=',$this->id);
    }

    public function father(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function FollowedUser()
    {
        return $this->belongsTo(FollowUser::class,'id','follow');
    }

    public function mother(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function wives(): BelongsToMany
    {
        return $this->belongsToMany(User::class,'user_wife','user_id','wife_id')
            ->using(UserWife::class)
            ->withPivot(['marriage_start_date','marriage_end_date'])
            ->as('marriage');
    }

    public function husbands(): BelongsToMany
    {
        return $this->belongsToMany(User::class,'user_wife','wife_id','user_id')
            ->using(UserWife::class)
            ->withPivot(['marriage_start_date','marriage_end_date'])
            ->as('husband');
    }


    public function divorcedHusbands()
    {
        return $this->hasMany(UserWife::class,'wife_id','id')->whereNotNull("marriage_end_date");
    }

    public function divorcedwives()
    {
        return $this->hasMany(UserWife::class,'user_id','id')->whereNotNull("marriage_end_date");
    }

    public function getFamilyName(): string
    {
        return $this->family ? $this->family->title : $this->name;
    }

    public function getFamilyCount(): string
    {
        return $this->family ? $this->family->members.' '.__('family_mem') : '0 family members';
    }

    public function isAlreadyMarriedToHusband(?User $husband = null): bool
    {
        return $this->husbands()
            ->when($husband,function ($query) use ($husband) {
                $query->where('user_id',$husband->id);
            })
            ->whereNull('marriage_end_date')
            ->exists();
    }

    public function isAlreadyMarriedWife(?User $wife = null): bool
    {
        return $this->wives()
            ->when($wife,function ($query) use ($wife) {
                $query->where('wife_id',$wife->id);
            })
            ->whereNull('marriage_end_date')
            ->exists();
    }

    public function fatherHasWives(): bool
    {
        return ($this->father && $this->father->wives->isNotEmpty());
    }

    public function wifeChildrenFromHusband(User $user): Collection
    {
        return $this->wife_children()->where('father_id',$user->id)->get();
    }

    public function countAge(): int
    {
        return Carbon::now()->diffInYears($this->birth_date);
    }

    public function getTextualAge(): string
    {
        return $this->countAge()
            ? $this->countAge() .' '.__('years')
            : '';
    }

    public function countDie(): int
    {
        return Carbon::parse($this->birth_date)->diffInYears($this->death_date);
    }

    public function getDieAge(): string
    {
        return $this->countDie()
            ? __('lived'). $this->countDie() .' '.__('years')
            : '';
    }

    public function countDieS(): int
    {
        return Carbon::now()->diffInYears($this->death_date);
    }
    public function countDieSs(): int
    {
        return Carbon::now()->diffInMonths($this->death_date);
    }

    public function getDieSince(): string
    {
        if($this->countDieS() == "78"){
            return $this->countDieS()
            ? __('unknown_departure_date')
            : '';
        }else{
            if($this->countDieS() > "0"){
                return $this->countDieS()
            ? __('leave_us'). $this->countDieS() . __('years_ago')
            : '';
            }else{
                return $this->countDieSs()
            ? __('leave_us'). $this->countDieSs() . __('month_ago')
            : '';
            }


        }
    }

    public function countMarige($marriage_start_date): int
    {
        return Carbon::now()->diffInYears($marriage_start_date);
    }

    public function getMarriageAge($marriage_start_date): string
    {
        return $this->countMarige($marriage_start_date)
            ? $this->countMarige($marriage_start_date) .' '.__('years')
            : 'null';
    }

    public function posts()
    {
      return $this->belongsTo('App\Models\Post', 'post', 'user_id', 'id');
    }

    public function user_status()
    {
        return $this->belongsTo(Session::class,'id','user_id');
    }
    public function user_shares()
    {
        return $this->HasMany(ShareUserPayment::class); 
    }

    /**
     * Update the user's profile photo.
     *
     * @param  mixed  $photo
     * @return void
     */
    public function updateProfilePhoto($photo)
    {
        $this->deleteProfilePhoto();

        $path = $photo->storeAs('profile-photos', $photo->getClientOriginalName(), 'public_uploads');

        $this->forceFill([
            'profile_photo_path' => 'uploads/'.$path,
        ])->save();
    }

    /**
     * Delete the user's profile photo.
     *
     * @return void
     */
    public function deleteProfilePhoto()
    {
        if ($this->profile_photo_path) {
            Storage::disk('public_uploads')->delete($this->profile_photo_path);
        }
    }

}
