<?php

function arabic_w2e($str) {
	if(app()->getLocale() == 'ar'){
		$arabic_eastern = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
		$arabic_western = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
	}elseif(app()->getLocale() == 'cn'){
		$arabic_eastern = array('零', '一', '二', '三', '四', '五', '六', '七', '八', '九');
		$arabic_western = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
	}else{
		$arabic_eastern = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
		$arabic_western = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
	}
	

	return str_replace($arabic_western, $arabic_eastern, $str);
}
