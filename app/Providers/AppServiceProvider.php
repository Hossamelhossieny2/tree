<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Session;
use App\Models\User;
use App\Models\UserMsg;
use App\Models\Notification;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Models\FamilyName;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view) {
            if(Auth::user()){
                $father_fam_ca = FamilyName::find(auth()->user()->family_id);
                $finids = unserialize($father_fam_ca->fam_cache);

                if(isset(auth()->user()->family_2)){
                    $mother_fam_ca = FamilyName::find(auth()->user()->family_2);
                    $minids = unserialize($mother_fam_ca->fam_cache);
                    $res1 = array_merge($finids,$minids); 
                }else{
                        $res1 = $finids;
                   }


                if(isset(auth()->user()->family_m)){
                    $wife_fam_ca = FamilyName::find(auth()->user()->family_m);
                    $winids = unserialize($wife_fam_ca->fam_cache);
                    $res2 = array_merge($res1,$winids);
                }else{
                        $res2 = $res1;
                   }

                $view->with('famUsers', User::where(function ($query) use($res2) {
                        $query->whereIn('id',$res2);
                    })->whereNull('death_date')->get());
                $view->with('ChatMsgTo', UserMsg::where('to_user',auth()->id())->with('user')->orderBy('created_at','desc')->limit(5)->get());
                 $view->with('Notif', Notification::where('notifiable_id',Auth::id())->whereNull('read_at')->get());
            }
        });
    }
}
