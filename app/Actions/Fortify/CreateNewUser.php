<?php

namespace App\Actions\Fortify;

//use App\Models\Team;
use App\Models\User;
use App\Models\WheelUser;
use App\Models\PlanUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Laravel\Jetstream\Jetstream;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'name'          => ['required', 'string', 'max:255','alpha_dash'],
            'gender'        => ['required'],
            'birth_date'    => ['required'],
            'email'         => ['required','email'],
            'country'       => ['required'],
            'phone_number'  => ['required', 'numeric', 'unique:users'],
            'password'      => $this->passwordRules(),
            'terms'         => Jetstream::hasTermsAndPrivacyPolicyFeature() ? ['required', 'accepted'] : '',
        ])->validate();
        
        return DB::transaction(function () use ($input) {
            return tap(User::create([
                'name'          => $input['name'],
                'birth_date'    => $input['birth_date'],
                'email'         => $input['email'],
                'country_id'    => $input['country'],
                'phone_number'  => $input['phone_number'],
                'gender'        => $input['gender'],
                'password'      => Hash::make($input['password']),
            ]), function (User $user) {
                $this->createFamily($user);
                $this->createQuranPlan($user);
                $userWheel = new WheelUser;
                $userWheel->user_id = $user->id;
                $userWheel->save();
            });
        });
    }

    protected function createFamily(User $user)
    {
        $ids[] = $user->id;
        $serial = serialize($ids);

        $newFam = $user->family()->create([
            'title' => $user->name.'(FAM)',
            'fam_cache' => $serial,
            'members' => '1',
            'live' => '1',
            $user->gender => '1'
        ]);
        $user->update(['family_id'=>$newFam->id]);

    }

    protected function createQuranPlan(User $user)
    {
        $new_user_plan = new PlanUser;
        $new_user_plan->user_id = $user;
        $new_user_plan->sura = '114';
        $new_user_plan->aya = '1';
        $new_user_plan->page = '604';
        $new_user_plan->save();

    }
}
