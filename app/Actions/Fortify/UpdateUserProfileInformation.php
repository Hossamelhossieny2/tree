<?php

namespace App\Actions\Fortify;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\UpdatesUserProfileInformation;
use Illuminate\Support\Facades\Storage;

class UpdateUserProfileInformation implements UpdatesUserProfileInformation
{
    /**
     * Validate and update the given user's profile information.
     *
     * @param  mixed  $user
     * @param  array  $input
     * @return void
     */
    public function update($user, array $input)
    {
        Validator::make($input, [
            'name'          => ['required', 'string', 'max:255'],
            'phone_number'  => ['required', 'regex:/(01)[0-9]{9}/', 'unique:users,id,'.$user->id],
            'email'         => ['required', 'email', 'max:255'],
            'birth_date'    => ['nullable', 'date'],
            'photo'         => ['nullable', 'mimes:jpg,jpeg,png', 'max:1024'],
            'profile_photo' => ['nullable', 'image', 'max:1024'], // Validate the image
        ]);
        
        if (isset($input['photo'])) {
            $user->updateProfilePhoto($input['photo']);
        }

        if (isset($input['profile_photo'])) {
            $user->updateProfilePhoto($input['profile_photo']);
        }

        if ($input['email'] !== $user->email &&
            $user instanceof MustVerifyEmail) {
            $this->updateVerifiedUser($user, $input);
        } else {
            $user->forceFill([
                'name'          => $input['name'],
                'email'         => $input['email'],
                'phone_number'  => $input['phone_number'],
                'birth_date'    => $input['birth_date'],
            ])->save();
        }
    }

    /**
     * Update the given verified user's profile information.
     *
     * @param  mixed  $user
     * @param  array  $input
     * @return void
     */
    protected function updateVerifiedUser($user, array $input)
    {
        $user->forceFill([
            'name'  => $input['name'],
            'email' => $input['email'],
            'email_verified_at' => null,
        ])->save();

        $user->sendEmailVerificationNotification();
    }
}
