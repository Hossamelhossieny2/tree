<?php

namespace App\Traits;

use Intervention\Zodiac\AbstractZodiac;
use Intervention\Zodiac\Exceptions\NotReadableException;
use Intervention\Zodiac\Facades\Zodiac;
use Intervention\Zodiac\Calculator;
use DateTime;
use Carbon\Carbon;

trait CustomZodiac
{
    protected $zodiacAttribute = 'birth_date';

    public function getZodiacAttribute(): ?AbstractZodiac
    {
        $birthday = data_get($this->attributes, $this->zodiacAttribute);
        //dd(Calculator::zodiac($birthday));
        $zodiac = Calculator::zodiac($birthday);
        $name = $zodiac->name(); // 'gemini'
        $html = $zodiac->html(); // '♊︎'
        $localized = $zodiac->localized('fr'); // Gémeaux
        $compatibility = $zodiac->compatibility($zodiac); // .6
        //dd($name, $html, $localized, $compatibility);
        //dd($zodiac);
        try {
            return $zodiac;
        } catch (NotReadableException $e) {
            return null;
        }
    }
}
