<?php

namespace App\View\Components;

use App\Models\Like;
use App\Models\Post;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\Component;

class PostLikedUsers extends Component
{
    public Post $post;
    public Collection $liked_users;

    public function __construct(Post $post)
    {
        $this->post = $post;
        $this->liked_users = $post->likes()->groupBy('user_id')->limit(6)->get()->map(function (Like $like) {
            $like->user_photo = $like->user->profile_photo_url ?? '';
            $like->user_name = $like->user->name ?? '';
           return $like;
        });
    }

    public function render()
    {
        return view('components.post-liked-users');
    }
}
