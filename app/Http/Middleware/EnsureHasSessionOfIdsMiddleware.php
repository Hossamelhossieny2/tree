<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class EnsureHasSessionOfIdsMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if(session('ids')){
            if(in_array($request->user()->id, session('ids')[0])){
                return $next($request);
            }else{
                session()->flash('message', __('out_of_fam'));
                return redirect(route('family_dashboard',app()->getLocale()));
            }
        }else{
            return redirect(route('family_dashboard',app()->getLocale()));
        }
    }
}
