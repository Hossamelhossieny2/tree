<?php

namespace App\Http\Responses;

use Illuminate\Support\Facades\Auth;
use Laravel\Fortify\Contracts\LoginResponse as LoginResponseContract;

class LoginResponse implements LoginResponseContract
{

    public function toResponse($request)
    {
        $home = auth()->user()->is_admin ? app()->getLocale() .'/admin' : app()->getLocale() .'/family_dashboard';
 
        activity()
           ->withProperties(['view' => 'login' ,'action' => 'success login '.auth()->user()->name])
            ->useLog('authintication')
           ->log('Success Login To your Account');

        return redirect()->intended($home);
    }

}