<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserMsg;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    public function getChat(User $user)
    {
        $userMsg = UserMsg::where('user_id',auth()->id())->where('to_user',$user->id)->whereNull('replay')->with('user')->first();
        
        if(!empty($userMsg->id)){
            $userRep = UserMsg::where('replay',$userMsg->id)->with('user')->get();
             if(count($userRep)>0){
                    $user_msg = $userMsg;
                    $user_rep = $userRep;
                }else{
                    $user_msg = $userMsg;
                    $user_rep = "";
                }
            }else{
                $TouserMsg = UserMsg::where('to_user',auth()->id())->where('user_id',$user->id)->whereNull('replay')->with('user')->first();
                if(!empty($TouserMsg->id)){
                    $user_msg = $TouserMsg;
                    $TouserRep = UserMsg::where('replay',$TouserMsg->id)->with('user')->get();
                    if(count($TouserRep)>0){
                        $user_msg = $TouserMsg;
                        $user_rep = $TouserRep;
                    }else{
                        $user_msg = $TouserMsg;
                        $user_rep = "";
                    }
                }else{
                    $user_msg = "";
                    $user_rep = "";
                }
            }

        $arr = [
                'user'  => $user,
                'msg'   => $user_msg,
                'replay'=> $user_rep
            ];
    
        return $arr;
    }

    public function add_user_msg(Request $request)
    {
        if(!empty($request['body'])){
            $msg = new userMsg;
        $msg->user_id = auth()->id();
        $msg->to_user = $request['touser'];
        $msg->replay = $request['msgid'];
        $msg->body = $request['body'];
        $msg->save();
        }
        
        $touser = User::find($request['touser']);
        return $touser;
    }
}
