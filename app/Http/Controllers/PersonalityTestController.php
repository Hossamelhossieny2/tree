<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PersonalityTest;
use App\Models\FaqsHelp;
use App\Models\UserHelp;
use App\Models\UserPersonality;
use App\Models\PersonalityDesc;
use DB;

class PersonalityTestController extends Controller
{
    public function index()
    {
    	$user_test = UserPersonality::where('user_id',auth()->id())->first();
        if(!isset($user_test->id)){
            $new_personality = new UserPersonality;
            $new_personality->user_id = auth()->id();
            $new_personality->last_ques = '1';
            $new_personality->save();

            $arr['test'] = PersonalityTest::where('lang',app()->getLocale())->where('ques_id','1')->first();
        }else{
            if(!isset($user_test->person)){
            $arr['test'] = PersonalityTest::where('lang',app()->getLocale())->where('ques_id',$user_test->last_ques)->first();
        }else{
            //$arr['test_res'] = PersonalityDesc::where('lang',app()->getLocale())->where('person_tag',$user_test->person)->first();
                $arr['test_res'] = DB::table('general')->where('person',$user_test->person)->where('lang',app()->getLocale())->first();
                $arr['test_res_full'] = DB::table('general')->where('person',$user_test->person)->where('lang','en')->first();
            }
        }
    	
    	$arr['faq'] = []; 
    	$arr['FaqsHelp'] = FaqsHelp::where('topic_id',6)->where('lang',app()->getLocale())->get();
        $arr['needHelp'] = UserHelp::where('topic_id',6)->where('user_id',auth()->id())->first();
//dd($user_test);
    	return view('evaluations.personlity',$arr);
    }

    public function person_ques(request $request)
    {
    	// if($request['question'] == 1){
    	// 	$ques = new UserPersonality;
    	// 	$ques->user_id = auth()->id();
    	// 	$ques->save();
    	// }

    	$uptest = UserPersonality::where('user_id',auth()->id())->first();
    	$uptest->last_ques = $request['question'] + 1;
    	$uptest->increment($request['ans'],1);
    	$uptest->save();

    	if($request['question'] == 70){
    		$pp = '';
    		if($uptest->E > $uptest->I){
    			$pp = $pp .'E';
    		}else{
    			$pp = $pp .'I';
    		}

    		if($uptest->S > $uptest->N){
    			$pp = $pp .'S';
    		}else{
    			$pp = $pp .'N';
    		}

    		if($uptest->T > $uptest->F){
    			$pp = $pp .'T';
    		}else{
    			$pp = $pp .'F';
    		}

    		if($uptest->J > $uptest->P){
    			$pp = $pp .'J';
    		}else{
    			$pp = $pp .'P';
    		}

		$uptest->person = $pp;   		
    	$uptest->save();
    	}
		return back()->with('successs',__('evaluation_added'));
    }
}
