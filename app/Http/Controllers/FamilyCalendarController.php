<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Familycalendar;
use App\Models\UserWife;
use App\Models\EventJoin;
use App\Models\Post;
use App\Models\FaqsHelp;
use App\Models\UserHelp;
use App\Models\CountryEvent;
use App\Models\MyFamily;
use Carbon\Carbon;


class FamilyCalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data['FaqsHelp'] = FaqsHelp::where('topic_id',4)->where('lang',app()->getLocale())->get();
        $data['needHelp'] = UserHelp::where('topic_id',4)->where('user_id',auth()->id())->first();

        $users = User::whereIn('id',session('ids')[0])->get();
        $events =[];
        $today_events =[];
        $no = 0;
        foreach($users as $user){
            if(!empty($user->father)){
                $fullName = $user->name.' '.$user->father->name;
            }else{
                $fullName = $user->name;
            }
            
            if(!empty($user->birth_date)){

                $this_birthdate = \Carbon\Carbon::parse($user->birth_date)->year(now()->format('Y'))->format('Y-m-d');
                $events[] = [
                    'id'=> $no,
                    'title' => __('birthday').' '.$fullName,
                    'start' => $this_birthdate,
                    'url' => 'javascript:showNames('.$user->id.',1)',
                    'txt' => "white",
                    'bg'  => "#E01921",
                    'day' => 'true',
                    'type' => __('birth_anv'),
                    'pic'=> $user->profile_photo_path
                ];

                if($this_birthdate == date('Y-m-d')){
                    $today_events[] = [
                        'id'=> $no,
                        'title' => __('birthday').' '.$user->name.' '.$user->family->title,
                        'start' => $this_birthdate,
                        'url' => 'javascript:showNames('.$user->id.',1)',
                        'user' => __('system_robot'),
                        'user_img' => 'img/default/system.png'
                    ];
                }
            }

            if(!empty($user->death_date)){

                $this_deathdate = \Carbon\Carbon::parse($user->death_date)->year(now()->format('Y'))->format('Y-m-d');
                //dd($this_birthdate);
                $events[] = [
                    'id'=> $no,
                    'title' => __('death_anv').' '.$fullName,
                    'start' => $this_deathdate,
                    'url' => "javascript:showNames(".$user->id.",2)",
                    'txt' => "white",
                    'bg'  => "black",
                    'day' => 'true',
                    'type' => __('death_anv'),
                    'pic'=> $user->profile_photo_path
                ];

                if($this_deathdate == date('Y-m-d')){
                    $today_events[] = [
                        'id'=> $no,
                        'title' => __('death_anv').' '.$user->name.' '.$user->family->title,
                        'start' => $this_deathdate,
                        'url' => 'javascript:showNames('.$user->id.',2)',
                        'user' => __('system_robot'),
                        'user_img' => 'img/default/system.png'
                    ];
                }
            }
            

            if($user->gender == 'male'){
                if( $user->wives()->count() > 0 ){
                    $hus = $user->wives()->get();
                    foreach($hus as $partner){
                        if($partner->marriage->marriage_start_date != null){
                        $this_marriage = \Carbon\Carbon::parse($partner->marriage->marriage_start_date)->year(now()->format('Y'))->format('Y-m-d');

                    $events[] = [
                        'id'=> $no,
                        'title' => __('anv_wed').' '.$fullName.' with '.$partner->name,
                        'start' => $this_marriage,
                        'url' => 'javascript:showMarriage('.$user->id.','.$partner->id.',4)',
                        'txt' => "yellow",
                        'bg'  => "blue",
                        'day' => 'true',
                        'type' => __('anv_wed'),
                        'pic'=> $user->profile_photo_path,
                        'pic2'=> $partner->profile_photo_path
                    ];
                    if($this_marriage == date('Y-m-d')){
                        if(!empty($user->family->title)){
                            $couple_name = $user->name.' '.$user->family->title;
                        }else{
                            $couple_name = $fullName;
                        }
                        if(!empty($partner->family->title)){
                            $couple_name2 = $partner->name.' '.$partner->family->title;
                        }else{
                            $couple_name2 = $partner->name;
                        }
                        $today_events[] = [
                            'id'=> $no,
                            'title' => __('anv_wed').' '.$couple_name.' with '.$couple_name2,
                            'start' => $this_marriage,
                            'url' => 'javascript:showMarriage('.$user->id.','.$partner->id.',4)',
                            'user' => __('system_robot'),
                            'user_img' => 'img/default/system.png'
                        ];
                    }

                    }
                }
                }
            }

        }
        // user added events to family
         $user_events = FamilyCalendar::whereIn('id',session('ids')[0])->with('userEvents')->get();
        foreach($user_events as $user_event){
                     $events[] = [
                        'id'    => $no,
                        'title' => $user_event->event_name,
                        'start' => $user_event->event_date,
                        'url' => 'javascript:showDetails('.$user_event->id.')',
                        'txt' => "#000",
                        'bg'  => "#F6E505",
                        'day' => 'false',
                        'type' => __('user_event'),
                        'pic'=> $user_event->userEvents->profile_photo_path
                    ];

                    if($user_event->event_date == date('Y-m-d')){
                        if($user_event->userEvents->profile_photo_path){
                            $user_image = $user_event->userEvents->profile_photo_path;
                        }else{
                            $user_image = 'img/default/user_male.png';
                        }
                        $today_events[] = [
                            'id'=> $no,
                            'title' => $user_event->event_name,
                            'start' => $user_event->event_time,
                            'url' => 'javascript:showDetails('.$user_event->id.')',
                            'user' => $user_event->userEvents->name,
                            'user_img' => $user_image
                        ];
                    }
                    $no+=1;
        }


         $country_events = CountryEvent::orderBy('start_date','asc')->where('country_id',auth()->user()->country_id)->get();
         foreach($country_events as $ce){

            $tit = 'title_'.app()->getLocale();
            $flag = 'img/flag/'.strtolower($ce->country_iso).'.svg';

            $events[] = [
                        'id'=> $ce->id,
                        'title' => $ce->$tit,
                        'start' => $ce->start_date,
                        'end' => $ce->end_date,
                        'url' => 'javascript:showCountry('.$ce->id.')',
                        'txt' => "#fff",
                        'bg'  => "#694DA8",
                        'day' => 'false',
                        'type' => __('country_vac'),
                        'pic'=> $flag
                    ];

                    if($ce->start_date == date('Y-m-d')){



                        $today_events[] = [
                            'id'=> $no,
                            'title' => $ce->$tit,
                            'start' => $ce->start_date,
                            'end' => $ce->end_date,
                            'url' => 'javascript:showDetails('.$ce->id.')',
                            'user' => $ce->country_iso,
                            'user_img' => $flag
                        ];
                    }
         }

        $data['events'] = $events;
        $data['today_events'] = $today_events;
        $data['event_joins'] = EventJoin::with('events.userEvents')->whereIn('id',session('ids')[0])->with('user_join')->get();

        return view('familycalender.view', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function join_event(Request $request)
    {

        $event_join = new EventJoin;
        $event_join->user_id = auth()->user()->id;
        $event_join->event_id = $request['event_id'];
        $event_join->confirm = $request['event_confirm'];
        $event_join->save();

        $event = FamilyCalendar::find($request['event_id']);
        activity()
           ->withProperties(['event_id' => $event->id,'event_name' => $event->event_name,'event_privacy' => $event->event_privacy,'event_time' => $event->event_day.' '.$event->event_time.' '.$event->event_timezone ,'event_confirm'=>$request['event_confirm'] ])
            ->useLog('join event')
           ->log($event->event_description);

        return redirect()->back()->with('success', __('ev_react'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'event_title' => 'required|max:255',
            'event_place' => 'required',
            'event_day' => 'required',
            'event_time' => 'required',
            'event_desc' => 'required',
        ]);
        $event = new Familycalendar;
        $event->user_id = auth()->user()->id;
        $event->family_id = auth()->user()->family_id;
        $event->event_name = $validated['event_title'];
        $event->event_location = $validated['event_place'];
        $event->event_date = $validated['event_day'];
        $event->event_time = $validated['event_time'];
        $event->event_description = $validated['event_desc'];
        $event->event_timezone = $request['event_timezone'];
        $event->event_per = $request['event_per'];
        $event->event_privacy = $request['event_privacy'];
        $event->save();
        //dd($new_event);
        // add this post to family posts wall
        if($request['event_privacy'] == 'public'){
            $post = new Post;
            $post->user_id = auth()->user()->id;
            $post->family_id = auth()->user()->family_id;
            $post->post = $validated['event_title'].' : ';
            $post->post .= $validated['event_desc'].' On ';
            $post->post .= $validated['event_day'].' '.$validated['event_time'].' '.$request['event_timezone'].' '.__('label_in_cal');
            $post->type = 3;
            $post->post_pic = 'img/event-def.png';
            $post->save();
        }
        //logging add event
        activity()
           ->withProperties(['event_id' => $event->id,'event_name' => $event->event_name,'event_privacy' => $event->event_privacy,'event_time' => $validated['event_day'].' '.$validated['event_time'].' '.$request['event_timezone'] ])
            ->useLog('events')
           ->log($event->event_description);

        return redirect()->route('family_calender',app()->getLocale());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return FamilyCalendar::where('familycalendars.id',$id)->leftJoin('users','users.id','familycalendars.user_id')->select('users.created_at as created_at','users.name as user','users.profile_photo_path as img','familycalendars.event_name','familycalendars.event_location as loc','familycalendars.event_date as ev-d','familycalendars.event_time as ev-t','familycalendars.event_timezone as ev-z','familycalendars.event_description as desc','familycalendars.event_per as per','familycalendars.event_privacy as ev')->first();
    }

    public function show_date($id)
    {
        return User::find($id);
    }

    public function show_country($id)
    {
        return CountryEvent::find($id);
    }

    public function show_marriage($hus,$wif,$kind)
    {
        // 3 female with male  -- 4 male with female
        if($kind == 3){
            $husband = User::find($hus);
            $wife = User::find($wif);
            $mar_dat = UserWife::where('wife_id',$wif)->where('wife_id',$hus)->first();
        }elseif($kind == 4){
            $husband = User::find($hus);
            $wife = User::find($wif);
            $mar_dat = UserWife::where('user_id',$hus)->where('wife_id',$wif)->first();
        }


        return ['husband'=>$husband->name ,'wife'=>$wife->name ,'start'=>$mar_dat->marriage_start_date,'end'=>$mar_dat->marriage_end_date,'created_at'=>$wife->created_at];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
