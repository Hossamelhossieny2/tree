<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\WheelUser;
use App\Models\WheelType;
use App\Models\WheelQues;
use App\Models\WheelMiss;
use App\Models\WheelUserMission;
use App\Models\FaqsHelp;
use App\Models\UserHelp;
use App\Models\User;
use App\Models\HelpVedio;
use Session;
use Carbon\Carbon;
use DB;

class WheelController extends Controller
{

    public function index()
    {
        $arr['wheel_types'] = WheelType::with('ques')->where('lang',app()->getLocale())->get();
        $arr['MyinitWheel'] = WheelUser::where('user_id',auth()->id())->where('init','1')->first();
        $arr['MyrunWheel'] = WheelUser::where('user_id',auth()->id())->where('init','0')->first();
        $arr['Wheelnotcomplete'] = WheelUser::where('user_id',auth()->id())->first();
        $wheelMiss = WheelMiss::where('user_id',auth()->id())->leftJoin('wheel_ques as wq', function($join){
                    $join->on('wq.q_id', '=', 'wheel_misses.q_id');
                    $join->on('wq.type','=','wheel_misses.type'); 
                })->where('lang',app()->getLocale())->get();
        $collection = collect($wheelMiss);
        $arr['WheelMiss'] = $collection->groupBy('type');

        $myMision = WheelUserMission::where('user_id',auth()->id())->leftJoin('wheel_ques as wq', function($join){
                    $join->on('wq.q_id', '=', 'wheel_user_missions.ques');
                    $join->on('wq.type','=','wheel_user_missions.type'); 
                })->where('lang',app()->getLocale())->get();
        $collection = collect($myMision);
        $arr['MyMissons'] = $collection->groupBy('quarter');

        $arr['FaqsHelp'] = FaqsHelp::where('topic_id',1)->where('lang',app()->getLocale())->get();
        $arr['needHelp'] = UserHelp::where('topic_id',1)->where('user_id',auth()->id())->first();
        $arr['VidHelp'] = HelpVedio::where('sub',6)->where('lang',app()->getLocale())->get();
        //dd($arr);
        return view('evaluations.wheel',$arr);
    }

    public function send_ques(request $request)
    {
        $type = 't'.$request->wheel_type;
        $ans = 0;
        for($i=1;$i<11;$i++){
            $user_ans = 'ans-'.$i;
            $ans += $request->$user_ans;
            if($request->$user_ans == 0){
                $add_miss =new  WheelMiss;
                $add_miss->type = $request->wheel_type;
                $add_miss->q_id = $i;
                $add_miss->user_id = auth()->id();
                $add_miss->save();
            }
        }

        $userWheel = WheelUser::where('user_id',auth()->id())->first();
        $userWheel->$type = $ans;
        $userWheel->save();
        
        if($request->wheel_type == 8){
            $userWheel = WheelUser::where('user_id',auth()->id())->first();
            $userWheel->init = 1;
            $userWheel->save();
        }
        
               Session::flash('type', $request->wheel_type);
        return back()->with('success',__('evaluation_added'));
    }

    public function add_mission()
    {
        $typ = explode('-',$_POST['Chap']);
        $type = $typ[1];
        $ques = $typ[2];

        $to = explode('missionDiv',$_POST['ToDiv']);
        $quarter = $to[1];

        $time_to = \Carbon\carbon::now()->addMonth($quarter * 3);

        $userMission = new WheelUserMission;
        $userMission->user_id = auth()->id();
        $userMission->type = $typ[1];
        $userMission->ques = $typ[2];
        $userMission->quarter = $quarter;
        $userMission->end_date = $time_to;
        $userMission->save();

        return 'done';
    }

    public function remove_help()
    {
        $UserHelp = new UserHelp;
        $UserHelp->user_id = auth()->id();
        $UserHelp->topic_id = $_POST['Topic'];
        $UserHelp->save();

        return 'done';
    }

    public function mark_mission_done()
    {
        $userMission = WheelUserMission::find($_POST['mission']);
        $userMission->is_done = $userMission->is_done + 1;
        $userMission->done_day = date('Y-m-d');
        $userMission->save();

        return 'done';
    }

}
