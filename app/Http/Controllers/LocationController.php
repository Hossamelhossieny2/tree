<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    public function index(Request $request)
    {
            $userIp = $request->ip();
            if($userIp == '127.0.0.1')$userIp = '41.46.213.47';

            $locationData = \Location::get($userIp);
            
            $get_country = Country::where('iso',$locationData->countryCode)->first;
            return $get_country->id;
    }
}