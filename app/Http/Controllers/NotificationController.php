<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Models\User;

use App\Notifications\OffersNotification;
use Notification;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
  
    public function index()
    {
        return view('product');
    }
    
    public function sendOfferNotification() {
        $userSchema = User::first();
  
        $offerData = [
            'form' => 'BOGO',
            'to' => 'You received an offer.',
            'title' => 'Thank you',
            'desc' => 'Check out the offer',
            'share_id' => 007
        ];
  
        Notification::send($userSchema, new OffersNotification($offerData));
   
        dd(Notification::send($userSchema, new OffersNotification($offerData)));
    }
}