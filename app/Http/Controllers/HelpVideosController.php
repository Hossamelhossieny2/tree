<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\HelpVedio;

class HelpVideosController extends Controller
{
    public function index()
    {
        $arr['videos'] = HelpVedio::where('sub','0')->where('lang',app()->getLocale())->with('subs')->get();

        return view('help_videos',$arr);
    }
}
