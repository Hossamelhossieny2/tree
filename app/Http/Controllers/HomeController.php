<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserInfo;
use App\Models\UserEducation;
use App\Models\Country;
use App\Models\News;
use App\Models\Testmonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
Use App\Models\FollowUser;
Use App\Models\FamilyName;
Use App\Models\Faqshelp;
use App\Events\FollowEvent;
use App\Models\PlanUser;
use App\Models\WheelUser;
use App\Models\UserPersonality;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Pusher\Pusher;
use Session;
use Laravel\Jetstream\Jetstream;

class HomeController extends Controller
{
    
    public $weathard ;

    public function index()
    {
        
        return view('dashboard');
    }

    public function webfamily()
    {
        
        $arr['news'] = News::inRandomOrder()->whereNotNull('image')->limit('3')->get();
        $arr['countries'] = Country::all();
        $testmonials = Testmonial::where('lang',app()->getLocale())->get();
        $arr['tests'] = $testmonials->shuffle();;
        $userIp = \Request::ip();;
        if($userIp == '127.0.0.1')$userIp = '41.46.213.47';
        // $locationData = \Location::get($userIp);
        // if(!empty($locationData)){
        //     $loc = $locationData->countryCode;
        // }else{
        //     $loc = 'EG';
        // }
        $loc = 'EG';
        $cc = Country::where('iso',$loc)->first();
        $arr['user_country'] = $cc->id;
        $arr['family'] = FamilyName::all()->count();
        // $arr['tree1'] = FaqsHelp::where('lang',app()->getLocale())->where('topic_id',1)->skip('1')->first();
        // $arr['tree2'] = FaqsHelp::where('lang',app()->getLocale())->where('topic_id',6)->first();
        // $arr['tree'] = FaqsHelp::where('lang',app()->getLocale())->where('topic_id',3)->first();

        return view('welcome',$arr);
    }

    public function get_user_with_info(Request $request)
    {
        $arr['this_user'] =  User::with('profile')->with('study')->with('UserFamily')->with('country')->where('id',$request['user'])->first();
        return view('user_profile',$arr);
    }

    public function SelaaLogin(Request $request)
    {
         $this->validate($request, [
            'login'     => 'required',
            'password'  => 'required',
        ]);
        $checkUser = User::where('email',$request['login'])
        ->orWhere('phone_number',$request['login'])
        ->first();
        if(!empty($checkUser->id)){
            if (Hash::check($request['password'], $checkUser->password)) {
                Auth::loginUsingId($checkUser->id);
                //dd($request['part']);
                    $father_fam_ca = FamilyName::find(auth()->user()->family_id);
                    $finids = unserialize($father_fam_ca->fam_cache);
                Session::push('ids', $finids);
                if(isset(auth()->user()->family_2)){
                    $mother_fam_ca = FamilyName::find(auth()->user()->family_2);
                    $minids = unserialize($mother_fam_ca->fam_cache);
                Session::push('ids', $minids);
                }
                if(isset(auth()->user()->family_m)){
                    $wife_fam_ca = FamilyName::find(auth()->user()->family_m);
                    $winids = unserialize($wife_fam_ca->fam_cache);
                Session::push('ids', $winids);
                }
                return redirect()->route('family_dashboard',app()->getLocale());
            }else{
                return redirect()->back()->withInput($request->only('email', 'remember'))->withErrors([
                    'approve' => __('auth.password'),
                ]);
            }
        }else{
            return redirect()->back()->withInput($request->only('email', 'remember'))->withErrors([
                    'approve' => __('auth.failed'),
                ]);
        }
    }

    public function SelaaRegister(Request $request)
    {
        $this->validate($request, [
            'email'     => 'required|unique:users',
            'password'  => 'required|confirmed|min:4',
            'terms'     => Jetstream::hasTermsAndPrivacyPolicyFeature() ? ['required', 'accepted'] : '',
        ]);
        DB::transaction(function () use ($request) {
             tap(User::create([
                'name'          => 'Guest User',
                'birth_date'    => \carbon\Carbon::now(),
                'email'         => $request['email'],
                'country_id'    => $request['country'],
                'phone_number'  => 'null',
                'gender'        => $request['gender'],
                'password'      => Hash::make($request['password']),
            ]), function (User $user) {
                $this->createFamily($user);
                $userWheel = new WheelUser;
                $userWheel->user_id = $user->id;
                $userWheel->save();
                $ques = new UserPersonality;
                $ques->user_id = $user->id;
                $ques->last_ques = '1';
                $ques->save();
                $memo = new PlanUser;
                $memo->user_id = $user->id;
                $memo->sura = 114;
                $memo->aya = 1;
                $memo->page = 604;
                $memo->save();

                Auth::loginUsingId($user->id);
                $father_fam_ca = FamilyName::find(auth()->user()->family_id);
                    $finids = unserialize($father_fam_ca->fam_cache);
                Session::push('ids', $finids);
                if(isset(auth()->user()->family_2)){
                    $mother_fam_ca = FamilyName::find(auth()->user()->family_2);
                    $minids = unserialize($mother_fam_ca->fam_cache);
                Session::push('ids', $minids);
                }
                if(isset(auth()->user()->family_m)){
                    $wife_fam_ca = FamilyName::find(auth()->user()->family_m);
                    $winids = unserialize($wife_fam_ca->fam_cache);
                Session::push('ids', $winids);
                }
            });
        });

        return redirect()->route($request['part'],app()->getLocale());
    }

    public function logUser($user='')
    {
        # code...
    }
    public function login(Request $request)
    {
        $this->validate($request, [
            'email'     => 'required',
            'password'  => 'required',
            'part'      => 'required',
        ]);
        

        $checkUser = User::where('email',$request['email'])->first();
        if(!empty($checkUser->id)){
            if (Hash::check($request['password'], $checkUser->password)) {
                Auth::loginUsingId($checkUser->id);
                //dd($request['part']);
                    $father_fam_ca = FamilyName::find(auth()->user()->family_id);
                    $finids = unserialize($father_fam_ca->fam_cache);
                Session::push('ids', $finids);
                if(isset(auth()->user()->family_2)){
                    $mother_fam_ca = FamilyName::find(auth()->user()->family_2);
                    $minids = unserialize($mother_fam_ca->fam_cache);
                Session::push('ids', $minids);
                }
                if(isset(auth()->user()->family_m)){
                    $wife_fam_ca = FamilyName::find(auth()->user()->family_m);
                    $winids = unserialize($wife_fam_ca->fam_cache);
                Session::push('ids', $winids);
                }
                return redirect()->route($request['part'],app()->getLocale());
            }else{
                return redirect()->back()->withInput($request->only('email', 'remember'))->withErrors([
                    'approve' => __('auth.password'),
                ]);
            }
        }else{
            $this->validate($request, [
                'email'     => 'required|unique:users',
                'password'  => 'required',
                'part'      => 'required',
            ]);
            DB::transaction(function () use ($request) {
             tap(User::create([
                'name'          => 'Guest User',
                'birth_date'    => \carbon\Carbon::now(),
                'email'         => $request['email'],
                'country_id'    => $request['country'],
                'phone_number'  => 'null',
                'gender'        => 'male',
                'password'      => Hash::make($request['password']),
            ]), function (User $user) {
                $this->createFamily($user);
                $userWheel = new WheelUser;
                $userWheel->user_id = $user->id;
                $userWheel->save();
                $ques = new UserPersonality;
                $ques->user_id = $user->id;
                $ques->last_ques = '1';
                $ques->save();

                Auth::loginUsingId($user->id);
            });
        });

        return redirect()->route($request['part'],app()->getLocale());
        }
         

    }

    protected function createFamily(User $user)
    {
        $ids[] = $user->id;
        $serial = serialize($ids);

        $newFam = $user->family()->create([
            'title' => $user->name.'(FAM)',
            'fam_cache' => $serial,
            'members' => '1',
            'live' => '1',
            $user->gender => '1'
        ]);
        $user->update(['family_id'=>$newFam->id]);

    }

}
