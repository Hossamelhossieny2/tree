<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\QuranSurah;
use App\Models\QuranWord;
use App\Models\PlanUser;
use App\Models\Tafseer1;
use App\Models\Tafseer2;
use App\Models\Tafseer3;
use App\Models\Tafseer4;

class QuranController extends Controller
{
    public function index()
    {
    	$arr['plan_user'] = PlanUser::where('user_id',auth()->id())->orderBy('id','desc')->first();

    	$arr['memos'] = PlanUser::where('user_id',auth()->id())->get()->groupBy(function($data) {
            return $data->sura;
            });
    	// $arr['memos'] = $memo->keys();
    	//dd($arr['memos'] = PlanUser::where('user_id',auth()->id())->get());
    	return view('quran.view',$arr);
    }

    public function memo($lang,$sura=null,$page=null)
    {
    	$plan_user = PlanUser::where('user_id',auth()->id())->orderBy('id','desc')->first();
    	if($sura == null){
    		return redirect()->route('quran.memo', [app()->getLocale(),$plan_user->sura,$plan_user->page]);
    	}
    	
    	if($page == 0){
            $pg = QuranWord::where('sura',$sura)->first();

            return redirect()->route('quran.memo', [app()->getLocale(),$sura,$pg->page]);
        }

        if($page % 2 == 1){
             $rr = QuranWord::where('page',$page)->get()->groupBy(function($data) {
            return $data->line;
            });
              $ll = QuranWord::where('page',$page+1)->get()->groupBy(function($data) {
                return $data->line;
                });
        }else{
             $rr = QuranWord::where('page',$page-1)->get()->groupBy(function($data) {
            return $data->line;
            });
              $ll = QuranWord::where('page',$page)->get()->groupBy(function($data) {
            return $data->line;
            });
              
        }
        
        $rightPagee['numberR'] = '';
        $leftPagee['numberL'] = '';
        $meta['last'] = '';
        $meta['last_page'] = '';
        $meta['last_aya'] = '';
        $meta['last_aya_global'] = '';
        $meta['tafseer1'] = '';
        $meta['tafseer2'] = '';
        $meta['tafseer3'] = '';
        $meta['tafseer4'] = '';

        for($i=1;$i<16;$i++){
            $rightPagee[$i] = '<div class="LineAya">';
            if(isset($rr[$i])){
                
                foreach($rr[$i] as $r){
                    if($rightPagee['numberR'] == ''){
                        $rightPagee['numberR'] = arabic_w2e($r->page);
                        $rightPagee['juzR'] = $r->juz;
                        $rightPagee['hezbR'] = $r->hezb;
                    }
                    if($r->char_type == 'end'){
                        $rightPagee[$i] = $rightPagee[$i].' '.arabic_w2e($r->aya);
                        
                    }else{
                        if($plan_user['sura'] > $r->sura ){
                            $rightPagee[$i] = $rightPagee[$i].' <label aya="'.$r->aya_global.'" class="not_read">'.$r->text.'</label>';
                        }else{
                            if($plan_user['sura'] == $r->sura && $plan_user['aya'] == $r->aya ){
                             $rightPagee[$i] = $rightPagee[$i].' <label aya="'.$r->aya_global.'" class="last">'.$r->text.'</label>';
                             $lastt = $this->listen_link($r->sura,$r->aya,0);
                             $meta['last'] = $lastt;
                             $lastp = $this->page_link($r->page);
                             $meta['last_page'] = $lastp;
                             $meta['last_aya_global'] = $r->aya_global ;
                             $meta['last_aya'] = $meta['last_aya'].' '.$r->text;
                             $meta['tafseer1'] = Tafseer1::where('sura',$r->sura)->where('aya',$r->aya)->first()->tafseer;
                             $meta['tafseer2'] = Tafseer2::where('sura',$r->sura)->where('aya',$r->aya)->first()->tafseer;
                             $meta['tafseer3'] = Tafseer3::where('sura',$r->sura)->where('aya',$r->aya)->first()->tafseer;
                             $meta['tafseer4'] = Tafseer4::where('sura',$r->sura)->where('aya',$r->aya)->first()->tafseer;
                           }else{
                            $rightPagee[$i] = $rightPagee[$i].' <label aya="'.$r->aya_global.'" class="read_done">'.$r->text.'</label>';
                            }
                        }
                        
                        if($r->text == "۞"){
                            $rightPagee['finishR'] = $r->line;
                        }
                    }
                    
                }
                $rightPagee[$i] = $rightPagee[$i].'<span> </span></div>';
            }else{
                 
                if($rr->take(1)->first()[0]->line == 2){
                    $rightPagee[$i] = '<li class="text-center LineAya"><label class="suraStart">﷽</label></li>';
                }else{
                    if($sura!=9){
                        if($i<13){
                            $sn = $rr[$i+2][0]->sura;
                        }else{
                            $sn = $ll[2][0]->sura;
                        }
                        $rightPagee[$i] = '<li class="LineAya"><label class="suraName">( '.arabic_w2e(QuranSurah::find($sn)->id).' ) '.QuranSurah::find($sn)->arabic.'</label></li>';

                        $i=$i+1;
                        if($i!=16)
                        $rightPagee[$i] = '<li class="text-center LineAya"><label class="suraStart pt">﷽</label></li>';
                    }else{
                        $rightPagee[$i] = '<li class="LineAya"><label class="suraName">( '.arabic_w2e(QuranSurah::find($sn)->id).' ) '.QuranSurah::find($rr[$i+2][0]->sura)->arabic.'</label></li>';
                    }
                    
                }
                
            }

        }
        for($i=1;$i<16;$i++){
            
            if(isset($ll[$i])){
                $leftPagee[$i] = '<div class="LineAya">';
                foreach($ll[$i] as $l){
                            $leftPagee['numberL'] = arabic_w2e($l->page);
                            $leftPagee['juzL'] = $l->juz;
                            $leftPagee['hezbL'] = $l->hezb;
                    if($l->char_type == 'end'){
                        $leftPagee[$i] = $leftPagee[$i].' '.arabic_w2e($l->aya);
                        
                    }else{

                        if($plan_user['sura'] > $l->sura ){
                            $leftPagee[$i] = $leftPagee[$i].' <label aya="'.$l->aya_global.'" class="not_read">'.$l->text.'</label>';
                        }else{
                           if($plan_user['sura'] == $l->sura && $plan_user['aya'] == $l->aya ){
                             $leftPagee[$i] = $leftPagee[$i].' <label aya="'.$l->aya_global.'" class="last">'.$l->text.'</label>';
                              $lastt = $this->listen_link($l->sura,$l->aya,0);
                             $meta['last'] = $lastt;
                             $lastp = $this->page_link($l->page);
                             $meta['last_page'] = $lastp;
                             $meta['last_aya_global'] = $l->aya_global ;
                             $meta['last_aya'] = $meta['last_aya'].' '.$l->text;
                             $meta['tafseer1'] = Tafseer1::where('sura',$l->sura)->where('aya',$l->aya)->first()->tafseer;
                             $meta['tafseer2'] = Tafseer2::where('sura',$l->sura)->where('aya',$l->aya)->first()->tafseer;
                             $meta['tafseer3'] = Tafseer3::where('sura',$l->sura)->where('aya',$l->aya)->first()->tafseer;
                             $meta['tafseer4'] = Tafseer4::where('sura',$l->sura)->where('aya',$l->aya)->first()->tafseer;
                           }else{
                            $leftPagee[$i] = $leftPagee[$i].' <label aya="'.$l->aya_global.'" class="read_done">'.$l->text.'</label>';
                            }
                        
                            if($l->text == "۞"){
                                $leftPagee['finishL'] = $l->line;
                            }
                        }
                    }
                    
                }
                $leftPagee[$i] = $leftPagee[$i].'<span> </span></div>';
            }else{
                if($ll->take(1)->first()[0]->line == 2){
                    $leftPagee[$i] = '<li class="text-center LineAya"><label class="suraStart">﷽</label></li>';
                }else{
                    if($i<13){
                            $sn = $ll[$i+2][0]->sura;
                        }else{
                            $sn = $ll[14][0]->sura+1;
                        }
                    $leftPagee[$i] = '<li><label class="suraName">( '.arabic_w2e(QuranSurah::find($sn)->id).' ) '.QuranSurah::find($sn)->arabic.'</label></li>';
                $i=$i+1;
                if($i!=16)
                    $leftPagee[$i] = '<li class="text-center LineAya"><label class="suraStart">﷽</label></li>';
                }
                
            }

        }
        
        $arr['meta'] = $meta;
        $arr['rightPagee'] = $rightPagee ;
        $arr['leftPagee'] = $leftPagee ;

    	return view('quran.memo',$arr);
    }

    public function listen_link($sura,$aya,$sheik)
    {
        if(strlen($aya) == 1){
            $ayaa = '00'.$aya;
        }elseif(strlen($aya) == 2){
            $ayaa = '0'.$aya;
        }elseif(strlen($aya) == 3){
            $ayaa = $aya;
        }

        if(strlen($sura) == 1){
            $suraa = '00'.$sura;
        }elseif(strlen($sura) == 2){
            $suraa = '0'.$sura;
        }elseif(strlen($sura) == 3){
            $suraa = $sura;
        }
        return $suraa.$ayaa;
    }

    public function page_link($page)
    {
        if(strlen($page) == 1){
            $pagee = '00'.$page;
        }elseif(strlen($page) == 2){
            $pagee = '0'.$page;
        }elseif(strlen($page) == 3){
            $pagee = $page;
        }

        return $pagee;
    }

    public function next($lang,$ayasura)
    {
        $ayah = str_split($ayasura,3);
        $sura = $ayah[0];
        $aya = $ayah[1];
        $page = $ayah[2];

        $sura_name=QuranSurah::where('id',$sura)->first();
        $new_aya = $aya + 1;
        if($new_aya <= $sura_name->ayah){
            $record_aya = $new_aya;
            $sura_link = $sura;
            $page_link = $page;
            
        }elseif($new_aya > $sura_name->ayah){
            $second_sura=QuranWord::where('sura',$sura-1)->orderBy('id','desc')->first();
            $record_aya = 1;
            $sura_link = $sura-1;
            $page_link = $second_sura->page;
        }

        $new_user_plan = new PlanUser;
        $new_user_plan->user_id = auth()->id();
        $new_user_plan->sura = $sura_link;
        $new_user_plan->aya = $record_aya;
        $new_user_plan->page = $page_link;
        $new_user_plan->save();
        
        return redirect()->route('quran.memo', [app()->getLocale(),$sura_link,$page_link]);

    }
}
