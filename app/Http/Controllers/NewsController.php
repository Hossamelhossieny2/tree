<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;
use App\Models\ManSay;
use App\Models\CountryEvent;
use jcobhams\NewsApi\NewsApi;
use App\Models\User;
use Carbon\Carbon;
use DB;

class NewsController extends Controller
{
    
    public ?User $user;
    public $events ;
    public $news;

    public function index()
    {

        $this->user = auth()->user();

        $country_ev = CountryEvent::orderBy('start_date','asc')->where('start_date','>',date('Y-m-d'))->where('country_id',auth()->user()->country_id)->get();
        //$birth_ev = User::orderBy('birth_date','asc')->where('family_id',auth()->user()->family_id)->orWhere('family_2',auth()->user()->family_id)->whereMonth('birth_date','>=',date('m'))->whereDay('birth_date','>=',date('d'))->whereNull('death_date')->with('country')->get();
        //$death_ev = User::whereNotNull('death_date')->orderBy('death_date','asc')->where('family_id',auth()->user()->family_id)->orWhere('family_2',auth()->user()->family_id)->where('death_date','!=','2100-01-01')->whereMonth('death_date','>=',date('m'))->whereDay('death_date','>=',date('d'))->with('country')->get();
        $man_says = ManSay::where('lang',app()->getLocale())->get();
        $man_says = $man_says->shuffle();
            // 'can use also https://github.com/jcobhams/newsapi-php'
        

        $check_news = News::where('country',$this->user->country->iso)->orderBy('id','desc')->limit(20)->get();
        if(count($check_news)>0){
            foreach($check_news as $ev){
                        
                    $news[] = [
                        'title' => $ev->title,
                        'desc' => $ev->desc,
                        'country' => 'news',
                        'image' => $ev->image,
                        'start_date' => $ev->start_date ,
                        'created_at' => $ev->created_at ,
                        'link'  =>$ev->link,
                        'source'  =>$ev->source,
                        'site' => $ev->site,
                        'lang' => $ev->lang,
                        'tag' => $ev->tag,
                        'color' => '#37A9FF'
                    ];
                }
        }else{
            
                $api_link = @file_get_contents("https://newsapi.org/v2/top-headlines?country=".auth()->user()->country->iso."&apiKey=c443e5659be5405a92a73aa691a35c3c");
                
                $decode_news = json_decode($api_link);
                
                $all_news = $decode_news->articles;
                
                foreach($all_news as $ev){
                    // dd($ev);
                    $news_feed = new News;
                    $news_feed->title = $ev->title;
                    $news_feed->desc = $ev->description;
                    $news_feed->country = auth()->user()->country->iso;
                    $news_feed->image = $ev->urlToImage;
                    $news_feed->start_date = $ev->publishedAt;
                    $news_feed->link = $ev->url;
                    $news_feed->site = 'public';
                    $news_feed->lang = 'ar';
                    $news_feed->tag = 'Top-Heading-News';
                    $news_feed->source = $ev->source->name;
                    $news_feed->save();
                    $news[] = $news_feed;
            }
        
        }
                    
            
            
            $tit = 'title_'.app()->getLocale();
            foreach($country_ev as $ev){
                $event[] = [
                    'title' => $ev->$tit,
                    'desc' => __('country_vac'),
                    'country' => $ev->country_iso,
                    'image' => $ev->image,
                    'start_date' => $ev->start_date,
                    'created_at' => $ev->created_at->format('Y-m-d'),
                    'link'  =>'#',
                    'site' => 'private',
                    'lang' => 'ar',
                    'tag' => __('country_vac'),
                    'color' => 'lightred'
                ];
                

            }
            // foreach($birth_ev as $ev){
            //     $event[] = [
            //         'title' => $ev->name,
            //         'desc' => __('birth_anv').' '.$ev->name,
            //         'country' => $ev->country->iso,
            //         'image' => 'img/default/event8.png',
            //         'start_date' => \Carbon\Carbon::parse($ev->birth_date)->year(now()->format('Y'))->format('Y-m-d') ,
            //         'created_at' => $ev->created_at->format('Y-m-d'),
            //         'link'  =>'x',
            //         'site' => 'private',
            //         'lang' => 'ar',
            //         'tag' => __('birth_anv'),
            //         'color' => 'green'
            //     ];
            // }
            // foreach($death_ev as $ev){
            //     $event[] = [
            //         'title' => $ev->name,
            //         'desc' => __('death_anv').' '.$ev->name,
            //         'country' => $ev->country->iso,
            //         'image' => 'img/default/event12.png',
            //         'start_date' => \Carbon\Carbon::parse($ev->death_date)->year(now()->format('Y'))->format('Y-m-d') ,
            //         'created_at' => $ev->created_at->format('Y-m-d'),
            //         'link'  =>'x',
            //         'site' => 'private',
            //         'lang' => 'ar',
            //         'tag' => __('death_anv'),
            //         'color' => 'gray'
            //     ];
            // }


            foreach($man_says as $ev){
                $event[] = [
                    'title' => $ev->name,
                    'desc' => $ev->title,
                    'country' => '',
                    'image' => $ev->pic,
                    'start_date' => \Carbon\Carbon::now()->format('Y-m-d') ,
                    'created_at' => $ev->created_at->format('Y-m-d'),
                    'link'  =>'#',
                    'site' => 'private',
                    'lang' => 'ar',
                    'tag' => __('words'),
                    'color' => $ev->color
                ];
            }
            
            

        shuffle($event);
        
        $arr['events'] = $event;
        $arr['news'] = $news;
        //dd($this->events);

        return view('news',$arr);
    }

}
