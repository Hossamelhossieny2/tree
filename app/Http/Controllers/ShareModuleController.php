<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;

use App\Models\FamilyShare;
use App\Models\User;
use App\Models\ShareTypes;
use App\Models\ShareUser;
use App\Models\ShareUserPayment;
use App\Models\Notification;
use App\Models\FaqsHelp;
use App\Models\UserHelp;
use Illuminate\Http\Request;
use App\Models\HelpVedio;
use Carbon\Carbon;
use App\Models\Post;
use DB;

use App\Notifications\OffersNotification;

class ShareModuleController extends Controller
{
    public $ids ;

    public function index($lang,$id,$share)
    {

        $user = auth()->user();
        $arr['shares_titles'] = ShareTypes::where('lang',$lang)->get();

        if($share == 0){
            if($id == 0){
                $arr['shares']= FamilyShare::whereIn('user_id',session('ids')[0])->with(['user','user_share.user:id,name,gender,profile_photo_path','desc'])->get();
            }else{
                $arr['shares']= FamilyShare::whereIn('user_id',session('ids')[0])->where('type',$id)->with(['user','user_share.user:id,name,gender,profile_photo_path','desc'])->get();
            }
        }else{
            $arr['shares']= [];
            $arr['one_share'] = FamilyShare::where('id',$share)->whereIn('user_id',session('ids')[0])->with(['user','user_share.user:id,name,gender,profile_photo_path','desc'])->first();
        }
        
        $arr['section'] = $id;

        if(substr($id,0,1) =='x'){
            $user->unreadNotifications()->update(['read_at' => Carbon::now()]);
        }
        //dd($arr['shares']);

        $arr['FaqsHelp'] = FaqsHelp::where('topic_id',5)->where('lang',app()->getLocale())->get();
        $arr['needHelp'] = UserHelp::where('topic_id',5)->where('user_id',auth()->id())->first();
        $arr['VidHelp'] = HelpVedio::where('sub',4)->where('lang',app()->getLocale())->get();

        return view('shares.user_shares',$arr);
    }

    public function user_shares_select(Request $request)
    {

        $select_part = ShareUser::find($request['part']);
        $select_part->user_id = auth()->id();
        $select_part->save();

        $unit=FamilyShare::find($select_part->share_id);
       
        $name = 'piece_name_'.app()->getLocale();
        $div = '<div class="icons-block text-gray h3">
                        .'. $select_part->$name .' ('.arabic_w2e($select_part->piece_id).')<br><a class="btn btn-lg">'.$select_part->share_pay_time.'</a>
                    </div>';

        if(auth()->user()->profile_photo_path){
            $div .= '<img loading="lazy" src="'.asset(auth()->user()->profile_photo_path) .'" alt="author" width="92" height="92"><br/>';
        }else{
            $div .= '<img loading="lazy" src="'.asset('img\default\user_'.auth()->user()->gender.'.png') .'" alt="author" width="92" height="92" style="background-color: gray;"><br/>';
        }
                       
        $div .= '<button class="btn btn-md-2 btn-border-think btn-transparent c-grey" style="font-weight: 400;font-size: 20px;">'. auth()->user()->name .'<div class="ripple-container"></div></button>' ;

        // $unit find ShareUser from id user_share table
        // $select_part find FamilyShare from id share table
            $payDate = Carbon::createFromFormat('Y-m-d', $unit->start_pay_time);;
            $next_unit = 'add'.$unit->unit;
        
        for($i=0;$i<$unit->period;$i++){

            $userPayment            = new ShareUserPayment;
            $userPayment->user_id   = auth()->id();
            $userPayment->share_id  = $unit->id;
            $userPayment->share_type = $unit->type;
            $userPayment->piece_id  = $select_part->piece_id;
            if($userPayment->share_type != 2 && $userPayment->share_type != 4 && $userPayment->share_type != 7){
                $userPayment->amount    = ceil($unit->piece/$unit->period);
            }elseif($userPayment->share_type == 2 || $userPayment->share_type == 4 || $userPayment->share_type == 7){
                $userPayment->amount    = $unit->piece;
            }
            
            $userPayment->pay_time  = $payDate;
            $userPayment->period    = $i+1;
            $userPayment->confirm   = 0;
            $userPayment->save();

            //array_push($this->ids , $userPayment->id);
            $this->ids .= $userPayment->id.',';
            $payDate->$next_unit();
        }
        $ids = str_replace('"','',$this->ids);
        $addToShare = ShareUser::find($select_part->id);
        $addToShare->share_ids = $ids;
        $addToShare->save();

        $one_user = User::find($select_part->share_user);

        $offerData = [
                    'from'      => auth()->id(),
                    'to'        => $select_part->share_user,
                    'title'     => auth()->user()->name.__('has_joined'),
                    'desc'      => $select_part->share_desc,
                    'share_id'  => $select_part->share_id,
                ];
        $one_user->notify(new OffersNotification($offerData));

        //return $unit;
        return $div;
    }

    public function user_shares_done(Request $request)
    {
        $select_part = ShareUserPayment::find($request['part']);
        //return $select_part;
        if($select_part->share_type == 3){
            $select_part->confirm   = 1; 
        }
        $select_part->pay_status = 1;
        $select_part->save();
        
        $one_user = FamilyShare::find($select_part->share_id);
        
        $notfy_user = User::find($one_user->user_id);
       $offerData = [
                    'from'      => auth()->id(),
                    'to'        => $notfy_user->id,
                    'title'     => __('waiting_payment_confirmation'),
                    'desc'      => __('pay_amount_of').$select_part->amount,
                    'share_id'  => $select_part->share_id,
                ];
        $notfy_user->notify(new OffersNotification($offerData));

        $div = '<button class="btn" style="background-color:black;">
                '. arabic_w2e($select_part->amount).' ( '. arabic_w2e($select_part->piece_id) .' )
                <a class="btn btn-sm">'.$select_part->pay_time.'</a>
                <div><img src="'.asset("img/default/inprogress.png").'"></div>
                </button>';

        return $div;
    }

    public function user_shares_confirm(Request $request)
    {
        $select_part = ShareUserPayment::find($request['part']);
        $select_part->confirm = 1;
        $select_part->save();
        
        $one_user = User::find($select_part->user_id);

          $addToUserPayment = ShareUser::find($select_part->piece_id);
         $addToUserPayment->is_payed = $addToUserPayment->is_payed + $select_part->amount;
         $addToUserPayment->save();

        $div = '<a class="btn btn-sm btn-success">paid</a>';

        $offerData = [
                    'from'      => auth()->id(),
                    'to'        => $one_user->id,
                    'title'     => __('confirm_payment'),
                    'desc'      => __('pay_amount_of').$select_part->amount,
                    'share_id'  => $select_part->share_id,
                ];
        $one_user->notify(new OffersNotification($offerData));

        return $div;
    }

    public function new($lang,$id,$share)
    {
        $user = auth()->user();
        $arr['shares_titles'] = ShareTypes::where('lang',$lang)->get();
        $multiFamilyCallback = function($query) use($user) {
                            $query->where('family_id',$user->family_id)
                            ->orWhere('family_2',$user->family_id)
                            ->orWhere('family_m',$user->family_id);
                        };
        if($share == 0){
            if($id == 0){
               $arr['shares']= FamilyShare::query()
                    ->where($multiFamilyCallback)
                    ->with(['user','user_share.user:name,profile_photo_path','desc'])
                    ->get();
            }else{
                $arr['shares']= $arr['shares']= FamilyShare::query()
                    ->where('type',$id)
                    ->where($multiFamilyCallback)
                    ->with(['user','user_share.user:name,profile_photo_path','desc'])
                    ->get();
            }
        }else{
            $arr['shares']= [];
            $arr['one_share'] = FamilyShare::query()
                    ->where('id',$share)
                    ->where($multiFamilyCallback)
                    ->with(['user','user_share.user:name,profile_photo_path','desc'])
                    ->first();
        }
        $arr['section'] = $id;

        //dd($arr['one_share']);
        return view('user_shares',$arr);
    }

    public function my_all_shares($lang,$partId)
    {
        $arr['share_types'] = ShareTypes::where('lang',app()->getLocale())->get();

        $shares =FamilyShare::where('user_id',auth()->id())->with(['user:id,name,gender,profile_photo_path','desc'])->get();

        $arr['my_shares'] = $shares->map(function (FamilyShare $familyShare) {
            return [
                'share' => $familyShare,
                'user_piece' => collect(collect()->pad(
                    $familyShare->period,
                    ['period' => $familyShare->unit])
                )->mapWithKeys(fn($item,$key) => [
                    __($familyShare->unit)." ".arabic_w2e($key + 1) => $familyShare->user_share->map(fn(ShareUser $shareUser) => [
                        'user' => $shareUser->user ?? 'no name',
                        'paid' => $shareUser->user ? $shareUser->pays()->where('period', $key + 1)->first() : 'not paid',
                    ])
                ]),
            ];
        });

        //$arr['my_shares']=$shares;
        return view('shares.my_all_shares',$arr);
    }

    public function my_choice_shares()
    {
        
         $arr['shares'] = ShareUser::where('user_id',auth()->id())->with(['share_user_id:id,name,gender,profile_photo_path','share'])->groupBy('share_id')->get();

        $arr['my_shares'] = auth()->user()->user_shares->mapToGroups(function(ShareUserPayment $shareUserPayment,$key) {
           return [$shareUserPayment['share_id'] => $shareUserPayment];
        });

        return view('shares.my_choice_shares',$arr);
    }

    public function add_share_from_user(Request $request)
    {
        if(!isset(auth()->user()->family_id))
        return view("error",['error'=>'101','desc'=>__('add_father_first')]);
        
        $this->validate($request, [
            'piece_name'    => 'required',
            'share_type'    => 'required',
            'post_privacy'  => 'required',
            'share_period'  => 'required|numeric',
            'share_total'   => 'required|numeric',
            'share_unit'    => 'required',
            'start_date'    => 'required|date',
        ]);

        if($request['share_type'] != 4 || $request['share_type'] != 5 || $request['share_type'] != 6){
            $one_piece = intval($request['share_total']/$request['share_count']);
        }else{
            $one_piece = intval($request['share_total']/$request['share_count']/$request['share_period']);
        }
        

            $Family_share               = new FamilyShare;
            $Family_share->user_id      = auth()->id();
            $Family_share->family_id    = auth()->user()->family_id;
            if(!empty(auth()->user()->family_2)){
                   $Family_share->family_2 = auth()->user()->family_2; 
                }
            if(!empty(auth()->user()->family_m)){
               $Family_share->family_m = auth()->user()->family_m; 
            }
            $Family_share->type         = $request['share_type'];
            $Family_share->privacy      = $request['post_privacy'];
            $Family_share->period       = $request['share_period'];
            $Family_share->unit         = $request['share_unit'];
            $Family_share->piece        = $one_piece;
            $Family_share->total        = $request['share_total'];
            $Family_share->member_count = $request['share_count'];
            $Family_share->piece_name   = $request['piece_name'];
            $Family_share->start_pay_time = $request['start_date'];
            $Family_share->save();

        $total_w = $request['share_count'] * $request['share_piece'];
        $total_p = $request['share_period'] * $request['share_piece'];

        $date = Carbon::createFromFormat('Y-m-d', $request['start_date']);

        if($request['share_type'] == 1){
            // sacrfice
            
            for($i=0;$i<$request['share_count'];$i++){
                $ShareType              = ShareTypes::find($request['share_type']);

                $ShareFam               = new ShareUser;
                $ShareFam->family_id    = auth()->user()->family_id;
                if(!empty(auth()->user()->family_2)){
                   $ShareFam->family_2  = auth()->user()->family_2; 
                }
                $ShareFam->share_id     = $Family_share->id;
                $ShareFam->share_type   = $ShareType->id;
                $ShareFam->share_user   = auth()->id();
                $ShareFam->piece_id     = $i+1;
                $ShareFam->piece_name_ar = 'جزء';
                $ShareFam->piece_name_en = 'part';
                $ShareFam->share_type   = $request['share_type'];
                $ShareFam->share_title  = $request['piece_name'];
                $ShareFam->share_desc = arabic_w2e(number_format($Family_share->total)).__('divided_on').arabic_w2e($Family_share->member_count).__('piece_cost').' ( '.arabic_w2e($Family_share->piece).' ÷ '.arabic_w2e($Family_share->period).' '.__($Family_share->unit).' = '.arabic_w2e(intval($Family_share->piece/$Family_share->period)).' '.__('as_total').'/'.__($Family_share->unit).' )' ;
                $ShareFam->share_image = $ShareType->image;
                $ShareFam->share_pay_time = $date;
                $ShareFam->save();

                $next_unit = 'add'.$request['share_unit'];
                $date->$next_unit();
            }

        }elseif($request['share_type'] == 2){

            // fellow
                for($i=0;$i<$request['share_count'];$i++){
                $ShareType              = ShareTypes::find($request['share_type']);

                $ShareFam               = new ShareUser;
                $ShareFam->family_id    = auth()->user()->family_id;
                if(!empty(auth()->user()->family_2)){
                   $ShareFam->family_2  = auth()->user()->family_2; 
                }
                $ShareFam->share_id     = $Family_share->id;
                $ShareFam->share_user   = auth()->id();
                $ShareFam->piece_id     = $i+1;
                $ShareFam->piece_name_ar = 'دور';
                $ShareFam->piece_name_en = 'Role';
                $ShareFam->share_type   = $request['share_type'];
                $ShareFam->share_title  = $request['piece_name'];
                $ShareFam->share_desc = arabic_w2e(number_format($Family_share->total)).__('divided_on').arabic_w2e($Family_share->member_count).__('piece_cost').' ( '.arabic_w2e(number_format(ceil($Family_share->piece))).' x '.arabic_w2e($Family_share->member_count).' '.__($Family_share->unit).' )' ;
                $ShareFam->share_image = $ShareType->image;
                $ShareFam->share_pay_time = $date;
                $ShareFam->save();

                $next_unit = 'add'.$request['share_unit'];
                $date->$next_unit();
            }
            

        }elseif($request['share_type'] == 3){

            // quran
            for($i=0;$i<$request['share_count'];$i++){
                $ShareType = ShareTypes::find($request['share_type']);

                $ShareFam               = new ShareUser;
                $ShareFam->family_id    = auth()->user()->family_id;
                if(!empty(auth()->user()->family_2)){
                   $ShareFam->family_2  = auth()->user()->family_2; 
                }
                $ShareFam->share_id     = $Family_share->id;
                $ShareFam->share_user   = auth()->id();
                $ShareFam->share_type   = $request['share_type'];
                $ShareFam->share_title  = $request['piece_name'];
                $ShareFam->piece_id     = $i+1;
                $ShareFam->piece_name_ar = 'جزء';
                $ShareFam->piece_name_en = 'Portion';
                $ShareFam->share_desc=arabic_w2e($Family_share->member_count).__('piece_devide').' '.arabic_w2e($Family_share->period).' '.__($Family_share->unit).' )';
                $ShareFam->share_image = $ShareType->image;
                $ShareFam->share_pay_time = $date;
                $ShareFam->save();

                $next_unit = 'add'.$request['share_unit'];
                $date->$next_unit();
            }
            

        }elseif($request['share_type'] == 4){

            // help
             for($i=0;$i<$request['share_count'];$i++){
                $ShareType              = ShareTypes::find($request['share_type']);

                $ShareFam               = new ShareUser;
                $ShareFam->family_id    = auth()->user()->family_id;
                if(!empty(auth()->user()->family_2)){
                   $ShareFam->family_2  = auth()->user()->family_2; 
                }
                $ShareFam->share_id     = $Family_share->id;
                $ShareFam->share_user   = auth()->id();
                $ShareFam->piece_id     = $i+1;
                $ShareFam->piece_name_ar = 'مساهمة';
                $ShareFam->piece_name_en = 'Share';
                $ShareFam->share_type   = $request['share_type'];
                $ShareFam->share_title  = $request['piece_name'];
                $ShareFam->share_desc   = __('share_val').' '.arabic_w2e($Family_share->piece).' ( '.arabic_w2e($Family_share->member_count).__('share_devide').' X '.arabic_w2e($Family_share->period).' '.__($Family_share->unit).' ) ';
                $ShareFam->share_image = $ShareType->image;
                $ShareFam->share_pay_time = $date;
                $ShareFam->save();

                $next_unit = 'add'.$request['share_unit'];
                $date->$next_unit();
            }
        }elseif($request['share_type'] == 5){

            // box
             for($i=0;$i<$request['share_count'];$i++){
                $ShareType              = ShareTypes::find($request['share_type']);

                $ShareFam               = new ShareUser;
                $ShareFam->family_id    = auth()->user()->family_id;
                if(!empty(auth()->user()->family_2)){
                   $ShareFam->family_2  = auth()->user()->family_2; 
                }
                $ShareFam->share_id     = $Family_share->id;
                $ShareFam->share_user   = auth()->id();
                $ShareFam->piece_id     = $i+1;
                $ShareFam->piece_name_ar = 'مساهمة';
                $ShareFam->piece_name_en = 'Share';
                $ShareFam->share_type   = $request['share_type'];
                $ShareFam->share_title  = $request['piece_name'];
                $ShareFam->share_desc = __('share_val').' '.arabic_w2e($Family_share->piece).' ( '.__('divided_on').' '.arabic_w2e($Family_share->period).' '.__($Family_share->unit).' - '.__('as_one').arabic_w2e(ceil($one_piece/$Family_share->period)).' ) ';
                $ShareFam->share_image = $ShareType->image;
                $ShareFam->share_pay_time = $date;
                $ShareFam->save();

                $next_unit = 'add'.$request['share_unit'];
                $date->$next_unit();
            }
        }elseif($request['share_type'] == 6){

            // charity
             for($i=0;$i<$request['share_count'];$i++){
                $ShareType              = ShareTypes::find($request['share_type']);

                $ShareFam               = new ShareUser;
                $ShareFam->family_id    = auth()->user()->family_id;
                if(!empty(auth()->user()->family_2)){
                   $ShareFam->family_2  = auth()->user()->family_2; 
                }
                $ShareFam->share_id     = $Family_share->id;
                $ShareFam->share_user   = auth()->id();
                $ShareFam->piece_id     = $i+1;
                $ShareFam->piece_name_ar = 'صدقة';
                $ShareFam->piece_name_en = 'charity';
                $ShareFam->share_type   = $request['share_type'];
                $ShareFam->share_title  = $request['piece_name'];
                $ShareFam->share_desc = __('share_val').' '.arabic_w2e($Family_share->piece).' ( '.__('divided_on').' '.arabic_w2e($Family_share->period).' '.__($Family_share->unit).' - '.__('as_one').arabic_w2e(ceil($one_piece/$Family_share->period)).' ) ';
                $ShareFam->share_image = $ShareType->image;
                $ShareFam->share_pay_time = $date;
                $ShareFam->save();

                $next_unit = 'add'.$request['share_unit'];
                $date->$next_unit();
            }
        }elseif($request['share_type'] == 7){

            // charity
             for($i=0;$i<$request['share_count'];$i++){
                $ShareType              = ShareTypes::find($request['share_type']);

                $ShareFam               = new ShareUser;
                $ShareFam->family_id    = auth()->user()->family_id;
                if(!empty(auth()->user()->family_2)){
                   $ShareFam->family_2  = auth()->user()->family_2; 
                }
                $ShareFam->share_id     = $Family_share->id;
                $ShareFam->share_user   = auth()->id();
                $ShareFam->piece_id     = $i+1;
                $ShareFam->piece_name_ar = 'كفالة';
                $ShareFam->piece_name_en = 'bail';
                $ShareFam->share_type   = $request['share_type'];
                $ShareFam->share_title  = $request['piece_name'];
                $ShareFam->share_desc = arabic_w2e(number_format($Family_share->total)).' '.__('every_month').' '.__('collected_from').arabic_w2e($Family_share->member_count).__('piece_cost').' ( '.arabic_w2e(number_format(ceil($Family_share->piece))).' '.__('for').' '.arabic_w2e($Family_share->period).' '.__($Family_share->unit).' )' ;
                $ShareFam->share_image = $ShareType->image;
                $ShareFam->share_pay_time = $date;
                $ShareFam->save();

                $next_unit = 'add'.$request['share_unit'];
                $date->$next_unit();
            }
        }

        if($request['post_privacy'] == 'public'){
            $post = new Post;
            $post->user_id = auth()->user()->id;
            $post->family_id = auth()->user()->family_id;
            if(auth()->user()->family_2 != null)
            $post->family_2 = auth()->user()->family_2;
            if(auth()->user()->family_m != null)
            $post->family_m = auth()->user()->family_m;
            $post->post = __('ask_join').' ('.$ShareFam->share_title.') '.$ShareFam->share_desc;
            $post->type = 'public';
            $post->post_pic = $ShareFam->share_image;
            $post->save();
        }
        

        $user = auth()->user();
        $all_my_fam_user = User::where(function ($query) use($user) {
                        $query->where('family_id',$user->family_id)
                              ->orWhere('family_id',$user->family_2)
                              ->orWhere('family_2',$user->family_id)
                              ->orWhere('family_2',$user->family_2);
                    })->get();

        foreach($all_my_fam_user as $one_user){
            if($one_user->id != $user->id){
                $offerData = [
                        'from'      => auth()->id(),
                        'to'        => $one_user->id,
                        'title'     => $ShareType->title,
                        'desc'      => $ShareFam->share_desc,
                        'share_id'  => $Family_share->id,
                    ];
            $one_user->notify(new OffersNotification($offerData));
            }
        }
        

        return back()->with('success',__('shared_event_added'));
    }

    public function start_this_share($lang,$share_id)
    {

        $my_shar = FamilyShare::find($share_id);
        $my_shar->is_started = 1;
        $my_shar->save();
        //dd($my_shar);
        return back()->with('success',__('shared_event_started'));
    }

    public function remove_this_share($share_id)
    {
        
    }

    public function not_view()
    {
        $arr['all_not'] = Notification::where('notifiable_id',auth()->id())->orderBy('created_at','desc')->paginate('10');
        return view('noti_view',$arr);
    }

}
