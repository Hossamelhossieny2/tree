<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use DB;

class MotherFamilyPrintController extends Controller
{
    public $tree = [];
    public $thisgrand = [];
    //index
    public function familyTree(){ 

        $mother = DB::table('users')->where('id',auth()->id())->select('mother_id as id')->first();
        $id = DB::table('users')->where('id',$mother->id)->select('family_id as id','country_id as country')->first();
        
        if(!empty($id))
        {
            $grand = DB::table("users")->where("family_id",$id->id)->where("father_id",NULL)->where("mother_id",NULL)->leftJoin('countries','users.country_id','countries.id')->select('users.gender','users.name','users.id','users.profile_photo_path','users.death_date','countries.iso','countries.phonecode')->first();
            $this->marriges($grand);
            $arr['person'] = $grand;
        }else{
            return view("error",['error'=>'101','desc'=> __('add_mother_first')]);
        }
      
        $arr['tree'] = $this->tree;
        $arr['tree_name'] = strtoupper($grand->name).' '.__('fam_nam_gm');
        if(!empty($this->tree )){
            return view("treeprint",$arr); 
        }else{
           return view("error",['error'=>'101','desc'=>__('add_mother_fam_first')]);
        }
        
    }

    public function marriges($person){  
    //dd($person);      
        if($person->gender == "male"){           
            $marriges = DB::table("user_wife")->leftJoin("users","users.id","=","wife_id")->leftJoin('countries','users.country_id','countries.id')->select("wife_id as marrige_id","users.name as marrige_name","users.gender","users.phone_number","users.profile_photo_path","users.death_date",'users.birth_date','countries.iso','countries.phonecode')->where("user_id",$person->id)->get();
            if(!empty($marriges) && count($marriges) > 0)
                $this->tree[$person->id]['marriges'] = $marriges;
        }else{
            $marriges = DB::table("user_wife")->leftJoin("users","users.id","=","user_id")->leftJoin('countries','users.country_id','countries.id')->select("user_id as marrige_id","users.name as marrige_name","users.gender","users.profile_photo_path","users.death_date",'users.birth_date',"users.phone_number",'countries.iso','countries.phonecode')->where("wife_id",$person->id)->get();
            if(!empty($marriges) && count($marriges) > 0)
                $this->tree[$person->id]['marriges'] = $marriges;
        }        
        if(!empty($marriges)){            
            foreach($marriges as $marrige){             
                if($person->gender == "male"){
                    $this->tree[$person->id]['children'][$marrige->marrige_id] = $children = DB::table("users")->leftJoin('countries','users.country_id','countries.id')->where("father_id",$person->id)->where("mother_id",$marrige->marrige_id)->select('users.gender','users.name',"users.phone_number",'users.id','users.profile_photo_path','users.death_date','users.birth_date','countries.iso','countries.phonecode')->get();
                    
                }else{
                    $this->tree[$person->id]['children'][$marrige->marrige_id] = $children = DB::table("users")->leftJoin('countries','users.country_id','countries.id')->where("mother_id",$person->id)->where("father_id",$marrige->marrige_id)->select('users.gender','users.name',"users.phone_number",'users.id','users.profile_photo_path','users.death_date','users.birth_date','countries.iso','countries.phonecode')->get();
                }
                if(!empty($children)){
                    foreach($children as $child){                    
                        $this->marriges($child);
                    }
                }
            }
            
        }
    }

    public function drawTree($person){  


        if(!empty($this->tree[$person->id]['marriges'])){
            echo "<ul>";
            foreach($this->tree[$person->id]['marriges'] as $marrige){
                if(!empty($marrige->marrige_name)){
                echo "<li>";
                echo $person->name." married to ".$marrige->marrige_name."<br /><br /><br />";

                if(!empty($this->tree[$person->id]['children'][$marrige->marrige_id])){
                    echo "<ul>";
                    foreach($this->tree[$person->id]['children'][$marrige->marrige_id] as $child){
                        if(!empty($child->name)){
                        echo "<li>";
                        echo $child->name."<br />";
                        $this->drawTree($child);
                        echo "</li>";
                    }
                }
                    echo "</ul>";
                }
                echo "</li>";
            }
            }
            echo "</ul>";
        }
    }
}
