<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Contactus;

class ContactusController extends Controller
{
    public function index()
    {
        return view('contactus');
    }

    public function send_msg(Request $request)
    {
        $this->validate($request, [
            'first_name'        => ['required', 'string', 'max:255','alpha_dash'],
            'last_name'         => ['required', 'string', 'max:255','alpha_dash'],
            'user_email'        => ['required','email'],
            'user_mobile'       => ['required'],
            'about'             => ['required'],
            'message'           => ['required'],
        ]);

        $user_msg = new Contactus;
        $user_msg->name =$request['first_name'].' '.$request['last_name'];
        $user_msg->email =$request['user_email'];
        $user_msg->mobile =$request['user_mobile'];
        $user_msg->about =$request['about'];
        $user_msg->message =$request['message'];
        $user_msg->save();

        return back()->with('success','Done .. our responsible will replay within(1-3)Days !');
    }
}
