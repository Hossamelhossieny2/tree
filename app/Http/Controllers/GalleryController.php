<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Gallery;
use App\Models\Post;
use Livewire\WithFileUploads;
use Carbon\Carbon;
use Image;
use DB;
use File;

class GalleryController extends Controller
{
    use WithFileUploads;

    public function index()
    {
        $gallerys = Gallery::all();
        return view('gallery.view', compact('gallerys'));
    }

    public function store(Request $request)
    {
        //dd($request['path']);
        $this->validate($request, [
            'title_name' => 'required',
            'content' => 'required',
            'tags' => 'required',
            'pic' => 'required|image|mimes:jpeg,png,jpg,gif,svg', // 2MB Max
        ]);
        $imageName = time().'.'.$request['pic']->extension();  
     //dd($imageName);
        //$request['pic']->storeAs('public/gallery', $imageName);
        //$request['pic']->file($imageName)->store('gallery', 'public');
        $this->imgResize($request['pic']);

        $input = $request->all();
        $input['user_id'] = auth()->user()->id;
        
        if(auth()->user()->family_id){
            $input['family_id'] = auth()->user()->family_id;
        }else{
            $input['family_id'] = auth()->user()->family_2;
        }
        $input['pic'] = $imageName;
        $tags = explode(",", $request->tags);
        //dd($input);
        $gallery = Gallery::create($input);
        $gallery->tag($tags);

        // $destinationPath = public_path('/uploads/images');
        // if(!File::isDirectory($destinationPath)){
        //     File::makeDirectory($destinationPath);
        // }

        if($input['public'] == '1'){
            $post = new Post;
            $post->user_id = auth()->user()->id;
            $post->family_id = $input['family_id'];
            $post->post = __('gall_added').$input['title_name'] .'( '.$input['content'].' ) '.__('check_it');
            $post->type = 1;
            $post->post_pic = 'uploads/gallery_uploads/'.$input['pic'];
            $post->save();

        }

        //return back()->with('success','Post added to database.');

        return redirect()->route('my_photos',app()->getLocale());
    }

    public function imgResize($file)
    {
        
  
        $image = $file;
        $input['imagename'] = time().'.'.$image->extension();
     
        $filePath = public_path('/uploads/thumbnails');
        if(!File::isDirectory($filePath)){
            File::makeDirectory($filePath, 0777, true, true);
        }
        $img = Image::make($image->path());
        $img->resize(332, 283, function ($const) {
            $const->aspectRatio();
        })->save($filePath.'/'.$input['imagename']);
   
        $filePath = public_path('/uploads/gallery_uploads');
        if(!File::isDirectory($filePath)){
            File::makeDirectory($filePath, 0777, true, true);
        }
        $img2 = Image::make($image->path());
        $img2->resize(700, 372, function ($const) {
            $const->aspectRatio();
        })->save($filePath.'/'.$input['imagename']);
   
        return back()
            ->with('success','Image uploaded')
            ->with('fileName',$input['imagename']);
    }

    protected function profilePhotoDisk()
    {
        return isset($_ENV['VAPOR_ARTIFACT_NAME']) ? 's3' : config('jetstream.profile_photo_disk', 'public');
    }

}
