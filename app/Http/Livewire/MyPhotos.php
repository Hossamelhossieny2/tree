<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\Gallery;
use App\Models\HelpVedio;

class MyPhotos extends Component
{
    use WithFileUploads;

    public $famPic;
    public $title_name;
    public $content;
    public $tags;

    public $userGal,$VidHelp;

    public function mount()
    {
        $this->VidHelp = HelpVedio::where('sub',5)->where('lang',app()->getLocale())->get();
    }

    public function imageUserStore()
    {
        //dd($request['path']);
        $this->validate([
            'title_name' => 'required',
            'content' => 'required',
            'tags' => 'required',
            'famPic' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048', // 2MB Max
        ]);
        $imageName = time().'.'.$this->famPic->extension(); 
        $tags = explode(",", $this->tags);
     //dd($imageName);
        $this->famPic->storeAs('gallery', $imageName);
        
        $gallery = new Gallery;
        $gallery->title_name = $this->title_name;
        $gallery->famPic = $imageName;
        $gallery->tags = $this->tags;
        $gallery->content = $this->content;
        $gallery->save();
        
        $gallery->tag($this->tags);

        
        //session()->flash('success',"Your Pic Added Successfully");
        //return back()->with('success','Post added to database.');
    }

    public function render()
    {
        $this->userGal = Gallery::where('user_id',auth()->user()->id)->get();
        
        return view('livewire.my-photos');
    }

}
