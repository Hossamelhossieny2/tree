<?php

namespace App\Http\Livewire;

use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;
use App\Models\User;
use App\Models\UserWife;
use App\Models\FamilyName;
use App\Models\Post;
use App\Models\Gallery;
use App\Models\Country;
use App\Models\WheelUser;
use App\Models\WheelType;
use App\Models\FaqsHelp;
use App\Models\UserHelp;
use App\Models\WheelUserMission;
use Carbon\Carbon;
use DB;
use App\Models\News;
use Session;

class FamilyStatistics extends Component
{
    public $user;
    public $family;

    public $maleCount,$femaleCount,$dead,$lived;
    public $fatherFamily,$motherFamily,$wifeFamily;
    public $oldestMale,$oldestFemale,$youngestMale,$youngestFemale;

    public $single,$married,$widowed,$divorced,$age,$treeMemberId;

    public $weathard,$familyS,$wheel,$user_map,$FaqsHelp,$needHelp;

    public $quarter = 0;
    public function mount(){
         $this->age = [
            '0' => 0 , '1' => 0 , '2' => 0 , '3' => 0 , '4' => 0 , '5' => 0 , '6' => 0 , '7' => 0 , '8' => 0 ,'9' => 0 ,'10' => 0 ,'11' => 0 , 'null' => 0 , 'total' => 0
        ];
        $this->user = auth()->user();
        //$this->treeMemberId = $this->user->family_id;
        $treeMemberId['fatherC'] = 0;
        $treeMemberId['motherC'] = 0;
        $treeMemberId['wifeC'] = 0;
        $treeMemberId['male'] = 0;
        $treeMemberId['female'] = 0;
        $treeMemberId['live'] = 0;
        $treeMemberId['die'] = 0;
        $treeMemberId['married'] = 0;
        $treeMemberId['single'] = 0;
        $treeMemberId['widowed'] = 0;
        $treeMemberId['divorced'] = 0;
        $treeMemberId['biggestmale'] = 0;
        $treeMemberId['biggestfemale'] = 0;
        $treeMemberId['youngestmale'] = 0;
        $treeMemberId['youngestfemale'] = 0;
        $treeMemberId['ids'] = [];
        $user_map = [];

        $father_fam_ca = FamilyName::find(auth()->user()->family_id);
        $finids = unserialize($father_fam_ca->fam_cache);
        $treeMemberId['father'] =  User::where(function ($query) use($finids) {
                        $query->whereIn('id',$finids);
                    })->with('country')->get();

        if(isset(auth()->user()->family_2)){
            $mother_fam_ca = FamilyName::find(auth()->user()->family_2);
            $minids = unserialize($mother_fam_ca->fam_cache);
            $treeMemberId['mother'] =  User::where(function ($query) use($minids) {
                            $query->whereIn('id',$minids);
                        })->with('country')->get();
        }

        if(isset(auth()->user()->family_m)){
             $wife_fam_ca = FamilyName::find(auth()->user()->family_m);
            $winids = unserialize($wife_fam_ca->fam_cache);
            $treeMemberId['wife'] =  User::where(function ($query) use($winids) {
                            $query->whereIn('id',$winids);
                        })->with('country')->get();
        }
        
        foreach($treeMemberId['father'] as $mem){
            
            if(!isset($this->treeMemberId[$mem->id])){
                $treeMemberId['fatherC'] = $treeMemberId['fatherC'] + 1;
                array_push($treeMemberId['ids'], $mem->id);

                // maritale state
                if($mem->gender == 'male' && $mem->wives()->count() > 0 ){
                    if($mem->death_date == null){
                        if($mem->divorcedwives->count() > 0){
                            $treeMemberId['divorced'] = $treeMemberId['divorced'] + 1;
                        }else{
                            $treeMemberId['married'] = $treeMemberId['married'] + 1;
                        }
                    }else{
                        $treeMemberId['widowed'] = $treeMemberId['widowed'] + 1;
                    }
                }else{
                    $treeMemberId['single'] = $treeMemberId['single'] + 1;
                }

                // male VS female
                if($mem->gender == 'male') $treeMemberId['male'] = $treeMemberId['male'] + 1;
                if($mem->gender == 'female') $treeMemberId['female'] = $treeMemberId['female'] + 1;
                
                // live VS die
                if($mem->death_date == null){
                    $treeMemberId['live'] = $treeMemberId['live'] + 1;
                }else{
                    $treeMemberId['die'] = $treeMemberId['die'] + 1;
                }
                
                // age distribution
                if($mem->birth_date != null){
                    $ag = Carbon::now()->diffInYears($mem->birth_date);
                    $agg = intval($ag/10);
                    if($agg > 8)$agg=9;
                    $this->age[$agg] +=1;
                    $this->age['total'] +=1;
                }else{
                    $this->age['null'] +=1;
                }
                
                // country map
                if(isset($user_map[$mem['country']['iso']])){
                    $user_map[$mem['country']['iso']] = $user_map[$mem['country']['iso']]+1;
                }else{
                    $user_map[$mem['country']['iso']] = 1;
                }
                
                // person stat
                if($mem->birth_date != null && $mem->death_date == null){
                    if($mem->gender == 'male'){
                        if(!empty($this->oldestMale)){
                            if($mem->birth_date->lt($this->oldestMale)){
                                $this->oldestMale = $mem->birth_date;
                                $treeMemberId['biggestmale'] = $mem;
        
                            }elseif($mem->birth_date->gt($this->youngestMale)){
                                $this->youngestMale = $mem->birth_date;
                                $treeMemberId['youngestmale'] = $mem;
                            }
                        }else{
                            $this->oldestMale = $mem->birth_date;
                            $treeMemberId['biggestmale'] = $mem;
                        }
                    }else{
                        if(!empty($this->oldestFemale)){
                            if($mem->birth_date->lt($this->oldestFemale)){
                                $this->oldestFemale = $mem->birth_date;
                                $treeMemberId['biggestfemale'] = $mem;
        
                            }elseif($mem->birth_date->gt($this->youngestFemale)){
                                $this->youngestFemale = $mem->birth_date;
                                $treeMemberId['youngestfemale'] = $mem;
                            }
                        }else{
                            $this->oldestFemale = $mem->birth_date;
                            $treeMemberId['biggestfemale'] = $mem;
                        }
                    }
                }
            }
            
            $this->treeMemberId[$mem->id] = $mem->id;
            
        }

        if(!empty(auth()->user()->family_2)){
            foreach($treeMemberId['mother'] as $mem){
            
                if(!isset($this->treeMemberId[$mem->id])){
                    $treeMemberId['motherC'] = $treeMemberId['motherC'] + 1;
                    array_push($treeMemberId['ids'], $mem->id);
                    // maritale state
                    if($mem->gender == 'male' && $mem->wives()->count() > 0 ){
                        if($mem->death_date == null){
                            if($mem->divorcedwives->count() > 0){
                                $treeMemberId['divorced'] = $treeMemberId['divorced'] + 1;
                            }else{
                                $treeMemberId['married'] = $treeMemberId['married'] + 1;
                            }
                        }else{
                            $treeMemberId['widowed'] = $treeMemberId['widowed'] + 1;
                        }
                    }else{
                        $treeMemberId['single'] = $treeMemberId['single'] + 1;
                    }

                    // male VS female
                    if($mem->gender == 'male') $treeMemberId['male'] = $treeMemberId['male'] + 1;
                    if($mem->gender == 'female') $treeMemberId['female'] = $treeMemberId['female'] + 1;
                    
                    // live VS die
                    if($mem->death_date == null){
                        $treeMemberId['live'] = $treeMemberId['live'] + 1;
                    }else{
                        $treeMemberId['die'] = $treeMemberId['die'] + 1;
                    }
                    
                    // age distribution
                    if($mem->birth_date != null){
                        $ag = Carbon::now()->diffInYears($mem->birth_date);
                        $agg = intval($ag/10);
                        if($agg > 8)$agg=9;
                        $this->age[$agg] +=1;
                        $this->age['total'] +=1;
                    }else{
                        $this->age['null'] +=1;
                    }
                    
                    // country map
                    if(isset($user_map[$mem['country']['iso']])){
                        $user_map[$mem['country']['iso']] = $user_map[$mem['country']['iso']]+1;
                    }else{
                        $user_map[$mem['country']['iso']] = 1;
                    }

                    // person stat
                    if($mem->birth_date != null && $mem->death_date == null){
                        if($mem->gender == 'male'){
                            if(!empty($this->oldestMale)){
                                if($mem->birth_date->lt($this->oldestMale)){
                                    $this->oldestMale = $mem->birth_date;
                                    $treeMemberId['biggestmale'] = $mem;
            
                                }elseif($mem->birth_date->gt($this->youngestMale)){
                                    $this->youngestMale = $mem->birth_date;
                                    $treeMemberId['youngestmale'] = $mem;
                                }
                            }else{
                                $this->oldestMale = $mem->birth_date;
                                $treeMemberId['biggestmale'] = $mem;
                            }
                        }else{
                            if(!empty($this->oldestFemale)){
                                if($mem->birth_date->lt($this->oldestFemale)){
                                    $this->oldestFemale = $mem->birth_date;
                                    $treeMemberId['biggestfemale'] = $mem;
            
                                }elseif($mem->birth_date->gt($this->youngestFemale)){
                                    $this->youngestFemale = $mem->birth_date;
                                    $treeMemberId['youngestfemale'] = $mem;
                                }
                            }else{
                                $this->oldestFemale = $mem->birth_date;
                                $treeMemberId['biggestfemale'] = $mem;
                            }
                        }
                    }
                }

                $this->treeMemberId[$mem->id] = $mem->id;
            }
        }
        
        if(!empty(auth()->user()->family_m)){
            foreach($treeMemberId['wife'] as $mem){
                
                if(!isset($this->treeMemberId[$mem->id])){
                    $treeMemberId['wifeC'] = $treeMemberId['wifeC'] + 1;
                    array_push($treeMemberId['ids'], $mem->id);
                    // maritale state
                    if($mem->gender == 'male' && $mem->wives()->count() > 0 ){
                        if($mem->death_date == null){
                            if($mem->divorcedwives->count() > 0){
                                $treeMemberId['divorced'] = $treeMemberId['divorced'] + 1;
                            }else{
                                $treeMemberId['married'] = $treeMemberId['married'] + 1;
                            }
                        }else{
                            $treeMemberId['widowed'] = $treeMemberId['widowed'] + 1;
                        }
                    }else{
                        $treeMemberId['single'] = $treeMemberId['single'] + 1;
                    }

                    // male VS female
                    if($mem->gender == 'male') $treeMemberId['male'] = $treeMemberId['male'] + 1;
                    if($mem->gender == 'female') $treeMemberId['female'] = $treeMemberId['female'] + 1;
                    
                    // live VS die
                    if($mem->death_date == null){
                        $treeMemberId['live'] = $treeMemberId['live'] + 1;
                    }else{
                        $treeMemberId['die'] = $treeMemberId['die'] + 1;
                    }
                    
                    // age distribution
                    if($mem->birth_date != null){
                        $ag = Carbon::now()->diffInYears($mem->birth_date);
                        $agg = intval($ag/10);
                        if($agg > 8)$agg=9;
                        $this->age[$agg] +=1;
                        $this->age['total'] +=1;
                    }else{
                        $this->age['null'] +=1;
                    }
                    
                    // country map
                    if(isset($user_map[$mem['country']['iso']])){
                        $user_map[$mem['country']['iso']] = $user_map[$mem['country']['iso']]+1;
                    }else{
                        $user_map[$mem['country']['iso']] = 1;
                    }
                    
                    // person stat
                    if($mem->birth_date != null && $mem->death_date == null){
                        if($mem->gender == 'male'){
                            if(!empty($this->oldestMale)){
                                if($mem->birth_date->lt($this->oldestMale)){
                                    $this->oldestMale = $mem->birth_date;
                                    $treeMemberId['biggestmale'] = $mem;
            
                                }elseif($mem->birth_date->gt($this->youngestMale)){
                                    $this->youngestMale = $mem->birth_date;
                                    $treeMemberId['youngestmale'] = $mem;
                                }
                            }else{
                                $this->oldestMale = $mem->birth_date;
                                $treeMemberId['biggestmale'] = $mem;
                            }
                        }else{
                            if(!empty($this->oldestFemale)){
                                if($mem->birth_date->lt($this->oldestFemale)){
                                    $this->oldestFemale = $mem->birth_date;
                                    $treeMemberId['biggestfemale'] = $mem;
            
                                }elseif($mem->birth_date->gt($this->youngestFemale)){
                                    $this->youngestFemale = $mem->birth_date;
                                    $treeMemberId['youngestfemale'] = $mem;
                                }
                            }else{
                                $this->oldestFemale = $mem->birth_date;
                                $treeMemberId['biggestfemale'] = $mem;
                            }
                        }
                    }
                }

                $this->treeMemberId[$mem->id] = $mem->id;
            }
        }

        $this->user_map = $user_map;
        $this->maleCount = $treeMemberId['male'];
        $this->femaleCount = $treeMemberId['female'];
        $this->dead = $treeMemberId['die'];
        $this->lived = $treeMemberId['live'];
        $this->married = $treeMemberId['married'];
        $this->single = $treeMemberId['single'];
        $this->divorced = $treeMemberId['divorced'];
        $this->widowed = $treeMemberId['widowed'];
        $this->oldestMale = $treeMemberId['biggestmale'];
        $this->oldestFemale = $treeMemberId['biggestfemale'];
        $this->youngestMale = $treeMemberId['youngestmale'];
        $this->youngestFemale = $treeMemberId['youngestfemale'];
        $this->fatherFamily = $treeMemberId['fatherC'];
        $this->motherFamily = $treeMemberId['motherC'];
        $this->wifeFamily = $treeMemberId['wifeC'];
        
        if(!session('ids')){
            Session::push('ids', $treeMemberId['ids']);
        }
        //dd($treeMemberId);

        $user_country = strtolower($this->user->country->iso);

        $check_news = News::whereDate('created_at',\Carbon\Carbon::today())->where('country',$user_country)->get();
            if(count($check_news)>0){
            }else{
                //dd(json_decode(@file_get_contents("https://newsapi.org/v2/top-headlines?country=Eg&apiKey=c443e5659be5405a92a73aa691a35c3c"))->articles);
                $news = json_decode(@file_get_contents("https://newsapi.org/v2/top-headlines?country=".auth()->user()->country->iso."&apiKey=c443e5659be5405a92a73aa691a35c3c"));
                if(!empty($news->articles))
                foreach($news->articles as $ev){
                    $news_feed = new News;
                    $news_feed->title = $ev->title;
                    $news_feed->desc = $ev->description;
                    $news_feed->country = auth()->user()->country->iso;
                    $news_feed->image = $ev->urlToImage;
                    $news_feed->start_date = $ev->publishedAt;
                    $news_feed->link = $ev->url;
                    $news_feed->site = 'public';
                    $news_feed->lang = 'ar';
                    $news_feed->tag = 'Top-Heading-News';
                    $news_feed->source = $ev->source->name;
                    $news_feed->save();
            }
        }

        $this->FaqsHelp = FaqsHelp::where('topic_id',2)->where('lang',app()->getLocale())->get();

        $this->needHelp = UserHelp::where('topic_id',2)->where('user_id',auth()->id())->first();

        $myinitw = WheelUser::where('user_id',auth()->id())->where('init','1')->first();
        if(!empty($myinitw)){
            $this->wheel['MyinitWheel'] = $myinitw;
        }else{
            $this->wheel['MyinitWheel'] = [];
        }
        
        $this->wheel['wheel_types'] = WheelType::with('ques')->where('lang',app()->getLocale())->get();

        $currentQuarter = 0;
        $test = WheelUserMission::select(DB::raw("distinct(end_date)"),"quarter")->where("user_id",auth()->id())->orderBy("end_date","asc")->get();
        $i = 0;
        foreach($test as $t){
            $start = Carbon::createFromFormat('Y-m-d',$t->end_date)->subMonth(3)->format('Y-m-d');
            $end = $t->end_date;
            if(Carbon::now() > $start && Carbon::now() < $end){
                $currentQuarter = $t->quarter;
                $this->quarter = $end;
                break;
            }

            $i++;
        }
        
        $this->wheel['myMision'] = WheelUserMission::with("quesWithType")->where("user_id",auth()->id())->where("quarter",$currentQuarter)->get();

       
        $this->familyS['gallery'] = Gallery::whereIn('user_id',session('ids')[0])->count();
        $this->familyS['post'] = Post::whereIn('user_id',session('ids')[0])->count();
        $this->familyS['post_without_pic'] = Post::whereIn('user_id',session('ids')[0])->whereNull('post_pic')->count();
        $this->familyS['post_with_pic'] = Post::whereIn('user_id',session('ids')[0])->where('post_pic','!=',NULL)->orWhere('post_vid','!=',NULL)->count();
        $this->familyS['post_event'] = Post::whereIn('user_id',session('ids')[0])->where('type','3')->count();
        $this->familyS['total'] = $this->familyS['gallery'] + $this->familyS['post_without_pic'] + $this->familyS['post_with_pic'] + $this->familyS['post_event'];
        
    }

    public function render()
    {

        $urlContents = @file_get_contents("http://api.weatherapi.com/v1/current.json?key=e860928c53f84d3184b135110212308&q=".auth()->user()->country->name."&aqi=no");
        if ($urlContents === FALSE){
           $this->weathard['location']['name'] = "Cairo";
           $this->weathard['location']['region'] = "Al Qahirah";
           $this->weathard['location']['country'] = "Egypt";
           $this->weathard['location']['tz_id'] = "Africa/Cairo";

           $this->weathard['current']['temp_c'] = "29.0";
           $this->weathard['current']['last_updated'] = "2021-08-28 01:00";
           $this->weathard['current']['condition']['icon'] = "//cdn.weatherapi.com/weather/64x64/night/113.png";
           $this->weathard['current']['condition']['text'] = "Clear";

           $this->weathard['current']['wind_mph'] = "5.6";
           $this->weathard['current']['humidity'] = "66";
           $this->weathard['current']['cloud'] = "0";
           
        }
        else {
            $jsonData = json_decode($urlContents, true);
            $this->weathard = $jsonData;
        }

        

        return view('livewire.family-statistics');
    }

    public function fatherFamilyTree(){ 

        $id = DB::table('users')->where('id',auth()->user()->id)->select('family_id as id')->first();
        
        $grand = DB::table("users")->where("family_id",$id->id)->where("father_id",NULL)->where("mother_id",NULL)->select('users.gender','users.name','users.id')->first();
        array_push($this->tree_ids,$grand->id);
        $this->marriges($grand);

        
    }

     public function marriges($person){  
    
        if($person->gender == "male"){           
            $marriges = DB::table("user_wife")->leftJoin("users","users.id","=","wife_id")->select("wife_id as marrige_id","users.gender")->where("user_id",$person->id)->get();
        }else{
            $marriges = DB::table("user_wife")->leftJoin("users","users.id","=","user_id")->select("user_id as marrige_id","users.gender")->where("wife_id",$person->id)->get();
        }        
        if(!empty($marriges)){            
            foreach($marriges as $marrige){ 
                array_push($this->tree_ids,$marrige->marrige_id);            
                if($person->gender == "male"){
                    $children = DB::table("users")->where("father_id",$person->id)->where("mother_id",$marrige->marrige_id)->select('users.gender','users.id')->get();
                    
                }else{
                    $children = DB::table("users")->where("mother_id",$person->id)->where("father_id",$marrige->marrige_id)->select('users.gender','users.id')->get();
                }
                if(!empty($children)){
                    foreach($children as $child){
                        array_push($this->tree_ids,$child->id);                     
                        $this->marriges($child);
                    }
                }
            }
            
        }
    }

    public function motherFamilyTree(){ 
        $mother = DB::table('users')->where('id',auth()->user()->mother_id)->first();
        $id = DB::table('users')->where('id',$mother->id)->select('family_id as id')->first();
        
        $grand = DB::table("users")->where("family_id",$id->id)->where("father_id",NULL)->where("mother_id",NULL)->select('users.gender','users.name','users.id')->first();
        array_push($this->tree2_ids,$grand->id);
        $this->marriges2($grand);

        
    }

     public function marriges2($person){  
    
        if($person->gender == "male"){           
            $marriges = DB::table("user_wife")->leftJoin("users","users.id","=","wife_id")->select("wife_id as marrige_id","users.gender")->where("user_id",$person->id)->get();
        }else{
            $marriges = DB::table("user_wife")->leftJoin("users","users.id","=","user_id")->select("user_id as marrige_id","users.gender")->where("wife_id",$person->id)->get();
        }        
        if(!empty($marriges)){            
            foreach($marriges as $marrige){ 
                array_push($this->tree2_ids,$marrige->marrige_id);            
                if($person->gender == "male"){
                    $children = DB::table("users")->where("father_id",$person->id)->where("mother_id",$marrige->marrige_id)->select('users.gender','users.id')->get();
                    
                }else{
                    $children = DB::table("users")->where("mother_id",$person->id)->where("father_id",$marrige->marrige_id)->select('users.gender','users.id')->get();
                }
                if(!empty($children)){
                    foreach($children as $child){
                        array_push($this->tree2_ids,$child->id);                     
                        $this->marriges2($child);
                    }
                }
            }
            
        }
    }


}
