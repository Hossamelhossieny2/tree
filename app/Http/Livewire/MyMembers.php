<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;
use App\Models\FollowUser;
use App\Models\FamilyName;
use App\Models\UserInfo;
use App\Models\UserEducation;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;
use DB;

class MyMembers extends Component
{
    public $FatherFam;
    public $MotherFam;
    public $WifeFam;
    public $FollowFam;


     public function Follow($user)
    {
        $newFollow = new FollowUser;
        $newFollow->user_id = auth()->id();
        $newFollow->follow = $user;
        $newFollow->save();

        return 'success';
    }

     public function unFollow($user)
    {
        $newFollow = FollowUser::where('user_id',auth()->id())->where('follow',$user)->first();
        
        $newFollow->delete();

        return 'success';
    }
    
    public function render()
    {
        $user = auth()->user();

        $father_fam_ca = FamilyName::find(auth()->user()->family_id);
        $finids = unserialize($father_fam_ca->fam_cache);
        $this->FatherFam =  User::where(function ($query) use($finids) {
                        $query->whereIn('id',$finids);
                    })->get();

        
        if(isset(auth()->user()->family_2)){
            $mother_fam_ca = FamilyName::find(auth()->user()->family_2);
            $minids = unserialize($mother_fam_ca->fam_cache);
            $this->MotherFam =  User::where(function ($query) use($minids) {
                            $query->whereIn('id',$minids);
                        })->get();
        }

        if(isset(auth()->user()->family_m)){
             $wife_fam_ca = FamilyName::find(auth()->user()->family_m);
            $winids = unserialize($wife_fam_ca->fam_cache);
            $this->WifeFam =  User::where(function ($query) use($winids) {
                            $query->whereIn('id',$winids);
                        })->get();
        }
        
        $this->FollowFam = FollowUser::where('user_id',auth()->id())->with('user')->get();

        return view('livewire.my-members');
    }


    
}
