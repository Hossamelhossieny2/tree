<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\UserInfo;
use App\Models\UserEducation;

class ProfileAbout extends Component
{
    public $infoId;
    public $bio;
    public $hobby;
    public $tv;
    public $movies;
    public $games;
    public $music;
    public $books;
    public $writer;
    public $other;
    public $birth_place;
    public $religion;
    public $politic;
    public $website;
    public $facebook;
    public $twitter;
    public $instagram;
    public $ticktok;
    public $youtube;
    public $profession;

    public $UserEdu;
    public $userInfo;

    public $Eplace;
    public $Estudy;
    public $Estart;
    public $Eend;

    public function mount()
    {
        $userInfo = userInfo::where('user_id',auth()->id())->first();
        $this->UserEdu = UserEducation::where('user_id',auth()->id())->get();

        if(!empty($userInfo))
        {
            $this->infoId = $userInfo->id;
            $this->hobby = $userInfo->hobby;
            $this->tv = $userInfo->tv;
            $this->movies = $userInfo->movies;
            $this->games = $userInfo->games;
            $this->music = $userInfo->music;
            $this->books = $userInfo->books;
            $this->writer = $userInfo->writer;
            $this->other = $userInfo->other;
            $this->bio = $userInfo->bio;
            $this->birth_place = $userInfo->birth_place;
            $this->lives_in = auth()->user()->country->nicename;
            $this->religion = $userInfo->religion;
            $this->politic = $userInfo->politic;
            $this->website = $userInfo->website;
            $this->facebook = $userInfo->facebook;
            $this->twitter = $userInfo->twitter;
            $this->instagram = $userInfo->instagram;
            $this->ticktok = $userInfo->ticktok;
            $this->youtube = $userInfo->youtube;
            $this->profession = $userInfo->profession;
        }
    }
    public function storeMyInfo()
    {
        if(!empty($this->userInfo)){
            $info = userInfo::find($this->userInfo->id);
        }else{
            $info = new userInfo;
        }
        
        $info->user_id = auth()->id();
        $info->hobby = $this->hobby;
        $info->tv = $this->tv;
        $info->movies = $this->movies;
        $info->games = $this->games;
        $info->music = $this->music;
        $info->books = $this->books;
        $info->writer = $this->writer;
        $info->other = $this->other;

        $info->bio = $this->bio;
        $info->birth_place = $this->birth_place;
        $info->religion = $this->religion;
        $info->politic = $this->politic;
        $info->website = $this->website;
        $info->facebook = $this->facebook;
        $info->twitter = $this->twitter;
        $info->instagram = $this->instagram;
        $info->ticktok = $this->ticktok;
        $info->youtube = $this->youtube;
        $info->profession = $this->profession;
        
        $info->save();  

        activity()
           ->withProperties(['profile_id' => $info->id,'change' => 'fill my profile' ])
            ->useLog('Profile Data')
           ->log('fill profile more of my data');
        
    }

     public function storeMyStudy($kind)
    {
        $this->validate([
            'Eplace' => ['required'],
            'Estudy' => ['required'],
            'Estart' => ['required'],
            'Eend' => ['required']
        ]);

        $study = new UserEducation;
        $study->user_id = auth()->id();
        $study->is_job = $kind;
        $study->place = $this->Eplace;
        $study->specialty = $this->Estudy;
        $study->start = $this->Estart;
        $study->end = $this->Eend;
        $study->save();

        $this->reset([ 'Eplace','Estudy','Estart','Eend']);

        if($kind == 0){
            $miss = 'Study';
        }else{
            $miss = 'Job';
        }
        activity()
           ->withProperties(['profile_id' => $this->userInfo->id,'change' => 'fill my profile' ])
            ->useLog('Profile Data')
           ->log('Add more of '.$miss.' details');

    }

    public function render()
    {
        $this->userInfo = userInfo::where('user_id',auth()->id())->first();
        $this->UserEdu = UserEducation::where('user_id',auth()->id())->get();
        return view('livewire.profile-about');
    }
}
