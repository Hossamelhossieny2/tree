<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\ActivityLog;

class MyTimeline extends Component
{
    public $actLogs;

    public function render()
    {
        $this->actLogs = ActivityLog::where('causer_id',auth()->id())->orderBy('created_at','desc')->get();

        return view('livewire.my-timeline');
    }
}
