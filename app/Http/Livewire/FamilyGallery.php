<?php

namespace App\Http\Livewire;

use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;
use App\Models\Gallery;
use App\Models\User;
use App\Models\FamilyName;
use Carbon\Carbon;
use Session;
use DB;

class FamilyGallery extends Component
{
    public $gallerys;

    // public function mount()
    // {
    //     $this->gallerys = Gallery::whereIn('user_id',session('ids')[0])->with('user_add')->orderBy('created_at','desc')->get();
    // }


    public function render()
    {
         return view('livewire.family-gallery', [
            'galleries' => Gallery::whereIn('user_id',session('ids')[0])->with('user_add')->orderBy('created_at','desc')->paginate(20),
        ]);
    }

   
}
