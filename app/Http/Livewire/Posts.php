<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Post;
use App\Models\Comment;
use App\Models\User;
use App\Models\UserWife;
use App\Models\FamilyName;
use Livewire\WithFileUploads;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Spatie\Activitylog\Contracts\Activity;
use Image;
use File;


class Posts extends Component
{
    use WithFileUploads;

    public $post;
    public ?User $user;
    public $postBody;
    public $commentBody;
    public $postType=1;
    public $postPic='null';
    public $allposts=[];
    public $horoScope;
    public $youtubeLink='';

    public function mount(User $user)
    {
        $this->user = auth()->user();
        //$this->refreshPosts();
        // $urlContents = @file_get_contents("http://horoscope-api.herokuapp.com/horoscope/today/".$this->user->zodiac->name());
         
        // if ($urlContents === FALSE){
        //     $this->horoScope['date'] = "2021-08-30";
        //     $this->horoScope['horoscope'] = "Call it your generous side, but today is the perfect day to give back to all those from whom you have received so far. With roots set firmly in community service, it might do you well to extend that helping hand and return something back to them, preferably in the double. Go along with the spirit of an early thanksgiving today. Ganesha reminds you that giving is always more fulfilling than receiving. May today be a fulfilling day for you! ";
        //     $this->horoScope['sunsign'] = "Libra";
        //     $this->horoScope['horoscope'] = "";
        // }else{
        //     $jsonData = json_decode($urlContents, true);
        //     $this->horoScope = $jsonData;
        // }
        $this->horoScope = "";

    }

     public function addPost()
    {
        $this->validate([
            'postBody' => ['required', 'string']
        ]);
        //if($this->youtubeLink == null && $this->postPic == null){
            $post = new Post;
            $post->user_id = auth()->user()->id;
            $post->family_id = auth()->user()->family_id;
            if(auth()->user()->family_2 != null)
            $post->family_2 = auth()->user()->family_2;
            if(auth()->user()->family_m != null)
            $post->family_m = auth()->user()->family_m;
            $post->post = $this->postBody;
            $post->type = $this->postType;
            $post->post_pic = $this->postPic;
            $post->save();
        //}
        
        
        //$this->refreshPosts();
        session()->flash('message',"Your Post Added Successfully");

        $this->reset([ 'postBody','postType','postPic']);
    }

    public function youtubePost()
    {
        $check = $this->validate([
            'youtubeLink' => 'required',
        ]);
        $vedio = explode('v=', $this->youtubeLink);

        if(!empty($vedio[1])){
            $this->youtubeLink = $vedio[1];
        }else{
            return redirect()->back()->withErrors('not found');
        }

        $post = new Post;
        $post->user_id = auth()->user()->id;
        $post->family_id = auth()->user()->family_id;
        if(auth()->user()->family_2 != null)
        $post->family_2 = auth()->user()->family_2;
        if(auth()->user()->family_m != null)
        $post->family_m = auth()->user()->family_m;
        $post->post = $this->postBody;
        $post->type = $this->postType;
        $post->post_vid = $this->youtubeLink;
        $post->save();

        //$this->refreshPosts();
        $this->emitSelf('pic_added');
        session()->flash('message',"Your Vedio Added Successfully");
    }

    public function imageUploadPost()
    {
         $check = $this->validate([
            'postPic' => 'image|max:2024', // 1MB Max
        ]);
        $image = $this->postPic;
        $imagename = time().'.'.$image->extension();
        $filePath = public_path('uploads/post_pic');
        if(!File::isDirectory($filePath)){
            File::makeDirectory($filePath, 0777, true, true);
        }
        $img = Image::make($image->path());
        $img->resize(769, 522, function ($const) {
            $const->aspectRatio();
        })->save($filePath.'/'.$imagename);

        $this->postPic = '/uploads/post_pic/'.$imagename;

        //$this->refreshPosts();
        $this->emitSelf('pic_added');
        session()->flash('message',"Your pic Added Successfully");
        
    }

    protected function profilePhotoDisk()
    {
        return isset($_ENV['VAPOR_ARTIFACT_NAME']) ? 's3' : config('jetstream.profile_photo_disk', 'public');
    }

     public function addPostPic()
    {
        $this->validate([
            'postPic' => ['required']
        ]);


        //$this->refreshPosts();
        $this->emitSelf('pic_added');
        session()->flash('message',"Your pic Added Successfully");
        
    }

    public function addComment(Post $post)
    {
        $this->validate([
            'commentBody' => ['required', 'string']
        ]);

        $post->comments()->create([
            'user_id' => auth()->user()->id,
            'comment' => $this->commentBody,
        ]);

        //$this->refreshPosts();

        session()->flash('message',"Your Comment Added Successfully");

        $this->reset([ 'commentBody']);
    }

    public function toggleLikePost(Post $post)
    {

        $liked = $post->likes()->where('user_id',auth()->id())->exists();
        if ($liked) {
            $post->likes()->where('user_id',auth()->id())->delete();
        } else {
            $post->likes()->where('user_id',auth()->id())->create([
                'user_id' => auth()->id(),
            ]);
        }
    }
    // public function refreshPosts()
    // {
    //     $familyPost = Post::whereIn('user_id',session('ids')[0])->orderBy('id','desc')->with('comments.user')->get();
    //     //dd(session('ids')[0]);
    //     //dd($familyPost);
    //     if(!empty($familyPost)){
    //        $this->allposts = $familyPost; 
    //     }
        
    // }

    
    public function render()
    {
         return view('livewire.posts', [
            'posts' => Post::whereIn('user_id',session('ids')[0])->orderBy('created_at','desc')->with('comments.user')->paginate(10),
        ]);
        //return view('livewire.posts');
    }


}
