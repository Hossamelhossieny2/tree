<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;
use Carbon\Carbon;
use Cache;
use DB;

class ChatBar extends Component
{

    public function render()
    {
        return view('livewire.chat-bar');
    }
}
