<?php

namespace App\Http\Livewire;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Livewire\Component;
use App\Models\User;
use App\Models\FamilyName;
use App\Models\UserWife;
use App\Models\Country;
use Illuminate\Support\Facades\Hash;
use App\Models\FaqsHelp;
use App\Models\HelpVedio;
use App\Models\UserHelp;
use Carbon\Carbon;
use Livewire\WithFileUploads;
use Spatie\Activitylog\Contracts\Activity;
use App\Http\Controller\LocationController;
use App\Models\WheelUser;
use App\Models\PlanUser;
use Request;
use Image;

class Tree extends Component
{
    use WithFileUploads;

    public $treeMemberId;
    public $family;
    public ?User $user;

    public $countries,$my_wives,$marriage,$brothers,$sisters,$die;
    public $editUser,$editName,$editLive,$editGender,$editPhone,$editBirth,$editDeath,$editEmail,$editPass,$editPic;
    public $marriage_id,$marriage_start_edit,$marriage_end_edit;
    public $name,$email,$pic,$phone,$gender,$birth,$death,$live,$marriage_start,$marriage_end,$mother,$father,$marriage_time;
    public $FaqsHelp,$needHelp,$VidHelp,$isEx,$this_fam_ids;

    public function mount(User $user)
    {
        $this->FaqsHelp = FaqsHelp::where('topic_id',3)->where('lang',app()->getLocale())->get();
        $this->needHelp = UserHelp::where('topic_id',3)->where('user_id',auth()->id())->first();
        $this->VidHelp = HelpVedio::where('sub',3)->where('lang',app()->getLocale())->get();

        $this->user = auth()->user();
        $this->family = auth()->user()->family_id;
        $this->treeMemberId = $user->id;
        $this->countries = Country::all();

        $userIp = Request::ip();;
        if($userIp == '127.0.0.1')$userIp = '41.46.213.47';
        $locationData = \Location::get($userIp);
        if(!empty($locationData)){
            $thiscountryCode = $locationData->countryCode;
        }else{
            $thiscountryCode = 'EG';
        }
        $get_country = Country::where('iso',$thiscountryCode)->first();
        $this->live = $get_country->id;
        
        //ToDo fix this out of family
    //    if(!in_array($user->id, session('ids')[0])){
    //         session()->flash('message', __('out_of_fam'));
    //         return redirect(route('tree_view',[app()->getLocale(),auth()->id()]));
    //    }
        
    }

    private function resetInput()
    {
        $this->name = null;
        $this->email = null;
        $this->pic = null;
        $this->phone = null;
        $this->gender = null;
        $this->birth = null;
        $this->death = null;
        //$this->live = null;
        $this->marriage_start = null;
        $this->marriage_end = null;
    }


    public function editPic()
    {
        // $this->can_submit = false;
        $this->validate([
            'editPic' => 'image|max:2048', // 1MB Max
        ]);

        if ($this->editUser instanceof User) {
            $this->imgResize($this->editPic,$this->editUser );
            //$this->editUser->updateProfilePhoto($this->editPic);
        }
        $this->reset(['editPic']);
        session()->flash('image_uploaded','Profile Pic Updated Successfully');
        $this->emitSelf('image_uploaded');
        
    }

    public function imgResize($file,User $user)
    {
        $image = $file;
        $imagename = time().'.'.$image->extension();
        $filePath = public_path('uploads/profile-photos');
            if(!File::isDirectory($filePath)){
                File::makeDirectory($filePath, 0777, true, true);
            }
        $img = Image::make($image->path());
        $img->resize(200, 200, function ($const) {
            $const->aspectRatio();
        })->save($filePath.'/'.$imagename);
        $user->profile_photo_path = 'uploads/profile-photos/'.$imagename;
        $user->save();

    }

    public function deleteUser($value)
    {
        $del = User::find($value);
       // User::where('id',$value)->delete();
        session()->flash('message',"User Deleted Successfully");
    }

    public function setFatherAndMotherForNewChild(User $mother,User $father)
    {
        $this->mother = $mother;
        $this->father = $father;
    }

    public function updateUserInfo($eduser)
    {
        $this->editUser = $editUser = User::find($eduser);
        $this->editName = $editUser->name;
        $this->editGender = $editUser->gender;
        $this->editPhone = $editUser->phone_number;
        $this->editLive = $editUser->country_id;
        if($this->editUser->birth_date){
            $this->editBirth = $this->editUser->birth_date->format('Y-m-d');
        }else{
            $this->editBirth = '';
        }
        if($editUser->death_date){
            $this->editDeath = $this->editUser->death_date->format('Y-m-d');
        }else{
            $this->editDeath = '';
        }
        $this->editEmail = $editUser->email;
        $this->editPass = $editUser->password;
    }

    public function updateMarInfo($wife,$hus)
    {
        $getMarDate = UserWife::where('wife_id',$wife)->where('user_id',$hus)->first();
        $this->marriage_start_edit = $getMarDate->marriage_start_date;
        $this->marriage_end_edit = $getMarDate->marriage_end_date;
        $this->marriage_id = $getMarDate->id;
        $w = User::find($wife);
        $h = User::find($hus);
        $this->editName = ' '.$w->name.' & '.$h->name;
       
    }

    public function add_fam_cach($sex,$family_id,$id)
    {
        $fam = FamilyName::find($family_id);
        if(!empty($fam->fam_cache)){
            $unserial = unserialize($fam->fam_cache);
            $unserial[] = $id;
            $serial = serialize($unserial);
        }else{
            $ids[] = $id;
            $serial = serialize($ids);
        }
        $fam->fam_cache = $serial ;
        $fam->members   = $fam->members + 1 ;
        $fam->live      = $fam->live + 1 ;
        $fam->$sex      = $fam->$sex + 1 ;
        $fam->save();
    }

    public function add_fam_stat($stat,$family_id)
    {
        $fam = FamilyName::find($family_id);
        if($stat == 'dead2'){
            $fam->live    = $fam->live - 1 ;
            $fam->dead    = $fam->dead + 1 ;
            $fam->widow   = $fam->widow + 1 ;
            $fam->save();
            
        }else{
            if($stat == "divorced") $fam->married      = $fam->married - 1 ;
            if($stat == "dead") $fam->live      = $fam->live - 1 ;
            $fam->$stat    = $fam->$stat + 1 ;
            $fam->save();
        }
        
    }

    public function setParentGenderAndLabel(string $gender)
    {
        $this->parent_gender    = $gender;
        $this->parentLabel      = $gender == 'male' ? 'Father' : 'Mother';
    }

    public function addParents(string $gender)
    {
        FamilyName::unguard();

        $this->validate([
            'name'   => ['required', 'string', 'max:25','alpha_dash'],
            'live'   => 'required',
            'phone'  => ['nullable', 'unique:users,phone_number'],
            'email'  => ['nullable','email:rfc,dns']
        ]);
        
        $parent = new User;
        $parent->name           = $this->name;
        $parent->email          = $this->email;
        $parent->country_id     = $this->live;
        $parent->phone_number   = $this->phone;
        if($this->birth != null){
        $parent->birth_date     = $this->birth;}
        $parent->death_date     = $this->death;
        $parent->gender         = $gender;
        $parent->password       = Hash::make('password');
        $parent->save();

        
        $this->add_wheel($parent);
        $this->createQuranPlan($parent);
        //Adding the father himself in the family after the save
        if ($parent->gender == 'male') {
            if ($this->user->family) {
                    $this->user->family()->update([
                        'title' => $this->name
                    ]);
                } else {
                    //make sure that the parent now is father
                     $this->user->family()
                    ->associate(FamilyName::create([
                        'title' => $this->name,
                    ]))
                    ->save();
                }
                $parent->family()->associate($this->user->family)->save();
                $this->add_fam_cach('male',$this->user->family_id,$parent->id);
                if(!empty($this->death)) $this->add_fam_stat('dead2',$this->user->family_id);
            }

            // Add family2 to mother
            if ($parent->gender == 'female') {
                //create family name to mother
                $mother = new FamilyName;
                $mother->title = $this->name.'(FAM)';
                $mother->save();
                User::where('id',$parent->id)->update([
                    'family_id' => $mother->id,
                    'family_m' => $this->user->family_id
                ]);
                User::where('id',$this->user->father->id)->update([
                    'family_m' => $mother->id
                ]);
                
                $this->add_fam_stat('married',$mother->id);
                $this->add_fam_stat('married',$this->user->family_id);
                if(!empty($this->marriage_end)){
                    $this->add_fam_stat('divorced',$mother->id);
                    $this->add_fam_stat('divorced',$this->user->family_id);
                } 
                $this->user->update([
                        'family_2' => $mother->id
                    ]);
                $this->add_fam_cach('female',$this->user->family_id,$parent->id);
                $this->add_fam_cach('female',$mother->id,$parent->id);
                $this->add_fam_cach('male',$mother->id,$this->user->father->id);
                $this->add_fam_stat('married',$mother->id);
                $this->add_fam_cach('male',$mother->id,$this->user->id);
                if(!empty($this->death)) $this->add_fam_stat('dead2',$mother->id);
            }

        $this->user->refresh();


        if ($parent->gender == 'male') {
            //Logic For Associating Father
            $this->user
                ->father()
                ->associate($parent)
                ->save();
            if ($this->user->mother) {
                $parent->wives()->attach($this->user->mother_id);
            }
             //Refreshing the father again
            $this->user->refresh();
            $this->emitSelf('Father_added');
        }else{
            //Logic For Associating Mother
            $this->user
                ->mother()
                ->associate($parent)
                ->save();
            if ($this->user->father) {
                $this->user->father->wives()->attach($parent->id,[
                'marriage_start_date' => $this->marriage_start,
                'marriage_end_date' => $this->marriage_end,
                ]);
            }
             //Refreshing the MOther again
            $this->user->refresh();
            $this->emitSelf('Mother_added');
        }
        session()->flash('message',__('add_succ'));
        $this->resetInput();
    }

    public function addSibling(string $gender)
    {
        $this->validate([
            'mother' => ['required', 'string', 'max:25','alpha_dash'],
            'name'   => 'required',
            'live'   => 'required',
            'phone'  => ['nullable', 'unique:users,phone_number'],
            'email'  => ['nullable','email:rfc,dns']
        ]);
        $brother                = new User;
        $brother->name          = $this->name;
        $brother->country_id    = $this->live;
        $brother->email         = $this->email;
        $brother->phone_number  = $this->phone;
        $brother->mother_id     = $this->mother;
        $brother->father_id     = $this->user->father_id;
        $brother->family_id     = $this->user->family_id;
        $brother->family_2      = $this->user->family_2;
        $brother->gender        = $gender;
        if($this->birth != null){
        $brother->birth_date    = $this->birth;}
        $brother->death_date    = $this->death;
        $brother->password      = Hash::make('password');
        $brother->save();

        $this->add_wheel($brother);
        $this->createQuranPlan($brother);
        $this->add_fam_cach($gender,$brother->family_id,$brother->id);
        if(!empty($this->death)) $this->add_fam_stat('dead',$this->user->family_id);
        $this->user->refresh();
        $this->emitSelf('Sibling_added');
        session()->flash('message',$brother->name.' '.__('add_succ'));
        $this->resetInput();
    }

    public function addWife()
    {
        $this->validate([
            'name'     => ['required', 'string', 'max:25','alpha_dash'],
            'phone'    => ['nullable','unique:users,phone_number'],
            'live'     => 'required',
            'email'    => ['nullable','email:rfc,dns'],
        ]);
        $wife               = new User;
        $wife->name         = $this->name;
        $wife->country_id   = $this->live;
        $wife->email        = $this->email;
        $wife->phone_number = $this->phone;
        $wife->family_m     = $this->user->family_id;
        $wife->gender       = 'female';
        if($this->birth != null){
        $wife->birth_date   = $this->birth;}
        $wife->death_date   = $this->death;
        $wife->password     = Hash::make('password');
        $wife->save();
        


        $this->add_wheel($wife);
        $this->createQuranPlan($wife);
        $this->user->wives()->attach($wife->id,[
            'marriage_start_date' => $this->marriage_start,
            'marriage_end_date' => $this->marriage_end,
            ]);
         //create family name to wife
        $wifeFam = new FamilyName;
        $wifeFam->title = $this->name.'(FAM)';
        $wifeFam->save();

        $this->user->update(['family_m'=>$wifeFam->id]);

        $this->add_fam_cach('female',$wifeFam->id,$wife->id);

        $this->add_fam_cach('male',$this->user->family_id,$wife->id);
        $this->add_fam_stat('married',$wifeFam->id);
        $this->add_fam_stat('married',$this->user->family_id);
        if(!empty($this->marriage_end)){
           $this->add_fam_stat('divorced',$wifeFam->id); 
           $this->add_fam_stat('divorced',$this->user->family_id);
        } 
        if(!empty($this->death)) $this->add_fam_stat('dead2',$wifeFam->id);
        User::where('id',$wife->id)->update([
            'family_id' => $wifeFam->id,
        ]);
        $this->user->refresh();
        $this->emitSelf('wife_added');
        session()->flash('message',__('add_succ'));
        $this->resetInput();
    }

    public function addChild()
    {
        $this->validate([
            'live'    => 'required',
            'name'    => ['required', 'string', 'max:25','alpha_dash'],
            'gender'  => 'required',
            'phone'   => ['nullable','unique:users,phone_number'],
            'email'   => ['nullable','email:rfc,dns']
        ]);
        $mother = User::find($this->mother);
        $child                  = new User;
        $child->name            = $this->name;
        $child->country_id      = $this->live;
        $child->family_id       = $this->user->family_id;
        $child->family_2        = $this->mother->family_id;
        $child->phone_number    = $this->phone;
        $child->gender          = $this->gender;
        if($this->birth != null){
        $child->birth_date      = $this->birth;}
        $child->death_date      = $this->death;
        $child->password        = Hash::make('password');
        $child->save();

        $this->add_fam_cach($child->gender,$child->family_id,$child->id);
        $this->add_fam_cach($child->gender,$child->family_2,$child->id);
        $this->add_wheel($child);
        $this->createQuranPlan($child);
        if(!empty($this->death)) $this->add_fam_stat('dead',$this->user->family_id);
        $child->father()->associate($this->father)->save();
        $child->mother()->associate($this->mother)->save();
        $child->family()->associate($child->father->family)->save();
        $this->user->refresh();
        $this->emitSelf('child_added');
        session()->flash('message',$child->name.' '.__('add_succ'));
        $this->resetInput();
    }

    public function addHusband()
    {
        $this->validate([
            'name'         => ['required', 'string', 'max:25','alpha_dash'],
            'phone'        => ['nullable','unique:users,phone_number'],
            'live'         => 'required',
            'email'        => ['nullable','email:rfc,dns']
        ]);
        $husband                    = new User;
        $husband->name              = $this->name;
        $husband->country_id        = $this->live;
        $husband->phone_number      = $this->phone;
        $husband->email             = $this->email;
        $husband->family_m          = $this->user->family_id;
        $husband->gender            = 'male';
        if($this->birth != null){
        $husband->birth_date        = $this->birth;}
        $husband->death_date        = $this->death;
        $husband->password          = Hash::make('password');
        $husband->save();
        
        $this->add_wheel($husband);
        $this->createQuranPlan($husband);
        $this->user->husbands()->attach($husband->id,[
            'marriage_end_date'     => $this->marriage_end,
            'marriage_start_date'   => $this->marriage_start,
        ]);
         //create family name to wife
        $husFam = new FamilyName;
        $husFam->title = $this->name;
        $husFam->save();
        $husband->family_id      = $husFam->id;
        $husband->save();

        $this->add_fam_stat('married',$husFam->id);
        $this->add_fam_stat('married',$this->user->family_id);
        if(!empty($this->marriage_end)){
            $this->add_fam_stat('divorced',$husFam->id);
            $this->add_fam_stat('divorced',$this->user->family_id);
        } 
        if(!empty($this->death)) $this->add_fam_stat('dead2',$husFam->id);
        $this->add_fam_cach('male',$husFam->id,$husband->id);
        $this->add_fam_cach('female',$this->user->family_id,$husband->id);
        $this->user->refresh();
        $this->emitSelf('husband_added');
        session()->flash('message',__('add_succ'));
        $this->resetInput();
    }

    public function userEdit(){
        $this->validate([
            'editName'   => ['required', 'string', 'max:25','alpha_dash'],
            'editPhone'  => ['nullable','unique:users,id,'.$this->editUser->id],
            'editEmail'  => [ 'nullable','email', 'max:25'],
        ]);
        $editedUser = User::find($this->editUser->id);

        $editedUser->name           = $this->editName;
        if($this->editLive)
        $editedUser->country_id     = $this->editLive;
        if($this->editEmail)
        $editedUser->email          = $this->editEmail;
        $editedUser->phone_number   = $this->editPhone;
        $editedUser->gender         = $this->editGender;
        if($this->editBirth)
        $editedUser->birth_date     = $this->editBirth;
        if($this->editDeath)
        $editedUser->death_date     = $this->editDeath;
        $editedUser->password       = $this->editPass;
        $editedUser->save();

        if($editedUser->gender == 'male'){
            if($editedUser->wives()->count()>0){
                $this->add_fam_stat('dead2',$this->editUser->family_id);
            }else{
                $this->add_fam_stat('dead',$this->editUser->family_id);
            }
        }else{
            if($editedUser->husbands()->count()>0){
                $this->add_fam_stat('dead2',$this->editUser->family_id);
            }else{
                $this->add_fam_stat('dead',$this->editUser->family_id);
            }
        }
        if(!empty($this->editDeath)) $this->add_fam_stat('dead',$this->editUser->family_id);
        $this->emitSelf('User_modified');
        session()->flash('message',__('mod_succ'));
        $this->reset(['editUser','editName','editPhone','editGender','editBirth','editDeath','editPass','editLive']);
    }

    public function marEdit(){
        $userMarr = UserWife::find($this->marriage_id);
        $userMarr->marriage_start_date = $this->marriage_start_edit;
        $userMarr->marriage_end_date = $this->marriage_end_edit;
        $userMarr->save();

        $this->emitSelf('marr_modified');
        session()->flash('message',__('mod_succ'));
        $this->reset(['marriage_start','marriage_end','marriage_id','editName']);
    }
    public function get_country_from_ip(Request $request)
    {
            $userIp = $request->ip();
            if($userIp == '127.0.0.1')$userIp = '41.46.213.47';
            $locationData = \Location::get($userIp);
            $get_country = Country::where('iso',$locationData->countryCode)->first;
            return $get_country->id;
    }

    public function add_wheel(User $user)
    {
        $userWheel = new WheelUser;
        $userWheel->user_id = $user->id;
        $userWheel->save();
    }

    protected function createQuranPlan(User $user)
    {
        $new_user_plan = new PlanUser;
        $new_user_plan->user_id = $user->id;
        $new_user_plan->sura = '114';
        $new_user_plan->aya = '1';
        $new_user_plan->page = '604';
        $new_user_plan->save();

    }

    public function render()
    {
        $id = $this->treeMemberId;
        $this->father_wives = collect();

        //TODO this is wrong
        if (!empty($id)) {
            $current_user = User::find($id);
            $this->user = $current_user;
        }else {
            $current_user = Auth::user();
        }

        if($current_user->mother){
            $now = Carbon::now();
            $this->marriage_time = __('unknown_date');
            if($this->user->mother->isAlreadyMarriedToHusband()){
                $current_husband = $this->user->mother->husbands()->whereNull('marriage_end_date')->whereNotNull('marriage_start_date')->first();
                if ($current_husband) {
                    $this->marriage_time = Carbon::parse($current_husband->husband->marriage_start_date)->diffInYears($now);
                }
            }

        }

        $siblings = User::where(function ($query) use ($current_user){
            $query->where('father_id', '=', $current_user->father_id)
                ->orWhere('mother_id', '=', $current_user->mother_id);
        })
            ->where('id','!=',$current_user->id)
            ->whereNotNull('father_id')
            ->whereNotNull('mother_id')
            ->get();

        $this->brothers = $siblings->filter(function ($sibling){
            return $sibling->gender == 'male';
        });

        $this->sisters = $siblings->filter(function ($sibling){
            return $sibling->gender == 'female';
        });

        return view('livewire.tree');
    }

}
