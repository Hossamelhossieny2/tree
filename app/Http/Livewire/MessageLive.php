<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\UserMsg;

class MessageLive extends Component
{
    public $Msgs;

    public function mount()
    {
        $this->Msgs = UserMsg::whereNull('replay')->with('user')->with('from_user')->with('repliess')->get();
    }
    
    public function render()
    {
        return view('livewire.message-live');
    }
}
