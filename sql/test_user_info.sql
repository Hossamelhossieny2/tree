# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.34)
# Database: tree-app
# Generation Time: 2021-08-26 19:28:25 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table user_education
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_education`;

CREATE TABLE `user_education` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `is_job` tinyint(1) NOT NULL DEFAULT '0',
  `place` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `specialty` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `user_education` WRITE;
/*!40000 ALTER TABLE `user_education` DISABLE KEYS */;

INSERT INTO `user_education` (`id`, `user_id`, `is_job`, `place`, `specialty`, `start`, `end`, `created_at`, `updated_at`)
VALUES
	(1,1,0,'The New College of Design','Breaking Good, RedDevil, People of Interest, The Running Dead, Found, American Guy.','2001','2006','2021-08-26 16:40:44',NULL),
	(2,1,0,'Digital Design Intern','Digital Design Intern for the “Multimedz” agency. Was in charge of the communication with the clients.','2006','2008','2021-08-26 16:40:44',NULL),
	(3,1,0,'Rembrandt Institute','Five months Digital Illustration course. Professor: Leonardo Stagg.','2008','','2021-08-26 16:40:44',NULL),
	(4,1,0,'UI/UX Designer','UI/UX Designer for the “Daydreams” agency.','2008','2013','2021-08-26 16:40:44',NULL),
	(5,1,0,'The Digital College','6 months intensive Motion Graphics course. After Effects and Premire. Professor: Donatello Urtle.','2013','2014','2021-08-26 16:40:44',NULL),
	(6,1,1,'Senior UI/UX Designer','Senior UI/UX Designer for the “Daydreams” agency. I’m in charge of a ten person group, overseeing all the proyects and talking to potential clients.','2013','','2021-08-26 16:40:44',NULL);

/*!40000 ALTER TABLE `user_education` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_infos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_infos`;

CREATE TABLE `user_infos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `bio` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `hobby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tv` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `movies` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `games` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `music` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `books` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `writer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `other` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lives_in` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ticktok` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `user_infos` WRITE;
/*!40000 ALTER TABLE `user_infos` DISABLE KEYS */;

INSERT INTO `user_infos` (`id`, `user_id`, `bio`, `hobby`, `tv`, `movies`, `games`, `music`, `books`, `writer`, `other`, `birth_place`, `lives_in`, `website`, `facebook`, `twitter`, `instagram`, `ticktok`, `created_at`, `updated_at`)
VALUES
	(1,1,'Hi, I’m James, I’m 36 and I work as a Digital Designer for the “Daydreams” Agency in Pier 56','I like to ride the bike to work, swimming, and working out. I also like reading design magazines, go to museums, and binge watching a good tv show while it’s raining outside.\n','Breaking Good, RedDevil, People of Interest, The Running Dead, Found, American Guy.','Idiocratic, The Scarred Wizard and the Fire Crown, Crime Squad, Ferrum Man.','The First of Us, Assassin’s Squad, Dark Assylum, NMAK16, Last Cause 4, Grand Snatch Auto.','Iron Maid, DC/AC, Megablow, The Ill, Kung Fighters, System of a Revenge.','The Crime of the Century, Egiptian Mythology 101, The Scarred Wizard, Lord of the Wings, Amongst Gods, The Oracle, A Tale of Air and Water.','Martin T. Georgeston, Jhonathan R. Token, Ivana Rowle, Alexandria Platt, Marcus Roth.','Swimming, Surfing, Scuba Diving, Anime, Photography, Tattoos, Street Art.','Manial','Futre City','hossamelhossieny.com','facebook.com\\hossamelhossieny','','','','2021-08-26 16:40:44',NULL);

/*!40000 ALTER TABLE `user_infos` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
