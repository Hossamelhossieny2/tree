# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.34)
# Database: tree-app
# Generation Time: 2021-07-17 13:31:31 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table failed_jobs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table family_names
# ------------------------------------------------------------

DROP TABLE IF EXISTS `family_names`;

CREATE TABLE `family_names` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `members` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `family_names` WRITE;
/*!40000 ALTER TABLE `family_names` DISABLE KEYS */;

INSERT INTO `family_names` (`id`, `title`, `members`, `created_at`, `updated_at`)
VALUES
	(1,'ramadan',16,'2021-07-14 22:43:05','2021-07-17 11:05:33'),
	(3,'Elnemr',1,'2021-07-15 09:36:05','2021-07-15 09:36:05'),
	(4,'ahmed',3,'2021-07-15 11:09:35','2021-07-15 11:20:34'),
	(5,'baba',3,'2021-07-15 12:29:04','2021-07-15 12:30:14'),
	(6,'Waleed',2,'2021-07-17 12:01:24','2021-07-17 12:02:14');

/*!40000 ALTER TABLE `family_names` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2014_10_12_000000_create_users_table',1),
	(2,'2014_10_12_100000_create_password_resets_table',1),
	(3,'2014_10_12_200000_add_two_factor_columns_to_users_table',1),
	(4,'2019_08_19_000000_create_failed_jobs_table',1),
	(5,'2019_12_14_000001_create_personal_access_tokens_table',1),
	(6,'2021_07_13_095526_create_sessions_table',1),
	(7,'2020_05_21_100000_create_teams_table',2),
	(8,'2020_05_21_200000_create_team_user_table',2),
	(9,'2020_05_21_300000_create_team_invitations_table',2);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table personal_access_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `personal_access_tokens`;

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sessions`;

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sessions_user_id_index` (`user_id`),
  KEY `sessions_last_activity_index` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`)
VALUES
	('6iI7y7bVwYpZdVWOANjmGN3io1lTfBgKWaGwtghu',1,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36','YTo2OntzOjY6Il90b2tlbiI7czo0MDoiUmVuNWE2ak10WExMek91d1oxczlPclE4MkFPVVNveDNmbE1nVm5abyI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzA6Imh0dHA6Ly90cmVlLWFwcC50ZXN0L2Rhc2hib2FyZCI7fXM6NTA6ImxvZ2luX3dlYl81OWJhMzZhZGRjMmIyZjk0MDE1ODBmMDE0YzdmNThlYTRlMzA5ODlkIjtpOjE7czoxNzoicGFzc3dvcmRfaGFzaF93ZWIiO3M6NjA6IiQyeSQxMCRwUU4xMUtyajRxeDRNTmxIcGlHZXlPSnZjN0c4bk1yV0RQTktIcEVyL2p5MUxLWUtnOEV0SyI7czoyMToicGFzc3dvcmRfaGFzaF9zYW5jdHVtIjtzOjYwOiIkMnkkMTAkcFFOMTFLcmo0cXg0TU5sSHBpR2V5T0p2YzdHOG5NcldEUE5LSHBFci9qeTFMS1lLZzhFdEsiO30=',1626525351),
	('7AyJ5I2u5TMZnN45jIyRbG2Gm3sz5gVrNV1O1ULC',1,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36','YTo2OntzOjY6Il90b2tlbiI7czo0MDoiZWRFRDNJMnpJdFFWS2ZlYkFVa0UyUDdvMXk5c3MzOGxCQ3dXajJYYyI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mjk6Imh0dHA6Ly90cmVlLnRlc3QvdHJlZV92aWV3LzU1Ijt9czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6MTtzOjE3OiJwYXNzd29yZF9oYXNoX3dlYiI7czo2MDoiJDJ5JDEwJHBRTjExS3JqNHF4NE1ObEhwaUdleU9KdmM3RzhuTXJXRFBOS0hwRXIvankxTEtZS2c4RXRLIjtzOjIxOiJwYXNzd29yZF9oYXNoX3NhbmN0dW0iO3M6NjA6IiQyeSQxMCRwUU4xMUtyajRxeDRNTmxIcGlHZXlPSnZjN0c4bk1yV0RQTktIcEVyL2p5MUxLWUtnOEV0SyI7fQ==',1626527463);

/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table team_invitations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_invitations`;

CREATE TABLE `team_invitations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `team_id` bigint(20) unsigned NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `team_invitations_team_id_email_unique` (`team_id`,`email`),
  CONSTRAINT `team_invitations_team_id_foreign` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table team_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_user`;

CREATE TABLE `team_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `team_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `team_user_team_id_user_id_unique` (`team_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table teams
# ------------------------------------------------------------

DROP TABLE IF EXISTS `teams`;

CREATE TABLE `teams` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_team` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `teams_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;

INSERT INTO `teams` (`id`, `user_id`, `name`, `personal_team`, `created_at`, `updated_at`)
VALUES
	(1,2,'Hossam\'s Team',1,'2021-07-13 10:54:00','2021-07-13 10:54:00'),
	(2,1,'loo\'s Team',1,'2021-07-13 12:33:30','2021-07-13 12:33:30'),
	(3,27,'khaled\'s Team',1,'2021-07-14 13:17:33','2021-07-14 13:17:33'),
	(4,1,'Hossam\'s Team',1,'2021-07-14 21:00:55','2021-07-14 21:00:55'),
	(5,44,'khaled\'s Team',1,'2021-07-15 11:08:55','2021-07-15 11:08:55'),
	(6,56,'mohsen\'s Team',1,'2021-07-15 12:28:47','2021-07-15 12:28:47'),
	(7,66,'Khaled\'s Team',1,'2021-07-17 12:00:37','2021-07-17 12:00:37'),
	(8,55,'loloo\'s Team',1,'2021-07-13 10:54:00','2021-07-13 10:54:00');

/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `family_id` int(11) DEFAULT NULL,
  `gender` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father` int(11) DEFAULT NULL,
  `mother` int(11) DEFAULT NULL,
  `wife` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exwife` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint(20) unsigned DEFAULT NULL,
  `profile_photo_path` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `family_id`, `gender`, `father`, `mother`, `wife`, `exwife`, `email`, `mobile`, `email_verified_at`, `password`, `two_factor_secret`, `two_factor_recovery_codes`, `remember_token`, `current_team_id`, `profile_photo_path`, `created_at`, `updated_at`)
VALUES
	(1,'Hossam',1,'male',24,33,',55,,62,',NULL,'microsystems2@gmail.com',NULL,NULL,'$2y$10$pQN11Krj4qx4MNlHpiGeyOJvc7G8nMrWDPNKHpEr/jy1LKYKg8EtK',NULL,NULL,'38tMB9DfWVY3AEvRFD23PpaCnuWAdWyQCm1dMMO0yjEDOVz8705o8U8GV4dX',2,NULL,'2021-07-14 21:00:55','2021-07-17 10:12:57'),
	(24,'elsayed',1,'male',25,34,',33,,42,,50,',NULL,'elsayed@q.q',NULL,NULL,'$2y$10$734cBGEs4lJ7WRYN/Ix0Re4lrLgAgGT.IjqgjebsJGvL.nIzcY0vq',NULL,NULL,NULL,NULL,NULL,'2021-07-14 22:43:05','2021-07-15 12:02:15'),
	(25,'elhossieny',1,'male',35,36,',34,',NULL,'elho@a.a',NULL,NULL,'$2y$10$FvWBotlOYTkP1TXPq6gXq.8Y8chgBfTwszMbjA4O.pTcR6.fftF0y',NULL,NULL,NULL,NULL,NULL,'2021-07-14 22:43:19','2021-07-15 09:35:35'),
	(33,'fofa',3,'female',37,NULL,',24,',NULL,'hhhh@gm.cc',NULL,NULL,'$2y$10$6R7h5CSRzbhMCE5Ey23HOOVbpK8C5LWwgX4FKlZ8TMy5cdEek7dnm',NULL,NULL,NULL,NULL,NULL,'2021-07-15 09:28:00','2021-07-15 09:36:05'),
	(34,'sokar',NULL,'female',NULL,NULL,',25,',NULL,'hossddam@test.com',NULL,NULL,'$2y$10$1t8VLvOADmHTIuxmcdfKJetN.6apCBWNvBR6Y95k0gB1BPjhi58fW',NULL,NULL,NULL,NULL,NULL,'2021-07-15 09:31:33','2021-07-15 09:31:33'),
	(35,'abdullah',1,'male',38,NULL,',36,',NULL,'ab@s.s',NULL,NULL,'$2y$10$X3hyNeuY59tqLhDnBhs7jeXxd7r5kE8N4qacSvYXzkeiSn4mKx8yu',NULL,NULL,NULL,NULL,NULL,'2021-07-15 09:35:11','2021-07-15 09:43:41'),
	(36,'khadiga',NULL,'female',NULL,NULL,',35,',NULL,'kh@a.a',NULL,NULL,'$2y$10$9uSCeFTitA1hnSwX5gJvvu3prrtaLb/LOOoNqzAofutNwmKEbHUeu',NULL,NULL,NULL,NULL,NULL,'2021-07-15 09:35:35','2021-07-15 09:35:35'),
	(37,'Elnemr',3,'male',NULL,NULL,NULL,NULL,'nmr@a.a',NULL,NULL,'$2y$10$SiX8aIYIErdbkenFRhHrFOuPAnOW6Azu.MLmCzv2NxrmA2deD9.ha',NULL,NULL,NULL,NULL,NULL,'2021-07-15 09:36:05','2021-07-15 09:36:05'),
	(38,'ramadan',1,'male',NULL,NULL,NULL,NULL,'rama@q.a',NULL,NULL,'$2y$10$awkZd1u/vibQ8Y425ySbHOh2vPhWaEtpLlmdog6oS5vAKy49FwWiq',NULL,NULL,NULL,NULL,NULL,'2021-07-15 09:43:41','2021-07-15 09:43:41'),
	(40,'mohamed',1,'male',24,33,NULL,NULL,'as@as.as',NULL,NULL,'$2y$10$FODGM7U30fQ2REtZKuQkrOop4eX3XzfaO3KeTa4CPw1/tEWZPVyuq',NULL,NULL,NULL,NULL,NULL,'2021-07-15 09:55:15','2021-07-15 09:55:15'),
	(41,'nona',1,'female',24,33,NULL,NULL,'no@a.a',NULL,NULL,'$2y$10$9GpJvyhrMOZzNio3q.7qq.Q5LP/bD1IYAVNcd5CBVOIQfTDId4AwW',NULL,NULL,NULL,NULL,NULL,'2021-07-15 10:02:50','2021-07-15 10:02:50'),
	(42,'aliaa',1,'female',NULL,NULL,',24,',NULL,'al@al.a',NULL,NULL,'$2y$10$FteEubZLGw7DLhU9lBrnk.aogbP3HmZvFirITAYSpTgj5443eepOK',NULL,NULL,NULL,NULL,NULL,'2021-07-15 10:14:48','2021-07-15 10:14:48'),
	(43,'alll',1,'male',24,42,NULL,NULL,'aa@aa.aaa',NULL,NULL,'$2y$10$7EIfGZKEg.mCPzuhJzwdOetLRuS/sz2s.QR/Doe8iD8QvGp3qCCmu',NULL,NULL,NULL,NULL,NULL,'2021-07-15 10:57:23','2021-07-15 10:57:23'),
	(44,'khaled',4,'male',45,46,NULL,NULL,'elmasry.dev@gmail.com',NULL,NULL,'$2y$10$4AcAAzYEwLBNF7xxcFXpQuUi6LoLhtSRH4xUhNerk0WMxnOVUNKaa',NULL,NULL,NULL,5,NULL,'2021-07-15 11:08:55','2021-07-15 11:17:17'),
	(45,'ahmed',4,'male',NULL,NULL,',46,',NULL,'ahmed1122@gmail.com',NULL,NULL,'$2y$10$144QZQNBh7KCnTSl4fwdQu.cdVeZMMRZ.3fBJskbxV3.W3FbFryze',NULL,NULL,NULL,NULL,NULL,'2021-07-15 11:09:35','2021-07-15 11:17:17'),
	(46,'mona',NULL,'female',NULL,NULL,',45,',NULL,'mona1122@gmail.com',NULL,NULL,'$2y$10$p0ViMGim2ks3iU5aXvaem.aYs6K765OLGrGxC0q.EYPPEbfu8OnXS',NULL,NULL,NULL,NULL,NULL,'2021-07-15 11:17:17','2021-07-15 11:17:17'),
	(47,'wa2el',4,'male',45,46,NULL,NULL,'wa2el@gmail.com',NULL,NULL,'$2y$10$A99oUGD17BnZb32pQwa9L.ksU5O.wm0VepGWXg8ZOR5Z0wKd4P8Qe',NULL,NULL,NULL,NULL,NULL,'2021-07-15 11:20:02','2021-07-15 11:20:02'),
	(48,'nahla',4,'female',45,46,NULL,NULL,'nahla@gmail.com',NULL,NULL,'$2y$10$blATenM7gRC8nXCHPKzBI.jK8762WpTAf56EUZ4VXH9ZaH5Fh0uFW',NULL,NULL,NULL,NULL,NULL,'2021-07-15 11:20:34','2021-07-15 11:20:34'),
	(49,'ss',1,'male',24,50,NULL,NULL,'ss@ss.ss',NULL,NULL,'$2y$10$o91Se5fudSE4oVTbNUiEaeRyJs3Kp7jJyKBy/rGTyBlVRCBO1DnQy',NULL,NULL,NULL,NULL,NULL,'2021-07-15 11:46:20','2021-07-15 12:02:15'),
	(50,'samiha',NULL,'female',NULL,NULL,',24,',NULL,'sam@gmail.com',NULL,NULL,'$2y$10$WVVutyWyefj..Q5DwNItBO.nFGO77qFFIfgBxB49nJHXa/kjeDuZa',NULL,NULL,NULL,NULL,NULL,'2021-07-15 12:02:15','2021-07-15 12:02:15'),
	(51,'shosho',1,'female',24,33,NULL,NULL,'microsystems2222@gmail.com',NULL,NULL,'$2y$10$SjCzEz7fJ4zNjhy2z5Ff1ubQ7XXXoSVeVxz8kDqaprfQz.uFKtqEy',NULL,NULL,NULL,NULL,NULL,'2021-07-15 12:08:59','2021-07-15 12:08:59'),
	(52,'belel',1,'male',25,34,NULL,NULL,'belal@gmail.com',NULL,NULL,'$2y$10$eaDuB9q0V.ogdNYXKu7G7.ObL.KT78ibVuPI5FJWeEnggPPPNzqLC',NULL,NULL,NULL,NULL,NULL,'2021-07-15 12:09:48','2021-07-15 12:09:48'),
	(53,'lolo',NULL,'female',NULL,NULL,NULL,NULL,'loo@gmail.com',NULL,NULL,'$2y$10$Snl0E37U1/IQKk/kzA15OeuGpv.fQFscFE3pLBOOKYH46RaGlXVCq',NULL,NULL,NULL,8,NULL,'2021-07-15 12:21:58','2021-07-17 12:54:52'),
	(54,'eee',NULL,'female',NULL,NULL,NULL,NULL,'w@qw.qw',NULL,NULL,'$2y$10$c7YNT4jSroPjbviIz.i1A.yCoEQiTOSzQKRuqNG3XMhAMg0Hg7hsG',NULL,NULL,NULL,NULL,NULL,'2021-07-15 12:23:31','2021-07-15 12:23:31'),
	(55,'loloo',1,'female',NULL,NULL,',1,',NULL,'lolo@gmail.com',NULL,NULL,'$2y$10$LcOpADxRi9Q21gY4kwI5yelOWKmf9QDNG.1OgSRPBzrvGvYULLq1G',NULL,NULL,NULL,2,NULL,'2021-07-15 12:24:29','2021-07-15 12:24:29'),
	(56,'mohsen',5,NULL,57,58,',61,',NULL,'mohsen@gmail.com',NULL,NULL,'$2y$10$cK9uHREdbyKLef6dtQdIFer12RxtBvJ3Rf/WEY878RPdvEuxnud.C',NULL,NULL,NULL,6,NULL,'2021-07-15 12:28:47','2021-07-15 12:31:15'),
	(57,'baba',5,'male',NULL,NULL,',58,',NULL,'baba@gmail.com',NULL,NULL,'$2y$10$YQwfp3JD/R/wars6nZdUgeEMWpST3WbElx1JDz8KU348UIMaPS6Wi',NULL,NULL,NULL,NULL,NULL,'2021-07-15 12:29:04','2021-07-15 12:29:26'),
	(58,'mama',NULL,'female',NULL,NULL,',57,',NULL,'mama@gmail.com',NULL,NULL,'$2y$10$aCvOBaiyPlMeZS5BS/dvBesqX0xBLmPQc9vGn3FkeE0aP3.hj2pdO',NULL,NULL,NULL,NULL,NULL,'2021-07-15 12:29:26','2021-07-15 12:29:26'),
	(59,'bro',5,'male',57,58,NULL,NULL,'bro@gmail.com',NULL,NULL,'$2y$10$pFStWzumMAy68/2WUJpLQuOtGqSm4TIwUj0gyaASn9JBvK2HQMk6y',NULL,NULL,NULL,NULL,NULL,'2021-07-15 12:29:56','2021-07-15 12:29:56'),
	(60,'sis',5,'female',57,58,NULL,NULL,'sis@gmail.com',NULL,NULL,'$2y$10$rIPkpgkCwv9ha.wb522TNOTKjuleekGF0mXU0CQLhDsNU7loKkJxK',NULL,NULL,NULL,NULL,NULL,'2021-07-15 12:30:14','2021-07-15 12:30:14'),
	(61,'wife',NULL,'female',NULL,NULL,',56,',NULL,'wife@wife.com',NULL,NULL,'$2y$10$epDH9pjFOKNG0/g64wBRW.2e5Ou1wf3lZFk2j.POCrm/LXbvfhRSe',NULL,NULL,NULL,NULL,NULL,'2021-07-15 12:31:15','2021-07-15 12:31:15'),
	(62,'chereen',NULL,'female',NULL,NULL,',1,',NULL,'cherren@gmail.com',NULL,NULL,'$2y$10$uPAhOVTyYQmIJeTEWWMgQeElBehlVH5QNK7XTjmjGKQ08lJH8lwhW',NULL,NULL,NULL,NULL,NULL,'2021-07-17 10:12:57','2021-07-17 10:12:57'),
	(63,'hohos',1,'male',1,55,NULL,NULL,'hos@sdsd.sdsd',NULL,NULL,'$2y$10$iqOjmDdE2T6YxcdIb1gD/O.1kU.rpNIek9VlvzuRCKHZbl82UrEk.',NULL,NULL,NULL,NULL,NULL,'2021-07-17 10:57:34','2021-07-17 10:57:34'),
	(64,'lovy',1,'female',1,62,NULL,NULL,'lovy@sdsd.sdsd',NULL,NULL,'$2y$10$k/sA7WQ5u4qGacSZGRDn.umTumvW7/I.RrjJ/c5QsMV5Xpc54EiKu',NULL,NULL,NULL,NULL,NULL,'2021-07-17 10:58:23','2021-07-17 10:58:23'),
	(65,'rania',1,'female',0,55,NULL,NULL,'rania@g.g',NULL,NULL,'$2y$10$uLWgPOwntIOhDj.08X.qO.AiT16Jg4Px1HF5zbKklvttp9SP2Sp7.',NULL,NULL,NULL,NULL,NULL,'2021-07-17 11:05:33','2021-07-17 11:05:33'),
	(66,'Khaled',6,NULL,67,68,',70,',NULL,'khaledw62@gmail.com',NULL,NULL,'$2y$10$OwQqBc6tT4avaF5YBylpyur39b.JBBLltap1tvnOOz4FeBDundIRK',NULL,NULL,NULL,7,NULL,'2021-07-17 12:00:37','2021-07-17 12:02:30'),
	(67,'Waleed',6,'male',NULL,NULL,',68,',NULL,'waleed@jds.com',NULL,NULL,'$2y$10$AHDNOpws11chkJ1u7l5JeuO/BusZDgPAjieNJRDolPHzJkysdOpRW',NULL,NULL,NULL,NULL,NULL,'2021-07-17 12:01:24','2021-07-17 12:01:50'),
	(68,'Abeer',NULL,'female',NULL,NULL,',67,',NULL,'abeer@kfs.com',NULL,NULL,'$2y$10$0tJRjt2XHk2LGSqVv8up/uw0Cnfpg6hOUFJM/3jsEQ3C8llRtpKce',NULL,NULL,NULL,NULL,NULL,'2021-07-17 12:01:50','2021-07-17 12:01:50'),
	(69,'Ahmed',6,'male',67,68,NULL,NULL,'ahmed@j.com',NULL,NULL,'$2y$10$HGl1rR1uYJi0asXfvjV63.ZwmobVboVZ67gEzUxfWzY1DNbU6vlGG',NULL,NULL,NULL,NULL,NULL,'2021-07-17 12:02:14','2021-07-17 12:02:14'),
	(70,'Yasmin',NULL,'female',NULL,NULL,',66,',NULL,'yasmin@jsf.com',NULL,NULL,'$2y$10$T4tQ7my38LIEwWAYTyQ7Re.YMt4BOv5ATMYBfO6BKKwkgj7uvX.H.',NULL,NULL,NULL,NULL,NULL,'2021-07-17 12:02:30','2021-07-17 12:02:30');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users_bu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_bu`;

CREATE TABLE `users_bu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father` int(11) DEFAULT NULL,
  `mother` int(11) DEFAULT NULL,
  `wife` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exwife` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint(20) unsigned DEFAULT NULL,
  `profile_photo_path` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table users_ho
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_ho`;

CREATE TABLE `users_ho` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father` int(11) DEFAULT NULL,
  `mother` int(11) DEFAULT NULL,
  `wife` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exwife` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint(20) unsigned DEFAULT NULL,
  `profile_photo_path` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users_ho` WRITE;
/*!40000 ALTER TABLE `users_ho` DISABLE KEYS */;

INSERT INTO `users_ho` (`id`, `name`, `gender`, `father`, `mother`, `wife`, `exwife`, `email`, `mobile`, `email_verified_at`, `password`, `two_factor_secret`, `two_factor_recovery_codes`, `remember_token`, `current_team_id`, `profile_photo_path`, `created_at`, `updated_at`)
VALUES
	(1,'hossam','male',6,7,',10,16,','','hossam@gmail.com','0111',NULL,'$2y$10$VIdgTcS878crKq/6NhsBROvZQSmeehe.NcUrynTbLz09A516hErQi',NULL,NULL,'yKtEsi2dxxv0SN3kLK12A80tS2VAq9DuunoXvVpZCbLX53be93yvn5QqIwSo',3,NULL,'2021-07-14 13:17:33','2021-07-14 13:17:33'),
	(6,'elsayed','male',21,24,',7,',NULL,'elsayed@gmail.com','0666',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(7,'fadya','female',34,0,',6,',NULL,'fadya@gmail.com','0777',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,'2021-07-14 20:23:41'),
	(8,'mohamed','male',6,7,',20,',NULL,'mohamed@gmail.com','0888',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(9,'nona','female',6,7,'0',NULL,'nona@gmail.com','0999',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(10,'lolo','female',14,15,',1,',NULL,'lolo@gmail.com','010',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(11,'shosho','female',6,7,'0',NULL,'shosho@gmail.com','011',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(12,'posy','female',1,10,'0',NULL,'posy@gmail.com','012',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(13,'ali','male',1,10,'0',NULL,'ahmed@gmail.com','013',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(14,'abdulmeneam','male',0,0,',15,',NULL,'abdul@gmail.com','014',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(15,'safya','female',0,0,',14,',NULL,'safya@gmail.com','015',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(16,'chereen','female',0,0,',1,',NULL,'chereen@gmail.com','016',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(18,'ahmed','male',19,16,'0',NULL,'ahmed2@gmail.com','017',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(19,'abdulbar','male',0,0,'0',',16,','abdulbar@gmail.com','018',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(20,'hoda','female',0,0,',8,',NULL,'hoda@gmail.com','019',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(21,'elhossieny','male',0,0,',24,',NULL,'elhossieny@gmail.com','021',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(24,'sokar','female',0,0,',21,',NULL,'sokar@gmail.com','022',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(26,'belal','male',21,24,'',NULL,'belal@gmail.com','06667',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(27,'khaled',NULL,NULL,NULL,NULL,NULL,'elmasry.dev@gmail.com',NULL,NULL,'$2y$10$VIdgTcS878crKq/6NhsBROvZQSmeehe.NcUrynTbLz09A516hErQi',NULL,NULL,NULL,3,NULL,'2021-07-14 13:17:33','2021-07-14 13:17:33'),
	(28,'ee',NULL,NULL,NULL,NULL,NULL,'microsystems22@gmail.com',NULL,NULL,'$2y$10$zEG3sBCJXYqNvoAH.AJdQu5D07P4L0DLsn365cHABPoPet/QuYqyu',NULL,NULL,NULL,NULL,NULL,'2021-07-14 20:15:21','2021-07-14 20:15:21'),
	(29,'rr',NULL,NULL,NULL,NULL,NULL,'sohagy@gmail.com',NULL,NULL,'$2y$10$LiR32.Db4.MU0roxU5GNVu3xQMGrGNEhA8hdr.4ZfERBxlNlZlYhO',NULL,NULL,NULL,NULL,NULL,'2021-07-14 20:18:30','2021-07-14 20:18:30'),
	(31,'ff',NULL,NULL,NULL,NULL,NULL,'sohagy3@gmail.com',NULL,NULL,'$2y$10$iMsWEGmXYkZMCc4u9N17buzUpAE9sV9YcArtaQ55XS4DXC3yWri/q',NULL,NULL,NULL,NULL,NULL,'2021-07-14 20:21:55','2021-07-14 20:21:55'),
	(32,'dd',NULL,NULL,NULL,NULL,NULL,'hossam2@gmail.com',NULL,NULL,'$2y$10$wllOw4jlCoZFm5pDsATQmeoQyG1R1ZTm6RnqMZA2iVxBEwL.XlXNe',NULL,NULL,NULL,NULL,NULL,'2021-07-14 20:22:27','2021-07-14 20:22:27'),
	(33,'gg',NULL,NULL,NULL,NULL,NULL,'hhhh@gm.cc',NULL,NULL,'$2y$10$Q15cFNne5xLrIl2kzqkrXOlWrAS6k3X4xrXYIAQO3N21oZWo0oa7y',NULL,NULL,NULL,NULL,NULL,'2021-07-14 20:22:47','2021-07-14 20:22:47'),
	(34,'sss',NULL,35,NULL,NULL,NULL,'microsystems232@gmail.com',NULL,NULL,'$2y$10$DG1v7n7BUc6cfna1.W6.tetfrOqTI0HXewKNBv./ZySMOhZNtuo6a',NULL,NULL,NULL,NULL,NULL,'2021-07-14 20:23:41','2021-07-14 20:53:37'),
	(35,'demo',NULL,36,NULL,NULL,NULL,'hossam@test.com',NULL,NULL,'$2y$10$OwEIu.QHG7jrhUXb4eV41e.YTigaJ1.TlZ.825o1QGIELMiTBUbVC',NULL,NULL,NULL,NULL,NULL,'2021-07-14 20:53:37','2021-07-14 20:53:59'),
	(36,'ddddd',NULL,37,NULL,NULL,NULL,'hhhh@gm.ccd',NULL,NULL,'$2y$10$BSR8.9HBeDeP.GdtdFYGy.AKP5jQMUwqctWj2WX2NzAeavMoCa6l2',NULL,NULL,NULL,NULL,NULL,'2021-07-14 20:53:59','2021-07-14 20:55:36'),
	(37,'eeeeee',NULL,38,NULL,NULL,NULL,'microsyseetems2@gmail.com',NULL,NULL,'$2y$10$G9YhfP7mRBnRhGAKh92JUeHWFS7jrlZtHGm3hwhMdYdWKeEfyVH9i',NULL,NULL,NULL,NULL,NULL,'2021-07-14 20:55:36','2021-07-14 20:56:19'),
	(38,'d',NULL,39,NULL,NULL,NULL,'sohagddy@gmail.com',NULL,NULL,'$2y$10$jgS5sIyBDBMxPD/uqjNsUezDSzywBUXvQNtwFhy5Mt2kK.T22J.SC',NULL,NULL,NULL,NULL,NULL,'2021-07-14 20:56:19','2021-07-14 20:56:53'),
	(39,'demoee',NULL,NULL,NULL,NULL,NULL,'hossam@test.comeee',NULL,NULL,'$2y$10$6O89PfdGAa8sintZw5UjM.grMa4BGwoO8CLIqfUR01Sx8xifdWN.q',NULL,NULL,NULL,NULL,NULL,'2021-07-14 20:56:53','2021-07-14 20:56:53');

/*!40000 ALTER TABLE `users_ho` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
