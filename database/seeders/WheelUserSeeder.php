<?php

namespace Database\Seeders;

use App\Models\WheelUser;
use Illuminate\Database\Seeder;

class WheelUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        WheelUser::create([
            'user_id'=>1
        ]);
    }
}
