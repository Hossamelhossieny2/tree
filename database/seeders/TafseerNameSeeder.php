<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TafseerNameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Tafseer name table start seed!');

        $path = 'database/sql/tafseer_name.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Tafseer name table seeded ok!');
    }
}
