<?php

namespace Database\Seeders;

use App\Models\FamilyName;
use Illuminate\Database\Seeder;

class FamilyNamesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FamilyName::create([
            'id'=>1,
            'title'=>'Hossam(FAM)',
            'fam_cache' =>'a:1:{i:0;i:1;}',
            'members' => '1',
            'live' => '1',
            'male' => 1
        ]);
    }
}