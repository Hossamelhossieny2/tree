<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ManSay;

class ManSaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ManSay::create([
            'man_id'    => '1',
            'name'      => 'أفلاطون',
            'title'     => 'قليلٌ من العلمِ مع العملِ به، أنفع من كثيرٍ من العلم مع قلّة العمل به .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Plato',
            'title'     => 'A little knowledge with its action is more beneficial than a lot of knowledge with little action.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);    

         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Platon',
            'title'     => "Un peu de connaissance avec son action est plus bénéfique que beaucoup de connaissance avec peu d'action.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);    

         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Platon',
            'title'     => 'Az amel ile az ilim, az amel ile çok ilimden daha faydalıdır.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);    

         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Платон',
            'title'     => 'Немного знаний о его действии более полезно, чем много знаний при небольшом действии.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);    

         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Plato',
            'title'     => 'Ein wenig Wissen mit seinem Handeln ist vorteilhafter als viel Wissen mit wenig Handeln.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);    

         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Platón',
            'title'     => 'Un poco de conocimiento con su acción es más beneficioso que mucho conocimiento con poca acción.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);    

         ManSay::create([
            'man_id'    => '1',
            'name'      => '柏拉图',
            'title'     => '有行动的小知识比有小行动的大知识更有益。',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]);    

        ManSay::create([
            'man_id'    => '1',
            'name'      => 'أفلاطون',
            'title'     => 'لو أمطرت السماء حريّةً لرأيتَ بعضَ العبيدِ يحملونَ مظلّات .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Plato',
            'title'     => 'If it rained freely, you would see some slaves carrying umbrellas.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);    
         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Platon',
            'title'     => "S'il pleuvait librement, vous verriez des esclaves portant des parapluies.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);   
         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Platon',
            'title'     => 'Serbestçe yağmur yağsaydı, bazı kölelerin şemsiye taşıdığını görürdünüz.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);   
         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Платон',
            'title'     => 'Если бы шел дождь, вы бы увидели рабов с зонтиками.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);   
         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Plato',
            'title'     => 'Wenn es regnete, sah man einige Sklaven, die Regenschirme trugen.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);   
         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Platón',
            'title'     => 'Si llovía abundantemente, verías a algunos esclavos con paraguas.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);   
         ManSay::create([
            'man_id'    => '1',
            'name'      => '柏拉图',
            'title'     => '如果下雨，你会看到一些奴隶撑着雨伞。',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]);   

        ManSay::create([
            'man_id'    => '1',
            'name'      => 'أفلاطون',
            'title'     => 'إن تعِبتَ في الخيرِ فإنّ التّعَبَ يزولُ والخيرُ يبقى، وإن تلذّذتَ بالآثام فإنّ اللذة تزول والآثام تبقى .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Plato',
            'title'     => 'If you get tired of doing good, the fatigue goes away and the goodness stays, and if you take pleasure in sins, then the pleasure passes away and the sins remain.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);   
         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Platon',
            'title'     => "Si vous êtes fatigué de faire le bien, la fatigue s'en va et la bonté reste, et si vous prenez plaisir dans les péchés, alors le plaisir s'en va et les péchés restent.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Platon',
            'title'     => 'İyilik yapmaktan yorulursan yorgunluk gider, iyilik kalır, günahlardan zevk alırsan zevk geçer ve günahlar kalır.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Платон',
            'title'     => 'Если вы устали делать добро, усталость уходит, а добро остается, а если вы наслаждаетесь грехами, тогда удовольствие проходит, а грехи остаются.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Plato',
            'title'     => 'Wenn Sie müde werden, Gutes zu tun, verschwindet die Müdigkeit und das Gute bleibt, und wenn Sie Freude an Sünden haben, dann vergeht die Freude und die Sünden bleiben.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Platón',
            'title'     => 'Si te cansas de hacer el bien, la fatiga desaparece y la bondad permanece, y si te complaces en los pecados, entonces el placer pasa y los pecados permanecen.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '1',
            'name'      => '柏拉图',
            'title'     => '如果你厌倦了行善，那么疲劳就会消失，而善良就会存在；如果你以罪恶为乐，那么快乐就会消失，罪恶依然存在。',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '1',
            'name'      => 'أفلاطون',
            'title'     => 'تستطيع أن تعرف عن شخصٍ في ساعةِ لَعِبٍ أكثر ممّا يمكن أن تعرفه في ساعة حديث .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Plato',
            'title'     => 'You can know more about a person in an hour of playing than you can know in an hour of conversation.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Platon',
            'title'     => "Vous pouvez en savoir plus sur une personne en une heure de jeu que vous ne pouvez en savoir en une heure de conversation.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Platon',
            'title'     => 'Bir kişi hakkında bir saatlik oyunla, bir saatlik konuşmayla öğrenebileceğinizden daha fazlasını bilebilirsiniz.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Платон',
            'title'     => 'Вы можете узнать о человеке больше за час игры, чем за час разговора.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Plato',
            'title'     => 'Sie können in einer Stunde Spiel mehr über eine Person wissen als in einer Stunde Gespräch.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Platón',
            'title'     => 'Puedes saber más sobre una persona en una hora de juego de lo que puedes saber en una hora de conversación.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '1',
            'name'      => '柏拉图',
            'title'     => '在一个小时的游戏中你可以比在一个小时的谈话中了解更多的人。',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '1',
            'name'      => 'أفلاطون',
            'title'     => 'ما زال الصبر دواءنا جميعا .. و سيبقى الحب و الأمل عكازنا .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Plato',
            'title'     => 'Patience is still our cure.. Love and hope will remain our crutches.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Platon',
            'title'     => "La patience est toujours notre remède. L'amour et l'espoir resteront nos béquilles.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Platon',
            'title'     => 'Sabır hala bizim ilacımız.. Sevgi ve umut koltuk değneğimiz olarak kalacak.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Платон',
            'title'     => 'Терпение по-прежнему наше лекарство. Любовь и надежда останутся нашими костылями.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Plato',
            'title'     => 'Geduld ist immer noch unser Heilmittel. Liebe und Hoffnung werden unsere Krücken bleiben.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '1',
            'name'      => 'Platón',
            'title'     => 'La paciencia sigue siendo nuestra cura. El amor y la esperanza seguirán siendo nuestras muletas.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '1',
            'name'      => '柏拉图',
            'title'     => '耐心仍然是我们的治疗方法。爱和希望仍将是我们的拐杖。',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Head_Platon_Glyptothek_Munich_548.jpg/475px-Head_Platon_Glyptothek_Munich_548.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 

         ManSay::create([
            'man_id'    => '2',
            'name'      => 'سقراط',
            'title'     => 'ليس العاطل من لا يؤدي عملاً فقط ،العاطل من يؤدي عملاً في وسعه أني يؤدي أفضل منه .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '2',
            'name'      => 'Socrates',
            'title'     => 'The unemployed person is not the one who does not perform a job only, the unemployed person is the one who performs a job in his capacity that he does better than him.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);  
         ManSay::create([
            'man_id'    => '2',
            'name'      => 'Socrate',
            'title'     => "Le chômeur n'est pas celui qui n'exerce pas seulement un travail, le chômeur est celui qui effectue un travail en sa qualité qu'il fait mieux que lui.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);  
          
         ManSay::create([
            'man_id'    => '2',
            'name'      => 'Sokrates',
            'title'     => 'İşsiz sadece bir işi yapmayan kişi değildir, işsiz kişi bir işi kendi sıfatıyla kendisinden daha iyi yapan kişidir.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);  
         ManSay::create([
            'man_id'    => '2',
            'name'      => 'Сократ',
            'title'     => 'Безработный - это не тот, кто не выполняет только работу, безработный - это тот, кто выполняет работу в своем качестве, и делает это лучше, чем он сам.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);  
         ManSay::create([
            'man_id'    => '2',
            'name'      => 'Sokrates',
            'title'     => 'Der Arbeitslose ist nicht derjenige, der nicht nur eine Arbeit ausübt, der Arbeitslose ist derjenige, der eine Arbeit in seiner Eigenschaft ausübt, die er besser macht als er.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);  
         ManSay::create([
            'man_id'    => '2',
            'name'      => 'Sócrates',
            'title'     => 'El desempleado no es el que no solo realiza un trabajo, el desempleado es el que realiza un trabajo en su capacidad que lo hace mejor que él.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);  

         ManSay::create([
            'man_id'    => '2',
            'name'      => '苏格拉底',
            'title'     => '失业者不仅仅是不从事某项工作的人，失业者是以他比他做得更好的能力从事一项工作的人。',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 

        ManSay::create([
            'man_id'    => '2',
            'name'      => 'سقراط',
            'title'     => 'الحياة من دون تجربة لا تستحق أن تعاش .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '2',
            'name'      => 'Socrates',
            'title'     => 'Life without experience is not worth living.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);     
         ManSay::create([
            'man_id'    => '2',
            'name'      => 'Socrate',
            'title'     => "La vie sans expérience ne vaut pas la peine d'être vécue.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '2',
            'name'      => 'Sokrates',
            'title'     => 'Tecrübesiz hayat yaşamaya değmez.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '2',
            'name'      => 'Сократ',
            'title'     => 'Жизнь без опыта не стоит того.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '2',
            'name'      => 'Sokrates',
            'title'     => 'Ein Leben ohne Erfahrung ist nicht lebenswert.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '2',
            'name'      => 'Sócrates',
            'title'     => 'La vida sin experiencia no vale la pena vivirla.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '2',
            'name'      => '苏格拉底',
            'title'     => '没有经验的生活不值得过。',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 
          ManSay::create([
            'man_id'    => '2',
            'name'      => 'سقراط',
            'title'     => 'ليس من الضروري أن يكون كلامي مقبول ، من الضروري ان يكون صادقا .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '2',
            'name'      => 'Socrates',
            'title'     => 'My words do not have to be accepted, they need to be true.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);   
        ManSay::create([
            'man_id'    => '2',
            'name'      => 'Socrate',
            'title'     => "Mes paroles ne doivent pas être acceptées, elles doivent être vraies.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);   
        ManSay::create([
            'man_id'    => '2',
            'name'      => 'Sokrates',
            'title'     => 'Sözlerim kabul edilmek zorunda değil, doğru olmalı.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);   
        ManSay::create([
            'man_id'    => '2',
            'name'      => 'Сократ',
            'title'     => 'Мои слова не нужно принимать, они должны быть правдой.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);   
        ManSay::create([
            'man_id'    => '2',
            'name'      => 'Sokrates',
            'title'     => 'Meine Worte müssen nicht akzeptiert werden, sie müssen wahr sein.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);   
        ManSay::create([
            'man_id'    => '2',
            'name'      => 'Sócrates',
            'title'     => 'Mis palabras no tienen que ser aceptadas, deben ser verdaderas.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);   
        ManSay::create([
            'man_id'    => '2',
            'name'      => '苏格拉底',
            'title'     => '我的话不必被接受，它们必须是真实的。',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]);     

         ManSay::create([
            'man_id'    => '2',
            'name'      => 'سقراط',
            'title'     => 'النفس الخيرة يجزيها القليل من الأدب والنفس الشريرة لا ينفع فيها الأدب الكثير .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '2',
            'name'      => 'Socrates',
            'title'     => 'A good soul is rewarded with a little politeness, and an evil soul does not benefit from a lot of etiquette.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);     
         ManSay::create([
            'man_id'    => '2',
            'name'      => 'Socrate',
            'title'     => "Une bonne âme est récompensée par un peu de politesse, et une mauvaise âme ne bénéficie pas de beaucoup d'étiquette.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '2',
            'name'      => 'Sokrates',
            'title'     => 'İyi bir ruh biraz nezaketle ödüllendirilir ve kötü bir ruh çok fazla görgü kurallarından faydalanmaz.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '2',
            'name'      => 'Сократ',
            'title'     => 'Добрая душа награждается небольшой вежливостью, а злая душа не получает выгоды от большого этикета.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '2',
            'name'      => 'Sokrates',
            'title'     => 'Eine gute Seele wird mit ein wenig Höflichkeit belohnt und eine böse Seele profitiert nicht von viel Etikette.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '2',
            'name'      => 'Sócrates',
            'title'     => 'Un alma buena es recompensada con un poco de cortesía, y un alma malvada no se beneficia de mucha etiqueta.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '2',
            'name'      => '苏格拉底',
            'title'     => '善良的灵魂会得到一点礼貌的回报，而邪恶的灵魂不会受益于很多礼节。',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 

        ManSay::create([
            'man_id'    => '2',
            'name'      => 'سقراط',
            'title'     => 'المرة العظيمة هى التى تُعلمنا كيف نُحب عندما نريد أن نكره، وكيف نضحك عندما نريد أن نبكى، وكيف نبتسم عندما نتألم .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '2',
            'name'      => 'Socrates',
            'title'     => 'The great time is that which teaches us how to love when we want to hate, how to laugh when we want to cry, and how to smile when we are in pain.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '2',
            'name'      => 'Socrate',
            'title'     => 'Le grand moment est celui qui nous apprend à aimer quand on a envie de haïr, à rire quand on a envie de pleurer, et à sourire quand on a mal.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '2',
            'name'      => 'Sokrates',
            'title'     => 'Harika zaman, bize nefret etmek istediğimizde nasıl seveceğimizi, ağlamak istediğimizde nasıl güleceğimizi ve acı çektiğimizde nasıl gülümseyeceğimizi öğreten zamandır.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '2',
            'name'      => 'Сократ',
            'title'     => 'Прекрасное время - это то, что учит нас, как любить, когда мы хотим ненавидеть, как смеяться, когда мы хотим плакать, и как улыбаться, когда нам больно.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '2',
            'name'      => 'Sokrates',
            'title'     => 'Die große Zeit ist die, die uns lehrt, wie wir lieben, wenn wir hassen wollen, wie wir lachen, wenn wir weinen wollen und wie wir lächeln, wenn wir Schmerzen haben.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '2',
            'name'      => 'Sócrates',
            'title'     => 'El gran momento es aquel que nos enseña cómo amar cuando queremos odiar, cómo reír cuando queremos llorar y cómo sonreír cuando tenemos dolor.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '2',
            'name'      => '苏格拉底',
            'title'     => '伟大的时光教会我们在想恨时如何去爱，在想哭时如何笑，在痛苦时如何微笑。',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg/300px-Marcello_Bacciarelli_-_Alcibiades_Being_Taught_by_Socrates%2C_1776-77_crop.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]);     


        ManSay::create([
            'man_id'    => '3',
            'name'      => 'أرسطو',
            'title'     => 'أولئك الذين هم في ثورة الغضب؛ يفقدون كل سلطان على أنفسهم .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristotle',
            'title'     => 'those who are in a rage; They lose all authority over themselves.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);       
         ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristote',
            'title'     => "ceux qui sont en colère; Ils perdent toute autorité sur eux-mêmes.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristo',
            'title'     => 'öfke içinde olanlar; Kendileri üzerindeki tüm yetkilerini kaybederler.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '3',
            'name'      => 'Аристотель',
            'title'     => 'те, кто в ярости; Они теряют всякую власть над собой.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristoteles',
            'title'     => 'diejenigen, die wütend sind; Sie verlieren alle Autorität über sich selbst.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristóteles',
            'title'     => 'los que están furiosos; Pierden toda autoridad sobre sí mismos.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '3',
            'name'      => '亚里士多德',
            'title'     => '发怒的人；他们失去了对自己的所有权威。',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '3',
            'name'      => 'أرسطو',
            'title'     => 'ما نحن عليه، هو ما اعتدنا على تكراره، والمداومة عليه إذاً التفوق ليس فعل؛ بل عادة .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristotle',
            'title'     => 'What we are, is what we are accustomed to repeating, and to persevere in it, so excellence is not an act; but usually.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);     
         ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristote',
            'title'     => "Ce que nous sommes, c'est ce que nous avons l'habitude de répéter, et d'y persévérer, donc l'excellence n'est pas un acte ; mais habituellement.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristo',
            'title'     => 'Biz neysek, onu tekrar etmeye ve onda sebat etmeye alışmışızdır, bu yüzden mükemmellik bir eylem değildir; ama genellikle.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '3',
            'name'      => 'Аристотель',
            'title'     => 'То, что мы есть, - это то, что мы привыкли повторять и упорно делать это, так что совершенство - это не действие; но обычно.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristoteles',
            'title'     => 'Was wir sind, ist das, was wir zu wiederholen und zu beharren gewohnt sind, daher ist Exzellenz keine Tat; aber normalerweise.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristóteles',
            'title'     => 'Lo que somos, es lo que estamos acostumbrados a repetir, ya perseverar en ello, por eso la excelencia no es un acto; pero usualmente.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '3',
            'name'      => '亚里士多德',
            'title'     => '我们是什么，就是我们习惯于重复的，并持之以恒，所以卓越不是一种行为；但通常。',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '3',
            'name'      => 'أرسطو',
            'title'     => 'ليست الشجاعة أن تقول ما تعتقد، إنّما الشجاعة أن تعتقد كل ما تقول .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristotle',
            'title'     => 'It is not the courage to say what you think, but the courage to believe everything you say.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);    
         ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristote',
            'title'     => "Ce n'est pas le courage de dire ce que vous pensez, mais le courage de croire tout ce que vous dites.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristo',
            'title'     => 'Düşündüğünü söyleme cesareti değil, söylediğin her şeye inanma cesaretidir.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '3',
            'name'      => 'Аристотель',
            'title'     => 'Это не смелость говорить то, что вы думаете, а смелость верить всему, что вы говорите.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristoteles',
            'title'     => 'Es ist nicht der Mut zu sagen, was man denkt, sondern der Mut, alles zu glauben, was man sagt.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristóteles',
            'title'     => 'No es el coraje de decir lo que piensas, sino el coraje de creer todo lo que dices.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '3',
            'name'      => '亚里士多德',
            'title'     => '不是说出你所想的勇气，而是相信你所说一切的勇气。',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '3',
            'name'      => 'أرسطو',
            'title'     => 'سئل من يصنع الطغاة ؟  فرد قائلاً ضعف المظلومين .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristotle',
            'title'     => 'He was asked who makes tyrants? He replied, "The weak of the oppressed."',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristote',
            'title'     => "On lui a demandé qui fait les tyrans ? Il répondit : Le faible des opprimés.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristo',
            'title'     => "Tiranları kim yapar diye soruldu. 'Mazlumun zayıfı' diye cevap verdi.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '3',
            'name'      => 'Аристотель',
            'title'     => 'Его спросили, кто делает тиранов? Он ответил: "Слабые из угнетенных".',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristoteles',
            'title'     => "Er wurde gefragt, wer Tyrannen macht? Er antwortete: Der Schwache der Unterdrückten.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristóteles',
            'title'     => "Se le preguntó ¿quién crea tiranos? Él respondió: El débil de los oprimidos.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '3',
            'name'      => '亚里士多德',
            'title'     => "他被问到谁做暴君？他回答说，被压迫者的弱者。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]);   

         ManSay::create([
            'man_id'    => '3',
            'name'      => 'أرسطو',
            'title'     => 'لا تترك الحق، لأنّك متى تركت الحق، فإنّك لا تتركه إلّا إلى الباطل .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristotle',
            'title'     => 'Do not leave the truth, because when you leave the truth, you will only leave it to falsehood.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristote',
            'title'     => "Ne laissez pas la vérité, car lorsque vous quittez la vérité, vous ne la laisserez qu'au mensonge.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristo',
            'title'     => "Hakkı terk etme, çünkü hakikati terk ettiğinde, onu ancak batıla terk edersin.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '3',
            'name'      => 'Аристотель',
            'title'     => "Не оставляйте истину, потому что, когда вы оставите истину, вы оставите ее только лжи.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristoteles',
            'title'     => "Überlasse nicht die Wahrheit, denn wenn du die Wahrheit verlässt, überlässt du sie nur der Lüge.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '3',
            'name'      => 'Aristóteles',
            'title'     => "No dejes la verdad, porque cuando dejas la verdad, solo la dejarás a la falsedad.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '3',
            'name'      => '亚里士多德',
            'title'     => "不要离开真相，因为当你离开真相时，你只会把它留给虚假。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg/541px-Rembrandt_-_Aristotle_with_a_Bust_of_Homer_-_WGA19232.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '4',
            'name'      => 'الدكتور مصطفي محمود',
            'title'     => 'السعادة ليست في المال أو القوة أو السلطة، بل فيما نفعله بالمال والقوة والسلطة .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '4',
            'name'      => 'Dr. Mustafa Mahmoud',
            'title'     => 'Happiness is not in money, power, or power, but in what we do with money, power, and power.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);     
          ManSay::create([
            'man_id'    => '4',
            'name'      => 'Dr. Mostafa Mahmoud',
            'title'     => "",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]); 
           ManSay::create([
            'man_id'    => '4',
            'name'      => 'Dr. Mustafa Mahmud',
            'title'     => "",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]); 
            ManSay::create([
            'man_id'    => '4',
            'name'      => 'Доктор Мостафа Махмуд',
            'title'     => "",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]); 
             ManSay::create([
            'man_id'    => '4',
            'name'      => 'DR. Mostafa Mahmoud',
            'title'     => "",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]); 
              ManSay::create([
            'man_id'    => '4',
            'name'      => 'Dr. Mostafa Mahmoud',
            'title'     => "",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]); 
               ManSay::create([
            'man_id'    => '4',
            'name'      => '博士。穆斯塔法·马哈茂德',
            'title'     => "",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '4',
            'name'      => 'الدكتور مصطفي محمود',
            'title'     => 'ابدأ بنفسك وأصلح ذاتك، بدلًا من الجلوس على كرسي الفتاوى واتهام الآخرين .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '4',
            'name'      => 'Dr. Mustafa Mahmoud',
            'title'     => 'Start with yourself and fix yourself, instead of sitting on the fatwa chair and accusing others.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]); 
          ManSay::create([
            'man_id'    => '4',
            'name'      => 'Dr. Mostafa Mahmoud',
            'title'     => "",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'fr',
         ]); 
          ManSay::create([
            'man_id'    => '4',
            'name'      => 'Dr. Mustafa Mahmud',
            'title'     => "",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]); 
            ManSay::create([
            'man_id'    => '4',
            'name'      => 'Доктор Мостафа Махмуд',
            'title'     => "",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]); 
             ManSay::create([
            'man_id'    => '4',
            'name'      => 'DR. Mostafa Mahmoud',
            'title'     => "",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]); 
              ManSay::create([
            'man_id'    => '4',
            'name'      => 'Dr. Mostafa Mahmoud',
            'title'     => "",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]); 
               ManSay::create([
            'man_id'    => '4',
            'name'      => '博士。穆斯塔法·马哈茂德',
            'title'     => "",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '4',
            'name'      => 'الدكتور مصطفي محمود',
            'title'     => 'ما دُنيانا إلا عطش بلا ارتواء، وجوع بلا شبع،  وتعب بلا راحة، وحطب يأكل نفسه. وهي بدون إيمان: خواء، وخراب، وظلمة، وتيه، وسعي في لا شئ .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '4',
            'name'      => 'Dr. Mustafa Mahmoud',
            'title'     => 'Our world is nothing but thirst without satiation, hunger without satiation, fatigue without rest, and wood that eats itself. And it is without faith: emptiness, ruin, darkness, wandering, and striving for nothing.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);  
          ManSay::create([
            'man_id'    => '4',
            'name'      => 'Dr. Mostafa Mahmoud',
            'title'     => "Notre monde n'est que soif sans satiété, faim sans satiété, fatigue sans repos et bois qui se mange. Et c'est sans foi : le vide, la ruine, les ténèbres, l'errance et l'effort pour rien.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'fr',
         ]);

          ManSay::create([
            'man_id'    => '4',
            'name'      => 'Dr. Mustafa Mahmud',
            'title'     => "Dünyamız doymayan susuzluktan, doymayan açlıktan, dinlenmeyen yorgunluktan ve kendini yiyen odundan başka bir şey değildir. Ve inançsızdır: boşluk, harabe, karanlık, dolaşma ve boşuna çabalama.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]); 
            ManSay::create([
            'man_id'    => '4',
            'name'      => 'Доктор Мостафа Махмуд',
            'title'     => "Наш мир - не что иное, как жажда без насыщения, голод без насыщения, усталость без отдыха и дерево, которое поедает само себя. И это без веры: пустота, разорение, тьма, блуждание и стремление ни к чему.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]); 
             ManSay::create([
            'man_id'    => '4',
            'name'      => 'DR. Mostafa Mahmoud',
            'title'     => "Unsere Welt ist nichts als Durst ohne Sättigung, Hunger ohne Sättigung, Müdigkeit ohne Ruhe und Holz, das sich selbst frisst. Und es ist ohne Glauben: Leere, Verderben, Finsternis, Wandern und Streben nach Nichts.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]); 
              ManSay::create([
            'man_id'    => '4',
            'name'      => 'Dr. Mostafa Mahmoud',
            'title'     => "Nuestro mundo no es más que sed sin saciedad, hambre sin saciedad, fatiga sin descanso y madera que se come a sí misma. Y es sin fe: vacío, ruina, oscuridad, vagabundeo y lucha por nada.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]); 
               ManSay::create([
            'man_id'    => '4',
            'name'      => '博士。穆斯塔法·马哈茂德',
            'title'     => "我们的世界不过是口渴而不饱，饥而不饱，疲倦而不休息，木头自食。它是没有信仰的：空虚、毁灭、黑暗、流浪和无所求。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '4',
            'name'      => 'الدكتور مصطفي محمود',
            'title'     => 'يظهر الإنسان على حقيقته حينما يُحرم مما يحب، ويُحمل على ما يكره .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '4',
            'name'      => 'Dr. Mustafa Mahmoud',
            'title'     => 'A person appears as he is when he is deprived of what he loves and is made to do what he hates.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);    
          ManSay::create([
            'man_id'    => '4',
            'name'      => 'Dr. Mostafa Mahmoud',
            'title'     => "Une personne apparaît telle qu'elle est lorsqu'elle est privée de ce qu'elle aime et qu'on la fait faire ce qu'elle déteste.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'fr',
         ]);
          ManSay::create([
            'man_id'    => '4',
            'name'      => 'Dr. Mustafa Mahmud',
            'title'     => "İnsan sevdiğinden mahrum kaldığında ve nefret ettiği şeyi yaptırdığında olduğu gibi görünür.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]); 
            ManSay::create([
            'man_id'    => '4',
            'name'      => 'Доктор Мостафа Махмуд',
            'title'     => "Человек появляется таким, какой он есть, когда его лишают того, что он любит, и заставляют делать то, что он ненавидит.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]); 
             ManSay::create([
            'man_id'    => '4',
            'name'      => 'DR. Mostafa Mahmoud',
            'title'     => "Ein Mensch erscheint, wie er ist, wenn ihm das, was er liebt, vorenthalten wird und er gezwungen wird, das zu tun, was er hasst.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]); 
              ManSay::create([
            'man_id'    => '4',
            'name'      => 'Dr. Mostafa Mahmoud',
            'title'     => "Una persona aparece como es cuando se le priva de lo que ama y se le obliga a hacer lo que odia.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]); 
               ManSay::create([
            'man_id'    => '4',
            'name'      => '博士。穆斯塔法·马哈茂德',
            'title'     => "当一个人被剥夺了他喜欢的东西而被迫做他讨厌的事情时，他就会表现出他的真实面貌。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '4',
            'name'      => 'الدكتور مصطفي محمود',
            'title'     => 'ليس عيبًا أن نكون فقراء، لكنها جريمة أن نصبح مُتخلفين .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '4',
            'name'      => 'Dr. Mustafa Mahmoud',
            'title'     => 'It is not a shame to be poor, but it is a crime to become backward.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);   
          ManSay::create([
            'man_id'    => '4',
            'name'      => 'Dr. Mostafa Mahmoud',
            'title'     => "Ce n'est pas une honte d'être pauvre, mais c'est un crime de devenir arriéré.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'fr',
         ]);
          
          ManSay::create([
            'man_id'    => '4',
            'name'      => 'Dr. Mustafa Mahmud',
            'title'     => "Fakir olmak ayıp değil, geri kalmak suçtur.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]); 
            ManSay::create([
            'man_id'    => '4',
            'name'      => 'Доктор Мостафа Махмуд',
            'title'     => "Быть бедным не стыдно, но стать отсталым - преступление.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]); 
             ManSay::create([
            'man_id'    => '4',
            'name'      => 'DR. Mostafa Mahmoud',
            'title'     => "Es ist keine Schande, arm zu sein, aber es ist ein Verbrechen, rückständig zu werden.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]); 
              ManSay::create([
            'man_id'    => '4',
            'name'      => 'Dr. Mostafa Mahmoud',
            'title'     => "No es una vergüenza ser pobre, pero es un crimen volverse atrasado.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]); 
               ManSay::create([
            'man_id'    => '4',
            'name'      => '博士。穆斯塔法·马哈茂德',
            'title'     => "贫穷并不可耻，但落后是犯罪。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/5/5f/%D9%85%D8%B5%D8%B7%D9%81%D9%89_%D9%85%D8%AD%D9%85%D9%88%D8%AF_%D9%81%D9%8A_%D8%A7%D9%84%D8%AA%D9%84%D9%81%D8%B2%D9%8A%D9%88%D9%86_%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 

         ManSay::create([
            'man_id'    => '5',
            'name'      => 'الشيخ الشعراوي',
            'title'     => 'إذا لم تجد لك حاقداً فاعلم أنك إنسان فاشل .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '5',
            'name'      => 'Sheikh Al Shaarawy',
            'title'     => 'If you do not find a hater, then know that you are a failure.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]); 
          ManSay::create([
            'man_id'    => '5',
            'name'      => "Cheikh Al Shaarawy",
            'title'     => "Si vous ne trouvez pas de haineux, sachez que vous êtes un échec.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]); 
           ManSay::create([
            'man_id'    => '5',
            'name'      => "Şeyh El Shaarawy",
            'title'     => "Bir düşman bulamazsan, o zaman başarısız olduğunu bil.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]); 
            ManSay::create([
            'man_id'    => '5',
            'name'      => "Шейх Аль-Шаарави",
            'title'     => "Если вы не найдете ненавистника, знайте, что вы неудачник.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]); 
             ManSay::create([
            'man_id'    => '5',
            'name'      => "Scheich Al Shaarawy",
            'title'     => "Wenn Sie keinen Hasser finden, dann wissen Sie, dass Sie ein Versager sind.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]); 
              ManSay::create([
            'man_id'    => '5',
            'name'      => "Sheikh Al Shaarawy",
            'title'     => "Si no encuentra un enemigo, sepa que es un fracaso.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]); 
               ManSay::create([
            'man_id'    => '5',
            'name'      => "谢赫·沙拉维",
            'title'     => "如果你没有找到仇敌，那就知道你是个失败者。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 

         ManSay::create([
            'man_id'    => '5',
            'name'      => 'الشيخ الشعراوي',
            'title'     => 'إذا رأيت فقيراً فى بلاد المسلمين؛ فاعلم أن هناك غنياً سرق ماله .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '5',
            'name'      => 'Sheikh Al Shaarawy',
            'title'     => 'If you see a poor in Muslim countries; Know that there is a rich man who stole his money.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]); 
          ManSay::create([
            'man_id'    => '5',
            'name'      => "Cheikh Al Shaarawy",
            'title'     => "Si vous voyez un pauvre dans les pays musulmans ; Sachez qu'il y a un homme riche qui a volé son argent.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]); 
           ManSay::create([
            'man_id'    => '5',
            'name'      => "Şeyh El Shaarawy",
            'title'     => "Müslüman ülkelerde bir fakir görürseniz; Bilin ki parasını çalan zengin bir adam var.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]); 
            ManSay::create([
            'man_id'    => '5',
            'name'      => "Шейх Аль-Шаарави",
            'title'     => "Если вы видите бедных в мусульманских странах; Знайте, что есть богатый человек, который украл его деньги.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]); 
             ManSay::create([
            'man_id'    => '5',
            'name'      => "Scheich Al Shaarawy",
            'title'     => "Wenn Sie in muslimischen Ländern einen Armen sehen; Wisse, dass es einen reichen Mann gibt, der sein Geld gestohlen hat.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]); 
              ManSay::create([
            'man_id'    => '5',
            'name'      => "Sheikh Al Shaarawy",
            'title'     => "Si ves a un pobre en los países musulmanes; Sepa que hay un hombre rico que le robó su dinero.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]); 
               ManSay::create([
            'man_id'    => '5',
            'name'      => "谢赫·沙拉维",
            'title'     => "如果你在穆斯林国家看到一个穷人；要知道有一个有钱人偷了他的钱。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 

         ManSay::create([
            'man_id'    => '5',
            'name'      => 'الشيخ الشعراوي',
            'title'     => 'لا يقلق من كان له أب ، فكيف بمن كان له رب .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '5',
            'name'      => 'Sheikh Al Shaarawy',
            'title'     => 'He who has a father does not worry, so how about someone who has a Lord.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);  
          ManSay::create([
            'man_id'    => '5',
            'name'      => "Cheikh Al Shaarawy",
            'title'     => "Celui qui a un père ne s'inquiète pas, alors qu'en est-il de quelqu'un qui a un Seigneur.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]); 
           ManSay::create([
            'man_id'    => '5',
            'name'      => "Şeyh El Shaarawy",
            'title'     => "Babası olan dert etmez, peki ya Rabbi olana.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]); 
            ManSay::create([
            'man_id'    => '5',
            'name'      => "Шейх Аль-Шаарави",
            'title'     => "Тот, у кого есть отец, не волнуется, а как насчет того, у кого есть Господь.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]); 
             ManSay::create([
            'man_id'    => '5',
            'name'      => "Scheich Al Shaarawy",
            'title'     => "Wer einen Vater hat, macht sich keine Sorgen, wie wäre es also mit jemandem, der einen Herrn hat.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]); 
              ManSay::create([
            'man_id'    => '5',
            'name'      => "Sheikh Al Shaarawy",
            'title'     => "El que tiene un padre no se preocupa, entonces, ¿qué hay de alguien que tiene un Señor?",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]); 
               ManSay::create([
            'man_id'    => '5',
            'name'      => "谢赫·沙拉维",
            'title'     => "有父不愁，有主又何妨。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 

         ManSay::create([
            'man_id'    => '5',
            'name'      => 'الشيخ الشعراوي',
            'title'     => 'لا تعبدوا الله ليعطي، بل اعبدوه ليرضى، فإن رضي أدهشكم بعطائه .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '5',
            'name'      => 'Sheikh Al Shaarawy',
            'title'     => 'Do not worship God in order to give, but worship Him so that He is pleased, and if He is pleased, He will amaze you with His giving.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);  
          ManSay::create([
            'man_id'    => '5',
            'name'      => "Cheikh Al Shaarawy",
            'title'     => "N'adorez pas Dieu pour donner, mais adorez-le pour qu'il soit satisfait, et s'il est satisfait, il vous étonnera par son don.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]); 
           ManSay::create([
            'man_id'    => '5',
            'name'      => "Şeyh El Shaarawy",
            'title'     => "Allah'a vermek için değil, O'na razı olmak için kulluk edin ve eğer O razı olursa, vermesiyle sizi şaşırtacaktır.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]); 
            ManSay::create([
            'man_id'    => '5',
            'name'      => "Шейх Аль-Шаарави",
            'title'     => "Не поклоняйтесь Богу, чтобы дать, но поклоняйтесь Ему, чтобы Он был доволен, и, если Он доволен, Он поразит вас Своей отдачей.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]); 
             ManSay::create([
            'man_id'    => '5',
            'name'      => "Scheich Al Shaarawy",
            'title'     => "Bete Gott nicht an, um zu geben, sondern bete ihn an, damit er zufrieden ist, und wenn er zufrieden ist, wird er dich mit seinem Geben in Erstaunen versetzen.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]); 
              ManSay::create([
            'man_id'    => '5',
            'name'      => "Sheikh Al Shaarawy",
            'title'     => "No adores a Dios para dar, sino adóralo para que Él esté complacido, y si Él está complacido, te sorprenderá con Su ofrenda.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]); 
               ManSay::create([
            'man_id'    => '5',
            'name'      => "谢赫·沙拉维",
            'title'     => "不要为了施舍而敬拜神，而是要敬拜祂使祂喜悦，如果祂喜悦，祂的施舍会让你感到惊奇。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 

         ManSay::create([
            'man_id'    => '5',
            'name'      => 'الشيخ الشعراوي',
            'title'     => 'الذي لا يملك ضربة فأسه لا يملك قرار رأسه .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '5',
            'name'      => 'Sheikh Al Shaarawy',
            'title'     => 'He who does not have the right to strike his axe does not have the power of his head.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]); 
          ManSay::create([
            'man_id'    => '5',
            'name'      => "Cheikh Al Shaarawy",
            'title'     => "Celui qui n'a pas le droit de frapper sa hache n'a pas le pouvoir de sa tête.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]); 
           ManSay::create([
            'man_id'    => '5',
            'name'      => "Şeyh El Shaarawy",
            'title'     => "Baltasını vurmaya hakkı olmayanın, başının gücü de yoktur.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]); 
            ManSay::create([
            'man_id'    => '5',
            'name'      => "Шейх Аль-Шаарави",
            'title'     => "Тот, кто не имеет права бить топором, не имеет силы головы.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]); 
             ManSay::create([
            'man_id'    => '5',
            'name'      => "Scheich Al Shaarawy",
            'title'     => "Wer nicht das Recht hat, seine Axt zu schlagen, hat nicht die Kraft seines Kopfes.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]); 
              ManSay::create([
            'man_id'    => '5',
            'name'      => "Sheikh Al Shaarawy",
            'title'     => "El que no tiene derecho a golpear con el hacha no tiene el poder de la cabeza.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]); 
               ManSay::create([
            'man_id'    => '5',
            'name'      => "谢赫·沙拉维",
            'title'     => "没有砍斧权的人，就没有脑袋的力量。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/d/da/Sheik_Sharawy.jpg',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 

         ManSay::create([
            'man_id'    => '6',
            'name'      => 'الحسن البصري',
            'title'     => 'إن المؤمن في الدنيا غريب لا يجزع من ذلها ولا ينافس أهلها في عزها .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => 'The believer in this world is a stranger who does not panic from its humiliation and does not compete with its people in its glory.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => "Le croyant en ce monde est un étranger qui ne panique pas de son humiliation et ne rivalise pas avec son peuple dans sa gloire.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => "Bu dünyada mümin, onun alçalmasından korkmayan ve şanında halkıyla rekabet etmeyen bir yabancıdır.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Хасан Аль Басри',
            'title'     => "Верующий в этом мире - пришелец, который не паникует от его унижения и не соревнуется с его людьми в его славе.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => "Der Gläubige in dieser Welt ist ein Fremder, der vor seiner Erniedrigung nicht in Panik gerät und in seiner Herrlichkeit nicht mit seinem Volk konkurriert.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => "El creyente en este mundo es un extraño que no entra en pánico por su humillación y no compite con su gente en su gloria.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => '哈桑·阿尔巴斯里',
            'title'     => "这个世界的信徒是个陌生人，不因屈辱而惊慌，不与人争光。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'الحسن البصري',
            'title'     => 'يا ابن آدم إنما أنت أيام كلما ذهب يوم ذهب بعضك .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => 'O son of Adam, you are only days. Whenever a day goes, some of you are gone.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => "fils d'Adam, tu n'es que des jours. Chaque fois qu'un jour passe, certains d'entre vous sont partis.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => "Ey Âdemoğlu, sizler günlersiniz, gün geçince bazılarınız gitmiş olur.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Хасан Аль Басри',
            'title'     => "О сын Адама, ты всего лишь дни. Каждый день, когда проходит день, некоторые из вас уходят.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => "Oh Sohn Adams, du bist nur Tage, und wenn ein Tag vergeht, sind einige von dir weg.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => "Oh hijo de Adán, eres solo unos días, cada vez que pasa un día, algunos de ustedes se han ido.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => '哈桑·阿尔巴斯里',
            'title'     => "哦亚当之子，你们只是几天。每当一天过去，你们中的一些人就走了。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'الحسن البصري',
            'title'     => 'ما رأيت مثل النار نام هاربها، ولا مثل الجنة نام طالبها .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => 'I have not seen the likeness of fire whose fugitive sleeps, nor the likeness of heaven whose seeker sleeps.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);  
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => "Je n'ai pas vu la ressemblance du feu dont le fugitif dort, ni la ressemblance du ciel dont le chercheur dort.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => "Ben, firarisi uyuyan ateşin suretini, arayanı uyuyan cennetin suretini görmedim.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Хасан Аль Басри',
            'title'     => "Я не видел подобия огня, которого спит беглец, и подобия неба, которого спит ищущий.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => "Ich habe weder das Bild des Feuers gesehen, dessen Flüchtling schläft, noch das Bild des Himmels, dessen Suchender schläft.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => "No he visto semejanza de fuego cuyo fugitivo duerme, ni semejanza de cielo cuyo buscador duerme.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => '哈桑·阿尔巴斯里',
            'title'     => "我没见过逃亡者沉睡的火像，也未见过寻道者沉睡的天堂。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'الحسن البصري',
            'title'     => 'من خاف الله أخاف الله منه كل شيء، ومن خاف الناس أخافه الله من كل شيء .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => 'Whoever fears God, God will fear everything from him, and whoever fears people, God will fear him from everything.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => "Quiconque craint Dieu, Dieu craindra tout de lui, et quiconque craint les gens, Dieu craindra tout de lui.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => "Kim Allah'tan korkarsa Allah ondan her şeyden korkar ve kim insanlardan korkarsa Allah ondan her şeyden korkar.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Хасан Аль Басри',
            'title'     => "Кто боится Бога, Бог будет бояться всего от него, а кто боится людей, Бог будет бояться всего от него.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => "Wer Gott fürchtet, fürchtet Gott alles von ihm, und wer die Menschen fürchtet, fürchtet Gott alles von ihm.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => "Quien teme a Dios, Dios temerá todo de él, y quien teme a la gente, Dios temerá todo de él.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => '哈桑·阿尔巴斯里',
            'title'     => "凡敬畏神的，神就凡事都怕他；凡怕人的，神就凡事都怕他。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'الحسن البصري',
            'title'     => 'بئس الرفيقان : الدينار والدرهم، لا ينفعانك حتى يفارقاك .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => 'Miserable companions: the dinar and the dirham, they do not benefit you until they leave you.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => "Misérables compagnons : le dinar et le dirham, ils ne vous profitent que lorsqu'ils vous quittent.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => "Zavallı yoldaşlar: Dinar ve dirhem, sizden ayrılıncaya kadar size bir fayda sağlamaz.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Хасан Аль Басри',
            'title'     => "Несчастные товарищи: динар и дирхам, они не принесут вам пользы, пока не оставят вас.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => "Elende Gefährten: der Dinar und der Dirham, sie nützen dir nicht, bis sie dich verlassen.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => 'Hassan Al Basri',
            'title'     => "Compañeros miserables: el dinar y el dirham, no te benefician hasta que te dejan.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '6',
            'name'      => '哈桑·阿尔巴斯里',
            'title'     => "悲惨的同伴：第纳尔和迪拉姆，它们在离开你之前不会给你带来好处。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png/560px-%D8%A7%D9%84%D8%AD%D8%B3%D9%86_%D8%A7%D9%84%D8%A8%D8%B5%D8%B1%D9%8A.png',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '7',
            'name'      => 'الإمام مالك',
            'title'     => 'ما زهد أحد في الدنيا إلا أنطقه الله بالحكمه .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '7',
            'name'      => 'Imam Malik',
            'title'     => 'No one is ascetic in the world except that God pronounces him with wisdom.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => 'Imam Malik',
            'title'     => "Nul n'est ascète au monde si ce n'est que Dieu le prononce avec sagesse.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => 'İmam Malik',
            'title'     => "Allah onu hikmetle zikretmedikçe dünyada kimse münzevi değildir.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => 'Имам Малик',
            'title'     => "В мире нет аскетов, кроме тех, которые Бог произносит с мудростью.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => 'Imam Malik',
            'title'     => "Niemand auf der Welt ist asketisch, außer dass Gott ihn mit Weisheit ausspricht.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => 'Imam Malik',
            'title'     => "Nadie es ascético en el mundo a menos que Dios lo pronuncie con sabiduría.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => '伊玛目马利克',
            'title'     => "世上没有人是苦行者，除非上帝以智慧宣告他。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]);  

         ManSay::create([
            'man_id'    => '7',
            'name'      => 'الإمام مالك',
            'title'     => 'ليس العلم بكثرة الرواية ، إنما هو نور يضعه الله في القلب.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '7',
            'name'      => 'Imam Malik',
            'title'     => 'Knowledge is not an abundance of narration, but rather it is a light that God places in the heart.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);  
         ManSay::create([
            'man_id'    => '7',
            'name'      => 'Imam Malik',
            'title'     => "La connaissance n'est pas une abondance de narration, mais plutôt une lumière que Dieu place dans le cœur.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => 'İmam Malik',
            'title'     => "İlim, anlatı bolluğu değil, Allah'ın kalbe yerleştirdiği bir nurdur.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => 'Имам Малик',
            'title'     => "Знание - это не изобилие повествований, а, скорее, свет, который Бог помещает в сердце.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => 'Imam Malik',
            'title'     => "Wissen ist keine Fülle von Erzählungen, sondern ein Licht, das Gott ins Herz legt.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => 'Imam Malik',
            'title'     => "El conocimiento no es una abundancia de narración, sino una luz que Dios pone en el corazón.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => '伊玛目马利克',
            'title'     => "知识不是大量的叙述，而是上帝放在心中的一盏灯。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '7',
            'name'      => 'الإمام مالك',
            'title'     => 'من علم أن قوله من عمله قل كلامه .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '7',
            'name'      => 'Imam Malik',
            'title'     => 'Whoever knows that his words are from his actions, say his words.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '7',
            'name'      => 'Imam Malik',
            'title'     => "Quiconque sait que ses paroles viennent de ses actes, dis ses paroles.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => 'İmam Malik',
            'title'     => "Sözünün amelinden olduğunu bilen, sözünü söylesin.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => 'Имам Малик',
            'title'     => "Кто знает, что его слова основаны на его действиях, скажите его слова.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => 'Imam Malik',
            'title'     => "Wer weiß, dass seine Worte von seinen Taten stammen, der sagt seine Worte.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => 'Imam Malik',
            'title'     => "Quien sepa que sus palabras provienen de sus acciones, diga sus palabras.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => '伊玛目马利克',
            'title'     => "谁知道他的话来自他的行动，就说他的话。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '7',
            'name'      => 'الإمام مالك',
            'title'     => 'لا يصلح المرء حتى يترك ما لا يعنيه ويشتغل بما يعنيه فإذا كان كذلك أوشك أن يفتح الله تعالى قلبه له .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '7',
            'name'      => 'Imam Malik',
            'title'     => 'A person is not fit until he leaves what does not concern him and works with what does concern him. If that is the case, God Almighty will soon open his heart to him.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '7',
            'name'      => 'Imam Malik',
            'title'     => "Une personne n'est pas apte tant qu'elle ne quitte pas ce qui ne la concerne pas et travaille avec ce qui la concerne. Si tel est le cas, Dieu Tout-Puissant lui ouvrira bientôt son cœur.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => 'İmam Malik',
            'title'     => "İnsan, kendisini ilgilendirmeyeni bırakıp, kendisini ilgilendirenle uğraşmadıkça fit değildir.Eğer öyleyse, Cenab-ı Hak ona kalbini açar.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => 'Имам Малик',
            'title'     => "Человек не годится до тех пор, пока он не оставит то, что его не волнует, и не займется тем, что его волнует. Если это так, Всемогущий Бог вскоре откроет ему свое сердце.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => 'Imam Malik',
            'title'     => "Ein Mensch ist erst dann fit, wenn er das verlässt, was ihn nicht betrifft, und mit dem arbeitet, was ihn betrifft, und dann wird ihm der allmächtige Gott bald sein Herz öffnen.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => 'Imam Malik',
            'title'     => "Una persona no está en forma hasta que deja lo que no le concierne y trabaja con lo que sí le preocupa. Si ese es el caso, Dios Todopoderoso pronto le abrirá su corazón.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => '伊玛目马利克',
            'title'     => "一个人只有放下不关心的事，做关心的事，才能适应，这样的话，全能的神很快就会向他敞开心扉。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '7',
            'name'      => 'الإمام مالك',
            'title'     => 'لا خير فيمن يرى نفسه في حال لا يراه الناس لها أهلا.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '7',
            'name'      => 'Imam Malik',
            'title'     => 'There is no good in the one who sees himself when people do not see him as worthy.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);  
         ManSay::create([
            'man_id'    => '7',
            'name'      => 'Imam Malik',
            'title'     => "Il n'y a rien de bon chez celui qui se voit quand les gens ne le voient pas comme digne.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => 'İmam Malik',
            'title'     => "İnsanlar onu lâyık görmezken kendini gören kimsede hayır yoktur.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => 'Имам Малик',
            'title'     => "Нет добра в том, кто видит себя, когда люди не считают его достойным.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => 'Imam Malik',
            'title'     => "Es gibt nichts Gutes in dem, der sich selbst sieht, wenn die Leute ihn nicht als würdig ansehen.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => 'Imam Malik',
            'title'     => "No hay nada bueno en el que se ve a sí mismo cuando la gente no lo ve como digno.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]); 
        ManSay::create([
            'man_id'    => '7',
            'name'      => '伊玛目马利克',
            'title'     => "当人们不认为他有价值时，他认为自己没有好处。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/0/04/Malik_Bin_Anas_Name.png',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]);
         ManSay::create([
            'man_id'    => '8',
            'name'      => 'الإمام الشافعي',
            'title'     => 'تموت الأسد في الغابات جوعًا… ولحم الضأن تأكله الكــلاب.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => 'The lion dies in the forests of starvation...and the lamb is eaten by the dogs.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);   
         ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => "Le lion meurt dans les forêts de famine... et le mouton est mangé par les chiens.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => "Aslan ormanlarda açlıktan ölür... ve koyun eti köpekler tarafından yenir.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => 'аль-Эмам аш-Шафи',
            'title'     => "Лев умирает в лесах от голода ... а баранину съедают собаки.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => "Der Löwe verhungert in den Wäldern... und das Hammelfleisch wird von den Hunden gefressen.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => "El león muere en los bosques de hambre ... y los perros se comen el cordero.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => '艾玛·沙菲',
            'title'     => "狮子死在饥饿的森林里……羊肉被狗吃掉。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '8',
            'name'      => 'الإمام الشافعي',
            'title'     => 'إذا نطق السفيه فلا تجبه… فخير من إجابته السكوت.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => 'If a fool utters, do not answer him... It is better for him to respond by silence.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);  
         ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => "Si un imbécile prononce, ne lui répondez pas... C'est mieux que sa réponse au silence.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => "Bir aptal konuşursa, ona cevap vermeyin... Sessizliğe verdiği yanıttan daha iyidir.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => 'аль-Эмам аш-Шафи',
            'title'     => "Если дурак произносит, не отвечай ему ... Это лучше, чем его ответ на молчание.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => "Wenn ein Narr etwas sagt, antworte ihm nicht ... Es ist besser als seine Reaktion auf Schweigen.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => "Si un necio habla, no le respondas ... Es mejor que su respuesta al silencio.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => '艾玛·沙菲',
            'title'     => "如果一个傻瓜说话，不要回答他……这比他对沉默的回应要好。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '8',
            'name'      => 'الإمام الشافعي',
            'title'     => 'ولرب نازلة يضيق لها الفتى….. ذرعا وعند الله منها المخرج.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => 'And the Lord of the coming down the boy narrows her….. and God has a way out.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);  
         ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => "Et peut-être qu'une calamité la réduit au garçon... et Dieu a une issue.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => "Ve belki bir bela onu çocuğa indirger... ve Tanrı'nın bir çıkış yolu vardır.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => 'аль-Эмам аш-Шафи',
            'title'     => "И, может быть, беда свела ее к мальчику ... и у Бога есть выход.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => "Und vielleicht beschränkt sie ein Unglück auf den Jungen... und Gott hat einen Ausweg.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => "Y tal vez una calamidad la reduzca al niño ... y Dios tiene una salida.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => '艾玛·沙菲',
            'title'     => "也许一场灾难将她缩小到那个男孩的范围……而上帝有一条出路。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '8',
            'name'      => 'الإمام الشافعي',
            'title'     => 'ورزقك ليس ينقصه التأني….. وليس يزيد في الرزق العناء.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => 'Your sustenance is not lacking in deliberation....and it does not increase in sustenance the hardship.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => "Votre subsistance ne manque pas de délibération... et elle n'augmente pas en subsistance les difficultés.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => "Rızkınız düşünceden yoksun değildir.... ve rızıkta zorluğu artırmaz.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => 'аль-Эмам аш-Шафи',
            'title'     => "Ваша поддержка не лишена осознанности ... и она не увеличивает поддержку в трудностях.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => "Deiner Nahrung fehlt es nicht an Überlegung .... und sie erhöht nicht die Not.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => "Su sustento no carece de deliberación ... y no aumenta en sustento las dificultades.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => '艾玛·沙菲',
            'title'     => "你的寄托不缺乏深思熟虑……而且它不增加寄托的艰辛。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '8',
            'name'      => 'الإمام الشافعي',
            'title'     => 'ضاقت فلما استحكمت حلقاتها….. فرجت وكنت أظنها لا تفرج.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => 'It narrowed, and when its episodes became tight, they were released, and I thought they would not be released.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);   
         ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => "Il s'est rétréci, et quand ses épisodes sont devenus serrés, ils ont été libérés, et j'ai pensé qu'ils ne seraient pas libérés.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => "Daraldı ve bölümleri sıkılaştığında serbest bırakıldı ve yayınlanmayacaklarını düşündüm.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => 'аль-Эмам аш-Шафи',
            'title'     => "Он сузился, и когда его эпизоды стали тесными, их выпустили, и я думал, что они не будут выпущены.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => "Es wurde enger, und als die Episoden knapp wurden, wurden sie veröffentlicht, und ich dachte, sie würden nicht veröffentlicht.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => 'al-Emam Al Shafi',
            'title'     => "Se redujo, y cuando sus episodios se hicieron estrechos, se lanzaron y pensé que no se lanzarían.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '8',
            'name'      => '艾玛·沙菲',
            'title'     => "它缩小了，当它的剧集变得紧张时，它们被释放，我认为它们不会被释放。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/3/36/Al-Shafie_Name.png',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '9',
            'name'      => 'آبن القيم',
            'title'     => 'الحكمة هى قول ما ينبغي على الوجهة الذى ينبغى فى الوقت الذى ينبغي.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ibn al-Qayyim',
            'title'     => 'Wisdom is saying what is ought in the proper manner, at the appropriate time.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ibn al Qayyim',
            'title'     => "La sagesse dit ce qui doit de la bonne manière, au bon moment.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => "İbnü'l Kayyım",
            'title'     => "Bilgelik, olması gerekeni uygun bir şekilde, uygun zamanda söylemektir.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ибн аль-Кайим',
            'title'     => "Мудрость говорит о том, что следует, в надлежащей манере и в надлежащее время.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ibn al-Qayyim',
            'title'     => "Weisheit bedeutet, zur richtigen Zeit zu sagen, was in der richtigen Weise zu tun ist.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ibn al-Qayyim',
            'title'     => "La sabiduría es decir lo que se debe de la manera adecuada, en el momento adecuado.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => '伊本·卡伊姆',
            'title'     => "智慧就是在适当的时间以适当的方式说出应该做的事情。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 

         ManSay::create([
            'man_id'    => '9',
            'name'      => 'آبن القيم',
            'title'     => 'أغبي الناس من ضل في آخر سفره وقد قارب المنزل .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ibn al-Qayyim',
            'title'     => 'The dumbest people are the ones who got lost at the end of their journey and approached the house.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);  
         ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ibn al Qayyim',
            'title'     => "Les personnes les plus stupides sont celles qui se sont perdues à la fin de leur voyage et se sont approchées de la maison.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => "İbnü'l Kayyım",
            'title'     => "En aptal insanlar, yolculuğun sonunda kaybolup eve yaklaşanlardır.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ибн аль-Кайим',
            'title'     => "Самые тупые люди - это те, кто заблудился в конце пути и подошел к дому.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ibn al-Qayyim',
            'title'     => "Die dümmsten Leute sind diejenigen, die sich am Ende ihrer Reise verlaufen und sich dem Haus näherten.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ibn al-Qayyim',
            'title'     => "Las personas más tontas son las que se perdieron al final de su viaje y se acercaron a la casa.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => '伊本·卡伊姆',
            'title'     => "最愚蠢的人是那些在旅程结束时迷路并接近房子的人。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '9',
            'name'      => 'آبن القيم',
            'title'     => 'من أساء إليك ثم جاء يعتذرُ عن إساءته، فإنّ التواضع يُوجب عليك قبول اعتذاره حقاً كان أو باطلاً، وتكلُ سريرته إلى الله .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ibn al-Qayyim',
            'title'     => 'Whoever offended you and then came to apologize for his offense, humility necessitates that you accept his apology, right or wrong, and entrust his heart to God.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ibn al Qayyim',
            'title'     => "Quiconque vous a offensé et est ensuite venu s'excuser pour son offense, l'humilité nécessite que vous acceptiez ses excuses, bonnes ou mauvaises, et confiiez son cœur à Dieu.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => "İbnü'l Kayyım",
            'title'     => "Kim seni kırdıysa ve sonra suçundan dolayı özür dilemeye geldiyse, doğru ya da yanlış, onun özrünü kabul etmeni ve kalbini Allah'a emanet etmeni tevazu gerektirir.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ибн аль-Кайим',
            'title'     => "Кто бы ни обидел вас, а затем пришел извиниться за свое оскорбление, смирение требует, чтобы вы приняли его извинения, правильные или неправильные, и доверили его сердце Богу.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ibn al-Qayyim',
            'title'     => "Wer auch immer Sie beleidigt hat und sich dann für seine Beleidigung entschuldigt, Demut erfordert, dass Sie seine Entschuldigung annehmen, ob richtig oder falsch, und sein Herz Gott anvertrauen.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ibn al-Qayyim',
            'title'     => "Quien sea que te ofendió y luego vino a disculparse por su ofensa, la humildad requiere que aceptes sus disculpas, sean correctas o incorrectas, y confíes su corazón a Dios.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => '伊本·卡伊姆',
            'title'     => "谁得罪了你，然后来为他的冒犯道歉，谦卑需要你接受他的道歉，无论对错，把他的心交托给上帝。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '9',
            'name'      => 'آبن القيم',
            'title'     => 'اذا أردت أن تعلم ما عندك وعند غيرك من محبة الله فانظر محبة القرآن من قلبك.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ibn al-Qayyim',
            'title'     => 'If you want to know what you and others have of the love of God, then see the love of the Qur’an from your heart.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);  
         ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ibn al Qayyim',
            'title'     => "Si vous voulez savoir ce que vous et les autres avez de l'amour de Dieu, alors voyez l'amour du Coran dans votre cœur.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => "İbnü'l Kayyım",
            'title'     => "Allah sevgisi konusunda sizin ve başkalarının sahip olduklarını bilmek istiyorsanız, o halde kalbinizden Kuran sevgisini görün.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ибн аль-Кайим',
            'title'     => "Если вы хотите знать, что вы и другие относитесь к любви Бога, то смотрите на любовь Корана от всего сердца.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ibn al-Qayyim',
            'title'     => "Wenn Sie wissen möchten, was Sie und andere von der Liebe Gottes haben, dann sehen Sie die Liebe des Korans aus Ihrem Herzen.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ibn al-Qayyim',
            'title'     => "Si quieres saber lo que tú y otros tienen del amor de Dios, entonces mira el amor del Corán desde tu corazón.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => '伊本·卡伊姆',
            'title'     => "如果你想知道你和其他人对真主的爱是什么，那么从你的内心中看到古兰经的爱。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '9',
            'name'      => 'آبن القيم',
            'title'     => 'من استطاع منكم أن يجعل كنزه في السماء حيث لا يأكله السوس ولا يناله السراق فليفعل فإنّ قلب الرجل مع كنزه.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ibn al-Qayyim',
            'title'     => 'Whoever among you is able to place his treasure in the sky where neither mites eat it nor thieves can reach it, let him do so, for the heart of a man is with his treasure.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);   
         ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ibn al Qayyim',
            'title'     => "Quiconque parmi vous est capable de placer son trésor dans le ciel où ni les acariens ne le mangent ni les voleurs ne peuvent l'atteindre, qu'il le fasse, car le cœur de l'homme est avec son trésor.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => "İbnü'l Kayyım",
            'title'     => "Sizden kim, göğe hazinesini akarların yemediği ve hırsızların ulaşamayacağı bir yere koyabilirse, bıraksın, çünkü insanın kalbi hazinesiyle birliktedir.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ибн аль-Кайим',
            'title'     => "Кто из вас может поместить свое сокровище на небо, где ни клещи его не съедят, ни воры не смогут достать его, пусть сделает это, ибо сердце человека с его сокровищами.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ibn al-Qayyim',
            'title'     => "Wer von euch seinen Schatz in den Himmel legen kann, wo ihn weder Milben fressen noch Diebe erreichen können, der soll es tun, denn das Herz eines Menschen ist bei seinem Schatz.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => 'Ibn al-Qayyim',
            'title'     => "Cualquiera de ustedes que pueda poner su tesoro en el cielo donde ni las ácaras se lo coman ni los ladrones puedan alcanzarlo, que lo haga, porque el corazón del hombre está con su tesoro.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '9',
            'name'      => '伊本·卡伊姆',
            'title'     => "你们当中谁能把他的财宝放在天上，没有螨虫吃，也没有小偷可以拿到它，让他这样做，因为一个人的心是他的财宝。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png/560px-%D8%A7%D8%A8%D9%86_%D9%82%D9%8A%D9%85_%D8%A7%D9%84%D8%AC%D9%88%D8%B2%D9%8A%D8%A9.png',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 
          ManSay::create([
            'man_id'    => '10',
            'name'      => 'الإمام علي',
            'title'     => 'أكرم ضيفك وإن كان حقيراً، وقُم على مجلسك لأبيك ومعلمك وإن كنت أميراً.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '10',
            'name'      => 'Imam Ali',
            'title'     => 'Honor your guest, even if he is despicable, and rise from your seat to your father and teacher, even if you are a prince.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => 'Imam Ali',
            'title'     => "Honorez votre invité, même s'il est méprisable, et montez de votre siège vers votre père et professeur, même si vous êtes un prince.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => 'İmam Ali',
            'title'     => "Aşağılık da olsa misafirinize hürmet edin ve şehzade olsanız bile koltuğunuzdan babanıza ve öğretmeninize yükselin.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => 'Имам Али',
            'title'     => "Уважайте своего гостя, даже если он презренный, и поднимитесь со своего места к своему отцу и учителю, даже если вы принц.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => 'Imam Ali',
            'title'     => "Ehre deinen Gast, auch wenn er verabscheuungswürdig ist, und erhebe dich von deinem Platz zu deinem Vater und Lehrer, auch wenn du ein Prinz bist.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => 'Imam Ali',
            'title'     => "Honra a tu invitado, incluso si es despreciable, y levántate de tu asiento hacia tu padre y maestro, incluso si eres un príncipe.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => '伊玛目阿里',
            'title'     => "尊敬你的客人，即使他是卑鄙的，从你的座位上起身到你的父亲和老师面前，即使你是王子。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]);  

         ManSay::create([
            'man_id'    => '10',
            'name'      => 'الإمام علي',
            'title'     => 'أحسن الكلام ما لا تمجّه الأذان ولا يُتعب فهمه الأفهام.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '10',
            'name'      => 'Imam Ali',
            'title'     => 'The best speech is that which does not disturb the adhan and does not make the understanding difficult for the understanding.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '10',
            'name'      => 'Imam Ali',
            'title'     => "Le meilleur discours est celui qui ne déforme pas l'appel à la prière et ne rend pas la compréhension difficile pour la compréhension.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => 'İmam Ali',
            'title'     => "En güzel söz, ezanı bozmayan ve anlamayı zorlaştırmayan sözdür.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => 'Имам Али',
            'title'     => "Лучшая речь - это та, которая не искажает призыв к молитве и не затрудняет понимание.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => 'Imam Ali',
            'title'     => "Die beste Rede ist die, die den Gebetsruf nicht entstellt und dem Verstehen das Verstehen nicht erschwert.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => 'Imam Ali',
            'title'     => "El mejor discurso es el que no distorsiona el llamado a la oración y no dificulta el entendimiento para el entendimiento.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => '伊玛目阿里',
            'title'     => "最好的演讲是不扭曲祈祷的呼召，也不会使理解变得困难的演讲。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '10',
            'name'      => 'الإمام علي',
            'title'     => 'ينبّئ عن عقل كل امرءٍ لسانه، ويدلّ على فضله بيانه .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '10',
            'name'      => 'Imam Ali',
            'title'     => 'His tongue informs about the intellect of every person, and his eloquence indicates his merit.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '10',
            'name'      => 'Imam Ali',
            'title'     => "Sa langue informe sur l'intellect de chaque personne, et son éloquence indique son mérite.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => 'İmam Ali',
            'title'     => "Dili her insanın aklını haber verir ve belagati onun faziletini gösterir.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => 'Имам Али',
            'title'     => "Его язык сообщает об интеллекте каждого человека, а его красноречие указывает на его достоинства.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => 'Imam Ali',
            'title'     => "Seine Zunge informiert über den Intellekt eines jeden Menschen, und seine Beredsamkeit zeigt seinen Verdienst.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => 'Imam Ali',
            'title'     => "Su lengua informa sobre el intelecto de cada persona, y su elocuencia indica su mérito.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => '伊玛目阿里',
            'title'     => "他的舌头表明了每个人的智力，他的口才表明了它的优点。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 
          ManSay::create([
            'man_id'    => '10',
            'name'      => 'الإمام علي',
            'title'     => 'إيّاك ومصادقة الأحمق.. فإنّه يريد أن ينفعك فيضرّك.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '10',
            'name'      => 'Imam Ali',
            'title'     => 'Beware of making friends with a fool, for he wants to benefit you and harm you.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]); 
         ManSay::create([
            'man_id'    => '10',
            'name'      => 'Imam Ali',
            'title'     => "Méfiez-vous de vous lier d'amitié avec un imbécile, car il veut vous faire du bien et vous nuire.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => 'İmam Ali',
            'title'     => "Bir aptalla arkadaşlık etmekten sakının, çünkü o size fayda sağlamak ve size zarar vermek ister.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => 'Имам Али',
            'title'     => "Остерегайтесь дружить с глупцом, потому что он хочет принести вам пользу и навредить вам.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => 'Imam Ali',
            'title'     => "Hüte dich davor, dich mit einem Narren anzufreunden, denn er will dir nützen und dir schaden.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => 'Imam Ali',
            'title'     => "Cuídate de hacerte amigo de un necio, porque él quiere beneficiarte y perjudicarte.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => '伊玛目阿里',
            'title'     => "谨防与傻瓜交朋友，因为他要利益你，要害你。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]); 
          ManSay::create([
            'man_id'    => '10',
            'name'      => 'الإمام علي',
            'title'     => 'أكرم الشّيم إكرام المُصاحب وإسعاف الطّالب .',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'ar',
            'color'     => '#084B9A',
        ]);

         ManSay::create([
            'man_id'    => '10',
            'name'      => 'Imam Ali',
            'title'     => 'Honor the sham, honor the companion and help the student.',
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'en',
            'color'     => '#084B9A',
        ]);     
        ManSay::create([
            'man_id'    => '10',
            'name'      => 'Imam Ali',
            'title'     => "Honorez l'imposture en honorant le compagnon et en aidant l'élève.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'fr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => 'İmam Ali',
            'title'     => "Arkadaşı onurlandırarak ve öğrenciye yardım ederek sahtekarlığı onurlandırın.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'tr',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => 'Имам Али',
            'title'     => "Уважайте притворство, почитая товарища и помогая ученику.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'ru',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => 'Imam Ali',
            'title'     => "Ehre den Schein, indem du den Gefährten ehrst und dem Schüler hilfst.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'de',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => 'Imam Ali',
            'title'     => "Honre la farsa honrando al compañero y ayudando al estudiante.",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'es',
            'color'     => '#084B9A',
        ]);
        ManSay::create([
            'man_id'    => '10',
            'name'      => '伊玛目阿里',
            'title'     => "通过尊重同伴和帮助学生来尊重假冒。",
            'pic'       => 'https://upload.wikimedia.org/wikipedia/ar/thumb/8/80/%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif/500px-%D8%B9%D9%84%D9%8A_%D8%A8%D9%86_%D8%A3%D8%A8%D9%8A_%D8%B7%D8%A7%D9%84%D8%A8.gif',
            'lang'      => 'cn',
            'color'     => '#084B9A',
        ]);    
        
    }
}
