<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Testmonial;
class TestmonialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Testmonial::create( [
            'image'=>'profile-photos/1635937892.png',
            'title'=>'برنامج روعة',
            'desc'=>'مكنني من التعرف علي عائلتي الكبيرة والتي لم أستطيع التعرف عليهم من قبل',
            'user_name'=>'بسنت الحسيني',
            'place'=>'مصر',
            'rate'=>'5',
            'iso'=>'eg',
            'lang'=>'ar'
        ] );

        Testmonial::create( [
            'image'=>'profile-photos/1635937892.png',
            'title'=>'wonderful platform',
            'desc'=>'make me know more family members I dont know before',
            'user_name'=>'Basant Elhossieny',
            'place'=>'Egypt',
            'rate'=>'5',
            'iso'=>'eg',
            'lang'=>'en'
        ] );

        Testmonial::create( [
            'image'=>'profile-photos/1635937782.jpg',
            'title'=>'برنامج جميل جدا',
            'desc'=>'الآن يمكنني التواصل مع كل أفراد عائلتي',
            'user_name'=>'حسام عبدالله',
            'place'=>'مصر',
            'rate'=>'5',
            'iso'=>'eg',
            'lang'=>'ar'
        ] );

        Testmonial::create( [
            'image'=>'profile-photos/1635937782.jpg',
            'title'=>'very nice app',
            'desc'=>'Now I can connect all me family members',
            'user_name'=>'Hossam Abdallah',
            'place'=>'Egypt',
            'rate'=>'5',
            'iso'=>'eg',
            'lang'=>'en'
        ] );

        Testmonial::create( [
            'image'=>'profile-photos/1635939011.png',
            'title'=>'تجربة فريدة',
            'desc'=>'أخيرا تمكنت من التواصل مع أفراد عائلتي البعيدين',
            'user_name'=>'أحمد عبدالعال',
            'place'=>'الإمارات',
            'rate'=>'5',
            'iso'=>'ae',
            'lang'=>'ar'
        ] );

        Testmonial::create( [
            'image'=>'profile-photos/1635939011.png',
            'title'=>'New idea',
            'desc'=>'finally I connect my losted family connections',
            'user_name'=>'Ahmed Abdulall',
            'place'=>'U A E',
            'rate'=>'5',
            'iso'=>'ae',
            'lang'=>'en'
        ] );

        Testmonial::create( [
            'image'=>'profile-photos/1636204487.png',
            'title'=>'فكرة عظيمة',
            'desc'=>'تفاعل رائع مع أعضاء عائلتي',
            'user_name'=>'نفين الحسيني',
            'place'=>'كندا',
            'rate'=>'5',
            'iso'=>'ca',
            'lang'=>'ar'
        ] );

        Testmonial::create( [
            'image'=>'profile-photos/1636204487.png',
            'title'=>'wonderfull idea',
            'desc'=>'nice connection with my family',
            'user_name'=>'nivien elhossieny',
            'place'=>'Canada',
            'rate'=>'5',
            'iso'=>'ca',
            'lang'=>'en'
        ] );

        Testmonial::create( [
            'image'=>'profile-photos/1635938697.png',
            'title'=>'جميل جدا',
            'desc'=>'سوف أقضي وقت أطول مع عائلتي',
            'user_name'=>'لؤي قايد',
            'place'=>'كندا',
            'rate'=>'5',
            'iso'=>'sa',
            'lang'=>'ar'
        ] );

        Testmonial::create( [
            'image'=>'profile-photos/1635938697.png',
            'title'=>'very nice',
            'desc'=>'Ill make more time to talk to my members',
            'user_name'=>'Loay Kaied',
            'place'=>'KSA',
            'rate'=>'5',
            'iso'=>'sa',
            'lang'=>'en'
        ] );

    }

}