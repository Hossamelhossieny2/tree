<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\WheelQues;

class WheelQuesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '1',
            'title'     => 'Keep your prayers on time',
            'lang'      => 'en',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '1',
            'title'     => "Gardez vos prières à l'heure",
            'lang'      => 'fr',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '1',
            'title'     => 'Namazlarını vaktinde tut',
            'lang'      => 'tr',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '1',
            'title'     => 'Молитесь вовремя',
            'lang'      => 'ru',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '1',
            'title'     => 'Halte deine Gebete pünktlich',
            'lang'      => 'de',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '1',
            'title'     => 'Mantén tus oraciones a tiempo',
            'lang'      => 'es',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '1',
            'title'     => '准时祈祷',
            'lang'      => 'cn',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '1',
            'title'     => 'تحافظ علي صلاتك في وقتها',
            'lang'      => 'ar',
        ]);

        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '2',
            'title'     =>  "I don't rush to pray, even if I have an important date",
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '2',
            'title'     =>  "Je ne me précipite pas pour prier, même si j'ai un rendez-vous important",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '2',
            'title'     =>  "Önemli bir randevum olsa bile dua etmek için acele etmem.",
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '2',
            'title'     =>  "Я не тороплюсь с молитвой, даже если у меня важное свидание",
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '2',
            'title'     =>  "Ich beeile nicht zu beten, auch wenn ich ein wichtiges Date habe",
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '2',
            'title'     =>  "No me apresuro a rezar, aunque tenga una cita importante",
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '2',
            'title'     =>  "我不急于祈祷，即使我有一个重要的约会",
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '2',
            'title'     =>  'لا أستعجل في صلاتي حتي وإن كنت مرتبط بموعد هام',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '3',
            'title'     =>  'I spend from time to time a retreat with my Lord',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '3',
            'title'     =>  'Je passe de temps en temps une retraite avec mon Seigneur',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '3',
            'title'     =>  'Zaman zaman Rabbimle inzivaya çekiliyorum',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '3',
            'title'     =>  'Время от времени я провожу ретрит с моим Господом',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '3',
            'title'     =>  'Ich verbringe von Zeit zu Zeit ein Retreat mit meinem Herrn',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '3',
            'title'     =>  'De vez en cuando paso un retiro con mi señor',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '3',
            'title'     =>  '我不时与我的主一起静修',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '3',
            'title'     =>  'أقضي من حين لآخر خلوة مع ربي',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '4',
            'title'     =>  'I read in the books of religious teachings and guidance of my Messenger constantly',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '4',
            'title'     =>  'Je lis dans les livres des enseignements religieux et des conseils de mon Messager constamment',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '4',
            'title'     =>  'Peygamberimin dini öğretileri ve rehberlik kitaplarında sürekli okuyorum.',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '4',
            'title'     =>  'Я постоянно читаю в книгах религиозных учений и наставлений моего Посланника.',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '4',
            'title'     =>  'Ich lese ständig in den Büchern über religiöse Lehren und Führungen meines Gesandten',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '4',
            'title'     =>  'Leo en los libros de enseñanzas religiosas y la guía de mi Mensajero constantemente.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '4',
            'title'     =>  '我不断地阅读我的信使的宗教教义和指导书籍',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '4',
            'title'     =>  'أقرأ في كتب تعاليم ديني وتوجيهات رسولي بإستمرار',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '5',
            'title'     =>  'I keep my daily roses from the holy book of my Lord',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '5',
            'title'     =>  'Je garde mes roses quotidiennes du livre saint de mon Seigneur',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '5',
            'title'     =>  'Günlük güllerimi Rabbimin kutsal kitabından saklıyorum',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '5',
            'title'     =>  'Я храню свои ежедневные розы из священной книги моего Господа',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '5',
            'title'     =>  'Ich bewahre meine täglichen Rosen aus dem heiligen Buch meines Herrn',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '5',
            'title'     =>  'Guardo mis rosas diarias del libro sagrado de mi Señor',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '5',
            'title'     =>  '我从我主的圣书中保留我的日常玫瑰',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '5',
            'title'     =>  'أحافظ علي ورد يومي من كتاب ربي المقدس',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '6',
            'title'     =>  'I have a minimum of waffles that I do regularly',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '6',
            'title'     =>  "J'ai un minimum de gaufres que je fais régulièrement",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '6',
            'title'     =>  'Düzenli olarak yaptığım en az waffle var',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '6',
            'title'     =>  'У меня есть минимум вафель, которые я делаю регулярно',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '6',
            'title'     =>  'Ich habe ein Minimum an Waffeln, die ich regelmäßig mache',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '6',
            'title'     =>  'Tengo un mínimo de gofres que hago con regularidad.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '6',
            'title'     =>  '我至少有我经常做的华夫饼',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '6',
            'title'     =>  'لدي حد أدني من النوافل أؤديه بإستمرار',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '7',
            'title'     =>  'I have remembrances and supplications that I keep',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '7',
            'title'     =>  "J'ai des souvenirs et des supplications que je garde",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '7',
            'title'     =>  'Sakladığım hatıralarım ve dualarım var',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '7',
            'title'     =>  'У меня есть воспоминания и мольбы, которые я храню',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '7',
            'title'     =>  'Ich habe Erinnerungen und Bitten, die ich halte',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '7',
            'title'     =>  'Tengo recuerdos y súplicas que guardo',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '7',
            'title'     =>  '我有我的回忆和恳求',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '7',
            'title'     =>  'لدي أذكار وأدعية أحافظ عليها',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '8',
            'title'     =>  'Not a month goes by without me offering voluntary fasts and alms',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '8',
            'title'     =>  "Il ne se passe pas un mois sans que j'offre des jeûnes et des aumônes volontaires",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '8',
            'title'     =>  'Benden nafile oruç ve sadaka vermediğim bir ay geçmiyor.',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '8',
            'title'     =>  'Не проходит и месяца, чтобы я не предлагал добровольные посты и милостыню',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '8',
            'title'     =>  'Es vergeht kein Monat, in dem ich nicht freiwillig Fasten und Almosen anbiete',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '8',
            'title'     =>  'No pasa un mes sin que ofrezca ayunos y limosnas voluntarias',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '8',
            'title'     =>  '没有一个月没有我提供自愿斋戒和施舍',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '8',
            'title'     =>  'لا يمر شهر دون أن أودي صيام نافلة وصدقة',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '9',
            'title'     =>  'I do not backbite anyone and cover my hearing and sight',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '9',
            'title'     =>  'Je ne médise personne et couvre mon ouïe et ma vue',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '9',
            'title'     =>  'Ben kimseyi gıybet etmem, işitmemi ve görmemi örtmem.',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '9',
            'title'     =>  'Я никого не клевещу и прикрываю слух и зрение',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '9',
            'title'     =>  'Ich beschimpfe niemanden und bedecke mein Gehör und mein Sehvermögen',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '9',
            'title'     =>  'No muerdo a nadie y me tapo el oído y la vista',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '9',
            'title'     =>  '我不反咬任何人并掩盖我的听力和视力',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '9',
            'title'     =>  'لا أغتاب أحدا وأغط سمعي وبصري',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '10',
            'title'     =>  'If you commit a sin, seek forgiveness and follow it with a good deed immediately',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '10',
            'title'     =>  'Si vous commettez un péché, demandez pardon et suivez-le immédiatement par une bonne action',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '10',
            'title'     =>  'Günah işlersen hemen mağfiret dile ve hemen ardından bir iyilik yap.',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '10',
            'title'     =>  'Если вы совершили грех, ищите прощения и немедленно сделайте доброе дело',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '10',
            'title'     =>  'Wenn du eine Sünde begehst, bitte um Vergebung und folge ihr sofort mit einer guten Tat',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '10',
            'title'     =>  'Si comete un pecado, busque el perdón y sígalo con una buena acción de inmediato.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '10',
            'title'     =>  '如果你犯了罪，寻求宽恕并立即行善',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '1',
            'q_id'      =>  '10',
            'title'     =>  'إذا فعلت معصية فأستغفر وأتبعها حسنة فورا',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '1',
            'title'     =>  'I can speak brilliantly in front of the crowd',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '1',
            'title'     =>  'Je peux parler brillamment devant la foule',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '1',
            'title'     =>  'Kalabalığın önünde zekice konuşabilirim',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '1',
            'title'     =>  'Я могу блестяще говорить перед толпой',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '1',
            'title'     =>  'Ich kann vor der Menge brillant sprechen',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '1',
            'title'     =>  'Puedo hablar brillantemente frente a la multitud.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '1',
            'title'     =>  '我可以在人群面前精彩地说话',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '1',
            'title'     =>  'أستطيع أن أتحدث ببراعة أمام الجماهير',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '2',
            'title'     =>  'I have art and taste in dialogue (diplomatic)',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '2',
            'title'     =>  "J'ai l'art et le goût du dialogue (diplomatique)",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '2',
            'title'     =>  'Diyalogda sanatım ve zevkim var (diplomatik)',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '2',
            'title'     =>  'У меня искусство и вкус к диалогу (дипломатический)',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '2',
            'title'     =>  'Ich habe Kunst und Geschmack im Dialog (diplomatisch)',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '2',
            'title'     =>  'Tengo arte y gusto para dialogar (diplomático)',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '2',
            'title'     =>  '我对对话有艺术和品味（外交）',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '2',
            'title'     =>  'لدي فن وذوق في الحوار ( دبلوماسي )',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '3',
            'title'     =>  'I can deal with difficult characters',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '3',
            'title'     =>  'Je peux gérer des personnages difficiles',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '3',
            'title'     =>  'Zor karakterlerle başa çıkabilirim',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '3',
            'title'     =>  'Я могу справиться с трудными персонажами',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '3',
            'title'     =>  'Ich kann mit schwierigen Charakteren umgehen',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '3',
            'title'     =>  'Puedo lidiar con personajes difíciles',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '3',
            'title'     =>  '我可以处理困难的角色',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '3',
            'title'     =>  'أستطيع أن أتعامل مع الشخصيات ذوي الطباع الصعبة',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '4',
            'title'     =>  'I have enough flexibility in times of crisis',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '4',
            'title'     =>  "J'ai assez de flexibilité en temps de crise",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '4',
            'title'     =>  'Kriz zamanlarında yeterince esnekliğe sahibim',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '4',
            'title'     =>  'У меня достаточно гибкости во время кризиса',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '4',
            'title'     =>  'Ich habe genug Flexibilität in Krisenzeiten',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '4',
            'title'     =>  'Tengo suficiente flexibilidad en tiempos de crisis.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '4',
            'title'     =>  '我在危机时期有足够的灵活性',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '4',
            'title'     =>  'لدي مرونة كافية وقت الأزمات',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '5',
            'title'     =>  'I am promoting my job faster than usual',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '5',
            'title'     =>  "Je fais la promotion de mon travail plus rapidement que d'habitude",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '5',
            'title'     =>  'İşimi normalden daha hızlı terfi ettiriyorum',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '5',
            'title'     =>  'Я продвигаю свою работу быстрее, чем обычно',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '5',
            'title'     =>  'Ich bewerbe meinen Job schneller als sonst',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '5',
            'title'     =>  'Estoy promocionando mi trabajo más rápido de lo habitual.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '5',
            'title'     =>  '我比平时更快地晋升我的工作',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '5',
            'title'     =>  'أترقي في وظيفتي أسرع من المعتاد',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '6',
            'title'     =>  "I am aware of other people's problems without being involved in them",
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '6',
            'title'     =>  "Je suis conscient des problèmes des autres sans m'y impliquer",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '6',
            'title'     =>  "Başkalarının problemlerine karışmadan onların problemlerinin farkındayım.",
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '6',
            'title'     =>  "Я осведомлен о проблемах других людей, но не участвую в них",
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '6',
            'title'     =>  "Ich bin mir der Probleme anderer Menschen bewusst, ohne in sie verwickelt zu sein",
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '6',
            'title'     =>  "Soy consciente de los problemas de otras personas sin involucrarme en ellos.",
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '6',
            'title'     =>  "我知道其他人的问题，但不参与其中",
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '6',
            'title'     =>  'لدي وعي بمشاكل الآخرين دون أن أنخرط فيها',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '7',
            'title'     =>  'He has high morals and a chaste tongue',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '7',
            'title'     =>  'Il a une haute moralité et une langue chaste',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '7',
            'title'     =>  'Yüksek ahlaka ve iffetli bir dile sahiptir.',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '7',
            'title'     =>  'У него высокая нравственность и целомудренный язык',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '7',
            'title'     =>  'Er hat eine hohe Moral und eine keusche Zunge',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '7',
            'title'     =>  'Tiene alta moral y una lengua casta',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '7',
            'title'     =>  '他有高尚的道德和纯洁的舌头',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '7',
            'title'     =>  'صاحب أخلاق عالية ولساني عفيف',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '8',
            'title'     =>  'I am said to have charisma',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '8',
            'title'     =>  "on dit que j'ai du charisme",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '8',
            'title'     =>  'karizmam var demiştim',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '8',
            'title'     =>  'Говорят, что у меня харизма',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '8',
            'title'     =>  'Mir wird nachgesagt, dass ich Charisma habe',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '8',
            'title'     =>  'Se dice que tengo carisma',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '8',
            'title'     =>  '被说有魅力',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '8',
            'title'     =>  'يقال عني صاحب كاريزما',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '9',
            'title'     =>  'people or my friends often afraid of me',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '9',
            'title'     =>  'les gens ou mes amis ont souvent peur de moi',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '9',
            'title'     =>  'insanlar veya arkadaşlarım genellikle benden korkar',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '9',
            'title'     =>  'люди или мои друзья часто меня боятся',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '9',
            'title'     =>  'Leute oder meine Freunde haben oft Angst vor mir',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '9',
            'title'     =>  'La gente o mis amigas a menudo me tienen miedo.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '9',
            'title'     =>  '人们或我的朋友经常害怕我',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '9',
            'title'     =>  'كثيرا ما يهابني الناس أو أصدقائي',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '10',
            'title'     =>  'I respect myself with others and do not often allow insult',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '10',
            'title'     =>  'Je me respecte avec les autres et ne permet pas souvent les insultes',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '10',
            'title'     =>  'Başkalarının yanında kendime saygı duyarım ve çoğu zaman hakarete izin vermem',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '10',
            'title'     =>  'Я уважаю себя по отношению к другим и не допускаю часто оскорблений',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '10',
            'title'     =>  'Ich respektiere mich mit anderen und lasse nicht oft Beleidigungen zu',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '10',
            'title'     =>  'Me respeto con las demás y no suelo permitir insultos.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '10',
            'title'     =>  '我尊重他人，不经常受到侮辱',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '2',
            'q_id'      =>  '10',
            'title'     =>  'أحترم نفسي مع الغير ولا أسمح غالبا بالإهانة',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '1',
            'title'     =>  'I have places to hang out with myself',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '1',
            'title'     =>  "J'ai des endroits pour sortir avec moi-même",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '1',
            'title'     =>  'kendimle takılacağım yerler var',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '1',
            'title'     =>  'У меня есть где пообщаться',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '1',
            'title'     =>  'Ich habe Orte, um mit mir abzuhängen',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '1',
            'title'     =>  'Tengo lugares para pasar el rato conmigo mismo',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '1',
            'title'     =>  '我有地方可以和自己闲逛',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '1',
            'title'     =>  'لدي أماكن أختلي بها مع نفسي',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '2',
            'title'     =>  "I play and enjoy children's games",
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '2',
            'title'     =>  "Je joue et apprécie les jeux d'enfants",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '2',
            'title'     =>  "çocuk oyunları oynarım ve zevk alırım",
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '2',
            'title'     =>  "Я играю и наслаждаюсь детскими играми",
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '2',
            'title'     =>  "Ich spiele und genieße Kinderspiele",
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '2',
            'title'     =>  "我玩和享受儿童游戏",
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '2',
            'title'     =>  "Juego y disfruto de los juegos infantiles.",
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '2',
            'title'     =>  'ألعب ألعاب الأطفال وأستمتع بها',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '3',
            'title'     =>  'I do my favorite hobbies on a regular basis',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '3',
            'title'     =>  'Je fais régulièrement mes loisirs préférés',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '3',
            'title'     =>  'En sevdiğim hobilerimi düzenli olarak yaparım',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '3',
            'title'     =>  'Я регулярно занимаюсь любимым хобби',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '3',
            'title'     =>  'Ich gehe regelmäßig meinen Lieblingshobbys nach',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '3',
            'title'     =>  'Hago mis pasatiempos favoritos de forma regular.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '3',
            'title'     =>  '我定期做我最喜欢的爱好',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '3',
            'title'     =>  'أمارس هواياتي المفضلة بشكل منتظم',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '4',
            'title'     =>  'I do relaxation exercises and discharge excess charges when working pressure',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '4',
            'title'     =>  'Je fais des exercices de relaxation et décharge les charges en excès lorsque je travaille la pression',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '4',
            'title'     =>  'Gevşeme egzersizleri yaparım ve çalışma baskısı sırasında fazla yükleri boşaltırım.',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '4',
            'title'     =>  'Делаю упражнения на расслабление и сбрасываю лишние заряды при рабочем давлении',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '4',
            'title'     =>  'Ich mache Entspannungsübungen und entlade Überladungen bei Arbeitsdruck',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '4',
            'title'     =>  'Hago ejercicios de relajación y descargo cargas excesivas cuando trabajo presión.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '4',
            'title'     =>  '我在工作压力时做放松运动并释放多余的电荷',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '4',
            'title'     =>  'أمارس تمارين الإسترخاء وتفريغ الشحنات الزائده عند ضغط العمل',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '5',
            'title'     =>  'I travel and enjoy traveling to other cities, resorts or countries',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '5',
            'title'     =>  "Je voyage et j'aime voyager dans d'autres villes, centres de villégiature ou pays",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '5',
            'title'     =>  'Seyahat ediyorum ve diğer şehirlere, tatil köylerine veya ülkelere seyahat etmekten zevk alıyorum',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '5',
            'title'     =>  'Я путешествую и люблю путешествовать по другим городам, курортам или странам',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '5',
            'title'     =>  'Ich reise und reise gerne in andere Städte, Resorts oder Länder',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '5',
            'title'     =>  'Viajo y disfruto viajar a otras ciudades, resorts o países.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '5',
            'title'     =>  '我旅行并喜欢去其他城市、度假村或国家旅行',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '5',
            'title'     =>  'أسافر وأستمتع بالسفر لمدن أخري أو مصايف أو دول',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '6',
            'title'     =>  'Read or listen to interesting novels or stories, jokes and riddles and play them with my friends',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '6',
            'title'     =>  'Lire ou écouter des romans ou des histoires intéressantes, des blagues et des énigmes et les jouer avec mes amis',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '6',
            'title'     =>  'İlginç romanlar veya hikayeler, şakalar ve bilmeceler okuyun veya dinleyin ve arkadaşlarımla oynayın',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '6',
            'title'     =>  'Читайте или слушайте интересные романы или рассказы, анекдоты и загадки и играйте в них с друзьями.',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '6',
            'title'     =>  'Lesen oder hören Sie interessante Romane oder Geschichten, Witze und Rätsel und spielen Sie sie mit meinen Freunden',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '6',
            'title'     =>  'Leer o escuchar novelas o historias interesantes, bromas y acertijos y jugarlos con mis amigos',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '6',
            'title'     =>  '阅读或聆听有趣的小说或故事、笑话和谜语，并与我的朋友一起玩',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '6',
            'title'     =>  'أقرأ روايات ممتعة أو قصص أو أستمع إليها وطرائف وفوازير وألعبها مع أصدقائي',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '7',
            'title'     =>  'I have times of meditation and contemplation, whether about nature, creatures or strangeness',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '7',
            'title'     =>  "J'ai des moments de méditation et de contemplation, que ce soit sur la nature, les créatures ou l'étrangeté",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '7',
            'title'     =>  'Doğa, yaratıklar veya tuhaflık hakkında olsun, meditasyon ve tefekkür zamanlarım var.',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '7',
            'title'     =>  'У меня бывают времена медитации и созерцания, будь то природа, существа или странности',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '7',
            'title'     =>  'Ich habe Zeiten der Meditation und Kontemplation, ob über Natur, Kreaturen oder Fremdheit',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '7',
            'title'     =>  'Tengo momentos de meditación y contemplación, ya sea sobre la naturaleza, las criaturas o la extrañeza.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '7',
            'title'     =>  '我有冥想和沉思的时间，无论是关于自然、生物还是陌生',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '7',
            'title'     =>  'لي توقيتات من التأمل والتفكر  سواء في الطبيعة أو المخلوقات أو الغرائب',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '8',
            'title'     =>  'I have mental thinking strategies like the 15 second method',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '8',
            'title'     =>  "J'ai des stratégies de réflexion mentale comme la méthode des 15 secondes",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '8',
            'title'     =>  '15 saniye yöntemi gibi zihinsel düşünme stratejilerim var',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '8',
            'title'     =>  'У меня есть стратегии ментального мышления, такие как 15-секундный метод',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '8',
            'title'     =>  'Ich habe mentale Denkstrategien wie die 15-Sekunden-Methode',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '8',
            'title'     =>  'Tengo estrategias de pensamiento mental como el método de 15 segundos.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '8',
            'title'     =>  '我有像 十五 秒方法这样的心理思考策略',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '8',
            'title'     =>  'لدي إستراتيجيات التفكير العقلي مثل طريقة ١٥ ثانية',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '9',
            'title'     =>  'I enjoy my time all the time and I have nothing to bother me',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '9',
            'title'     =>  "J'apprécie mon temps tout le temps et je n'ai rien pour me déranger",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '9',
            'title'     =>  'Her zaman zamanımın tadını çıkarırım ve beni rahatsız edecek hiçbir şeyim yok',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '9',
            'title'     =>  'Я все время наслаждаюсь своим временем, и мне не о чем беспокоить',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '9',
            'title'     =>  'Ich genieße meine Zeit die ganze Zeit und ich habe nichts, was mich stört',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '9',
            'title'     =>  'Disfruto mi tiempo todo el tiempo y no tengo nada que me moleste',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '9',
            'title'     =>  '我一直很享受我的时间，我没有什么可打扰我的',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '9',
            'title'     =>  'أستمتع بوقتي بإستمرار وليس لدي ما ينغصني',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '10',
            'title'     =>  'I play with children and mingle with them to the point that I feel like a child',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '10',
            'title'     =>  'Je joue avec les enfants et me mêle à eux au point que je me sens comme un enfant',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '10',
            'title'     =>  'Çocuklarla oynuyorum ve kendimi çocuk gibi hissedene kadar onlarla kaynaşıyorum.',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '10',
            'title'     =>  'Я играю с детьми и общаюсь с ними до такой степени, что чувствую себя ребенком',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '10',
            'title'     =>  'Ich spiele mit Kindern und mische mich mit ihnen so weit, dass ich mich wie ein Kind fühle',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '10',
            'title'     =>  'Juego con los niños y me relaciono con ellos hasta el punto que me siento como un niño',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '10',
            'title'     =>  '我和孩子们一起玩，和他们打成一片，以至于我觉得自己像个孩子',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '3',
            'q_id'      =>  '10',
            'title'     =>  'ألعب مع الأطفال وأندمج معهم لدرجة أنني أشعر وكأنني طفل',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '1',
            'title'     =>  'I know my nutritional needs and meet them, such as increasing protein to build muscle and reducing fat and carbohydrates to lose weight',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '1',
            'title'     =>  "Je connais mes besoins nutritionnels et j'y réponds, comme augmenter les protéines pour développer mes muscles et réduire les graisses et les glucides pour perdre du poids",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '1',
            'title'     =>  'Kas inşa etmek için proteini artırmak ve kilo vermek için yağ ve karbonhidratları azaltmak gibi beslenme ihtiyaçlarımı biliyorum ve bunları karşılıyorum.',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '1',
            'title'     =>  'Я знаю свои потребности в питании и удовлетворяю их, например, увеличиваю количество белка для наращивания мышечной массы и уменьшаю количество жиров и углеводов для похудения.',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '1',
            'title'     =>  'Ich kenne meine Ernährungsbedürfnisse und erfülle sie, z. B. Erhöhung des Proteins zum Muskelaufbau und Reduzierung von Fett und Kohlenhydraten zum Abnehmen',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '1',
            'title'     =>  'Conozco mis necesidades nutricionales y las satisfago, como aumentar las proteínas para desarrollar músculo y reducir las grasas y los carbohidratos para perder peso.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '1',
            'title'     =>  '我知道我的营养需求并满足它们，例如增加蛋白质以增强肌肉并减少脂肪和碳水化合物以减轻体重',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '1',
            'title'     =>  'أعرف حاجاتي الغذائية وألبيها مثل زيادة البروتين لبناء العضلات وتخفيف الدهون والنشويات لتقليل الوزن',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '2',
            'title'     =>  'I exercise or exercise for 20-30 minutes a day or an hour at least three times a week',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '2',
            'title'     =>  "Je fais de l'exercice pendant 20 à 30 minutes par jour ou une heure au moins trois fois par semaine",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '2',
            'title'     =>  'Haftada en az üç kez günde 20-30 dakika veya bir saat egzersiz yapıyorum',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '2',
            'title'     =>  'Я занимаюсь спортом по 20-30 минут в день или по часу не реже трех раз в неделю.',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '2',
            'title'     =>  'Ich trainiere mindestens dreimal pro Woche 20-30 Minuten pro Tag oder eine Stunde',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '2',
            'title'     =>  'Hago ejercicio durante 20-30 minutos al día o una hora al menos tres veces a la semana.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '2',
            'title'     =>  '我每周至少锻炼 三 次，每天锻炼 二十-三十 分钟或一个小时',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '2',
            'title'     =>  'أتمرن أو أمارس الرياضة من ٢٠-٣٠ دقيقة يوميا أو ساعة ثلاث مرات أسبوعيا علي الأقل',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '3',
            'title'     =>  'Get enough sleep without too much',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '3',
            'title'     =>  'Dormez suffisamment sans trop',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '3',
            'title'     =>  'Çok fazla uyumadan yeterince uyuyun',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '3',
            'title'     =>  'Высыпайтесь без лишнего',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '3',
            'title'     =>  'Genug Schlaf bekommen, ohne zu viel',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '3',
            'title'     =>  'Duerma lo suficiente sin demasiado',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '3',
            'title'     =>  '获得充足的睡眠，不要太多',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '3',
            'title'     =>  'أحصل علي قسط كافي من النوم دون إفراط',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '4',
            'title'     =>  'Reduce the amount of junk food in my food',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '4',
            'title'     =>  'Réduire la quantité de malbouffe dans ma nourriture',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '4',
            'title'     =>  'Yemeğimdeki abur cubur miktarını azaltın',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '4',
            'title'     =>  'Уменьшите количество нездоровой пищи в еде',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '4',
            'title'     =>  'Reduziere die Menge an Junk Food in meinem Essen',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '4',
            'title'     =>  'Reducir la cantidad de comida chatarra en mi comida.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '4',
            'title'     =>  '减少我食物中垃圾食品的数量',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '4',
            'title'     =>  'أقلل من كمية الوجبات السريعة في طعامي',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '5',
            'title'     =>  'Eating more vegetables and fruits',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '5',
            'title'     =>  'Manger plus de légumes et de fruits',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '5',
            'title'     =>  'Daha fazla sebze ve meyve yemek',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '5',
            'title'     =>  'Ешьте больше овощей и фруктов',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '5',
            'title'     =>  'Essen Sie mehr Gemüse und Obst',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '5',
            'title'     =>  'Comer más verduras y frutas',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '5',
            'title'     =>  '多吃蔬菜和水果',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '5',
            'title'     =>  'أكثر من تناول الخضروات والفاكهة',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '6',
            'title'     =>  'Cut back on soft drinks',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '6',
            'title'     =>  'Réduire les boissons gazeuses',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '6',
            'title'     =>  'Alkolsüz içecekleri azaltın',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '6',
            'title'     =>  'Сократите потребление безалкогольных напитков',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '6',
            'title'     =>  'Reduzieren Sie Softdrinks',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '6',
            'title'     =>  'Reduzca el consumo de refrescos',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '6',
            'title'     =>  '减少软饮料',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '6',
            'title'     =>  'أقلل من المشروبات الغازية',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '7',
            'title'     =>  'If I get sick, I do not delay in seeking treatment and keep it',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '7',
            'title'     =>  'Si je tombe malade, je ne tarde pas à me faire soigner et je le garde',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '7',
            'title'     =>  'Hastalanırsam tedaviyi geciktirmem ve tedaviyi sürdürürüm.',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '7',
            'title'     =>  'Если я заболею, я не откладываю обращение за лечением и держу его',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '7',
            'title'     =>  'Wenn ich krank werde, zögere ich nicht mit der Behandlung und behalte sie',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '7',
            'title'     =>  'Si me enfermo, no tardo en buscar tratamiento y lo guardo',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '7',
            'title'     =>  '如果我生病了，我不会延迟求医并坚持下去',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '7',
            'title'     =>  'إذا مرضت فلا أتآخر في طلب العلاج وأحافظ عليه',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '8',
            'title'     =>  'I keep track of information related to food, health and fitness',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '8',
            'title'     =>  "Je garde une trace des informations relatives à l'alimentation, la santé et la forme physique",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '8',
            'title'     =>  'Gıda, sağlık ve fitness ile ilgili bilgileri takip ederim',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '8',
            'title'     =>  'Я отслеживаю информацию о еде, здоровье и фитнесе',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '8',
            'title'     =>  'Ich verfolge Informationen zu Ernährung, Gesundheit und Fitness',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '8',
            'title'     =>  'Realizo un seguimiento de la información relacionada con la alimentación, la salud y el estado físico.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '8',
            'title'     =>  '我会跟踪与食物、健康和健身相关的信息',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '8',
            'title'     =>  'أتابع بإستمرار المعلومات المتعلقة بالغذاء والصحة واللياقة',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '9',
            'title'     =>  'Keep the heart muscle strong',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '9',
            'title'     =>  'Gardez le muscle cardiaque fort',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '9',
            'title'     =>  'Kalp kasını güçlü tut',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '9',
            'title'     =>  'Держите сердечную мышцу сильной',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '9',
            'title'     =>  'Halte den Herzmuskel stark',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '9',
            'title'     =>  'Mantenga fuerte el músculo cardíaco',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '9',
            'title'     =>  '保持心肌强壮',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '9',
            'title'     =>  'أحافظ علي تقوية عضلة القلب',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '10',
            'title'     =>  'I do active walking',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '10',
            'title'     =>  'je fais de la marche active',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '10',
            'title'     =>  'aktif yürüyüş yaparım',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '10',
            'title'     =>  'Я активно гуляю',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '10',
            'title'     =>  'Ich gehe aktiv spazieren',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '10',
            'title'     =>  'Yo hago caminata activa',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '10',
            'title'     =>  '我积极步行',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '4',
            'q_id'      =>  '10',
            'title'     =>  'أمارس رياضة المشي بشكل فعال',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '1',
            'title'     =>  'I have time with parents and children without interruptions or phones',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '1',
            'title'     =>  "J'ai du temps avec les parents et les enfants sans interruption ni téléphone",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '1',
            'title'     =>  'Ebeveynler ve çocuklarla kesintisiz veya telefonsuz vakit geçiriyorum.',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '1',
            'title'     =>  'Я провожу время с родителями и детьми без перерывов и телефонов',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '1',
            'title'     =>  'Ich habe Zeit mit Eltern und Kindern ohne Unterbrechungen oder Telefone',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '1',
            'title'     =>  'Tengo tiempo con padres e hijos sin interrupciones ni teléfonos.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '1',
            'title'     =>  '我有时间和父母和孩子在一起，不受打扰或打电话',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '1',
            'title'     =>  'لدي وقت مع الأهل والأولاد بدون مقاطعات ولا هواتف',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '2',
            'title'     =>  'Always connected with my father and mother in her life or after their death',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '2',
            'title'     =>  'Toujours connecté avec mon père et ma mère dans sa vie ou après leur mort',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '2',
            'title'     =>  'Babam ve annemle hayatında veya ölümlerinden sonra her zaman bağlantıdayım.',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '2',
            'title'     =>  'Всегда был связан с моими отцом и матерью в ее жизни или после их смерти',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '2',
            'title'     =>  'Immer verbunden mit meinem Vater und meiner Mutter in ihrem Leben oder nach ihrem Tod',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '2',
            'title'     =>  'Siempre conectado con mi padre y mi madre en su vida o después de su muerte.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '2',
            'title'     =>  '在父亲和母亲的生前或死后始终与她保持联系',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '2',
            'title'     =>  'بار بأبي وأمي في حياتها أو بعد وفاتهما',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '3',
            'title'     =>  'I read constantly about raising children and understand the age stages',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '3',
            'title'     =>  "Je lis constamment sur l'éducation des enfants et je comprends les âges",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '3',
            'title'     =>  'Çocuk yetiştirme hakkında sürekli okuyorum ve yaş aşamalarını anlıyorum',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '3',
            'title'     =>  'Постоянно читаю о воспитании детей и понимаю возрастные этапы',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '3',
            'title'     =>  'Ich lese ständig über Kindererziehung und verstehe die Altersstufen',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '3',
            'title'     =>  'Leo constantemente sobre la crianza de los hijos y entiendo las etapas de la edad.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '3',
            'title'     =>  '我经常阅读有关抚养孩子的信息并了解年龄阶段',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '3',
            'title'     =>  'أقرأ بإستمرار عن تربية الأبناء وأتفهم المراحل العمرية',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '4',
            'title'     =>  'When I am busy at work, I compensate my family with gifts or entertainment',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '4',
            'title'     =>  'Lorsque je suis occupé au travail, je rémunère ma famille avec des cadeaux ou des divertissements',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '4',
            'title'     =>  'İşle meşgul olduğumda, aileme hediyeler veya eğlence ile tazminat öderim.',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '4',
            'title'     =>  'Когда я занят на работе, я компенсирую своей семье подарки или развлечения.',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '4',
            'title'     =>  'Wenn ich bei der Arbeit beschäftigt bin, entschädige ich meine Familie mit Geschenken oder Unterhaltung',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '4',
            'title'     =>  'Cuando estoy ocupado en el trabajo, recompenso a mi familia con obsequios o entretenimiento.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '4',
            'title'     =>  '当我忙于工作时，我会用礼物或招待来补偿我的家人',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '4',
            'title'     =>  'عند إنشغالي في العمل أعوض أهلي إما بهدايا أو ترفيه',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '5',
            'title'     =>  'I keep my wombs out constantly, at least by phone, and I visit them on non-famous occasions (and I know the danger of surrogacy)',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '5',
            'title'     =>  "Je garde constamment mes utérus à l'extérieur, au moins par téléphone, et je leur rends visite à des occasions peu célèbres (et je connais le danger de la maternité de substitution)",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '5',
            'title'     =>  'Rahimlerimi en azından telefonla sürekli dışarıda tutuyorum ve onları ünlü olmayan durumlarda ziyaret ediyorum (ve taşıyıcı anneliğin tehlikesini biliyorum)',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '5',
            'title'     =>  'Я постоянно держу свои матки снаружи, по крайней мере, по телефону, и я навещаю их в не известных случаях (и я знаю опасность суррогатного материнства)',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '5',
            'title'     =>  'Ich halte meine Gebärmutter ständig draußen, zumindest telefonisch, und besuche sie bei nicht berühmten Anlässen (und ich kenne die Gefahr der Leihmutterschaft)',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '5',
            'title'     =>  'Mantengo mis úteros fuera constantemente, al menos por teléfono, y los visito en ocasiones no famosas (y conozco el peligro de la subrogación).',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '5',
            'title'     =>  '我经常让我的子宫离开，至少通过电话，我在不知名的场合拜访他们（我知道代孕的危险）',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '5',
            'title'     =>  'أصل أرحامي بإستمرار علي الأقل هاتفيا وأزورهم في غير مناسبات شهيرة (وأعرف خطورة قاطع الأرحام)',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '6',
            'title'     =>  'I help the needy in my family',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '6',
            'title'     =>  "J'aide les nécessiteux de ma famille",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '6',
            'title'     =>  'ailemdeki muhtaçlara yardım ederim',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '6',
            'title'     =>  'Я помогаю нуждающимся в своей семье',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '6',
            'title'     =>  'Ich helfe den Bedürftigen in meiner Familie',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '6',
            'title'     =>  'Ayudo a los necesitados de mi familia',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '6',
            'title'     =>  '我帮助家里有需要的人',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '6',
            'title'     =>  'أساعد المحتاجين في عائلتي',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '7',
            'title'     =>  'We have a regular family meeting, fund or family association',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '7',
            'title'     =>  'Nous avons une réunion de famille régulière, un fonds ou une association familiale',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '7',
            'title'     =>  'Düzenli bir aile toplantımız, fonumuz veya aile derneğimiz var',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '7',
            'title'     =>  'У нас есть регулярное семейное собрание, фонд или семейное объединение',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '7',
            'title'     =>  'Wir haben ein regelmäßiges Familientreffen, Fonds oder Familienverein',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '7',
            'title'     =>  'Tenemos una reunión familiar regular, un fondo o una asociación familiar.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '7',
            'title'     =>  '我们有定期的家庭会议、基金或家庭协会',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '7',
            'title'     =>  'لدينا إجتماع عائلي منتظم أو صندوق أو جمعية عائلية',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '8',
            'title'     =>  'Read or take marital happiness courses',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '8',
            'title'     =>  'Lire ou suivre des cours sur le bonheur conjugal',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '8',
            'title'     =>  'Evlilik mutluluğu kurslarını okuyun veya alın',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '8',
            'title'     =>  'Прочтите или пройдите курсы семейного счастья',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '8',
            'title'     =>  'Lies oder nimm an Eheglückskursen teil',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '8',
            'title'     =>  'Leer o tomar cursos de felicidad conyugal',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '8',
            'title'     =>  '阅读或参加婚姻幸福课程',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '8',
            'title'     =>  'أقرأ أو أحضر دورات في السعادة الزوجية',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '9',
            'title'     =>  'I simply pass on what I have learned from the humours to my family members (emotional participation)',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '9',
            'title'     =>  "Je transmets simplement ce que j'ai appris des humeurs aux membres de ma famille (participation émotionnelle)",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '9',
            'title'     =>  'Sadece mizahlardan öğrendiklerimi aile üyelerime aktarırım (duygusal katılım)',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '9',
            'title'     =>  'Я просто передаю то, что я узнал из юмора, членам моей семьи (эмоциональное участие)',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '9',
            'title'     =>  'Ich gebe das, was ich aus den Humoren gelernt habe, einfach an meine Familienmitglieder weiter (emotionale Teilhabe)',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '9',
            'title'     =>  'Simplemente transmito lo que he aprendido de los humores a los miembros de mi familia (participación emocional)',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '9',
            'title'     =>  '我只是将我从幽默中学到的东西传递给我的家人（情感参与）',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '9',
            'title'     =>  'أنقل ما تعلمته من مهازات بشكل بسيط لأفراد عائلتي (المشاركة الوجدانية)',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '10',
            'title'     =>  'The strongest friendships I have are from within the family',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '10',
            'title'     =>  "Les amitiés les plus fortes que j'ai sont au sein de la famille",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '10',
            'title'     =>  'Sahip olduğum en güçlü dostluklar aile içinden',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '10',
            'title'     =>  'Самая крепкая дружба у меня внутри семьи',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '10',
            'title'     =>  'Die stärksten Freundschaften, die ich habe, sind innerhalb der Familie',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '10',
            'title'     =>  'Las amistades más fuertes que tengo son dentro de la familia.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '10',
            'title'     =>  '我拥有的最牢固的友谊来自家庭内部',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '5',
            'q_id'      =>  '10',
            'title'     =>  'أقوي الصداقات لدي من داخل العائلة',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '1',
            'title'     =>  'I have an active participation in my neighborhood',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '1',
            'title'     =>  "J'ai une participation active dans mon quartier",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '1',
            'title'     =>  'Mahallemde aktif bir katılımım var',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '1',
            'title'     =>  'Я активно участвую в жизни моего района',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '1',
            'title'     =>  'Ich beteilige mich aktiv in meiner Nachbarschaft',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '1',
            'title'     =>  'Tengo una participación activa en mi barrio',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '1',
            'title'     =>  '我积极参与我的社区',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '1',
            'title'     =>  'لدي مشاركة فعالة في الحي الذي أسكنه',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '2',
            'title'     =>  'I ask and check on the neighbors when they are away, visit them, share their occasions and be loved by them',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '2',
            'title'     =>  'Je demande et vérifie les voisins quand ils sont absents, leur rends visite, partage leurs occasions et suis aimé par eux',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '2',
            'title'     =>  'Komşulara uzaktayken sorar, onları kontrol eder, onları ziyaret eder, fırsatlarını paylaşır ve onlar tarafından sevilirim.',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '2',
            'title'     =>  'Я спрашиваю и проверяю соседей, когда они уезжают, навещаю их, рассказываю об их событиях и люблю их',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '2',
            'title'     =>  'Ich frage und schaue nach den Nachbarn, wenn sie weg sind, besuche sie, teile ihre Anlässe und werde von ihnen geliebt',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '2',
            'title'     =>  'Pregunto y reviso a los vecinos cuando están fuera, los visito, comparto sus ocasiones y me quieren',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '2',
            'title'     =>  '我在邻居外出时询问并检查他们，拜访他们，分享他们的场合并被他们所爱',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '2',
            'title'     =>  'أسأل وأتفقد الجيران عند الغياب وأزورهم وأشاركهم مناسباتهم ومحبوب لديهم',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '3',
            'title'     =>  'I have in my phone more than 500 registered numbers that I know and they know me',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '3',
            'title'     =>  "J'ai dans mon téléphone plus de 500 numéros enregistrés que je connais et ils me connaissent",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '3',
            'title'     =>  "Telefonumda bildiğim ve beni tanıyan 500'den fazla kayıtlı numara var",
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '3',
            'title'     =>  'У меня в телефоне более 500 зарегистрированных номеров, которые я знаю, и они меня знают',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '3',
            'title'     =>  'Ich habe in meinem Telefon mehr als 500 registrierte Nummern, die ich kenne und die mich kennen',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '3',
            'title'     =>  'Tengo en mi teléfono más de 500 números registrados que conozco y me conocen',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '3',
            'title'     =>  '我的手机里有超过 五百 个我认识的注册号码，他们也认识我',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '3',
            'title'     =>  'لدي في هاتفي أكثر من ٥٠٠ رقم مسجل أعرفهم ويعرفونني',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '4',
            'title'     =>  'Read or take courses in the art of building relationships',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '4',
            'title'     =>  "Lire ou suivre des cours sur l'art de nouer des relations",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '4',
            'title'     =>  'İlişki kurma sanatında okuyun veya kurslara katılın',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '4',
            'title'     =>  'Прочтите или пройдите курсы по искусству построения отношений',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '4',
            'title'     =>  'Lesen oder belegen Sie Kurse in der Kunst des Aufbaus von Beziehungen',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '4',
            'title'     =>  'Leer o tomar cursos sobre el arte de construir relaciones.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '4',
            'title'     =>  '阅读或学习建立关系的艺术课程',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '4',
            'title'     =>  'أقرأ أو أحضر دورات في فن بناء العلاقات',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '5',
            'title'     =>  'I reserve the right of the neighbor',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '5',
            'title'     =>  'Je me réserve le droit du voisin',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '5',
            'title'     =>  'komşunun hakkını saklı tutarım',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '5',
            'title'     =>  'Оставляю за собой право соседа',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '5',
            'title'     =>  'Ich behalte mir das Recht des Nachbarn vor',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '5',
            'title'     =>  'Me reservo el derecho del vecino.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '5',
            'title'     =>  '我保留邻居的权利',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '5',
            'title'     =>  'أحفظ حق الجار',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '6',
            'title'     =>  'Subscribe to a club',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '6',
            'title'     =>  "S'inscrire à un club",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '6',
            'title'     =>  'Bir kulübe abone ol',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '6',
            'title'     =>  'Подпишитесь на клуб',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '6',
            'title'     =>  'Einen Club abonnieren',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '6',
            'title'     =>  'Suscríbete a un club',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '6',
            'title'     =>  '订阅俱乐部',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '6',
            'title'     =>  'مشترك في أحد النوادي',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '7',
            'title'     =>  'I can make new friends easily and inexpensively',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '7',
            'title'     =>  'Je peux me faire de nouveaux amis facilement et à peu de frais',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '7',
            'title'     =>  'Kolayca ve ucuza yeni arkadaşlar edinebilirim',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '7',
            'title'     =>  'Я могу легко и недорого завести новых друзей',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '7',
            'title'     =>  'Ich kann einfach und günstig neue Freunde finden',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '7',
            'title'     =>  'Puedo hacer nuevos amigos fácil y económicamente',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '7',
            'title'     =>  '我可以轻松且廉价地结交新朋友',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '7',
            'title'     =>  'أستطيع أن أكون صداقات جديدة بسهولة وبدون تكلف',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '8',
            'title'     =>  'I can lead a student election campaign or a local council',
            'lang'      =>  'en',
        ]);
         WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '8',
            'title'     =>  'Je peux mener une campagne électorale étudiante ou un conseil municipal',
            'lang'      =>  'fr',
        ]);
          WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '8',
            'title'     =>  'Bir öğrenci seçim kampanyasına veya yerel konseye liderlik edebilirim',
            'lang'      =>  'tr',
        ]);
           WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '8',
            'title'     =>  'Я могу возглавить студенческую избирательную кампанию или местный совет',
            'lang'      =>  'ru',
        ]);
            WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '8',
            'title'     =>  'Ich kann einen Studentenwahlkampf oder einen Gemeinderat leiten',
            'lang'      =>  'de',
        ]);
             WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '8',
            'title'     =>  'Puedo liderar una campaña electoral estudiantil o un consejo local.',
            'lang'      =>  'es',
        ]);
              WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '8',
            'title'     =>  '我可以领导学生竞选活动或地方议会',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '8',
            'title'     =>  'أستطيع أن أقود حملة إنتخابية طلابية أو مجلس محلي',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '9',
            'title'     =>  'One of my ambitions is to become a governor, minister, diplomat or president',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '9',
            'title'     =>  'Une de mes ambitions est de devenir gouverneur, ministre, diplomate ou président',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '9',
            'title'     =>  'Hedeflerimden biri vali, bakan, diplomat veya cumhurbaşkanı olmak.',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '9',
            'title'     =>  'Одно из моих стремлений - стать губернатором, министром, дипломатом или президентом.',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '9',
            'title'     =>  'Einer meiner Ambitionen ist es, Gouverneur, Minister, Diplomat oder Präsident zu werden',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '9',
            'title'     =>  'Una de mis ambiciones es convertirme en gobernador, ministro, diplomático o presidente.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '9',
            'title'     =>  '我的抱负之一是成为州长、部长、外交官或总统',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '9',
            'title'     =>  'من طموحاتي أن أصبح محافظ أو وزير أو دبلوماسي أو رئيس',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '10',
            'title'     =>  'I have relationships with important people',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '10',
            'title'     =>  "J'ai des relations avec des personnes importantes",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '10',
            'title'     =>  'Önemli insanlarla ilişkilerim var',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '10',
            'title'     =>  'У меня отношения с важными людьми',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '10',
            'title'     =>  'Ich habe Beziehungen zu wichtigen Menschen',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '10',
            'title'     =>  'Tengo relaciones con personas importantes',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '10',
            'title'     =>  '我和重要的人有关系',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '6',
            'q_id'      =>  '10',
            'title'     =>  'لدي علاقات بشخصيات هامة',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '1',
            'title'     =>  'I attend a scientific conference in my field every 3 years',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '1',
            'title'     =>  "J'assiste à une conférence scientifique dans mon domaine tous les 3 ans",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '1',
            'title'     =>  'Her 3 yılda bir alanımla ilgili bilimsel bir konferansa katılıyorum',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '1',
            'title'     =>  'Я посещаю научную конференцию по своей специальности каждые 3 года.',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '1',
            'title'     =>  'Ich besuche alle 3 Jahre eine wissenschaftliche Konferenz in meinem Fachgebiet',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '1',
            'title'     =>  'Asisto a una conferencia científica en mi campo cada 3 años.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '1',
            'title'     =>  '我每 三 年参加一次我所在领域的科学会议',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '1',
            'title'     =>  'أحضر مؤتمر علمي في مجال تخصصي كل ٣ سنوات',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '2',
            'title'     =>  'I am learning personally or professionally in the field that I love',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '2',
            'title'     =>  "J'apprends personnellement ou professionnellement dans le domaine que j'aime",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '2',
            'title'     =>  'Sevdiğim alanda kişisel veya profesyonel olarak öğreniyorum',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '2',
            'title'     =>  'Я учусь лично или профессионально в той области, которую люблю',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '2',
            'title'     =>  'Ich lerne persönlich oder beruflich in dem Bereich, den ich liebe',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '2',
            'title'     =>  'Estoy aprendiendo personal o profesionalmente en el campo que amo',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '2',
            'title'     =>  '我正在我喜欢的领域进行个人或专业学习',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '2',
            'title'     =>  'أتعلم ذاتيا أو تخصصيا في المجال الذي أحبه',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '3',
            'title'     =>  'I attend courses in my field of specialization and constantly progress in it',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '3',
            'title'     =>  'Je suis des cours dans mon domaine de spécialisation et progresse constamment dans celui-ci',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '3',
            'title'     =>  'Uzmanlık alanımla ilgili kurslara katılıyorum ve bu alanda sürekli ilerleme kaydediyorum.',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '3',
            'title'     =>  'Я хожу на курсы по своей специальности и постоянно совершенствуюсь в ней.',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '3',
            'title'     =>  'Ich besuche Kurse in meinem Spezialgebiet und mache mich darin ständig weiter',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '3',
            'title'     =>  'Asisto a cursos en mi campo de especialización y progreso constantemente en él.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '3',
            'title'     =>  '我参加我专业领域的课程并不断进步',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '3',
            'title'     =>  'أحضر دورات في مجال تخصصي وأتقدم فيه بإستمرار',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '4',
            'title'     =>  'It is my ambition to be the owner of my own business',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '4',
            'title'     =>  "C'est mon ambition d'être propriétaire de ma propre entreprise",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '4',
            'title'     =>  'Kendi işimin sahibi olmak benim hırsım',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '4',
            'title'     =>  'Я стремлюсь быть владельцем собственного бизнеса',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '4',
            'title'     =>  'Es ist mein Ehrgeiz, Inhaber eines eigenen Unternehmens zu sein',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '4',
            'title'     =>  'Mi ambición es ser dueña de mi propio negocio.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '4',
            'title'     =>  '成为自己企业的所有者是我的抱负',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '4',
            'title'     =>  'من طموحي أن أكون صاحب عمل خاص بي',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '5',
            'title'     =>  'I love books about starting a business',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '5',
            'title'     =>  "J'adore les livres sur la création d'entreprise",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '5',
            'title'     =>  'İş kurmakla ilgili kitapları seviyorum',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '5',
            'title'     =>  'Я люблю книги о начале бизнеса',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '5',
            'title'     =>  'Ich liebe Bücher über die Gründung eines Unternehmens',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '5',
            'title'     =>  'Me encantan los libros sobre cómo iniciar un negocio.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '5',
            'title'     =>  '我喜欢关于创业的书',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '5',
            'title'     =>  'أحب الكتب التي تتحدث عن تأسيس الآعمال',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '6',
            'title'     =>  'I can improve in my field',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '6',
            'title'     =>  "Je peux m'améliorer dans mon domaine",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '6',
            'title'     =>  'alanımda kendimi geliştirebilirim',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '6',
            'title'     =>  'Я могу совершенствоваться в своей области',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '6',
            'title'     =>  'Ich kann mich in meinem Bereich verbessern',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '6',
            'title'     =>  'Puedo mejorar en mi campo',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '6',
            'title'     =>  '我可以在我的领域有所提高',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '6',
            'title'     =>  'أستطيع أن أطور في مجال عملي',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '7',
            'title'     =>  'I can lead a team',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '7',
            'title'     =>  'je peux diriger une équipe',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '7',
            'title'     =>  'bir takıma liderlik edebilirim',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '7',
            'title'     =>  'Я могу возглавить команду',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '7',
            'title'     =>  'Ich kann ein Team führen',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '7',
            'title'     =>  'Puedo liderar un equipo',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '7',
            'title'     =>  '我可以带领团队',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '7',
            'title'     =>  'أستطيع قيادة فريق عمل',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '8',
            'title'     =>  'I can stand the time of crisis to accomplish the task assigned to me',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '8',
            'title'     =>  "Je peux supporter le temps de crise pour accomplir la tâche qui m'a été assignée",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '8',
            'title'     =>  'Bana verilen görevi yerine getirmek için kriz zamanına dayanabilirim',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '8',
            'title'     =>  'Я выдержу кризисные времена, чтобы выполнить поставленную передо мной задачу',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '8',
            'title'     =>  'Ich kann die Zeit der Krise ertragen, um die mir übertragene Aufgabe zu erfüllen',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '8',
            'title'     =>  'Puedo soportar el tiempo de crisis para cumplir con la tarea que se me ha asignado.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '8',
            'title'     =>  '我能经受住危机时刻完成分配给我的任务',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '8',
            'title'     =>  'أستطيع التحمل وقت الأزمات لإنجاز المهمة الموكلة لي',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '9',
            'title'     =>  'I follow what is new in my field of work and I work hard to enjoy my time at work',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '9',
            'title'     =>  'Je suis les nouveautés dans mon domaine de travail et je travaille dur pour profiter de mon temps au travail',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '9',
            'title'     =>  'İş alanımdaki yenilikleri takip ederim ve işte geçirdiğim zamandan keyif almak için çok çalışırım.',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '9',
            'title'     =>  'Я слежу за новинками в моей сфере работы и много работаю, чтобы получать удовольствие от работы',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '9',
            'title'     =>  'Ich verfolge das Neue in meinem Arbeitsbereich und arbeite hart, um meine Zeit bei der Arbeit zu genießen',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '9',
            'title'     =>  'Sigo las novedades en mi campo de trabajo y trabajo duro para disfrutar de mi tiempo en el trabajo.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '9',
            'title'     =>  '我关注我工作领域的新鲜事物，并努力工作以享受我的工作时间',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '9',
            'title'     =>  'أتابع الجديد في مجال عملي وأتفنن لكي أستمتع بوقتي في العمل',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '10',
            'title'     =>  'I watch my Lord in my work as much as I can and I have principles',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '10',
            'title'     =>  "Je regarde mon Seigneur dans mon travail autant que je peux et j'ai des principes",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '10',
            'title'     =>  'İşimde elimden geldiğince Rabbime dikkat ederim ve prensiplerim vardır.',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '10',
            'title'     =>  'Я наблюдаю за своим Господом в моей работе, насколько могу, и у меня есть принципы',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '10',
            'title'     =>  'Ich beobachte meinen Herrn bei meiner Arbeit so gut ich kann und habe Prinzipien',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '10',
            'title'     =>  'Veo a mi Señor en mi trabajo tanto como puedo y tengo principios',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '10',
            'title'     =>  '我在我的工作中尽可能多地观察我的主，我有原则',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '7',
            'q_id'      =>  '10',
            'title'     =>  'أراقب ربي في عملي قدر إستطاعتي وأمتلك مبادئ',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '1',
            'title'     =>  'My monthly income is better than it was a year ago',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '1',
            'title'     =>  "Mon revenu mensuel est meilleur qu'il y a un an",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '1',
            'title'     =>  'Aylık gelirim bir yıl öncesine göre daha iyi',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '1',
            'title'     =>  'Мой ежемесячный доход лучше, чем год назад',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '1',
            'title'     =>  'Mein monatliches Einkommen ist besser als vor einem Jahr',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '1',
            'title'     =>  'Mi ingreso mensual es mejor que hace un año',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '1',
            'title'     =>  '我的月收入比一年前好',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '1',
            'title'     =>  'دخلي الشهري أفضل مما كان عليه قبل سنة',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '2',
            'title'     =>  'I save at least 10% of my income',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '2',
            'title'     =>  "J'économise au moins 10% de mes revenus",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '2',
            'title'     =>  "Gelirimin en az %10'unu biriktiriyorum",
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '2',
            'title'     =>  'Я откладываю не менее 10% своего дохода',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '2',
            'title'     =>  'Ich spare mindestens 10% meines Einkommens',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '2',
            'title'     =>  'Ahorro al menos el 10% de mis ingresos',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '2',
            'title'     =>  '我至少存入收入的 十%',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '2',
            'title'     =>  'أدخر من دخلي ١٠٪ علي الأقل',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '3',
            'title'     =>  'Read or attend courses on the art of budget management and how to become an investor',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '3',
            'title'     =>  "Lisez ou assistez à des cours sur l'art de la gestion budgétaire et comment devenir un investisseur",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '3',
            'title'     =>  'Bütçe yönetimi sanatı ve nasıl yatırımcı olunacağına dair kursları okuyun veya bunlara katılın',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '3',
            'title'     =>  'Прочтите или посетите курсы по искусству управления бюджетом и о том, как стать инвестором.',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '3',
            'title'     =>  'Lesen oder besuchen Sie Kurse über die Kunst des Budgetmanagements und wie man Investor wird',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '3',
            'title'     =>  'Leer o asistir a cursos sobre el arte de la gestión presupuestaria y cómo convertirse en inversor.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '3',
            'title'     =>  '阅读或参加有关预算管理艺术以及如何成为投资者的课程',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '3',
            'title'     =>  'أقرأ أو أحضر دورات في فن إدارة الميزانية وكيف أصبح مستثمر',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '4',
            'title'     =>  'I have a plan to double my holdings every 5 years',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '4',
            'title'     =>  "J'ai un plan pour doubler mes avoirs tous les 5 ans",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '4',
            'title'     =>  'Her 5 yılda bir holdinglerimi ikiye katlama planım var.',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '4',
            'title'     =>  'У меня есть план удваивать свои активы каждые 5 лет',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '4',
            'title'     =>  'Ich habe vor, meine Bestände alle 5 Jahre zu verdoppeln
',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '4',
            'title'     =>  'Tengo un plan para duplicar mis tenencias cada 5 años.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '4',
            'title'     =>  '我计划每 五 年将我的持股翻一番',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '4',
            'title'     =>  'لدي خطة لمضاعفة ممتلكاتي كل ٥ سنوات',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '5',
            'title'     =>  'I control my expenses and differentiate between need and desire',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '5',
            'title'     =>  'Je contrôle mes dépenses et fais la différence entre besoin et désir',
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '5',
            'title'     =>  'Harcamalarımı kontrol ediyorum ve ihtiyaç ile arzu arasında ayrım yapıyorum',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '5',
            'title'     =>  'Я контролирую свои расходы и различаю потребность и желание',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '5',
            'title'     =>  'Ich kontrolliere meine Ausgaben und unterscheide zwischen Bedarf und Wunsch',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '5',
            'title'     =>  'Controlo mis gastos y distingo entre necesidad y deseo',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '5',
            'title'     =>  '我控制我的开支并区分需要和欲望',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '5',
            'title'     =>  'أتحكم في نفقاتي وأفرق بين الحاجة والرغبة',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '6',
            'title'     =>  'I only buy what I have',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '6',
            'title'     =>  "je n'achète que ce que j'ai",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '6',
            'title'     =>  'sadece sahip olduklarımı satın alırım',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '6',
            'title'     =>  'Я покупаю только то, что у меня есть',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '6',
            'title'     =>  'Ich kaufe nur das was ich habe',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '6',
            'title'     =>  'Solo compro lo que tengo',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '6',
            'title'     =>  '我只买我有的',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '6',
            'title'     =>  'لا أشتري إلا ما أملك ثمنه',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '7',
            'title'     =>  'I can earn through the internet and treat time as money',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '7',
            'title'     =>  "Je peux gagner sur Internet et traiter le temps comme de l'argent",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '7',
            'title'     =>  'İnternet üzerinden kazanabilirim ve zamanı para olarak değerlendirebilirim',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '7',
            'title'     =>  'Я могу зарабатывать в Интернете и относиться ко времени как к деньгам',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '7',
            'title'     =>  'Ich kann über das Internet verdienen und Zeit als Geld behandeln',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '7',
            'title'     =>  'Puedo ganar a través de Internet y tratar el tiempo como dinero.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '7',
            'title'     =>  '我可以通过互联网赚钱，把时间当作金钱',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '7',
            'title'     =>  'أستطيع أن أكسب من خلال الإنترنت وأتعامل مع الوقت علي أنه مال',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '8',
            'title'     =>  'I have an account for me and all my children in the bank',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '8',
            'title'     =>  "J'ai un compte pour moi et tous mes enfants à la banque",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '8',
            'title'     =>  'Bankada benim ve tüm çocuklarım için bir hesabım var.',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '8',
            'title'     =>  'У меня есть счет в банке для меня и всех моих детей',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '8',
            'title'     =>  'Ich habe ein Konto für mich und alle meine Kinder auf der Bank',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '8',
            'title'     =>  'Tengo una cuenta para mí y para todos mis hijos en el banco.',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '8',
            'title'     =>  '我和我所有的孩子在银行都有一个账户',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '8',
            'title'     =>  'لدي حساب لي ولجميع أولادي في البنك',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '9',
            'title'     =>  'I always look for ways to invest the saved money',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '9',
            'title'     =>  "Je cherche toujours des moyens d'investir l'argent économisé",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '9',
            'title'     =>  'Her zaman biriktirdiğim parayı yatırmanın yollarını ararım',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '9',
            'title'     =>  'Всегда ищу способы вложить сэкономленные деньги',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '9',
            'title'     =>  'Ich suche immer nach Möglichkeiten, das gesparte Geld zu investieren',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '9',
            'title'     =>  'Siempre busco formas de invertir el dinero ahorrado',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '9',
            'title'     =>  '我总是想办法把省下来的钱投资',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '9',
            'title'     =>  'أبحث وعندي طرق دائما لإستثمار المال المدخر',
            'lang'      =>  'ar',
        ]);

        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '10',
            'title'     =>  'I never borrowed before',
            'lang'      =>  'en',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '10',
            'title'     =>  "je n'ai jamais emprunté avant",
            'lang'      =>  'fr',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '10',
            'title'     =>  'daha önce hiç ödünç almadım',
            'lang'      =>  'tr',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '10',
            'title'     =>  'Я никогда не занимал раньше',
            'lang'      =>  'ru',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '10',
            'title'     =>  'Ich habe noch nie ausgeliehen',
            'lang'      =>  'de',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '10',
            'title'     =>  'Nunca pedí prestado antes',
            'lang'      =>  'es',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '10',
            'title'     =>  '我以前从未借过',
            'lang'      =>  'cn',
        ]);
        WheelQues::create([
            'type'      =>  '8',
            'q_id'      =>  '10',
            'title'     =>  'لم أقترض من قبل',
            'lang'      =>  'ar',
        ]);
    }
}
