<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Country;
use App\Models\CountryEvent;

class CountryEventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CountryEvent::create( [
            'country_id'    =>'63',
            'country_iso'   =>'EG',
            'title_en'      =>'Holiday Christmas',
            'title_ar'      =>'عيد الميلاد المجيد',
            'title_fr'      =>"Noël de vacances",
            'title_tr'      =>"tatil noel",
            'title_ru'      =>"Праздник Рождества",
            'title_de'      =>"Feiertag Weihnachten",
            'title_es'      =>"Navidad navideña",
            'title_cn'      =>"假期圣诞节",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Nativity_of_christ.jpg/500px-Nativity_of_christ.jpg',
            'start_date'    =>'2021-01-07',
            'end_date'      => null
        ] );


        CountryEvent::create( [
            'country_id'    =>'63',
            'country_iso'   =>'EG',
            'title_en'      =>'January 25 Revolution and Police Day',
            'title_ar'      =>'ثورة ٢٥ يناير وعيد الشرطة',
            'title_fr'      =>"Fête de la Révolution et de la Police du 25 janvier",
            'title_tr'      =>"25 Ocak Devrim ve Polis Günü",
            'title_ru'      =>"25 января День революции и полиции",
            'title_de'      =>"25. Januar Revolution und Tag der Polizei",
            'title_es'      =>"25 de enero Día de la Revolución y la Policía",
            'title_cn'      =>"一 月 二十五 日革命和警察日",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Tahrir_Square_on_February_8_2011.png/560px-Tahrir_Square_on_February_8_2011.png',
            'start_date'    =>'2021-01-28',
            'end_date'      => null
        ] );   


        CountryEvent::create( [
            'country_id'    =>'63',
            'country_iso'   =>'EG',
            'title_en'      =>'Sinai Liberation day',
            'title_ar'      =>'عيد تحرير سيناء',
            'title_fr'      =>"Jour de libération du Sinaï",
            'title_tr'      =>"Sina Kurtuluş günü",
            'title_ru'      =>"День освобождения Синая",
            'title_de'      =>"Tag der Befreiung des Sinai",
            'title_es'      =>"Día de la Liberación del Sinaí",
            'title_cn'      =>"西奈解放日",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Egyptianbridge.jpg/440px-Egyptianbridge.jpg',
            'start_date'    =>'2021-04-29',
            'end_date'      => null
        ] );   


        CountryEvent::create( [
            'country_id'    =>'63',
            'country_iso'   =>'EG',
            'title_en'      =>'Labor Day',
            'title_ar'      =>'عيد العمال',
            'title_fr'      =>"Fête du travail",
            'title_tr'      =>"İşçi bayramı",
            'title_ru'      =>"День Труда",
            'title_de'      =>"Tag der Arbeit",
            'title_es'      =>"Día laboral",
            'title_cn'      =>"劳动节",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/1.Mai_2013_%288697603319%29.jpg/560px-1.Mai_2013_%288697603319%29.jpg',
            'start_date'    =>'2021-05-01',
            'end_date'      => null
        ] );

        CountryEvent::create( [
            'country_id'    =>'63',
            'country_iso'   =>'EG',
            'title_en'      =>'Easter of Sham El-Nessim',
            'title_ar'      =>'عيد شم النسيم',
            'title_fr'      =>"Pâques de Sham El-Nessim",
            'title_tr'      =>"Şam El-Nessim Paskalyası",
            'title_ru'      =>"Пасха Шам Эль-Нессима",
            'title_de'      =>"Ostern von Sham El-Nessim",
            'title_es'      =>"Pascua de Sham El-Nessim",
            'title_cn'      =>"的复活节",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2e/Arabic-Shells-Dips-Sauces-Dumplings-Appetizers-Vegetables-1626976.jpg/400px-Arabic-Shells-Dips-Sauces-Dumplings-Appetizers-Vegetables-1626976.jpg',
            'start_date'    =>'2021-05-03',
            'end_date'      => null
        ] ); 

        CountryEvent::create( [
            'country_id'    =>'63',
            'country_iso'   =>'EG',
            'title_en'      =>'Eid al-Fitr',
            'title_ar'      =>'عيد الفطر المبارك',
            'title_fr'      =>"Aïd el-Fitr",
            'title_tr'      =>"Eid al-fitr",
            'title_ru'      =>"Курбан-байрам",
            'title_de'      =>"Eid al-Fitr",
            'title_es'      =>"Eid al-Fitr",
            'title_cn'      =>"开斋节",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/5/5d/Sultan_Ahmed_Mosque_mahya3.jpg/560px-Sultan_Ahmed_Mosque_mahya3.jpg',
            'start_date'    =>'2021-05-12',
            'end_date'      =>'2021-05-16'
        ] );    

        CountryEvent::create( [
            'country_id'    =>'63',
            'country_iso'   =>'EG',
            'title_en'      =>'June 30 Revolution',
            'title_ar'      =>'ثورة ٣٠ يونيو',
            'title_fr'      =>"Révolution du 30 juin",
            'title_tr'      =>"30 Haziran Devrimi",
            'title_ru'      =>"30 июня революция",
            'title_de'      =>"30. Juni Revolution",
            'title_es'      =>"Revolución del 30 de junio",
            'title_cn'      =>"六 月 三十 日革命",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/Anti_Morsi_protest_march_at_28th_June_2013.jpg/600px-Anti_Morsi_protest_march_at_28th_June_2013.jpg',
            'start_date'    =>'2021-07-01',
            'end_date'      => null
        ] );  

        CountryEvent::create( [
            'country_id'    =>'63',
            'country_iso'   =>'EG',
            'title_en'      =>'Eid al-Adha',
            'title_ar'      =>'عيد الأضحى المبارك',
            'title_fr'      =>"Aïd al-Adha",
            'title_tr'      =>"Kurban Bayramı",
            'title_ru'      =>"Курбан-байрам",
            'title_de'      =>"Eid al-Adha",
            'title_es'      =>"Eid al-Adha",
            'title_cn'      =>"宰牲节",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/d/de/The_Badshahi_in_all_its_glory_during_the_Eid_Prayers.JPG/560px-The_Badshahi_in_all_its_glory_during_the_Eid_Prayers.JPG',
            'start_date'    =>'2021-07-17',
            'end_date'      =>'2021-07-23'
        ] );     

        CountryEvent::create( [
            'country_id'    =>'63',
            'country_iso'   =>'EG',
            'title_en'      =>'July 23 Revolution (Revolution Day)',
            'title_ar'      =>'ثورة ٢٣ يوليو (عيد الثورة)',
            'title_fr'      =>"Révolution du 23 juillet (Jour de la Révolution)",
            'title_tr'      =>"23 Temmuz Devrimi (Devrim Günü)",
            'title_ru'      =>"23 июля Революция (День революции)",
            'title_de'      =>"23. Juli Revolution (Tag der Revolution)",
            'title_es'      =>"23 de julio Revolución (Día de la Revolución)",
            'title_cn'      =>"七 月 二十三 日革命（革命日）",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/7/7a/Nasser_and_Naguib%2C_1954.jpg',
            'start_date'    =>'2021-07-24',
            'end_date'      => null
        ] ); 

        CountryEvent::create( [
            'country_id'    =>'63',
            'country_iso'   =>'EG',
            'title_en'      =>'Islamic New Year',
            'title_ar'      =>'رأس السنة الهجرية',
            'title_fr'      =>"Nouvel an islamique",
            'title_tr'      =>"İslami Yılbaşı",
            'title_ru'      =>"Исламский Новый год",
            'title_de'      =>"Islamisches Neujahr",
            'title_es'      =>"Año nuevo islámico",
            'title_cn'      =>"伊斯兰新年",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Madina_old.jpg/440px-Madina_old.jpg',
            'start_date'    =>'2021-08-12',
            'end_date'      => null
        ] );  

        CountryEvent::create( [
            'country_id'    =>'63',
            'country_iso'   =>'EG',
            'title_en'      =>'Armed Forces Day',
            'title_ar'      =>'عيد القوات المسلحة',
            'title_fr'      =>"Journée des forces armées",
            'title_tr'      =>"Silahlı Kuvvetler Günü",
            'title_ru'      =>"День Вооруженных Сил",
            'title_de'      =>"Tag der Bundeswehr",
            'title_es'      =>"día de las Fuerzas Armadas",
            'title_cn'      =>"武装部队日",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Octoberportal.jpg/400px-Octoberportal.jpg',
            'start_date'    =>'2021-10-06',
            'end_date'      => null
        ] );  

        CountryEvent::create( [
            'country_id'    =>'63',
            'country_iso'   =>'EG',
            'title_en'      =>"Prophet's Birthday",
            'title_ar'      =>'المولد النبوي الشريف',
            'title_fr'      =>"L'anniversaire du prophète",
            'title_tr'      =>"peygamberimizin doğum günü",
            'title_ru'      =>"День Рождения Пророка",
            'title_de'      =>"Geburtstag des Propheten",
            'title_es'      =>"Cumpleaños del Profeta",
            'title_cn'      =>"先知诞辰",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/3/3d/Muhammad2.png',
            'start_date'    =>'2021-10-18',
            'end_date'      => null
        ] );  

        CountryEvent::create( [
            'country_id'    =>'187',
            'country_iso'   =>'SA',
            'title_en'      =>'Eid al-Fitr',
            'title_ar'      =>'عيد الفطر المبارك',
            'title_fr'      =>"Aïd el-Fitr",
            'title_tr'      =>"Eid al-fitr",
            'title_ru'      =>"Курбан-байрам",
            'title_de'      =>"Eid al-Fitr",
            'title_es'      =>"Eid al-Fitr",
            'title_cn'      =>"开斋节",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Kaaba_mirror_edit_jj.jpg/600px-Kaaba_mirror_edit_jj.jpg',
            'start_date'    =>'2021-05-12',
            'end_date'      =>'2021-05-15'
        ] );  

        CountryEvent::create( [
            'country_id'    =>'187',
            'country_iso'   =>'SA',
            'title_en'      =>'Eid al-Adha',
            'title_ar'      =>'عيد الأضحى المبارك',
            'title_fr'      =>"Aïd al-Adha",
            'title_tr'      =>"Kurban Bayramı",
            'title_ru'      =>"Курбан-байрам",
            'title_de'      =>"Eid al-Adha",
            'title_es'      =>"Eid al-Adha",
            'title_cn'      =>"宰牲节",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/MasjidNabawi.jpg/600px-MasjidNabawi.jpg',
            'start_date'    =>'2021-07-19',
            'end_date'      =>'2021-07-22'
        ] );  

        CountryEvent::create( [
            'country_id'    =>'187',
            'country_iso'   =>'SA',
            'title_en'      =>'Kingdom National Day',
            'title_ar'      =>'اليوم الوطني للمملكة',
            'title_fr'      =>"Fête nationale du Royaume",
            'title_tr'      =>"Krallık Ulusal Günü",
            'title_ru'      =>"Национальный день королевства",
            'title_de'      =>"Nationalfeiertag des Königreichs",
            'title_es'      =>"Día Nacional del Reino",
            'title_cn'      =>"王国国庆日",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/8/81/Masmak_Fort_%2812753717253%29.jpg/677px-Masmak_Fort_%2812753717253%29.jpg',
            'start_date'    =>'2021-09-23',
            'end_date'      => null
        ] );  

        CountryEvent::create( [
            'country_id'    =>'38',
            'country_iso'   =>'CA',
            'title_en'      =>"New Year's Day",
            'title_ar'      =>'يوم رأس السنة الجديدة',
            'title_fr'      =>"Le jour de l'An",
            'title_tr'      =>"Yılbaşı",
            'title_ru'      =>"День нового года",
            'title_de'      =>"Neujahr",
            'title_es'      =>"Día de Año Nuevo",
            'title_cn'      =>"元旦",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/Wildsch%C3%B6nau_feiert_Neues_Jahr_03.jpg/560px-Wildsch%C3%B6nau_feiert_Neues_Jahr_03.jpg',
            'start_date'    =>'2021-01-01',
            'end_date'      => null
        ] ); 

        CountryEvent::create( [
            'country_id'    =>'38',
            'country_iso'   =>'CA',
            'title_en'      =>'family day',
            'title_ar'      =>'يوم الأسرة',
            'title_fr'      =>"journée familiale",
            'title_tr'      =>"aile Günü",
            'title_ru'      =>"день семьи",
            'title_de'      =>"Familientag",
            'title_es'      =>"dia familiar",
            'title_cn'      =>"家庭日",
            'type'          =>'vacation',
            'image'         =>'',
            'start_date'    =>'2021-02-15',
            'end_date'      => null
        ] ); 

        CountryEvent::create( [
            'country_id'    =>'38',
            'country_iso'   =>'CA',
            'title_en'      =>'The Great Friday',
            'title_ar'      =>'الجمعة العظيمة',
            'title_fr'      =>"Le grand vendredi",
            'title_tr'      =>"Büyük Cuma",
            'title_ru'      =>"Великая пятница",
            'title_de'      =>"Der große Freitag",
            'title_es'      =>"El gran viernes",
            'title_cn'      =>"伟大的星期五",
            'type'          =>'vacation',
            'image'         =>'',
            'start_date'    =>'2021-04-02',
            'end_date'      => null
        ] );  

        CountryEvent::create( [
            'country_id'    =>'38',
            'country_iso'   =>'CA',
            'title_en'      =>'Easter Holiday',
            'title_ar'      =>'عيد الفصح',
            'title_fr'      =>"Fête de Pâques",
            'title_tr'      =>"Paskalya tatil",
            'title_ru'      =>"Пасха",
            'title_de'      =>"Osterferien",
            'title_es'      =>"Pascua vacaciones",
            'title_cn'      =>"复活节假期",
            'type'          =>'vacation',
            'image'         =>'',
            'start_date'    =>'2021-04-05',
            'end_date'      => null
        ] ); 

        CountryEvent::create( [
            'country_id'    =>'38',
            'country_iso'   =>'CA',
            'title_en'      =>'victorian day',
            'title_ar'      =>'يوم فيكتوري',
            'title_fr'      =>"jour victorien",
            'title_tr'      =>"Viktorya günü",
            'title_ru'      =>"викторианский день",
            'title_de'      =>"viktorianischer Tag",
            'title_es'      =>"día victoriano",
            'title_cn'      =>"维多利亚时代",
            'type'          =>'vacation',
            'image'         =>'',
            'start_date'    =>'2021-05-24',
            'end_date'      => null
        ] );  

        CountryEvent::create( [
            'country_id'    =>'38',
            'country_iso'   =>'CA',
            'title_en'      =>'Canada Day',
            'title_ar'      =>'يوم كندا',
            'title_fr'      =>"jour du Canada",
            'title_tr'      =>"Kanada Günü",
            'title_ru'      =>"День Канады",
            'title_de'      =>"Kanada Tag",
            'title_es'      =>"Dia de Canada",
            'title_cn'      =>"加拿大国庆日",
            'type'          =>'vacation',
            'image'         =>'',
            'start_date'    =>'2021-07-01',
            'end_date'      => null
        ] ); 

        CountryEvent::create( [
            'country_id'    =>'38',
            'country_iso'   =>'CA',
            'title_en'      =>'civil holiday',
            'title_ar'      =>'العطلة المدنية',
            'title_fr'      =>"jour férié",
            'title_tr'      =>"resmi tatil",
            'title_ru'      =>"гражданский праздник",
            'title_de'      =>"bürgerlicher Feiertag",
            'title_es'      =>"feriado civil",
            'title_cn'      =>"公民假期",
            'type'          =>'vacation',
            'image'         =>'',
            'start_date'    =>'2021-08-02',
            'end_date'      => null
        ] ); 

        CountryEvent::create( [
            'country_id'    =>'38',
            'country_iso'   =>'CA',
            'title_en'      =>'Labor Day',
            'title_ar'      =>'عيد العمال',
            'title_fr'      =>"Fête du travail",
            'title_tr'      =>"İşçi bayramı",
            'title_ru'      =>"День Труда",
            'title_de'      =>"Tag der Arbeit",
            'title_es'      =>"Día laboral",
            'title_cn'      =>"劳动节",
            'type'          =>'vacation',
            'image'         =>'',
            'start_date'    =>'2021-09-06',
            'end_date'      => null
        ] );  

        CountryEvent::create( [
            'country_id'    =>'38',
            'country_iso'   =>'CA',
            'title_en'      =>'Thanksgiving Day',
            'title_ar'      =>'عيد الشكر',
            'title_fr'      =>"Jour de Thanksgiving",
            'title_tr'      =>"Şükran Günü",
            'title_ru'      =>"День Благодарения",
            'title_de'      =>"Erntedankfest",
            'title_es'      =>"día de Gracias",
            'title_cn'      =>"感恩节",
            'type'          =>'vacation',
            'image'         =>'',
            'start_date'    =>'2021-10-11',
            'end_date'      => null
        ] );  

        CountryEvent::create( [
            'country_id'    =>'38',
            'country_iso'   =>'CA',
            'title_en'      =>'Memorial Day',
            'title_ar'      =>'يوم الذكرى',
            'title_fr'      =>"Jour commémoratif",
            'title_tr'      =>"Anma Günü",
            'title_ru'      =>"день памяти",
            'title_de'      =>"Gedenktag",
            'title_es'      =>"Día Conmemorativo",
            'title_cn'      =>"纪念日",
            'type'          =>'vacation',
            'image'         =>'',
            'start_date'    =>'2021-11-11',
            'end_date'      => null
        ] ); 

        CountryEvent::create( [
            'country_id'    =>'38',
            'country_iso'   =>'CA',
            'title_en'      =>'Christmas Day',
            'title_ar'      =>'يوم عيد الميلاد',
            'title_fr'      =>"le jour de Noël",
            'title_tr'      =>"Noel günü",
            'title_ru'      =>"Рождество",
            'title_de'      =>"Weihnachtstag",
            'title_es'      =>"día de Navidad",
            'title_cn'      =>"圣诞节",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Nativity_of_christ.jpg/500px-Nativity_of_christ.jpg',
            'start_date'    =>'2021-12-25',
            'end_date'      => null
        ] ); 

        CountryEvent::create( [
            'country_id'    =>'38',
            'country_iso'   =>'CA',
            'title_en'      =>'Boxing Day',
            'title_ar'      =>'يوم الصناديق',
            'title_fr'      =>"Le lendemain de Noël",
            'title_tr'      =>"Boks Günü",
            'title_ru'      =>"День подарков",
            'title_de'      =>"2. Weihnachtsfeiertag",
            'title_es'      =>"Día de San Esteban",
            'title_cn'      =>"节礼日",
            'type'          =>'vacation',
            'image'         =>'',
            'start_date'    =>'2021-12-26',
            'end_date'      => null
        ] ); 

        CountryEvent::create( [
            'country_id'    =>'226',
            'country_iso'   =>'US',
            'title_en'      =>'Martin Luther King Day',
            'title_ar'      =>'يوم مارتن لوثر كينغ',
            'title_fr'      =>"jour de Martin Luther King",
            'title_tr'      =>"Martin Luther King Günü",
            'title_ru'      =>"День Мартина Лютера Кинга",
            'title_de'      =>"Martin Luther King Tag",
            'title_es'      =>"día de Martin Luther King",
            'title_cn'      =>"马丁路德金纪念日",
            'type'          =>'vacation',
            'image'         =>'',
            'start_date'    =>'2021-01-16',
            'end_date'      => null
        ] );  

        CountryEvent::create( [
            'country_id'    =>'226',
            'country_iso'   =>'US',
            'title_en'      =>"Washington's birthday and President's Day",
            'title_ar'      =>'عيد ميلاد واشنطن ويوم الرئيس',
            'title_fr'      =>"L'anniversaire de Washington et la fête du président",
            'title_tr'      =>"Washington'un doğum günü ve Başkanlık Günü",
            'title_ru'      =>"День рождения Вашингтона и День президента",
            'title_de'      =>"Washingtons Geburtstag und President's Day",
            'title_es'      =>"El cumpleaños de Washington y el día del presidente",
            'title_cn'      =>"华盛顿的生日和总统日",
            'type'          =>'vacation',
            'image'         =>'',
            'start_date'    =>'2021-02-3',
            'end_date'      => null
        ] ); 

        CountryEvent::create( [
            'country_id'    =>'226',
            'country_iso'   =>'US',
            'title_en'      =>"Christmas",
            'title_ar'      =>'عيد الميلاد',
            'title_fr'      =>"Noël",
            'title_tr'      =>"Noel",
            'title_ru'      =>"Рождество",
            'title_de'      =>"Weihnachten",
            'title_es'      =>"Navidad",
            'title_cn'      =>"圣诞节",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Nativity_of_christ.jpg/500px-Nativity_of_christ.jpg',
            'start_date'    =>'2021-12-24',
            'end_date'      => null
        ] );  

        CountryEvent::create( [
            'country_id'    =>'114',
            'country_iso'   =>'MA',
            'title_en'      =>"Presentation of the Declaration of Independence",
            'title_ar'      =>" تقديم وثيقة الاستقلال",
            'title_fr'      =>"Présentation de la Déclaration d'Indépendance",
            'title_tr'      =>"Bağımsızlık Bildirgesi'nin Sunumu",
            'title_ru'      =>"Презентация Декларации независимости",
            'title_de'      =>"Präsentation der Unabhängigkeitserklärung",
            'title_es'      =>"Presentación de la Declaración de Independencia",
            'title_cn'      =>"独立宣言的介绍",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/8/88/%D9%88%D8%AB%D9%8A%D9%82%D8%A9_%D8%A7%D9%84%D8%A7%D8%B3%D8%AA%D9%82%D9%84%D8%A7%D9%84_11_%D9%8A%D9%86%D8%A7%D9%8A%D8%B1_1944.jpg',
            'start_date'    =>'2021-01-11',
            'end_date'      => null
        ] );  

        CountryEvent::create( [
            'country_id'    =>'114',
            'country_iso'   =>'MA',
            'title_en'      =>"work Birthday",
            'title_ar'      =>"عيد الشغل",
            'title_fr'      =>"travail anniversaire",
            'title_tr'      =>"işçi bayramı",
            'title_ru'      =>"работа День рождения",
            'title_de'      =>"Arbeit Geburtstag",
            'title_es'      =>"trabajo cumpleaños",
            'title_cn'      =>"工作生日",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/1.Mai_2013_%288697603319%29.jpg/560px-1.Mai_2013_%288697603319%29.jpg',
            'start_date'    =>'2021-05-01',
            'end_date'      => null
        ] ); 

        CountryEvent::create( [
            'country_id'    =>'114',
            'country_iso'   =>'MA',
            'title_en'      =>'Eid al-Fitr',
            'title_ar'      =>'عيد الفطر المبارك',
            'title_fr'      =>"Aïd el-Fitr",
            'title_tr'      =>"Eid al-fitr",
            'title_ru'      =>"Курбан-байрам",
            'title_de'      =>"Eid al-Fitr",
            'title_es'      =>"Eid al-Fitr",
            'title_cn'      =>"开斋节",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Kaaba_mirror_edit_jj.jpg/600px-Kaaba_mirror_edit_jj.jpg',
            'start_date'    =>'2021-05-13',
            'end_date'      => null
        ] ); 
        CountryEvent::create( [
            'country_id'    =>'114',
            'country_iso'   =>'MA',
            'title_en'      =>'Eid al-Adha',
            'title_ar'      =>'عيد الأضحى المبارك',
            'title_fr'      =>"Aïd al-Adha",
            'title_tr'      =>"Kurban Bayramı",
            'title_ru'      =>"Курбан-байрам",
            'title_de'      =>"Eid al-Adha",
            'title_es'      =>"Eid al-Adha",
            'title_cn'      =>"宰牲节",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/d/de/The_Badshahi_in_all_its_glory_during_the_Eid_Prayers.JPG/560px-The_Badshahi_in_all_its_glory_during_the_Eid_Prayers.JPG',
            'start_date'    =>'2021-07-20',
            'end_date'      => null
        ] ); 
        CountryEvent::create( [
            'country_id'    =>'114',
            'country_iso'   =>'MA',
            'title_en'      =>"throne Festival",
            'title_ar'      =>"عيد العرش",
            'title_fr'      =>"Fête du trône",
            'title_tr'      =>"taht Festivali",
            'title_ru'      =>"трон фестиваль",
            'title_de'      =>"Thronfest",
            'title_es'      =>"Festival del trono",
            'title_cn'      =>"宝座节",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/King_Mohammed_VI_%28cropped%29.jpg/457px-King_Mohammed_VI_%28cropped%29.jpg',
            'start_date'    =>'2021-07-30',
            'end_date'      => null
        ] ); 
        CountryEvent::create( [
            'country_id'    =>'114',
            'country_iso'   =>'MA',
            'title_en'      =>"Islamic New Year",
            'title_ar'      =>"رأس السنة الهجرية",
            'title_fr'      =>"Nouvel an islamique",
            'title_tr'      =>"İslami Yılbaşı",
            'title_ru'      =>"Исламский Новый год",
            'title_de'      =>"Islamisches Neujahr",
            'title_es'      =>"Año nuevo islámico",
            'title_cn'      =>"伊斯兰新年",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Madina_old.jpg/440px-Madina_old.jpg',
            'start_date'    =>'2021-08-10',
            'end_date'      => null
        ] ); 
        CountryEvent::create( [
            'country_id'    =>'114',
            'country_iso'   =>'MA',
            'title_en'      =>"Valley of Gold Day",
            'title_ar'      =>"يوم وادي الذهب",
            'title_fr'      =>"Journée de la Vallée de l'Or",
            'title_tr'      =>"Altın Günü Vadisi",
            'title_ru'      =>"День Долины Золота",
            'title_de'      =>"Tag im Tal des Goldes",
            'title_es'      =>"Día del Valle de Oro",
            'title_cn'      =>"黄金谷之日",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Flags_of_the_regions_in_Western_Sahara_ar.png/440px-Flags_of_the_regions_in_Western_Sahara_ar.png',
            'start_date'    =>'2021-08-14',
            'end_date'      => null
        ] ); 
        CountryEvent::create( [
            'country_id'    =>'114',
            'country_iso'   =>'MA',
            'title_en'      =>"The memory of the revolution of the king and the people",
            'title_ar'      =>"ذكرى ثورة الملك والشعب",
            'title_fr'      =>"La mémoire de la révolution du roi et du peuple",
            'title_tr'      =>"Kralın ve halkın devriminin hatırası",
            'title_ru'      =>"Память о революции короля и народа",
            'title_de'      =>"Die Erinnerung an die Revolution des Königs und des Volkes",
            'title_es'      =>"El recuerdo de la revolución del rey y del pueblo",
            'title_cn'      =>"国王和人民革命的记忆",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/2/2c/ProtectoradoMarruecos-ar.png',
            'start_date'    =>'2021-08-20',
            'end_date'      => null
        ] ); 
        CountryEvent::create( [
            'country_id'    =>'114',
            'country_iso'   =>'MA',
            'title_en'      =>"youth day",
            'title_ar'      =>"عيد الشباب",
            'title_fr'      =>"journée de la jeunesse",
            'title_tr'      =>"gençlik günü",
            'title_ru'      =>"день молодежи",
            'title_de'      =>"Jugendtag",
            'title_es'      =>"día de la Juventud",
            'title_cn'      =>"青年节",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/8/8d/%D9%85%D8%AD%D9%85%D8%AF_%D8%A7%D9%84%D8%AE%D8%A7%D9%85%D8%B3_%D8%A8%D9%86_%D9%8A%D9%88%D8%B3%D9%81.jpg',
            'start_date'    =>'2021-08-21',
            'end_date'      => null
        ] ); 
        CountryEvent::create( [
            'country_id'    =>'114',
            'country_iso'   =>'MA',
            'title_en'      =>"Prophet's Birthday",
            'title_ar'      =>'المولد النبوي الشريف',
            'title_fr'      =>"L'anniversaire du prophète",
            'title_tr'      =>"peygamberimizin doğum günü",
            'title_ru'      =>"День Рождения Пророка",
            'title_de'      =>"Geburtstag des Propheten",
            'title_es'      =>"Cumpleaños del Profeta",
            'title_cn'      =>"先知诞辰",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/3/3d/Muhammad2.png',
            'start_date'    =>'2021-10-10',
            'end_date'      => null
        ] ); 
        CountryEvent::create( [
            'country_id'    =>'114',
            'country_iso'   =>'MA',
            'title_en'      =>"Memory of the Green March",
            'title_ar'      =>"ذكرى المسيرة الخضراء",
            'title_fr'      =>"Souvenir de la Marche Verte",
            'title_tr'      =>"Yeşil Yürüyüşün Hatırası",
            'title_ru'      =>"Память о зеленом марше",
            'title_de'      =>"Erinnerung an den Grünen Marsch",
            'title_es'      =>"Memoria de la Marcha Verde",
            'title_cn'      =>"绿色三月的记忆",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Gr%C3%BCner_Marsch_Karte-ar.png/560px-Gr%C3%BCner_Marsch_Karte-ar.png',
            'start_date'    =>'2021-11-06',
            'end_date'      => null
        ] ); 
        CountryEvent::create( [
            'country_id'    =>'114',
            'country_iso'   =>'MA',
            'title_en'      =>"Independence Day",
            'title_ar'      =>"عيد الاستقلال",
            'title_fr'      =>"Fête de l'Indépendance",
            'title_tr'      =>"istiklal Bayramı",
            'title_ru'      =>"День независимости",
            'title_de'      =>"Tag der Unabhängigkeit",
            'title_es'      =>"Día de la Independencia",
            'title_cn'      =>"独立日",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Flag_of_Morocco.svg/560px-Flag_of_Morocco.svg.png',
            'start_date'    =>'2021-11-18',
            'end_date'      => null
        ] );  

        CountryEvent::create( [
            'country_id'    =>'73',
            'country_iso'   =>'FR',
            'title_en'      =>"New Year's Day",
            'title_ar'      =>"عيد رأس السنة الجديدة",
            'title_fr'      =>"Le jour de l'An",
            'title_tr'      =>"Yılbaşı",
            'title_ru'      =>"День нового года",
            'title_de'      =>"Neujahr",
            'title_es'      =>"Día de Año Nuevo",
            'title_cn'      =>"元旦",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b4/Fireworks_on_New_Year%27s_Eve_in_a_small_Swabian_village_%281%29%2C_brightened.jpg/560px-Fireworks_on_New_Year%27s_Eve_in_a_small_Swabian_village_%281%29%2C_brightened.jpg',
            'start_date'    =>'2021-01-01',
            'end_date'      => null
        ] );  
        CountryEvent::create( [
            'country_id'    =>'73',
            'country_iso'   =>'FR',
            'title_en'      =>"Easter Holiday",
            'title_ar'      =>"عيد الفصح",
            'title_fr'      =>"Congé de Pâques",
            'title_tr'      =>"Paskalya Bayramı",
            'title_ru'      =>"Пасха",
            'title_de'      =>"Osterferien",
            'title_es'      =>"Pascua vacaciones",
            'title_cn'      =>"复活节假期",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/5/5b/BytomKosciolDuchaSwietegoWielkiPiatek.jpg/560px-BytomKosciolDuchaSwietegoWielkiPiatek.jpg',
            'start_date'    =>'2021-04-05',
            'end_date'      => null
        ] );  
        CountryEvent::create( [
            'country_id'    =>'73',
            'country_iso'   =>'FR',
            'title_ar'      =>'عيد العمال',
            'title_fr'      =>"Fête du travail",
            'title_tr'      =>"İşçi bayramı",
            'title_ru'      =>"День Труда",
            'title_de'      =>"Tag der Arbeit",
            'title_es'      =>"Día laboral",
            'title_cn'      =>"劳动节",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/1.Mai_2013_%288697603319%29.jpg/560px-1.Mai_2013_%288697603319%29.jpg',
            'start_date'    =>'2021-05-01',
            'end_date'      => null
        ] );  
        CountryEvent::create( [
            'country_id'    =>'73',
            'country_iso'   =>'FR',
            'title_en'      =>"Victory day",
            'title_ar'      =>"عيد النصر",
            'title_fr'      =>"Jour de la victoire",
            'title_tr'      =>"Zafer günü",
            'title_ru'      =>"День Победы",
            'title_de'      =>"Tag des Sieges",
            'title_es'      =>"Dia de Victoria",
            'title_cn'      =>"胜利日",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Intocht_van_het_Canadese_1e_Leger_in_de_provincie_Utrecht%2C_Bestanddeelnr_900-2770.jpg/560px-Intocht_van_het_Canadese_1e_Leger_in_de_provincie_Utrecht%2C_Bestanddeelnr_900-2770.jpg',
            'start_date'    =>'2021-05-08',
            'end_date'      => null
        ] );  
        CountryEvent::create( [
            'country_id'    =>'73',
            'country_iso'   =>'FR',
            'title_en'      =>"Ascension Day",
            'title_ar'      =>"عيد الصعود",
            'title_fr'      =>"Jour de l'Ascension",
            'title_tr'      =>"Yükseliş Günü",
            'title_ru'      =>"Вознесение",
            'title_de'      =>"Christi Himmelfahrt",
            'title_es'      =>"Día de la ascensión",
            'title_cn'      =>"耶稣升天节",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/b/b8/Ascension2007-04.jpg',
            'start_date'    =>'2021-05-13',
            'end_date'      => null
        ] );  
        CountryEvent::create( [
            'country_id'    =>'73',
            'country_iso'   =>'FR',
            'title_en'      =>"White Monday",
            'title_ar'      =>"يوم الاثنين الأبيض",
            'title_fr'      =>"Lundi blanc",
            'title_tr'      =>"beyaz pazartesi",
            'title_ru'      =>"Белый понедельник",
            'title_de'      =>"Weißer Montag",
            'title_es'      =>"Lunes Blanco",
            'title_cn'      =>"白色星期一",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/0/04/Ingeborg_Psalter_02f_1200_%28cropped%29.jpg',
            'start_date'    =>'2021-05-24',
            'end_date'      => null
        ] );  
        CountryEvent::create( [
            'country_id'    =>'73',
            'country_iso'   =>'FR',
            'title_en'      =>"Liberation from slavery",
            'title_ar'      =>"التحرر من العبودية",
            'title_fr'      =>"Libération de l'esclavage",
            'title_tr'      =>"kölelikten kurtuluş",
            'title_ru'      =>"Освобождение от рабства",
            'title_de'      =>"Befreiung aus der Sklaverei",
            'title_es'      =>"Liberación de la esclavitud",
            'title_cn'      =>"从奴隶制中解放",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/IJzeren_voetring_voor_gevangenen_transparent_background.png/300px-IJzeren_voetring_voor_gevangenen_transparent_background.png',
            'start_date'    =>'2021-07-10',
            'end_date'      => null
        ] );  
        CountryEvent::create( [
            'country_id'    =>'73',
            'country_iso'   =>'FR',
            'title_en'      =>"French National Day",
            'title_ar'      =>"اليوم الوطنيّ الفرنسيّ",
            'title_fr'      =>"Fête nationale française",
            'title_tr'      =>"Fransız Ulusal Günü",
            'title_ru'      =>"Национальный праздник Франции",
            'title_de'      =>"Französischer Nationalfeiertag",
            'title_es'      =>"Día Nacional de Francia",
            'title_cn'      =>"法国国庆日",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Feu_d%27artifice_du_14_juillet_2017_depuis_le_champ_de_Mars_%C3%A0_Paris%2C_devant_la_Tour_Eiffel%2C_Bastille_day_2017_%2835118978683%29.jpg/560px-Feu_d%27artifice_du_14_juillet_2017_depuis_le_champ_de_Mars_%C3%A0_Paris%2C_devant_la_Tour_Eiffel%2C_Bastille_day_2017_%2835118978683%29.jpg',
            'start_date'    =>'2021-07-14',
            'end_date'      => null
        ] );  
        CountryEvent::create( [
            'country_id'    =>'73',
            'country_iso'   =>'FR',
            'title_en'      =>"Assumption of the Virgin to Heaven",
            'title_ar'      =>"انتقال العذراء إلى الجنة",
            'title_fr'      =>"Assomption de la Vierge au Ciel",
            'title_tr'      =>"Bakire'nin Cennete Göğe Kabulü",
            'title_ru'      =>"Вознесение Богородицы на небеса",
            'title_de'      =>"Aufnahme der Jungfrau in den Himmel",
            'title_es'      =>"Asunción de la Virgen al Cielo",
            'title_cn'      =>"圣母升天",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Tizian_041.jpg/358px-Tizian_041.jpg',
            'start_date'    =>'2021-08-15',
            'end_date'      => null
        ] );  
        CountryEvent::create( [
            'country_id'    =>'73',
            'country_iso'   =>'FR',
            'title_en'      =>"Feast of All Saints",
            'title_ar'      =>"عيد جميع القديسين",
            'title_fr'      =>"Fête de la Toussaint",
            'title_tr'      =>"Tüm Azizler Bayramı",
            'title_ru'      =>"Праздник Всех Святых",
            'title_de'      =>"Fest Allerheiligen",
            'title_es'      =>"Fiesta de todos los santos",
            'title_cn'      =>"诸圣节",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/All-Saints.jpg/460px-All-Saints.jpg',
            'start_date'    =>'2021-11-01',
            'end_date'      => null
        ] );  
        CountryEvent::create( [
            'country_id'    =>'73',
            'country_iso'   =>'FR',
            'title_en'      =>"Armistice Day",
            'title_ar'      =>"يوم الهدنة",
            'title_fr'      =>"Jour de l'Armistice",
            'title_tr'      =>"Ateşkes günü",
            'title_ru'      =>"День перемирия",
            'title_de'      =>"Tag des Waffenstillstands",
            'title_es'      =>"Día del Armisticio",
            'title_cn'      =>"停战纪念日",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/NYTimes-Page1-11-11-1918.jpg/520px-NYTimes-Page1-11-11-1918.jpg',
            'start_date'    =>'2021-11-11',
            'end_date'      => null
        ] );  
        CountryEvent::create( [
            'country_id'    =>'73',
            'country_iso'   =>'FR',
            'title_en'      =>'Holiday Christmas',
            'title_ar'      =>'عيد الميلاد المجيد',
            'title_fr'      =>"Noël de vacances",
            'title_tr'      =>"tatil noel",
            'title_ru'      =>"Праздник Рождества",
            'title_de'      =>"Feiertag Weihnachten",
            'title_es'      =>"Navidad navideña",
            'title_cn'      =>"假期圣诞节",
            'type'          =>'vacation',
            'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Nativity_of_christ.jpg/500px-Nativity_of_christ.jpg',
            'start_date'    =>'2021-12-25',
            'end_date'      => null
        ] );  
        CountryEvent::create( [
            'country_id'    =>'73',
            'country_iso'   =>'FR',
            'title_en'      =>"The second day of Christmas",
            'title_ar'      =>"اليوم الثاني لعيد الميلاد",
            'title_fr'      =>"Le deuxième jour de Noël",
            'title_tr'      =>"Noel'in ikinci günü",
            'title_ru'      =>"Второй день Рождества",
            'title_de'      =>"Der zweite Weihnachtstag",
            'title_es'      =>"El segundo dia de navidad",
            'title_cn'      =>"圣诞节的第二天",
            'type'          =>'vacation',
             'image'         =>'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Nativity_of_christ.jpg/500px-Nativity_of_christ.jpg',
            'start_date'    =>'2021-12-26',
            'end_date'      => null
        ] );  

    }
}
