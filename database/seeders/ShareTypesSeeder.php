<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ShareTypes;
class ShareTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ShareTypes::create( [
            'type_id'=>'1',
            'title'=>'أضحيات',
            'desc'=>'أضحية عيد الأضحي القادم بإذن الله',
            'color'=>'blue',
            'image'=>'img/default/sacrifice.png',
            'icon'=>'img/avatar42-sm.webp',
            'lang'=>'ar'
        ] );

        ShareTypes::create( [
            'type_id'=>'1',
            'title'=>'sacrifice',
            'desc'=>'The sacrifice of the next Eid al-Adha',
            'color'=>'blue',
            'image'=>'img/default/sacrifice.png',
            'icon'=>'img/avatar42-sm.webp',
            'lang'=>'en'
        ] );

         ShareTypes::create( [
            'type_id'=>'1',
            'title'=>'sacrifice',
            'desc'=>'Le sacrifice du prochain Aïd al-Adha',
            'color'=>'blue',
            'image'=>'img/default/sacrifice.png',
            'icon'=>'img/avatar42-sm.webp',
            'lang'=>'fr'
        ] );

         ShareTypes::create( [
            'type_id'=>'1',
            'title'=>'kurban',
            'desc'=>"Bir sonraki Kurban Bayramı'nın kurbanı",
            'color'=>'blue',
            'image'=>'img/default/sacrifice.png',
            'icon'=>'img/avatar42-sm.webp',
            'lang'=>'tr'
        ] );

         ShareTypes::create( [
            'type_id'=>'1',
            'title'=>'жертва',
            'desc'=>'Жертвоприношение следующего праздника Курбан-байрам',
            'color'=>'blue',
            'image'=>'img/default/sacrifice.png',
            'icon'=>'img/avatar42-sm.webp',
            'lang'=>'ru'
        ] );

         ShareTypes::create( [
            'type_id'=>'1',
            'title'=>'Opfern',
            'desc'=>'Das Opfer des nächsten Eid al-Adha',
            'color'=>'blue',
            'image'=>'img/default/sacrifice.png',
            'icon'=>'img/avatar42-sm.webp',
            'lang'=>'de'
        ] );

         ShareTypes::create( [
            'type_id'=>'1',
            'title'=>'sacrificio',
            'desc'=>'El sacrificio del próximo Eid al-Adha',
            'color'=>'blue',
            'image'=>'img/default/sacrifice.png',
            'icon'=>'img/avatar42-sm.webp',
            'lang'=>'es'
        ] );

         ShareTypes::create( [
            'type_id'=>'1',
            'title'=>'牺牲',
            'desc'=>'下一个开斋节的牺牲',
            'color'=>'blue',
            'image'=>'img/default/sacrifice.png',
            'icon'=>'img/avatar42-sm.webp',
            'lang'=>'cn'
        ] );

        ShareTypes::create( [
            'type_id'=>'2',
            'title'=>'جمعيات',
            'desc'=>'جمعية عائلية بين عائلتنا فقط',
            'color'=>'green',
            'image'=>'img/default/fellow.png',
            'icon'=>'img/avatar45-sm.webp',
            'lang'=>'ar'
        ] );

        ShareTypes::create( [
            'type_id'=>'2',
            'title'=>'fellow',
            'desc'=>'A family association between our family only',
            'color'=>'green',
            'image'=>'img/default/fellow.png',
            'icon'=>'img/avatar45-sm.webp',
            'lang'=>'en'
        ] );

        ShareTypes::create( [
            'type_id'=>'2',
            'title'=>'compagnon',
            'desc'=>'Une association familiale entre notre famille seulement',
            'color'=>'green',
            'image'=>'img/default/fellow.png',
            'icon'=>'img/avatar45-sm.webp',
            'lang'=>'fr'
        ] );

        ShareTypes::create( [
            'type_id'=>'2',
            'title'=>'dost',
            'desc'=>'Sadece ailemiz arasında bir aile birliği',
            'color'=>'green',
            'image'=>'img/default/fellow.png',
            'icon'=>'img/avatar45-sm.webp',
            'lang'=>'tr'
        ] );

        ShareTypes::create( [
            'type_id'=>'2',
            'title'=>'парень',
            'desc'=>'Семейная связь только между нашей семьей',
            'color'=>'green',
            'image'=>'img/default/fellow.png',
            'icon'=>'img/avatar45-sm.webp',
            'lang'=>'ru'
        ] );

        ShareTypes::create( [
            'type_id'=>'2',
            'title'=>'Gefährte',
            'desc'=>'Ein Familienverband nur zwischen unserer Familie',
            'color'=>'green',
            'image'=>'img/default/fellow.png',
            'icon'=>'img/avatar45-sm.webp',
            'lang'=>'de'
        ] );

        ShareTypes::create( [
            'type_id'=>'2',
            'title'=>'compañero',
            'desc'=>'Una asociación familiar entre nuestra familia solamente',
            'color'=>'green',
            'image'=>'img/default/fellow.png',
            'icon'=>'img/avatar45-sm.webp',
            'lang'=>'es'
        ] );

        ShareTypes::create( [
            'type_id'=>'2',
            'title'=>'伙计',
            'desc'=>'仅在我们家庭之间的家庭协会',
            'color'=>'green',
            'image'=>'img/default/fellow.png',
            'icon'=>'img/avatar45-sm.webp',
            'lang'=>'en'
        ] );

        ShareTypes::create( [
            'type_id'=>'3',
            'title'=>'ختمة قرآن',
            'desc'=>'ختمة علي روح فقيد لنا ولكم الأجر والثواب',
            'color'=>'grey',
            'image'=>'img/default/quran.png',
            'icon'=>'img/avatar43-sm.webp',
            'lang'=>'ar'
        ] );

        ShareTypes::create( [
            'type_id'=>'3',
            'title'=>'quran',
            'desc'=>'The conclusion of the Quran on the soul of a deceased for us and for you the reward',
            'color'=>'grey',
            'image'=>'img/default/quran.png',
            'icon'=>'img/avatar43-sm.webp',
            'lang'=>'en'
        ] );

        ShareTypes::create( [
            'type_id'=>'3',
            'title'=>'coran',
            'desc'=>"La conclusion du Coran sur l'âme d'un défunt pour nous et pour vous la récompense",
            'color'=>'grey',
            'image'=>'img/default/quran.png',
            'icon'=>'img/avatar43-sm.webp',
            'lang'=>'fr'
        ] );

        ShareTypes::create( [
            'type_id'=>'3',
            'title'=>'kuran',
            'desc'=>"Kur'an'ın bir merhumun ruhundaki hükmü bizim için ve sizin için mükâfat",
            'color'=>'grey',
            'image'=>'img/default/quran.png',
            'icon'=>'img/avatar43-sm.webp',
            'lang'=>'tr'
        ] );

        ShareTypes::create( [
            'type_id'=>'3',
            'title'=>'Коран',
            'desc'=>'Заключение Корана о душе умершего для нас и для вас награда',
            'color'=>'grey',
            'image'=>'img/default/quran.png',
            'icon'=>'img/avatar43-sm.webp',
            'lang'=>'ru'
        ] );

        ShareTypes::create( [
            'type_id'=>'3',
            'title'=>'Koran',
            'desc'=>'Der Abschluss des Korans über die Seele eines Verstorbenen für uns und für Sie die Belohnung',
            'color'=>'grey',
            'image'=>'img/default/quran.png',
            'icon'=>'img/avatar43-sm.webp',
            'lang'=>'de'
        ] );

        ShareTypes::create( [
            'type_id'=>'3',
            'title'=>'corán',
            'desc'=>'La conclusión del Corán sobre el alma de un difunto para nosotros y para ti la recompensa.',
            'color'=>'grey',
            'image'=>'img/default/quran.png',
            'icon'=>'img/avatar43-sm.webp',
            'lang'=>'es'
        ] );

        ShareTypes::create( [
            'type_id'=>'3',
            'title'=>'古兰经',
            'desc'=>'古兰经关于死者灵魂的结论为我们和你的奖励',
            'color'=>'grey',
            'image'=>'img/default/quran.png',
            'icon'=>'img/avatar43-sm.webp',
            'lang'=>'cn'
        ] );

        ShareTypes::create( [
            'type_id'=>'4',
            'title'=>'إغاثة',
            'desc'=>'مساعدة عائلية ولا ننسي أولي الأرحام',
            'color'=>'purple',
            'image'=>'img/default/helpus.png',
            'icon'=>'img/avatar46-sm.webp',
            'lang'=>'ar'
        ] );

        ShareTypes::create( [
            'type_id'=>'4',
            'title'=>'relief',
            'desc'=>'Family assistance and do not forget the first wombs',
            'color'=>'purple',
            'image'=>'img/default/helpus.png',
            'icon'=>'img/avatar46-sm.webp',
            'lang'=>'en'
        ] );

        ShareTypes::create( [
            'type_id'=>'4',
            'title'=>'soulagement',
            'desc'=>"Aide familiale et n'oubliez pas les premiers utérus",
            'color'=>'purple',
            'image'=>'img/default/helpus.png',
            'icon'=>'img/avatar46-sm.webp',
            'lang'=>'fr'
        ] );

        ShareTypes::create( [
            'type_id'=>'4',
            'title'=>'Rahatlama',
            'desc'=>'Aile yardımı ve ilk rahimleri unutma',
            'color'=>'purple',
            'image'=>'img/default/helpus.png',
            'icon'=>'img/avatar46-sm.webp',
            'lang'=>'tr'
        ] );

        ShareTypes::create( [
            'type_id'=>'4',
            'title'=>'облегчение',
            'desc'=>'Помощь семье и не забывайте первые утробы',
            'color'=>'purple',
            'image'=>'img/default/helpus.png',
            'icon'=>'img/avatar46-sm.webp',
            'lang'=>'ru'
        ] );

        ShareTypes::create( [
            'type_id'=>'4',
            'title'=>'Relief',
            'desc'=>'Familienhilfe und die ersten Gebärmutter nicht vergessen',
            'color'=>'purple',
            'image'=>'img/default/helpus.png',
            'icon'=>'img/avatar46-sm.webp',
            'lang'=>'de'
        ] );

        ShareTypes::create( [
            'type_id'=>'4',
            'title'=>'alivio',
            'desc'=>'Asistencia familiar y no te olvides de los primeros vientres',
            'color'=>'purple',
            'image'=>'img/default/helpus.png',
            'icon'=>'img/avatar46-sm.webp',
            'lang'=>'es'
        ] );

        ShareTypes::create( [
            'type_id'=>'4',
            'title'=>'宽慰',
            'desc'=>'家庭援助，不要忘记第一个子宫',
            'color'=>'purple',
            'image'=>'img/default/helpus.png',
            'icon'=>'img/avatar46-sm.webp',
            'lang'=>'cn'
        ] );

        ShareTypes::create( [
            'type_id'=>'5',
            'title'=>'صندوق العائلة',
            'desc'=>'صندوق مشاريع وتطوير وطوارئ العائلة',
            'color'=>'#571745',
            'image'=>'img/default/box.png',
            'icon'=>'img/avatar47-sm.webp',
            'lang'=>'ar'
        ] );

        ShareTypes::create( [
            'type_id'=>'5',
            'title'=>'family fund',
            'desc'=>'Family Projects, Development and Emergency Fund',
            'color'=>'#571745',
            'image'=>'img/default/box.png',
            'icon'=>'img/avatar47-sm.webp',
            'lang'=>'en'
        ] );

        ShareTypes::create( [
            'type_id'=>'5',
            'title'=>'fonds familial',
            'desc'=>"Projets Familiaux, Fonds de Développement et d'Urgence",
            'color'=>'#571745',
            'image'=>'img/default/box.png',
            'icon'=>'img/avatar47-sm.webp',
            'lang'=>'fr'
        ] );

        ShareTypes::create( [
            'type_id'=>'5',
            'title'=>'aile fonu',
            'desc'=>'Aile Projeleri, Kalkınma ve Acil Durum Fonu',
            'color'=>'#571745',
            'image'=>'img/default/box.png',
            'icon'=>'img/avatar47-sm.webp',
            'lang'=>'tr'
        ] );

        ShareTypes::create( [
            'type_id'=>'5',
            'title'=>'семейный фонд',
            'desc'=>'Фонд семейных проектов, развития и чрезвычайных ситуаций',
            'color'=>'#571745',
            'image'=>'img/default/box.png',
            'icon'=>'img/avatar47-sm.webp',
            'lang'=>'ru'
        ] );

        ShareTypes::create( [
            'type_id'=>'5',
            'title'=>'Familienfonds',
            'desc'=>'Familienprojekte, Entwicklungs- und Notfallfonds',
            'color'=>'#571745',
            'image'=>'img/default/box.png',
            'icon'=>'img/avatar47-sm.webp',
            'lang'=>'de'
        ] );

        ShareTypes::create( [
            'type_id'=>'5',
            'title'=>'fondo familiar',
            'desc'=>'Fondo de Proyectos, Desarrollo y Emergencias Familiares',
            'color'=>'#571745',
            'image'=>'img/default/box.png',
            'icon'=>'img/avatar47-sm.webp',
            'lang'=>'es'
        ] );

        ShareTypes::create( [
            'type_id'=>'5',
            'title'=>'家庭基金',
            'desc'=>'家庭项目、发展和应急基金',
            'color'=>'#571745',
            'image'=>'img/default/box.png',
            'icon'=>'img/avatar47-sm.webp',
            'lang'=>'cn'
        ] );

        ShareTypes::create( [
            'type_id'=>'6',
            'title'=>'صدقة جارية',
            'desc'=>'صدقات جارية الأجر والثواب',
            'color'=>'#00450C',
            'image'=>'img/default/charity.png',
            'icon'=>'img/avatar47-sm.webp',
            'lang'=>'ar'
        ] );

        ShareTypes::create( [
            'type_id'=>'6',
            'title'=>'charity',
            'desc'=>'Ongoing charitable giving',
            'color'=>'#00450C',
            'image'=>'img/default/charity.png',
            'icon'=>'img/avatar47-sm.webp',
            'lang'=>'en'
        ] );

        ShareTypes::create( [
            'type_id'=>'6',
            'title'=>'charité',
            'desc'=>'Dons de charité continus',
            'color'=>'#00450C',
            'image'=>'img/default/charity.png',
            'icon'=>'img/avatar47-sm.webp',
            'lang'=>'fr'
        ] );

        ShareTypes::create( [
            'type_id'=>'6',
            'title'=>'hayır kurumu',
            'desc'=>'Devam eden hayırseverlik',
            'color'=>'#00450C',
            'image'=>'img/default/charity.png',
            'icon'=>'img/avatar47-sm.webp',
            'lang'=>'tr'
        ] );

        ShareTypes::create( [
            'type_id'=>'6',
            'title'=>'благотворительная деятельность',
            'desc'=>'Постоянные благотворительные пожертвования',
            'color'=>'#00450C',
            'image'=>'img/default/charity.png',
            'icon'=>'img/avatar47-sm.webp',
            'lang'=>'ru'
        ] );

        ShareTypes::create( [
            'type_id'=>'6',
            'title'=>'Wohltätigkeit',
            'desc'=>'Laufende Spenden für wohltätige Zwecke',
            'color'=>'#00450C',
            'image'=>'img/default/charity.png',
            'icon'=>'img/avatar47-sm.webp',
            'lang'=>'de'
        ] );

        ShareTypes::create( [
            'type_id'=>'6',
            'title'=>'caridad',
            'desc'=>'Donaciones caritativas continuas',
            'color'=>'#00450C',
            'image'=>'img/default/charity.png',
            'icon'=>'img/avatar47-sm.webp',
            'lang'=>'es'
        ] );

        ShareTypes::create( [
            'type_id'=>'6',
            'title'=>'慈善机构',
            'desc'=>'持续的慈善捐赠',
            'color'=>'#00450C',
            'image'=>'img/default/charity.png',
            'icon'=>'img/avatar47-sm.webp',
            'lang'=>'cn'
        ] );

        ShareTypes::create( [
            'type_id'=>'7',
            'title'=>'كفالة يتيم',
            'desc'=>'أنا وكافل اليتيم كهاتين في الجنة',
            'color'=>'#1d90f1',
            'image'=>'img/default/orphan.png',
            'icon'=>'img/avatar47-sm.webp',
            'lang'=>'ar'
        ] );

        ShareTypes::create( [
            'type_id'=>'7',
            'title'=>'orphan',
            'desc'=>'I and the one who looks after an orphan are like these two in heaven',
            'color'=>'#1d90f1',
            'image'=>'img/default/orphan.png',
            'icon'=>'img/avatar47-sm.webp',
            'lang'=>'en'
        ] );

        ShareTypes::create( [
            'type_id'=>'7',
            'title'=>'orphelin',
            'desc'=>"Moi et celui qui s'occupe d'un orphelin sommes comme ces deux-là au paradis",
            'color'=>'#1d90f1',
            'image'=>'img/default/orphan.png',
            'icon'=>'img/avatar47-sm.webp',
            'lang'=>'fr'
        ] );

        ShareTypes::create( [
            'type_id'=>'7',
            'title'=>'yetim',
            'desc'=>'Ben ve yetime bakan cennetteki bu ikisi gibiyiz.',
            'color'=>'#1d90f1',
            'image'=>'img/default/orphan.png',
            'icon'=>'img/avatar47-sm.webp',
            'lang'=>'tr'
        ] );

        ShareTypes::create( [
            'type_id'=>'7',
            'title'=>'сирота',
            'desc'=>'Я и тот, кто присматривает за сиротой, как эти двое на небесах',
            'color'=>'#1d90f1',
            'image'=>'img/default/orphan.png',
            'icon'=>'img/avatar47-sm.webp',
            'lang'=>'ru'
        ] );

        ShareTypes::create( [
            'type_id'=>'7',
            'title'=>'verwaist',
            'desc'=>'Ich und der, der ein Waisenkind betreut, sind wie diese beiden im Himmel',
            'color'=>'#1d90f1',
            'image'=>'img/default/orphan.png',
            'icon'=>'img/avatar47-sm.webp',
            'lang'=>'de'
        ] );

        ShareTypes::create( [
            'type_id'=>'7',
            'title'=>'huérfano',
            'desc'=>'Yo y el que cuida a un huérfano somos como estos dos en el cielo',
            'color'=>'#1d90f1',
            'image'=>'img/default/orphan.png',
            'icon'=>'img/avatar47-sm.webp',
            'lang'=>'es'
        ] );

        ShareTypes::create( [
            'type_id'=>'7',
            'title'=>'孤儿',
            'desc'=>'我和照顾孤儿的人就像天堂里的这两个人',
            'color'=>'#1d90f1',
            'image'=>'img/default/orphan.png',
            'icon'=>'img/avatar47-sm.webp',
            'lang'=>'cn'
        ] );
    }
}
