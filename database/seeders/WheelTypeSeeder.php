<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\WheelType;

class WheelTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        WheelType::create([
            'type_id'   => '1',
            'title'     => 'spiritual',
            'lang'      => 'en',
            'color'     => '#FF8E3A',
        ]);

        WheelType::create([
            'type_id'   => '1',
            'title'     => 'spirituel',
            'lang'      => 'fr',
            'color'     => '#FF8E3A',
        ]);

        WheelType::create([
            'type_id'   => '1',
            'title'     => 'manevi',
            'lang'      => 'tr',
            'color'     => '#FF8E3A',
        ]);

        WheelType::create([
            'type_id'   => '1',
            'title'     => 'духовный',
            'lang'      => 'ru',
            'color'     => '#FF8E3A',
        ]);

        WheelType::create([
            'type_id'   => '1',
            'title'     => 'spirituell',
            'lang'      => 'de',
            'color'     => '#FF8E3A',
        ]);

        WheelType::create([
            'type_id'   => '1',
            'title'     => 'espiritual',
            'lang'      => 'es',
            'color'     => '#FF8E3A',
        ]);

        WheelType::create([
            'type_id'   => '1',
            'title'     => '精神',
            'lang'      => 'cn',
            'color'     => '#FF8E3A',
        ]);

        WheelType::create([
            'type_id'   => '1',
            'title'     => 'الروحي',
            'lang'      => 'ar',
            'color'     => '#FF8E3A',
        ]);

        WheelType::create([
            'type_id'   => '2',
            'title'     => 'Mental',
            'lang'      => 'en',
            'color'     => '#FFCA3D',
        ]);

        WheelType::create([
            'type_id'   => '2',
            'title'     => 'Mental',
            'lang'      => 'fr',
            'color'     => '#FFCA3D',
        ]);

        WheelType::create([
            'type_id'   => '2',
            'title'     => 'zihinsel',
            'lang'      => 'tr',
            'color'     => '#FFCA3D',
        ]);

        WheelType::create([
            'type_id'   => '2',
            'title'     => 'Ментальный',
            'lang'      => 'ru',
            'color'     => '#FFCA3D',
        ]);

        WheelType::create([
            'type_id'   => '2',
            'title'     => 'Mental',
            'lang'      => 'de',
            'color'     => '#FFCA3D',
        ]);

        WheelType::create([
            'type_id'   => '2',
            'title'     => 'Mental',
            'lang'      => 'es',
            'color'     => '#FFCA3D',
        ]);

        WheelType::create([
            'type_id'   => '2',
            'title'     => '精神的',
            'lang'      => 'cn',
            'color'     => '#FFCA3D',
        ]);

        WheelType::create([
            'type_id'   => '2',
            'title'     => 'العقلي',
            'lang'      => 'ar',
            'color'     => '#FFCA3D',
        ]);

        WheelType::create([
            'type_id'   => '3',
            'title'     => 'recreation',
            'lang'      => 'en',
            'color'     => '#EE4845',
        ]);

        WheelType::create([
            'type_id'   => '3',
            'title'     => 'des loisirs',
            'lang'      => 'fr',
            'color'     => '#EE4845',
        ]);

        WheelType::create([
            'type_id'   => '3',
            'title'     => 'rekreasyon',
            'lang'      => 'tr',
            'color'     => '#EE4845',
        ]);

        WheelType::create([
            'type_id'   => '3',
            'title'     => 'отдых',
            'lang'      => 'ru',
            'color'     => '#EE4845',
        ]);

        WheelType::create([
            'type_id'   => '3',
            'title'     => 'Erholung',
            'lang'      => 'de',
            'color'     => '#EE4845',
        ]);

        WheelType::create([
            'type_id'   => '3',
            'title'     => 'recreación',
            'lang'      => 'es',
            'color'     => '#EE4845',
        ]);

        WheelType::create([
            'type_id'   => '3',
            'title'     => '娱乐',
            'lang'      => 'cn',
            'color'     => '#EE4845',
        ]);

        WheelType::create([
            'type_id'   => '3',
            'title'     => 'النفسي',
            'lang'      => 'ar',
            'color'     => '#EE4845',
        ]);

        WheelType::create([
            'type_id'   => '4',
            'title'     => 'health',
            'lang'      => 'en',
            'color'     => '#065587',
        ]);

        WheelType::create([
            'type_id'   => '4',
            'title'     => 'santé',
            'lang'      => 'fr',
            'color'     => '#065587',
        ]);

        WheelType::create([
            'type_id'   => '4',
            'title'     => 'sağlık',
            'lang'      => 'tr',
            'color'     => '#065587',
        ]);

        WheelType::create([
            'type_id'   => '4',
            'title'     => 'здоровье',
            'lang'      => 'ru',
            'color'     => '#065587',
        ]);

        WheelType::create([
            'type_id'   => '4',
            'title'     => 'Gesundheit',
            'lang'      => 'de',
            'color'     => '#065587',
        ]);

        WheelType::create([
            'type_id'   => '4',
            'title'     => 'salud',
            'lang'      => 'es',
            'color'     => '#065587',
        ]);

        WheelType::create([
            'type_id'   => '4',
            'title'     => '健康',
            'lang'      => 'cn',
            'color'     => '#065587',
        ]);

        WheelType::create([
            'type_id'   => '4',
            'title'     => 'الجسدي',
            'lang'      => 'ar',
            'color'     => '#065587',
        ]);

        WheelType::create([
            'type_id'   => '5',
            'title'     => 'family',
            'lang'      => 'en',
            'color'     => '#A4CB4B',
        ]);

        WheelType::create([
            'type_id'   => '5',
            'title'     => 'famille',
            'lang'      => 'fr',
            'color'     => '#A4CB4B',
        ]);

        WheelType::create([
            'type_id'   => '5',
            'title'     => 'aile',
            'lang'      => 'tr',
            'color'     => '#A4CB4B',
        ]);

        WheelType::create([
            'type_id'   => '5',
            'title'     => 'семья',
            'lang'      => 'ru',
            'color'     => '#A4CB4B',
        ]);

        WheelType::create([
            'type_id'   => '5',
            'title'     => 'Familie',
            'lang'      => 'de',
            'color'     => '#A4CB4B',
        ]);

        WheelType::create([
            'type_id'   => '5',
            'title'     => 'familia',
            'lang'      => 'es',
            'color'     => '#A4CB4B',
        ]);

        WheelType::create([
            'type_id'   => '5',
            'title'     => '家庭',
            'lang'      => 'cn',
            'color'     => '#A4CB4B',
        ]);

        WheelType::create([
            'type_id'   => '5',
            'title'     => 'الأسري',
            'lang'      => 'ar',
            'color'     => '#A4CB4B',
        ]);

        WheelType::create([
            'type_id'   => '6',
            'title'     => 'social',
            'lang'      => 'en',
            'color'     => '#8F2649',
        ]);

        WheelType::create([
            'type_id'   => '6',
            'title'     => 'social',
            'lang'      => 'fr',
            'color'     => '#8F2649',
        ]);

        WheelType::create([
            'type_id'   => '6',
            'title'     => 'sosyal',
            'lang'      => 'tr',
            'color'     => '#8F2649',
        ]);

        WheelType::create([
            'type_id'   => '6',
            'title'     => 'Социальное',
            'lang'      => 'ru',
            'color'     => '#8F2649',
        ]);

        WheelType::create([
            'type_id'   => '6',
            'title'     => 'Sozial',
            'lang'      => 'de',
            'color'     => '#8F2649',
        ]);

        WheelType::create([
            'type_id'   => '6',
            'title'     => 'social',
            'lang'      => 'es',
            'color'     => '#8F2649',
        ]);

        WheelType::create([
            'type_id'   => '6',
            'title'     => '社会的',
            'lang'      => 'cn',
            'color'     => '#8F2649',
        ]);

        WheelType::create([
            'type_id'   => '6',
            'title'     => 'الإجتماعي',
            'lang'      => 'ar',
            'color'     => '#8F2649',
        ]);

        WheelType::create([
            'type_id'   => '7',
            'title'     => 'career',
            'lang'      => 'en',
            'color'     => '#68BDA0',
        ]);

        WheelType::create([
            'type_id'   => '7',
            'title'     => 'carrière',
            'lang'      => 'fr',
            'color'     => '#68BDA0',
        ]);

        WheelType::create([
            'type_id'   => '7',
            'title'     => 'kariyer',
            'lang'      => 'tr',
            'color'     => '#68BDA0',
        ]);

        WheelType::create([
            'type_id'   => '7',
            'title'     => 'карьера',
            'lang'      => 'ru',
            'color'     => '#68BDA0',
        ]);

        WheelType::create([
            'type_id'   => '7',
            'title'     => 'Werdegang',
            'lang'      => 'de',
            'color'     => '#68BDA0',
        ]);

        WheelType::create([
            'type_id'   => '7',
            'title'     => 'carrera',
            'lang'      => 'es',
            'color'     => '#68BDA0',
        ]);

        WheelType::create([
            'type_id'   => '7',
            'title'     => '职业',
            'lang'      => 'cn',
            'color'     => '#68BDA0',
        ]);

        WheelType::create([
            'type_id'   => '7',
            'title'     => 'المهني',
            'lang'      => 'ar',
            'color'     => '#68BDA0',
        ]);

        WheelType::create([
            'type_id'   => '8',
            'title'     => 'financial',
            'lang'      => 'en',
            'color'     => '#4FB7C4',
        ]);

        WheelType::create([
            'type_id'   => '8',
            'title'     => 'financier',
            'lang'      => 'fr',
            'color'     => '#4FB7C4',
        ]);

        WheelType::create([
            'type_id'   => '8',
            'title'     => 'parasal',
            'lang'      => 'tr',
            'color'     => '#4FB7C4',
        ]);

        WheelType::create([
            'type_id'   => '8',
            'title'     => 'финансовый',
            'lang'      => 'ru',
            'color'     => '#4FB7C4',
        ]);

        WheelType::create([
            'type_id'   => '8',
            'title'     => 'finanziell',
            'lang'      => 'de',
            'color'     => '#4FB7C4',
        ]);

        WheelType::create([
            'type_id'   => '8',
            'title'     => 'financiero',
            'lang'      => 'es',
            'color'     => '#4FB7C4',
        ]);

        WheelType::create([
            'type_id'   => '8',
            'title'     => '金融',
            'lang'      => 'cn',
            'color'     => '#4FB7C4',
        ]);

        WheelType::create([
            'type_id'   => '8',
            'title'     => 'المالي',
            'lang'      => 'ar',
            'color'     => '#4FB7C4',
        ]);
    }
}
