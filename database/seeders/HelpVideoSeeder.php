<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\HelpVedio;
class HelpVideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        HelpVedio::create( [
            'cat'=>'1',
            'cat_title'=>'السيرة الذاتية للأعضاء',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'ar'
        ] );

        HelpVedio::create( [
            'cat'=>'1',
            'cat_title'=>'member CVs',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'en'
        ] );

        HelpVedio::create( [
            'cat'=>'1',
            'cat_title'=>'CV des membres',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'fr'
        ] );

        HelpVedio::create( [
            'cat'=>'1',
            'cat_title'=>'üye özgeçmişleri',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'tr'
        ] );

        HelpVedio::create( [
            'cat'=>'1',
            'cat_title'=>'резюме участников',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'ru'
        ] );

        HelpVedio::create( [
            'cat'=>'1',
            'cat_title'=>'Lebensläufe der Mitglieder',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'de'
        ] );

        HelpVedio::create( [
            'cat'=>'1',
            'cat_title'=>'CV de miembros',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'es'
        ] );

        HelpVedio::create( [
            'cat'=>'1',
            'cat_title'=>'成员简历',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'cn'
        ] );

         HelpVedio::create( [
            'cat'=>'1',
            'cat_title'=>'',
            'sub'=>'1',
            'title'=>'تصفح السير الذاتية',
            'video'=>'videos/profile2.mp4',
            'image'=>'videos/video_info.webp',
            'lenght'=>'٠:٢٥',
            'lang'=>'ar'
        ] );

        HelpVedio::create( [
            'cat'=>'1',
            'cat_title'=>'',
            'sub'=>'1',
            'title'=>'browse the member CVs',
            'video'=>'videos/profile2.mp4',
            'image'=>'videos/video_info.webp',
            'lenght'=>'0:25',
            'lang'=>'en'
        ] );

        HelpVedio::create( [
            'cat'=>'1',
            'cat_title'=>'',
            'sub'=>'1',
            'title'=>'parcourir les CV des membres',
            'video'=>'videos/profile2.mp4',
            'image'=>'videos/video_info.webp',
            'lenght'=>'0:25',
            'lang'=>'fr'
        ] );

        HelpVedio::create( [
            'cat'=>'1',
            'cat_title'=>'',
            'sub'=>'1',
            'title'=>'üye özgeçmişlerine göz atın',
            'video'=>'videos/profile2.mp4',
            'image'=>'videos/video_info.webp',
            'lenght'=>'0:25',
            'lang'=>'tr'
        ] );

        HelpVedio::create( [
            'cat'=>'1',
            'cat_title'=>'',
            'sub'=>'1',
            'title'=>'просмотреть резюме участников',
            'video'=>'videos/profile2.mp4',
            'image'=>'videos/video_info.webp',
            'lenght'=>'0:25',
            'lang'=>'ru'
        ] );

        HelpVedio::create( [
            'cat'=>'1',
            'cat_title'=>'',
            'sub'=>'1',
            'title'=>'Durchsuchen Sie die Lebensläufe der Mitglieder',
            'video'=>'videos/profile2.mp4',
            'image'=>'videos/video_info.webp',
            'lenght'=>'0:25',
            'lang'=>'de'
        ] );

        HelpVedio::create( [
            'cat'=>'1',
            'cat_title'=>'',
            'sub'=>'1',
            'title'=>'navegar por los CV de los miembros',
            'video'=>'videos/profile2.mp4',
            'image'=>'videos/video_info.webp',
            'lenght'=>'0:25',
            'lang'=>'es'
        ] );

        HelpVedio::create( [
            'cat'=>'1',
            'cat_title'=>'',
            'sub'=>'1',
            'title'=>'浏览成员简历',
            'video'=>'videos/profile2.mp4',
            'image'=>'videos/video_info.webp',
            'lenght'=>'零:二十五',
            'lang'=>'cn'
        ] );

        HelpVedio::create( [
            'cat'=>'1',
            'cat_title'=>'',
            'sub'=>'1',
            'title'=>'إضافة بياناتي',
            'video'=>'videos/profile.mp4',
            'image'=>'videos/video_info.webp',
            'lenght'=>'٠:٥٩',
            'lang'=>'ar'
        ] );

        HelpVedio::create( [
            'cat'=>'1',
            'cat_title'=>'',
            'sub'=>'1',
            'title'=>'add my cv',
            'video'=>'videos/profile.mp4',
            'image'=>'videos/video_info.webp',
            'lenght'=>'0:59',
            'lang'=>'en'
        ] );

        HelpVedio::create( [
            'cat'=>'1',
            'cat_title'=>'',
            'sub'=>'1',
            'title'=>'ajouter mon cv',
            'video'=>'videos/profile.mp4',
            'image'=>'videos/video_info.webp',
            'lenght'=>'0:59',
            'lang'=>'fr'
        ] );

        HelpVedio::create( [
            'cat'=>'1',
            'cat_title'=>'',
            'sub'=>'1',
            'title'=>"cv'mi ekle",
            'video'=>'videos/profile.mp4',
            'image'=>'videos/video_info.webp',
            'lenght'=>'0:59',
            'lang'=>'tr'
        ] );

        HelpVedio::create( [
            'cat'=>'1',
            'cat_title'=>'',
            'sub'=>'1',
            'title'=>'добавить мое резюме',
            'video'=>'videos/profile.mp4',
            'image'=>'videos/video_info.webp',
            'lenght'=>'0:59',
            'lang'=>'ru'
        ] );

        HelpVedio::create( [
            'cat'=>'1',
            'cat_title'=>'',
            'sub'=>'1',
            'title'=>'füge meinen Lebenslauf hinzu',
            'video'=>'videos/profile.mp4',
            'image'=>'videos/video_info.webp',
            'lenght'=>'0:59',
            'lang'=>'de'
        ] );

        HelpVedio::create( [
            'cat'=>'1',
            'cat_title'=>'',
            'sub'=>'1',
            'title'=>'agregar mi cv',
            'video'=>'videos/profile.mp4',
            'image'=>'videos/video_info.webp',
            'lenght'=>'0:59',
            'lang'=>'es'
        ] );

        HelpVedio::create( [
            'cat'=>'1',
            'cat_title'=>'',
            'sub'=>'1',
            'title'=>'添加我的简历',
            'video'=>'videos/profile.mp4',
            'image'=>'videos/video_info.webp',
            'lenght'=>'零:五十九',
            'lang'=>'cn'
        ] );

         HelpVedio::create( [
            'cat'=>'2',
            'cat_title'=>'تعديل البيانات الشخصية',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'ar'
        ] );

        HelpVedio::create( [
            'cat'=>'2',
            'cat_title'=>'Edit the personal data',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'en'
        ] );

        HelpVedio::create( [
            'cat'=>'2',
            'cat_title'=>'Modifier les données personnelles',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'fr'
        ] );

        HelpVedio::create( [
            'cat'=>'2',
            'cat_title'=>'Kişisel verileri düzenleyin',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'tr'
        ] );

        HelpVedio::create( [
            'cat'=>'2',
            'cat_title'=>'Редактировать личные данные',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'ru'
        ] );

        HelpVedio::create( [
            'cat'=>'2',
            'cat_title'=>'Bearbeiten Sie die persönlichen Daten',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'de'
        ] );

        HelpVedio::create( [
            'cat'=>'2',
            'cat_title'=>'Editar los datos personales',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'es'
        ] );

        HelpVedio::create( [
            'cat'=>'2',
            'cat_title'=>'编辑个人资料',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'cn'
        ] );

        HelpVedio::create( [
            'cat'=>'2',
            'cat_title'=>'',
            'sub'=>'2',
            'title'=>'التعديل من صفحة البروفايل',
            'video'=>'videos/profile3.mp4',
            'image'=>'videos/video_personal_data.webp',
            'lenght'=>'٠:٤٠',
            'lang'=>'ar'
        ] );

        HelpVedio::create( [
            'cat'=>'2',
            'cat_title'=>'',
            'sub'=>'2',
            'title'=>'edit from profile page',
            'video'=>'videos/profile3.mp4',
            'image'=>'videos/video_personal_data.webp',
            'lenght'=>'0:40',
            'lang'=>'en'
        ] );

        HelpVedio::create( [
            'cat'=>'2',
            'cat_title'=>'',
            'sub'=>'2',
            'title'=>'modifier à partir de la page de profil',
            'video'=>'videos/profile3.mp4',
            'image'=>'videos/video_personal_data.webp',
            'lenght'=>'0:40',
            'lang'=>'fr'
        ] );

        HelpVedio::create( [
            'cat'=>'2',
            'cat_title'=>'',
            'sub'=>'2',
            'title'=>'profil sayfasından düzenle',
            'video'=>'videos/profile3.mp4',
            'image'=>'videos/video_personal_data.webp',
            'lenght'=>'0:40',
            'lang'=>'tr'
        ] );

        HelpVedio::create( [
            'cat'=>'2',
            'cat_title'=>'',
            'sub'=>'2',
            'title'=>'редактировать со страницы профиля',
            'video'=>'videos/profile3.mp4',
            'image'=>'videos/video_personal_data.webp',
            'lenght'=>'0:40',
            'lang'=>'ru'
        ] );

        HelpVedio::create( [
            'cat'=>'2',
            'cat_title'=>'',
            'sub'=>'2',
            'title'=>'von der Profilseite bearbeiten',
            'video'=>'videos/profile3.mp4',
            'image'=>'videos/video_personal_data.webp',
            'lenght'=>'0:40',
            'lang'=>'de'
        ] );

        HelpVedio::create( [
            'cat'=>'2',
            'cat_title'=>'',
            'sub'=>'2',
            'title'=>'editar desde la página de perfil',
            'video'=>'videos/profile3.mp4',
            'image'=>'videos/video_personal_data.webp',
            'lenght'=>'0:40',
            'lang'=>'es'
        ] );

        HelpVedio::create( [
            'cat'=>'2',
            'cat_title'=>'',
            'sub'=>'2',
            'title'=>'从个人资料页面编辑',
            'video'=>'videos/profile3.mp4',
            'image'=>'videos/video_personal_data.webp',
            'lenght'=>'零:四十',
            'lang'=>'cn'
        ] );

        HelpVedio::create( [
            'cat'=>'2',
            'cat_title'=>'',
            'sub'=>'2',
            'title'=>'التعديل من صفحة الشجرة',
            'video'=>'videos/profile4.mp4',
            'image'=>'videos/video_personal_data.webp',
            'lenght'=>'٠:٢٧',
            'lang'=>'ar'
        ] );

        HelpVedio::create( [
            'cat'=>'2',
            'cat_title'=>'',
            'sub'=>'2',
            'title'=>'edit from Tree page',
            'video'=>'videos/profile4.mp4',
            'image'=>'videos/video_personal_data.webp',
            'lenght'=>'0:27',
            'lang'=>'en'
        ] );

        HelpVedio::create( [
            'cat'=>'2',
            'cat_title'=>'',
            'sub'=>'2',
            'title'=>"modifier à partir de la page de l'arborescence",
            'video'=>'videos/profile4.mp4',
            'image'=>'videos/video_personal_data.webp',
            'lenght'=>'0:27',
            'lang'=>'fr'
        ] );

        HelpVedio::create( [
            'cat'=>'2',
            'cat_title'=>'',
            'sub'=>'2',
            'title'=>'Ağaç sayfasından düzenle',
            'video'=>'videos/profile4.mp4',
            'image'=>'videos/video_personal_data.webp',
            'lenght'=>'0:27',
            'lang'=>'tr'
        ] );

        HelpVedio::create( [
            'cat'=>'2',
            'cat_title'=>'',
            'sub'=>'2',
            'title'=>'eредактировать со страницы дерева',
            'video'=>'videos/profile4.mp4',
            'image'=>'videos/video_personal_data.webp',
            'lenght'=>'0:27',
            'lang'=>'ru'
        ] );

        HelpVedio::create( [
            'cat'=>'2',
            'cat_title'=>'',
            'sub'=>'2',
            'title'=>'Bearbeiten von der Baumseite',
            'video'=>'videos/profile4.mp4',
            'image'=>'videos/video_personal_data.webp',
            'lenght'=>'0:27',
            'lang'=>'de'
        ] );

        HelpVedio::create( [
            'cat'=>'2',
            'cat_title'=>'',
            'sub'=>'2',
            'title'=>'editar desde la página del árbol',
            'video'=>'videos/profile4.mp4',
            'image'=>'videos/video_personal_data.webp',
            'lenght'=>'0:27',
            'lang'=>'es'
        ] );

        HelpVedio::create( [
            'cat'=>'2',
            'cat_title'=>'',
            'sub'=>'2',
            'title'=>'从树页面编辑',
            'video'=>'videos/profile4.mp4',
            'image'=>'videos/video_personal_data.webp',
            'lenght'=>'零:二十七',
            'lang'=>'cn'
        ] );

         HelpVedio::create( [
            'cat'=>'4',
            'cat_title'=>'المساهمات العائلية',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'ar'
        ] );

        HelpVedio::create( [
            'cat'=>'4',
            'cat_title'=>'family contributions',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'en'
        ] );

        HelpVedio::create( [
            'cat'=>'4',
            'cat_title'=>'cotisations familiales',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'fr'
        ] );

        HelpVedio::create( [
            'cat'=>'4',
            'cat_title'=>'aile katkıları',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'tr'
        ] );

        HelpVedio::create( [
            'cat'=>'4',
            'cat_title'=>'семейные взносы',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'ru'
        ] );

        HelpVedio::create( [
            'cat'=>'4',
            'cat_title'=>'Familienbeiträge',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'de'
        ] );

        HelpVedio::create( [
            'cat'=>'4',
            'cat_title'=>'contribuciones familiares',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'es'
        ] );

        HelpVedio::create( [
            'cat'=>'4',
            'cat_title'=>'家庭贡献',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'cn'
        ] );

        HelpVedio::create( [
            'cat'=>'4',
            'cat_title'=>'',
            'sub'=>'4',
            'title'=>'إضافة ومتابعة مساهمة جديدة',
            'video'=>'videos/share1.mp4',
            'image'=>'videos/video_share.webp',
            'lenght'=>'١:١٠',
            'lang'=>'ar'
        ] );

        HelpVedio::create( [
            'cat'=>'4',
            'cat_title'=>'',
            'sub'=>'4',
            'title'=>'add and manage new contribution',
            'video'=>'videos/share1.mp4',
            'image'=>'videos/video_share.webp',
            'lenght'=>'1:10',
            'lang'=>'en'
        ] );

        HelpVedio::create( [
            'cat'=>'4',
            'cat_title'=>'',
            'sub'=>'4',
            'title'=>'ajouter et gérer une nouvelle contribution',
            'video'=>'videos/share1.mp4',
            'image'=>'videos/video_share.webp',
            'lenght'=>'1:10',
            'lang'=>'fr'
        ] );

        HelpVedio::create( [
            'cat'=>'4',
            'cat_title'=>'',
            'sub'=>'4',
            'title'=>'yeni katkı ekle ve yönet',
            'video'=>'videos/share1.mp4',
            'image'=>'videos/video_share.webp',
            'lenght'=>'1:10',
            'lang'=>'tr'
        ] );

        HelpVedio::create( [
            'cat'=>'4',
            'cat_title'=>'',
            'sub'=>'4',
            'title'=>'добавлять и управлять новым вкладом',
            'video'=>'videos/share1.mp4',
            'image'=>'videos/video_share.webp',
            'lenght'=>'1:10',
            'lang'=>'ru'
        ] );

        HelpVedio::create( [
            'cat'=>'4',
            'cat_title'=>'',
            'sub'=>'4',
            'title'=>'neuen Beitrag hinzufügen und verwalten',
            'video'=>'videos/share1.mp4',
            'image'=>'videos/video_share.webp',
            'lenght'=>'1:10',
            'lang'=>'de'
        ] );

        HelpVedio::create( [
            'cat'=>'4',
            'cat_title'=>'',
            'sub'=>'4',
            'title'=>'agregar y administrar una nueva contribución',
            'video'=>'videos/share1.mp4',
            'image'=>'videos/video_share.webp',
            'lenght'=>'1:10',
            'lang'=>'es'
        ] );

        HelpVedio::create( [
            'cat'=>'4',
            'cat_title'=>'',
            'sub'=>'4',
            'title'=>'添加和管理新的贡献',
            'video'=>'videos/share1.mp4',
            'image'=>'videos/video_share.webp',
            'lenght'=>'一:十',
            'lang'=>'cn'
        ] );

        HelpVedio::create( [
            'cat'=>'4',
            'cat_title'=>'',
            'sub'=>'4',
            'title'=>'بدء المساهمة المضافه ومتابعتها',
            'video'=>'videos/share2.mp4',
            'image'=>'videos/video_share.webp',
            'lenght'=>'٢:٤٨',
            'lang'=>'ar'
        ] );

        HelpVedio::create( [
            'cat'=>'4',
            'cat_title'=>'',
            'sub'=>'4',
            'title'=>'start contribution and manage it',
            'video'=>'videos/share2.mp4',
            'image'=>'videos/video_share.webp',
            'lenght'=>'2:48',
            'lang'=>'en'
        ] );

        HelpVedio::create( [
            'cat'=>'4',
            'cat_title'=>'',
            'sub'=>'4',
            'title'=>'commencer la contribution et la gérer',
            'video'=>'videos/share2.mp4',
            'image'=>'videos/video_share.webp',
            'lenght'=>'2:48',
            'lang'=>'fr'
        ] );

        HelpVedio::create( [
            'cat'=>'4',
            'cat_title'=>'',
            'sub'=>'4',
            'title'=>'katkıyı başlat ve yönet',
            'video'=>'videos/share2.mp4',
            'image'=>'videos/video_share.webp',
            'lenght'=>'2:48',
            'lang'=>'tr'
        ] );

        HelpVedio::create( [
            'cat'=>'4',
            'cat_title'=>'',
            'sub'=>'4',
            'title'=>'начать вклад и управлять им',
            'video'=>'videos/share2.mp4',
            'image'=>'videos/video_share.webp',
            'lenght'=>'2:48',
            'lang'=>'ru'
        ] );

        HelpVedio::create( [
            'cat'=>'4',
            'cat_title'=>'',
            'sub'=>'4',
            'title'=>'Beitrag starten und verwalten',
            'video'=>'videos/share2.mp4',
            'image'=>'videos/video_share.webp',
            'lenght'=>'2:48',
            'lang'=>'de'
        ] );

        HelpVedio::create( [
            'cat'=>'4',
            'cat_title'=>'',
            'sub'=>'4',
            'title'=>'iniciar la contribución y gestionarla',
            'video'=>'videos/share2.mp4',
            'image'=>'videos/video_share.webp',
            'lenght'=>'2:48',
            'lang'=>'es'
        ] );

        HelpVedio::create( [
            'cat'=>'4',
            'cat_title'=>'',
            'sub'=>'4',
            'title'=>'开始贡献并管理它',
            'video'=>'videos/share2.mp4',
            'image'=>'videos/video_share.webp',
            'lenght'=>'二:四十八',
            'lang'=>'en'
        ] );

         HelpVedio::create( [
            'cat'=>'6',
            'cat_title'=>'عجلة الحياة',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'ar'
        ] );

        HelpVedio::create( [
            'cat'=>'6',
            'cat_title'=>'wheel of life',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'en'
        ] );

        HelpVedio::create( [
            'cat'=>'6',
            'cat_title'=>'roue de la vie',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'fr'
        ] );

        HelpVedio::create( [
            'cat'=>'6',
            'cat_title'=>'hayat çarkı',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'tr'
        ] );

        HelpVedio::create( [
            'cat'=>'6',
            'cat_title'=>'колесо жизни',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'ru'
        ] );

        HelpVedio::create( [
            'cat'=>'6',
            'cat_title'=>'das Lebensrad',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'de'
        ] );

        HelpVedio::create( [
            'cat'=>'6',
            'cat_title'=>'rueda de la vida',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'es'
        ] );

        HelpVedio::create( [
            'cat'=>'6',
            'cat_title'=>'生命之轮',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'cn'
        ] );

        HelpVedio::create( [
            'cat'=>'6',
            'cat_title'=>'',
            'sub'=>'6',
            'title'=>'إجراء تقييم عجلة الحياة',
            'video'=>'videos/wheel1.mp4',
            'image'=>'videos/video_wheel.webp',
            'lenght'=>'٠:٥٦',
            'lang'=>'ar'
        ] );

        HelpVedio::create( [
            'cat'=>'6',
            'cat_title'=>'',
            'sub'=>'6',
            'title'=>'Do wheel of life test',
            'video'=>'videos/wheel1.mp4',
            'image'=>'videos/video_wheel.webp',
            'lenght'=>'0:56',
            'lang'=>'en'
        ] );

        HelpVedio::create( [
            'cat'=>'6',
            'cat_title'=>'',
            'sub'=>'6',
            'title'=>'Faire le test de la roue de la vie',
            'video'=>'videos/wheel1.mp4',
            'image'=>'videos/video_wheel.webp',
            'lenght'=>'0:56',
            'lang'=>'fr'
        ] );

        HelpVedio::create( [
            'cat'=>'6',
            'cat_title'=>'',
            'sub'=>'6',
            'title'=>'Yaşam çarkı testi yapın',
            'video'=>'videos/wheel1.mp4',
            'image'=>'videos/video_wheel.webp',
            'lenght'=>'0:56',
            'lang'=>'tr'
        ] );

        HelpVedio::create( [
            'cat'=>'6',
            'cat_title'=>'',
            'sub'=>'6',
            'title'=>'Сделайте испытание колеса жизни',
            'video'=>'videos/wheel1.mp4',
            'image'=>'videos/video_wheel.webp',
            'lenght'=>'0:56',
            'lang'=>'ru'
        ] );

        HelpVedio::create( [
            'cat'=>'6',
            'cat_title'=>'',
            'sub'=>'6',
            'title'=>'Rad des Lebens testen',
            'video'=>'videos/wheel1.mp4',
            'image'=>'videos/video_wheel.webp',
            'lenght'=>'0:56',
            'lang'=>'de'
        ] );

        HelpVedio::create( [
            'cat'=>'6',
            'cat_title'=>'',
            'sub'=>'6',
            'title'=>'Haz la prueba de la rueda de la vida',
            'video'=>'videos/wheel1.mp4',
            'image'=>'videos/video_wheel.webp',
            'lenght'=>'0:56',
            'lang'=>'es'
        ] );

        HelpVedio::create( [
            'cat'=>'6',
            'cat_title'=>'',
            'sub'=>'6',
            'title'=>'做车轮寿命测试',
            'video'=>'videos/wheel1.mp4',
            'image'=>'videos/video_wheel.webp',
            'lenght'=>'零:五十六',
            'lang'=>'cn'
        ] );

        HelpVedio::create( [
            'cat'=>'6',
            'cat_title'=>'',
            'sub'=>'6',
            'title'=>'إضافة المهام لتحسين العجلة',
            'video'=>'videos/wheel2.mp4',
            'image'=>'videos/video_wheel.webp',
            'lenght'=>'١:٣٤',
            'lang'=>'ar'
        ] );

        HelpVedio::create( [
            'cat'=>'6',
            'cat_title'=>'',
            'sub'=>'6',
            'title'=>'add missions to enhance wheel',
            'video'=>'videos/wheel2.mp4',
            'image'=>'videos/video_wheel.webp',
            'lenght'=>'1:34',
            'lang'=>'en'
        ] );

        HelpVedio::create( [
            'cat'=>'6',
            'cat_title'=>'',
            'sub'=>'6',
            'title'=>'ajouter des missions pour améliorer la roue',
            'video'=>'videos/wheel2.mp4',
            'image'=>'videos/video_wheel.webp',
            'lenght'=>'1:34',
            'lang'=>'fr'
        ] );

        HelpVedio::create( [
            'cat'=>'6',
            'cat_title'=>'',
            'sub'=>'6',
            'title'=>'tekerleği geliştirmek için görevler ekleyin',
            'video'=>'videos/wheel2.mp4',
            'image'=>'videos/video_wheel.webp',
            'lenght'=>'1:34',
            'lang'=>'tr'
        ] );

        HelpVedio::create( [
            'cat'=>'6',
            'cat_title'=>'',
            'sub'=>'6',
            'title'=>'добавить миссии для улучшения колеса',
            'video'=>'videos/wheel2.mp4',
            'image'=>'videos/video_wheel.webp',
            'lenght'=>'1:34',
            'lang'=>'ru'
        ] );

        HelpVedio::create( [
            'cat'=>'6',
            'cat_title'=>'',
            'sub'=>'6',
            'title'=>'füge Missionen hinzu, um das Rad zu verbessern',
            'video'=>'videos/wheel2.mp4',
            'image'=>'videos/video_wheel.webp',
            'lenght'=>'1:34',
            'lang'=>'de'
        ] );

        HelpVedio::create( [
            'cat'=>'6',
            'cat_title'=>'',
            'sub'=>'6',
            'title'=>'agregar misiones para mejorar la rueda',
            'video'=>'videos/wheel2.mp4',
            'image'=>'videos/video_wheel.webp',
            'lenght'=>'1:34',
            'lang'=>'es'
        ] );

        HelpVedio::create( [
            'cat'=>'6',
            'cat_title'=>'',
            'sub'=>'6',
            'title'=>'添加任务以增强轮子',
            'video'=>'videos/wheel2.mp4',
            'image'=>'videos/video_wheel.webp',
            'lenght'=>'一:三十四',
            'lang'=>'cn'
        ] );
        
         HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'إضافة أعضاء العائلة',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'ar'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'add family members',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'en'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'ajouter des membres de la famille',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'fr'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'aile üyeleri ekle',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'tr'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'добавить членов семьи',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'ru'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'Familienmitglieder hinzufügen',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'de'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'agregar miembros de la familia',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'es'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'添加家庭成员',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'cn'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'إضافة كاملة لنموذج',
            'video'=>'videos/tree4.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'١:٢٥',
            'lang'=>'ar'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'full adding as sample',
            'video'=>'videos/tree4.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'1:25',
            'lang'=>'en'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'ajout complet comme échantillon',
            'video'=>'videos/tree4.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'1:25',
            'lang'=>'fr'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'örnek olarak tam ekleme',
            'video'=>'videos/tree4.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'1:25',
            'lang'=>'tr'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'полное добавление в качестве образца',
            'video'=>'videos/tree4.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'1:25',
            'lang'=>'ru'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'vollständiges Hinzufügen als Probe',
            'video'=>'videos/tree4.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'1:25',
            'lang'=>'de'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'adición completa como muestra',
            'video'=>'videos/tree4.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'1:25',
            'lang'=>'es'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'作为样品完全添加',
            'video'=>'videos/tree4.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'一:二十五',
            'lang'=>'cn'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'إضافة زوجة وأولادها',
            'video'=>'videos/tree1.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'١:٢٥',
            'lang'=>'ar'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'adding wife with children',
            'video'=>'videos/tree1.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'1:25',
            'lang'=>'en'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'ajouter une femme avec des enfants',
            'video'=>'videos/tree1.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'1:25',
            'lang'=>'fr'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'çocuklu eş eklemek',
            'video'=>'videos/tree1.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'1:25',
            'lang'=>'tr'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'добавление жены с детьми',
            'video'=>'videos/tree1.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'1:25',
            'lang'=>'ru'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'Frau mit Kindern hinzufügen',
            'video'=>'videos/tree1.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'1:25',
            'lang'=>'de'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'agregando esposa con hijos',
            'video'=>'videos/tree1.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'1:25',
            'lang'=>'es'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'添加带孩子的妻子',
            'video'=>'videos/tree1.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'一:二十五',
            'lang'=>'cn'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'إضافة جد',
            'video'=>'videos/tree3.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'٠:٥٢',
            'lang'=>'ar'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'adding grandparent',
            'video'=>'videos/tree3.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'0:52',
            'lang'=>'en'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'ajouter un grand-parent',
            'video'=>'videos/tree3.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'0:52',
            'lang'=>'fr'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'büyükanne ve büyükbaba ekleme',
            'video'=>'videos/tree3.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'0:52',
            'lang'=>'tr'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'добавление бабушки и дедушки',
            'video'=>'videos/tree3.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'0:52',
            'lang'=>'ru'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'Großeltern hinzufügen',
            'video'=>'videos/tree3.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'0:52',
            'lang'=>'de'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'agregando abuelo',
            'video'=>'videos/tree3.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'0:52',
            'lang'=>'es'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'添加祖父母',
            'video'=>'videos/tree3.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'零:五十二',
            'lang'=>'cn'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'إضافة عم أو خال',
            'video'=>'videos/tree2.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'٠:٥٢',
            'lang'=>'ar'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'adding uncle',
            'video'=>'videos/tree2.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'0:52',
            'lang'=>'en'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>"ajout d'oncle",
            'video'=>'videos/tree2.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'0:52',
            'lang'=>'fr'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'amca eklemek',
            'video'=>'videos/tree2.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'0:52',
            'lang'=>'tr'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'добавление дяди',
            'video'=>'videos/tree2.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'0:52',
            'lang'=>'ru'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'Onkel hinzufügen',
            'video'=>'videos/tree2.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'0:52',
            'lang'=>'de'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'agregando tío',
            'video'=>'videos/tree2.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'0:52',
            'lang'=>'es'
        ] );

        HelpVedio::create( [
            'cat'=>'3',
            'cat_title'=>'',
            'sub'=>'3',
            'title'=>'添加叔叔',
            'video'=>'videos/tree2.mp4',
            'image'=>'videos/video_tree.webp',
            'lenght'=>'零:五十二',
            'lang'=>'cn'
        ] );
        

         HelpVedio::create( [
            'cat'=>'5',
            'cat_title'=>'إضافة صور العائلة',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'ar'
        ] );

        HelpVedio::create( [
            'cat'=>'5',
            'cat_title'=>'add family gallery pics',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'en'
        ] );

        HelpVedio::create( [
            'cat'=>'5',
            'cat_title'=>'ajouter des photos de la galerie de famille',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'fr'
        ] );

        HelpVedio::create( [
            'cat'=>'5',
            'cat_title'=>'aile galerisi resimleri ekle',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'tr'
        ] );

        HelpVedio::create( [
            'cat'=>'5',
            'cat_title'=>'добавить фото из семейной галереи',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'ru'
        ] );

        HelpVedio::create( [
            'cat'=>'5',
            'cat_title'=>'Bilder der Familiengalerie hinzufügen',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'de'
        ] );

        HelpVedio::create( [
            'cat'=>'5',
            'cat_title'=>'agregar fotos de la galería familiar',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'es'
        ] );

        HelpVedio::create( [
            'cat'=>'5',
            'cat_title'=>'添加家庭画廊照片',
            'sub'=>'0',
            'title'=>'',
            'video'=>'',
            'image'=>'',
            'lenght'=>'',
            'lang'=>'cn'
        ] );

        HelpVedio::create( [
            'cat'=>'5',
            'cat_title'=>'',
            'sub'=>'5',
            'title'=>'إضافة صورة جديدة في معرض العائلة',
            'video'=>'videos/pic.mp4',
            'image'=>'videos/video_pic.webp',
            'lenght'=>'٠:٣٥',
            'lang'=>'ar'
        ] );

        HelpVedio::create( [
            'cat'=>'5',
            'cat_title'=>'',
            'sub'=>'5',
            'title'=>'add new pic in family gallery',
            'video'=>'videos/pic.mp4',
            'image'=>'videos/video_pic.webp',
            'lenght'=>'0:35',
            'lang'=>'en'
        ] );

        HelpVedio::create( [
            'cat'=>'5',
            'cat_title'=>'',
            'sub'=>'5',
            'title'=>'ajouter une nouvelle photo dans la galerie familiale',
            'video'=>'videos/pic.mp4',
            'image'=>'videos/video_pic.webp',
            'lenght'=>'0:35',
            'lang'=>'fr'
        ] );

        HelpVedio::create( [
            'cat'=>'5',
            'cat_title'=>'',
            'sub'=>'5',
            'title'=>'aile galerisine yeni resim ekle',
            'video'=>'videos/pic.mp4',
            'image'=>'videos/video_pic.webp',
            'lenght'=>'0:35',
            'lang'=>'tr'
        ] );

        HelpVedio::create( [
            'cat'=>'5',
            'cat_title'=>'',
            'sub'=>'5',
            'title'=>'добавить новую картинку в семейную галерею',
            'video'=>'videos/pic.mp4',
            'image'=>'videos/video_pic.webp',
            'lenght'=>'0:35',
            'lang'=>'ru'
        ] );

        HelpVedio::create( [
            'cat'=>'5',
            'cat_title'=>'',
            'sub'=>'5',
            'title'=>'neues Bild in der Familiengalerie hinzufügen',
            'video'=>'videos/pic.mp4',
            'image'=>'videos/video_pic.webp',
            'lenght'=>'0:35',
            'lang'=>'de'
        ] );

        HelpVedio::create( [
            'cat'=>'5',
            'cat_title'=>'',
            'sub'=>'5',
            'title'=>'agregar una nueva foto en la galería familiar',
            'video'=>'videos/pic.mp4',
            'image'=>'videos/video_pic.webp',
            'lenght'=>'0:35',
            'lang'=>'es'
        ] );

        HelpVedio::create( [
            'cat'=>'5',
            'cat_title'=>'',
            'sub'=>'5',
            'title'=>'在家庭图库中添加新图片',
            'video'=>'videos/pic.mp4',
            'image'=>'videos/video_pic.webp',
            'lenght'=>'零:三十五',
            'lang'=>'cn'
        ] );
        
    }
}
