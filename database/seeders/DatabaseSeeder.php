<?php

namespace Database\Seeders;

use App\Models\QuranWord;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (app()->environment('local')){

             User::create([
                'id'=>1,
                'name' => 'hossam',
                'email'=> 'microsystems2@gmail.com',
                'phone_number' => '01001191955',
                'password' => Hash::make('123123'), // password
                'family_id' => 1,
                'gender' => 'male',
                'country_id' => '63',
                'email_verified_at' => Carbon::now(),
            ]);

            $this->call(QuranSurahSeeder::class);
            // $this->call(QuranWordSeeder::class);
            // $this->call(TafseerNameSeeder::class);
            // $this->call(Tafseer1Seeder::class);
            // $this->call(Tafseer2Seeder::class);
            // $this->call(Tafseer3Seeder::class);
            // $this->call(Tafseer4Seeder::class);

            $this->call(FamilyNamesSeeder::class);
            $this->call(PlanUserSeeder::class);
            $this->call(UserpersonalitySeeder::class);
            $this->call(WheelUserSeeder::class);
            $this->call(PersonalitySeeder::class);

            $this->call(CountrySeeder::class);
            $this->call(CountryEventSeeder::class);
            $this->call(ShareTypesSeeder::class);

            $this->call(WheelTypeSeeder::class);
            $this->call(WheelQuesSeeder::class);

            $this->call(HelpVideoSeeder::class);
            $this->call(ManSaySeeder::class);
            $this->call(FaqsHelpSeeder::class);

            $this->call(TestmonialSeeder::class);
        }
    }
}
