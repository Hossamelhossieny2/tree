<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Tafseer1Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Tafseer 1 table start seed!');

        $path = 'database/sql/tafseer1.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Tafseer 1 table seeded ok!');
    }
}
