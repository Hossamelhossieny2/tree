<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Country;
class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::create( [
            'iso'=>'AF',
            'name'=>'AFGHANISTAN',
            'nicename'=>'Afghanistan',
            'iso3'=>'AFG',
            'numcode'=>4,
            'phonecode'=>93,
            'lat'=>33.60567630,
            'lon'=>65.69806670,
            'value'=>40860011
        ] );



        Country::create( [
            'iso'=>'AL',
            'name'=>'ALBANIA',
            'nicename'=>'Albania',
            'iso3'=>'ALB',
            'numcode'=>8,
            'phonecode'=>355,
            'lat'=>41.31298300,
            'lon'=>19.90954230,
            'value'=>3698386
        ] );



        Country::create( [
            'iso'=>'DZ',
            'name'=>'ALGERIA',
            'nicename'=>'Algeria',
            'iso3'=>'DZA',
            'numcode'=>12,
            'phonecode'=>213,
            'lat'=>28.13350670,
            'lon'=>2.80114230,
            'value'=>47519416
        ] );



        Country::create( [
            'iso'=>'AS',
            'name'=>'AMERICAN SAMOA',
            'nicename'=>'American Samoa',
            'iso3'=>'ASM',
            'numcode'=>16,
            'phonecode'=>1684,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'AD',
            'name'=>'ANDORRA',
            'nicename'=>'Andorra',
            'iso3'=>'AND',
            'numcode'=>20,
            'phonecode'=>376,
            'lat'=>42.54243760,
            'lon'=>1.45758910,
            'value'=>99089
        ] );



        Country::create( [
            'iso'=>'AO',
            'name'=>'ANGOLA',
            'nicename'=>'Angola',
            'iso3'=>'AGO',
            'numcode'=>24,
            'phonecode'=>244,
            'lat'=>'-8.83680400',
            'lon'=>13.23317400,
            'value'=>22561196
        ] );



        Country::create( [
            'iso'=>'AI',
            'name'=>'ANGUILLA',
            'nicename'=>'Anguilla',
            'iso3'=>'AIA',
            'numcode'=>660,
            'phonecode'=>1264,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'AQ',
            'name'=>'ANTARCTICA',
            'nicename'=>'Antarctica',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>0,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'AG',
            'name'=>'ANTIGUA AND BARBUDA',
            'nicename'=>'Antigua and Barbuda',
            'iso3'=>'ATG',
            'numcode'=>28,
            'phonecode'=>1268,
            'lat'=>17.32635810,
            'lon'=>'-62.29038860',
            'value'=>103053
        ] );



        Country::create( [
            'iso'=>'AR',
            'name'=>'ARGENTINA',
            'nicename'=>'Argentina',
            'iso3'=>'ARG',
            'numcode'=>32,
            'phonecode'=>54,
            'lat'=>'-34.60638900',
            'lon'=>'-58.43527800',
            'value'=>46879245
        ] );



        Country::create( [
            'iso'=>'AM',
            'name'=>'ARMENIA',
            'nicename'=>'Armenia',
            'iso3'=>'ARM',
            'numcode'=>51,
            'phonecode'=>374,
            'lat'=>40.06731820,
            'lon'=>43.91966480,
            'value'=>3565271
        ] );



        Country::create( [
            'iso'=>'AW',
            'name'=>'ARUBA',
            'nicename'=>'Aruba',
            'iso3'=>'ABW',
            'numcode'=>533,
            'phonecode'=>297,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'AU',
            'name'=>'AUSTRALIA',
            'nicename'=>'Australia',
            'iso3'=>'AUS',
            'numcode'=>36,
            'phonecode'=>61,
            'lat'=>'-33.86148100',
            'lon'=>151.20547500,
            'value'=>26013689
        ] );



        Country::create( [
            'iso'=>'AT',
            'name'=>'AUSTRIA',
            'nicename'=>'Austria',
            'iso3'=>'AUT',
            'numcode'=>40,
            'phonecode'=>43,
            'lat'=>48.20000000,
            'lon'=>16.36666700,
            'value'=>9681850
        ] );



        Country::create( [
            'iso'=>'AZ',
            'name'=>'AZERBAIJAN',
            'nicename'=>'Azerbaijan',
            'iso3'=>'AZE',
            'numcode'=>31,
            'phonecode'=>994,
            'lat'=>40.39527800,
            'lon'=>49.88222200,
            'value'=>10543200
        ] );



        Country::create( [
            'iso'=>'BS',
            'name'=>'BAHAMAS',
            'nicename'=>'Bahamas',
            'iso3'=>'BHS',
            'numcode'=>44,
            'phonecode'=>1242,
            'lat'=>24.42292980,
            'lon'=>'-78.21045650',
            'value'=>399252
        ] );



        Country::create( [
            'iso'=>'BH',
            'name'=>'BAHRAIN',
            'nicename'=>'Bahrain',
            'iso3'=>'BHR',
            'numcode'=>48,
            'phonecode'=>973,
            'lat'=>25.94129800,
            'lon'=>50.30738960,
            'value'=>1522065
        ] );



        Country::create( [
            'iso'=>'BD',
            'name'=>'BANGLADESH',
            'nicename'=>'Bangladesh',
            'iso3'=>'BGD',
            'numcode'=>50,
            'phonecode'=>880,
            'lat'=>23.49563060,
            'lon'=>88.10059120,
            'value'=>173067706
        ] );



        Country::create( [
            'iso'=>'BB',
            'name'=>'BARBADOS',
            'nicename'=>'Barbados',
            'iso3'=>'BRB',
            'numcode'=>52,
            'phonecode'=>1246,
            'lat'=>13.18847290,
            'lon'=>'-59.67534040',
            'value'=>315013
        ] );



        Country::create( [
            'iso'=>'BY',
            'name'=>'BELARUS',
            'nicename'=>'Belarus',
            'iso3'=>'BLR',
            'numcode'=>112,
            'phonecode'=>375,
            'lat'=>53.90000000,
            'lon'=>27.56666700,
            'value'=>10893950
        ] );



        Country::create( [
            'iso'=>'BE',
            'name'=>'BELGIUM',
            'nicename'=>'Belgium',
            'iso3'=>'BEL',
            'numcode'=>56,
            'phonecode'=>32,
            'lat'=>50.83333300,
            'lon'=>4.33333300,
            'value'=>12659199
        ] );



        Country::create( [
            'iso'=>'BZ',
            'name'=>'BELIZE',
            'nicename'=>'Belize',
            'iso3'=>'BLZ',
            'numcode'=>84,
            'phonecode'=>501,
            'lat'=>17.19030800,
            'lon'=>'-89.44138440',
            'value'=>410089
        ] );



        Country::create( [
            'iso'=>'BJ',
            'name'=>'BENIN',
            'nicename'=>'Benin',
            'iso3'=>'BEN',
            'numcode'=>204,
            'phonecode'=>229,
            'lat'=>9.30576880,
            'lon'=>0.06526010,
            'value'=>10464910
        ] );



        Country::create( [
            'iso'=>'BM',
            'name'=>'BERMUDA',
            'nicename'=>'Bermuda',
            'iso3'=>'BMU',
            'numcode'=>60,
            'phonecode'=>1441,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'BT',
            'name'=>'BHUTAN',
            'nicename'=>'Bhutan',
            'iso3'=>'BTN',
            'numcode'=>64,
            'phonecode'=>975,
            'lat'=>27.47244930,
            'lon'=>89.31467090,
            'value'=>849007
        ] );



        Country::create( [
            'iso'=>'BO',
            'name'=>'BOLIVIA',
            'nicename'=>'Bolivia',
            'iso3'=>'BOL',
            'numcode'=>68,
            'phonecode'=>591,
            'lat'=>'-17.78329600',
            'lon'=>'-63.18211700',
            'value'=>11601324
        ] );



        Country::create( [
            'iso'=>'BA',
            'name'=>'BOSNIA AND HERZEGOVINA',
            'nicename'=>'Bosnia and Herzegovina',
            'iso3'=>'BIH',
            'numcode'=>70,
            'phonecode'=>387,
            'lat'=>43.85000000,
            'lon'=>18.38333300,
            'value'=>4315062
        ] );



        Country::create( [
            'iso'=>'BW',
            'name'=>'BOTSWANA',
            'nicename'=>'Botswana',
            'iso3'=>'BWA',
            'numcode'=>72,
            'phonecode'=>267,
            'lat'=>'-24.64638900',
            'lon'=>25.91194400,
            'value'=>2335348
        ] );



        Country::create( [
            'iso'=>'BV',
            'name'=>'BOUVET ISLAND',
            'nicename'=>'Bouvet Island',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>0,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'BR',
            'name'=>'BRAZIL',
            'nicename'=>'Brazil',
            'iso3'=>'BRA',
            'numcode'=>76,
            'phonecode'=>55,
            'lat'=>'-23.57325200',
            'lon'=>'-46.64168100',
            'value'=>226153266
        ] );



        Country::create( [
            'iso'=>'IO',
            'name'=>'BRITISH INDIAN OCEAN TERRITORY',
            'nicename'=>'British Indian Ocean Territory',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>246,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'BN',
            'name'=>'BRUNEI DARUSSALAM',
            'nicename'=>'Brunei Darussalam',
            'iso3'=>'BRN',
            'numcode'=>96,
            'phonecode'=>673,
            'lat'=>4.55076090,
            'lon'=>114.43938800,
            'value'=>466828
        ] );



        Country::create( [
            'iso'=>'BG',
            'name'=>'BULGARIA',
            'nicename'=>'Bulgaria',
            'iso3'=>'BGR',
            'numcode'=>100,
            'phonecode'=>359,
            'lat'=>42.68333300,
            'lon'=>23.31666700,
            'value'=>8597400
        ] );



        Country::create( [
            'iso'=>'BF',
            'name'=>'BURKINA FASO',
            'nicename'=>'Burkina Faso',
            'iso3'=>'BFA',
            'numcode'=>854,
            'phonecode'=>226,
            'lat'=>12.37027800,
            'lon'=>'-1.52472200',
            'value'=>19513021
        ] );



        Country::create( [
            'iso'=>'BI',
            'name'=>'BURUNDI',
            'nicename'=>'Burundi',
            'iso3'=>'BDI',
            'numcode'=>108,
            'phonecode'=>257,
            'lat'=>'-3.38220000',
            'lon'=>29.36440000,
            'value'=>9861447
        ] );



        Country::create( [
            'iso'=>'KH',
            'name'=>'CAMBODIA',
            'nicename'=>'Cambodia',
            'iso3'=>'KHM',
            'numcode'=>116,
            'phonecode'=>855,
            'lat'=>11.56245000,
            'lon'=>104.91600600,
            'value'=>16450960
        ] );



        Country::create( [
            'iso'=>'CM',
            'name'=>'CAMEROON',
            'nicename'=>'Cameroon',
            'iso3'=>'CMR',
            'numcode'=>120,
            'phonecode'=>237,
            'lat'=>4.04690000,
            'lon'=>9.70840000,
            'value'=>23034916
        ] );



        Country::create( [
            'iso'=>'CA',
            'name'=>'CANADA',
            'nicename'=>'Canada',
            'iso3'=>'CAN',
            'numcode'=>124,
            'phonecode'=>1,
            'lat'=>43.66666700,
            'lon'=>'-79.41666700',
            'value'=>39655195
        ] );



        Country::create( [
            'iso'=>'CV',
            'name'=>'CAPE VERDE',
            'nicename'=>'Cape Verde',
            'iso3'=>'CPV',
            'numcode'=>132,
            'phonecode'=>238,
            'lat'=>16.02285550,
            'lon'=>'-25.10989270',
            'value'=>575672
        ] );



        Country::create( [
            'iso'=>'KY',
            'name'=>'CAYMAN ISLANDS',
            'nicename'=>'Cayman Islands',
            'iso3'=>'CYM',
            'numcode'=>136,
            'phonecode'=>1345,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'CF',
            'name'=>'CENTRAL AFRICAN REPUBLIC',
            'nicename'=>'Central African Republic',
            'iso3'=>'CAF',
            'numcode'=>140,
            'phonecode'=>236,
            'lat'=>6.61124520,
            'lon'=>16.44028490,
            'value'=>5159862
        ] );



        Country::create( [
            'iso'=>'TD',
            'name'=>'CHAD',
            'nicename'=>'Chad',
            'iso3'=>'TCD',
            'numcode'=>148,
            'phonecode'=>235,
            'lat'=>12.10850200,
            'lon'=>15.04817300,
            'value'=>13254320
        ] );



        Country::create( [
            'iso'=>'CL',
            'name'=>'CHILE',
            'nicename'=>'Chile',
            'iso3'=>'CHL',
            'numcode'=>152,
            'phonecode'=>56,
            'lat'=>'-33.45000000',
            'lon'=>'-70.66666700',
            'value'=>19859953
        ] );



        Country::create( [
            'iso'=>'CN',
            'name'=>'CHINA',
            'nicename'=>'China',
            'iso3'=>'CHN',
            'numcode'=>156,
            'phonecode'=>86,
            'lat'=>35.22222200,
            'lon'=>116.45805600,
            'value'=>1594049499
        ] );



        Country::create( [
            'iso'=>'CX',
            'name'=>'CHRISTMAS ISLAND',
            'nicename'=>'Christmas Island',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>61,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'CC',
            'name'=>'COCOS (KEELING) ISLANDS',
            'nicename'=>'Cocos (Keeling) Islands',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>672,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'CO',
            'name'=>'COLOMBIA',
            'nicename'=>'Colombia',
            'iso3'=>'COL',
            'numcode'=>170,
            'phonecode'=>57,
            'lat'=>4.64917800,
            'lon'=>'-74.06282700',
            'value'=>53966193
        ] );



        Country::create( [
            'iso'=>'KM',
            'name'=>'COMOROS',
            'nicename'=>'Comoros',
            'iso3'=>'COM',
            'numcode'=>174,
            'phonecode'=>269,
            'lat'=>'-11.90223590',
            'lon'=>43.60057990,
            'value'=>867034
        ] );



        Country::create( [
            'iso'=>'CG',
            'name'=>'CONGO',
            'nicename'=>'Congo',
            'iso3'=>'COG',
            'numcode'=>178,
            'phonecode'=>242,
            'lat'=>'-4.32710800',
            'lon'=>15.30717000,
            'value'=>4760710
        ] );



        Country::create( [
            'iso'=>'CD',
            'name'=>'CONGO, THE DEMOCRATIC REPUBLIC OF THE',
            'nicename'=>'Congo, the Democratic Republic of the',
            'iso3'=>'COD',
            'numcode'=>180,
            'phonecode'=>242,
            'lat'=>'-4.32710800',
            'lon'=>15.30717000,
            'value'=>77921213
        ] );



        Country::create( [
            'iso'=>'CK',
            'name'=>'COOK ISLANDS',
            'nicename'=>'Cook Islands',
            'iso3'=>'COK',
            'numcode'=>184,
            'phonecode'=>682,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'CR',
            'name'=>'COSTA RICA',
            'nicename'=>'Costa Rica',
            'iso3'=>'CRI',
            'numcode'=>188,
            'phonecode'=>506,
            'lat'=>8.35701130,
            'lon'=>'-87.05277880',
            'value'=>5435561
        ] );



        Country::create( [
            'iso'=>'CI',
            'name'=>'COTE D\'IVOIRE',
            'nicename'=>'Cote D\'Ivoire',
            'iso3'=>'CIV',
            'numcode'=>384,
            'phonecode'=>225,
            'lat'=>5.35052700,
            'lon'=>'-4.02463700',
            'value'=>23175828
        ] );



        Country::create( [
            'iso'=>'HR',
            'name'=>'CROATIA',
            'nicename'=>'Croatia',
            'iso3'=>'HRV',
            'numcode'=>191,
            'phonecode'=>385,
            'lat'=>45.80000000,
            'lon'=>16.00000000,
            'value'=>5068050
        ] );



        Country::create( [
            'iso'=>'CU',
            'name'=>'CUBA',
            'nicename'=>'Cuba',
            'iso3'=>'CUB',
            'numcode'=>192,
            'phonecode'=>53,
            'lat'=>23.13194400,
            'lon'=>'-82.36416700',
            'value'=>12941714
        ] );



        Country::create( [
            'iso'=>'CY',
            'name'=>'CYPRUS',
            'nicename'=>'Cyprus',
            'iso3'=>'CYP',
            'numcode'=>196,
            'phonecode'=>357,
            'lat'=>35.16863230,
            'lon'=>32.86476840,
            'value'=>1284048
        ] );



        Country::create( [
            'iso'=>'CZ',
            'name'=>'CZECH REPUBLIC',
            'nicename'=>'Czech Republic',
            'iso3'=>'CZE',
            'numcode'=>203,
            'phonecode'=>420,
            'lat'=>49.78567100,
            'lon'=>13.23243580,
            'value'=>12127899
        ] );



        Country::create( [
            'iso'=>'DK',
            'name'=>'DENMARK',
            'nicename'=>'Denmark',
            'iso3'=>'DNK',
            'numcode'=>208,
            'phonecode'=>45,
            'lat'=>55.66666700,
            'lon'=>12.58333300,
            'value'=>6410099
        ] );



        Country::create( [
            'iso'=>'DJ',
            'name'=>'DJIBOUTI',
            'nicename'=>'Djibouti',
            'iso3'=>'DJI',
            'numcode'=>262,
            'phonecode'=>253,
            'lat'=>11.81412040,
            'lon'=>42.06691470,
            'value'=>1041398
        ] );



        Country::create( [
            'iso'=>'DM',
            'name'=>'DOMINICA',
            'nicename'=>'Dominica',
            'iso3'=>'DMA',
            'numcode'=>212,
            'phonecode'=>1767,
            'lat'=>15.42666030,
            'lon'=>'-61.49758980',
            'value'=>77826
        ] );



        Country::create( [
            'iso'=>'DO',
            'name'=>'DOMINICAN REPUBLIC',
            'nicename'=>'Dominican Republic',
            'iso3'=>'DOM',
            'numcode'=>214,
            'phonecode'=>1809,
            'lat'=>18.46666700,
            'lon'=>'-69.90000000',
            'value'=>11564608
        ] );



        Country::create( [
            'iso'=>'EC',
            'name'=>'ECUADOR',
            'nicename'=>'Ecuador',
            'iso3'=>'ECU',
            'numcode'=>218,
            'phonecode'=>593,
            'lat'=>'-2.20584200',
            'lon'=>'-79.90794800',
            'value'=>16865963
        ] );



        Country::create( [
            'iso'=>'EG',
            'name'=>'EGYPT',
            'nicename'=>'Egypt',
            'iso3'=>'EGY',
            'numcode'=>818,
            'phonecode'=>20,
            'lat'=>29.86000000,
            'lon'=>30.34000000,
            'value'=>113317285
        ] );



        Country::create( [
            'iso'=>'SV',
            'name'=>'EL SALVADOR',
            'nicename'=>'El Salvador',
            'iso3'=>'SLV',
            'numcode'=>222,
            'phonecode'=>503,
            'lat'=>13.74967980,
            'lon'=>'-89.49070720',
            'value'=>7161614
        ] );



        Country::create( [
            'iso'=>'GQ',
            'name'=>'EQUATORIAL GUINEA',
            'nicename'=>'Equatorial Guinea',
            'iso3'=>'GNQ',
            'numcode'=>226,
            'phonecode'=>240,
            'lat'=>1.14862070,
            'lon'=>6.19343780,
            'value'=>828244
        ] );



        Country::create( [
            'iso'=>'ER',
            'name'=>'ERITREA',
            'nicename'=>'Eritrea',
            'iso3'=>'ERI',
            'numcode'=>232,
            'phonecode'=>291,
            'lat'=>15.18176590,
            'lon'=>37.58825980,
            'value'=>6227571
        ] );



        Country::create( [
            'iso'=>'EE',
            'name'=>'ESTONIA',
            'nicename'=>'Estonia',
            'iso3'=>'EST',
            'numcode'=>233,
            'phonecode'=>372,
            'lat'=>58.60376400,
            'lon'=>22.69016910,
            'value'=>1540999
        ] );



        Country::create( [
            'iso'=>'ET',
            'name'=>'ETHIOPIA',
            'nicename'=>'Ethiopia',
            'iso3'=>'ETH',
            'numcode'=>231,
            'phonecode'=>251,
            'lat'=>9.02432500,
            'lon'=>38.74922600,
            'value'=>97444401
        ] );



        Country::create( [
            'iso'=>'FK',
            'name'=>'FALKLAND ISLANDS (MALVINAS)',
            'nicename'=>'Falkland Islands (Malvinas)',
            'iso3'=>'FLK',
            'numcode'=>238,
            'phonecode'=>500,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'FO',
            'name'=>'FAROE ISLANDS',
            'nicename'=>'Faroe Islands',
            'iso3'=>'FRO',
            'numcode'=>234,
            'phonecode'=>298,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'FJ',
            'name'=>'FIJI',
            'nicename'=>'Fiji',
            'iso3'=>'FJI',
            'numcode'=>242,
            'phonecode'=>679,
            'lat'=>'-16.53691320',
            'lon'=>177.21781060,
            'value'=>998666
        ] );



        Country::create( [
            'iso'=>'FI',
            'name'=>'FINLAND',
            'nicename'=>'Finland',
            'iso3'=>'FIN',
            'numcode'=>246,
            'phonecode'=>358,
            'lat'=>60.17555600,
            'lon'=>24.93416700,
            'value'=>6195049
        ] );



        Country::create( [
            'iso'=>'FR',
            'name'=>'FRANCE',
            'nicename'=>'France',
            'iso3'=>'FRA',
            'numcode'=>250,
            'phonecode'=>33,
            'lat'=>47.06893260,
            'lon'=>1.99924630,
            'value'=>75252034
        ] );



        Country::create( [
            'iso'=>'GF',
            'name'=>'FRENCH GUIANA',
            'nicename'=>'French Guiana',
            'iso3'=>'GUF',
            'numcode'=>254,
            'phonecode'=>594,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'PF',
            'name'=>'FRENCH POLYNESIA',
            'nicename'=>'French Polynesia',
            'iso3'=>'PYF',
            'numcode'=>258,
            'phonecode'=>689,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'TF',
            'name'=>'FRENCH SOUTHERN TERRITORIES',
            'nicename'=>'French Southern Territories',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>0,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'GA',
            'name'=>'GABON',
            'nicename'=>'Gabon',
            'iso3'=>'GAB',
            'numcode'=>266,
            'phonecode'=>241,
            'lat'=>'-0.91753890',
            'lon'=>9.22980680,
            'value'=>1764401
        ] );



        Country::create( [
            'iso'=>'GM',
            'name'=>'GAMBIA',
            'nicename'=>'Gambia',
            'iso3'=>'GMB',
            'numcode'=>270,
            'phonecode'=>220,
            'lat'=>13.41819640,
            'lon'=>'-15.92935060',
            'value'=>2042518
        ] );



        Country::create( [
            'iso'=>'GE',
            'name'=>'GEORGIA',
            'nicename'=>'Georgia',
            'iso3'=>'GEO',
            'numcode'=>268,
            'phonecode'=>995,
            'lat'=>42.30299150,
            'lon'=>41.11475540,
            'value'=>5158900
        ] );



        Country::create( [
            'iso'=>'DE',
            'name'=>'GERMANY',
            'nicename'=>'Germany',
            'iso3'=>'DEU',
            'numcode'=>276,
            'phonecode'=>49,
            'lat'=>52.51666700,
            'lon'=>13.40000000,
            'value'=>83240000
        ] );



        Country::create( [
            'iso'=>'GH',
            'name'=>'GHANA',
            'nicename'=>'Ghana',
            'iso3'=>'GHA',
            'numcode'=>288,
            'phonecode'=>233,
            'lat'=>7.90392640,
            'lon'=>'-3.27454050',
            'value'=>28710688
        ] );



        Country::create( [
            'iso'=>'GI',
            'name'=>'GIBRALTAR',
            'nicename'=>'Gibraltar',
            'iso3'=>'GIB',
            'numcode'=>292,
            'phonecode'=>350,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'GR',
            'name'=>'GREECE',
            'nicename'=>'Greece',
            'iso3'=>'GRC',
            'numcode'=>300,
            'phonecode'=>30,
            'lat'=>37.98333300,
            'lon'=>23.73333300,
            'value'=>12999599
        ] );



        Country::create( [
            'iso'=>'GL',
            'name'=>'GREENLAND',
            'nicename'=>'Greenland',
            'iso3'=>'GRL',
            'numcode'=>304,
            'phonecode'=>299,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'GD',
            'name'=>'GRENADA',
            'nicename'=>'Grenada',
            'iso3'=>'GRD',
            'numcode'=>308,
            'phonecode'=>1473,
            'lat'=>12.26010250,
            'lon'=>'-61.73038500',
            'value'=>120623
        ] );



        Country::create( [
            'iso'=>'GP',
            'name'=>'GUADELOUPE',
            'nicename'=>'Guadeloupe',
            'iso3'=>'GLP',
            'numcode'=>312,
            'phonecode'=>590,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'GU',
            'name'=>'GUAM',
            'nicename'=>'Guam',
            'iso3'=>'GUM',
            'numcode'=>316,
            'phonecode'=>1671,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'GT',
            'name'=>'GUATEMALA',
            'nicename'=>'Guatemala',
            'iso3'=>'GTM',
            'numcode'=>320,
            'phonecode'=>502,
            'lat'=>14.63342600,
            'lon'=>'-90.51672200',
            'value'=>16970913
        ] );



        Country::create( [
            'iso'=>'GN',
            'name'=>'GUINEA',
            'nicename'=>'Guinea',
            'iso3'=>'GIN',
            'numcode'=>324,
            'phonecode'=>224,
            'lat'=>9.50916700,
            'lon'=>'-13.71222200',
            'value'=>11755079
        ] );



        Country::create( [
            'iso'=>'GW',
            'name'=>'GUINEA-BISSAU',
            'nicename'=>'Guinea-Bissau',
            'iso3'=>'GNB',
            'numcode'=>624,
            'phonecode'=>245,
            'lat'=>11.70128560,
            'lon'=>'-16.41047130',
            'value'=>1779120
        ] );



        Country::create( [
            'iso'=>'GY',
            'name'=>'GUYANA',
            'nicename'=>'Guyana',
            'iso3'=>'GUY',
            'numcode'=>328,
            'phonecode'=>592,
            'lat'=>4.94516780,
            'lon'=>'-61.19716850',
            'value'=>869445
        ] );



        Country::create( [
            'iso'=>'HT',
            'name'=>'HAITI',
            'nicename'=>'Haiti',
            'iso3'=>'HTI',
            'numcode'=>332,
            'phonecode'=>509,
            'lat'=>19.03565340,
            'lon'=>'-73.67543020',
            'value'=>11642355
        ] );



        Country::create( [
            'iso'=>'HM',
            'name'=>'HEARD ISLAND AND MCDONALD ISLANDS',
            'nicename'=>'Heard Island and Mcdonald Islands',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>0,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'VA',
            'name'=>'HOLY SEE (VATICAN CITY STATE)',
            'nicename'=>'Holy See (Vatican City State)',
            'iso3'=>'VAT',
            'numcode'=>336,
            'phonecode'=>39,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'HN',
            'name'=>'HONDURAS',
            'nicename'=>'Honduras',
            'iso3'=>'HND',
            'numcode'=>340,
            'phonecode'=>504,
            'lat'=>15.22418170,
            'lon'=>'-87.33053950',
            'value'=>8917890
        ] );



        Country::create( [
            'iso'=>'HK',
            'name'=>'HONG KONG',
            'nicename'=>'Hong Kong',
            'iso3'=>'HKG',
            'numcode'=>344,
            'phonecode'=>852,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'HU',
            'name'=>'HUNGARY',
            'nicename'=>'Hungary',
            'iso3'=>'HUN',
            'numcode'=>348,
            'phonecode'=>36,
            'lat'=>47.50000000,
            'lon'=>19.08333300,
            'value'=>11466650
        ] );



        Country::create( [
            'iso'=>'IS',
            'name'=>'ICELAND',
            'nicename'=>'Iceland',
            'iso3'=>'ISL',
            'numcode'=>352,
            'phonecode'=>354,
            'lat'=>64.80142960,
            'lon'=>'-23.72861280',
            'value'=>366850
        ] );



        Country::create( [
            'iso'=>'IN',
            'name'=>'INDIA',
            'nicename'=>'India',
            'iso3'=>'IND',
            'numcode'=>356,
            'phonecode'=>91,
            'lat'=>18.98780700,
            'lon'=>72.83644700,
            'value'=>1427715754
        ] );



        Country::create( [
            'iso'=>'ID',
            'name'=>'INDONESIA',
            'nicename'=>'Indonesia',
            'iso3'=>'IDN',
            'numcode'=>360,
            'phonecode'=>62,
            'lat'=>'-6.18180000',
            'lon'=>106.82230000,
            'value'=>278674483
        ] );



        Country::create( [
            'iso'=>'IR',
            'name'=>'IRAN, ISLAMIC REPUBLIC OF',
            'nicename'=>'Iran, Islamic Republic of',
            'iso3'=>'IRN',
            'numcode'=>364,
            'phonecode'=>98,
            'lat'=>35.70500000,
            'lon'=>51.42160000,
            'value'=>86018388
        ] );



        Country::create( [
            'iso'=>'IQ',
            'name'=>'IRAQ',
            'nicename'=>'Iraq',
            'iso3'=>'IRQ',
            'numcode'=>368,
            'phonecode'=>964,
            'lat'=>33.34058200,
            'lon'=>44.40087600,
            'value'=>37906252
        ] );



        Country::create( [
            'iso'=>'IE',
            'name'=>'IRELAND',
            'nicename'=>'Ireland',
            'iso3'=>'IRL',
            'numcode'=>372,
            'phonecode'=>353,
            'lat'=>53.33305600,
            'lon'=>'-6.24888900',
            'value'=>5160050
        ] );



        Country::create( [
            'iso'=>'IL',
            'name'=>'ISRAEL',
            'nicename'=>'Israel',
            'iso3'=>'ISR',
            'numcode'=>376,
            'phonecode'=>972,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'IT',
            'name'=>'ITALY',
            'nicename'=>'Italy',
            'iso3'=>'ITA',
            'numcode'=>380,
            'phonecode'=>39,
            'lat'=>41.90000000,
            'lon'=>12.48333300,
            'value'=>69885500
        ] );



        Country::create( [
            'iso'=>'JM',
            'name'=>'JAMAICA',
            'nicename'=>'Jamaica',
            'iso3'=>'JAM',
            'numcode'=>388,
            'phonecode'=>1876,
            'lat'=>18.11982910,
            'lon'=>'-77.83656940',
            'value'=>3115694
        ] );



        Country::create( [
            'iso'=>'JP',
            'name'=>'JAPAN',
            'nicename'=>'Japan',
            'iso3'=>'JPN',
            'numcode'=>392,
            'phonecode'=>81,
            'lat'=>35.68500000,
            'lon'=>139.75138900,
            'value'=>146989868
        ] );



        Country::create( [
            'iso'=>'JO',
            'name'=>'JORDAN',
            'nicename'=>'Jordan',
            'iso3'=>'JOR',
            'numcode'=>400,
            'phonecode'=>962,
            'lat'=>31.95000000,
            'lon'=>35.93333300,
            'value'=>7108149
        ] );



        Country::create( [
            'iso'=>'KZ',
            'name'=>'KAZAKHSTAN',
            'nicename'=>'Kazakhstan',
            'iso3'=>'KAZ',
            'numcode'=>398,
            'phonecode'=>7,
            'lat'=>43.25666700,
            'lon'=>76.92861100,
            'value'=>19042227
        ] );



        Country::create( [
            'iso'=>'KE',
            'name'=>'KENYA',
            'nicename'=>'Kenya',
            'iso3'=>'KEN',
            'numcode'=>404,
            'phonecode'=>254,
            'lat'=>'-1.28333300',
            'lon'=>36.81666700,
            'value'=>47851187
        ] );



        Country::create( [
            'iso'=>'KI',
            'name'=>'KIRIBATI',
            'nicename'=>'Kiribati',
            'iso3'=>'KIR',
            'numcode'=>296,
            'phonecode'=>686,
            'lat'=>'-3.75166620',
            'lon'=>'-177.17472130',
            'value'=>116256
        ] );



        Country::create( [
            'iso'=>'KP',
            'name'=>'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF',
            'nicename'=>'Korea, Democratic People\'s Republic of',
            'iso3'=>'PRK',
            'numcode'=>408,
            'phonecode'=>850,
            'lat'=>38.99305600,
            'lon'=>125.71527800,
            'value'=>28118977
        ] );



        Country::create( [
            'iso'=>'KR',
            'name'=>'KOREA, REPUBLIC OF',
            'nicename'=>'Korea, Republic of',
            'iso3'=>'KOR',
            'numcode'=>410,
            'phonecode'=>82,
            'lat'=>37.59850000,
            'lon'=>126.97830000,
            'value'=>57245849
        ] );



        Country::create( [
            'iso'=>'KW',
            'name'=>'KUWAIT',
            'nicename'=>'Kuwait',
            'iso3'=>'KWT',
            'numcode'=>414,
            'phonecode'=>965,
            'lat'=>29.34164170,
            'lon'=>47.77850230,
            'value'=>3240748
        ] );



        Country::create( [
            'iso'=>'KG',
            'name'=>'KYRGYZSTAN',
            'nicename'=>'Kyrgyzstan',
            'iso3'=>'KGZ',
            'numcode'=>417,
            'phonecode'=>996,
            'lat'=>42.87305600,
            'lon'=>74.60027800,
            'value'=>6333049
        ] );



        Country::create( [
            'iso'=>'LA',
            'name'=>'LAO PEOPLE\'S DEMOCRATIC REPUBLIC',
            'nicename'=>'Lao People\'s Democratic Republic',
            'iso3'=>'LAO',
            'numcode'=>418,
            'phonecode'=>856,
            'lat'=>17.96666700,
            'lon'=>102.60000000,
            'value'=>7231242
        ] );



        Country::create( [
            'iso'=>'LV',
            'name'=>'LATVIA',
            'nicename'=>'Latvia',
            'iso3'=>'LVA',
            'numcode'=>428,
            'phonecode'=>371,
            'lat'=>56.95000000,
            'lon'=>24.10000000,
            'value'=>2553000
        ] );



        Country::create( [
            'iso'=>'LB',
            'name'=>'LEBANON',
            'nicename'=>'Lebanon',
            'iso3'=>'LBN',
            'numcode'=>422,
            'phonecode'=>961,
            'lat'=>33.87194400,
            'lon'=>35.50972200,
            'value'=>4898315
        ] );



        Country::create( [
            'iso'=>'LS',
            'name'=>'LESOTHO',
            'nicename'=>'Lesotho',
            'iso3'=>'LSO',
            'numcode'=>426,
            'phonecode'=>266,
            'lat'=>'-29.61608810',
            'lon'=>27.11235660,
            'value'=>2522919
        ] );



        Country::create( [
            'iso'=>'LR',
            'name'=>'LIBERIA',
            'nicename'=>'Liberia',
            'iso3'=>'LBR',
            'numcode'=>430,
            'phonecode'=>231,
            'lat'=>6.41235650,
            'lon'=>'-10.57369690',
            'value'=>4747857
        ] );



        Country::create( [
            'iso'=>'LY',
            'name'=>'LIBYAN ARAB JAMAHIRIYA',
            'nicename'=>'Libyan Arab Jamahiriya',
            'iso3'=>'LBY',
            'numcode'=>434,
            'phonecode'=>218,
            'lat'=>32.89250000,
            'lon'=>13.18000000,
            'value'=>7386187
        ] );



        Country::create( [
            'iso'=>'LI',
            'name'=>'LIECHTENSTEIN',
            'nicename'=>'Liechtenstein',
            'iso3'=>'LIE',
            'numcode'=>438,
            'phonecode'=>423,
            'lat'=>47.15956640,
            'lon'=>9.41355250,
            'value'=>41749
        ] );



        Country::create( [
            'iso'=>'LT',
            'name'=>'LITHUANIA',
            'nicename'=>'Lithuania',
            'iso3'=>'LTU',
            'numcode'=>440,
            'phonecode'=>370,
            'lat'=>54.68333300,
            'lon'=>25.31666700,
            'value'=>3683449
        ] );



        Country::create( [
            'iso'=>'LU',
            'name'=>'LUXEMBOURG',
            'nicename'=>'Luxembourg',
            'iso3'=>'LUX',
            'numcode'=>442,
            'phonecode'=>352,
            'lat'=>49.61166700,
            'lon'=>6.13000000,
            'value'=>594550
        ] );



        Country::create( [
            'iso'=>'MO',
            'name'=>'MACAO',
            'nicename'=>'Macao',
            'iso3'=>'MAC',
            'numcode'=>446,
            'phonecode'=>853,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'MK',
            'name'=>'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF',
            'nicename'=>'Macedonia, the Former Yugoslav Republic of',
            'iso3'=>'MKD',
            'numcode'=>807,
            'phonecode'=>389,
            'lat'=>41.99645700,
            'lon'=>21.43140700,
            'value'=>2373476
        ] );



        Country::create( [
            'iso'=>'MG',
            'name'=>'MADAGASCAR',
            'nicename'=>'Madagascar',
            'iso3'=>'MDG',
            'numcode'=>450,
            'phonecode'=>261,
            'lat'=>'-18.93333300',
            'lon'=>47.53333300,
            'value'=>24512405
        ] );



        Country::create( [
            'iso'=>'MW',
            'name'=>'MALAWI',
            'nicename'=>'Malawi',
            'iso3'=>'MWI',
            'numcode'=>454,
            'phonecode'=>265,
            'lat'=>'-13.96691900',
            'lon'=>33.78724700,
            'value'=>17688021
        ] );



        Country::create( [
            'iso'=>'MY',
            'name'=>'MALAYSIA',
            'nicename'=>'Malaysia',
            'iso3'=>'MYS',
            'numcode'=>458,
            'phonecode'=>60,
            'lat'=>4.11102950,
            'lon'=>100.54402450,
            'value'=>33188027
        ] );



        Country::create( [
            'iso'=>'MV',
            'name'=>'MALDIVES',
            'nicename'=>'Maldives',
            'iso3'=>'MDV',
            'numcode'=>462,
            'phonecode'=>960,
            'lat'=>3.11520690,
            'lon'=>70.99619420,
            'value'=>368093
        ] );



        Country::create( [
            'iso'=>'ML',
            'name'=>'MALI',
            'nicename'=>'Mali',
            'iso3'=>'MLI',
            'numcode'=>466,
            'phonecode'=>223,
            'lat'=>12.65000000,
            'lon'=>'-8.00000000',
            'value'=>18215468
        ] );



        Country::create( [
            'iso'=>'MT',
            'name'=>'MALTA',
            'nicename'=>'Malta',
            'iso3'=>'MLT',
            'numcode'=>470,
            'phonecode'=>356,
            'lat'=>35.89972200,
            'lon'=>14.51472200,
            'value'=>481849
        ] );



        Country::create( [
            'iso'=>'MH',
            'name'=>'MARSHALL ISLANDS',
            'nicename'=>'Marshall Islands',
            'iso3'=>'MHL',
            'numcode'=>584,
            'phonecode'=>692,
            'lat'=>'-9.33990000',
            'lon'=>161.73244730,
            'value'=>63038
        ] );



        Country::create( [
            'iso'=>'MQ',
            'name'=>'MARTINIQUE',
            'nicename'=>'Martinique',
            'iso3'=>'MTQ',
            'numcode'=>474,
            'phonecode'=>596,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'MR',
            'name'=>'MAURITANIA',
            'nicename'=>'Mauritania',
            'iso3'=>'MRT',
            'numcode'=>478,
            'phonecode'=>222,
            'lat'=>20.96984960,
            'lon'=>'-15.44548590',
            'value'=>4072770
        ] );



        Country::create( [
            'iso'=>'MU',
            'name'=>'MAURITIUS',
            'nicename'=>'Mauritius',
            'iso3'=>'MUS',
            'numcode'=>480,
            'phonecode'=>230,
            'lat'=>'-20.20051530',
            'lon'=>56.55431090,
            'value'=>1478958
        ] );



        Country::create( [
            'iso'=>'YT',
            'name'=>'MAYOTTE',
            'nicename'=>'Mayotte',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>269,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'MX',
            'name'=>'MEXICO',
            'nicename'=>'Mexico',
            'iso3'=>'MEX',
            'numcode'=>484,
            'phonecode'=>52,
            'lat'=>19.43416700,
            'lon'=>'-99.13861100',
            'value'=>132012342
        ] );



        Country::create( [
            'iso'=>'FM',
            'name'=>'MICRONESIA, FEDERATED STATES OF',
            'nicename'=>'Micronesia, Federated States of',
            'iso3'=>'FSM',
            'numcode'=>583,
            'phonecode'=>691,
            'lat'=>5.17299380,
            'lon'=>141.18018890,
            'value'=>128273
        ] );



        Country::create( [
            'iso'=>'MD',
            'name'=>'MOLDOVA, REPUBLIC OF',
            'nicename'=>'Moldova, Republic of',
            'iso3'=>'MDA',
            'numcode'=>498,
            'phonecode'=>373,
            'lat'=>47.00555600,
            'lon'=>28.85750000,
            'value'=>4092849
        ] );



        Country::create( [
            'iso'=>'MC',
            'name'=>'MONACO',
            'nicename'=>'Monaco',
            'iso3'=>'MCO',
            'numcode'=>492,
            'phonecode'=>377,
            'lat'=>43.73788110,
            'lon'=>7.40834290,
            'value'=>40741
        ] );



        Country::create( [
            'iso'=>'MN',
            'name'=>'MONGOLIA',
            'nicename'=>'Mongolia',
            'iso3'=>'MNG',
            'numcode'=>496,
            'phonecode'=>976,
            'lat'=>47.91666700,
            'lon'=>106.91666700,
            'value'=>3220131
        ] );



        Country::create( [
            'iso'=>'MS',
            'name'=>'MONTSERRAT',
            'nicename'=>'Montserrat',
            'iso3'=>'MSR',
            'numcode'=>500,
            'phonecode'=>1664,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'MA',
            'name'=>'MOROCCO',
            'nicename'=>'Morocco',
            'iso3'=>'MAR',
            'numcode'=>504,
            'phonecode'=>212,
            'lat'=>31.82191450,
            'lon'=>'-8.07644970',
            'value'=>37113920
        ] );



        Country::create( [
            'iso'=>'MZ',
            'name'=>'MOZAMBIQUE',
            'nicename'=>'Mozambique',
            'iso3'=>'MOZ',
            'numcode'=>508,
            'phonecode'=>258,
            'lat'=>'-25.96527800',
            'lon'=>32.58916700,
            'value'=>27519164
        ] );



        Country::create( [
            'iso'=>'MM',
            'name'=>'MYANMAR',
            'nicename'=>'Myanmar',
            'iso3'=>'MMR',
            'numcode'=>104,
            'phonecode'=>95,
            'lat'=>18.80193180,
            'lon'=>87.64320110,
            'value'=>55587277
        ] );



        Country::create( [
            'iso'=>'NA',
            'name'=>'NAMIBIA',
            'nicename'=>'Namibia',
            'iso3'=>'NAM',
            'numcode'=>516,
            'phonecode'=>264,
            'lat'=>'-22.57000000',
            'lon'=>17.08361100,
            'value'=>2672604
        ] );



        Country::create( [
            'iso'=>'NR',
            'name'=>'NAURU',
            'nicename'=>'Nauru',
            'iso3'=>'NRU',
            'numcode'=>520,
            'phonecode'=>674,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'NP',
            'name'=>'NEPAL',
            'nicename'=>'Nepal',
            'iso3'=>'NPL',
            'numcode'=>524,
            'phonecode'=>977,
            'lat'=>28.38384140,
            'lon'=>81.88656400,
            'value'=>35058667
        ] );



        Country::create( [
            'iso'=>'NL',
            'name'=>'NETHERLANDS',
            'nicename'=>'Netherlands',
            'iso3'=>'NLD',
            'numcode'=>528,
            'phonecode'=>31,
            'lat'=>52.08333300,
            'lon'=>4.30000000,
            'value'=>19200400
        ] );



        Country::create( [
            'iso'=>'AN',
            'name'=>'NETHERLANDS ANTILLES',
            'nicename'=>'Netherlands Antilles',
            'iso3'=>'ANT',
            'numcode'=>530,
            'phonecode'=>599,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'NC',
            'name'=>'NEW CALEDONIA',
            'nicename'=>'New Caledonia',
            'iso3'=>'NCL',
            'numcode'=>540,
            'phonecode'=>687,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'NZ',
            'name'=>'NEW ZEALAND',
            'nicename'=>'New Zealand',
            'iso3'=>'NZL',
            'numcode'=>554,
            'phonecode'=>64,
            'lat'=>'-36.86666700',
            'lon'=>174.76666700,
            'value'=>5065980
        ] );



        Country::create( [
            'iso'=>'NI',
            'name'=>'NICARAGUA',
            'nicename'=>'Nicaragua',
            'iso3'=>'NIC',
            'numcode'=>558,
            'phonecode'=>505,
            'lat'=>12.86919280,
            'lon'=>'-86.13895630',
            'value'=>6750337
        ] );



        Country::create( [
            'iso'=>'NE',
            'name'=>'NIGER',
            'nicename'=>'Niger',
            'iso3'=>'NER',
            'numcode'=>562,
            'phonecode'=>227,
            'lat'=>13.51666700,
            'lon'=>2.11666700,
            'value'=>18479343
        ] );



        Country::create( [
            'iso'=>'NG',
            'name'=>'NIGERIA',
            'nicename'=>'Nigeria',
            'iso3'=>'NGA',
            'numcode'=>566,
            'phonecode'=>234,
            'lat'=>6.45406600,
            'lon'=>3.39467300,
            'value'=>219614343
        ] );



        Country::create( [
            'iso'=>'NU',
            'name'=>'NIUE',
            'nicename'=>'Niue',
            'iso3'=>'NIU',
            'numcode'=>570,
            'phonecode'=>683,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'NF',
            'name'=>'NORFOLK ISLAND',
            'nicename'=>'Norfolk Island',
            'iso3'=>'NFK',
            'numcode'=>574,
            'phonecode'=>672,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'MP',
            'name'=>'NORTHERN MARIANA ISLANDS',
            'nicename'=>'Northern Mariana Islands',
            'iso3'=>'MNP',
            'numcode'=>580,
            'phonecode'=>1670,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'NO',
            'name'=>'NORWAY',
            'nicename'=>'Norway',
            'iso3'=>'NOR',
            'numcode'=>578,
            'phonecode'=>47,
            'lat'=>59.91666700,
            'lon'=>10.75000000,
            'value'=>5694800
        ] );



        Country::create( [
            'iso'=>'OM',
            'name'=>'OMAN',
            'nicename'=>'Oman',
            'iso3'=>'OMN',
            'numcode'=>512,
            'phonecode'=>968,
            'lat'=>21.39691760,
            'lon'=>51.65763600,
            'value'=>3273066
        ] );



        Country::create( [
            'iso'=>'PK',
            'name'=>'PAKISTAN',
            'nicename'=>'Pakistan',
            'iso3'=>'PAK',
            'numcode'=>586,
            'phonecode'=>92,
            'lat'=>24.90560000,
            'lon'=>67.08220000,
            'value'=>203257168
        ] );



        Country::create( [
            'iso'=>'PW',
            'name'=>'PALAU',
            'nicename'=>'Palau',
            'iso3'=>'PLW',
            'numcode'=>585,
            'phonecode'=>680,
            'lat'=>5.44036470,
            'lon'=>130.80004660,
            'value'=>23700
        ] );



        Country::create( [
            'iso'=>'PS',
            'name'=>'PALESTINIAN TERRITORY, OCCUPIED',
            'nicename'=>'Palestinian Territory, Occupied',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>970,
            'lat'=>31.88582630,
            'lon'=>34.33022880,
            'value'=>4622347
        ] );



        Country::create( [
            'iso'=>'PA',
            'name'=>'PANAMA',
            'nicename'=>'Panama',
            'iso3'=>'PAN',
            'numcode'=>591,
            'phonecode'=>507,
            'lat'=>8.96666700,
            'lon'=>'-79.53333300',
            'value'=>4106862
        ] );



        Country::create( [
            'iso'=>'PG',
            'name'=>'PAPUA NEW GUINEA',
            'nicename'=>'Papua New Guinea',
            'iso3'=>'PNG',
            'numcode'=>598,
            'phonecode'=>675,
            'lat'=>'-6.34584600',
            'lon'=>145.90519570,
            'value'=>8065903
        ] );



        Country::create( [
            'iso'=>'PY',
            'name'=>'PARAGUAY',
            'nicename'=>'Paraguay',
            'iso3'=>'PRY',
            'numcode'=>600,
            'phonecode'=>595,
            'lat'=>'-25.29389000',
            'lon'=>'-57.61111100',
            'value'=>7553533
        ] );



        Country::create( [
            'iso'=>'PE',
            'name'=>'PERU',
            'nicename'=>'Peru',
            'iso3'=>'PER',
            'numcode'=>604,
            'phonecode'=>51,
            'lat'=>'-12.05000000',
            'lon'=>'-77.05000000',
            'value'=>33809789
        ] );



        Country::create( [
            'iso'=>'PH',
            'name'=>'PHILIPPINES',
            'nicename'=>'Philippines',
            'iso3'=>'PHL',
            'numcode'=>608,
            'phonecode'=>63,
            'lat'=>14.63330000,
            'lon'=>121.00000000,
            'value'=>109079834
        ] );



        Country::create( [
            'iso'=>'PN',
            'name'=>'PITCAIRN',
            'nicename'=>'Pitcairn',
            'iso3'=>'PCN',
            'numcode'=>612,
            'phonecode'=>0,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'PL',
            'name'=>'POLAND',
            'nicename'=>'Poland',
            'iso3'=>'POL',
            'numcode'=>616,
            'phonecode'=>48,
            'lat'=>50.25841500,
            'lon'=>19.02754500,
            'value'=>43948400
        ] );



        Country::create( [
            'iso'=>'PT',
            'name'=>'PORTUGAL',
            'nicename'=>'Portugal',
            'iso3'=>'PRT',
            'numcode'=>620,
            'phonecode'=>351,
            'lat'=>38.71666700,
            'lon'=>'-9.13333300',
            'value'=>12232549
        ] );



        Country::create( [
            'iso'=>'PR',
            'name'=>'PUERTO RICO',
            'nicename'=>'Puerto Rico',
            'iso3'=>'PRI',
            'numcode'=>630,
            'phonecode'=>1787,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'QA',
            'name'=>'QATAR',
            'nicename'=>'Qatar',
            'iso3'=>'QAT',
            'numcode'=>634,
            'phonecode'=>974,
            'lat'=>25.34429000,
            'lon'=>50.65730370,
            'value'=>2150547
        ] );



        Country::create( [
            'iso'=>'RE',
            'name'=>'REUNION',
            'nicename'=>'Reunion',
            'iso3'=>'REU',
            'numcode'=>638,
            'phonecode'=>262,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'RO',
            'name'=>'ROMANIA',
            'nicename'=>'Romania',
            'iso3'=>'ROM',
            'numcode'=>642,
            'phonecode'=>40,
            'lat'=>44.43333300,
            'lon'=>26.10000000,
            'value'=>24598499
        ] );



        Country::create( [
            'iso'=>'RU',
            'name'=>'RUSSIAN FEDERATION',
            'nicename'=>'Russian Federation',
            'iso3'=>'RUS',
            'numcode'=>643,
            'phonecode'=>70,
            'lat'=>55.75222200,
            'lon'=>37.61555600,
            'value'=>163219500
        ] );



        Country::create( [
            'iso'=>'RW',
            'name'=>'RWANDA',
            'nicename'=>'Rwanda',
            'iso3'=>'RWA',
            'numcode'=>646,
            'phonecode'=>250,
            'lat'=>'-1.94219100',
            'lon'=>29.31997670,
            'value'=>12584392
        ] );



        Country::create( [
            'iso'=>'SH',
            'name'=>'SAINT HELENA',
            'nicename'=>'Saint Helena',
            'iso3'=>'SHN',
            'numcode'=>654,
            'phonecode'=>290,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'KN',
            'name'=>'SAINT KITTS AND NEVIS',
            'nicename'=>'Saint Kitts and Nevis',
            'iso3'=>'KNA',
            'numcode'=>659,
            'phonecode'=>1869,
            'lat'=>17.24987900,
            'lon'=>'-62.83668480',
            'value'=>61008
        ] );



        Country::create( [
            'iso'=>'LC',
            'name'=>'SAINT LUCIA',
            'nicename'=>'Saint Lucia',
            'iso3'=>'LCA',
            'numcode'=>662,
            'phonecode'=>1758,
            'lat'=>13.91314610,
            'lon'=>'-61.11060120',
            'value'=>202399
        ] );



        Country::create( [
            'iso'=>'PM',
            'name'=>'SAINT PIERRE AND MIQUELON',
            'nicename'=>'Saint Pierre and Miquelon',
            'iso3'=>'SPM',
            'numcode'=>666,
            'phonecode'=>508,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'VC',
            'name'=>'SAINT VINCENT AND THE GRENADINES',
            'nicename'=>'Saint Vincent and the Grenadines',
            'iso3'=>'VCT',
            'numcode'=>670,
            'phonecode'=>1784,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'WS',
            'name'=>'SAMOA',
            'nicename'=>'Samoa',
            'iso3'=>'WSM',
            'numcode'=>882,
            'phonecode'=>684,
            'lat'=>'-13.75099780',
            'lon'=>'-172.38312400',
            'value'=>211455
        ] );



        Country::create( [
            'iso'=>'SM',
            'name'=>'SAN MARINO',
            'nicename'=>'San Marino',
            'iso3'=>'SMR',
            'numcode'=>674,
            'phonecode'=>378,
            'lat'=>33.29907910,
            'lon'=>'-88.96529330',
            'value'=>36495
        ] );



        Country::create( [
            'iso'=>'ST',
            'name'=>'SAO TOME AND PRINCIPE',
            'nicename'=>'Sao Tome and Principe',
            'iso3'=>'STP',
            'numcode'=>678,
            'phonecode'=>239,
            'lat'=>0.89997640,
            'lon'=>6.43302450,
            'value'=>193804
        ] );



        Country::create( [
            'iso'=>'SA',
            'name'=>'SAUDI ARABIA',
            'nicename'=>'Saudi Arabia',
            'iso3'=>'SAU',
            'numcode'=>682,
            'phonecode'=>966,
            'lat'=>24.83577890,
            'lon'=>45.25129630,
            'value'=>32294922
        ] );


        Country::create( [
            'iso'=>'SS',
            'name'=>'SOUTH SUDANESE',
            'nicename'=>'South Sudanese',
            'iso3'=>'SSD',
            'numcode'=>682,
            'phonecode'=>211,
            'lat'=>7.8496852,
            'lon'=>25.197542,
            'value'=>12778250
        ] );



        Country::create( [
            'iso'=>'SN',
            'name'=>'SENEGAL',
            'nicename'=>'Senegal',
            'iso3'=>'SEN',
            'numcode'=>686,
            'phonecode'=>221,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'CS',
            'name'=>'SERBIA AND MONTENEGRO',
            'nicename'=>'Serbia and Montenegro',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>381,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'SC',
            'name'=>'SEYCHELLES',
            'nicename'=>'Seychelles',
            'iso3'=>'SYC',
            'numcode'=>690,
            'phonecode'=>248,
            'lat'=>'-7.07955030',
            'lon'=>48.94396130,
            'value'=>98899
        ] );



        Country::create( [
            'iso'=>'SL',
            'name'=>'SIERRA LEONE',
            'nicename'=>'Sierra Leone',
            'iso3'=>'SLE',
            'numcode'=>694,
            'phonecode'=>232,
            'lat'=>8.42341570,
            'lon'=>'-12.95875810',
            'value'=>6897108
        ] );



        Country::create( [
            'iso'=>'SG',
            'name'=>'SINGAPORE',
            'nicename'=>'Singapore',
            'iso3'=>'SGP',
            'numcode'=>702,
            'phonecode'=>65,
            'lat'=>1.31433940,
            'lon'=>103.70416500,
            'value'=>5961255
        ] );



        Country::create( [
            'iso'=>'SK',
            'name'=>'SLOVAKIA',
            'nicename'=>'Slovakia',
            'iso3'=>'SVK',
            'numcode'=>703,
            'phonecode'=>421,
            'lat'=>48.15000000,
            'lon'=>17.11666700,
            'value'=>6255999
        ] );



        Country::create( [
            'iso'=>'SI',
            'name'=>'SLOVENIA',
            'nicename'=>'Slovenia',
            'iso3'=>'SVN',
            'numcode'=>705,
            'phonecode'=>386,
            'lat'=>46.05527800,
            'lon'=>14.51444400,
            'value'=>2359800
        ] );



        Country::create( [
            'iso'=>'SB',
            'name'=>'SOLOMON ISLANDS',
            'nicename'=>'Solomon Islands',
            'iso3'=>'SLB',
            'numcode'=>90,
            'phonecode'=>677,
            'lat'=>'-9.33990000',
            'lon'=>157.07587960,
            'value'=>635107
        ] );



        Country::create( [
            'iso'=>'SO',
            'name'=>'SOMALIA',
            'nicename'=>'Somalia',
            'iso3'=>'SOM',
            'numcode'=>706,
            'phonecode'=>252,
            'lat'=>2.04619300,
            'lon'=>45.33407000,
            'value'=>10990403
        ] );



        Country::create( [
            'iso'=>'ZA',
            'name'=>'SOUTH AFRICA',
            'nicename'=>'South Africa',
            'iso3'=>'ZAF',
            'numcode'=>710,
            'phonecode'=>27,
            'lat'=>'-26.20517100',
            'lon'=>28.04981500,
            'value'=>58174770
        ] );



        Country::create( [
            'iso'=>'GS',
            'name'=>'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS',
            'nicename'=>'South Georgia and the South Sandwich Islands',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>0,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'ES',
            'name'=>'SPAIN',
            'nicename'=>'Spain',
            'iso3'=>'ESP',
            'numcode'=>724,
            'phonecode'=>34,
            'lat'=>40.41275200,
            'lon'=>'-3.70772100',
            'value'=>53170249
        ] );



        Country::create( [
            'iso'=>'LK',
            'name'=>'SRI LANKA',
            'nicename'=>'Sri Lanka',
            'iso3'=>'LKA',
            'numcode'=>144,
            'phonecode'=>94,
            'lat'=>7.85892140,
            'lon'=>79.58498240,
            'value'=>23999350
        ] );



        Country::create( [
            'iso'=>'SD',
            'name'=>'SUDAN',
            'nicename'=>'Sudan',
            'iso3'=>'SDN',
            'numcode'=>736,
            'phonecode'=>249,
            'lat'=>15.58805600,
            'lon'=>32.53416700,
            'value'=>26466142
        ] );



        Country::create( [
            'iso'=>'SR',
            'name'=>'SURINAME',
            'nicename'=>'Suriname',
            'iso3'=>'SUR',
            'numcode'=>740,
            'phonecode'=>597,
            'lat'=>3.98543400,
            'lon'=>'-57.12797410',
            'value'=>608831
        ] );



        Country::create( [
            'iso'=>'SJ',
            'name'=>'SVALBARD AND JAN MAYEN',
            'nicename'=>'Svalbard and Jan Mayen',
            'iso3'=>'SJM',
            'numcode'=>744,
            'phonecode'=>47,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'SZ',
            'name'=>'SWAZILAND',
            'nicename'=>'Swaziland',
            'iso3'=>'SWZ',
            'numcode'=>748,
            'phonecode'=>268,
            'lat'=>'-26.51533690',
            'lon'=>30.90234050,
            'value'=>1227938
        ] );



        Country::create( [
            'iso'=>'SE',
            'name'=>'SWEDEN',
            'nicename'=>'Sweden',
            'iso3'=>'SWE',
            'numcode'=>752,
            'phonecode'=>46,
            'lat'=>59.33333300,
            'lon'=>18.05000000,
            'value'=>10870950
        ] );



        Country::create( [
            'iso'=>'CH',
            'name'=>'SWITZERLAND',
            'nicename'=>'Switzerland',
            'iso3'=>'CHE',
            'numcode'=>756,
            'phonecode'=>41,
            'lat'=>46.19560200,
            'lon'=>6.14811300,
            'value'=>9093050
        ] );



        Country::create( [
            'iso'=>'SY',
            'name'=>'SYRIAN ARAB REPUBLIC',
            'nicename'=>'Syrian Arab Republic',
            'iso3'=>'SYR',
            'numcode'=>760,
            'phonecode'=>963,
            'lat'=>36.20124100,
            'lon'=>37.16117300,
            'value'=>23943357
        ] );



        Country::create( [
            'iso'=>'TW',
            'name'=>'TAIWAN, PROVINCE OF CHINA',
            'nicename'=>'Taiwan, Province of China',
            'iso3'=>'TWN',
            'numcode'=>158,
            'phonecode'=>886,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'TJ',
            'name'=>'TAJIKISTAN',
            'nicename'=>'Tajikistan',
            'iso3'=>'TJK',
            'numcode'=>762,
            'phonecode'=>992,
            'lat'=>38.56000000,
            'lon'=>68.77388900,
            'value'=>8023501
        ] );



        Country::create( [
            'iso'=>'TZ',
            'name'=>'TANZANIA, UNITED REPUBLIC OF',
            'nicename'=>'Tanzania, United Republic of',
            'iso3'=>'TZA',
            'numcode'=>834,
            'phonecode'=>255,
            'lat'=>'-6.80000000',
            'lon'=>39.28333300,
            'value'=>53151258
        ] );



        Country::create( [
            'iso'=>'TH',
            'name'=>'THAILAND',
            'nicename'=>'Thailand',
            'iso3'=>'THA',
            'numcode'=>764,
            'phonecode'=>66,
            'lat'=>13.73057900,
            'lon'=>100.52388400,
            'value'=>79946338
        ] );



        Country::create( [
            'iso'=>'TL',
            'name'=>'TIMOR-LESTE',
            'nicename'=>'Timor-Leste',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>670,
            'lat'=>51.08995310,
            'lon'=>14.93909930,
            'value'=>1352262
        ] );



        Country::create( [
            'iso'=>'TG',
            'name'=>'TOGO',
            'nicename'=>'Togo',
            'iso3'=>'TGO',
            'numcode'=>768,
            'phonecode'=>228,
            'lat'=>8.60934070,
            'lon'=>'-1.41169330',
            'value'=>7078034
        ] );



        Country::create( [
            'iso'=>'TK',
            'name'=>'TOKELAU',
            'nicename'=>'Tokelau',
            'iso3'=>'TKL',
            'numcode'=>772,
            'phonecode'=>690,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'TO',
            'name'=>'TONGA',
            'nicename'=>'Tonga',
            'iso3'=>'TON',
            'numcode'=>776,
            'phonecode'=>676,
            'lat'=>'-18.60250980',
            'lon'=>'-176.84950320',
            'value'=>120185
        ] );



        Country::create( [
            'iso'=>'TT',
            'name'=>'TRINIDAD AND TOBAGO',
            'nicename'=>'Trinidad and Tobago',
            'iso3'=>'TTO',
            'numcode'=>780,
            'phonecode'=>1868,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'TN',
            'name'=>'TUNISIA',
            'nicename'=>'Tunisia',
            'iso3'=>'TUN',
            'numcode'=>788,
            'phonecode'=>216,
            'lat'=>36.80611200,
            'lon'=>10.17107800,
            'value'=>12274869
        ] );



        Country::create( [
            'iso'=>'TR',
            'name'=>'TURKEY',
            'nicename'=>'Turkey',
            'iso3'=>'TUR',
            'numcode'=>792,
            'phonecode'=>90,
            'lat'=>39.93732710,
            'lon'=>33.86630430,
            'value'=>84685535
        ] );



        Country::create( [
            'iso'=>'TM',
            'name'=>'TURKMENISTAN',
            'nicename'=>'Turkmenistan',
            'iso3'=>'TKM',
            'numcode'=>795,
            'phonecode'=>7370,
            'lat'=>38.88630760,
            'lon'=>55.02261600,
            'value'=>5871096
        ] );



        Country::create( [
            'iso'=>'TC',
            'name'=>'TURKS AND CAICOS ISLANDS',
            'nicename'=>'Turks and Caicos Islands',
            'iso3'=>'TCA',
            'numcode'=>796,
            'phonecode'=>1649,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'TV',
            'name'=>'TUVALU',
            'nicename'=>'Tuvalu',
            'iso3'=>'TUV',
            'numcode'=>798,
            'phonecode'=>688,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'UG',
            'name'=>'UGANDA',
            'nicename'=>'Uganda',
            'iso3'=>'UGA',
            'numcode'=>800,
            'phonecode'=>256,
            'lat'=>0.33886300,
            'lon'=>32.55406400,
            'value'=>39685585
        ] );



        Country::create( [
            'iso'=>'UA',
            'name'=>'UKRAINE',
            'nicename'=>'Ukraine',
            'iso3'=>'UKR',
            'numcode'=>804,
            'phonecode'=>380,
            'lat'=>50.43333300,
            'lon'=>30.51666700,
            'value'=>52562014
        ] );



        Country::create( [
            'iso'=>'AE',
            'name'=>'UNITED ARAB EMIRATES',
            'nicename'=>'United Arab Emirates',
            'iso3'=>'ARE',
            'numcode'=>784,
            'phonecode'=>971,
            'lat'=>24.33749730,
            'lon'=>51.71183150,
            'value'=>9074562
        ] );



        Country::create( [
            'iso'=>'GB',
            'name'=>'UNITED KINGDOM',
            'nicename'=>'United Kingdom',
            'iso3'=>'GBR',
            'numcode'=>826,
            'phonecode'=>44,
            'lat'=>51.51424800,
            'lon'=>'-0.09314500',
            'value'=>72037150
        ] );



        Country::create( [
            'iso'=>'US',
            'name'=>'UNITED STATES',
            'nicename'=>'United States',
            'iso3'=>'USA',
            'numcode'=>840,
            'phonecode'=>1,
            'lat'=>36.25011530,
            'lon'=>'-113.69779320',
            'value'=>358330704
        ] );



        Country::create( [
            'iso'=>'UM',
            'name'=>'UNITED STATES MINOR OUTLYING ISLANDS',
            'nicename'=>'United States Minor Outlying Islands',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>1,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'UY',
            'name'=>'URUGUAY',
            'nicename'=>'Uruguay',
            'iso3'=>'URY',
            'numcode'=>858,
            'phonecode'=>598,
            'lat'=>'-34.85805600',
            'lon'=>'-56.17083300',
            'value'=>3873884
        ] );



        Country::create( [
            'iso'=>'UZ',
            'name'=>'UZBEKISTAN',
            'nicename'=>'Uzbekistan',
            'iso3'=>'UZB',
            'numcode'=>860,
            'phonecode'=>998,
            'lat'=>41.31666700,
            'lon'=>69.25000000,
            'value'=>33742380
        ] );



        Country::create( [
            'iso'=>'VU',
            'name'=>'VANUATU',
            'nicename'=>'Vanuatu',
            'iso3'=>'VUT',
            'numcode'=>548,
            'phonecode'=>678,
            'lat'=>'-16.65341920',
            'lon'=>166.03647880,
            'value'=>282461
        ] );



        Country::create( [
            'iso'=>'VE',
            'name'=>'VENEZUELA',
            'nicename'=>'Venezuela',
            'iso3'=>'VEN',
            'numcode'=>862,
            'phonecode'=>58,
            'lat'=>10.50333400,
            'lon'=>'-66.93823800',
            'value'=>33669700
        ] );



        Country::create( [
            'iso'=>'VN',
            'name'=>'VIET NAM',
            'nicename'=>'Viet Nam',
            'iso3'=>'VNM',
            'numcode'=>704,
            'phonecode'=>84,
            'lat'=>10.77657700,
            'lon'=>106.70085000,
            'value'=>101015999
        ] );



        Country::create( [
            'iso'=>'VG',
            'name'=>'VIRGIN ISLANDS, BRITISH',
            'nicename'=>'Virgin Islands, British',
            'iso3'=>'VGB',
            'numcode'=>92,
            'phonecode'=>1284,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'VI',
            'name'=>'VIRGIN ISLANDS, U.S.',
            'nicename'=>'Virgin Islands, U.s.',
            'iso3'=>'VIR',
            'numcode'=>850,
            'phonecode'=>1340,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'WF',
            'name'=>'WALLIS AND FUTUNA',
            'nicename'=>'Wallis and Futuna',
            'iso3'=>'WLF',
            'numcode'=>876,
            'phonecode'=>681,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'EH',
            'name'=>'WESTERN SAHARA',
            'nicename'=>'Western Sahara',
            'iso3'=>'ESH',
            'numcode'=>732,
            'phonecode'=>212,
            'lat'=>NULL,
            'lon'=>NULL,
            'value'=>NULL
        ] );



        Country::create( [
            'iso'=>'YE',
            'name'=>'YEMEN',
            'nicename'=>'Yemen',
            'iso3'=>'YEM',
            'numcode'=>887,
            'phonecode'=>967,
            'lat'=>15.35472200,
            'lon'=>44.20666700,
            'value'=>28519861
        ] );



        Country::create( [
            'iso'=>'ZM',
            'name'=>'ZAMBIA',
            'nicename'=>'Zambia',
            'iso3'=>'ZMB',
            'numcode'=>894,
            'phonecode'=>260,
            'lat'=>'-15.41666700',
            'lon'=>28.28333300,
            'value'=>15496202
        ] );



        Country::create( [
            'iso'=>'ZW',
            'name'=>'ZIMBABWE',
            'nicename'=>'Zimbabwe',
            'iso3'=>'ZWE',
            'numcode'=>716,
            'phonecode'=>263,
            'lat'=>26.90902570,
            'lon'=>'-18.99688300',
            'value'=>14667534
        ] );
    }
}
