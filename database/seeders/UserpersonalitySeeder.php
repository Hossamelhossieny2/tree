<?php

namespace Database\Seeders;

use App\Models\UserPersonality;
use Illuminate\Database\Seeder;

class UserpersonalitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserPersonality::create([
            'user_id'=>1,
            'last_ques'=>1
        ]);
    }
}
