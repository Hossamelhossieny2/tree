<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\FaqsHelp;
class FaqsHelpSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'مؤسس تقييم عجلة الحياة :',
            'desc'=>'جاءت الفكرة الأصلية وراء عجلة الحياة المتوازنه من رائد الصناعة بول ماير في الستينيات لمساعدة الناس على تحقيق أهدافهم . بينما تحتوي فكرة الأختبار اليوم على العديد من الأشكال والأسماء المختلفة للعديد من رواد المجال ولكن علي نفس المنهج مع إختلاف الأسلوب .',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Founder of Wheel of Life Evaluation:',
            'desc'=>'The original idea behind the Wheel of Balanced Life came from industry pioneer Paul Meyer in the 1960s to help people achieve their goals. While the idea of the test today contains many different forms and names for many pioneers in the field, but according to the same method, with a different method.',
            'lang'=>'en'
        ] );

         FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Fondateur de Wheel of Life Evaluation :',
            'desc'=>"L'idée originale derrière la roue de la vie équilibrée est venue du pionnier de l'industrie Paul Meyer dans les années 1960 pour aider les gens à atteindre leurs objectifs. Alors que l'idée du test contient aujourd'hui de nombreuses formes et noms différents pour de nombreux pionniers dans le domaine, mais selon la même méthode, avec une méthode différente.",
            'lang'=>'fr'
        ] );

          FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Yaşam çarkı değerlendirmesinin kurucusu:',
            'desc'=>"Dengeli Yaşam Çarkı'nın arkasındaki orijinal fikir, 1960'larda insanların hedeflerine ulaşmalarına yardımcı olmak için endüstri öncüsü Paul Meyer'den geldi. Bugün test fikri, alanında birçok öncü için birçok farklı form ve isim içerirken, ancak aynı yönteme göre, farklı bir yöntemle.",
            'lang'=>'tr'
        ] );

           FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Основатель оценки колеса жизни:',
            'desc'=>'Первоначальная идея Колеса сбалансированной жизни пришла от пионера отрасли Пола Мейера в 1960-х годах, чтобы помочь людям достичь своих целей. В то время как идея теста сегодня содержит много разных форм и имен для многих пионеров в этой области, но в соответствии с тем же методом, с другим методом.',
            'lang'=>'ru'
        ] );

            FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Gründer der Lebensradbewertung:',
            'desc'=>'Die ursprüngliche Idee hinter dem Wheel of Balanced Life stammt von dem Branchenpionier Paul Meyer in den 1960er Jahren, um Menschen zu helfen, ihre Ziele zu erreichen. Zwar enthält die Testidee heute viele verschiedene Formen und Namen für viele Pioniere auf diesem Gebiet, aber nach der gleichen Methode, mit einer anderen Methode.',
            'lang'=>'de'
        ] );

             FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Fundador de la evaluación de la rueda de la vida:',
            'desc'=>'La idea original detrás de la Rueda de la Vida Equilibrada provino del pionero de la industria Paul Meyer en la década de 1960 para ayudar a las personas a lograr sus objetivos. Si bien la idea de la prueba hoy contiene muchas formas y nombres diferentes para muchos pioneros en el campo, pero de acuerdo con el mismo método, con un método diferente.',
            'lang'=>'es'
        ] );

              FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'生命之轮创始人评测',
            'desc'=>'平衡生活之轮背后的最初想法来自 1960 年代的行业先驱 Paul Meyer，旨在帮助人们实现目标。虽然今天的测试思想包含了许多不同的形式和名称，为该领域的许多先驱，但按照同样的方法，用不同的方法。',
            'lang'=>'cn'
        ] );


        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'فكرة المقياس:',
            'desc'=>'صمم هذا التمرين لمساعدتك على التوازن في حياتك والتأكد من التجديد المستمر، ونعتبره من أهم التمارين حيث سيحدد لك جوانب القوة والضعف في حياتك، و الأمور التي تحتاج إلى تعديلها للوصول إلى حياة متوازنة فعالة.وسيساهم هذا التمرين في تطوير خطة حياتك بإضافة الأمور التي تحتاج إلى علاج لخطة الحياة، وذلك في المجالات المختلفة التي يشملها هذا المقياس.',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Scale idea:',
            'desc'=>'This exercise is designed to help you balance in your life and ensure continuous renewal, and we consider it one of the most important exercises as it will identify the strengths and weaknesses in your life, and the things that need to be modified to reach an effective balanced life. This exercise will contribute to developing your life plan by adding the things that you need to Life plan treatment, in the different areas covered by this scale.',
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>"Idée d'échelle :",
            'desc'=>"Cet exercice est conçu pour vous aider à équilibrer votre vie et à assurer un renouvellement continu, et nous le considérons comme l'un des exercices les plus importants car il identifiera les forces et les faiblesses de votre vie, et les choses qui doivent être modifiées pour atteindre un vie équilibrée. Cet exercice contribuera à l'élaboration de votre plan de vie en ajoutant les éléments nécessaires au traitement du plan de vie, dans les différents domaines couverts par cette échelle.",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Ölçek fikri:',
            'desc'=>'Hayatınızda denge kurmanıza yardımcı olmak ve sürekli yenilenmeyi sağlamak için tasarlanmış bu egzersizi, hayatınızdaki güçlü ve zayıf yönlerinizi belirleyeceği ve etkili bir sonuca ulaşmak için değiştirilmesi gereken şeyleri belirleyeceği için en önemli egzersizlerden biri olarak görüyoruz. dengeli yaşam. Bu egzersiz, bu ölçeğin kapsadığı farklı alanlarda Yaşam planı tedavisine ihtiyacınız olan şeyleri ekleyerek yaşam planınızı geliştirmenize katkıda bulunacaktır.',
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Идея шкалы:',
            'desc'=>'Это упражнение разработано, чтобы помочь вам сбалансировать свою жизнь и обеспечить постоянное обновление, и мы считаем его одним из самых важных упражнений, поскольку оно выявит сильные и слабые стороны вашей жизни, а также то, что необходимо изменить для достижения эффективного сбалансированная жизнь. Это упражнение внесет свой вклад в разработку вашего жизненного плана, добавив то, что вам нужно, в лечение Life Plan в различных областях, охватываемых этой шкалой.',
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Maßstab Idee:',
            'desc'=>'Diese Übung wurde entwickelt, um Ihnen zu helfen, Ihr Leben auszugleichen und eine kontinuierliche Erneuerung zu gewährleisten. Wir betrachten sie als eine der wichtigsten Übungen, da sie die Stärken und Schwächen in Ihrem Leben identifiziert und die Dinge, die geändert werden müssen, um eine effektive Wirkung zu erzielen ausgeglichenes Leben. Diese Übung wird dazu beitragen, Ihren Lebensplan zu entwickeln, indem Sie die Dinge, die Sie zur Lebensplan-Behandlung benötigen, in den verschiedenen Bereichen, die von dieser Skala abgedeckt werden, hinzufügen.',
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Idea de escala:',
            'desc'=>'Este ejercicio está diseñado para ayudarlo a equilibrar su vida y garantizar una renovación continua, y lo consideramos uno de los ejercicios más importantes, ya que identificará las fortalezas y debilidades de su vida, y las cosas que deben modificarse para alcanzar una eficacia. vida equilibrada. Este ejercicio contribuirá a desarrollar su plan de vida agregando las cosas que necesita al tratamiento del plan de vida, en las diferentes áreas cubiertas por esta escala.',
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'规模理念',
            'desc'=>'此练习旨在帮助您平衡生活并确保持续更新，我们认为这是最重要的练习之一，因为它将确定您生活中的优势和劣势，以及需要修改以达到有效平衡的生活。此练习将在此量表涵盖的不同领域中添加您需要的生活计划治疗内容，从而有助于制定您的生活计划。',
            'lang'=>'cn'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'دقة المقياس:',
            'desc'=>'هذا المقياس يعتبر خطة منهجية علمية تم تطبيقها على الآلاف وتعديلها عدة مرات، لكي تكون الخطة واقعية وعملية ومبنية على الأولويات والمواهب والقيم والرغبات التي تختلف من إنسان لآخر.',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Scale accuracy:',
            'desc'=>'This scale is a scientific methodological plan that has been applied to thousands and modified several times, so that the plan is realistic, practical and based on priorities, talents, values and desires that differ from one person to another.',
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>"Précision de l'échelle :",
            'desc'=>"Cette échelle est un plan méthodologique scientifique qui a été appliqué à des milliers et modifié plusieurs fois, afin que le plan soit réaliste, pratique et basé sur des priorités, des talents, des valeurs et des désirs qui diffèrent d'une personne à l'autre.",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Ölçek doğruluğu:',
            'desc'=>'Bu ölçek, planın gerçekçi, pratik ve bir kişiden diğerine farklılık gösteren önceliklere, yeteneklere, değerlere ve arzulara dayalı olması için binlerce kişiye uygulanmış ve birkaç kez değiştirilmiş bilimsel bir metodolojik plandır.',
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Точность шкалы:',
            'desc'=>'Эта шкала представляет собой научно-методологический план, который применялся к тысячам людей и несколько раз изменялся, чтобы он был реалистичным, практичным и основывался на приоритетах, талантах, ценностях и желаниях, которые у разных людей различаются.',
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Waagengenauigkeit:',
            'desc'=>'Diese Skala ist ein wissenschaftlicher methodischer Plan, der auf Tausende angewendet und mehrmals modifiziert wurde, sodass der Plan realistisch und praktisch ist und auf Prioritäten, Talenten, Werten und Wünschen basiert, die sich von Person zu Person unterscheiden.',
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Precisión de escala:',
            'desc'=>'Esta escala es un plan científico metodológico que ha sido aplicado a miles y modificado varias veces, para que el plan sea realista, práctico y basado en prioridades, talentos, valores y deseos que difieren de una persona a otra.',
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'秤精度：',
            'desc'=>'该量表是一种科学的方法论计划，经过数千次应用并多次修改，因此该计划是现实的、实用的，并基于因人而异的优先事项、才能、价值观和愿望。',
            'lang'=>'cn'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'لمن يقدم المقياس؟',
            'desc'=>'لكل من يرغب في التوازن في حياته و التجديد المستمر فيها ، ولكل من أراد أن تكون لحياته أهداف يسعى لتحقيقها وتطويرها .',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Who provides the scale?',
            'desc'=>'For everyone who wants balance in his life and continuous renewal in it, and for everyone who wants his life to have goals that he seeks to achieve and develop.',
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>"Qui fournit l'échelle?",
            'desc'=>"Pour tous ceux qui veulent un équilibre dans leur vie et un renouvellement continu, et pour tous ceux qui veulent que leur vie ait des objectifs qu'ils cherchent à atteindre et à développer.",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Ölçeği kim sağlıyor?',
            'desc'=>'Hayatında denge ve sürekli yenilenme isteyen herkes için ve hayatında ulaşmak ve geliştirmek istediği hedeflere sahip olmak isteyen herkes için.',
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Кто предоставляет шкалу?',
            'desc'=>'Для всех, кто хочет баланса в своей жизни и постоянного обновления в ней, и для всех, кто хочет, чтобы в его жизни были цели, которых он стремится достичь и развить.',
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Wer stellt die Waage zur Verfügung?',
            'desc'=>'Für jeden, der ein Gleichgewicht in seinem Leben und eine kontinuierliche Erneuerung möchte, und für jeden, der möchte, dass sein Leben Ziele hat, die er erreichen und entwickeln möchte.',
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'¿Quién proporciona la escala?',
            'desc'=>'Para todo aquel que quiere equilibrio en su vida y una renovación continua en ella, y para todo aquel que quiere que su vida tenga metas que busca alcanzar y desarrollar.',
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'谁提供秤？',
            'desc'=>'对于每个希望在生活中保持平衡并在其中不断更新的人，以及每个希望他的生活有他寻求实现和发展的目标的人。',
            'lang'=>'cn'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'كيف نبدأ الأختبار ؟',
            'desc'=>'إبدأ أولا بالإجابة الصادقة عن كل جانب (فالتمرين لك أنت) وبعد إنهاء الإجابة عن الأسئلة العشر إضغط الزر الموجود أسفل الأسئلة لتنتقل إلي الجانب الآخر ، ركز في إجابتك جيدا وإن وجدت سؤالا لا ينطبق عليك فتخيله بالقياس بشيء آخر .',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'How do we start the test?',
            'desc'=>'First start with the honest answer for each side (the exercise is for you) and after you finish answering the ten questions, press the button below the questions to move to the other side, focus on your answer well, and if you find a question that does not apply to you, imagine it by analogy with something else.',
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Comment commençons-nous le test?',
            'desc'=>"Commencez d'abord par la réponse honnête de chaque côté (l'exercice est pour vous) et après avoir fini de répondre aux dix questions, appuyez sur le bouton sous les questions pour passer de l'autre côté, concentrez-vous bien sur votre réponse, et si vous trouvez une question cela ne s'applique pas à vous, imaginez-le par analogie avec autre chose.",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Teste nasıl başlayacağız?',
            'desc'=>'Önce her iki taraf için dürüst cevapla başlayın (egzersiz sizin için) ve on soruyu cevaplamayı bitirdikten sonra diğer tarafa geçmek için soruların altındaki düğmeye basın, cevabınıza iyi odaklanın ve bir soru bulursanız bu sizin için geçerli değil, başka bir şeye benzeterek hayal edin.',
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Как начать тест?',
            'desc'=>'Сначала начните с честного ответа для каждой стороны (упражнение для вас), а после того, как вы закончите отвечать на десять вопросов, нажмите кнопку под вопросами, чтобы перейти на другую сторону, хорошо сосредоточьтесь на своем ответе и, если вы найдете вопрос что к вам не относится, представьте это по аналогии с чем-то другим.',
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Wie starten wir den Test?',
            'desc'=>'Beginnen Sie zuerst mit der ehrlichen Antwort für jede Seite (die Übung ist für Sie) und nachdem Sie die zehn Fragen beantwortet haben, drücken Sie die Schaltfläche unter den Fragen, um zur anderen Seite zu gelangen, konzentrieren Sie sich gut auf Ihre Antwort und wenn Sie eine Frage finden das trifft nicht auf Sie zu, stellen Sie sich das analog zu etwas anderem vor.',
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'¿Cómo comenzamos la prueba?',
            'desc'=>'Primero comience con la respuesta honesta para cada lado (el ejercicio es para usted) y después de que termine de responder las diez preguntas, presione el botón debajo de las preguntas para pasar al otro lado, concéntrese bien en su respuesta, y si encuentra una pregunta eso no se aplica a usted, imagínelo por analogía con otra cosa.',
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'我们如何开始测试？',
            'desc'=>'先从每一方的诚实回答开始（这个练习是给你的），当你回答完十个问题后，按问题下方的按钮移动到另一边，专注于你的答案，如果你发现了一个问题这不适用于你，用别的东西类比想象一下。',
            'lang'=>'cn'
        ] );

         FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'تحليل نتيجة الإختبار',
            'desc'=>'ستحصل بعد نهاية الإختبار علي نسبة مئوية لكل جانب ونتيجة إما أنت متميز في هذا الجانب أو يحظي ببعض الإهتمام ويمكنك تحسينة أو لابد من البدء فورا بتحسينه .',
            'lang'=>'ar'
        ] );

         FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Test result analysis',
            'desc'=>'After the end of the test, you will get a percentage for each aspect and a result. Either you are distinguished in this aspect or receive some attention and you can improve it or you must start improving it immediately.',
            'lang'=>'en'
        ] );

         FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Analyse des résultats des tests',
            'desc'=>"Après la fin du test, vous obtiendrez un pourcentage pour chaque aspect et un résultat. Soit vous vous distinguez dans cet aspect, soit vous recevez de l'attention et vous pouvez l'améliorer, soit vous devez commencer à l'améliorer immédiatement.",
            'lang'=>'fr'
        ] );

         FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Test sonucu analizi',
            'desc'=>'Testin bitiminden sonra, her bir özellik ve sonuç için bir yüzde alacaksınız. Ya bu konuda fark yaratıyorsunuz ya da biraz dikkat çekiyorsunuz ve bunu geliştirebilirsiniz ya da hemen geliştirmeye başlamalısınız.',
            'lang'=>'tr'
        ] );

          FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Анализ результатов тестирования',
            'desc'=>'По окончании теста вы получите процент по каждому аспекту и результат. Либо вы выделяетесь в этом аспекте, либо получаете некоторое внимание, и вы можете его улучшить, либо вы должны немедленно приступить к его улучшению.',
            'lang'=>'ru'
        ] );

           FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Testergebnisanalyse',
            'desc'=>'Nach Abschluss des Tests erhalten Sie für jeden Aspekt einen Prozentsatz und ein Ergebnis. Entweder werden Sie in diesem Aspekt ausgezeichnet oder erhalten etwas Aufmerksamkeit und können es verbessern oder Sie müssen sofort anfangen, es zu verbessern.',
            'lang'=>'de'
        ] );

            FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Análisis del resultado de la prueba',
            'desc'=>'Una vez finalizada la prueba, obtendrá un porcentaje para cada aspecto y un resultado. O te distingues en este aspecto o recibes alguna atención y puedes mejorarlo o debes empezar a mejorarlo de inmediato.',
            'lang'=>'es'
        ] );

             FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'测试结果分析',
            'desc'=>'测试结束后，您将获得每个方面的百分比和结果。要么你在这方面出众，要么受到一些关注，你可以改进它，或者你必须立即开始改进它。',
            'lang'=>'cn'
        ] );


         FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'وضع خطط ومهام لتحسين عجلة حياتك',
            'desc'=>'يمكنك سحب أي من النقاط الظاهره أسفل النتيجة ووضعها في أي ربع من أرباع السنه لتقوم بإنجازه فيها',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Make plans and tasks to improve the wheel of your life',
            'desc'=>'You can drag any of the points shown below the score and put them in any quarter of the year to accomplish in it',
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Faites des plans et des tâches pour améliorer la roue de votre vie',
            'desc'=>"Vous pouvez faire glisser n'importe lequel des points affichés sous le score et les mettre dans n'importe quel trimestre de l'année pour y accomplir",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Hayatınızın çarkını geliştirmek için planlar ve görevler yapın',
            'desc'=>'Skorun altında gösterilen puanlardan herhangi birini sürükleyebilir ve başarmak için yılın herhangi bir çeyreğine koyabilirsiniz.',
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Составляйте планы и задачи, чтобы улучшить колесо своей жизни',
            'desc'=>'Вы можете перетащить любую из точек, показанных под оценкой, и поместить их в любой квартал года, чтобы добиться в ней результатов.',
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Machen Sie Pläne und Aufgaben, um das Rad Ihres Lebens zu verbessern',
            'desc'=>'Sie können jeden der Punkte, die unter der Punktzahl angezeigt werden, ziehen und sie in ein beliebiges Quartal des Jahres einfügen, um darin zu erreichen',
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'Haz planes y tareas para mejorar la rueda de tu vida',
            'desc'=>'Puede arrastrar cualquiera de los puntos que se muestran debajo de la puntuación y ponerlos en cualquier trimestre del año para lograrlo.',
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'wheel',
            'topic_id'=>'1',
            'title'=>'制定计划和任务以改善您的生活',
            'desc'=>'您可以拖动分数下方显示的任何点并将它们放在一年中的任何一个季度来完成',
            'lang'=>'cn'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'ماذا تقدم لك هذه الإحصائيات :',
            'desc'=>'إحصائيات العائلة هلي لوحة التحكم الخاصة بك والتي تقدم لك ملخص عن معظم الإحصائيات الموجودة في حسابك والمرتبطة بك أو بعائلتك',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'What do these stats give you :',
            'desc'=>'Family Statistics is your dashboard that provides you with a summary of most of the statistics in your account and associated with you or your family.',
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Que vous donnent ces statistiques :',
            'desc'=>'Family Statistics est votre tableau de bord qui vous fournit un résumé de la plupart des statistiques de votre compte et associées à vous ou à votre famille.',
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Bu istatistikler size ne veriyor:',
            'desc'=>'Aile İstatistikleri, hesabınızdaki ve sizinle veya ailenizle ilişkili istatistiklerin çoğunun bir özetini sağlayan gösterge tablonuzdur.',
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Что дает вам эта статистика:',
            'desc'=>'Семейная статистика - это ваша панель управления, которая предоставляет вам сводку большинства статистических данных в вашей учетной записи, связанных с вами или вашей семьей.',
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Was geben Ihnen diese Statistiken:',
            'desc'=>'Familienstatistik ist Ihr Dashboard, das Ihnen eine Zusammenfassung der meisten Statistiken in Ihrem Konto bietet und mit Ihnen oder Ihrer Familie verknüpft ist.',
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'¿Qué te dan estas estadísticas?',
            'desc'=>'Family Statistics es su panel de control que le proporciona un resumen de la mayoría de las estadísticas en su cuenta y asociadas con usted o su familia.',
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'这些统计数据给你什么：',
            'desc'=>'家庭统计信息是您的仪表板，可为您提供帐户中大多数与您或您的家人相关的统计信息的摘要。',
            'lang'=>'cn'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'إحصائيات العائلة حول العالم :',
            'desc'=>'نقدم لك تقرير عن عدد الأعضاء ،وعدد الصور التي قام أي من أفراد العائلة بإضافتها ،وعدد المنشورات العائلة التي نشرت داخل العائلة وأخيرا في هذا القسم عدد أفراد العائلة طبقا لموقعهم علي خريطة العالم .',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Family stats around the world :',
            'desc'=>'We provide you with a report on the number of members, the number of photos added by any of the family members, the number of family publications published within the family, and finally in this section the number of family members according to their location on the world map.',
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Statistiques familiales dans le monde :',
            'desc'=>"Nous vous fournissons un rapport sur le nombre de membres, le nombre de photos ajoutées par l'un des membres de la famille, le nombre de publications familiales publiées au sein de la famille, et enfin dans cette section le nombre de membres de la famille selon leur localisation sur le carte du monde.",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Dünyadaki aile istatistikleri:',
            'desc'=>'Üye sayısı, aile üyelerinden herhangi biri tarafından eklenen fotoğraf sayısı, aile içinde yayınlanan aile yayınlarının sayısı ve son olarak bu bölümde aile üyelerinin sayısı hakkında bir rapor sunuyoruz. Dünya haritası.',
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Семейная статистика по всему миру:',
            'desc'=>'Мы предоставляем вам отчет о количестве членов, количестве фотографий, добавленных кем-либо из членов семьи, количестве семейных публикаций, опубликованных в семье, и, наконец, в этом разделе количество членов семьи в соответствии с их местонахождением на карта мира.',
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Familienstatistiken weltweit:',
            'desc'=>'Wir bieten Ihnen einen Bericht über die Anzahl der Mitglieder, die Anzahl der Fotos, die von einem der Familienmitglieder hinzugefügt wurden, die Anzahl der innerhalb der Familie veröffentlichten Familienpublikationen und schließlich in diesem Abschnitt die Anzahl der Familienmitglieder nach ihrem Standort auf der Weltkarte.',
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Estadísticas familiares en todo el mundo:',
            'desc'=>'Te proporcionamos un informe sobre el número de miembros, el número de fotos añadidas por alguno de los miembros de la familia, el número de publicaciones familiares publicadas dentro de la familia, y finalmente en este apartado el número de miembros de la familia según su ubicación en el mapa del mundo.',
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'世界各地的家庭统计：',
            'desc'=>'我们为您提供关于成员数量、任何家庭成员添加的照片数量、家庭内发布的家庭出版物数量的报告，最后在此部分中根据他们在家庭成员上的位置提供家庭成员数量。世界地图。',
            'lang'=>'cn'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'تقييم عجلة حياتك :',
            'desc'=>'يظهر هذا الجزء في حالة إجراء الإختبار فقط الخاص بعجلة حياتك وبالطبع إن لم يكن موجودا فعليك بالقيام بإجراء الإختبار أولا ثم يظهر أسفل منه المهام التي قمت بتحديدها لتقوم بتنفيذها لتطوير عجلة حياتك ومن هذا القسم عليك متابعتها بشكل يومي لمراجعة تطوير حياتك في نهاية الفترة .',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Evaluate the wheel of your life :',
            'desc'=>'This part appears in the case of taking the test only for the wheel of your life, and of course, if it is not present, then you must take the test first, and then it appears below it the tasks that you have selected to implement to develop the wheel of your life, and from this section you have to follow it on a daily basis to review the development of your life at the end of the period.',
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Évaluez la roue de votre vie :',
            'desc'=>"Cette partie apparaît dans le cas de passer le test uniquement pour la roue de votre vie, et bien sûr, si elle n'est pas présente, alors vous devez d'abord passer le test, puis il apparaît en dessous les tâches que vous avez choisi de mettre en œuvre pour développer la roue de votre vie, et à partir de cette section vous devez la suivre au quotidien pour revoir le développement de votre vie à la fin de la période.",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Hayatınızın çarkını değerlendirin:',
            'desc'=>'Bu kısım, sadece hayatınızın çarkı için sınava girmeniz durumunda görünür ve tabii ki, mevcut değilse, o zaman önce sınava girmeniz gerekir ve ardından uygulamayı seçtiğiniz görevlerin altında görünür. hayatınızın çarkını geliştirmek için ve bu bölümden günlük olarak takip ederek dönem sonunda hayatınızın gelişimini gözden geçirmelisiniz.',
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Оцените колесо своей жизни:',
            'desc'=>'Эта часть появляется в случае прохождения теста только для колеса вашей жизни, и, конечно, если его нет, то вы должны сначала пройти тест, а затем под ним появляются задачи, которые вы выбрали для реализации. чтобы развить колесо своей жизни, и из этого раздела вы должны ежедневно следовать ему, чтобы анализировать развитие своей жизни в конце периода.',
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Bewerten Sie das Rad Ihres Lebens:',
            'desc'=>'Dieser Teil erscheint, wenn Sie den Test nur für das Rad Ihres Lebens ablegen, und natürlich, wenn er nicht vorhanden ist, müssen Sie zuerst den Test ablegen, und dann erscheinen darunter die Aufgaben, die Sie zur Umsetzung ausgewählt haben um das Rad Ihres Lebens zu entwickeln, und von diesem Abschnitt aus müssen Sie es täglich verfolgen, um die Entwicklung Ihres Lebens am Ende der Periode zu überprüfen.',
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Evalúa la rueda de tu vida:',
            'desc'=>'Esta parte aparece en el caso de tomar la prueba solo para la rueda de tu vida, y por supuesto, si no está presente, entonces debes tomar la prueba primero, y luego aparecen debajo las tareas que has seleccionado para implementar. para desarrollar la rueda de tu vida, y desde este apartado tienes que seguirla a diario para repasar el desarrollo de tu vida al final del período.',
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'评估你的生命之轮',
            'desc'=>'这部分出现在只为你人生的轮子参加考试的情况下，当然，如果没有，那么你必须先参加考试，然后在它下面出现你选择执行的任务发展你的生命之轮，从这个部分你必须每天跟随它来回顾你在这个时期结束时的生活发展。',
            'lang'=>'cn'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'إحصائيات توزيع الأعمار :',
            'desc'=>'ويظهر بالطبع توزيع الأعمار مع الأعداد إذا كان تاريخ الميلاد موجودا وتم تسجيله للعضو ، ويظهر بجانبها إحصائية بسيطة عن أعداد الصور والمنشورات والمشاركات والمناسبات التي أضيفت من أفراد العائلة .',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Age distribution statistics :',
            'desc'=>'Of course, it shows the distribution of ages with numbers if the date of birth is present and registered for the member, and a simple statistic appears next to it about the number of photos, publications, posts and events that were added by family members.',
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Statistiques de répartition par âge :',
            'desc'=>"Bien sûr, il montre la répartition des âges avec des chiffres si la date de naissance est présente et enregistrée pour le membre, et une simple statistique apparaît à côté d'elle sur le nombre de photos, publications, messages et événements qui ont été ajoutés par les membres de la famille.",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Yaş dağılımı istatistikleri:',
            'desc'=>'Tabii ki, doğum tarihi varsa ve üye için kayıtlıysa yaşların dağılımını sayılarla gösterir ve yanında aile üyeleri tarafından eklenen fotoğraf, yayın, gönderi ve etkinlik sayısı hakkında basit bir istatistik görünür.',
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Статистика распределения по возрасту:',
            'desc'=>'Конечно, он показывает распределение возрастов с числами, если дата рождения присутствует и зарегистрирована для члена, а рядом с ним появляется простая статистика о количестве фотографий, публикаций, сообщений и событий, которые были добавлены членами семьи.',
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Statistiken zur Altersverteilung:',
            'desc'=>'Natürlich zeigt es die Altersverteilung mit Zahlen an, wenn das Geburtsdatum vorhanden und für das Mitglied registriert ist, und daneben erscheint eine einfache Statistik über die Anzahl der Fotos, Veröffentlichungen, Beiträge und Ereignisse, die von Familienmitgliedern hinzugefügt wurden.',
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Estadísticas de distribución por edad:',
            'desc'=>'Por supuesto, muestra la distribución de edades con números si la fecha de nacimiento está presente y registrada para el miembro, y junto a ella aparece una estadística simple sobre la cantidad de fotos, publicaciones, publicaciones y eventos que agregaron los miembros de la familia.',
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'年龄分布统计：',
            'desc'=>'当然，如果有出生日期并为会员注册，它会用数字显示年龄分布，旁边会出现一个关于家庭成员添加的照片、出版物、帖子和事件数量的简单统计数据。',
            'lang'=>'cn'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'رسومات بيانية توضيحية :',
            'desc'=>'في هذا القسم تظهر رسومات بيانية كإحصائيات للجنس أو الحالة الأسرية أو الأحياء والراحلون عن العائلة في شكل منحني بياني .',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Illustrative graphics :',
            'desc'=>'In this section, graphs appear as statistics of gender, family status, living and departed from the family in the form of a graph.',
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Graphiques illustratifs :',
            'desc'=>"Dans cette section, les graphiques apparaissent sous forme de statistiques sur le sexe, la situation familiale, les personnes vivant et sorties de la famille sous la forme d'un graphique.",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Açıklayıcı grafikler:',
            'desc'=>'Bu bölümde grafikler cinsiyet, aile durumu, yaşama ve aileden ayrılma istatistikleri şeklinde grafik şeklinde yer almaktadır.',
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Иллюстративная графика:',
            'desc'=>'В этом разделе графики отображаются как статистика пола, семейного положения, проживания и выбывших из семьи в виде графика.',
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Anschauliche Grafiken:',
            'desc'=>'In diesem Abschnitt erscheinen Grafiken als Statistiken zu Geschlecht, Familienstand, Leben und Ausscheiden aus der Familie in Form einer Grafik.',
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Gráficos ilustrativos:',
            'desc'=>'En esta sección, los gráficos aparecen como estadísticas de género, situación familiar, vida y difuntos de la familia en forma de gráfico.',
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'说明性图形：',
            'desc'=>'在本节中，图表以图表的形式显示为性别、家庭状况、生活和离开家庭的统计数据。',
            'lang'=>'cn'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'من هم أكبر وأصغر الأعضاء :',
            'desc'=>'في هذا القسم الأخير يظهر أكبر الأعضاء الأحياء والراحلون من الجنسين بالصوره وبعض البيانات .',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Who are the oldest and youngest members :',
            'desc'=>'In this last section, the largest living and deceased members of both sexes appear in pictures and some data.',
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Qui sont les membres les plus âgés et les plus jeunes :',
            'desc'=>'Dans cette dernière section, les plus grands membres vivants et décédés des deux sexes apparaissent en images et dans certaines données.',
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'En yaşlı ve en genç üyeler kimlerdir:',
            'desc'=>'Bu son bölümde, her iki cinsiyetin yaşayan ve ölen en büyük üyeleri resimlerde ve bazı verilerde görünmektedir.',
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Кто самые старые и самые молодые участники:',
            'desc'=>'В этом последнем разделе самые крупные живые и умершие представители обоих полов показаны на изображениях и некоторых данных.',
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Wer sind die ältesten und jüngsten Mitglieder:',
            'desc'=>'In diesem letzten Abschnitt erscheinen die größten lebenden und verstorbenen Mitglieder beiderlei Geschlechts in Bildern und einigen Daten.',
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'Quiénes son los miembros más viejos y los más jóvenes:',
            'desc'=>'En este último apartado aparecen en fotografías y algunos datos los miembros mayores vivos y fallecidos de ambos sexos.',
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'statistics',
            'topic_id'=>'2',
            'title'=>'谁是最年长和最年轻的成员：',
            'desc'=>'在最后一节中，最大的两性在世和已故成员出现在图片和一些数据中。',
            'lang'=>'cn'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'إنشاء شجرة عائلتك :',
            'desc'=>'تستطيع من هنا إضافة كل أفراد عائلتك والذي سوف يكتمل بدخول كل فرد منهم وإضافة باقي عائلته وهكذا ، فنحن كعائلة واحدة نبني في نفس الشجرة فنصبح جميعنا متصلين ومتكاملين سويا .',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'Create your family tree :',
            'desc'=>'From here, you can add all your family members, which will be completed by entering each one of them and adding the rest of his family, and so on. As one family, we build in the same tree, so we are all connected and integrated together.',
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'Créez votre arbre généalogique :',
            'desc'=>"De là, vous pouvez ajouter tous les membres de votre famille, ce qui sera complété en entrant chacun d'eux et en ajoutant le reste de sa famille, et ainsi de suite. Comme une seule famille, nous construisons dans le même arbre, nous sommes donc tous connectés et intégrés ensemble.",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'Aile ağacınızı oluşturun:',
            'desc'=>'Buradan tüm aile üyelerinizi ekleyebilirsiniz, her birine girip ailesinin geri kalanını ekleyerek tamamlanacak vb. Bir aile olarak aynı ağaçta inşa ediyoruz, bu yüzden hepimiz birbirimize bağlı ve bütünleşmiş durumdayız.',
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'Отсюда вы можете добавить всех членов своей семьи, что будет завершено путем ввода каждого из них, добавления остальных членов его семьи и так далее. Как одна семья, мы строим одно и то же дерево, поэтому все мы связаны и интегрированы друг с другом.',
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'Erstellen Sie Ihren Stammbaum:',
            'desc'=>'Von hier aus können Sie alle Ihre Familienmitglieder hinzufügen, was durch Eingabe jedes einzelnen von ihnen und dem Hinzufügen des Rests seiner Familie usw. abgeschlossen wird. Als eine Familie bauen wir im selben Baum, sodass wir alle miteinander verbunden und integriert sind.',
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'Crea tu árbol genealógico:',
            'desc'=>'Desde aquí podrás agregar a todos los miembros de tu familia, que se completará ingresando a cada uno de ellos y agregando el resto de su familia, y así sucesivamente. Como una familia, construimos en el mismo árbol, por lo que todos estamos conectados e integrados.',
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'创建你的家谱：',
            'desc'=>'从这里，您可以添加所有家庭成员，这将通过输入每个家庭成员并添加其他家庭成员来完成，依此类推。作为一个家庭，我们建在同一棵树上，所以我们都联系在一起，整合在一起。',
            'lang'=>'cn'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'فكرة علم الأنساب :',
            'desc'=>'هو علم يتعرف منه أنساب الناس وقواعده الكلية والجزئية والغرض منه : الاحتراز عن الخطأ في نسب شخص وهو علم عظيم النفع جليل القدر أشار القرآن العظيم في : ((وجعلناكم شعوبا وقبائل لتعارفوا)) إلى تفهمه وحث الرسول الكريم في : ((تعلموا الأنساب كي تصلوا أرحامكم)) وحث على تعلمه والعرب قد اعتنى في ضبط نسبه إلى أن أكثر أهل الإسلام .',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'The idea of genealogy :',
            'desc'=>'It is a science from which people’s genealogy and its general and partial rules can be known. Its purpose is to guard against error in genealogy of a person, and it is a science of great benefit and great destiny. The great Qur’an indicated in: ((And We made you peoples and tribes so that you may know one another)) to understand it and the Holy Prophet urged: ((Learn genealogy in order to pray)) your wombs)) and urged him to learn it, and the Arabs have taken care of controlling his lineage because most of the people of Islam.',
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>"L'idée de généalogie :",
            'desc'=>"C'est une science à partir de laquelle on peut connaître la généalogie des gens et ses règles générales et partielles. Son but est de se prémunir contre l'erreur dans la généalogie d'une personne, et c'est une science d'une grande utilité et d'une grande destinée. Le grand Coran a indiqué dans : ((Et Nous avons fait de vous des peuples et des tribus afin que vous vous connaissiez)) pour le comprendre et le Saint Prophète a exhorté : ((Apprenez la généalogie pour prier)) vos entrailles)) et l'a poussé à l'apprendre, et les Arabes ont pris soin de contrôler sa lignée car la plupart des gens de l'Islam.",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'Soyağacı fikri:',
            'desc'=>"İnsanların şeceresinin, genel ve kısmî kurallarının öğrenilebildiği bir bilimdir. Amacı, bir kişinin şeceresinde hataya karşı korunmak ve büyük fayda ve büyük bir kader bilimidir. Büyük Kur'an'ın: ((Birbirinizi tanıyasınız diye sizi kavimler ve kabileler kıldık)) onu anlamanız için işaret ettiği ve Peygamber Efendimiz (s.a.v. Onu öğrenmeye teşvik etmiş ve Arapların çoğu İslam ehli olduğu için onun soyunu kontrol etmeye özen göstermişlerdir.",
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'Идея генеалогии:',
            'desc'=>'Это наука, из которой можно узнать генеалогию людей и ее общие и частичные правила. Его цель - уберечься от ошибок в генеалогии человека, и это наука, приносящая большую пользу и великую судьбу. Великий Коран указал в: ((Мы сделали вас народами и племенами, чтобы вы знали друг друга)) понять его, и Святой Пророк призвал: ((Изучите генеалогию, чтобы молиться)) ваши утробы)) и уговаривали его изучить это, и арабы позаботились о том, чтобы контролировать его родословную, потому что большинство людей ислама.',
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'Die Idee der Genealogie:',
            'desc'=>'Es ist eine Wissenschaft, aus der die Genealogie der Menschen und ihre allgemeinen und partiellen Regeln bekannt sind. Sein Zweck ist es, Fehler in der Genealogie einer Person zu vermeiden, und es ist eine Wissenschaft von großem Nutzen und großem Schicksal. Der große Koran deutete in: ((Und Wir haben euch zu Völkern und Stämmen gemacht, damit ihr einander kennt)) um es zu verstehen und der Heilige Prophet drängte: ((Lerne Genealogie, um zu beten)) deine Gebärmutter)) und drängte ihn, es zu lernen, und die Araber haben sich darum gekümmert, seine Abstammung zu kontrollieren, weil die meisten Menschen des Islam.',
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'La idea de la genealogía:',
            'desc'=>'Es una ciencia a partir de la cual se puede conocer la genealogía de las personas y sus reglas generales y parciales. Su propósito es protegerse contra el error en la genealogía de una persona, y es una ciencia de gran beneficio y gran destino. El gran Corán indicó en: ((Y os hicimos pueblos y tribus para que os conozcáis)) para entenderlo y el Santo Profeta exhortó: ((Aprende la genealogía para rezar)) vuestros vientres)) y Lo instó a aprenderlo, y los árabes se han encargado de controlar su linaje porque la mayoría de la gente del Islam.',
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'族谱思想：',
            'desc'=>'它是一门可以了解人们的家谱及其一般和部分规则的科学。它的目的是防止一个人的家谱错误，它是一门大利大运的科学。伟大的古兰经指出：（（我使你们成为人民和部落，以便你们可以相互认识））理解它，先知敦促：（（为了祈祷而学习家谱））你的子宫））和敦促他学习它，阿拉伯人已经照顾控制了他的血统，因为大多数人都是伊斯兰教徒。',
            'lang'=>'cn'
        ] );

         FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'القاعدة الأولي لإضافة الأعضاء بشكل سليم :',
            'desc'=>'بما أننا نتكلم عن علم الأنساب فلابد من إضافة الأصل ثم الفرع بمعني أنك تستطيع إضافة فرد وزوجته فقط ولن تستطيع إضافة أخوته وأخواته بدون إضافة الوالد الذي يربطهم سويا وبالطبع لابد من وجود الأم الذي تنسبها إليه وقد تتعدد الأمهات ولكن يستحيل تعدد الآباء لشخص واحد .',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'The first rule for adding members correctly:',
            'desc'=>'Since we are talking about genealogy, it is necessary to add the origin and then the branch, meaning that you can add an individual and his wife only, and you will not be able to add his brothers and sisters without adding the father who links them together.',
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'La première règle pour ajouter correctement des membres :',
            'desc'=>"Puisqu'on parle de généalogie, il faut ajouter l'origine puis la branche, c'est à dire que vous pouvez ajouter un individu et sa femme uniquement, et vous ne pourrez pas ajouter ses frères et sœurs sans ajouter le père qui les relie ensemble.",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'Üyeleri doğru şekilde eklemenin ilk kuralı:',
            'desc'=>'Soy kütüğünden bahsettiğimiz için önce menşei sonra dalı eklemek gerekiyor yani sadece bir bireyi ve karısını ekleyebiliyorsunuz ve onları bağlayan babayı eklemeden erkek ve kız kardeşlerini ekleyemezsiniz. bir arada.',
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'Первое правило правильного добавления участников:',
            'desc'=>'Поскольку мы говорим о генеалогии, необходимо добавить происхождение, а затем ветвь, что означает, что вы можете добавить только человека и его жену, и вы не сможете добавить его братьев и сестер, не добавив отца, который их связывает. вместе.',
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'Die erste Regel zum korrekten Hinzufügen von Mitgliedern:',
            'desc'=>'Da wir über Genealogie sprechen, ist es notwendig, den Ursprung und dann den Zweig hinzuzufügen, was bedeutet, dass Sie nur eine Person und ihre Frau hinzufügen können und Sie können nicht ihre Brüder und Schwestern hinzufügen, ohne den Vater hinzuzufügen, der sie verbindet zusammen.',
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'La primera regla para agregar miembros correctamente:',
            'desc'=>'Como estamos hablando de genealogía, es necesario agregar el origen y luego la rama, lo que significa que puede agregar un individuo y su esposa solo, y no podrá agregar a sus hermanos y hermanas sin agregar el padre que los vincula. juntos.',
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'正确添加成员的第一条规则：',
            'desc'=>'既然是家谱，就需要先加起源，然后是分支，也就是说你只能加一个人和他的妻子，不能加他的兄弟姐妹，不加联系他们的父亲一起。',
            'lang'=>'cn'
        ] );

         FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'القاعدة الثانية لإضافة بيانات الأعضاء :',
            'desc'=>'من الضروري كتابة التواريخ بشكل دقيق للحصول علي معلومات قريبة من الواقع بما فيها تواريخ الميلاد والزواج والوفاة ، وحتي تظهر لك المناسبات في مواعيدها السليمة ، ويمكن لأي فرد من العائلة تعديل أي تاريخ إذا كان علي يقين منه .',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'The second rule for adding member data:',
            'desc'=>'It is necessary to write the dates accurately to obtain information that is close to reality, including the dates of birth, marriage and death, and to show you the occasions on their proper dates, and any family member can modify any date if he is certain of it.',
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'La deuxième règle pour ajouter des données de membre :',
            'desc'=>"Il est nécessaire d'écrire les dates avec précision pour obtenir des informations proches de la réalité, y compris les dates de naissance, de mariage et de décès, et pour vous montrer les occasions à leurs dates appropriées, et tout membre de la famille peut modifier n'importe quelle date s'il en est certain. de celui-ci.",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'Üye verilerini eklemek için ikinci kural:',
            'desc'=>'Doğum, evlilik ve ölüm tarihleri de dahil olmak üzere gerçeğe yakın bilgileri elde etmek ve size olayları uygun tarihlerinde göstermek için tarihleri doğru yazmak gerekir ve herhangi bir aile üyesi, kesin olduğu takdirde herhangi bir tarihi değiştirebilir. ondan.',
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'Второе правило добавления данных о членах:',
            'desc'=>'Необходимо точно указывать даты, чтобы получить информацию, близкую к реальности, включая даты рождения, брака и смерти, и показать вам случаи в их надлежащие даты, и любой член семьи может изменить любую дату, если он уверен из этого.',
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'Die zweite Regel zum Hinzufügen von Mitgliedsdaten:',
            'desc'=>'Es ist notwendig, die Daten genau zu schreiben, um realitätsnahe Informationen, einschließlich Geburts-, Heirats- und Sterbedaten, zu erhalten und Ihnen die Anlässe an den richtigen Daten anzuzeigen, und jedes Familienmitglied kann jedes Datum ändern, wenn es sich sicher ist davon.',
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'La segunda regla para agregar datos de miembros:',
            'desc'=>'Es necesario escribir las fechas con precisión para obtener información cercana a la realidad, incluidas las fechas de nacimiento, matrimonio y muerte, y para mostrarle las ocasiones en sus fechas adecuadas, y cualquier miembro de la familia puede modificar cualquier fecha si está seguro. de ella.',
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'添加成员数据的第二条规则：',
            'desc'=>'需要准确地写出日期，以获得接近现实的信息，包括出生日期、结婚日期和死亡日期，并向您展示合适的日期，任何家庭成员如果确定可以修改任何日期其中。',
            'lang'=>'cn'
        ] );

         FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'القاعدة الأخيرة في الإضافة :',
            'desc'=>'إضافة بلد المعيشة لكل فرد تمكنك من مشاهدة أخبار البلد والطقس والمناسبات المحلية في هذا البلد ، وبالتأكيد لا ننسي رقم الهاتف لأننا نستعمل كود الدوله بشكل مخفي داخل البرنامج لكي نجعلك دائما على تواصل معه فلا تفقد هذه الميزه .',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'The last rule to add:',
            'desc'=>"Adding the country of living for each individual enables you to watch the country's news, weather and local events in this country, and certainly do not forget the phone number because we use the country code in a hidden way inside the program in order to keep you in touch with it, so do not lose this feature.",
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'La dernière règle à ajouter :',
            'desc'=>"L'ajout du pays de vie pour chaque individu vous permet de regarder les actualités du pays, la météo et les événements locaux dans ce pays, et certainement de ne pas oublier le numéro de téléphone car nous utilisons le code du pays de manière cachée à l'intérieur du programme afin de vous garder en contact avec lui, alors ne perdez pas cette fonctionnalité.",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'Eklenecek son kural:',
            'desc'=>"Her birey için yaşadığı ülkeyi eklemek, bu ülkedeki ülkenin haberlerini, hava durumunu ve yerel olaylarını izlemenizi sağlar ve telefon numarasını kesinlikle unutmayın çünkü sizi tutmak için program içinde ülke kodunu gizli bir şekilde kullanıyoruz. onunla temasa geçin, bu yüzden bu özelliği kaybetmeyin.",
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'Последнее правило, которое нужно добавить:',
            'desc'=>"Добавление страны проживания для каждого человека позволяет вам смотреть новости страны, погоду и местные события в этой стране и, конечно же, не забывать номер телефона, потому что мы используем код страны скрытым образом внутри программы, чтобы вы в контакте с ним, поэтому не теряйте эту функцию.",
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'Die letzte hinzuzufügende Regel:',
            'desc'=>"Wenn Sie das Land, in dem Sie leben, für jeden Einzelnen hinzufügen, können Sie die Nachrichten des Landes, das Wetter und die lokalen Ereignisse in diesem Land verfolgen und die Telefonnummer auf keinen Fall vergessen, da wir die Landesvorwahl im Programm versteckt verwenden, um Sie zu behalten in Kontakt damit, also verliere diese Funktion nicht.",
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'La última regla para agregar:',
            'desc'=>"Agregar el país de vida de cada individuo le permite ver las noticias del país, el clima y los eventos locales en este país, y ciertamente no olvide el número de teléfono porque usamos el código del país de manera oculta dentro del programa para mantenerlo en contacto con él, así que no pierda esta función.",
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'要添加的最后一条规则：',
            'desc'=>"添加每个人的居住国家/地区使您可以观看该国家/地区的新闻，天气和当地事件，当然不要忘记电话号码，因为我们在程序中以隐藏的方式使用国家/地区代码，以保持您接触它，所以不要失去这个功能。",
            'lang'=>'en'
        ] );

         FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'إضافة صور أفراد العائلة :',
            'desc'=>'يمكنك إضافة صورة العضو الذي قمت بإضافته من حسابك وإذا كنت لا تمتلك صورة جيدة له فلا تنسي إبلاغه بوضع صورة له حتي تظهر في مواضيعه أو مشاركاته وكذلك عند طباعة شجرة العائلة كلها سواء لعائلة الأب أو الأم فالصوره الإفتراضية ليست أفضل من صورته الخاصه وتضفي جمالا بارزا لشجرة عائلتك .',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'Add family member photos:',
            'desc'=>"You can add a picture of the member that you added from your account and if you do not have a good picture of him, do not forget to inform him by placing a picture of him so that it appears in his topics or posts, as well as when printing the entire family tree, whether for the family of the father or mother, the default picture is not better than his own picture and gives a prominent beauty to the tree your family .",
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'Ajouter des photos de membres de la famille :',
            'desc'=>"Vous pouvez ajouter une photo du membre que vous avez ajouté depuis votre compte et si vous n'avez pas une bonne photo de lui, n'oubliez pas de l'en informer en plaçant une photo de lui afin qu'elle apparaisse dans ses sujets ou ses publications, ainsi comme lors de l'impression de l'ensemble de l'arbre généalogique, que ce soit pour la famille du père ou de la mère, la photo par défaut n'est pas meilleure que la sienne et donne une beauté proéminente à l'arbre de votre famille.",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'Aile üyesi fotoğrafları ekleyin:',
            'desc'=>"Eklediğiniz üyenin fotoğrafını hesabınızdan ekleyebilirsiniz ve eğer iyi bir fotoğrafınız yoksa onun konu veya gönderilerinde de görünmesi için fotoğrafını koyarak onu bilgilendirmeyi unutmayın. tüm aile ağacını yazdırırken olduğu gibi, babanın veya annenin ailesi için, varsayılan resim kendi resminden daha iyi değildir ve ailenizin ağacına belirgin bir güzellik verir.",
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'Добавьте фотографии членов семьи:',
            'desc'=>"Вы можете добавить изображение участника, которого вы добавили из своей учетной записи, и если у вас нет хорошего изображения его, не забудьте сообщить ему, разместив его изображение, чтобы оно также отображалось в его темах или сообщениях например, при печати всего генеалогического дерева, будь то для семьи отца или матери, изображение по умолчанию не лучше, чем его собственное изображение, и придает выдающуюся красоту дереву вашей семьи.",
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'Fotos von Familienmitgliedern hinzufügen:',
            'desc'=>"Sie können ein Bild des Mitglieds hinzufügen, das Sie von Ihrem Konto aus hinzugefügt haben, und wenn Sie kein gutes Bild von ihm haben, vergessen Sie nicht, es zu informieren, indem Sie ein Bild von ihm platzieren, damit es auch in seinen Themen oder Beiträgen erscheint B. beim Drucken des gesamten Stammbaums, ob für die Familie des Vaters oder der Mutter, ist das Standardbild nicht besser als sein eigenes Bild und verleiht dem Stammbaum Ihrer Familie eine herausragende Schönheit.",
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'Agregar fotos de miembros de la familia:',
            'desc'=>"Puedes agregar una foto del miembro que agregaste desde tu cuenta y si no tienes una buena foto de él, no olvides informarle colocando una foto de él para que aparezca en sus temas o publicaciones, así. Como cuando se imprime todo el árbol genealógico, ya sea para la familia del padre o la madre, la imagen predeterminada no es mejor que la de su propia imagen y le da una belleza destacada al árbol de su familia.",
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'添加家庭成员照片：',
            'desc'=>"您可以添加从您的帐户添加的成员的照片，如果您没有他的好照片，请不要忘记通过放置他的照片来通知他，以便它也出现在他的主题或帖子中就像在打印整个家谱时，无论是父亲或母亲的家庭，默认图片并不比他自己的图片更好，并为您的家庭树提供突出的美感。",
            'lang'=>'cn'
        ] );

         FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'أخطاء شائعة في الإضافة :',
            'desc'=>'أدخل الاسم الأول فقط ، إذا كان الاسم الذي تضيفه هو اسم مركب ، فقم بإضافته بدون مسافة أو أضف شرطة بين أجزاء الاسم. لا تضيف أبًا بدون أم لتفادي حدوث مشاكل لاحقًا. النقطة الأخيرة حاول تجنب حذف كل ما تبذلونه من الإضافات واستبدالها عن طريق تحرير أي مستخدم مرغوب.',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'general adding mistakes',
            'desc'=>"enter only first name ,if the name you add is compounded name so add it without space of add dash between name parts . don't add father without mother to avoid Problems will come later .final point try avoid deleting in all your add as posible and replace it by editting any desired user .",
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>"général d'ajout d'erreurs",
            'desc'=>"entrez uniquement le prénom, si le nom que vous ajoutez est un nom composé, alors ajoutez-le sans espace ou ajoutez un tiret entre les parties du nom. n'ajoutez pas de père sans mère pour éviter les problèmes viendront plus tard. dernier point essayez d'éviter de supprimer tous vos ajouts possibles et remplacez-le en modifiant l'utilisateur souhaité.",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'genel ekleme hataları',
            'desc'=>"yalnızca ilk adı girin, eklediğiniz ad birleşik adsa, boşluk bırakmadan ekleyin veya ad bölümlerinin arasına tire ekleyin. sakın annesiz baba eklemeyin sorunlar sonra gelir. son nokta mümkünse tüm eklemelerinizi silmemeye çalışın ve istediğiniz kullanıcıyı düzenleyerek değiştirin.",
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'общие ошибки добавления',
            'desc'=>"введите только имя, если имя, которое вы добавляете, является составным, добавьте его без пробела или добавьте дефис между частями имени. Не добавляйте отца без матери, чтобы избежать проблем. Конечный момент постарайтесь не удалять все ваши добавления, насколько это возможно, и замените его, отредактировав любого желаемого пользователя.",
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'allgemeine Fehler hinzufügen',
            'desc'=>"Geben Sie nur den Vornamen ein, wenn der Name, den Sie hinzufügen, ein zusammengesetzter Name ist, also fügen Sie ihn ohne Leerzeichen hinzu oder fügen Sie einen Bindestrich zwischen den Namensteilen hinzu. fügen Sie keinen Vater ohne Mutter hinzu, um zu vermeiden Probleme werden später auftreten. Letzter Punkt Versuchen Sie es zu vermeiden, alle Ihre Einträge zu löschen, und ersetzen Sie sie, indem Sie jeden gewünschten Benutzer bearbeiten.",
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'errores generales de adición',
            'desc'=>"ingrese solo el nombre, si el nombre que agrega es un nombre compuesto, así que agréguelo sin espacios o agregue un guión entre las partes del nombre. no agregue padre sin madre para evitar problemas vendrán más tarde. punto final intente evitar eliminar todo lo que pueda agregar y reemplácelo editando cualquier usuario deseado.",
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_tree',
            'topic_id'=>'3',
            'title'=>'一般添加错误',
            'desc'=>"仅输入名字，如果您添加的名称是复合名称，则添加不带空格或在名称部分之间添加破折号。不要在没有母亲的情况下添加父亲以避免问题稍后会出现。最后一点尽量避免删除所有添加的内容，并通过编辑任何所需的用户来替换它。",
            'lang'=>'cn'
        ] );

         FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'فكرة التقويم العائلي :',
            'desc'=>'يمكنك متابعة كافة تواريخ الميلاد والزواج والوفاة وغير ذلك من النتيجة أو من فلتر التواريخ بالشهور مثل المواقع الأخري ، ولكن الجديد هنا أنه يمكنك إضافة تاريخ معين لحدث معين وتطلب فيه حضور أفراد عائلتك أو مشاركتهم لك في أي حدث منظم في أي مكان قمت بتحديده .',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'family calendar idea :',
            'desc'=>"You can follow all the dates of birth, marriage, death and other results or from the filter dates in months like other sites, but what is new here is that you can add a specific date for a specific event and request the attendance of your family members or their participation for you in any organized event anywhere you have selected.",
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'idée de calendrier familial :',
            'desc'=>"Vous pouvez suivre toutes les dates de naissance, mariage, décès et autres résultats ou à partir des dates de filtrage en mois comme sur d'autres sites, mais ce qui est nouveau ici c'est que vous pouvez ajouter une date précise pour un événement précis et demander la présence de votre famille membres ou leur participation pour vous à tout événement organisé où que vous ayez sélectionné.",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'aile takvimi fikri:',
            'desc'=>"Diğer siteler gibi tüm doğum, evlilik, ölüm ve diğer sonuçları veya aylardaki filtre tarihlerini takip edebilirsiniz, ancak burada yeni olan belirli bir etkinlik için belirli bir tarih ekleyebilir ve ailenizin katılımını talep edebilirsiniz. Seçtiğiniz herhangi bir yerdeki herhangi bir organize etkinliğe üyeler veya onların katılımı.",
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'идея семейного календаря:',
            'desc'=>"Вы можете отслеживать все даты рождения, брака, смерти и другие результаты или даты фильтра в месяцах, как на других сайтах, но что здесь нового, так это то, что вы можете добавить конкретную дату для определенного события и запросить присутствие вашей семьи членов или их участие для вас в любом организованном мероприятии в любом месте, которое вы выбрали.",
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'Idee für einen Familienkalender:',
            'desc'=>"Sie können alle Geburts-, Heirats-, Todes- und andere Ergebnisse oder aus den Filterdaten in Monaten wie auf anderen Websites verfolgen, aber neu hier ist, dass Sie ein bestimmtes Datum für eine bestimmte Veranstaltung hinzufügen und die Teilnahme Ihrer Familie anfordern können Mitglieder oder deren Teilnahme für Sie an einer von Ihnen ausgewählten organisierten Veranstaltung.",
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'idea de calendario familiar:',
            'desc'=>"Puedes seguir todas las fechas de nacimiento, matrimonio, defunción y otros resultados o desde el filtro de fechas en meses como otros sitios, pero lo nuevo aquí es que puedes agregar una fecha específica para un evento específico y solicitar la asistencia de tu familia. miembros o su participación para usted en cualquier evento organizado en cualquier lugar que haya seleccionado.",
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'家庭日历的想法：',
            'desc'=>"您可以跟踪所有出生日期、结婚日期、死亡日期和其他结果，或者像其他网站一样从月份中筛选日期，但这里的新功能是您可以为特定事件添加特定日期并请求您的家人出席会员或他们为您参与您选择的任何有组织的活动。",
            'lang'=>'cn'
        ] );

         FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'إضافة حدث عائلي :',
            'desc'=>'يمكنك إضافة أي حدث عائلي تود إنضمام العائلة إليك من علامة الإضافة الموجودة أسفل تاريخ اليوم ، فتحدد بها كل تفاصيل المناسبة وإذا أردت نشرها علي صفحة العائلة فأجعلها منشور عام .',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'Add a family event:',
            'desc'=>"You can add any family event that you would like the family to join you from from the plus sign below today's date, specifying all the details of the event and if you want to publish it on the family page, make it a public post.",
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'Ajouter un événement familial :',
            'desc'=>"Vous pouvez ajouter n'importe quel événement familial à partir duquel vous souhaitez que la famille vous rejoigne à partir du signe plus sous la date d'aujourd'hui, en spécifiant tous les détails de l'événement et si vous souhaitez le publier sur la page de la famille, en faire un message public.",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'Bir aile etkinliği ekleyin:',
            'desc'=>"YAilenizin size katılmasını istediğiniz herhangi bir aile etkinliğini bugünün tarihinin altındaki artı işaretinden etkinliğin tüm detaylarını belirterek ekleyebilir ve aile sayfasında yayınlamak istiyorsanız herkese açık bir gönderi yapabilirsiniz.",
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'Добавить семейное мероприятие:',
            'desc'=>"Вы можете добавить любое семейное мероприятие, от которого вы хотите, чтобы семья присоединилась к вам, с помощью знака плюса под сегодняшней датой, указав все детали мероприятия, и, если вы хотите опубликовать его на семейной странице, сделайте его общедоступным.",
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'Fügen Sie eine Familienveranstaltung hinzu:',
            'desc'=>"Sie können jedes Familienereignis, zu dem Sie möchten, dass die Familie zu Ihnen kommt, über das Pluszeichen unter dem heutigen Datum hinzufügen, alle Details des Ereignisses angeben und wenn Sie es auf der Familienseite veröffentlichen möchten, machen Sie es zu einem öffentlichen Beitrag.",
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'Agregar un evento familiar:',
            'desc'=>"Puede agregar cualquier evento familiar al que le gustaría que la familia se uniera a usted desde el signo más debajo de la fecha de hoy, especificando todos los detalles del evento y, si desea publicarlo en la página familiar, hágalo una publicación pública.",
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'添加家庭活动：',
            'desc'=>"您可以从今天日期下方的加号添加您希望家人加入的任何家庭活动，指定活动的所有详细信息，如果您想将其发布在家庭页面上，请将其设为公开帖子。",
            'lang'=>'cn'
        ] );

         FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'الإنضمام لحدث عائلي :',
            'desc'=>'إبحث عن عنوان ( الإنضمام / الإهتمام بحدث ) عائلي تحت الصورة الرئيسية بالصفحة وإضغط عليها لتتمكن من الإنضمام إليها أو تسجيل مجرد إهتمامك بها ليصل هذا التنبية إلي صاحب الحدث مباشرة .',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'Join a family event:',
            'desc'=>"Search for the title of (joining / interest) in a family event under the main image of the page and click on it to be able to join it or just register your interest in it so that this alert reaches the owner of the event directly.",
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'Participez à un événement familial :',
            'desc'=>"Recherchez le titre de (rejoindre / intérêt) dans un événement familial sous l'image principale de la page et cliquez dessus pour pouvoir le rejoindre ou enregistrez simplement votre intérêt pour que cette alerte parvienne directement au propriétaire de l'événement.",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'Bir aile etkinliğine katılın:',
            'desc'=>"Sayfanın ana resminin altında bir aile etkinliğinde (katılma / ilgi) başlığını arayın ve katılmak için üzerine tıklayın veya bu uyarının doğrudan etkinliğin sahibine ulaşması için ilginizi kaydedin.",
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'Присоединяйтесь к семейному мероприятию:',
            'desc'=>"Найдите заголовок (присоединение / интерес) к семейному мероприятию под основным изображением страницы и щелкните его, чтобы присоединиться к нему, или просто зарегистрируйте свой интерес к нему, чтобы это предупреждение напрямую доходило до владельца мероприятия.",
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'Nehmen Sie an einem Familienevent teil:',
            'desc'=>"Suchen Sie unter dem Hauptbild der Seite nach dem Titel (Beitritt / Interesse) an einer Familienveranstaltung und klicken Sie darauf, um daran teilzunehmen oder registrieren Sie einfach Ihr Interesse daran, damit diese Benachrichtigung den Eigentümer der Veranstaltung direkt erreicht.",
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'Únase a un evento familiar:',
            'desc'=>"Busque el título de (participación / interés) en un evento familiar debajo de la imagen principal de la página y haga clic en él para poder unirse o simplemente registre su interés en él para que esta alerta llegue directamente al propietario del evento.",
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'参加家庭活动',
            'desc'=>"在页面主图像下搜索（加入/感兴趣）家庭活动的标题，然后单击它以能够加入它或仅注册您对它的兴趣，以便此警报直接到达活动的所有者。",
            'lang'=>'cn'
        ] );

         FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'ميزة التنقل عبر الأيام :',
            'desc'=>'يمكنك بكل سلاسة التنقل داخل النتيجة المعروضة أمامك للبحث داخل الأشهر ثم الولوج داخل الأسبوع فاليوم لتري الساعات بالضبط ، كما يمكنك الإنتقال إلي مشاهدة كافة التواريخ بالسنة كلها من خلال زر ( الأعضاء والأحداث) والموجود أسفل الصورة الرئيسية في الصفحة لتجد كل شهر متبوعا بكل مناسباته .',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'Day Navigation Feature:',
            'desc'=>"You can smoothly navigate within the result displayed in front of you to search within the months and then access within the week for today to see the exact hours, and you can also move to view all dates in the whole year through the (Members and Events) button located at the bottom of the main image on the page to find each month followed by all its occasions.",
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'Fonction de navigation de jour :',
            'desc'=>"Vous pouvez naviguer en douceur dans le résultat affiché devant vous pour rechercher dans les mois, puis accéder dans la semaine pour aujourd'hui pour voir les heures exactes, et vous pouvez également vous déplacer pour afficher toutes les dates de l'année entière à travers les (membres et événements ) situé en bas de l'image principale de la page pour retrouver chaque mois suivi de toutes ses occasions.",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'Gündüz Navigasyon Özelliği:',
            'desc'=>"Aylar içinde arama yapmak için önünüzde görüntülenen sonuç içinde sorunsuzca gezinebilir ve ardından tam saatleri görmek için hafta içinde bugün için erişebilir ve ayrıca (Üyeler ve Etkinlikler) aracılığıyla tüm yıl içindeki tüm tarihleri görüntülemek için hareket edebilirsiniz. ) butonu ile ana görselin alt kısmında bulunan buton ile her ayı takip ederek tüm durumlarını bulabilirsiniz.",
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'Функция дневной навигации:',
            'desc'=>"Вы можете плавно перемещаться по результату, отображаемому перед вами, для поиска по месяцам, а затем получить доступ в течение недели на сегодняшний день, чтобы увидеть точные часы, а также вы можете перейти к просмотру всех дат за весь год с помощью (Участники и события ), расположенную в нижней части основного изображения на странице, чтобы найти каждый месяц, за которым следуют все его события.",
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'Tagesnavigationsfunktion:',
            'desc'=>"Sie können problemlos innerhalb des vor Ihnen angezeigten Ergebnisses navigieren, um innerhalb der Monate zu suchen und dann innerhalb der Woche für heute auf die genauen Stunden zuzugreifen ) Schaltfläche am unteren Rand des Hauptbilds auf der Seite, um jeden Monat gefolgt von all seinen Anlässen zu finden.",
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'Función de navegación diurna:',
            'desc'=>"Puede navegar sin problemas dentro del resultado que se muestra frente a usted para buscar dentro de los meses y luego acceder dentro de la semana para hoy para ver las horas exactas, y también puede moverse para ver todas las fechas en todo el año a través de (Miembros y eventos ) ubicado en la parte inferior de la imagen principal de la página para encontrar cada mes seguido de todas sus ocasiones.",
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_calendar',
            'topic_id'=>'4',
            'title'=>'日导航功能：',
            'desc'=>"您可以在显示在您面前的结果中顺畅导航以在几个月内进行搜索，然后在一周内访问今天的确切时间，您还可以通过（成员和事件）移动以查看全年中的所有日期) 按钮位于页面上的主图像底部，以查找每个月以及其所有场合。",
            'lang'=>'cn'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'ماهو مفهوم المشاركات العائلية ؟',
            'desc'=>'المقصود بالمشاركات العائلية هو الموضوعات التي تهم العائلة ويمكن المشاركة الفعالة فيها من أي عضو من الأعضاء ،مثل الجمعيات بين الأفراد والأضحيات السنوية والمساهمات مع بعض الأعضاء الذين يجب علينا مساعدتهم داخل العائلة والمشاركة في ختمة القرآن علي روح أحد مفقودينا أو المشاركة في صندوق فعال للعائلة بشكل دوري للطوارئ أو تطوير الشباب أو ما قد نراه مناسبا كعائلة واحدة ( فالقوة في نحن ) .',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'What is the concept of family posts?',
            'desc'=>"What is meant by family participations are topics of interest to the family and can be actively participated in by any member of the family, such as interpersonal associations, annual sacrifices, contributions with some members whom we must help within the family, participating in the recitation of the Qur’an on the soul of one of our missing persons, or participating in an effective family fund on a regular basis. For emergencies, youth development, or what we may see fit as one family (the strength is in us).",
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'Quel est le concept des postes familiaux ?',
            'desc'=>"Ce que l'on entend par participation familiale, ce sont des sujets d'intérêt pour la famille et auxquels tout membre de la famille peut participer activement, tels que les associations interpersonnelles, les sacrifices annuels, les contributions avec certains membres que nous devons aider au sein de la famille, la participation à la récitation du Coran sur l'âme d'une de nos personnes disparues, ou en participant régulièrement à un fonds familial efficace. Pour les urgences, le développement des jeunes, ou ce que nous pouvons juger bon comme une seule famille (la force est en nous).",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'Aile gönderilerinin konsepti nedir?',
            'desc'=>"Aile katılımlarından kastedilen, aileyi ilgilendiren konulardır ve kişilerarası dernekler, yıllık fedakarlıklar, aile içinde yardım etmemiz gereken bazı üyelerle yapılan katkılar, kıraatlere katılım gibi ailenin herhangi bir üyesinin aktif olarak katılabileceği konulardır. Kayıp şahıslarımızdan birinin ruhu hakkında Kuran'a uymak veya düzenli olarak etkin bir aile fonuna katılmak. Acil durumlar, gençlik gelişimi veya tek bir aile olarak uygun gördüğümüz durumlar için (güç bizdedir).",
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'Что такое семейные посты?',
            'desc'=>"Под семейным участием понимаются темы, представляющие интерес для семьи, в которых может активно участвовать любой член семьи, например, межличностные ассоциации, ежегодные жертвоприношения, пожертвования с некоторыми членами, которым мы должны помогать в семье, участие в декламации. Корана на душе одного из наших пропавших без вести, или участие в эффективном семейном фонде на регулярной основе. Для чрезвычайных ситуаций, развития молодежи или того, что мы считаем нужным стать одной семьей (сила в нас).",
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'Was ist das Konzept von Familienposts?',
            'desc'=>"Mit Familienbeteiligungen sind Themen gemeint, die für die Familie von Interesse sind und an denen jedes Familienmitglied aktiv teilnehmen kann, wie z des Korans auf der Seele einer unserer Vermissten oder regelmäßige Teilnahme an einem effektiven Familienfonds. Für Notfälle, Jugendförderung oder was wir als eine Familie für richtig halten (die Stärke liegt in uns).",
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'¿Cuál es el concepto de publicaciones familiares?',
            'desc'=>"Lo que se entiende por participaciones familiares son temas de interés para la familia y en los que puede participar activamente cualquier miembro de la familia, como asociaciones interpersonales, sacrificios anuales, aportes con algunos miembros a los que debemos ayudar dentro de la familia, participando en la recitación. del Corán en el alma de una de nuestras personas desaparecidas, o participando regularmente en un fondo familiar efectivo. Para emergencias, desarrollo juvenil o lo que consideremos adecuado como una familia (la fuerza está en nosotros).",
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'家族帖是什么概念？',
            'desc'=>"家庭参与的含义是家庭感兴趣的话题，任何家庭成员都可以积极参与，比如人际交往、年度祭祀、与一些家庭中必须帮助的成员一起贡献、参与背诵古兰经在我们失踪人员的灵魂上，或定期参与有效的家庭基金。对于紧急情况、青年发展或我们认为适合一个家庭的事情（力量在我们身上）。",
            'lang'=>'cn'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'كيف أضيف مساهمة جديدة ؟',
            'desc'=>'أدخل علي إعدادات ضبط حسابك من القائمة العلوية تحت صورتك الشخصية وإذهب إلي قائمة ( مساهماتي المضافة ) لإضافة مساهمة جديدة أنت المسئول عن إدارتها ، لا تنسي إضافة وصف لها وإضافتها للتصنيف الصحيح مع تحديد العدد المطلوب وإنتظر إنضمام باقي الأعضاء حتي تكتمل فإضغط علي زر (البدء) الموجود أسفل المشاركة . ',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'How do I add a new contribution?',
            'desc'=>"Enter your account settings from the top menu under your profile picture and go to the (My Added Contributions) list to add a new contribution that you are responsible for managing, do not forget to add a description for it and add it to the correct classification with the required number and wait for the rest of the members to join until it is completed, then press the (start) button at the bottom of your share.",
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'Comment ajouter une nouvelle contribution ?',
            'desc'=>"Entrez les paramètres de votre compte dans le menu du haut sous votre photo de profil et accédez à la liste (Mes contributions ajoutées) pour ajouter une nouvelle contribution que vous êtes responsable de la gestion, n'oubliez pas d'ajouter une description et de l'ajouter à la bonne classification avec le numéro requis et attendez que le reste des membres se joignent jusqu'à ce qu'il soit terminé, puis appuyez sur le bouton (démarrer) en bas de votre partage.",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'Yeni bir katkıyı nasıl eklerim?',
            'desc'=>"Profil resminizin altındaki üst menüden hesap ayarlarınızı girin ve yönetmekle sorumlu olduğunuz yeni bir katkı eklemek için (Eklenen Katkılarım) listesine gidin, ona bir açıklama eklemeyi ve doğru sınıflandırmaya eklemeyi unutmayın. gerekli numara ile ve tamamlanana kadar diğer üyelerin katılmasını bekleyin, ardından paylaşımınızın altındaki (başlat) düğmesine basın.",
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'Как мне добавить новый вклад?',
            'desc'=>"Введите настройки своей учетной записи в верхнем меню под изображением вашего профиля и перейдите в список (Мои добавленные вклады), чтобы добавить новый вклад, за управление которым вы несете ответственность, не забудьте добавить для него описание и добавить его в правильную классификацию. с нужным номером и дождитесь, пока остальные участники присоединятся, пока он не будет завершен, затем нажмите кнопку (старт) в нижней части вашей общей папки.",
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'Wie füge ich einen neuen Beitrag hinzu?',
            'desc'=>"Geben Sie Ihre Kontoeinstellungen im oberen Menü unter Ihrem Profilbild ein und gehen Sie zur Liste (Meine hinzugefügten Beiträge), um einen neuen Beitrag hinzuzufügen, für den Sie verantwortlich sind. Vergessen Sie nicht, eine Beschreibung hinzuzufügen und ihn der richtigen Klassifizierung hinzuzufügen mit der erforderlichen Nummer und warten Sie, bis die restlichen Mitglieder beigetreten sind, bis der Vorgang abgeschlossen ist, und drücken Sie dann die (Start)-Schaltfläche am unteren Rand Ihres Anteils.",
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'¿Cómo agrego una nueva contribución?',
            'desc'=>"Ingrese la configuración de su cuenta desde el menú superior debajo de su imagen de perfil y vaya a la lista (Mis contribuciones agregadas) para agregar una nueva contribución que usted es responsable de administrar, no olvide agregar una descripción y agregarla a la clasificación correcta con el número requerido y espere a que el resto de los miembros se unan hasta que se complete, luego presione el botón (iniciar) en la parte inferior de su recurso compartido.",
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'如何添加新的贡献？',
            'desc'=>"从您的个人资料图片下的顶部菜单中输入您的帐户设置，然后转到（我添加的贡献）列表添加您负责管理的新贡献，不要忘记为其添加描述并将其添加到正确的分类中并等待其他成员加入，直到完成，然后按共享底部的（开始）按钮。",
            'lang'=>'cn'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'ملاحظات اضافة مشاركة ناجحة :',
            'desc'=>'لا تنسي إضافة وصف دقيق للمشاركة حتي تشجع أفراد عائلتك ،عند الإضافة لا تقوم بأي حسابات فأنت تضع الوحدة وعدد الأفراد والمبلغ الإجمالي فقط وأترك الباقي لنا لكي نقوم بدورنا لمساعدتك ولكن حاول أن تضع أرقام أقرب إلي القسمة علي العدد لتجنب الكسور العشرية .',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'Notes for adding a successful share:',
            'desc'=>"Do not forget to add an accurate description of the participation in order to encourage your family members. When adding, do not make any calculations, you put the unit, the number of individuals and the total amount only, and leave the rest to us to do our part to help you, but try to put numbers closer to the division by the number to avoid decimals.",
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>"Remarques pour l'ajout d'un partage réussi :",
            'desc'=>"N'oubliez pas d'ajouter une description précise de la participation afin d'encourager les membres de votre famille. Lors de l'addition, ne faites aucun calcul, vous mettez l'unité, le nombre d'individus et le montant total seulement, et laissez-nous le reste faire notre part pour vous aider, mais essayez de mettre les nombres plus près de la division par le nombre pour éviter les décimales.",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'Başarılı bir paylaşım eklemek için notlar:',
            'desc'=>"Aile üyelerinizi teşvik etmek için katılımın doğru bir tanımını eklemeyi unutmayın. Toplama yaparken herhangi bir hesaplama yapmayın, sadece birimi, kişi sayısını ve toplam tutarı koyup gerisini bize yardımcı olmak için bize bırakın, ancak sayıları sayıya göre bölmeye daha yakın koymaya çalışın. ondalık sayılardan kaçınmak için.",
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'Примечания по добавлению успешной доли:',
            'desc'=>"Не забудьте добавить точное описание участия, чтобы воодушевить членов вашей семьи. При добавлении не производите никаких вычислений, вы указываете только единицу измерения, количество людей и общую сумму, а остальное оставляете нам, чтобы мы вносили свой вклад, чтобы помочь вам, но постарайтесь поставить числа ближе к делению на число чтобы избежать десятичных знаков.",
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'Hinweise zum Hinzufügen einer erfolgreichen Freigabe:',
            'desc'=>"Vergessen Sie nicht, eine genaue Beschreibung der Teilnahme hinzuzufügen, um Ihre Familienmitglieder zu ermutigen. Machen Sie beim Addieren keine Berechnungen, sondern geben Sie nur die Einheit, die Anzahl der Personen und den Gesamtbetrag an und überlassen Sie uns den Rest, um Ihnen zu helfen, aber versuchen Sie, die Zahlen näher an die Division durch die Zahl zu setzen Dezimalstellen zu vermeiden.",
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'Notas para agregar un recurso compartido exitoso:',
            'desc'=>"No olvide agregar una descripción precisa de la participación para animar a los miembros de su familia. Al sumar, no hagas ningún cálculo, pones la unidad, el número de individuos y el monto total solo, y deja el resto a nosotros para que hagamos nuestra parte para ayudarte, pero intenta acercar los números a la división por el número para evitar decimales.",
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'添加成功分享的注意事项',
            'desc'=>"不要忘记添加对参与的准确描述，以鼓励您的家人。加法的时候，不要做任何计算，你只填单位、人数和总数，剩下的交给我们来帮你做，但尽量把数字放得更接近除法以避免小数点。",
            'lang'=>'cn'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'قواعد المشاركة :',
            'desc'=>'لابد أن يتمتع المشرك بالمصداقية لإنجاح الجميع ، فلا تقوم بالمشاركة بما لا تملكه أو تستطيع المواظبة عليه بشكل دائم حتي لا تفشل المنظومة ، وتذكر دائما أن نجاح أي عمل يعتمد بشكل أساسي علينا لا علي النظام الذي نعمل عليه .',
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'Participation rules:',
            'desc'=>"The polytheist must have credibility for the success of everyone, so do not participate in what you do not own or can attend to on a permanent basis so that the system does not fail, and always remember that the success of any work depends mainly on us, not on the system we are working on.",
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'Règles de participation :',
            'desc'=>"Le polythéiste doit avoir de la crédibilité pour le succès de tout le monde, alors ne participez pas à ce que vous ne possédez pas ou ne pouvez pas vous occuper de manière permanente afin que le système n'échoue pas, et rappelez-vous toujours que le succès de tout travail dépend principalement de nous , pas sur le système sur lequel nous travaillons.",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'Katılım kuralları:',
            'desc'=>"Müşrik herkesin başarısı için inandırıcı olmalıdır, bu yüzden sistemin çökmemesi için sürekli olarak sahip olmadığınız veya ilgilenemeyeceğiniz şeylere ortak olmayın ve her işin başarısının esas olarak bize bağlı olduğunu her zaman unutmayın. , üzerinde çalıştığımız sistemde değil.",
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'Правила участия:',
            'desc'=>"Политеист должен иметь доверие к успеху каждого, поэтому не участвуйте в том, что вам не принадлежит или чем вы можете заниматься на постоянной основе, чтобы система не подводила, и всегда помните, что успех любой работы зависит главным образом от нас. , а не в системе, над которой мы работаем.",
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'Teilnahmebedingungen:',
            'desc'=>"Der Polytheist muss für den Erfolg aller glaubwürdig sein, also beteiligen Sie sich nicht an dem, was Sie nicht besitzen oder auf Dauer betreuen können, damit das System nicht versagt, und denken Sie immer daran, dass der Erfolg jeder Arbeit hauptsächlich von uns abhängt , nicht auf dem System, an dem wir arbeiten.",
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'Reglas de participación:',
            'desc'=>"El politeísta debe tener credibilidad para el éxito de todos, por eso no participe de lo que no le pertenece o pueda atender de manera permanente para que el sistema no falle, y recuerde siempre que el éxito de cualquier trabajo depende principalmente de nosotros. , no en el sistema en el que estamos trabajando.",
            'lang'=>'sp'
        ] );

        FaqsHelp::create( [
            'topic'=>'family_share',
            'topic_id'=>'5',
            'title'=>'参与规则',
            'desc'=>"多神论者必须对每个人的成功都有信誉，所以不要参与你不拥有或可以永久参与的东西，这样系统就不会失败，并且永远记住，任何工作的成功主要取决于我们，而不是在我们正在研究的系统上。",
            'lang'=>'cn'
        ] );

        FaqsHelp::create( [
            'topic'=>'personality',
            'topic_id'=>'6',
            'title'=>'مقياس مايرز بريجز',
            'desc'=>"هدف مؤشر مايرز أن يساعد الأشخاص على أن يتعرفوا على النوع الأقرب لشخصياتهم، ليكون دليل لهم لفهم أنفسهم ومحاولة تحقيق النجاح والوصول لأهدافهم، وكان المحلل النفسي كارل يونج قد صنف الشخصيات وفق الانبساط أم الإنطوائية، والاستشعار أم الحدس، التفكير أم المشاعر، وأضافت لهم مايرز الحكم أم الإدراك، ويخضع الأشخاص لاختبار- أصبح متاحا بشكل مجاني على الإنترنت- يوضح نسبة كل تصنيف داخل الشخصية الواحدة، وبناءا عليه يحدد طبيعتها.",
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'personality',
            'topic_id'=>'6',
            'title'=>'Myers-Briggs scale',
            'desc'=>"The goal of the Myers Index is to help people identify the type closest to their personality, to be a guide for them to understand themselves and try to achieve success and reach their goals. The mother of perception, and people are subjected to a test - now available free of charge on the Internet - that shows the percentage of each classification within a single personality, and accordingly determines its nature.",
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'personality',
            'topic_id'=>'6',
            'title'=>'Échelle de Myers-Briggs',
            'desc'=>"Le but de l'indice de Myers est d'aider les gens à identifier le type le plus proche de leur personnalité, d'être un guide pour qu'ils se comprennent et essaient de réussir et d'atteindre leurs objectifs.La mère de la perception, et les gens sont soumis à un test - maintenant disponible gratuitement sur Internet - qui montre le pourcentage de chaque classification au sein d'une même personnalité, et détermine en conséquence sa nature.",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'personality',
            'topic_id'=>'6',
            'title'=>'Myers-Briggs ölçeği',
            'desc'=>"Myers İndeksinin amacı, kişinin kişiliğine en yakın tipi belirlemesine yardımcı olmak, onlara kendilerini anlamaları ve başarıya ulaşmaları ve hedeflerine ulaşmaları için yol gösterici olmaktır.Algı anası ve insanlar bir teste tabi tutulur - artık internette ücretsiz olarak mevcuttur - bu, her bir sınıflandırmanın tek bir kişilik içindeki yüzdesini gösterir ve buna göre doğasını belirler.",
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'personality',
            'topic_id'=>'6',
            'title'=>'Шкала Майерс-Бриггс',
            'desc'=>"Цель индекса Майерса - помочь людям определить тип, наиболее близкий к их личности, стать для них руководством к пониманию самих себя и попыткам достичь успеха и достичь своих целей. Мать восприятия, и люди подвергаются испытанию - теперь в свободном доступе в Интернете - это показывает процентное соотношение каждой классификации в рамках одной личности и, соответственно, определяет ее характер.",
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'personality',
            'topic_id'=>'6',
            'title'=>'Myers-Briggs-Skala',
            'desc'=>"Das Ziel des Myers Index ist es, Menschen zu helfen, den Typ zu identifizieren, der ihrer Persönlichkeit am nächsten kommt, eine Anleitung für sie zu sein, sich selbst zu verstehen und zu versuchen, erfolgreich zu sein und ihre Ziele zu erreichen. Die Mutter der Wahrnehmung und die Menschen werden einem Test unterzogen - jetzt frei im Internet verfügbar - das zeigt den prozentualen Anteil jeder Klassifikation innerhalb einer einzelnen Persönlichkeit und bestimmt dementsprechend deren Natur.",
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'personality',
            'topic_id'=>'6',
            'title'=>'Escala de Myers-Briggs',
            'desc'=>"El objetivo del Índice de Myers es ayudar a las personas a identificar el tipo más cercano a su personalidad, ser una guía para que se comprendan a sí mismos y traten de lograr el éxito y alcanzar sus metas. La madre de la percepción, y las personas se someten a una prueba - ahora disponible gratuitamente en Internet, que muestra el porcentaje de cada clasificación dentro de una sola personalidad y, en consecuencia, determina su naturaleza.",
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'personality',
            'topic_id'=>'6',
            'title'=>'迈尔斯-布里格斯量表',
            'desc'=>"迈尔斯指数的目标是帮助人们识别与自己性格最接近的类型，成为他们了解自己并努力实现成功和实现目标的指南。感知之母，人们接受一项测试——现在可以在 Internet 上免费获得——它显示了单个人格中每个分类的百分比，并相应地确定了它的性质。",
            'lang'=>'cn'
        ] );

        FaqsHelp::create( [
            'topic'=>'personality',
            'topic_id'=>'6',
            'title'=>'تفاصيل الإختبار',
            'desc'=>"الإختبار عبارة عن ٧٠ سؤال إجباري لإمكانية تحديد نوع شخصيتك بكل دقة ويمكنك إنهاؤه علي مدار عدة مرات وليس من الضرورة إنهاؤه لحظة البدء ولن تظهر تفاصيل شخصيتك إلا بعد الإنتهاء من كل الأسئلة .",
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'personality',
            'topic_id'=>'6',
            'title'=>'Test details',
            'desc'=>"The test consists of 70 compulsory questions for the ability to accurately determine your personality type, and you can finish it over several times.",
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'personality',
            'topic_id'=>'6',
            'title'=>'Détails des tests',
            'desc'=>"Le test se compose de 70 questions obligatoires pour la capacité de déterminer avec précision votre type de personnalité, et vous pouvez le terminer plusieurs fois.",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'personality',
            'topic_id'=>'6',
            'title'=>'Test ayrıntıları',
            'desc'=>"Test, kişilik tipinizi doğru bir şekilde belirleme yeteneği için 70 zorunlu sorudan oluşur ve birkaç kez bitirebilirsiniz.",
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'personality',
            'topic_id'=>'6',
            'title'=>'Детали теста',
            'desc'=>"Тест состоит из 70 обязательных вопросов на умение точно определить свой тип личности, и вы можете пройти его несколько раз.",
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'personality',
            'topic_id'=>'6',
            'title'=>'Testdetails',
            'desc'=>"Der Test besteht aus 70 Pflichtfragen zur genauen Bestimmung Ihres Persönlichkeitstyps und kann mehrmals absolviert werden.",
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'personality',
            'topic_id'=>'6',
            'title'=>'Detalles de la prueba',
            'desc'=>"La prueba consta de 70 preguntas obligatorias para la capacidad de determinar con precisión su tipo de personalidad, y puede terminarla varias veces.",
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'personality',
            'topic_id'=>'6',
            'title'=>'测试详情',
            'desc'=>"该测试包含 七十 道必考题，用于准确判断您的性格类型，您可以多次完成。",
            'lang'=>'cn'
        ] );

        FaqsHelp::create( [
            'topic'=>'personality',
            'topic_id'=>'6',
            'title'=>'دقة إختياراتي',
            'desc'=>"يجب عليك الإجابة عل إختيار واحد من الإجابتين المتاحين ، لابد أن تتأكد من الأقرب منهم إليك بعد فهمك الجيد للسؤال",
            'lang'=>'ar'
        ] );

        FaqsHelp::create( [
            'topic'=>'personality',
            'topic_id'=>'6',
            'title'=>'Accuracy of my choices',
            'desc'=>"You must answer the choice of one of the two available answers, you must make sure of the one closest to you after you have a good understanding of the question",
            'lang'=>'en'
        ] );

        FaqsHelp::create( [
            'topic'=>'personality',
            'topic_id'=>'6',
            'title'=>"Vous devez répondre au choix de l'une des deux réponses disponibles, vous devez vous assurer de celle la plus proche de vous après avoir bien compris la question",
            'lang'=>'fr'
        ] );

        FaqsHelp::create( [
            'topic'=>'personality',
            'topic_id'=>'6',
            'title'=>'Seçimlerimin doğruluğu',
            'desc'=>"Mevcut iki cevaptan birini seçmelisiniz, soruyu iyice anladıktan sonra size en yakın olandan emin olmalısınız.",
            'lang'=>'tr'
        ] );

        FaqsHelp::create( [
            'topic'=>'personality',
            'topic_id'=>'6',
            'title'=>'Точность моего выбора',
            'desc'=>"Вы должны ответить на выбор одного из двух доступных ответов, вы должны выбрать наиболее близкий вам после того, как хорошо поймете вопрос",
            'lang'=>'ru'
        ] );

        FaqsHelp::create( [
            'topic'=>'personality',
            'topic_id'=>'6',
            'title'=>'Genauigkeit meiner Entscheidungen',
            'desc'=>"Sie müssen die Auswahl einer der beiden verfügbaren Antworten beantworten, Sie müssen sich vergewissern, dass die Ihnen am nächsten liegt, nachdem Sie die Frage gut verstanden haben",
            'lang'=>'de'
        ] );

        FaqsHelp::create( [
            'topic'=>'personality',
            'topic_id'=>'6',
            'title'=>'Precisión de mis elecciones',
            'desc'=>"Debe responder a la elección de una de las dos respuestas disponibles, debe asegurarse de la más cercana a usted después de tener una buena comprensión de la pregunta",
            'lang'=>'es'
        ] );

        FaqsHelp::create( [
            'topic'=>'personality',
            'topic_id'=>'6',
            'title'=>'我的选择的准确性',
            'desc'=>"您必须从两个可用答案中选择一个来回答，您必须在充分理解问题后确定离您最近的那个",
            'lang'=>'cn'
        ] );

        
    }
}
