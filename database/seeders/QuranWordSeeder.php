<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuranWordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Quran words table start seed!');

        $path = 'database/sql/quran_words.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Quran words table seeded ok!');
    }
}
