<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuranSurahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Quran Surah table start seed!');

        $path = 'database/sql/quran_surah.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Quran Surah table seeded ok!');
    }
}
