<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\personTypes;
class PersonTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'1',
            'title'=>'Portrait of an ENFJ - Extraverted iNtuitive Feeling Judging (Extraverted Feeling with Introverted Intuition) The Giver',
            'desc'=>'',
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'1',
            'title'=>'صورة شخصية لـ ENFJ - الحكم على الشعور الخارجي المقلوب (الشعور الخارج عن الحدس الانطوائي) المعطي',
            'desc'=>'',
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'1',
            'title'=>"Portrait d'un ENFJ - Jugement du sentiment intuitif extraverti (Sentiment extraverti avec intuition introvertie)",
            'desc'=>'',
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'1',
            'title'=>"Bir ENFJ'nin Portresi - Dışa Dönük Sezgisel Duygu Yargılama (İçe Dönük Sezgiyle Dışa Dönük Duygu) Verici",
            'desc'=>'',
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'1',
            'title'=>"Портрет ENFJ - Экстравертное интуитивное суждение с чувством (экстравертное чувство с интровертной интуицией) Дающий",
            'desc'=>'',
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'1',
            'title'=>"",
            'desc'=>'Porträt eines ENFJ - Extravertiertes intuitives Gefühlsurteilen (Extravertiertes Gefühl mit introvertierter Intuition) Der Geber',
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'1',
            'title'=>"Retrato de un ENFJ - Juicio de sentimiento intuitivo extravertido (sentimiento extravertido con intuición introvertida) El dador",
            'desc'=>'',
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'1',
            'title'=>"ENFJ 的肖像 - 外倾直觉判断（外倾直觉与内倾直觉）给予者",
            'desc'=>'',
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"As an ENFJ, you're primary mode of living is focused externally, where you deal with things according to how you feel about them, or how they fit into your personal value system. Your secondary mode is internal, where you take things in primarily via your intuition.
ENFJs are people-focused individuals. They live in the world of people possibilities. More so than any other type, they have excellent people skills. They understand and care about people, and have a special talent for bringing out the best in others. ENFJ's main interest in life is giving love, support, and a good time to other people. They are focused on understanding, supporting, and encouraging others. They make things happen for people, and get their best personal satisfaction from this.",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"بصفتك ENFJ ، فإن نمط حياتك الأساسي يركز على الخارج ، حيث تتعامل مع الأشياء وفقًا لما تشعر به تجاهها ، أو كيف تتناسب مع نظام القيم الشخصية الخاص بك. وضعك الثانوي داخلي ، حيث تأخذ الأمور في المقام الأول من خلال حدسك.
ENFJs هم أفراد يركزون على الناس. إنهم يعيشون في عالم إمكانيات الناس. أكثر من أي نوع آخر ، لديهم مهارات ممتازة في التعامل مع الأشخاص. إنهم يفهمون الناس ويهتمون بهم ، ولديهم موهبة خاصة لإخراج الأفضل في الآخرين. اهتمام ENFJ الرئيسي في الحياة هو إعطاء الحب ، والدعم ، وقضاء وقت ممتع للآخرين. إنهم يركزون على فهم الآخرين ودعمهم وتشجيعهم. إنهم يجعلون الأشياء تحدث للناس ، ويحصلون على أفضل إرضاء شخصي لهم من هذا.",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"En tant qu'ENFJ, votre mode de vie principal est axé sur l'extérieur, où vous gérez les choses en fonction de ce que vous en pensez ou de la façon dont elles s'intègrent dans votre système de valeurs personnel. Votre mode secondaire est interne, où vous assimilez les choses principalement via votre intuition.
Les ENFJ sont des individus centrés sur les personnes. Ils vivent dans le monde des possibilités humaines. Plus que tout autre type, ils ont d'excellentes aptitudes relationnelles. Ils comprennent et se soucient des gens, et ont un talent particulier pour faire ressortir le meilleur des autres. Le principal intérêt d'ENFJ dans la vie est de donner de l'amour, du soutien et du bon temps aux autres. Ils se concentrent sur la compréhension, le soutien et l'encouragement des autres. Ils font bouger les choses pour les gens et en tirent leur meilleure satisfaction personnelle.",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Bir ENFJ olarak, birincil yaşam tarzınız dışsal olarak odaklanır, olaylarla onlar hakkında nasıl hissettiğinize veya kişisel değer sisteminize nasıl uyduklarına göre ilgilenirsiniz. İkincil modunuz içseldir, burada işleri öncelikle sezginiz aracılığıyla alırsınız.
ENFJ'ler insan odaklı bireylerdir. İnsanların olasılıklar dünyasında yaşıyorlar. Diğer türlerden daha çok, mükemmel insan becerilerine sahiptirler. İnsanları anlıyor ve önemsiyorlar ve başkalarının içindeki en iyiyi ortaya çıkarmak için özel bir yeteneğe sahipler. ENFJ'in hayattaki ana ilgisi, diğer insanlara sevgi, destek ve iyi vakit geçirmektir. Başkalarını anlamaya, desteklemeye ve teşvik etmeye odaklanırlar. İnsanlar için bir şeyler olmasını sağlarlar ve bundan en iyi kişisel tatminlerini alırlar.",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Как ENFJ, ваш основной образ жизни сфокусирован на внешнем, где вы решаете вещи в соответствии с тем, как вы к ним относитесь или как они вписываются в вашу личную систему ценностей. Ваш вторичный режим - внутренний, в котором вы воспринимаете вещи в первую очередь через интуицию.
ENFJ - люди, ориентированные на людей. Они живут в мире возможностей людей. В большей степени, чем любой другой тип, они обладают отличными навыками работы с людьми. Они понимают людей и заботятся о них, а также обладают особым талантом выявлять лучшее в других. Главный интерес ENFJ в жизни - дарить другим людям любовь, поддержку и хорошо проводить время. Они сосредоточены на понимании, поддержке и ободрении других. Они делают вещи реальностью для людей и получают от этого лучшее личное удовлетворение.",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Als ENFJ ist Ihre primäre Lebensweise nach außen gerichtet, wo Sie mit den Dingen so umgehen, wie Sie sie empfinden oder wie sie in Ihr persönliches Wertesystem passen. Ihr sekundärer Modus ist intern, wo Sie die Dinge hauptsächlich über Ihre Intuition aufnehmen.
ENFJs sind menschenorientierte Individuen. Sie leben in der Welt der Möglichkeiten der Menschen. Mehr als jeder andere Typ haben sie ausgezeichnete Menschenkenntnis. Sie verstehen und kümmern sich um Menschen und haben ein besonderes Talent, das Beste aus anderen herauszuholen. Das Hauptinteresse von ENFJ im Leben besteht darin, anderen Menschen Liebe, Unterstützung und eine gute Zeit zu schenken. Sie konzentrieren sich darauf, andere zu verstehen, zu unterstützen und zu ermutigen. Sie machen Dinge für Menschen möglich und ziehen daraus ihre beste persönliche Befriedigung.",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Como ENFJ, su modo de vida principal se centra en el exterior, donde se ocupa de las cosas de acuerdo con lo que siente por ellas o cómo encajan en su sistema de valores personal. Tu modo secundario es interno, donde tomas las cosas principalmente a través de tu intuición.
Los ENFJ son individuos centrados en las personas. Viven en el mundo de las posibilidades de las personas. Más que cualquier otro tipo, tienen excelentes habilidades con las personas. Entienden y se preocupan por las personas, y tienen un talento especial para sacar lo mejor de los demás. El principal interés de ENFJ en la vida es brindar amor, apoyo y un buen momento a otras personas. Están enfocados en comprender, apoyar y alentar a los demás. Hacen que las cosas sucedan para las personas y obtienen su mejor satisfacción personal de esto.",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"作为一名 ENFJ，您的主要生活方式是关注外部事物，根据您对事物的感受或它们如何融入您的个人价值体系来处理事物。你的次要模式是内在的，你主要通过直觉来接受事物。
ENFJ 是以人为本的个体。他们生活在充满可能性的世界中。他们比任何其他类型的人都具有出色的人际交往能力。他们理解并关心他人，并具有让他人发挥最佳才能的特殊才能。 ENFJ 对生活的主要兴趣是给予他人爱、支持和美好时光。他们专注于理解、支持和鼓励他人。他们让事情发生在人们身上，并从中获得最大的个人满足。",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Because ENFJ's people skills are so extraordinary, they have the ability to make people do exactly what they want them to do. They get under people's skins and get the reactions that they are seeking. ENFJ's motives are usually unselfish, but ENFJs who have developed less than ideally have been known to use their power over people to manipulate them.",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"نظرًا لأن مهارات الأشخاص في ENFJ غير عادية جدًا ، فإن لديهم القدرة على جعل الناس يفعلون ما يريدون منهم القيام به بالضبط. إنهم يحصلون على ردود الفعل التي يبحثون عنها. عادة ما تكون دوافع ENFJ غير أنانية ، ولكن من المعروف أن ENFJs الذين طوروا أقل من مثالي يستخدمون قوتهم على الناس للتلاعب بهم.",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Parce que les compétences relationnelles d'ENFJ sont si extraordinaires, ils ont la capacité de faire en sorte que les gens fassent exactement ce qu'ils veulent qu'ils fassent. Ils pénètrent dans la peau des gens et obtiennent les réactions qu'ils recherchent. Les motivations des ENFJ sont généralement altruistes, mais les ENFJ qui se sont développés moins qu'idéalement sont connus pour utiliser leur pouvoir sur les gens pour les manipuler.",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJ'nin insan becerileri olağanüstü olduğundan, insanlara tam olarak istedikleri şeyi yapma becerisine sahiptirler. İnsanların kılığına giriyorlar ve aradıkları tepkileri alıyorlar. ENFJ'in amaçları genellikle bencil değildir, ancak idealden daha az gelişmiş ENFJ'lerin, insanları manipüle etmek için insanlar üzerindeki güçlerini kullandıkları bilinmektedir.",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Поскольку человеческие навыки ENFJ настолько необычны, у них есть способность заставлять людей делать именно то, что они от них хотят. Они проникают под кожу людей и получают реакцию, которую ищут. Мотивы ENFJ обычно бескорыстны, но, как известно, ENFJ, которые развились хуже, чем в идеале, использовали свою власть над людьми, чтобы манипулировать ими.",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Da ENFJs soziale Fähigkeiten so außergewöhnlich sind, haben sie die Fähigkeit, Menschen dazu zu bringen, genau das zu tun, was sie von ihnen wollen. Sie gehen den Leuten unter die Haut und bekommen die Reaktionen, die sie suchen. Die Motive von ENFJ sind normalerweise selbstlos, aber ENFJs, die sich weniger als ideal entwickelt haben, sind dafür bekannt, ihre Macht über Menschen zu nutzen, um sie zu manipulieren.",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Debido a que las habilidades de ENFJ con las personas son tan extraordinarias, tienen la capacidad de hacer que las personas hagan exactamente lo que quieren que hagan. Se meten bajo la piel de las personas y obtienen las reacciones que buscan. Los motivos de ENFJ suelen ser desinteresados, pero se sabe que los ENFJ que se han desarrollado menos de lo ideal utilizan su poder sobre las personas para manipularlas.",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"因为 ENFJ 的人际交往能力如此非凡，他们有能力让人们做他们想做的事。他们深入人们的内心并得到他们正在寻求的反应。 ENFJ 的动机通常是无私的，但众所周知，发展不理想的 ENFJ 会利用他们对人的权力来操纵他们。",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJ's are so externally focused that it's especially important for them to spend time alone. This can be difficult for some ENFJs, because they have the tendency to be hard on themselves and turn to dark thoughts when alone. Consequently, ENFJs might avoid being alone, and fill their lives with activities involving other people. ENFJs tend to define their life's direction and priorities according to other people's needs, and may not be aware of their own needs. It's natural to their personality type that they will tend to place other people's needs above their own, but they need to stay aware of their own needs so that they don't sacrifice themselves in their drive to help others.",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"تركز ENFJ على الخارج لدرجة أنه من المهم بشكل خاص بالنسبة لهم قضاء الوقت بمفردهم. قد يكون هذا صعبًا بالنسبة لبعض ENFJs ، لأنهم يميلون إلى أن يكونوا قاسين على أنفسهم ويلجأون إلى الأفكار المظلمة عندما يكونون بمفردهم. وبالتالي ، قد يتجنب ENFJs البقاء بمفرده ، ويملأ حياتهم بأنشطة تشمل أشخاصًا آخرين. تميل ENFJs إلى تحديد اتجاه حياتهم وأولوياتهم وفقًا لاحتياجات الآخرين ، وقد لا يكونوا على دراية باحتياجاتهم الخاصة. من الطبيعي بالنسبة لنوع شخصيتهم أنهم يميلون إلى وضع احتياجات الآخرين فوق احتياجاتهم ، لكنهم بحاجة إلى البقاء على دراية باحتياجاتهم الخاصة حتى لا يضحوا بأنفسهم في دافعهم لمساعدة الآخرين.",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Les ENFJ sont tellement tournés vers l'extérieur qu'il est particulièrement important pour eux de passer du temps seuls. Cela peut être difficile pour certains ENFJ, car ils ont tendance à être durs avec eux-mêmes et à avoir des pensées noires lorsqu'ils sont seuls. Par conséquent, les ENFJ pourraient éviter d'être seuls et remplir leur vie d'activités impliquant d'autres personnes. Les ENFJ ont tendance à définir la direction et les priorités de leur vie en fonction des besoins des autres et peuvent ne pas être conscients de leurs propres besoins. Il est naturel à leur type de personnalité qu'ils aient tendance à placer les besoins des autres au-dessus des leurs, mais ils doivent rester conscients de leurs propres besoins afin de ne pas se sacrifier dans leur volonté d'aider les autres.",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJ'ler o kadar dışa odaklıdır ki, yalnız zaman geçirmeleri özellikle önemlidir. Bu, bazı ENFJ'ler için zor olabilir, çünkü kendilerine karşı katı olma ve yalnız olduklarında karanlık düşüncelere dönme eğilimi gösterirler. Sonuç olarak, ENFJ'ler yalnız kalmaktan kaçınabilir ve hayatlarını diğer insanları içeren aktivitelerle doldurabilir. ENFJ'ler yaşamlarının yönünü ve önceliklerini diğer insanların ihtiyaçlarına göre belirleme eğilimindedir ve kendi ihtiyaçlarının farkında olmayabilirler. Diğer insanların ihtiyaçlarını kendi ihtiyaçlarının önüne koyma eğiliminde olmaları kişilik tiplerine göre doğaldır, ancak başkalarına yardım etme çabalarında kendilerini feda etmemeleri için kendi ihtiyaçlarının farkında olmaları gerekir.",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJ настолько сосредоточены на внешнем мире, что для них особенно важно проводить время в одиночестве. Это может быть трудным для некоторых ENFJ, потому что они склонны быть суровыми к себе и обращаются к темным мыслям в одиночестве. Следовательно, ENFJ могут избегать одиночества и наполнить свою жизнь деятельностью с участием других людей. ENFJ склонны определять направление своей жизни и приоритеты в соответствии с потребностями других людей и могут не осознавать свои собственные потребности. Для их типа личности естественно, что они склонны ставить потребности других людей выше своих собственных, но им нужно осознавать свои собственные потребности, чтобы они не жертвовали собой в своем стремлении помочь другим.",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJs sind so nach außen orientiert, dass es für sie besonders wichtig ist, Zeit allein zu verbringen. Dies kann für einige ENFJs schwierig sein, da sie dazu neigen, hart zu sich selbst zu sein und sich dunklen Gedanken zuwenden, wenn sie allein sind. Folglich könnten ENFJs es vermeiden, allein zu sein, und ihr Leben mit Aktivitäten füllen, an denen andere Menschen beteiligt sind. ENFJs neigen dazu, ihre Lebensrichtung und Prioritäten nach den Bedürfnissen anderer Menschen zu definieren und sind sich ihrer eigenen Bedürfnisse möglicherweise nicht bewusst. Es liegt in ihrem Persönlichkeitstyp, dass sie dazu neigen, die Bedürfnisse anderer Menschen über ihre eigenen zu stellen, aber sie müssen sich ihrer eigenen Bedürfnisse bewusst bleiben, damit sie sich nicht in ihrem Drang opfern, anderen zu helfen.",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Los ENFJ están tan enfocados en el exterior que es especialmente importante para ellos pasar tiempo solos. Esto puede ser difícil para algunos ENFJ, porque tienen la tendencia a ser duros consigo mismos y a tener pensamientos oscuros cuando están solos. En consecuencia, los ENFJ podrían evitar estar solos y llenar sus vidas con actividades que involucren a otras personas. Los ENFJ tienden a definir la dirección y las prioridades de su vida de acuerdo con las necesidades de otras personas y pueden no ser conscientes de sus propias necesidades. Es natural para su tipo de personalidad que tenderán a colocar las necesidades de otras personas por encima de las suyas, pero deben ser conscientes de sus propias necesidades para no sacrificarse en su impulso por ayudar a los demás.",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJ 非常注重外在，因此花时间独处对他们来说尤为重要。这对一些 ENFJ 来说可能很困难，因为他们倾向于对自己苛刻，并在独自一人时转向黑暗的想法。因此，ENFJ 可能会避免独处，并通过与其他人有关的活动来充实他们的生活。 ENFJ 倾向于根据其他人的需要来定义他们的生活方向和优先事项，并且可能没有意识到自己的需要。对于他们的性格类型来说，他们倾向于将他人的需求置于自己的需求之上是很自然的，但他们需要时刻注意自己的需求，以免在帮助他人的过程中牺牲自己。",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJ's tend to be more reserved about exposing themselves than other extraverted types. Although they may have strongly-felt beliefs, they're likely to refrain from expressing them if doing so would interfere with bringing out the best in others. Because their strongest interest lies in being a catalyst of change in other people, they're likely to interact with others on their own level, in a chameleon-like manner, rather than as individuals.",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"تميل ENFJ إلى أن تكون أكثر تحفظًا بشأن تعريض نفسها من الأنواع الأخرى الخارجة. على الرغم من أنه قد يكون لديهم معتقدات راسخة ، فمن المحتمل أن يمتنعوا عن التعبير عنها إذا كان ذلك من شأنه أن يتعارض مع إبراز أفضل ما لدى الآخرين. نظرًا لأن اهتمامهم الأقوى يكمن في كونهم محفزًا للتغيير لدى الآخرين ، فمن المحتمل أن يتفاعلوا مع الآخرين على مستواهم الخاص ، بطريقة تشبه الحرباء ، وليس كأفراد.",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Les ENFJ ont tendance à être plus réservés à s'exposer que les autres types extravertis. Bien qu'ils puissent avoir des croyances bien ancrées, ils sont susceptibles de s'abstenir de les exprimer si cela les empêche de faire ressortir le meilleur des autres. Parce que leur plus grand intérêt réside dans le fait d'être un catalyseur de changement chez les autres, ils sont susceptibles d'interagir avec les autres à leur propre niveau, à la manière d'un caméléon, plutôt qu'en tant qu'individus.",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJ'ler, kendilerini diğer dışa dönük türlerden daha fazla ifşa etme konusunda daha çekingen olma eğilimindedir. Güçlü bir şekilde hissedilen inançlara sahip olsalar da, başkalarının içindeki en iyiyi ortaya çıkarmaya engel olacaksa, bunları ifade etmekten kaçınmaları muhtemeldir. En büyük ilgileri diğer insanlarda değişimin katalizörü olmak olduğu için, başkalarıyla bireylerden ziyade kendi seviyelerinde, bukalemun benzeri bir şekilde etkileşime girmeleri muhtemeldir.",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJ, как правило, более сдержанны в раскрытии себя, чем другие экстравертные типы. Хотя у них могут быть твердые убеждения, они, скорее всего, воздержатся от их выражения, если это помешает выявить лучшее в других. Поскольку их самый большой интерес заключается в том, чтобы быть катализатором изменений в других людях, они, вероятно, будут взаимодействовать с другими на своем собственном уровне, как хамелеоны, а не как личности.",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJs neigen dazu, sich selbst zu enthüllen, zurückhaltender als andere extravertierte Typen. Obwohl sie starke Überzeugungen haben, werden sie wahrscheinlich davon absehen, diese auszudrücken, wenn dies das Beste aus anderen herausholen würde. Da ihr stärkstes Interesse darin besteht, ein Katalysator für Veränderungen bei anderen zu sein, interagieren sie wahrscheinlich eher auf ihrer eigenen Ebene mit anderen, chamäleonartig als als Individuen.",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Los ENFJ tienden a ser más reservados acerca de exponerse que otros tipos extravertidos. Aunque pueden tener creencias muy arraigadas, es probable que se abstengan de expresarlas si hacerlo interferiría con sacar lo mejor de los demás. Debido a que su mayor interés radica en ser un catalizador del cambio en otras personas, es probable que interactúen con los demás a su propio nivel, de una manera camaleónica, en lugar de como individuos.",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"与其他外向型相比，ENFJ 往往对暴露自己更加保守。尽管他们可能有强烈的信念，但如果这样做会妨碍他人发挥最佳效果，他们可能会克制自己。因为他们最大的兴趣在于成为其他人变化的催化剂，所以他们可能会在自己的层面上与他人互动，以变色龙般的方式，而不是作为个人。",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJs have definite values and opinions which they're able to express clearly and succinctly. These beliefs will be expressed as long as they're not too personal. ENFJ is in many ways expressive and open, but is more focused on being responsive and supportive of others. When faced with a conflict between a strongly- held value and serving another person's need, they are highly likely to value the other person's needs.
The ENFJ may feel quite lonely even when surrounded by people. This feeling of aloneness may be exacerbated by the tendency to not reveal their true selves.",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"تتمتع ENFJs بقيم وآراء محددة يمكنهم التعبير عنها بوضوح وإيجاز. سيتم التعبير عن هذه المعتقدات طالما أنها ليست شخصية للغاية. ENFJ معبرة ومنفتحة من نواح كثيرة ، لكنها تركز بشكل أكبر على الاستجابة والدعم للآخرين. عند مواجهة تضارب بين قيمة راسخة وخدمة احتياجات شخص آخر ، فمن المرجح أن يقدّروا احتياجات الشخص الآخر.
قد تشعر ENFJ بالوحدة الشديدة حتى عندما تكون محاطة بالناس. قد يتفاقم هذا الشعور بالوحدة بسبب الميل إلى عدم الكشف عن ذواتهم الحقيقية.",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Les ENFJ ont des valeurs et des opinions précises qu'ils sont capables d'exprimer de manière claire et succincte. Ces croyances seront exprimées tant qu'elles ne sont pas trop personnelles. ENFJ est à bien des égards expressif et ouvert, mais se concentre davantage sur la réactivité et le soutien des autres. Lorsqu'ils sont confrontés à un conflit entre une valeur fortement ancrée et répondre aux besoins d'une autre personne, ils sont très susceptibles de valoriser les besoins de l'autre personne.
L'ENFJ peut se sentir assez seul même lorsqu'il est entouré de gens. Ce sentiment de solitude peut être exacerbé par la tendance à ne pas révéler leur vrai moi.",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJ'lerin açık ve özlü bir şekilde ifade edebilecekleri kesin değerleri ve görüşleri vardır. Bu inançlar çok kişisel olmadığı sürece ifade edilecektir. ENFJ birçok yönden etkileyici ve açıktır, ancak daha çok başkalarına duyarlı ve destekleyici olmaya odaklanmıştır. Güçlü bir şekilde sahiplenilen bir değer ile başka bir kişinin ihtiyacına hizmet etme arasında bir çatışma ile karşı karşıya kaldıklarında, diğer kişinin ihtiyaçlarına değer verme olasılıkları yüksektir.
ENFJ, etrafı insanlarla çevriliyken bile oldukça yalnız hissedebilir. Bu yalnızlık hissi, gerçek benliklerini ortaya çıkarmama eğilimi ile daha da kötüleşebilir.",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"У ENFJ есть определенные ценности и мнения, которые они могут выразить ясно и лаконично. Эти убеждения будут выражаться, если они не будут слишком личными. ENFJ во многих отношениях выразителен и открыт, но больше ориентирован на то, чтобы быть отзывчивым и поддерживать других. Столкнувшись с конфликтом между устоявшейся ценностью и удовлетворением потребностей другого человека, они, скорее всего, будут ценить потребности другого человека.
ENFJ может чувствовать себя одиноким даже в окружении людей. Это чувство одиночества может усугубляться склонностью не раскрывать свою истинную сущность.",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJs haben bestimmte Werte und Meinungen, die sie klar und prägnant ausdrücken können. Diese Überzeugungen werden zum Ausdruck gebracht, solange sie nicht zu persönlich sind. ENFJ ist in vielerlei Hinsicht ausdrucksstark und offen, konzentriert sich jedoch mehr darauf, auf andere einzugehen und sie zu unterstützen. Wenn sie mit einem Konflikt zwischen einem hochgehaltenen Wert und der Befriedigung der Bedürfnisse einer anderen Person konfrontiert sind, schätzen sie mit hoher Wahrscheinlichkeit die Bedürfnisse der anderen Person.
Der ENFJ kann sich sogar in der Nähe von Menschen ziemlich einsam fühlen. Dieses Gefühl des Alleinseins kann durch die Tendenz verstärkt werden, ihr wahres Selbst nicht zu offenbaren.",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Los ENFJ tienen valores y opiniones definidos que pueden expresar de manera clara y sucinta. Estas creencias se expresarán siempre que no sean demasiado personales. ENFJ es en muchos sentidos expresivo y abierto, pero está más enfocado en responder y apoyar a los demás. Cuando se enfrentan a un conflicto entre un valor muy arraigado y la satisfacción de las necesidades de otra persona, es muy probable que valoren las necesidades de la otra persona.
El ENFJ puede sentirse bastante solo incluso cuando está rodeado de gente. Este sentimiento de soledad puede verse exacerbado por la tendencia a no revelar su verdadero yo.",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJs 有明确的价值观和观点，他们能够清晰简洁地表达。只要这些信念不是太个人化，就会被表达出来。 ENFJ 在很多方面都具有表现力和开放性，但更注重对他人的反应和支持。当面临强烈持有的价值观与满足他人需求之间的冲突时，他们很可能会重视他人的需求。
即使被人包围，ENFJ 也可能会感到很孤独。这种孤独感可能会因不愿透露真实自我的倾向而加剧。",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"People love ENFJs. They are fun to be with, and truly understand and love people. They are typically very straight-forward and honest. Usually ENFJs exude a lot of self-confidence, and have a great amount of ability to do many different things. They are generally bright, full of potential, energetic and fast-paced. They are usually good at anything which captures their interest.
",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"يحب الناس ENFJs. من الممتع أن يكونوا مع الناس ويفهمون الناس ويحبونهم حقًا. هم عادة ما يكونون صريحين وصادقين للغاية. عادةً ما تنضح ENFJs كثيرًا من الثقة بالنفس ، ولديها قدر كبير من القدرة على القيام بالعديد من الأشياء المختلفة. فهي بشكل عام مشرقة ومليئة بالإمكانيات وحيوية وسريعة الإيقاع. عادة ما يكونون جيدين في أي شيء يجذب اهتمامهم.",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Les gens adorent les ENFJ. Ils sont amusants à côtoyer, et comprennent et aiment vraiment les gens. Ils sont généralement très simples et honnêtes. Habituellement, les ENFJ dégagent beaucoup de confiance en eux et ont une grande capacité à faire beaucoup de choses différentes. Ils sont généralement brillants, pleins de potentiel, énergiques et rapides. Ils sont généralement bons dans tout ce qui capte leur intérêt.",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"İnsanlar ENFJ'leri sever. Onlarla birlikte olmak eğlencelidir ve insanları gerçekten anlar ve severler. Genellikle çok açık sözlü ve dürüsttürler. Genellikle ENFJ'ler çok fazla özgüven yayar ve birçok farklı şeyi yapma konusunda büyük bir yeteneğe sahiptir. Genellikle parlak, potansiyel dolu, enerjik ve hızlıdırlar. Genellikle ilgilerini çeken her şeyde iyidirler.",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Людям нравятся ENFJ. С ними весело, они искренне понимают и любят людей. Обычно они очень прямолинейны и честны. Обычно ENFJ источают много уверенности в себе и обладают огромными способностями делать много разных вещей. Как правило, они ярки, полны потенциала, энергичны и стремительны. Обычно они хороши во всем, что вызывает их интерес.",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Die Leute lieben ENFJs. Es macht Spaß, mit ihnen zusammen zu sein, und sie verstehen und lieben die Menschen wirklich. Sie sind in der Regel sehr direkt und ehrlich. Normalerweise strahlen ENFJs viel Selbstvertrauen aus und haben eine große Fähigkeit, viele verschiedene Dinge zu tun. Sie sind im Allgemeinen hell, voller Potenzial, energisch und schnelllebig. Sie sind normalerweise gut in allem, was ihr Interesse weckt.",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"A la gente le encantan los ENFJ. Es divertido estar con ellos y realmente entienden y aman a las personas. Por lo general, son muy directos y honestos. Por lo general, los ENFJ exudan mucha confianza en sí mismos y tienen una gran capacidad para hacer muchas cosas diferentes. Generalmente son brillantes, llenos de potencial, enérgicos y de ritmo rápido. Suelen ser buenos en cualquier cosa que capte su interés.",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"人们喜欢 ENFJ。和他们在一起很有趣，并且真正理解和爱人。他们通常非常直接和诚实。通常ENFJ型人散发出很大的自信，并且有很强的能力去做很多不同的事情。他们一般都很聪明，充满潜力，精力充沛，节奏很快。他们通常擅长任何能引起他们兴趣的事情。",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJs like for things to be well-organized, and will work hard at maintaining structure and resolving ambiguity. They have a tendency to be fussy, especially with their home environments.
In the work place, ENFJs do well in positions where they deal with people. They are naturals for the social committee. Their uncanny ability to understand people and say just what needs to be said to make them happy makes them naturals for counseling. They enjoy being the center of attention, and do very well in situations where they can inspire and lead others, such as teaching.",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"يحب الناس ENFJs. من الممتع أن يكونوا مع الناس ويفهمون الناس ويحبونهم حقًا. هم عادة ما يكونون صريحين وصادقين للغاية. عادةً ما تنضح ENFJs كثيرًا من الثقة بالنفس ، ولديها قدر كبير من القدرة على القيام بالعديد من الأشياء المختلفة. فهي بشكل عام مشرقة ومليئة بالإمكانيات وحيوية وسريعة الإيقاع. عادة ما يكونون جيدين في أي شيء يجذب اهتمامهم.",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Les gens adorent les ENFJ. Ils sont amusants à côtoyer, et comprennent et aiment vraiment les gens. Ils sont généralement très simples et honnêtes. Habituellement, les ENFJ dégagent beaucoup de confiance en eux et ont une grande capacité à faire beaucoup de choses différentes. Ils sont généralement brillants, pleins de potentiel, énergiques et rapides. Ils sont généralement bons dans tout ce qui capte leur intérêt.",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"İnsanlar ENFJ'leri sever. Onlarla birlikte olmak eğlencelidir ve insanları gerçekten anlar ve severler. Genellikle çok açık sözlü ve dürüsttürler. Genellikle ENFJ'ler çok fazla özgüven yayar ve birçok farklı şeyi yapma konusunda büyük bir yeteneğe sahiptir. Genellikle parlak, potansiyel dolu, enerjik ve hızlıdırlar. Genellikle ilgilerini çeken her şeyde iyidirler.",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Людям нравятся ENFJ. С ними весело, они искренне понимают и любят людей. Обычно они очень прямолинейны и честны. Обычно ENFJ источают много уверенности в себе и обладают огромными способностями делать много разных вещей. Как правило, они ярки, полны потенциала, энергичны и стремительны. Обычно они хороши во всем, что вызывает их интерес.",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Die Leute lieben ENFJs. Es macht Spaß, mit ihnen zusammen zu sein, und sie verstehen und lieben die Menschen wirklich. Sie sind in der Regel sehr direkt und ehrlich. Normalerweise strahlen ENFJs viel Selbstvertrauen aus und haben eine große Fähigkeit, viele verschiedene Dinge zu tun. Sie sind im Allgemeinen hell, voller Potenzial, energisch und schnelllebig. Sie sind normalerweise gut in allem, was ihr Interesse weckt.",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"A la gente le encantan los ENFJ. Es divertido estar con ellos, y realmente entienden y aman a las personas. Por lo general, son muy directos y honestos. Por lo general, los ENFJ exudan mucha confianza en sí mismos y tienen una gran capacidad para hacer muchas cosas diferentes. Por lo general, son brillantes, llenos de potencial, enérgicos y de ritmo rápido. Suelen ser buenos en cualquier cosa que capte su interés.",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"人们喜欢 ENFJ。和他们在一起很有趣，并且真正理解和爱人。他们通常非常直接和诚实。通常ENFJ型人散发出很大的自信，并且有很强的能力去做很多不同的事情。他们一般都很聪明，充满潜力，精力充沛，节奏很快。他们通常擅长任何能引起他们兴趣的事情。",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJs do not like dealing with impersonal reasoning. They don't understand or appreciate its merit, and will be unhappy in situations where they're forced to deal with logic and facts without any connection to a human element. Living in the world of people possibilities, they enjoy their plans more than their achievements. They get excited about possibilities for the future, but may become easily bored and restless with the present.",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"لا تحب ENFJs التعامل مع التفكير غير الشخصي. إنهم لا يفهمون أو يقدرون مزاياه ، وسيكونون غير سعداء في المواقف التي يجبرون فيها على التعامل مع المنطق والحقائق دون أي صلة بالعنصر البشري. الذين يعيشون في عالم من إمكانيات الناس ، فهم يستمتعون بخططهم أكثر من إنجازاتهم. إنهم متحمسون بشأن احتمالات المستقبل ، لكنهم قد يشعرون بالملل والقلق بسهولة مع الحاضر.",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Les ENFJ n'aiment pas avoir affaire à des raisonnements impersonnels. Ils ne comprennent pas ou n'apprécient pas son mérite et seront mécontents dans des situations où ils sont obligés de faire face à la logique et aux faits sans aucun lien avec un élément humain. Vivant dans le monde des possibilités humaines, ils apprécient leurs projets plus que leurs réalisations. Ils sont enthousiasmés par les possibilités d'avenir, mais peuvent facilement s'ennuyer et s'inquiéter du présent.",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJ'ler kişisel olmayan akıl yürütme ile uğraşmaktan hoşlanmazlar. Değerini anlamıyorlar veya takdir etmiyorlar ve insan unsuruyla hiçbir bağlantısı olmadan mantık ve gerçeklerle uğraşmak zorunda kaldıkları durumlarda mutsuz olacaklar. İnsanların olasılıklar dünyasında yaşarken, başarılarından çok planlarından zevk alırlar. Geleceğe yönelik olasılıklar konusunda heyecanlanırlar, ancak şimdiki zamandan kolayca sıkılabilir ve huzursuz olabilirler.",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJ не любят безличных рассуждений. Они не понимают и не ценят его достоинств и будут несчастны в ситуациях, когда им приходится иметь дело с логикой и фактами без какой-либо связи с человеческим фактором. Живя в мире возможностей людей, они наслаждаются своими планами больше, чем достижениями. Их волнуют возможности на будущее, но настоящее может быстро надоесть и беспокоить их.",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJs mögen es nicht, mit unpersönlichen Argumenten umzugehen. Sie verstehen oder schätzen ihre Vorzüge nicht und werden in Situationen unglücklich sein, in denen sie gezwungen sind, mit Logik und Fakten ohne Verbindung zu einem menschlichen Element umzugehen. In der Welt der Möglichkeiten der Menschen lebend, genießen sie ihre Pläne mehr als ihre Errungenschaften. Sie freuen sich über die Möglichkeiten für die Zukunft, werden aber schnell gelangweilt und rastlos in der Gegenwart.",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"A los ENFJ no les gusta lidiar con razonamientos impersonales. No comprenden ni aprecian su mérito, y se sentirán infelices en situaciones en las que se vean obligados a lidiar con la lógica y los hechos sin ninguna conexión con un elemento humano. Al vivir en el mundo de las posibilidades de las personas, disfrutan más de sus planes que de sus logros. Se emocionan con las posibilidades para el futuro, pero pueden aburrirse e inquietarse fácilmente con el presente.",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJ 不喜欢处理非个人推理。他们不理解或欣赏它的优点，并且在他们被迫处理与人为因素没有任何联系的逻辑和事实的情况下会不高兴。生活在人们充满可能性的世界中，他们更喜欢他们的计划而不是他们的成就。他们对未来的可能性感到兴奋，但很容易对现在感到厌烦和不安。",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJs have a special gift with people, and are basically happy people when they can use that gift to help others. They get their best satisfaction from serving others. Their genuine interest in Humankind and their exceptional intuitive awareness of people makes them able to draw out even the most reserved individuals. ENFJs have a strong need for close, intimate relationships, and will put forth a lot of effort in creating and maintaining these relationships. They're very loyal and trustworthy once involved in a relationship.",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"لدى ENFJs هدية خاصة مع الناس ، وهم في الأساس أشخاص سعداء عندما يمكنهم استخدام هذه الهدية لمساعدة الآخرين. يحصلون على أفضل رضا من خدمة الآخرين. إن اهتمامهم الحقيقي بالبشرية ووعيهم البديهي الاستثنائي بالناس يجعلهم قادرين على جذب حتى الأفراد الأكثر تحفظًا. لدى ENFJs حاجة قوية لعلاقات وثيقة وحميمة ، وستبذل الكثير من الجهد في إنشاء هذه العلاقات والحفاظ عليها. إنهم مخلصون جدًا وجديرون بالثقة بمجرد مشاركتهم في علاقة.",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Les ENFJ ont un don spécial avec les gens et sont fondamentalement des gens heureux lorsqu'ils peuvent utiliser ce don pour aider les autres. Ils tirent leur meilleure satisfaction de servir les autres. Leur intérêt sincère pour l'Humanité et leur exceptionnelle conscience intuitive des personnes les rendent capables de faire sortir même les individus les plus réservés. Les ENFJ ont un grand besoin de relations étroites et intimes et feront beaucoup d'efforts pour créer et maintenir ces relations. Ils sont très loyaux et dignes de confiance une fois impliqués dans une relation.",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJ'lerin insanlarla özel bir yeteneği vardır ve bu hediyeyi başkalarına yardım etmek için kullanabildiklerinde temelde mutlu insanlardır. En iyi doyumlarını başkalarına hizmet etmekten alırlar. İnsanoğluna olan gerçek ilgileri ve insanlara karşı olağanüstü sezgisel farkındalıkları, onları en çekingen bireyleri bile çekebilmelerini sağlar. ENFJ'lerin yakın ve yakın ilişkilere güçlü bir ihtiyacı vardır ve bu ilişkileri oluşturmak ve sürdürmek için çok çaba sarf edeceklerdir. Bir kez bir ilişkiye girdiklerinde çok sadık ve güvenilirdirler.",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJ обладают особым даром по отношению к людям и в основном счастливы, когда могут использовать этот дар для помощи другим. Наибольшее удовлетворение они получают от служения другим. Их неподдельный интерес к Человечеству и их исключительное интуитивное понимание людей позволяет им привлекать даже самых замкнутых людей. ENFJ сильно нуждаются в близких, интимных отношениях и приложат много усилий для создания и поддержания этих отношений. Когда они вступают в отношения, они очень лояльны и заслуживают доверия.",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJs haben eine besondere Gabe im Umgang mit Menschen und sind im Grunde glückliche Menschen, wenn sie diese Gabe nutzen können, um anderen zu helfen. Sie erhalten ihre beste Befriedigung, wenn sie anderen dienen. Ihr echtes Interesse an der Menschheit und ihr außergewöhnliches intuitives Bewusstsein für Menschen machen sie in der Lage, selbst die zurückhaltendsten Individuen anzuziehen. ENFJs haben ein starkes Bedürfnis nach engen, intimen Beziehungen und werden sich sehr bemühen, diese Beziehungen aufzubauen und zu pflegen. Sie sind sehr loyal und vertrauenswürdig, wenn sie einmal in eine Beziehung verwickelt sind.",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Los ENFJ tienen un don especial con las personas y son básicamente personas felices cuando pueden usar ese don para ayudar a otros. Obtienen su mejor satisfacción al servir a los demás. Su interés genuino por la humanidad y su excepcional conciencia intuitiva de las personas les permite atraer incluso a los individuos más reservados. Los ENFJ tienen una gran necesidad de relaciones estrechas e íntimas y harán un gran esfuerzo para crear y mantener estas relaciones. Son muy leales y dignos de confianza una vez que están involucrados en una relación.",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJ 对人有一种特殊的天赋，当他们可以利用这种天赋帮助他人时，他们基本上是快乐的人。他们从为他人服务中获得最大的满足。他们对人类的真正兴趣和对人的非凡直觉使他们能够吸引甚至最保守的人。 ENFJ 非常需要亲密、亲密的关系，并且会付出很多努力来建立和维持这些关系。一旦涉及到关系，他们就会非常忠诚和值得信赖。",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"An ENFJ who has not developed their Feeling side may have difficulty making good decisions, and may rely heavily on other people in decision-making processes. If they have not developed their Intuition, they may not be able to see possibilities, and will judge things too quickly based on established value systems or social rules, without really understanding the current situation. An ENFJ who has not found their place in the world is likely to be extremely sensitive to criticism, and to have the tendency to worry excessively and feel guilty. They are also likely to be very manipulative and controlling with others.",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"قد يواجه ENFJ الذي لم يطور جانبه العاطفي صعوبة في اتخاذ قرارات جيدة ، وقد يعتمد بشكل كبير على أشخاص آخرين في عمليات صنع القرار. إذا لم يطوروا حدسهم ، فقد لا يكونوا قادرين على رؤية الاحتمالات ، وسوف يحكمون على الأشياء بسرعة كبيرة بناءً على أنظمة القيم الراسخة أو القواعد الاجتماعية ، دون فهم الموقف الحالي حقًا. من المحتمل أن يكون ENFJ الذي لم يجد مكانه في العالم حساسًا للغاية للنقد ، ويميل إلى القلق المفرط والشعور بالذنب. من المحتمل أيضًا أن يكونوا متلاعبين ومتحكمين للغاية مع الآخرين.",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Un ENFJ qui n'a pas développé son côté Sentiment peut avoir des difficultés à prendre de bonnes décisions et peut s'appuyer fortement sur d'autres personnes dans les processus de prise de décision. S'ils n'ont pas développé leur Intuition, ils ne verront peut-être pas les possibilités, et jugeront les choses trop rapidement sur la base de systèmes de valeurs ou de règles sociales établis, sans vraiment comprendre la situation actuelle. Un ENFJ qui n'a pas trouvé sa place dans le monde est susceptible d'être extrêmement sensible aux critiques, et d'avoir tendance à s'inquiéter excessivement et à se sentir coupable. Ils sont également susceptibles d'être très manipulateurs et contrôlants avec les autres.",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Duygu tarafını geliştirmemiş bir ENFJ, iyi kararlar vermekte zorluk çekebilir ve karar verme süreçlerinde büyük ölçüde diğer insanlara güvenebilir. Sezgilerini geliştirmedilerse, olasılıkları göremeyebilirler ve mevcut durumu gerçekten anlamadan yerleşik değer sistemlerine veya sosyal kurallara dayalı olarak çok hızlı bir şekilde yargılayabilirler. Dünyada yerini bulamayan bir ENFJ'nin eleştiriye karşı aşırı duyarlı olması, aşırı endişe duyma ve suçluluk duyma eğiliminde olması muhtemeldir. Ayrıca, başkalarını çok manipülatif ve kontrol edici olmaları muhtemeldir.",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJ, который не развил свою Чувственную сторону, может испытывать трудности с принятием правильных решений и может во многом полагаться на других людей в процессе принятия решений. Если у них не развита интуиция, они могут быть не в состоянии видеть возможности и будут слишком быстро судить о вещах, основываясь на установленных системах ценностей или социальных правилах, не понимая по-настоящему текущую ситуацию. ENFJ, не нашедший своего места в мире, вероятно, будет чрезвычайно чувствителен к критике и будет иметь тенденцию чрезмерно беспокоиться и чувствовать себя виноватым. Они также могут быть очень манипулятивными и контролирующими по отношению к другим.",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Ein ENFJ, der seine Gefühlsseite nicht entwickelt hat, kann Schwierigkeiten haben, gute Entscheidungen zu treffen, und kann sich bei Entscheidungsprozessen stark auf andere Menschen verlassen. Wenn sie ihre Intuition nicht entwickelt haben, können sie möglicherweise keine Möglichkeiten sehen und werden Dinge zu schnell anhand etablierter Wertesysteme oder gesellschaftlicher Regeln beurteilen, ohne die aktuelle Situation wirklich zu verstehen. Ein ENFJ, der seinen Platz in der Welt nicht gefunden hat, ist wahrscheinlich äußerst empfindlich gegenüber Kritik und neigt dazu, sich übermäßig Sorgen zu machen und sich schuldig zu fühlen. Sie sind wahrscheinlich auch sehr manipulativ und kontrollierend mit anderen.",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Un ENFJ que no ha desarrollado su lado Feeling puede tener dificultades para tomar buenas decisiones y puede depender en gran medida de otras personas en los procesos de toma de decisiones. Si no han desarrollado su intuición, es posible que no puedan ver las posibilidades y juzgarán las cosas demasiado rápido basándose en sistemas de valores establecidos o reglas sociales, sin comprender realmente la situación actual. Un ENFJ que no ha encontrado su lugar en el mundo probablemente sea extremadamente sensible a las críticas y tenga la tendencia a preocuparse excesivamente y sentirse culpable. También es probable que sean muy manipuladores y controladores con los demás.",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"ENFJ 没有发展他们的感觉方面可能难以做出正确的决定，并且可能在决策过程中严重依赖其他人。如果他们的直觉没有发展，他们可能无法看到可能性，并且会根据既定的价值体系或社会规则对事物进行过快的判断，而没有真正了解当前的情况。一个在世界上没有找到自己位置的ENFJ很可能对批评非常敏感，并且有过度担心和内疚的倾向。他们也可能非常善于操纵和控制他人。",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"In general, ENFJs are charming, warm, gracious, creative and diverse individuals with richly developed insights into what makes other people tick. This special ability to see growth potential in others combined with a genuine drive to help people makes the ENFJ a truly valued individual. As giving and caring as the ENFJ is, they need to remember to value their own needs as well as the needs of others.
",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"بشكل عام ، ENFJs هم أفراد ساحرون ودافئون وكريمون ومبدعون ومتنوعون يتمتعون برؤى متطورة غنية حول ما يجعل الآخرين يتحركون. هذه القدرة الخاصة على رؤية إمكانات النمو في الآخرين جنبًا إلى جنب مع دافع حقيقي لمساعدة الناس تجعل من ENFJ فردًا ذو قيمة حقيقية. بقدر العطاء والرعاية مثل ENFJ ، يجب أن يتذكروا تقدير احتياجاتهم الخاصة وكذلك احتياجات الآخرين.",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"En général, les ENFJ sont des personnes charmantes, chaleureuses, gracieuses, créatives et diversifiées avec des idées richement développées sur ce qui fait vibrer les autres. Cette capacité particulière à voir le potentiel de croissance chez les autres combinée à un véritable désir d'aider les gens fait de l'ENFJ une personne vraiment appréciée. Aussi généreux et attentionné que soient les ENFJ, ils doivent se rappeler de valoriser leurs propres besoins ainsi que les besoins des autres.",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Genel olarak, ENFJ'ler çekici, sıcak, zarif, yaratıcı ve diğer insanları neyin harekete geçirdiğine dair zengin bir şekilde gelişmiş içgörülere sahip çeşitli bireylerdir. İnsanlara yardım etmek için gerçek bir dürtü ile birlikte diğerlerinde büyüme potansiyeli görme konusundaki bu özel yetenek, ENFJ'yi gerçekten değerli bir birey yapar. ENFJ kadar verici ve şefkatli olduğu için, başkalarının ihtiyaçları kadar kendi ihtiyaçlarına da değer vermeyi hatırlamaları gerekir.",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"В целом ENFJ - очаровательные, теплые, любезные, творческие и разнообразные люди с богатым пониманием того, что движет другими людьми. Эта особая способность видеть потенциал роста в других в сочетании с искренним стремлением помогать людям делает ENFJ действительно ценным человеком. Какими бы щедрыми и заботливыми ни были ENFJ, им нужно не забывать ценить свои собственные потребности, а также потребности других.",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"Im Allgemeinen sind ENFJs charmante, herzliche, liebenswürdige, kreative und vielfältige Menschen mit einem reichen Einblick in das, was andere Menschen antreibt. Diese besondere Fähigkeit, Wachstumspotenzial in anderen zu sehen, kombiniert mit einem echten Willen, Menschen zu helfen, macht die ENFJ zu einer wirklich geschätzten Persönlichkeit. So großzügig und fürsorglich das ENFJ auch ist, sie müssen daran denken, ihre eigenen Bedürfnisse sowie die Bedürfnisse anderer zu schätzen.",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"En general, los ENFJ son personas encantadoras, cálidas, amables, creativas y diversas con conocimientos muy desarrollados sobre lo que mueve a otras personas. Esta capacidad especial de ver el potencial de crecimiento en los demás, combinada con un impulso genuino para ayudar a las personas, hace que la ENFJ sea una persona verdaderamente valorada. Por generoso y cariñoso que sea el ENFJ, deben recordar valorar sus propias necesidades y las de los demás.",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'1',
            'part'=>'1',
            'title'=>"",
            'desc'=>"总的来说，ENFJ 是迷人、热情、亲切、富有创造力和多样化的人，对是什么让其他人感到满意，拥有丰富的洞察力。这种看到他人成长潜力的特殊能力，加上真正帮助他人的动力，使 ENFJ 成为真正有价值的个体。与 ENFJ 一样给予和关怀，他们需要记住重视自己的需要以及他人的需要。",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'2',
            'title'=>"Jungian functional preference ordering:",
            'desc'=>"",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'2',
            'title'=>"ترتيب الأفضلية الوظيفية:",
            'desc'=>"",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'2',
            'title'=>"Ordre des préférences fonctionnelles jungiennes :",
            'desc'=>"",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'2',
            'title'=>"Jungian fonksiyonel tercih sıralaması:",
            'desc'=>"",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'2',
            'title'=>"Юнгианский функциональный порядок предпочтений:",
            'desc'=>"",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'2',
            'title'=>"Jungsche funktionale Präferenzordnung:",
            'desc'=>"",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'2',
            'title'=>"Orden de preferencia funcional de Jung:",
            'desc'=>"",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'2',
            'title'=>"荣格函数偏好排序：",
            'desc'=>"",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"Dominant: Extraverted Feeling",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"المهيمن: الشعور المقلوب",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"Dominant : Sentiment extraverti",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"Baskın: Dışa Dönük Duygu",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"Доминанта: экстравертное чувство",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"Dominant: Extravertiertes Gefühl",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"Dominante: sentimiento extravertido",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"主导：外倾感觉",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"Auxiliary: Introverted Intuition",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"المساعد: الحدس الانطوائي",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"Auxiliaire : Intuition introvertie",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"Yardımcı: İçe Dönük Sezgi",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"Вспомогательный: интровертная интуиция",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"Hilfsmittel: Introvertierte Intuition",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"Auxiliar: intuición introvertida",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"辅助：内向的直觉",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"Tertiary: Extraverted Sensing",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'3',
            'title'=>"",
            'desc'=>"العالي: استشعار منبسط",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"Tertiaire : Détection extravertie",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"Третичное: экстравертное восприятие",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"Вспомогательный: интровертная интуиция",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"Tertiär: Extravertierte Wahrnehmung",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"Terciario: detección extravertida",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"第三：外倾感觉",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"Inferior: Introverted Thinking",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"أدنى: التفكير الانطوائي",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"Inférieur : Pensée introvertie",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"Aşağı: İçedönük Düşünme",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"Низший: интровертное мышление",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"Minderwertig: Introvertiertes Denken",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"Inferior: pensamiento introvertido",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'2',
            'part'=>'2',
            'title'=>"",
            'desc'=>"劣势：内向思维",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'3',
            'title'=>"ENFJs generally have the following traits:",
            'desc'=>"",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'3',
            'title'=>"تتميز ENFJs عمومًا بالسمات التالية:",
            'desc'=>"",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'3',
            'title'=>"Les ENFJ ont généralement les traits suivants :",
            'desc'=>"",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'3',
            'title'=>"ENFJ'ler genellikle aşağıdaki özelliklere sahiptir:",
            'desc'=>"",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'3',
            'title'=>"ENFJ обычно имеют следующие черты:",
            'desc'=>"",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'3',
            'title'=>"ENFJs haben im Allgemeinen die folgenden Eigenschaften:",
            'desc'=>"",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'3',
            'title'=>"Los ENFJ generalmente tienen las siguientes características:",
            'desc'=>"",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'3',
            'title'=>"ENFJ 通常具有以下特征：",
            'desc'=>"",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'3',
            'part'=>'3',
            'title'=>"",
            'desc'=>"Genuinely and warmly interested in people
/n
Value people's feelings
/n
Value structure and organization
/n
Value harmony, and good at creating it
/n
Exceptionally good people skills
/n
Dislike impersonal logic and analysis
/n
Strong organizational capabilities
/n
Loyal and honest
/n
Creative and imaginative
/n
Enjoy variety and new challenges
/n
Get personal satisfaction from helping others
/n
Extremely sensitive to criticism and discord
/n
Need approval from others to feel good about themselves
",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'3',
            'part'=>'3',
            'title'=>"",
            'desc'=>"بصدق ودفء الاهتمام بالناس
/n
تقدير مشاعر الناس
/n
هيكل القيمة وتنظيمها
/n
قيمة الانسجام ، وجيدة في خلقه
/n
مهارات الناس جيدة بشكل استثنائي
/n
يكره المنطق غير الشخصي والتحليل
/n
قدرات تنظيمية قوية
/n
مخلص وصادق
/n
مبدع وخيالي
/n
استمتع بالتنوع والتحديات الجديدة
/n
احصل على الرضا الشخصي من مساعدة الآخرين
/n
حساسة للغاية للنقد والخلاف
/n
بحاجة إلى موافقة من الآخرين ليشعروا بالرضا عن أنفسهم",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'3',
            'part'=>'3',
            'title'=>"",
            'desc'=>"Vraiment et chaleureusement intéressé par les gens
/n
Valoriser les sentiments des gens
/n
Structure de valeur et organisation
/n
Valoriser l'harmonie, et bien la créer
/n
Compétences interpersonnelles exceptionnellement bonnes
/n
N'aime pas la logique et l'analyse impersonnelles
/n
Fortes capacités organisationnelles
/n
Loyal et honnête
/n
Créatif et imaginatif
/n
Profitez de la variété et de nouveaux défis
/n
Obtenir une satisfaction personnelle en aidant les autres
/n
Extrêmement sensible aux critiques et à la discorde
/n
Besoin de l'approbation des autres pour se sentir bien dans sa peau",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'3',
            'part'=>'3',
            'title'=>"",
            'desc'=>"İnsanlarla samimi ve sıcak bir şekilde ilgileniyor
/n
İnsanların duygularına değer verin
/n
Değer yapısı ve organizasyonu
/n
Uyum değeri ve onu yaratmada iyi
/n
Olağanüstü iyi insan becerileri
/n
Kişisel olmayan mantık ve analizden hoşlanmamak
/n
Güçlü organizasyon yetenekleri
/n
Sadık ve dürüst
/n
Yaratıcı ve hayal gücü kuvvetli
/n
Çeşitliliğin ve yeni zorlukların tadını çıkarın
/n
Başkalarına yardım etmekten kişisel tatmin elde edin
/n
Eleştiri ve anlaşmazlıklara karşı son derece hassas
/n
Kendilerini iyi hissetmek için başkalarından onay alma ihtiyacı",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'3',
            'part'=>'3',
            'title'=>"",
            'desc'=>"Искренне и горячо интересуюсь людьми
/n
Цените чувства людей
/n
Структура ценностей и организация
/n
Цените гармонию и умеете ее создавать
/n
Исключительно хорошие навыки работы с людьми
/n
Не люблю безличную логику и анализ
/n
Сильные организационные возможности
/n
Верный и честный
/n
Творческий и творческий
/n
Наслаждайтесь разнообразием и новыми задачами
/n
Получайте личное удовлетворение от помощи другим
/n
Чрезвычайно чувствителен к критике и разногласиям
/n
Нужно одобрение других, чтобы чувствовать себя хорошо",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'3',
            'part'=>'3',
            'title'=>"",
            'desc'=>"Aufrichtig und herzlich an Menschen interessiert
/n
Schätze die Gefühle der Menschen
/n
Wertestruktur und Organisation
/n
Wert auf Harmonie und gut darin, sie zu erschaffen
/n
Außergewöhnlich gute Menschenkenntnis
/n
Mag keine unpersönliche Logik und Analyse
/n
Starke organisatorische Fähigkeiten
/n
Loyal und ehrlich
/n
Kreativ und fantasievoll
/n
Genießen Sie Abwechslung und neue Herausforderungen
/n
Erhalte persönliche Befriedigung, indem du anderen hilfst
/n
Äußerst empfindlich auf Kritik und Zwietracht
/n
Brauchen Sie die Zustimmung anderer, um sich gut zu fühlen",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'3',
            'part'=>'3',
            'title'=>"",
            'desc'=>"Interesado genuina y calurosamente en las personas
/n
Valorar los sentimientos de las personas
/n
Estructura y organización de valores
/n
Valora la armonía y es bueno para crearla.
/n
Excepcionalmente buenas habilidades con las personas.
/n
No le gusta la lógica y el análisis impersonales
/n
Fuertes capacidades organizativas
/n
Leal y honesto
/n
Creativo e imaginativo
/n
Disfruta de la variedad y los nuevos desafíos.
/n
Obtenga satisfacción personal al ayudar a los demás
/n
Extremadamente sensible a la crítica y la discordia.
/n
Necesitan la aprobación de los demás para sentirse bien consigo mismos.",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'3',
            'part'=>'3',
            'title'=>"",
            'desc'=>"真诚而热情地对人感兴趣
/n
重视人的感受
/n
价值结构和组织
/n
重视和谐，善于创造
/n
非常好的人际交往能力
/n
不喜欢客观的逻辑和分析
/n
强大的组织能力
/n
忠诚和诚实
/n
创意和想象力
/n
享受多样性和新挑战
/n
从帮助他人中获得个人满足
/n
对批评和不和非常敏感
/n
需要别人的认可才能自我感觉良好",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'4',
            'title'=>"ENFJ Relationships",
            'desc'=>"",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'4',
            'title'=>"علاقات ENFJ",
            'desc'=>"",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'4',
            'title'=>"Relations ENFJ",
            'desc'=>"",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'4',
            'title'=>"ENFJ İlişkileri",
            'desc'=>"",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'4',
            'title'=>"ENFJ Отношения",
            'desc'=>"",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'4',
            'title'=>"",
            'desc'=>"ENFJ-Beziehungen",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'4',
            'title'=>"",
            'desc'=>"Relaciones ENFJ",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'4',
            'title'=>"ENFJ 关系",
            'desc'=>"",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'4',
            'part'=>'4',
            'title'=>"",
            'desc'=>"ENFJs put a lot of effort and enthusiasm into their relationships. To some extent, the ENFJ defines themselves by the closeness and authenticity of their personal relationships, and are therefore highly invested in the business of relationships. They have very good people skills, and are affectionate and considerate. They are warmly affirming and nurturing. They excel at bringing out the best in others, and warmly supporting them. They want responding affirmation from their relationships, although they have a problem asking for it. When a situation calls for it, the ENFJ will become very sharp and critical. After having made their point, they will return to their natural, warm selves. They may have a tendency to 'smother' their loved ones, but are generally highly valued for their genuine warmth and caring natures.",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'4',
            'part'=>'4',
            'title'=>"",
            'desc'=>"تضع ENFJs الكثير من الجهد والحماس في علاقاتهم. إلى حد ما ، تعرف ENFJ نفسها من خلال قرب وأصالة علاقاتهم الشخصية ، وبالتالي فهي تستثمر بشكل كبير في أعمال العلاقات. لديهم مهارات جيدة للغاية مع الناس ، وهم حنون ومراعيون. إنهم يؤكدون بحرارة ويرعون. إنهم يتفوقون في إخراج الأفضل في الآخرين ، ودعمهم بحرارة. يريدون الرد على تأكيد من علاقاتهم ، على الرغم من أن لديهم مشكلة في طلب ذلك. عندما يستدعي الموقف ذلك ، ستصبح ENFJ حادة للغاية وحرجة. بعد توضيح وجهة نظرهم ، سيعودون إلى ذواتهم الطبيعية الدافئة. قد يكون لديهم ميل إلى 'خنق' أحبائهم ، لكنهم عمومًا يحظون بتقدير كبير لدفئهم الحقيقي وطبيعتهم الراعية.",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'4',
            'part'=>'4',
            'title'=>"",
            'desc'=>"Les ENFJ mettent beaucoup d'efforts et d'enthousiasme dans leurs relations. Dans une certaine mesure, les ENFJ se définissent par la proximité et l'authenticité de leurs relations personnelles, et sont donc fortement investies dans le commerce des relations. Ils ont de très bonnes relations interpersonnelles, sont affectueux et prévenants. Ils sont chaleureusement affirmatifs et nourrissants. Ils excellent à faire ressortir le meilleur des autres et à les soutenir chaleureusement. Ils veulent répondre à l'affirmation de leurs relations, bien qu'ils aient du mal à le demander. Lorsqu'une situation l'exige, l'ENFJ deviendra très pointu et critique. Après avoir fait valoir leur point de vue, ils retourneront à leur moi naturel et chaleureux. Ils peuvent avoir tendance à « étouffer » leurs proches, mais sont généralement très appréciés pour leur chaleur authentique et leur nature bienveillante.",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'4',
            'part'=>'4',
            'title'=>"",
            'desc'=>"ENFJ'ler ilişkilerine çok fazla çaba ve coşku katarlar. Bir dereceye kadar, ENFJ kendilerini kişisel ilişkilerinin yakınlığı ve gerçekliği ile tanımlar ve bu nedenle ilişkiler işine büyük ölçüde yatırım yapar. Çok iyi insan becerilerine sahiptirler ve sevecen ve düşüncelidirler. Sıcak bir şekilde onaylıyorlar ve besliyorlar. Başkalarının içindeki en iyiyi ortaya çıkarmakta ve onları sıcak bir şekilde desteklemekte başarılıdırlar. Bunu istemekte bir sorunları olsa da, ilişkilerinden onay almak isterler. Bir durum gerektirdiğinde, ENFJ çok keskin ve kritik hale gelecektir. Noktalarını koyduktan sonra, doğal, sıcak benliklerine geri dönecekler. Sevdiklerini 'boğma' eğiliminde olabilirler, ancak genellikle gerçek sıcaklıkları ve sevecen doğaları için çok değerlidirler.",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'4',
            'part'=>'4',
            'title'=>"",
            'desc'=>"ENFJ вкладывают много усилий и энтузиазма в свои отношения. В некоторой степени ENFJ определяют себя по близости и подлинности своих личных отношений, и поэтому очень инвестируют в бизнес взаимоотношений. У них очень хорошие навыки общения с людьми, они ласковы и внимательны. Они горячо поддерживают и заботятся. Они преуспевают в том, чтобы выявлять лучшее в других и горячо поддерживать их. Они хотят получить ответное подтверждение своих отношений, хотя у них проблемы с тем, чтобы об этом просить. Когда того требует ситуация, ENFJ становится очень резким и критическим. Выразив свою точку зрения, они вернутся к своему естественному, теплому «я». Они могут иметь склонность «задушить» своих близких, но, как правило, их очень ценят за их искреннюю теплоту и заботу.",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'4',
            'part'=>'4',
            'title'=>"",
            'desc'=>"ENFJs stecken viel Mühe und Enthusiasmus in ihre Beziehungen. Die ENFJ definieren sich teilweise über die Nähe und Authentizität ihrer persönlichen Beziehungen und sind daher stark in das Beziehungsgeschäft investiert. Sie haben sehr gute Menschenkenntnis, sind liebevoll und rücksichtsvoll. Sie sind herzlich bejahend und pflegend. Sie zeichnen sich dadurch aus, das Beste aus anderen herauszuholen und sie herzlich zu unterstützen. Sie wollen eine Bestätigung aus ihren Beziehungen, obwohl sie ein Problem damit haben, danach zu fragen. Wenn es eine Situation erfordert, wird die ENFJ sehr scharf und kritisch. Nachdem sie ihren Standpunkt dargelegt haben, werden sie zu ihrem natürlichen, warmen Selbst zurückkehren. Sie neigen vielleicht dazu, ihre Lieben zu „ersticken“, werden aber im Allgemeinen wegen ihrer aufrichtigen Wärme und fürsorglichen Natur sehr geschätzt.",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'4',
            'part'=>'4',
            'title'=>"",
            'desc'=>"Los ENFJ ponen mucho esfuerzo y entusiasmo en sus relaciones. Hasta cierto punto, la ENFJ se define a sí misma por la cercanía y autenticidad de sus relaciones personales y, por lo tanto, está muy comprometida con el negocio de las relaciones. Tienen muy buenas habilidades con las personas, son cariñosos y considerados. Son calurosamente afirmativos y nutritivos. Sobresalen en sacar lo mejor de los demás y en apoyarlos calurosamente. Quieren responder a la afirmación de sus relaciones, aunque tienen problemas para pedirla. Cuando una situación lo requiere, la ENFJ se volverá muy aguda y crítica. Después de haber expresado su punto de vista, volverán a ser cálidos y naturales. Pueden tener una tendencia a 'asfixiar' a sus seres queridos, pero generalmente son muy valorados por su genuina calidez y su naturaleza afectuosa.",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'4',
            'part'=>'4',
            'title'=>"",
            'desc'=>"ENFJ 在他们的人际关系中投入了大量的努力和热情。在某种程度上，ENFJ 通过个人关系的亲密性和真实性来定义自己，因此对关系业务投入了大量资金。他们有很好的人际交往能力，深情和体贴。他们热情地肯定和培养。他们擅长发掘他人的优点，并热情地支持他们。他们希望从他们的关系中得到肯定，尽管他们在要求时遇到了问题。当情况需要时，ENFJ 会变得非常敏锐和挑剔。在表达了自己的观点后，他们将回归自然、温暖的自我。他们可能倾向于“扼杀”他们所爱的人，但通常因其真正的温暖和关怀的天性而受到高度评价。",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'5',
            'title'=>"Most ENFJs will exhibit the following strengths with regards to relationship issues:",
            'desc'=>"",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'5',
            'title'=>"ستظهر معظم ENFJs نقاط القوة التالية فيما يتعلق بقضايا العلاقة:",
            'desc'=>"",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'5',
            'title'=>"La plupart des ENFJ présenteront les points forts suivants en ce qui concerne les problèmes relationnels :",
            'desc'=>"",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'5',
            'title'=>"Çoğu ENFJ, ilişki sorunlarıyla ilgili olarak aşağıdaki güçlü yönleri sergileyecektir:",
            'desc'=>"",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'5',
            'title'=>"Большинство ENFJ будут демонстрировать следующие сильные стороны в вопросах взаимоотношений:",
            'desc'=>"",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'5',
            'title'=>"Die meisten ENFJs werden die folgenden Stärken in Bezug auf Beziehungsprobleme aufweisen:",
            'desc'=>"",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'5',
            'title'=>"La mayoría de los ENFJ exhibirán las siguientes fortalezas con respecto a los problemas de relación:",
            'desc'=>"",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'5',
            'title'=>"大多数 ENFJ 会在人际关系问题上表现出以下优势：",
            'desc'=>"",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'5',
            'part'=>'5',
            'title'=>"",
            'desc'=>"Good verbal communication skills
/n
Very perceptive about people's thoughts and motives
/n
Motivational, inspirational; bring out the best in others
/n
Warmly affectionate and affirming
/n
Fun to be with - lively sense of humor, dramatic, energetic, optimistic
/n
Good money skills
/n
Able to 'move on' after a love relationship has failed (although they blame themselves)
/n
Loyal and committed - they want lifelong relationships
/n
Strive for 'win-win' situations
/n
Driven to meet other's needs",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'5',
            'part'=>'5',
            'title'=>"",
            'desc'=>"مهارات الاتصال اللفظي جيدة
/n
مدرك جدا لأفكار الناس ودوافعهم
/n
تحفيزية ملهمة. يبرز أفضل ما في الآخرين
/n
حنون و مؤكد
/n
المرح مع - روح الدعابة حية ، درامية ، نشطة ، متفائل
/n
مهارات مالية جيدة
/n
القدرة على 'المضي قدمًا' بعد فشل علاقة الحب (على الرغم من أنهم يلومون أنفسهم)
/n
مخلصون وملتزمون - يريدون علاقات مدى الحياة
/n
نسعى جاهدين من أجل مواقف 'الفوز'
/n
مدفوعة لتلبية احتياجات الآخرين",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'5',
            'part'=>'5',
            'title'=>"",
            'desc'=>"Bonnes compétences en communication verbale
/n
Très perspicace sur les pensées et les motivations des gens
/n
Motivation, inspiration; faire ressortir le meilleur des autres
/n
Chaleureusement affectueux et affirmatif
/n
Amusant d'être avec - sens de l'humour vif, dramatique, énergique, optimiste
/n
Bonnes compétences en argent
/n
Capable de ' passer à autre chose ' après l'échec d'une relation amoureuse (bien qu'ils se blâment eux-mêmes)
/n
Loyal et engagé - ils veulent des relations durables
/n
Rechercher des situations « gagnant-gagnant »
/n
Poussé à répondre aux besoins des autres",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'5',
            'part'=>'5',
            'title'=>"",
            'desc'=>"İyi sözlü iletişim becerileri
/n
İnsanların düşünceleri ve güdüleri hakkında çok anlayışlı
/n
Motive edici, ilham verici; diğerlerinin içindeki en iyiyi ortaya çıkarmak
/n
Sıcak sevecen ve onaylayıcı
/n
Birlikte olmak eğlenceli - canlı mizah anlayışı, dramatik, enerjik, iyimser
/n
İyi para becerileri
/n
Bir aşk ilişkisi başarısız olduktan sonra 'devam edebilme' (kendilerini suçlasalar da)
/n
Sadık ve kararlı - ömür boyu ilişkiler istiyorlar
/n
'Kazan-kazan' durumları için çabalayın
/n
Başkalarının ihtiyaçlarını karşılamaya yönelik",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'5',
            'part'=>'5',
            'title'=>"",
            'desc'=>"Хорошие вербальные коммуникативные навыки
/п
Очень внимателен к мыслям и мотивам людей
/п
Мотивационный, вдохновляющий; выявить лучшее в других
/п
Тепло ласковый и подтверждающий
/п
Весело быть с - живое чувство юмора, драматичный, энергичный, оптимистичный
/п
Хорошие денежные навыки
/п
Способны `` двигаться дальше '' после того, как любовные отношения потерпели неудачу (хотя они винят себя)
/п
Верные и преданные - они хотят отношений на всю жизнь
/п
Стремитесь к беспроигрышным ситуациям
/п
Стремление удовлетворить потребности других",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'5',
            'part'=>'5',
            'title'=>"",
            'desc'=>"Gute verbale Kommunikationsfähigkeiten
/n
Sehr einfühlsam in Bezug auf die Gedanken und Motive der Menschen
/n
Motivierend, inspirierend; das Beste aus anderen herausholen
/n
Herzlich liebevoll und bestätigend
/n
Es macht Spaß, mit ihm zusammen zu sein - lebhafter Humor, dramatisch, energisch, optimistisch
/n
Gute Geldkenntnisse
/n
Kann 'weitermachen', nachdem eine Liebesbeziehung gescheitert ist (obwohl sie sich selbst die Schuld geben)
/n
Loyal und engagiert – sie wollen lebenslange Beziehungen
/n
Streben Sie nach Win-Win-Situationen
/n
Angetrieben, die Bedürfnisse anderer zu erfüllen",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'5',
            'part'=>'5',
            'title'=>"",
            'desc'=>"Buenas habilidades de comunicación verbal.
/norte
Muy perspicaz sobre los pensamientos y motivos de las personas.
/norte
Motivacional, inspirador; sacar lo mejor de los demás
/norte
Calurosamente cariñoso y afirmativo
/norte
Diversión para estar con: sentido del humor vivo, dramático, enérgico, optimista
/norte
Buenas habilidades de dinero
/norte
Capaz de 'seguir adelante' después de que una relación amorosa ha fracasado (aunque se culpan a sí mismos)
/norte
Leales y comprometidos: quieren relaciones de por vida.
/norte
Esfuércese por lograr situaciones en las que todos ganen
/norte
Impulsado por satisfacer las necesidades de los demás",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'5',
            'part'=>'5',
            'title'=>"",
            'desc'=>"良好的语言沟通能力
/n
对人们的想法和动机非常敏感
/n
励志、励志；发挥别人最好的一面
/n
热情的深情和肯定
/n
乐在其中——活泼的幽默感、戏剧性、精力充沛、乐观
/n
良好的理财能力
/n
在恋爱关系失败后能够“继续前进”（尽管他们自责）
/n
忠诚和忠诚 - 他们想要终生的关系
/n
争取“双赢”局面
/n
为满足他人的需要而努力",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'6',
            'title'=>"Most ENFJs will exhibit the following weaknesses with regards to relationships issues:",
            'desc'=>"",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'6',
            'title'=>"ستظهر معظم ENFJs نقاط الضعف التالية فيما يتعلق بقضايا العلاقات:",
            'desc'=>"",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'6',
            'title'=>"La plupart des ENFJ présenteront les faiblesses suivantes en ce qui concerne les problèmes de relations :",
            'desc'=>"",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'6',
            'title'=>"Çoğu ENFJ, ilişkiler sorunlarıyla ilgili olarak aşağıdaki zayıflıkları sergileyecektir:",
            'desc'=>"",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'6',
            'title'=>"Большинство ENFJ будут демонстрировать следующие слабости в вопросах взаимоотношений:",
            'desc'=>"",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'6',
            'title'=>"Die meisten ENFJs weisen die folgenden Schwächen in Bezug auf Beziehungsprobleme auf:",
            'desc'=>"",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'6',
            'title'=>"La mayoría de los ENFJ exhibirán las siguientes debilidades con respecto a los problemas de relaciones:",
            'desc'=>"",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'6',
            'title'=>"大多数 ENFJ 会在人际关系问题上表现出以下弱点：",
            'desc'=>"",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'6',
            'part'=>'6',
            'title'=>"",
            'desc'=>"Tendency to be smothering and over-protective
/n
Tendency to be controlling and/or manipulative
/n
Don't pay enough attention to their own needs
/n
Tend to be critical of opinions and attitudes which don't match their own
/n
Sometimes unaware of social appropriateness or protocol
/n
Extremely sensitive to conflict, with a tendency to sweep things under the rug as an avoidance
tactic
/n
Tendency to blame themselves when things go wrong, and not give themselves credit when things
go right
/n
Their sharply defined value systems make them unbending in some areas
/n
They may be so attuned to what is socially accepted or expected that they're unable to assess
whether something is 'right' or 'wrong' outside of what their social circle expects.",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'6',
            'part'=>'6',
            'title'=>"",
            'desc'=>"الميل إلى الاختناق والإفراط في الحماية
/n
الميل للسيطرة و / أو التلاعب
/n
لا تولي اهتماما كافيا لاحتياجاتهم الخاصة
/n
يميلون إلى نقد الآراء والمواقف التي لا تتطابق مع آرائهم
/n
في بعض الأحيان غير مدرك للملاءمة الاجتماعية أو البروتوكول
/n
حساسة للغاية للنزاع ، مع الميل إلى كنس الأشياء تحت البساط كتجنب
تكتيك
/n
الميل إلى إلقاء اللوم على أنفسهم عندما تسوء الأمور ، وعدم منحهم الفضل عند حدوث الأمور
اذهب يمينا
/n
أنظمة القيم المحددة بدقة تجعلها غير قابلة للانحياز في بعض المجالات
/n
قد يكونون منسجمين للغاية مع ما هو مقبول اجتماعيًا أو متوقعًا بحيث يتعذر عليهم تقييمه
ما إذا كان هناك شيء 'صحيح' أو 'خطأ' خارج ما تتوقعه دائرتهم الاجتماعية.",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'6',
            'part'=>'6',
            'title'=>"",
            'desc'=>"Tendance à être étouffante et surprotectrice
/n
Tendance à contrôler et/ou à manipuler
/n
Ne pas prêter assez d'attention à leurs propres besoins
/n
A tendance à critiquer les opinions et les attitudes qui ne correspondent pas aux leurs
/n
Parfois inconscient de la pertinence sociale ou du protocole
/n
Extrêmement sensible aux conflits, avec une tendance à balayer les choses sous le tapis pour éviter
tactique
/n
Tendance à se blâmer lorsque les choses tournent mal et à ne pas se donner de crédit lorsque les choses
aller à droite
/n
Leurs systèmes de valeurs bien définis les rendent inflexibles dans certains domaines
/n
Ils peuvent être tellement à l'écoute de ce qui est socialement accepté ou attendu qu'ils sont incapables d'évaluer
si quelque chose est « bien » ou « mauvais » en dehors de ce que leur cercle social attend.",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'6',
            'part'=>'6',
            'title'=>"",
            'desc'=>"Boğucu ve aşırı koruyucu olma eğilimi
/n
Kontrolcü ve/veya manipülatif olma eğilimi
/n
Kendi ihtiyaçlarına yeterince dikkat etme
/n
Kendilerine uymayan görüş ve tutumları eleştirme eğilimi
/n
Bazen sosyal uygunluk veya protokolden habersiz
/n
Çatışmalara karşı son derece hassas, kaçınma olarak olayları halının altına süpürme eğilimi
taktik
/n
İşler ters gittiğinde kendilerini suçlama ve işler ters gittiğinde kendilerine itibar etmeme eğilimi
sağa git
/n
Keskin bir şekilde tanımlanmış değer sistemleri, bazı alanlarda esnek olmalarını sağlar.
/n
Neyin sosyal olarak kabul edildiğine veya neyin beklendiğine o kadar uyum içinde olabilirler ki, onları değerlendiremezler.
sosyal çevrelerinin beklediğinin dışında bir şeyin 'doğru' veya 'yanlış' olup olmadığı.",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'6',
            'part'=>'6',
            'title'=>"",
            'desc'=>"Склонность задыхаться и чрезмерно защищать
/п
Склонность к контролю и / или манипуляциям
/п
Не уделяют достаточно внимания собственным потребностям
/п
Склонны критически относиться к мнениям и позициям, которые не совпадают с их собственными.
/п
Иногда не осознает социальную целесообразность или протокол
/п
Чрезвычайно чувствителен к конфликтам, склонен прятать вещи под ковер в качестве избегания
тактика
/п
Склонность винить себя, когда что-то идет не так, и не доверять себе, когда что-то
направо
/п
Их четко определенные системы ценностей делают их непоколебимыми в некоторых областях.
/п
Они могут быть настолько настроены на то, что принято или ожидается в обществе, что не могут оценить
является ли что-то «правильным» или «неправильным» за пределами того, чего ожидает их социальный круг.",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'6',
            'part'=>'6',
            'title'=>"",
            'desc'=>"Neigung zu ersticken und überfürsorglich
/n
Tendenz zu kontrollierend und/oder manipulativ
/n
Achte nicht genug auf die eigenen Bedürfnisse
/n
Neigt dazu, Meinungen und Einstellungen zu kritisieren, die nicht mit ihren eigenen übereinstimmen
/n
Manchmal ohne Kenntnis der sozialen Angemessenheit oder des Protokolls
/n
Äußerst konfliktsensibel, mit der Tendenz, Dinge zur Vermeidung unter den Teppich zu kehren
Taktik
/n
Neigung, sich selbst die Schuld zu geben, wenn etwas schief geht, und sich selbst keine Anerkennung zu geben, wenn etwas
Geh rechts
/n
Ihre scharf definierten Wertesysteme machen sie in manchen Bereichen unbeugsam
/n
Sie sind möglicherweise so auf das eingestellt, was gesellschaftlich akzeptiert oder erwartet wird, dass sie es nicht einschätzen können
ob etwas „richtig“ oder „falsch“ ist, außerhalb dessen, was ihr soziales Umfeld erwartet.",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'6',
            'part'=>'6',
            'title'=>"",
            'desc'=>"Tendencia a ser asfixiante y sobreprotector.
/norte
Tendencia a ser controlador y / o manipulador.
/norte
No preste suficiente atención a sus propias necesidades.
/norte
Suelen ser críticos con opiniones y actitudes que no coinciden con las suyas.
/norte
A veces desconoce la idoneidad social o el protocolo.
/norte
Extremadamente sensible a los conflictos, con tendencia a esconder las cosas debajo de la alfombra para evitarlo.
táctica
/norte
Tendencia a culparse a sí mismos cuando las cosas van mal y a no darse crédito cuando las cosas
ve a la derecha
/norte
Sus sistemas de valores claramente definidos los hacen inflexibles en algunas áreas.
/norte
Pueden estar tan en sintonía con lo que se acepta o se espera socialmente que no pueden evaluar
si algo está 'bien' o 'mal' fuera de lo que espera su círculo social.",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'6',
            'part'=>'6',
            'title'=>"",
            'desc'=>"倾向于窒息和过度保护
/n
控制和/或操纵的倾向
/n
对自己的需求不够重视
/n
倾向于批评与自己不相符的意见和态度
/n
有时不知道社交适当性或礼节
/n
对冲突极其敏感，倾向于将事情扫到地毯下作为逃避
战术
/n
当事情出错时倾向于责备自己，而不是在事情发生时给自己信任
向右走
/n
他们明确定义的价值体系使他们在某些领域不屈不挠
/n
他们可能非常适应社会接受或期望的事物，以至于他们无法评估
在他们的社交圈期望之外，某些事情是“对”还是“错”。",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'7',
            'title'=>"What does Success mean to an ENFJ?",
            'desc'=>"",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'7',
            'title'=>"ماذا يعني النجاح لـ ENFJ؟",
            'desc'=>"",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'7',
            'title'=>"Que signifie le succès pour un ENFJ ?",
            'desc'=>"",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'7',
            'title'=>"Bir ENFJ için Başarı ne anlama gelir?",
            'desc'=>"",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'7',
            'title'=>"Что означает успех для ENFJ?",
            'desc'=>"",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'7',
            'title'=>"Was bedeutet Erfolg für einen ENFJ?",
            'desc'=>"",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'7',
            'title'=>"¿Qué significa el éxito para un ENFJ?",
            'desc'=>"",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'7',
            'title'=>"成功对 ENFJ 意味着什么？",
            'desc'=>"",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'7',
            'part'=>'7',
            'title'=>"",
            'desc'=>"ENFJs are motivated by external human situations, primarily by other people; their talents, their needs, their aspirations and their cares forming the world in which an ENFJ lives. They thrive when able to “make things right” for others, to enable and empower their co-workers, friends and family through valuing their human strengths and abilities. When gifted with the added ENFJ ability to intuitively adapt their feelings to the way they are affected by others, the ENFJ has a positive drive to find co-operative pathways leading to the best possible outcome for all. Success for an ENFJ comes through involvement in the process of making things happen for people; through the accomplishments and satisfactions of those they have helped to enrich the human world with greater value, and through finding that their efforts on behalf of others have fulfilled their own life as well.",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'7',
            'part'=>'7',
            'title'=>"",
            'desc'=>"يتم تحفيز ENFJs من خلال المواقف البشرية الخارجية ، في المقام الأول من قبل أشخاص آخرين ؛ مواهبهم واحتياجاتهم وتطلعاتهم ورغباتهم تشكل العالم الذي تعيش فيه ENFJ. إنهم يزدهرون عندما يكونون قادرين على 'تصحيح الأمور' للآخرين ، لتمكين وتمكين زملائهم في العمل والأصدقاء والعائلة من خلال تقييم نقاط القوة والقدرات البشرية لديهم. عند منحها قدرة ENFJ المضافة على تكييف مشاعرهم بشكل حدسي مع الطريقة التي يتأثرون بها بالآخرين ، فإن ENFJ لديها دافع إيجابي لإيجاد مسارات تعاونية تؤدي إلى أفضل نتيجة ممكنة للجميع. يأتي نجاح ENFJ من خلال المشاركة في عملية جعل الأشياء تحدث للناس ؛ من خلال إنجازات ورضا أولئك الذين ساعدوا في إثراء العالم البشري بقيمة أكبر ، ومن خلال اكتشاف أن جهودهم لصالح الآخرين قد أكملت حياتهم أيضًا.",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'7',
            'part'=>'7',
            'title'=>"",
            'desc'=>"Les ENFJ sont motivés par des situations humaines extérieures, principalement par d'autres personnes ; leurs talents, leurs besoins, leurs aspirations et leurs soucis formant le monde dans lequel vit un ENFJ. Ils s'épanouissent lorsqu'ils sont capables de « faire les choses correctement » pour les autres, d'habiliter et de responsabiliser leurs collègues, amis et famille en valorisant leurs forces et leurs capacités humaines. Lorsqu'ils sont dotés de la capacité supplémentaire des ENFJ d'adapter intuitivement leurs sentiments à la façon dont ils sont affectés par les autres, les ENFJ ont une volonté positive de trouver des voies de coopération menant au meilleur résultat possible pour tous. Le succès d'un ENFJ passe par l'implication dans le processus pour faire bouger les choses pour les gens ; à travers les réalisations et les satisfactions de ceux qu'ils ont aidés à enrichir le monde humain d'une plus grande valeur, et en découvrant que leurs efforts en faveur des autres ont également rempli leur propre vie.",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'7',
            'part'=>'7',
            'title'=>"",
            'desc'=>"ENFJ'ler, başta diğer insanlar olmak üzere dış insani durumlar tarafından motive edilir; bir ENFJ'nin yaşadığı dünyayı oluşturan yetenekleri, ihtiyaçları, özlemleri ve kaygıları. İnsan güçlerine ve yeteneklerine değer vererek iş arkadaşlarına, arkadaşlarına ve ailelerine olanak sağlamak ve onları güçlendirmek için başkaları için 'işleri doğru hale getirebildiklerinde' gelişirler. ENFJ'nin duygularını başkalarından etkilenme biçimlerine sezgisel olarak uyarlama yeteneği eklendiğinde, ENFJ herkes için mümkün olan en iyi sonuca götüren işbirlikçi yollar bulmak için olumlu bir dürtüye sahiptir. Bir ENFJ için başarı, insanlar için bir şeyler yapma sürecine dahil olmakla gelir; insan dünyasını daha büyük bir değerle zenginleştirmeye yardım ettikleri kişilerin başarıları ve tatminleri aracılığıyla ve başkaları adına çabalarının kendi hayatlarını da yerine getirdiğini keşfederek.",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'7',
            'part'=>'7',
            'title'=>"",
            'desc'=>"ENFJ мотивированы внешними человеческими ситуациями, в первую очередь другими людьми; их таланты, их потребности, их стремления и их заботы формируют мир, в котором живет ENFJ. Они преуспевают, когда могут «делать все правильно» для других, помогать своим коллегам, друзьям и семье и расширять их возможности, ценив свои человеческие силы и способности. Обладая дополнительной способностью ENFJ интуитивно адаптировать свои чувства к тому, как на них влияют другие, ENFJ имеет позитивное стремление находить пути сотрудничества, ведущие к наилучшему возможному результату для всех. Успех ENFJ приходит через участие в процессе реализации вещей для людей; благодаря достижениям и удовлетворению тех, кому они помогли обогатить человеческий мир большей ценностью, и через обнаружение того, что их усилия в интересах других помогли реализовать и их собственную жизнь.",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'7',
            'part'=>'7',
            'title'=>"",
            'desc'=>"ENFJs werden durch externe menschliche Situationen motiviert, hauptsächlich durch andere Menschen; ihre Talente, ihre Bedürfnisse, ihre Bestrebungen und ihre Sorgen bilden die Welt, in der ein ENFJ lebt. Sie gedeihen, wenn sie in der Lage sind, für andere „die Dinge richtig zu machen“, ihre Mitarbeiter, Freunde und Familie zu befähigen und zu stärken, indem sie ihre menschlichen Stärken und Fähigkeiten schätzen. Ausgestattet mit der zusätzlichen Fähigkeit des ENFJ, seine Gefühle intuitiv der Art und Weise anzupassen, wie sie von anderen beeinflusst werden, hat das ENFJ einen positiven Antrieb, kooperative Wege zu finden, die zum bestmöglichen Ergebnis für alle führen. Der Erfolg eines ENFJ kommt durch die Beteiligung am Prozess, Dinge für die Menschen geschehen zu lassen; durch die Errungenschaften und Befriedigungen derer, die sie dazu beigetragen haben, die menschliche Welt um mehr Wert zu bereichern, und indem sie feststellen, dass ihre Bemühungen für andere auch ihr eigenes Leben erfüllt haben.",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'7',
            'part'=>'7',
            'title'=>"",
            'desc'=>"Los ENFJ están motivados por situaciones humanas externas, principalmente por otras personas; sus talentos, sus necesidades, sus aspiraciones y sus afanes forman el mundo en el que vive un ENFJ. Prosperan cuando son capaces de 'hacer las cosas bien' para los demás, para habilitar y empoderar a sus compañeros de trabajo, amigos y familiares mediante la valoración de sus fortalezas y habilidades humanas. Cuando se le otorga la capacidad adicional de ENFJ para adaptar intuitivamente sus sentimientos a la forma en que se ven afectados por los demás, el ENFJ tiene un impulso positivo para encontrar caminos cooperativos que conduzcan al mejor resultado posible para todos. El éxito de un ENFJ proviene de la participación en el proceso de hacer que las cosas sucedan para las personas; a través de los logros y satisfacciones de aquellos a quienes han ayudado a enriquecer el mundo humano con mayor valor, y al descubrir que sus esfuerzos en beneficio de los demás también han realizado su propia vida.",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'7',
            'part'=>'7',
            'title'=>"",
            'desc'=>"ENFJ 的动机是外部的人类情况，主要是其他人；他们的才能、需求、抱负和关心构成了 ENFJ 所生活的世界。当他们能够为他人“把事情做好”时，他们就会茁壮成长，通过重视他们的人的力量和能力来支持和授权他们的同事、朋友和家人。当 ENFJ 具有额外的能力，能够凭直觉调整他们的感受以适应他们受他人影响的方式时，ENFJ 有积极的动力去寻找合作途径，从而为所有人带来最好的结果。 ENFJ 的成功来自于参与为人们创造事物的过程；通过他们所帮助的那些人的成就和满足感，以更大的价值丰富了人类世界，并通过发现他们为他人所做的努力也实现了自己的生活。",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'8',
            'title'=>"Allowing Your ENFJ Strengths to Flourish",
            'desc'=>"",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'8',
            'title'=>"السماح لنقاط قوة ENFJ الخاصة بك بالازدهار",
            'desc'=>"",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'8',
            'title'=>"Permettre à vos forces ENFJ de s'épanouir",
            'desc'=>"",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'8',
            'title'=>"ENFJ Güçlü Yönlerinizin Gelişmesine İzin Vermek",
            'desc'=>"",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'8',
            'title'=>"Позвольте вашим сильным сторонам ENFJ процветать",
            'desc'=>"",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'8',
            'title'=>"Lassen Sie Ihre ENFJ-Stärken gedeihen",
            'desc'=>"",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'8',
            'title'=>"",
            'desc'=>"Permitir que florezcan sus fortalezas ENFJ",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'8',
            'title'=>"让您的 ENFJ 优势蓬勃发展",
            'desc'=>"",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'8',
            'part'=>'8',
            'title'=>"",
            'desc'=>"As an ENFJ, you have gifts that are specific to your personality type that aren't natural strengths for other types. By recognizing your special gifts and encouraging their growth and development, you will more readily see your place in the world, and be more content with your role. Nearly all ENFJs will recognize the following characteristics in themselves. They should embrace and nourish these strengths:",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'8',
            'part'=>'8',
            'title'=>"",
            'desc'=>"بصفتك ENFJ ، لديك هدايا خاصة بنوع شخصيتك والتي ليست نقاط قوة طبيعية للأنواع الأخرى. من خلال التعرف على مواهبك الخاصة وتشجيع نموها وتطورها ، سترى بسهولة مكانك في العالم ، وستكون أكثر رضا عن دورك. سوف تتعرف جميع ENFJs تقريبًا على الخصائص التالية في حد ذاتها. يجب أن يعتنقوا ويغذيوا نقاط القوة هذه:",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'8',
            'part'=>'8',
            'title'=>"",
            'desc'=>"En tant qu'ENFJ, vous avez des dons spécifiques à votre type de personnalité qui ne sont pas des atouts naturels pour les autres types. En reconnaissant vos dons spéciaux et en encourageant leur croissance et leur développement, vous verrez plus facilement votre place dans le monde et serez plus satisfait de votre rôle. Presque tous les ENFJ reconnaîtront en eux les caractéristiques suivantes. Ils devraient embrasser et nourrir ces forces :",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'8',
            'part'=>'8',
            'title'=>"",
            'desc'=>"Bir ENFJ olarak, diğer tipler için doğal olarak güçlü olmayan, kişilik tipinize özel hediyeleriniz var. Özel yeteneklerinizin farkına vararak ve onların büyümesini ve gelişmesini teşvik ederek, dünyadaki yerinizi daha kolay görecek ve rolünüzden daha fazla memnun kalacaksınız. Neredeyse tüm ENFJ'ler aşağıdaki özellikleri kendilerinde tanıyacaktır. Bu güçleri benimsemeli ve beslemelidirler:",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'8',
            'part'=>'8',
            'title'=>"",
            'desc'=>"Как ENFJ, у вас есть дары, характерные для вашего типа личности, которые не являются естественными сильными сторонами для других типов. Признавая свои особые дары и поощряя их рост и развитие, вы с большей готовностью увидите свое место в мире и будете более довольны своей ролью. Почти все ENFJ распознают в себе следующие характеристики. Они должны охватывать и питать эти сильные стороны:",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'8',
            'part'=>'8',
            'title'=>"",
            'desc'=>"Als ENFJ haben Sie Gaben, die spezifisch für Ihren Persönlichkeitstyp sind, die für andere Typen keine natürlichen Stärken sind. Indem Sie Ihre besonderen Gaben erkennen und ihr Wachstum und ihre Entwicklung fördern, werden Sie Ihren Platz in der Welt leichter erkennen und mit Ihrer Rolle zufriedener sein.Fast alle ENFJs werden die folgenden Eigenschaften an sich erkennen. Sie sollten diese Stärken annehmen und fördern:",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'8',
            'part'=>'8',
            'title'=>"",
            'desc'=>"Como ENFJ, tienes dones específicos para tu tipo de personalidad que no son fortalezas naturales para otros tipos. Al reconocer sus dones especiales y alentar su crecimiento y desarrollo, verá más fácilmente su lugar en el mundo y estará más satisfecho con su papel.Casi todos los ENFJ reconocerán las siguientes características en sí mismos. Deben abrazar y nutrir estas fortalezas:",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'8',
            'part'=>'8',
            'title'=>"",
            'desc'=>"作为一名 ENFJ，您拥有特定于您的性格类型的天赋，而这些天赋并不是其他类型的天生优势。通过认可您的特殊天赋并鼓励他们的成长和发展，您将更容易看到自己在世界上的位置，并对自己的角色更加满意。几乎所有 ENFJ 都会认识到自己的以下特征。他们应该拥抱和滋养这些优势：",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'8',
            'part'=>'8',
            'title'=>"",
            'desc'=>"Making others feel valued and important
/n
Quickly seeing the positive and negative aspects of a human situation
/n
Expressing their feelings clearly
/n
Offering loyalty and commitment to partners, family and work mates
/n
Trying to always find the solution which works for everyone
/n
Encouraging humor and self expression in others
/n
Finding ways to help others fulfill their needs
/n
Affirming positive community values
/n
Naturally falling into leadership roles in their community",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'8',
            'part'=>'8',
            'title'=>"",
            'desc'=>"جعل الآخرين يشعرون بالتقدير والأهمية
/ن
رؤية الجوانب الإيجابية والسلبية للوضع الإنساني بسرعة
/ن
التعبير عن مشاعرهم بوضوح
/ن
إبداء الولاء والالتزام للشركاء والأسرة وزملاء العمل
/ن
نحاول دائمًا إيجاد الحل الذي يناسب الجميع
/ن
تشجيع الفكاهة والتعبير عن الذات في الآخرين
/ن
إيجاد طرق لمساعدة الآخرين على تلبية احتياجاتهم
/ن
التأكيد على قيم المجتمع الإيجابية
/ن
الوقوع بشكل طبيعي في الأدوار القيادية في مجتمعهم",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'8',
            'part'=>'8',
            'title'=>"",
            'desc'=>"Faire en sorte que les autres se sentent valorisés et importants
/n
Voir rapidement les aspects positifs et négatifs d'une situation humaine
/n
Exprimer clairement ses sentiments
/n
Offrir loyauté et engagement envers les partenaires, la famille et les collègues de travail
/n
Essayer de toujours trouver la solution qui fonctionne pour tout le monde
/n
Encourager l'humour et l'expression de soi chez les autres
/n
Trouver des moyens d'aider les autres à répondre à leurs besoins
/n
Affirmer des valeurs communautaires positives
/n
Tomber naturellement dans des rôles de leadership dans leur communauté",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'8',
            'part'=>'8',
            'title'=>"",
            'desc'=>"Başkalarını değerli ve önemli hissettirmek
/n
Bir insan durumunun olumlu ve olumsuz yönlerini hızla görmek
/n
Duygularını açıkça ifade etme
/n
Ortaklara, aileye ve iş arkadaşlarına sadakat ve bağlılık sunmak
/n
Her zaman herkes için işe yarayan çözümü bulmaya çalışmak
/n
Başkalarında mizahı ve kendini ifade etmeyi teşvik etmek
/n
Başkalarının ihtiyaçlarını karşılamalarına yardımcı olacak yollar bulmak
/n
Olumlu topluluk değerlerini onaylamak
/n
Doğal olarak topluluklarında liderlik rollerine düşüyorlar",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'8',
            'part'=>'8',
            'title'=>"",
            'desc'=>"Заставить других почувствовать себя ценными и важными
/п
Быстро видеть положительные и отрицательные аспекты человеческой ситуации
/п
Четко выражать свои чувства
/п
Предлагая лояльность и приверженность партнерам, семье и коллегам по работе
/п
Стараемся всегда найти решение, подходящее для всех
/п
Поощрение юмора и самовыражения в других
/п
Поиск способов помочь другим удовлетворить их потребности
/п
Утверждение позитивных ценностей сообщества
/п
Естественное попадание на руководящие роли в своем сообществе",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'8',
            'part'=>'8',
            'title'=>"",
            'desc'=>"Anderen das Gefühl geben, wertgeschätzt und wichtig zu sein
/n
Schnell die positiven und negativen Aspekte einer menschlichen Situation erkennen
/n
Ihre Gefühle klar ausdrücken
/n
Loyalität und Engagement für Partner, Familie und Arbeitskollegen anbieten
/n
Versuche immer die Lösung zu finden, die für alle funktioniert
/n
Förderung von Humor und Selbstausdruck bei anderen
/n
Wege finden, anderen zu helfen, ihre Bedürfnisse zu erfüllen
/n
Bekräftigung positiver Gemeinschaftswerte
/n
Natürlich in Führungsrollen in ihrer Gemeinschaft fallen",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'8',
            'part'=>'8',
            'title'=>"",
            'desc'=>"Hacer que los demás se sientan valorados e importantes
/n
Ver rápidamente los aspectos positivos y negativos de una situación humana.
/n
Expresando sus sentimientos con claridad
/n
Ofreciendo lealtad y compromiso a socios, familiares y compañeros de trabajo.
/n
Tratando de encontrar siempre la solución que funcione para todos
/n
Fomentar el humor y la autoexpresión en los demás.
/n
Encontrar formas de ayudar a otros a satisfacer sus necesidades.
/n
Afirmar valores comunitarios positivos
/n
Caer naturalmente en roles de liderazgo en su comunidad.",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'8',
            'part'=>'8',
            'title'=>"",
            'desc'=>"让别人觉得自己有价值和重要
/n
快速看到人类处境的积极和消极方面
/n
清楚地表达他们的感受
/n
为合作伙伴、家人和同事提供忠诚和承诺
/n
试图始终找到适合所有人的解决方案
/n
鼓励他人的幽默和自我表达
/n
寻找帮助他人满足需求的方法
/n
肯定积极的社区价值观
/n
自然而然地在他们的社区中担任领导角色",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'9',
            'title'=>"ENFJs who have developed their Introverted Intuition to the extent that they can see the possibilities within their perceptions will enjoy these very special gifts:",
            'desc'=>"",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'9',
            'title'=>"سيستمتع ENFJs الذين طوروا حدسهم الانطوائي إلى الحد الذي يمكنهم من رؤية الاحتمالات ضمن تصوراتهم بهذه الهدايا الخاصة جدًا:",
            'desc'=>"",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'9',
            'title'=>"Les ENFJ qui ont développé leur intuition introvertie dans la mesure où ils peuvent voir les possibilités au sein de leurs perceptions apprécieront ces cadeaux très spéciaux :",
            'desc'=>"",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'9',
            'title'=>"İçe Dönük Sezgilerini, olasılıkları algılarında görebilecekleri ölçüde geliştiren ENFJ'ler, bu çok özel hediyelerin tadını çıkaracaklar:",
            'desc'=>"",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'9',
            'title'=>"ENFJ, которые развили свою интровертную интуицию до такой степени, что они могут видеть возможности в рамках своего восприятия, будут пользоваться этими особыми дарами:",
            'desc'=>"",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'9',
            'title'=>"ENFJs, die ihre introvertierte Intuition so weit entwickelt haben, dass sie die Möglichkeiten in ihren Wahrnehmungen sehen können, werden diese ganz besonderen Gaben genießen:",
            'desc'=>"",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'9',
            'title'=>"Los ENFJ que han desarrollado su intuición introvertida en la medida en que pueden ver las posibilidades dentro de sus percepciones disfrutarán de estos dones muy especiales:",
            'desc'=>"",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'9',
            'title'=>"ENFJ 的内向直觉已经发展到可以看到他们感知中的可能性的程度，他们将享受这些非常特殊的礼物：",
            'desc'=>"",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'9',
            'part'=>'9',
            'title'=>"",
            'desc'=>"Understanding and empathizing with the feelings of others; realizing “where they are coming from”.
/n
A talent for creative expression which can turn ordinary things and situations into something magical.
/n
An enhanced feeling of connection with and sensitivity to the world around them.
/n
The ability to see many facets of a problem and the many ways it might be resolved for the best.
/n
The ability to make creative and valuable use of time spent alone.
/n
Openness to the spiritual connections between all things
/n
They become increasingly creative, visionary and empathetic, and are therefore effective and kind
managers of businesses, people, and various situations that life presents.",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'9',
            'part'=>'9',
            'title'=>"",
            'desc'=>"فهم مشاعر الآخرين والتعاطف معها ؛ إدراك 'من أين أتوا'.
/n
موهبة التعبير الإبداعي التي يمكنها تحويل الأشياء والمواقف العادية إلى شيء سحري.
/n
شعور معزز بالاتصال والحساسية تجاه العالم من حولهم.
/n
القدرة على رؤية العديد من جوانب المشكلة والطرق العديدة التي يمكن حلها للأفضل.
/n
القدرة على الاستفادة الإبداعية والقيمة للوقت الذي يقضيه الشخص بمفرده.
/n
الانفتاح على الروابط الروحية بين كل الأشياء
/n
يصبحون أكثر إبداعًا ورؤية وعاطفة ، وبالتالي فهم فعالون ولطيفون
مديري الأعمال والأفراد والمواقف المختلفة التي تطرحها الحياة.",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'9',
            'part'=>'9',
            'title'=>"",
            'desc'=>"Compréhension et empathie avec les sentiments des autres; réalisant 'd'où ils viennent'.
/n
Un talent pour l'expression créative qui peut transformer des choses et des situations ordinaires en quelque chose de magique.
/n
Un sentiment accru de connexion et de sensibilité au monde qui les entoure.
/n
La capacité de voir les nombreuses facettes d'un problème et les nombreuses façons de le résoudre au mieux.
/n
La capacité de faire un usage créatif et précieux du temps passé seul.
/n
Ouverture aux connexions spirituelles entre toutes choses
/n
Ils deviennent de plus en plus créatifs, visionnaires et empathiques, et sont donc efficaces et bienveillants
gestionnaires d'entreprises, de personnes et de diverses situations que la vie présente.",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'9',
            'part'=>'9',
            'title'=>"",
            'desc'=>"Başkalarının duygularını anlama ve empati kurma; “nereden geldiklerini” anlamak.
/n
Sıradan şeyleri ve durumları büyülü bir şeye dönüştürebilen yaratıcı ifade yeteneği.
/n
Çevrelerindeki dünyayla gelişmiş bir bağlantı ve duyarlılık duygusu.
/n
Bir problemin birçok yönünü ve en iyi şekilde çözülebileceği birçok yolu görme yeteneği.
/n
Yalnız geçirilen zamanı yaratıcı ve değerli kullanma yeteneği.
/n
Her şey arasındaki ruhsal bağlantılara açıklık
/n
Giderek daha yaratıcı, vizyoner ve empatik hale gelirler ve bu nedenle etkili ve kibardırlar.
İşletmelerin yöneticileri, insanlar ve hayatın sunduğu çeşitli durumlar.",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'9',
            'part'=>'9',
            'title'=>"",
            'desc'=>"Понимание и сочувствие чувствам других; понимая, «откуда они».
/п
Талант к творческому самовыражению, который может превращать обычные вещи и ситуации в нечто волшебное.
/п
Повышенное чувство связи с окружающим миром и чувствительность к нему.
/п
Способность видеть многие аспекты проблемы и множество способов ее решения к лучшему.
/п
Способность творчески и с пользой использовать время, проведенное в одиночестве.
/п
Открытость духовным связям между всеми вещами
/п
Они становятся все более творческими, дальновидными и чуткими, а значит, эффективными и добрыми.
менеджеры предприятий, люди и различные жизненные ситуации.",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'9',
            'part'=>'9',
            'title'=>"",
            'desc'=>"Verstehen und Einfühlen in die Gefühle anderer; erkennen, „woher sie kommen“.
/n
Ein Talent zum kreativen Ausdruck, das gewöhnliche Dinge und Situationen in etwas Magisches verwandeln kann.
/n
Ein verstärktes Gefühl der Verbundenheit und Sensibilität für die Welt um sie herum.
/n
Die Fähigkeit, viele Facetten eines Problems zu sehen und die vielen Möglichkeiten, es zum Besten zu lösen.
/n
Die Fähigkeit, die allein verbrachte Zeit kreativ und wertvoll zu nutzen.
/n
Offenheit für die spirituellen Verbindungen zwischen allen Dingen
/n
Sie werden immer kreativer, visionärer und einfühlsamer und sind daher effektiv und freundlich
Manager von Unternehmen, Menschen und verschiedenen Situationen, die das Leben bietet.",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'9',
            'part'=>'9',
            'title'=>"",
            'desc'=>"Comprender y sentir empatía por los sentimientos de los demás; dándose cuenta de 'de dónde vienen'.
/n
Un talento para la expresión creativa que puede convertir cosas y situaciones cotidianas en algo mágico.
/n
Una mayor sensación de conexión y sensibilidad con el mundo que los rodea.
/n
La capacidad de ver muchas facetas de un problema y las muchas formas en que podría resolverse de la mejor manera.
/n
La capacidad de hacer un uso creativo y valioso del tiempo que se pasa a solas.
/n
Apertura a las conexiones espirituales entre todas las cosas.
/n
Se vuelven cada vez más creativos, visionarios y empáticos, por lo que son eficaces y amables.
gerentes de negocios, personas y situaciones diversas que presenta la vida.",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'9',
            'part'=>'9',
            'title'=>"",
            'desc'=>"理解和同情他人的感受；意识到“他们来自哪里”。
/n
一种创造性表达的才能，可以将平凡的事物和情况变成神奇的事物。
/n
增强与周围世界的联系和敏感性。
/n
能够看到问题的许多方面以及可能以最佳方式解决问题的多种方法。
/n
能够创造性地和有价值地利用独处的时间。
/n
对万物之间的精神联系持开放态度
/n
他们变得越来越有创造力、远见和善解人意，因此变得有效和善良
企业、人员和生活中出现的各种情况的管理者。",
            'lang'=>'cn'
        ] );

         personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'10',
            'title'=>"Potential Problem Areas",
            'desc'=>"",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'10',
            'title'=>"مجالات المشاكل المحتملة",
            'desc'=>"",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'10',
            'title'=>"Problèmes potentiels",
            'desc'=>"",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'10',
            'title'=>"Potansiyel Sorun Alanları",
            'desc'=>"",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'10',
            'title'=>"Возможные проблемные области",
            'desc'=>"",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'10',
            'title'=>"Potenzielle Problembereiche",
            'desc'=>"",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'10',
            'title'=>"Posibles áreas problemáticas",
            'desc'=>"",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'10',
            'title'=>"潜在的问题领域",
            'desc'=>"",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'10',
            'part'=>'10',
            'title'=>"",
            'desc'=>"With any gift of strength, there is an associated weakness. Without “bad”, there would be no “good”. Without “difficult”, there would be no “easy”. We value our strengths, but we often curse and ignore our weaknesses. To grow as a person and get what we want out of life, we must not only capitalize upon our strengths, but also face our weaknesses and deal with them. That means taking a hard look at our personality type's potential problem areas.
Most of the weaker characteristics found in ENFJs are due to their dominant Extraverted Feeling overvaluing what they see as objective values in the external world and thereby judging too much by the needs of others, or by appearances. This is primarily due to the ENFJ having not fully adapted their Introverted Intuitive function sufficiently for them to be able to discern the vast range of ways in which they might be being missing the underlying needs within themselves and being misled by such appearances. The ENFJ naturally looks outward to find value and satisfaction, and whilst it is essential that this direction be taken to fulfill their primary needs of relation and comfort, without the supportive balance of a well developed Intuitive function, ENFJs can overvalue the external world to the point where they lose sight of themselves, becoming fixed in their judgments about people and the world. In such cases, the ENFJ will tend to live in a rigid - and to others, somewhat surreal - world of definite values which often seems “overstated” or obsessively connected to other people or human situations.",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'10',
            'part'=>'10',
            'title'=>"",
            'desc'=>"مع أي موهبة من القوة ، هناك ضعف مرتبط. بدون 'سيء' ، لن يكون هناك 'خير'. بدون 'صعب' ، لن يكون هناك 'سهل'. نحن نقدر نقاط قوتنا ، لكننا غالبًا ما نلعن ونتجاهل نقاط ضعفنا. لكي ننمو كشخص ونحصل على ما نريده من الحياة ، يجب ألا نستفيد فقط من نقاط قوتنا ، ولكن أيضًا نواجه نقاط ضعفنا ونتعامل معها. هذا يعني إلقاء نظرة فاحصة على مجالات المشاكل المحتملة لنوع شخصيتنا.
ترجع معظم الخصائص الأضعف الموجودة في ENFJs إلى شعورهم المهيمن الانبساطي المبالغة في تقدير ما يرونه قيمًا موضوعية في العالم الخارجي ، وبالتالي الحكم على الكثير من احتياجات الآخرين ، أو من خلال المظاهر. هذا يرجع في المقام الأول إلى أن ENFJ لم تكيف بشكل كامل وظيفتها البديهية الانطوائية بشكل كافٍ حتى يتمكنوا من تمييز مجموعة واسعة من الطرق التي قد يفقدون فيها الاحتياجات الأساسية داخل أنفسهم ويتم تضليلهم من خلال مثل هذه المظاهر. تتطلع ENFJ بشكل طبيعي إلى الخارج للعثور على القيمة والرضا ، وفي حين أنه من الضروري أن يتم اتخاذ هذا الاتجاه لتلبية احتياجاتهم الأساسية للعلاقة والراحة ، بدون التوازن الداعم لوظيفة حدسية مطورة جيدًا ، يمكن لـ ENFJ المبالغة في تقدير العالم الخارجي إلى النقطة التي يفقدون فيها بصرهم عن أنفسهم ، ويصبحون ثابتين في أحكامهم عن الناس والعالم. في مثل هذه الحالات ، تميل ENFJ إلى العيش في عالم جامد - وللآخرين ، سريالي إلى حد ما - من القيم المحددة التي غالبًا ما تبدو 'مبالغًا فيها' أو مرتبطة بشكل هوس بأشخاص آخرين أو مواقف بشرية.",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'10',
            'part'=>'10',
            'title'=>"",
            'desc'=>"Avec tout don de force, il y a une faiblesse associée. Sans 'mauvais', il n'y aurait pas de 'bien'. Sans 'difficile', il n'y aurait pas de 'facile'. Nous valorisons nos forces, mais nous maudissons et ignorons souvent nos faiblesses. Pour grandir en tant que personne et obtenir ce que nous voulons de la vie, nous devons non seulement capitaliser sur nos forces, mais aussi faire face à nos faiblesses et y faire face. Cela signifie examiner de près les problèmes potentiels de notre type de personnalité.
La plupart des caractéristiques les plus faibles trouvées chez les ENFJ sont dues à leur sentiment extraverti dominant surévaluant ce qu'ils considèrent comme des valeurs objectives dans le monde extérieur et jugeant ainsi trop par les besoins des autres, ou par les apparences. Cela est principalement dû au fait que les ENFJ n'ont pas suffisamment adapté leur fonction intuitive introvertie pour qu'ils puissent discerner le vaste éventail de façons dont ils pourraient manquer les besoins sous-jacents en eux-mêmes et être induits en erreur par de telles apparences. Les ENFJ regardent naturellement vers l'extérieur pour trouver de la valeur et de la satisfaction, et bien qu'il soit essentiel que cette direction soit prise pour répondre à leurs besoins primaires de relation et de confort, sans l'équilibre favorable d'une fonction intuitive bien développée, les ENFJ peuvent surévaluer le monde extérieur pour le point où ils se perdent de vue, se figent dans leurs jugements sur les gens et le monde. Dans de tels cas, les ENFJ auront tendance à vivre dans un monde rigide - et pour d'autres, quelque peu surréaliste - de valeurs définies qui semblent souvent 'surestimées' ou liées de manière obsessionnelle à d'autres personnes ou situations humaines.",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'10',
            'part'=>'10',
            'title'=>"",
            'desc'=>"Herhangi bir güç armağanı ile ilişkili bir zayıflık vardır. 'Kötü' olmasaydı, 'iyi' olmazdı. “Zor” olmadan, “kolay” olmazdı. Güçlü yönlerimize değer veririz, ancak çoğu zaman zayıf yönlerimizi lanetler ve görmezden geliriz. Bir insan olarak büyümek ve hayattan istediklerimizi elde etmek için sadece güçlü yönlerimizden yararlanmakla kalmamalı, aynı zamanda zayıflıklarımızla yüzleşmeli ve onlarla başa çıkmalıyız. Bu, kişilik tipimizin potansiyel sorun alanlarına yakından bakmak anlamına gelir.
ENFJ'lerde bulunan daha zayıf özelliklerin çoğu, dış dünyada nesnel değerler olarak gördüklerine aşırı değer vermeleri ve dolayısıyla başkalarının ihtiyaçlarına veya görünüşlerine göre çok fazla yargıda bulunmalarındaki baskın Dışa Dönük Duygularından kaynaklanmaktadır. Bu öncelikle, ENFJ'nin İçe Dönük Sezgisel işlevini, kendi içlerindeki temel ihtiyaçları kaçırabilecekleri ve bu tür görünüşler tarafından yanlış yönlendirilebilecekleri çok çeşitli yolları ayırt edebilmeleri için yeterince uyarlamamış olmasından kaynaklanmaktadır. ENFJ, değer ve tatmin bulmak için doğal olarak dışa bakar ve iyi gelişmiş bir Sezgisel işlevin destekleyici dengesi olmadan, birincil ilişki ve rahatlık ihtiyaçlarını karşılamak için bu yönün alınması gerekli olsa da, ENFJ'ler dış dünyaya aşırı değer verebilir. Kendilerini gözden kaybettikleri, insanlar ve dünya hakkındaki yargılarında sabit kaldıkları nokta. Bu gibi durumlarda, ENFJ, genellikle “abartılı” görünen veya takıntılı bir şekilde diğer insanlarla veya insan durumlarıyla bağlantılı görünen katı - ve diğerlerine göre biraz gerçeküstü - belirli değerler dünyasında yaşama eğiliminde olacaktır.",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'10',
            'part'=>'10',
            'title'=>"",
            'desc'=>"С любым даром силы связана слабость. Без «плохого» не было бы «хорошего». Без «трудного» не было бы «легкого». Мы ценим свои сильные стороны, но часто проклинаем и игнорируем свои слабости. Чтобы расти как личность и получать от жизни то, чего мы хотим, мы должны не только использовать свои сильные стороны, но и противостоять нашим слабостям и бороться с ними. Это означает, что нужно внимательно посмотреть на потенциальные проблемные области нашего типа личности.
Большинство более слабых характеристик, обнаруживаемых у ENFJ, обусловлено их доминирующим экстравертным чувством, которое переоценивает то, что они считают объективными ценностями во внешнем мире, и тем самым слишком много судит по потребностям других или по внешнему виду. Это в первую очередь связано с тем, что ENFJ не полностью адаптировали свою интровертную интуитивную функцию в достаточной степени, чтобы они могли различать широкий спектр способов, которыми они могут упускать основные потребности внутри себя и вводиться в заблуждение из-за такой внешности. ENFJ, естественно, смотрят вовне, чтобы найти ценность и удовлетворение, и хотя важно, чтобы это направление было взято для удовлетворения их основных потребностей в отношениях и комфорте, без поддерживающего баланса хорошо развитой интуитивной функции, ENFJ могут переоценить внешний мир для точка, в которой они теряют из виду себя, фиксируются в своих суждениях о людях и мире. В таких случаях ENFJ будет иметь тенденцию жить в жестком - и для других несколько сюрреалистическом - мире определенных ценностей, который часто кажется «завышенным» или навязчиво связанным с другими людьми или человеческими ситуациями.",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'10',
            'part'=>'10',
            'title'=>"",
            'desc'=>"Mit jeder Gabe von Stärke ist eine Schwäche verbunden. Ohne „schlecht“ gäbe es kein „gut“. Ohne „schwierig“ gäbe es kein „einfach“. Wir schätzen unsere Stärken, verfluchen aber oft und ignorieren unsere Schwächen. Um als Person zu wachsen und das zu erreichen, was wir uns vom Leben wünschen, müssen wir nicht nur unsere Stärken nutzen, sondern uns auch unseren Schwächen stellen und mit ihnen umgehen. Das bedeutet, die potentiellen Problembereiche unseres Persönlichkeitstyps genau unter die Lupe zu nehmen.
Die meisten der schwächeren Eigenschaften von ENFJs sind darauf zurückzuführen, dass ihr dominantes extravertiertes Gefühl das, was sie als objektive Werte in der Außenwelt betrachten, überbewertet und dadurch zu sehr nach den Bedürfnissen anderer oder nach dem Aussehen beurteilt wird. Dies liegt in erster Linie daran, dass die ENFJ ihre introvertierte intuitive Funktion nicht ausreichend angepasst hat, um in der Lage zu sein, die breite Palette von Möglichkeiten zu erkennen, auf denen sie die zugrunde liegenden Bedürfnisse in sich selbst vermissen und durch solche Erscheinungen in die Irre geführt werden. Der ENFJ schaut natürlich nach außen, um Wert und Zufriedenheit zu finden, und obwohl es wichtig ist, dass diese Richtung eingeschlagen wird, um seine primären Bedürfnisse nach Beziehung und Komfort zu erfüllen, können ENFJs ohne das unterstützende Gleichgewicht einer gut entwickelten intuitiven Funktion die Außenwelt überbewerten Punkt, an dem sie sich selbst aus den Augen verlieren und sich in ihren Urteilen über Menschen und die Welt festsetzen. In solchen Fällen wird die ENFJ dazu neigen, in einer starren – und für andere etwas surrealen – Welt bestimmter Werte zu leben, die oft „übertrieben“ oder obsessiv mit anderen Menschen oder menschlichen Situationen verbunden erscheint.",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'10',
            'part'=>'10',
            'title'=>"",
            'desc'=>"Con cualquier don de fuerza, hay una debilidad asociada. Sin 'mal', no habría 'bien'. Sin 'difícil', no habría 'fácil'. Valoramos nuestras fortalezas, pero a menudo maldecimos e ignoramos nuestras debilidades. Para crecer como persona y obtener lo que queremos de la vida, no solo debemos capitalizar nuestras fortalezas, sino también enfrentar nuestras debilidades y lidiar con ellas. Eso significa examinar detenidamente las áreas potencialmente problemáticas de nuestro tipo de personalidad.
La mayoría de las características más débiles que se encuentran en los ENFJ se deben a que su sentimiento extravertido dominante sobrevalora lo que ven como valores objetivos en el mundo externo y, por lo tanto, juzga demasiado por las necesidades de los demás o por las apariencias. Esto se debe principalmente a que el ENFJ no ha adaptado completamente su función intuitiva introvertida lo suficiente como para que puedan discernir la amplia gama de formas en las que podrían estar perdiendo las necesidades subyacentes dentro de sí mismos y ser engañados por tales apariencias. El ENFJ naturalmente mira hacia afuera para encontrar valor y satisfacción, y si bien es esencial que se tome esta dirección para satisfacer sus necesidades primarias de relación y comodidad, sin el equilibrio de apoyo de una función intuitiva bien desarrollada, los ENFJ pueden sobrevalorar el mundo externo para el punto en el que se pierden de vista, fijándose en sus juicios sobre las personas y el mundo. En tales casos, la ENFJ tenderá a vivir en un mundo rígido - y para otros, algo surrealista - de valores definidos que a menudo parece 'exagerado' o conectado obsesivamente con otras personas o situaciones humanas.",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'10',
            'part'=>'10',
            'title'=>"",
            'desc'=>"任何力量的礼物，都有一个相关的弱点。没有“坏”，就没有“好”。没有“难”，就没有“易”。我们重视自己的优势，但我们经常诅咒和忽视自己的弱点。要成长为一个人并从生活中得到我们想要的东西，我们不仅要利用自己的优势，还要正视自己的弱点并加以应对。这意味着要认真审视我们性格类型的潜在问题领域。
在 ENFJ 中发现的大多数较弱的特征是由于他们占主导地位的外倾情感高估了他们在外部世界中看到的客观价值，从而过多地根据他人的需要或外表来判断。这主要是因为 ENFJ 还没有完全适应他们内向的直觉功能，使他们能够辨别出他们可能会错过自己内在的潜在需求并被这种外表误导的各种方式。 ENFJ 自然地向外寻找价值和满足感，虽然必须采取这个方向来满足他们对关系和舒适的主要需求，但如果没有发达的直觉功能的支持性平衡，ENFJ 可能会高估外部世界的价值和满足感。他们失去了自我，对人和世界的判断变得固执。在这种情况下，ENFJ 将倾向于生活在一个僵化的——对其他人来说，有些超现实的——具有确定价值观的世界，这些价值观往往似乎“被夸大了”或与其他人或人类情况过分地联系在一起。",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'11',
            'title'=>"Explanation of Problems",
            'desc'=>"",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'11',
            'title'=>"شرح المشاكل",
            'desc'=>"",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'11',
            'title'=>"Explication des problèmes",
            'desc'=>"",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'11',
            'title'=>"Sorunların Açıklaması",
            'desc'=>"",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'11',
            'title'=>"Объяснение проблем",
            'desc'=>"",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'11',
            'title'=>"Erklärung der Probleme",
            'desc'=>"",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'11',
            'title'=>"Explicación de problemas",
            'desc'=>"",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'11',
            'title'=>"问题说明",
            'desc'=>"",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'11',
            'part'=>'11',
            'title'=>"",
            'desc'=>"Nearly all of the problematic characteristics described above can be attributed in various degrees to the common ENFJ problem of wanting to find the “proper” value in everything. If the ENFJ does not learn how to see beneath the appearance of what they quickly judge as good or bad about the people and situations in their external environment, they will only use their introverted intuition to support those judgments they feel are good for them and disregard not only other possibilities but their own quality of inner life as well. The consideration of these less obvious possibilities and their own needs requires that the ENFJ recognize that their own value judgments are indeed subjective, and that it is not appropriate or effective to apply them across the board to all civilized people. The practice of standing back and looking objectively at their own value system is not something that the ENFJ is accustomed to doing; trying to avoid abstract rationalization of problems and the feelings they engender is a natural survival technique for the ENFJ personality. The main driver to the ENFJ personality is Extraverted Feeling, whose purpose is above all to find and discriminate the values in people and human situations. If their ability to find a specific and worthy value in a person or situation is threatened, the ENFJ shuts out the threatening force. This is totally natural, but unfortunately the individual who exercises this type of agenda protection regularly will become more and more rigid in their judgments and expectations of people, but even less concerned with the effect such conditions have upon themselves. Where the unbalanced ENFJ does acquiesce to the images of intuition, these will generally be skewed to support the subjective agenda of dominant Feeling. In this way they always find justification for their determinations and their self sacrifices to people, things and situations, and they will be unable locate the reality of another’s true feelings, nor be interested in discovering that their seemingly objective judgments miss the reasons and subjectivities underlying both their own and others lives or worldly situation.",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'11',
            'part'=>'11',
            'title'=>"",
            'desc'=>"يمكن أن تُعزى جميع الخصائص الإشكالية الموضحة أعلاه تقريبًا بدرجات مختلفة إلى مشكلة ENFJ الشائعة المتمثلة في الرغبة في العثور على القيمة 'المناسبة' في كل شيء. إذا لم تتعلم ENFJ كيف ترى ما وراء ظهور ما يحكمون عليه بسرعة على أنه جيد أو سيئ حول الأشخاص والمواقف في بيئتهم الخارجية ، فسوف يستخدمون فقط حدسهم الانطوائي لدعم تلك الأحكام التي يشعرون أنها جيدة لهم وتجاهلها ليس فقط الاحتمالات الأخرى ولكن أيضًا نوعية الحياة الداخلية الخاصة بهم. يتطلب النظر في هذه الاحتمالات الأقل وضوحًا واحتياجاتها الخاصة أن تدرك ENFJ أن أحكامها القيمية ذاتية بالفعل ، وأنه ليس من المناسب أو الفعال تطبيقها في جميع المجالات على جميع الأشخاص المتحضرين. إن ممارسة الوقوف إلى الوراء والنظر بموضوعية إلى نظام القيم الخاص بهم ليس شيئًا اعتادت ENFJ على القيام به ؛ محاولة تجنب التبرير المجرد للمشاكل والمشاعر التي تولدها هي تقنية بقاء طبيعية لشخصية ENFJ. الدافع الرئيسي لشخصية ENFJ هو الشعور الانبساطي ، والذي يهدف قبل كل شيء إلى إيجاد وتمييز القيم في الأشخاص والمواقف الإنسانية. إذا كانت قدرتهم على العثور على قيمة محددة وقيمة في شخص أو موقف مهددة ، فإن ENFJ تغلق قوة التهديد. هذا أمر طبيعي تمامًا ، ولكن للأسف فإن الفرد الذي يمارس هذا النوع من حماية جدول الأعمال بانتظام سيصبح أكثر فأكثر جامدًا في أحكامه وتوقعاته من الناس ، ولكنه أقل اهتمامًا بتأثير مثل هذه الظروف على أنفسهم. عندما يرضخ ENFJ غير المتوازن لصور الحدس ، فإن هذه ستنحرف عمومًا لدعم الأجندة الذاتية للشعور السائد. بهذه الطريقة يجدون دائمًا مبررًا لقراراتهم وتضحياتهم الذاتية للناس والأشياء والمواقف ، ولن يكونوا قادرين على تحديد حقيقة مشاعر الآخرين الحقيقية ، ولن يكونوا مهتمين باكتشاف أن أحكامهم التي تبدو موضوعية تفوت الأسباب والذاتية الكامنة وراءها. كل من حياتهم الخاصة وحياة الآخرين أو الوضع الدنيوي.",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'11',
            'part'=>'11',
            'title'=>"",
            'desc'=>"Presque toutes les caractéristiques problématiques décrites ci-dessus peuvent être attribuées à divers degrés au problème commun de l'ENFJ consistant à vouloir trouver la valeur « correcte » dans tout. Si l'ENFJ n'apprend pas à voir sous l'apparence de ce qu'il juge rapidement bon ou mauvais sur les personnes et les situations de son environnement extérieur, il n'utilisera son intuition introvertie que pour étayer les jugements qu'il juge bon pour lui et méprise. non seulement d'autres possibilités, mais aussi leur propre qualité de vie intérieure. La prise en compte de ces possibilités moins évidentes et de leurs propres besoins exige que l'ENFJ reconnaisse que leurs propres jugements de valeur sont en effet subjectifs, et qu'il n'est pas approprié ou efficace de les appliquer de manière globale à toutes les personnes civilisées. La pratique de prendre du recul et de regarder objectivement leur propre système de valeurs n'est pas quelque chose à laquelle l'ENFJ est habituée ; essayer d'éviter la rationalisation abstraite des problèmes et des sentiments qu'ils engendrent est une technique de survie naturelle pour la personnalité ENFJ. Le moteur principal de la personnalité ENFJ est le sentiment extraverti, dont le but est avant tout de trouver et de discriminer les valeurs dans les personnes et les situations humaines. Si leur capacité à trouver une valeur spécifique et digne dans une personne ou une situation est menacée, l'ENFJ exclut la force menaçante. C'est tout à fait naturel, mais malheureusement, l'individu qui exerce régulièrement ce type de protection de l'agenda deviendra de plus en plus rigide dans ses jugements et ses attentes vis-à-vis des gens, mais encore moins préoccupé par l'effet que de telles conditions ont sur lui-même. Lorsque l'ENFJ déséquilibré acquiesce aux images de l'intuition, celles-ci seront généralement faussées pour soutenir l'agenda subjectif du sentiment dominant. De cette façon, ils trouvent toujours la justification de leurs déterminations et de leurs sacrifices personnels envers les personnes, les choses et les situations, et ils seront incapables de localiser la réalité des vrais sentiments d'autrui, ni seront intéressés à découvrir que leurs jugements apparemment objectifs manquent les raisons et les subjectivités sous-jacentes. à la fois leur propre vie et celle des autres ou leur situation mondaine.",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'11',
            'part'=>'11',
            'title'=>"",
            'desc'=>"Yukarıda açıklanan sorunlu özelliklerin hemen hemen tümü, her şeyde 'uygun' değeri bulma isteği şeklindeki yaygın ENFJ sorununa çeşitli derecelerde atfedilebilir. ENFJ, dış çevrelerindeki insanlar ve durumlar hakkında hızlı bir şekilde iyi veya kötü olarak yargıladıkları şeylerin görünümünün altında nasıl göreceğini öğrenmezse, sadece içe dönük sezgilerini kendileri için iyi olduğunu düşündükleri yargıları desteklemek için kullanacak ve göz ardı edeceklerdir. sadece diğer olasılıkları değil, aynı zamanda kendi iç yaşam kalitelerini de. Bu daha az belirgin olasılıkların ve kendi ihtiyaçlarının değerlendirilmesi, ENFJ'nin kendi değer yargılarının gerçekten öznel olduğunu ve bunları tüm uygar insanlara genel olarak uygulamanın uygun veya etkili olmadığını kabul etmesini gerektirir. Geride durup kendi değer sistemlerine objektif olarak bakma pratiği, ENFJ'nin yapmaya alışık olduğu bir şey değildir; Sorunların soyut rasyonelleştirilmesinden ve bunların doğurduğu duygulardan kaçınmaya çalışmak, ENFJ kişiliği için doğal bir hayatta kalma tekniğidir. ENFJ kişiliğinin ana itici gücü, amacı her şeyden önce insanlar ve insan durumlarındaki değerleri bulmak ve ayırt etmek olan Dışa Dönük Duygu'dur. Bir kişi veya durumda belirli ve değerli bir değer bulma yetenekleri tehdit edilirse, ENFJ tehdit eden gücü kapatır. Bu tamamen doğaldır, ancak ne yazık ki bu tür gündem korumasını düzenli olarak uygulayan kişi, yargılarında ve insanlardan beklentilerinde giderek daha katı hale gelecek, ancak bu tür koşulların kendileri üzerindeki etkisi ile daha da az ilgilenecektir. Dengesiz ENFJ'nin sezgi imgelerine boyun eğdiği durumlarda, bunlar genellikle baskın Duygu'nun öznel gündemini desteklemek için çarpık olacaktır. Bu şekilde, insanlar, şeyler ve durumlar için kararlılıkları ve özverileri için her zaman gerekçe bulurlar ve başkalarının gerçek duygularının gerçekliğini bulamayacaklar ve görünüşte nesnel yargılarının altında yatan nedenleri ve öznellikleri ıskaladığını keşfetmekle ilgilenmeyeceklerdir. hem kendilerinin hem de başkalarının hayatları veya dünyevi durumları.",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'11',
            'part'=>'11',
            'title'=>"",
            'desc'=>"Почти все описанные выше проблемные характеристики в той или иной степени можно отнести к общей проблеме ENFJ, состоящей в стремлении найти «правильную» ценность во всем. Если ENFJ не научится видеть за внешним видом то, что они быстро считают хорошим или плохим в людях и ситуациях во внешней среде, они будут использовать свою интровертную интуицию только для поддержки тех суждений, которые они считают хорошими для них, и игнорируют не только другие возможности, но и их собственное качество внутренней жизни. Рассмотрение этих менее очевидных возможностей и их собственных потребностей требует, чтобы ENFJ осознал, что их собственные оценочные суждения действительно субъективны и что применять их повсеместно ко всем цивилизованным людям нецелесообразно и неэффективно. Практика отстранения и объективного взгляда на свою собственную систему ценностей - это не то, что ENFJ привыкло делать; попытка избежать абстрактной рационализации проблем и порождаемых ими чувств - это естественный метод выживания для личности ENFJ. Основной движущей силой личности ENFJ является экстравертное чувство, цель которого, прежде всего, состоит в том, чтобы находить и различать ценности в людях и человеческих ситуациях. Если их способность находить конкретную и достойную ценность в человеке или ситуации оказывается под угрозой, ENFJ закрывает доступ к угрожающей силе. Это совершенно естественно, но, к сожалению, человек, который регулярно осуществляет этот тип защиты повестки дня, будет становиться все более жестким в своих суждениях и ожиданиях по отношению к людям, но еще меньше беспокоиться о влиянии таких условий на самих себя. Там, где неуравновешенный ENFJ соглашается с образами интуиции, они обычно искажаются, чтобы поддержать субъективную повестку дня доминирующего Чувства. Таким образом, они всегда находят оправдание своим определениям и самопожертвованию перед людьми, вещами и ситуациями, и они не смогут определить реальность истинных чувств другого человека и не будут заинтересованы в обнаружении того, что их, казалось бы, объективные суждения упускают из виду причины и субъективности, лежащие в основе как своей собственной, так и чужой жизни или мирской ситуации.",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'11',
            'part'=>'11',
            'title'=>"",
            'desc'=>"Fast alle der oben beschriebenen problematischen Eigenschaften lassen sich in unterschiedlichem Maße auf das häufige ENFJ-Problem zurückführen, in allem den „richtigen“ Wert finden zu wollen. Wenn die ENFJ nicht lernt, hinter dem Schein zu sehen, was sie schnell als gut oder schlecht über die Menschen und Situationen in ihrer äußeren Umgebung einschätzen, werden sie ihre introvertierte Intuition nur nutzen, um die Urteile zu unterstützen, die sie für gut halten und die sie missachten nicht nur andere Möglichkeiten, sondern auch ihre eigene Qualität des Innenlebens. Die Berücksichtigung dieser weniger offensichtlichen Möglichkeiten und ihrer eigenen Bedürfnisse erfordert, dass die ENFJ anerkennt, dass ihre eigenen Werturteile tatsächlich subjektiv sind und dass es nicht angemessen oder effektiv ist, sie pauschal auf alle zivilisierten Menschen anzuwenden. Die Praxis, sich zurückzuhalten und das eigene Wertesystem objektiv zu betrachten, ist für das ENFJ nicht üblich; Der Versuch, eine abstrakte Rationalisierung von Problemen und den von ihnen hervorgerufenen Gefühlen zu vermeiden, ist eine natürliche Überlebenstechnik für die ENFJ-Persönlichkeit. Der Hauptantrieb der ENFJ-Persönlichkeit ist das Extravertierte Gefühl, dessen Zweck es vor allem ist, die Werte in Menschen und menschlichen Situationen zu finden und zu unterscheiden. Wenn ihre Fähigkeit, einen bestimmten und würdigen Wert in einer Person oder Situation zu finden, gefährdet ist, schließt das ENFJ die bedrohliche Kraft aus. Dies ist ganz natürlich, aber leider wird der Einzelne, der diese Art von Agenda-Schutz regelmäßig ausübt, in seinen Urteilen und Erwartungen an die Menschen immer starrer, aber noch weniger besorgt über die Auswirkungen solcher Bedingungen auf sich selbst. Wo der unausgeglichene ENFJ den Bildern der Intuition nachgibt, werden diese im Allgemeinen verzerrt, um die subjektive Agenda des dominanten Gefühls zu unterstützen. Auf diese Weise finden sie immer eine Rechtfertigung für ihre Entscheidungen und ihre Selbstaufopferung gegenüber Menschen, Dingen und Situationen, und sie werden weder die Realität der wahren Gefühle eines anderen lokalisieren können noch daran interessiert sein, zu entdecken, dass ihre scheinbar objektiven Urteile die zugrunde liegenden Gründe und Subjektivitäten verfehlen sowohl ihr eigenes als auch das anderer Leben oder ihre weltliche Situation.",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'11',
            'part'=>'11',
            'title'=>"",
            'desc'=>"Casi todas las características problemáticas descritas anteriormente se pueden atribuir en varios grados al problema común de ENFJ de querer encontrar el valor 'adecuado' en todo. Si el ENFJ no aprende a ver más allá de la apariencia de lo que rápidamente juzga como bueno o malo sobre las personas y situaciones en su entorno externo, solo usará su intuición introvertida para respaldar esos juicios que sientan que son buenos para ellos y los ignorarán. no sólo otras posibilidades, sino también su propia calidad de vida interior. La consideración de estas posibilidades menos obvias y sus propias necesidades requiere que la ENFJ reconozca que sus propios juicios de valor son de hecho subjetivos, y que no es apropiado ni efectivo aplicarlos en todos los ámbitos a todas las personas civilizadas. La práctica de retroceder y mirar objetivamente su propio sistema de valores no es algo que la ENFJ esté acostumbrada a hacer; tratar de evitar la racionalización abstracta de los problemas y los sentimientos que engendran es una técnica de supervivencia natural para la personalidad ENFJ. El principal impulsor de la personalidad ENFJ es el Sentimiento Extravertido, cuyo propósito es sobre todo encontrar y discriminar los valores en las personas y situaciones humanas. Si su capacidad para encontrar un valor específico y digno en una persona o situación se ve amenazada, la ENFJ cierra la fuerza amenazante. Esto es totalmente natural, pero desafortunadamente el individuo que ejerce este tipo de protección de agenda con regularidad se volverá cada vez más rígido en sus juicios y expectativas de las personas, pero aún menos preocupado por el efecto que tales condiciones tienen sobre sí mismos. Cuando el ENFJ desequilibrado sí acepta las imágenes de la intuición, estas generalmente estarán sesgadas para apoyar la agenda subjetiva del Sentimiento dominante. De esta manera siempre encontrarán justificación para sus determinaciones y sus sacrificios a las personas, cosas y situaciones, y serán incapaces de localizar la realidad de los verdaderos sentimientos de los demás, ni estarán interesados ​​en descubrir que sus juicios aparentemente objetivos pasan por alto las razones y subjetividades subyacentes. tanto su propia vida como la de los demás o su situación mundana.",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'11',
            'part'=>'11',
            'title'=>"",
            'desc'=>"几乎所有上述问题特征都可以在不同程度上归因于想要在所有事物中找到“适当”价值的常见 ENFJ 问题。如果 ENFJ 不学习如何从表面上看他们对外部环境中的人和情况的快速判断是好是坏，他们只会用他们内向的直觉来支持那些他们认为对他们有益的判断，而无视不仅是其他可能性，还有他们自己的内在生活质量。考虑到这些不太明显的可能性和他们自己的需求，ENFJ 需要认识到他们自己的价值判断确实是主观的，将它们一刀切地应用于所有文明人是不合适或无效的。后退并客观地审视自己的价值体系的做法并不是 ENFJ 习惯做的事情；试图避免对问题及其产生的感觉进行抽象的合理化，是 ENFJ 人格的一种自然生存技巧。 ENFJ 人格的主要驱动力是外倾情感，其目的首先是发现和区分人和人类情境中的价值观。如果他们在某个人或情况中找到特定且有价值的价值的能力受到威胁，ENFJ 就会将威胁力量拒之门外。这是完全自然的，但不幸的是，经常行使这种议程保护的个人对人们的判断和期望会变得越来越僵化，而更不关心这种情况对自己的影响。在不平衡的 ENFJ 确实默许直觉图像的地方，这些图像通常会偏向于支持主导感觉的主观议程。就这样，他们总能为自己的决心和对人、事、境的自我牺牲找到理由，无法定位他人真实感受的真实性，也没有兴趣发现自己看似客观的判断漏掉了背后的原因和主观性。自己和他人的生活或世俗情况。",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'11',
            'part'=>'11',
            'title'=>"",
            'desc'=>"Petulance, pensiveness and a sense of being let down by others can often be the end result of this one sided approach to the world, whilst if the ENFJ is in a strong company or relationship position they might become driven to manipulate others and situations to conform to their own feeling needs and value judgments, irrespective of any true value to the situation or for the other persons involved. In this case, the “big picture” valued for its great worth to all, becomes a dominant drive which seeks to blot out or crush any opposition by claiming the moral high ground, even to the point where the ENFJ sacrifices their own life to the “cause”. The inability to recognize the plethora of subjective possibilities their feelings bring into their lives strip the unbalanced ENFJ of their access to both a deeper connection with others and the possibility of refining and developing pathways to the kind of self understanding and self nurturing their finer judgments might otherwise lead them to.",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'11',
            'part'=>'11',
            'title'=>"",
            'desc'=>"غالبًا ما يكون التساهل والحنق والشعور بالخذل من قبل الآخرين النتيجة النهائية لهذا النهج من جانب واحد تجاه العالم ، بينما إذا كانت ENFJ في شركة قوية أو في وضع علاقة ، فقد يصبحون مدفوعين للتلاعب بالآخرين والمواقف لتتوافق لاحتياجاتهم الشعورية والأحكام القيمية ، بغض النظر عن أي قيمة حقيقية للموقف أو للأشخاص الآخرين المعنيين. في هذه الحالة ، تصبح 'الصورة الكبيرة' التي تُقدر قيمتها الكبيرة للجميع ، دافعًا مهيمنًا يسعى إلى محو أو سحق أي معارضة من خلال المطالبة بمكانة أخلاقية عالية ، حتى إلى النقطة التي تضحي فيها ENFJ بحياتهم من أجل 'لانى'. عدم القدرة على التعرف على عدد كبير من الاحتمالات الذاتية التي تجلبها مشاعرهم في حياتهم تجرد ENFJ غير المتوازن من وصولهم إلى اتصال أعمق مع الآخرين وإمكانية صقل وتطوير مسارات لنوع من فهم الذات وتغذية الذات بأحكامهم الدقيقة. خلاف ذلك يقودهم إلى.",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'11',
            'part'=>'11',
            'title'=>"",
            'desc'=>"L'irritabilité, la réflexion et le sentiment d'être laissé tomber par les autres peuvent souvent être le résultat final de cette approche unilatérale du monde, tandis que si l'ENFJ est dans une position forte dans l'entreprise ou dans les relations, il peut être amené à manipuler les autres et les situations pour se conformer. à leurs propres besoins et jugements de valeur, indépendamment de toute valeur réelle pour la situation ou pour les autres personnes impliquées. Dans ce cas, la « grande image » appréciée pour sa grande valeur à tous, devient une pulsion dominante qui cherche à effacer ou à écraser toute opposition en revendiquant la hauteur morale, au point même que l'ENFJ sacrifie sa propre vie à la 'causer'. L'incapacité à reconnaître la pléthore de possibilités subjectives que leurs sentiments apportent dans leur vie prive les ENFJ déséquilibrés de leur accès à la fois à une connexion plus profonde avec les autres et à la possibilité d'affiner et de développer des voies vers le type de compréhension de soi et d'auto-consolidation de leurs jugements les plus fins. sinon les conduire à.",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'11',
            'part'=>'11',
            'title'=>"",
            'desc'=>"Hırs, dalgınlık ve başkaları tarafından hayal kırıklığına uğrama hissi genellikle dünyaya bu tek taraflı yaklaşımın sonucu olabilirken, ENFJ güçlü bir şirket veya ilişki konumundaysa başkalarını ve durumları uyum sağlamak için manipüle etmeye yönlendirilebilir. duruma veya ilgili diğer kişiler için herhangi bir gerçek değere bakılmaksızın, kendi duygu ihtiyaçlarına ve değer yargılarına. Bu durumda, herkes için büyük değeriyle değer verilen “büyük resim”, ENFJ'nin kendi hayatını feda ettiği noktaya kadar ahlaki açıdan yüksek bir zemin talep ederek herhangi bir muhalefeti ortadan kaldırmaya veya ezmeye çalışan baskın bir dürtü haline gelir. 'neden'. Duygularının hayatlarına getirdiği öznel olasılıkların bolluğunu fark edememek, dengesiz ENFJ'yi hem başkalarıyla daha derin bir bağlantıya erişimlerinden hem de daha ince yargılarını beslemek ve kendi kendini anlama türüne giden yolları iyileştirme ve geliştirme olasılığından mahrum bırakabilir. aksi takdirde onları yönlendirin.",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'11',
            'part'=>'11',
            'title'=>"",
            'desc'=>"Обидчивость, задумчивость и чувство разочарования со стороны других часто могут быть конечным результатом этого одностороннего подхода к миру, в то время как, если ENFJ находится в сильной компании или позиции отношений, они могут начать манипулировать другими и ситуациями, чтобы соответствовать. на свои собственные потребности в чувствах и оценочные суждения, независимо от их истинной ценности для ситуации или для других вовлеченных лиц. В этом случае «большая картина», ценимая за свою огромную ценность для всех, становится доминирующей движущей силой, стремящейся стереть или подавить любую оппозицию, требуя морального превосходства, даже до такой степени, что ENFJ жертвует собственной жизнью ради блага 'причина'. Неспособность распознать изобилие субъективных возможностей, которые их чувства привносят в их жизнь, лишает несбалансированных ENFJ их доступа как к более глубокой связи с другими, так и к возможности уточнения и развития путей к самопониманию и самовоспитанию их более тонких суждений. в противном случае привести их к.",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'11',
            'part'=>'11',
            'title'=>"",
            'desc'=>"Gereiztheit, Nachdenklichkeit und das Gefühl, von anderen enttäuscht zu werden, können oft das Endergebnis dieser einseitigen Herangehensweise an die Welt sein ihren eigenen gefühlten Bedürfnissen und Werturteilen, unabhängig von einem wahren Wert für die Situation oder für die anderen beteiligten Personen. In diesem Fall wird das „große Ganze“, das wegen seines hohen Wertes für alle geschätzt wird, zu einem dominanten Antrieb, der versucht, jede Opposition auszulöschen oder zu zerschlagen, indem er die moralische Vorherrschaft beansprucht, sogar bis zu dem Punkt, an dem die ENFJ ihr eigenes Leben opfert 'Ursache'. Die Unfähigkeit, die Fülle an subjektiven Möglichkeiten zu erkennen, die ihre Gefühle in ihr Leben bringen, beraubt das unausgeglichene ENFJ ihres Zugangs zu einer tieferen Verbindung mit anderen und der Möglichkeit, Wege zu einer Art von Selbstverständnis und Selbsterziehung zu verfeinern und zu entwickeln, die ihre feineren Urteile möglicherweise nähren sonst führen sie zu.",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'11',
            'part'=>'11',
            'title'=>"",
            'desc'=>"La petulancia, la pensatividad y la sensación de ser decepcionado por los demás a menudo pueden ser el resultado final de este enfoque unilateral del mundo, mientras que si el ENFJ se encuentra en una posición sólida de empresa o relación, podría verse impulsado a manipular a los demás y las situaciones para adaptarse. a sus propias necesidades sentimentales y juicios de valor, independientemente de cualquier valor real para la situación o para las otras personas involucradas. En este caso, el 'panorama general', valorado por su gran valor para todos, se convierte en un impulso dominante que busca borrar o aplastar cualquier oposición reclamando el terreno moral, incluso hasta el punto en que la ENFJ sacrifica su propia vida a la 'porque'. La incapacidad para reconocer la plétora de posibilidades subjetivas que sus sentimientos traen a sus vidas despojan al ENFJ desequilibrado de su acceso tanto a una conexión más profunda con los demás como a la posibilidad de refinar y desarrollar caminos hacia el tipo de autocomprensión y autoalimentación que sus juicios más finos podrían hacer. de lo contrario, llévelos a.",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'11',
            'part'=>'11',
            'title'=>"",
            'desc'=>"脾气暴躁、沉思和被他人辜负的感觉往往是这种片面看待世界的最终结果，而如果 ENFJ 处于强大的公司或关系位置，他们可能会被迫操纵他人和情况以顺从对他们自己的感觉需要和价值判断，而不管对情况或其他相关人员的任何真正价值。在这种情况下，因其对所有人的巨大价值而受到重视的“大局”成为一种主导驱动力，它试图通过占据道德制高点来抹杀或粉碎任何反对派，甚至到了 ENFJ 为“原因”。无法认识到他们的感受给生活带来的大量主观可能性，这剥夺了不平衡的 ENFJ 与他人建立更深层次联系的机会，以及完善和发展通往自我理解和自我培养的途径的可能性。否则导致他们。",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'12',
            'title'=>"Solutions",
            'desc'=>"",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'12',
            'title'=>"حلول",
            'desc'=>"",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'12',
            'title'=>"Solutions",
            'desc'=>"",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'12',
            'title'=>"Çözümler",
            'desc'=>"",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'12',
            'title'=>"Решения",
            'desc'=>"",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'12',
            'title'=>"Lösungen",
            'desc'=>"",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'12',
            'title'=>"Soluciones",
            'desc'=>"",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'12',
            'title'=>"解决方案",
            'desc'=>"",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'12',
            'part'=>'12',
            'title'=>"",
            'desc'=>"To grow as an individual, the ENFJ needs to focus on paying attention to their inner images. This means they need to be open to the possibilities that lie beneath their judgments and values, rather than just accepting the appearance of values which accord with their sense of rightness. The ENFJ needs to understand that developing their ability to see the subjective possibilities within themselves and others does not threaten their ability to make correct judgments, but rather enhances it, and enhances their personal chances for achieving a measure of success in their lives.
The ENFJ concerned with personal growth will pay close attention to their motivation for accepting values that come to them. Are they trying to see the background of circumstance behind their own and others value judgments, or are they trying to maintain their own image of how things “ought” to be? The goal is to find a balance between what seems correct and valuable and the many possible ways in which such a judgment might be subjective and not necessarily the best for themselves or a situation. Obviously, this is not entirely possible, but it is the exercise to keep in mind. They need to see the many divergent images of values and their conflicts which affect them, without feeling threatened, and without losing their sense of what is right and wrong.",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'12',
            'part'=>'12',
            'title'=>"",
            'desc'=>"لكي تنمو كفرد ، يحتاج ENFJ إلى التركيز على الانتباه إلى صورهم الداخلية. هذا يعني أنهم بحاجة إلى الانفتاح على الاحتمالات الكامنة وراء أحكامهم وقيمهم ، بدلاً من مجرد قبول ظهور القيم التي تتوافق مع إحساسهم بالصواب. تحتاج ENFJ إلى فهم أن تطوير قدرتهم على رؤية الاحتمالات الذاتية داخل أنفسهم والآخرين لا يهدد قدرتهم على إصدار أحكام صحيحة ، بل يعززها ، ويعزز فرصهم الشخصية لتحقيق قدر من النجاح في حياتهم.
ستولي ENFJ المهتمة بالنمو الشخصي اهتمامًا وثيقًا بدوافعهم لقبول القيم التي تأتي إليهم. هل يحاولون رؤية خلفية الظروف وراء الأحكام الخاصة بهم والآخرين ، أم أنهم يحاولون الحفاظ على صورتهم الخاصة لما 'يجب' أن تكون عليه الأشياء؟ الهدف هو إيجاد توازن بين ما يبدو صحيحًا وذا قيمة والعديد من الطرق الممكنة التي قد يكون فيها مثل هذا الحكم غير موضوعي وليس بالضرورة الأفضل لأنفسهم أو لموقف ما. من الواضح أن هذا ليس ممكنًا تمامًا ، لكنه تمرين يجب أخذه في الاعتبار. إنهم بحاجة لرؤية الصور العديدة المتباينة للقيم وصراعاتها التي تؤثر عليهم ، دون الشعور بالتهديد ، ودون أن يفقدوا إحساسهم بما هو صواب وما هو خطأ.",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'12',
            'part'=>'12',
            'title'=>"",
            'desc'=>"Pour grandir en tant qu'individu, l'ENFJ doit se concentrer sur l'attention qu'il porte à son image intérieure. Cela signifie qu'ils doivent être ouverts aux possibilités qui se cachent sous leurs jugements et leurs valeurs, plutôt que d'accepter simplement l'apparence de valeurs qui correspondent à leur sens de la justesse. L'ENFJ doit comprendre que développer sa capacité à voir les possibilités subjectives en lui-même et chez les autres ne menace pas sa capacité à porter des jugements corrects, mais l'améliore plutôt et augmente ses chances personnelles d'atteindre une certaine réussite dans sa vie.
L'ENFJ soucieux d'épanouissement personnel portera une attention particulière à sa motivation à accepter les valeurs qui lui viennent. Essayent-ils de voir l'arrière-plan des circonstances derrière leurs propres jugements de valeur et ceux des autres, ou essaient-ils de maintenir leur propre image de la façon dont les choses « devraient » être ? Le but est de trouver un équilibre entre ce qui semble correct et valable et les nombreuses manières possibles dont un tel jugement pourrait être subjectif et pas nécessairement le meilleur pour eux-mêmes ou une situation. Évidemment, ce n'est pas tout à fait possible, mais c'est l'exercice à garder à l'esprit. Ils ont besoin de voir les nombreuses images divergentes des valeurs et de leurs conflits qui les affectent, sans se sentir menacés, et sans perdre leur sens de ce qui est bien et mal.",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'12',
            'part'=>'12',
            'title'=>"",
            'desc'=>"Bir birey olarak büyümek için ENFJ'nin iç görüntülerine dikkat etmeye odaklanması gerekir. Bu, yalnızca doğruluk duygularıyla uyumlu değerlerin görünüşünü kabul etmektense, yargılarının ve değerlerinin altında yatan olasılıklara açık olmaları gerektiği anlamına gelir. ENFJ'nin, kendi içlerindeki ve diğerlerindeki öznel olasılıkları görme yeteneklerini geliştirmenin, onların doğru yargılarda bulunma yeteneklerini tehdit etmediğini, aksine onu geliştirdiğini ve yaşamlarında bir başarı ölçüsü elde etmek için kişisel şanslarını geliştirdiğini anlaması gerekir.
Kişisel gelişimle ilgilenen ENFJ, kendilerine gelen değerleri kabul etme motivasyonlarına çok dikkat edecektir. Kendilerinin ve başkalarının değer yargılarının ardındaki koşulların arka planını mı görmeye çalışıyorlar, yoksa işlerin nasıl “olması gerektiğine” dair kendi imajlarını korumaya mı çalışıyorlar? Amaç, doğru ve değerli görünen şeyler ile böyle bir yargının öznel olabileceği ve kendileri veya bir durum için mutlaka en iyisi olmayabileceği birçok olası yol arasında bir denge bulmaktır. Açıkçası, bu tamamen mümkün değil, ancak akılda tutulması gereken bir egzersiz. Kendini tehdit altında hissetmeden ve neyin doğru neyin yanlış olduğuna dair hislerini kaybetmeden, kendilerini etkileyen birçok farklı değerler ve onların çatışmalarını görmeleri gerekir.",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'12',
            'part'=>'12',
            'title'=>"",
            'desc'=>"Чтобы расти как личность, ENFJ необходимо сосредоточить внимание на своих внутренних образах. Это означает, что они должны быть открыты для возможностей, лежащих в основе их суждений и ценностей, а не просто принимать видимость ценностей, которые соответствуют их чувству правоты. ENFJ должны понимать, что развитие их способности видеть субъективные возможности внутри себя и других не угрожает их способности делать правильные суждения, а, скорее, усиливает их и увеличивает их личные шансы на достижение определенного успеха в жизни.
ENFJ, заинтересованные в личностном росте, будут уделять пристальное внимание своей мотивации принятия ценностей, которые к ним приходят. Пытаются ли они увидеть подоплеку обстоятельств, стоящих за их собственными оценочными суждениями и суждениями других, или они пытаются сохранить свое собственное представление о том, как вещи «должны» быть? Цель состоит в том, чтобы найти баланс между тем, что кажется правильным и ценным, и множеством возможных причин, по которым такое суждение может быть субъективным и не обязательно лучшим для них самих или ситуации. Очевидно, это не совсем возможно, но об этом следует помнить. Им необходимо видеть множество различных образов ценностей и их конфликтов, которые влияют на них, не чувствуя угрозы и не теряя понимания того, что правильно и что неправильно.",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'12',
            'part'=>'12',
            'title'=>"",
            'desc'=>"Um als Individuum zu wachsen, muss sich die ENFJ darauf konzentrieren, auf ihre inneren Bilder zu achten. Das heißt, sie müssen offen sein für die Möglichkeiten, die ihren Urteilen und Werten zugrunde liegen, anstatt nur den Anschein von Werten zu akzeptieren, die ihrem Sinn für Richtigkeit entsprechen. Die ENFJ muss verstehen, dass die Entwicklung ihrer Fähigkeit, die subjektiven Möglichkeiten in sich selbst und in anderen zu sehen, ihre Fähigkeit, richtige Urteile zu fällen, nicht gefährdet, sondern vielmehr verbessert und ihre persönlichen Chancen erhöht, ein gewisses Maß an Erfolg in ihrem Leben zu erzielen.
Die ENFJ, die sich mit persönlichem Wachstum beschäftigt, wird ihre Motivation für die Akzeptanz von Werten, die zu ihnen kommen, aufmerksam verfolgen. Versuchen sie, den Hintergrund der Umstände hinter ihren eigenen und anderen Werturteilen zu sehen, oder versuchen sie, ihr eigenes Bild davon aufrechtzuerhalten, wie die Dinge sein „sollten“? Ziel ist es, ein Gleichgewicht zwischen dem, was richtig und wertvoll erscheint, und den vielen möglichen Wegen zu finden, in denen ein solches Urteil subjektiv und nicht unbedingt das Beste für sich selbst oder eine Situation ist. Natürlich ist dies nicht ganz möglich, aber es ist die Übung, die Sie im Auge behalten sollten. Sie müssen die vielen divergierenden Wertebilder und ihre Konflikte, die sie betreffen, sehen, ohne sich bedroht zu fühlen und ohne den Sinn für richtig und falsch zu verlieren.",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'12',
            'part'=>'12',
            'title'=>"",
            'desc'=>"Para crecer como individuo, la ENFJ necesita enfocarse en prestar atención a sus imágenes internas. Esto significa que deben estar abiertos a las posibilidades que subyacen a sus juicios y valores, en lugar de simplemente aceptar la apariencia de valores que están de acuerdo con su sentido de lo correcto. El ENFJ debe comprender que el desarrollo de su capacidad para ver las posibilidades subjetivas dentro de sí mismos y de los demás no amenaza su capacidad para emitir juicios correctos, sino que la mejora y aumenta sus posibilidades personales de lograr una medida de éxito en sus vidas.
Los ENFJ preocupados por el crecimiento personal prestarán mucha atención a su motivación para aceptar los valores que les llegan. ¿Están tratando de ver el trasfondo de las circunstancias detrás de sus propios juicios de valor y de los demás, o están tratando de mantener su propia imagen de cómo 'deberían' ser las cosas? El objetivo es encontrar un equilibrio entre lo que parece correcto y valioso y las muchas formas posibles en las que tal juicio puede ser subjetivo y no necesariamente lo mejor para ellos mismos o para una situación. Evidentemente, esto no es del todo posible, pero es el ejercicio a tener en cuenta. Necesitan ver las muchas imágenes divergentes de valores y los conflictos que les afectan, sin sentirse amenazados y sin perder el sentido de lo que está bien y lo que está mal.",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'12',
            'part'=>'12',
            'title'=>"",
            'desc'=>"要成长为个人，ENFJ 需要专注于关注他们的内在形象。这意味着他们需要对隐藏在他们的判断和价值观之下的可能性持开放态度，而不是仅仅接受符合他们正义感的价值观的表象。 ENFJ 需要明白，培养他们看到自己和他人主观可能性的能力并不会威胁到他们做出正确判断的能力，而是会增强这种能力，并增加他们在生活中取得一定程度成功的个人机会。关注个人成长的 ENFJ 会密切关注他们接受价值观的动机。他们是试图看到自己和他人价值判断背后的环境背景，还是试图保持自己对事物“应该”如何的形象？我们的目标是在看似正确和有价值的事物与许多可能的主观判断方式之间找到平衡，这些方式不一定对他们自己或情况最好。显然，这不是完全可能的，但这是要记住的练习。他们需要看到影响他们的许多不同的价值观及其冲突的形象，而不会感到受到威胁，也不会失去对是非的意识。",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'13',
            'title'=>"Living Happily in our World as an ENFJ",
            'desc'=>"",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'13',
            'title'=>"العيش بسعادة في عالمنا باعتباره ENFJ",
            'desc'=>"",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'13',
            'title'=>"Vivre heureux dans notre monde en tant qu'ENFJ",
            'desc'=>"",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'13',
            'title'=>"ENFJ Olarak Dünyamızda Mutlu Yaşamak",
            'desc'=>"",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'13',
            'title'=>"Жить счастливо в нашем мире как ENFJ",
            'desc'=>"",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'13',
            'title'=>"Als ENFJ glücklich in unserer Welt leben",
            'desc'=>"",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'13',
            'title'=>"Viviendo felizmente en nuestro mundo como ENFJ",
            'desc'=>"",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'13',
            'title'=>"作为 ENFJ 幸福地生活在我们的世界",
            'desc'=>"",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'13',
            'part'=>'13',
            'title'=>"",
            'desc'=>"As can be seen from the above, some strongly expressed ENFJs can have difficulty fitting into society. Their problems are usually due to their Extraverted Feeling function being so dominant that they are so strongly bound to what they see as objective values that they cannot relate to the world except via the objects of their feeling. In such cases the intensity of their judgments can actually drive others away from them, and the resulting lack of close relationship felt as a personal failing for which the ENFJ feels guilty. Such guilt can drive even more strongly affective behavior which leads the ENFJ to ignore their own needs entirely, or it can become a negative drive to manipulate others to conform to their one-sided vision of the world. The ENFJ who consistently tries to see the underlying possibilities and the scope available in each situation will be able to see the right path to take with each person and situation in their life. This will always lead them to toward closer relationships, happiness and great achievements.
The key to personal growth for the ENFJ is competent execution of Introverted Intuition. Because it is often hard to define what this represents subjectively to each person, here are some action-oriented suggestions that will help lead you down the path towards more effective use of the Introverted Intuitive function.
",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'13',
            'part'=>'13',
            'title'=>"",
            'desc'=>"كما يتضح مما سبق ، قد يواجه بعض ENFJs المعبر عنها بقوة صعوبة في الاندماج في المجتمع. ترجع مشكلاتهم عادةً إلى كون وظيفة الشعور الانبساطي الخاصة بهم مهيمنة للغاية لدرجة أنهم مرتبطون بشدة بما يرونه قيمًا موضوعية بحيث لا يمكنهم الارتباط بالعالم إلا من خلال الأشياء التي يشعرون بها. في مثل هذه الحالات ، يمكن أن تؤدي شدة أحكامهم في الواقع إلى إبعاد الآخرين عنهم ، ويشعر أن الافتقار الناتج للعلاقة الوثيقة هو فشل شخصي تشعر ENFJ بالذنب بسببه. يمكن أن يؤدي هذا الشعور بالذنب إلى سلوك عاطفي أكثر قوة مما يؤدي إلى تجاهل ENFJ لاحتياجاتهم الخاصة تمامًا ، أو يمكن أن يصبح دافعًا سلبيًا للتلاعب بالآخرين للتوافق مع رؤيتهم أحادية الجانب للعالم. إن ENFJ الذي يحاول باستمرار رؤية الاحتمالات الأساسية والنطاق المتاح في كل موقف سيكون قادرًا على رؤية المسار الصحيح الذي يجب اتخاذه مع كل شخص وموقف في حياته. سيقودهم هذا دائمًا نحو علاقات أوثق وسعادة وإنجازات عظيمة.
مفتاح النمو الشخصي لـ ENFJ هو التنفيذ الكفء للحدس الانطوائي. نظرًا لأنه غالبًا ما يكون من الصعب تحديد ما يمثله هذا بشكل شخصي لكل شخص ، فإليك بعض الاقتراحات الموجهة نحو العمل والتي ستساعدك على السير في الطريق نحو استخدام أكثر فاعلية للوظيفة البديهية الانطوائية.",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'13',
            'part'=>'13',
            'title'=>"",
            'desc'=>"Comme le montre ce qui précède, certains ENFJ fortement exprimés peuvent avoir des difficultés à s'intégrer dans la société. Leurs problèmes sont généralement dus au fait que leur fonction de Sentiment Extraverti est si dominante qu'ils sont si fortement liés à ce qu'ils considèrent comme des valeurs objectives qu'ils ne peuvent se rapporter au monde que via les objets de leurs sentiments. Dans de tels cas, l'intensité de leurs jugements peut en fait éloigner les autres d'eux, et le manque de relation étroite qui en résulte est ressenti comme un échec personnel dont l'ENFJ se sent coupable. Une telle culpabilité peut conduire à un comportement encore plus affectif qui conduit les ENFJ à ignorer complètement leurs propres besoins, ou elle peut devenir une pulsion négative à manipuler les autres pour se conformer à leur vision unilatérale du monde. L'ENFJ qui essaie constamment de voir les possibilités sous-jacentes et la portée disponible dans chaque situation sera en mesure de voir le bon chemin à prendre avec chaque personne et situation dans sa vie. Cela les conduira toujours à des relations plus étroites, au bonheur et à de grandes réalisations.
La clé de la croissance personnelle pour l'ENFJ est l'exécution compétente de l'intuition introvertie. Parce qu'il est souvent difficile de définir ce que cela représente subjectivement pour chaque personne, voici quelques suggestions orientées vers l'action qui vous aideront à vous diriger vers une utilisation plus efficace de la fonction Introverti Intuitif.",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'13',
            'part'=>'13',
            'title'=>"",
            'desc'=>"Yukarıdan da anlaşılacağı gibi, güçlü bir şekilde ifade edilen bazı ENFJ'ler topluma uyum sağlamakta zorluk çekebilir. Sorunları genellikle Dışa Dönük Duygu işlevlerinin o kadar baskın olmasından kaynaklanır ki, nesnel değerler olarak gördüklerine o kadar güçlü bir şekilde bağlıdırlar ki, duygularının nesneleri dışında dünyayla ilişki kuramazlar. Bu gibi durumlarda, yargılarının yoğunluğu aslında başkalarını onlardan uzaklaştırabilir ve sonuçta ortaya çıkan yakın ilişki eksikliği, ENFJ'nin kendini suçlu hissettiği kişisel bir başarısızlık olarak hissedilir. Bu tür bir suçluluk, ENFJ'nin kendi ihtiyaçlarını tamamen görmezden gelmesine neden olan daha güçlü duygusal davranışları yönlendirebilir veya başkalarını tek taraflı dünya vizyonlarına uymaları için manipüle etmek için olumsuz bir dürtü haline gelebilir. Sürekli olarak altta yatan olasılıkları ve her durumda mevcut olan kapsamı görmeye çalışan ENFJ, yaşamlarındaki her kişi ve durum için izlenecek doğru yolu görebilecektir. Bu onları her zaman daha yakın ilişkilere, mutluluğa ve büyük başarılara yönlendirecektir.
ENFJ için kişisel gelişimin anahtarı, İçedönük Sezginin yetkin bir şekilde yürütülmesidir. Bunun her kişi için öznel olarak neyi temsil ettiğini tanımlamak genellikle zor olduğundan, İçe Dönük Sezgisel işlevinin daha etkin kullanımına giden yolda size yardımcı olacak eyleme yönelik bazı öneriler aşağıda verilmiştir.",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'13',
            'part'=>'13',
            'title'=>"",
            'desc'=>"Как видно из вышеизложенного, некоторые сильно выраженные ENFJ могут испытывать трудности с приспособлением к обществу. Их проблемы обычно возникают из-за того, что их функция экстравертного чувства настолько доминирует, что они настолько сильно привязаны к тому, что они считают объективными ценностями, что они не могут относиться к миру, кроме как через объекты своих чувств. В таких случаях интенсивность их суждений может фактически оттолкнуть других от них, и возникающее в результате отсутствие близких отношений воспринимается как личная ошибка, за которую ENFJ чувствует себя виноватой. Такая вина может стимулировать еще более сильное аффективное поведение, которое приводит к тому, что ENFJ полностью игнорируют свои собственные потребности, или может стать негативным побуждением манипулировать другими, чтобы соответствовать их одностороннему видению мира. ENFJ, который постоянно пытается увидеть основные возможности и объем, доступный в каждой ситуации, сможет увидеть правильный путь для каждого человека и ситуации в их жизни. Это всегда приведет их к более близким отношениям, счастью и великим свершениям.
Ключом к личностному росту для ENFJ является грамотное исполнение интровертной интуиции. Поскольку часто бывает трудно определить, что это представляет собой субъективно для каждого человека, вот несколько ориентированных на действия предложений, которые помогут вести вас по пути к более эффективному использованию функции интровертированной интуиции.",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'13',
            'part'=>'13',
            'title'=>"",
            'desc'=>"Wie aus dem Obigen ersichtlich ist, können einige stark ausgeprägte ENFJs Schwierigkeiten haben, sich in die Gesellschaft einzufügen. Ihre Probleme sind normalerweise darauf zurückzuführen, dass ihre Funktion des extravertierten Gefühls so dominant ist, dass sie so stark an das gebunden sind, was sie als objektive Werte ansehen, dass sie sich nur über die Objekte ihrer Gefühle mit der Welt in Verbindung setzen können. In solchen Fällen kann die Intensität ihrer Urteile andere sogar von ihnen vertreiben, und der daraus resultierende Mangel an enger Beziehung wird als persönliches Versagen empfunden, für das sich das ENFJ schuldig fühlt. Solche Schuldgefühle können zu einem noch stärkeren affektiven Verhalten führen, das dazu führt, dass die ENFJ ihre eigenen Bedürfnisse vollständig ignoriert, oder sie kann zu einem negativen Antrieb werden, andere zu manipulieren, um ihrer einseitigen Sicht der Welt zu entsprechen. Der ENFJ, der konsequent versucht, die zugrunde liegenden Möglichkeiten und die in jeder Situation verfügbaren Spielräume zu sehen, wird in der Lage sein, den richtigen Weg für jede Person und Situation in ihrem Leben zu sehen. Dies wird sie immer zu engeren Beziehungen, Glück und großen Erfolgen führen.
Der Schlüssel zum persönlichen Wachstum des ENFJ ist die kompetente Ausführung der introvertierten Intuition. Da es oft schwer zu definieren ist, was dies für jede Person subjektiv bedeutet, finden Sie hier einige handlungsorientierte Vorschläge, die Sie auf den Weg zu einer effektiveren Nutzung der introvertierten Intuitiven Funktion führen.",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'13',
            'part'=>'13',
            'title'=>"",
            'desc'=>"Como puede verse en lo anterior, algunos ENFJ fuertemente expresados ​​pueden tener dificultades para adaptarse a la sociedad. Sus problemas generalmente se deben a que su función de sentimiento extravertido es tan dominante que están tan fuertemente vinculados a lo que ven como valores objetivos que no pueden relacionarse con el mundo excepto a través de los objetos de su sentimiento. En tales casos, la intensidad de sus juicios puede en realidad alejar a otros de ellos, y la falta resultante de una relación cercana se siente como una falla personal por la cual la ENFJ se siente culpable. Tal culpa puede impulsar un comportamiento afectivo aún más fuertemente que lleva al ENFJ a ignorar por completo sus propias necesidades, o puede convertirse en un impulso negativo para manipular a otros para que se ajusten a su visión unilateral del mundo. El ENFJ que trata constantemente de ver las posibilidades subyacentes y el alcance disponible en cada situación podrá ver el camino correcto a seguir con cada persona y situación en su vida. Esto siempre los llevará hacia relaciones más cercanas, felicidad y grandes logros.
La clave para el crecimiento personal de la ENFJ es la ejecución competente de la intuición introvertida. Debido a que a menudo es difícil definir lo que esto representa subjetivamente para cada persona, aquí hay algunas sugerencias orientadas a la acción que lo ayudarán a guiarlo por el camino hacia un uso más efectivo de la función intuitiva introvertida.",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'13',
            'part'=>'13',
            'title'=>"",
            'desc'=>"从上面可以看出，一些表达强烈的 ENFJ 可能难以融入社会。他们的问题通常是由于他们的外倾情感功能占主导地位，以至于他们与他们认为的客观价值如此紧密地联系在一起，以至于除了通过他们的感觉对象之外，他们无法与世界联系起来。在这种情况下，他们强烈的判断力实际上会驱使他人远离他们，由此导致的亲密关系的缺乏被认为是一种个人的失败，ENFJ 对此感到内疚。这种内疚可以驱动更强烈的情感行为，导致 ENFJ 完全忽略自己的需求，或者它可以成为操纵他人以符合他们片面的世界观的消极驱动力。 ENFJ 始终试图看到每种情况下的潜在可能性和可用范围，将能够看到正确的道路，可以应对每个人和他们生活中的情况。这将始终引导他们走向更紧密的关系、幸福和伟大的成就。
ENFJ 个人成长的关键是内向直觉的有效执行。因为通常很难定义这对每个人来说主观上代表什么，这里有一些以行动为导向的建议，将帮助您更有效地使用内倾直觉功能。",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'14',
            'title'=>"Specific suggestions:",
            'desc'=>"",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'14',
            'title'=>"اقتراحات محددة:",
            'desc'=>"",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'14',
            'title'=>"Suggestions spécifiques :",
            'desc'=>"",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'14',
            'title'=>"Özel öneriler:",
            'desc'=>"",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'14',
            'title'=>"Конкретные предложения:",
            'desc'=>"",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'14',
            'title'=>"Konkrete Vorschläge:",
            'desc'=>"",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'14',
            'title'=>"Sugerencias específicas:",
            'desc'=>"",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'0',
            'part'=>'14',
            'title'=>"具体建议：",
            'desc'=>"",
            'lang'=>'cn'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'14',
            'part'=>'14',
            'title'=>"",
            'desc'=>"When confronted by a person or situation which seems to be rejecting or rebuffing your value judgments and your mind filling with all the arguments, images and alternatives to the situation, look closely at those you are immediately rejecting as negative or unsuitable ways to proceed. Within these images often lie paths to understanding and agreement if you look more closely. Some of these images hold the key to seeing another’s feelings and point of view more clearly. Remember, what seems positive to you may not be everything or even important to another.
   
Behind everything of value that you see lies much potential. Try not to be satisfied with just a good result, but let yourself imagine the ways in which a person might fulfill all their creative aspects; the ways in which a situation might become useful to many more than just what it was made for. Try to imagine everything as a source of untapped magic and creative power – let your mind see all the things it might become. Above all, apply this exercise to yourself, as if you were seeing yourself in a mirror: just as you would another person whom you love.
When you are alone try to become fully aware of how it feels to you, try to recognize the emptiness as a place of potential, try to imagine what you might be able to do for others in this empty time, try to realize that you are not truly alone but with this special person who is yourself. What would you do for this person if you could make their private world a better place?
Everything wonderful in life proceeds from the qualities which lie behind it. You can feel these things, these drives and attitudes which seem to come from a place outside, perhaps from the creator expressing himself within people and nature. Letting the sense of these background qualities permeate your drive to life will give you purpose and meaning. Allow yourself to feel the meanings and purposes of the world, let them become a valuable gift which can be expressed in your dealings with others and in the things you strive for.",
            'lang'=>'en'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'14',
            'part'=>'14',
            'title'=>"",
            'desc'=>"عندما تواجه شخصًا أو موقفًا يبدو أنه يرفض أو يرفض أحكامك القيمية ويمتلئ عقلك بجميع الحجج والصور والبدائل للموقف ، انظر عن كثب إلى أولئك الذين ترفضهم على الفور على أنهم طرق سلبية أو غير مناسبة للمضي قدمًا. غالبًا ما تكمن في هذه الصور مسارات للتفاهم والاتفاق إذا نظرت عن كثب. تحمل بعض هذه الصور المفتاح لرؤية مشاعر الآخرين ووجهة نظرهم بشكل أكثر وضوحًا. تذكر أن ما يبدو إيجابيًا بالنسبة لك قد لا يكون كل شيء أو حتى مهمًا للآخرين.
   
وراء كل شيء ذا قيمة تكمن الكثير من الإمكانات. حاول ألا تكون راضيًا عن مجرد نتيجة جيدة ، ولكن دع نفسك تتخيل الطرق التي قد يحقق بها الشخص جميع جوانبه الإبداعية ؛ الطرق التي قد يصبح من خلالها الموقف مفيدًا للكثيرين غير مجرد ما تم إنشاؤه من أجله. حاول أن تتخيل كل شيء على أنه مصدر للسحر غير المستغل والقوة الإبداعية - دع عقلك يرى كل الأشياء التي قد يصبح عليها. قبل كل شيء ، قم بتطبيق هذا التمرين على نفسك ، كما لو كنت ترى نفسك في المرآة: تمامًا كما تفعل مع شخص آخر تحبه.
عندما تكون بمفردك تحاول أن تدرك تمامًا ما تشعر به تجاهك ، حاول التعرف على الفراغ كمكان محتمل ، حاول أن تتخيل ما قد تتمكن من القيام به للآخرين في هذا الوقت الفارغ ، حاول أن تدرك أنك لست وحدك حقًا ولكن مع هذا الشخص المميز الذي هو نفسك. ماذا ستفعل لهذا الشخص إذا كان بإمكانك جعل عالمه الخاص مكانًا أفضل؟
كل شيء رائع في الحياة ينبع من الصفات التي تكمن وراءه. يمكنك أن تشعر بهذه الأشياء ، هذه الدوافع والمواقف التي يبدو أنها تأتي من مكان في الخارج ، ربما من الخالق الذي يعبر عن نفسه في الناس والطبيعة. إن ترك الإحساس بهذه الصفات الخلفية يتخلل دافعك للحياة سيمنحك هدفًا ومعنى. اسمح لنفسك أن تشعر بمعاني ومقاصد العالم ، واجعلها هدية قيمة يمكن التعبير عنها في تعاملاتك مع الآخرين وفي الأشياء التي تسعى من أجلها.",
            'lang'=>'ar'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'14',
            'part'=>'14',
            'title'=>"",
            'desc'=>"Lorsque vous êtes confronté à une personne ou à une situation qui semble rejeter ou repousser vos jugements de valeur et votre esprit se remplissant de tous les arguments, images et alternatives à la situation, examinez attentivement ceux que vous rejetez immédiatement comme des manières de procéder négatives ou inappropriées. À l'intérieur de ces images se trouvent souvent des chemins vers la compréhension et l'accord si vous regardez de plus près. Certaines de ces images détiennent la clé pour voir plus clairement les sentiments et le point de vue d'autrui. N'oubliez pas que ce qui vous semble positif n'est peut-être pas tout ou même important pour un autre.
   
Derrière tout ce qui a de la valeur se cache un grand potentiel. Essayez de ne pas vous contenter d'un bon résultat, mais laissez-vous imaginer les manières dont une personne pourrait réaliser tous ses aspects créatifs ; les façons dont une situation peut devenir utile à bien plus que ce pour quoi elle a été créée. Essayez d'imaginer tout comme une source de magie inexploitée et de pouvoir créatif - laissez votre esprit voir tout ce qu'il pourrait devenir. Surtout, appliquez cet exercice à vous-même, comme si vous vous voyiez dans un miroir : comme vous le feriez pour une autre personne que vous aimez.
Lorsque vous êtes seul, essayez de devenir pleinement conscient de ce que vous ressentez, essayez de reconnaître le vide comme un lieu de potentiel, essayez d'imaginer ce que vous pourriez faire pour les autres en cette période vide, essayez de réaliser que vous êtes pas vraiment seul mais avec cette personne spéciale qui est vous-même. Que feriez-vous pour cette personne si vous pouviez faire de son monde privé un endroit meilleur ?
Tout ce qui est merveilleux dans la vie procède des qualités qui se cachent derrière lui. Vous pouvez ressentir ces choses, ces pulsions et attitudes qui semblent venir d'un endroit extérieur, peut-être du créateur s'exprimant à l'intérieur des gens et de la nature. Laisser le sens de ces qualités d'arrière-plan imprégner votre envie de vivre vous donnera un but et un sens. Permettez-vous de ressentir les significations et les buts du monde, laissez-les devenir un cadeau précieux qui peut s'exprimer dans vos relations avec les autres et dans les choses que vous recherchez.",
            'lang'=>'fr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'14',
            'part'=>'14',
            'title'=>"",
            'desc'=>"Değer yargılarınızı reddediyor veya reddediyor gibi görünen ve zihniniz duruma ilişkin tüm argümanlar, görüntüler ve alternatiflerle dolu olan bir kişi veya durumla karşılaştığınızda, hemen reddettiğinize olumsuz veya uygun olmayan yollar olarak yakından bakın. Daha yakından bakarsanız, bu görüntülerin içinde genellikle anlama ve anlaşma yolları bulunur. Bu görüntülerin bazıları, bir başkasının duygularını ve bakış açısını daha net görmenin anahtarıdır. Unutmayın, size olumlu görünen şey, başkası için her şey, hatta önemli olmayabilir.
   
Gördüğünüz değerli her şeyin arkasında çok fazla potansiyel yatıyor. Sadece iyi bir sonuçla yetinmemeye çalışın, ancak bir kişinin tüm yaratıcı yönlerini nasıl yerine getirebileceğini hayal edin; bir durumun ne için yaratıldığından çok daha fazlası için yararlı hale gelebileceği yolları. Her şeyi kullanılmayan bir sihir ve yaratıcı güç kaynağı olarak hayal etmeye çalışın - zihninizin olabilecek her şeyi görmesine izin verin. Her şeyden önce, kendinizi bir aynada görüyormuşsunuz gibi, bu alıştırmayı kendinize uygulayın: tıpkı sevdiğiniz başka birini yaptığınız gibi.
Yalnız olduğunuzda, bunun size nasıl hissettirdiğinin tamamen farkında olmaya çalışın, boşluğu potansiyel bir yer olarak tanımaya çalışın, bu boş zamanda başkaları için neler yapabileceğinizi hayal etmeye çalışın, kendinizin ne olduğunu anlamaya çalışın. gerçekten yalnız değil, kendiniz olan bu özel kişiyle. Özel dünyalarını daha iyi bir yer haline getirebilseydiniz bu kişi için ne yapardınız?
Hayatta harika olan her şey, arkasında yatan niteliklerden kaynaklanır. Dışarıdan bir yerden geliyormuş gibi görünen bu şeyleri, bu dürtüleri ve tavırları hissedebilirsiniz, belki de yaratıcının kendini insanların ve doğanın içinde ifade etmesinden. Bu arka plan niteliklerinin duygusunun yaşama güdünüze nüfuz etmesine izin vermek, size amaç ve anlam verecektir. Dünyanın anlamlarını ve amaçlarını hissetmek için kendinize izin verin, onların başkalarıyla olan ilişkilerinizde ve çabaladığınız şeylerde ifade edilebilecek değerli bir hediye olmasına izin verin.",
            'lang'=>'tr'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'14',
            'part'=>'14',
            'title'=>"",
            'desc'=>"Когда вы сталкиваетесь с человеком или ситуацией, которые, кажется, отвергают или отвергают ваши оценочные суждения и ваш разум наполняется всеми аргументами, образами и альтернативами ситуации, внимательно присмотритесь к тем, которые вы сразу отвергаете, как к отрицательным или неподходящим способам действовать. В этих образах часто лежат пути к пониманию и соглашению, если вы присмотритесь более внимательно. Некоторые из этих изображений являются ключом к более четкому пониманию чувств и точки зрения другого человека. Помните: то, что кажется вам положительным, может не быть всем или даже важным для другого.
   
За всем ценным, что вы видите, скрывается большой потенциал. Старайтесь не довольствоваться просто хорошим результатом, а позвольте себе представить, как человек может реализовать все свои творческие аспекты; способы, которыми ситуация может стать полезной для многих, помимо того, для чего она была создана. Попробуйте представить все как источник неиспользованной магии и творческой силы - позвольте своему разуму увидеть все, чем это может стать. Прежде всего, примените это упражнение к себе, как если бы вы видели себя в зеркале: как если бы вы видели другого человека, которого вы любите.
Когда вы один, попытайтесь полностью осознать, что вы чувствуете, попытайтесь распознать пустоту как место потенциала, попытайтесь представить, что вы могли бы сделать для других в это пустое время, постарайтесь осознать, что вы не на самом деле один, но с этим особенным человеком, которым является вы сами. Что бы вы сделали для этого человека, если бы смогли сделать его личный мир лучше?
Все прекрасное в жизни проистекает из лежащих в основе качеств. Вы можете почувствовать эти вещи, эти побуждения и отношения, которые, кажется, исходят извне, возможно, от создателя, выражающего себя в людях и природе. Позволив ощущению этих фоновых качеств проникнуть в ваше стремление к жизни, вы придадите вам цель и смысл. Позвольте себе почувствовать смыслы и цели мира, позвольте им стать ценным подарком, который может быть выражен в ваших отношениях с другими людьми и в вещах, к которым вы стремитесь.",
            'lang'=>'ru'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'14',
            'part'=>'14',
            'title'=>"",
            'desc'=>"Wenn Sie mit einer Person oder Situation konfrontiert werden, die Ihre Werturteile abzulehnen oder abzulehnen scheint und Ihr Geist sich mit all den Argumenten, Bildern und Alternativen zur Situation füllt, sehen Sie sich diejenigen genau an, die Sie sofort als negative oder ungeeignete Vorgehensweise ablehnen. In diesen Bildern liegen bei genauerem Hinsehen oft Wege zu Verständigung und Übereinstimmung. Einige dieser Bilder sind der Schlüssel, um die Gefühle und den Standpunkt eines anderen klarer zu sehen. Denken Sie daran, dass das, was Ihnen positiv erscheint, für andere möglicherweise nicht alles oder sogar wichtig ist.
   
Hinter allem, was Sie sehen, steckt viel Potenzial. Versuchen Sie, sich nicht nur mit einem guten Ergebnis zufrieden zu geben, sondern stellen Sie sich vor, wie eine Person all ihre kreativen Aspekte erfüllen könnte; die Art und Weise, wie eine Situation für viele mehr nützlich sein könnte, als nur für das, wofür sie geschaffen wurde. Stellen Sie sich alles als Quelle ungenutzter Magie und kreativer Kraft vor – lassen Sie Ihren Geist sehen, was daraus werden könnte. Wenden Sie diese Übung vor allem an sich selbst an, als würden Sie sich selbst im Spiegel sehen: so wie Sie es auch mit einem anderen Menschen tun würden, den Sie lieben.
Wenn Sie allein sind, versuchen Sie, sich voll und ganz bewusst zu werden, wie es sich für Sie anfühlt, versuchen Sie, die Leere als einen Ort des Potenzials zu erkennen, versuchen Sie sich vorzustellen, was Sie in dieser leeren Zeit für andere tun können, versuchen Sie zu erkennen, dass Sie es sind nicht wirklich allein, sondern mit dieser besonderen Person, die du selbst bist. Was würden Sie für diese Person tun, wenn Sie ihre private Welt zu einem besseren Ort machen könnten?
Alles Wunderbare im Leben geht von den Qualitäten aus, die dahinter stecken. Sie können diese Dinge spüren, diese Triebe und Einstellungen, die von einem Ort außerhalb zu kommen scheinen, vielleicht vom Schöpfer, der sich in Mensch und Natur ausdrückt. Das Gefühl dieser Hintergrundqualitäten Ihren Lebenstrieb durchdringen zu lassen, wird Ihnen Sinn und Bedeutung geben. Erlauben Sie sich die Bedeutungen und Zwecke der Welt zu spüren, lassen Sie sie zu einem wertvollen Geschenk werden, das sich in Ihrem Umgang mit anderen und in den Dingen, die Sie anstreben, ausdrücken kann.",
            'lang'=>'de'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'14',
            'part'=>'14',
            'title'=>"",
            'desc'=>"Cuando se enfrente a una persona o situación que parezca estar rechazando o rechazando sus juicios de valor y su mente llenándose con todos los argumentos, imágenes y alternativas a la situación, observe de cerca aquellos que está rechazando inmediatamente como formas negativas o inadecuadas de proceder. Dentro de estas imágenes a menudo se encuentran caminos hacia la comprensión y el acuerdo si se mira más de cerca. Algunas de estas imágenes contienen la clave para ver los sentimientos y el punto de vista de los demás con mayor claridad. Recuerde, lo que le parece positivo puede no serlo todo o incluso importante para otra persona.
   
Detrás de todo lo valioso que ves hay mucho potencial. Trate de no estar satisfecho con un buen resultado, sino permítase imaginar las formas en que una persona podría cumplir con todos sus aspectos creativos; las formas en que una situación puede resultar útil para muchos más que para lo que fue creada. Intente imaginar todo como una fuente de magia y poder creativo sin explotar; deje que su mente vea todas las cosas en las que podría convertirse. Sobre todo, aplícate este ejercicio a ti mismo, como si te estuvieras viendo en un espejo: como lo harías con otra persona a la que amas.
Cuando estés solo, trata de ser plenamente consciente de cómo te sientes, trata de reconocer el vacío como un lugar de potencial, trata de imaginar lo que podrías hacer por los demás en este tiempo vacío, trata de darte cuenta de que estás no realmente solo, sino con esta persona especial que eres tú. ¿Qué harías por esta persona si pudieras hacer de su mundo privado un lugar mejor?
Todo lo maravilloso de la vida procede de las cualidades que se esconden detrás de él. Puedes sentir estas cosas, estos impulsos y actitudes que parecen provenir de un lugar externo, quizás del creador que se expresa dentro de las personas y la naturaleza. Dejar que el sentido de estas cualidades de trasfondo permee su impulso a la vida le dará un propósito y significado. Permítete sentir los significados y propósitos del mundo, deja que se conviertan en un regalo valioso que puede expresarse en tu trato con los demás y en las cosas por las que luchas.",
            'lang'=>'es'
        ] );

        personTypes::create( [
            'person_id'=>'1',
            'person'=>'ENFJ',
            'sub'=>'14',
            'part'=>'14',
            'title'=>"",
            'desc'=>"当遇到一个似乎拒绝或拒绝你的价值判断的人或情况，并且你的脑海里充满了所有的论点、图像和情况的替代方案时，仔细观察那些你立即拒绝的人或情况，认为它们是消极的或不合适的继续方式。如果您仔细观察，这些图像中通常包含通向理解和达成一致的路径。其中一些图像是更清楚地看到他人感受和观点的关键。请记住，对您来说似乎是积极的东西可能并不是全部，甚至对他人来说都不是重要的。
   
您所看到的一切有价值的事物背后都蕴藏着巨大的潜力。尽量不要仅仅满足于一个好的结果，而是让你自己想象一个人可以如何实现他们所有的创造性方面；一种情况可能对更多人有用的方式，而不仅仅是它的用途。试着把一切想象成未开发的魔法和创造力的源泉——让你的头脑看到它可能变成的所有东西。最重要的是，把这个练习应用到你自己身上，就好像你在镜子里看到自己一样：就像你对另一个你爱的人一样。
当你独自一人时，试着充分意识到你的感受，试着认识到空虚是一个有潜力的地方，试着想象在这个空虚的时间你可以为别人做些什么，试着意识到你是不是真正孤身一人，而是与这个特殊的人在一起，即你自己。如果你能让他们的私人世界变得更美好，你会为这个人做什么？
生活中一切美好的事物都源于它背后的品质。你可以感受到这些东西，这些动力和态度似乎来自外部，也许来自创造者在人和自然中表达自己。让这些背景品质的感觉渗透到你的生活中，会给你带来目标和意义。让自己感受这个世界的意义和目的，让它们成为一种宝贵的礼物，可以在你与他人的交往和你所追求的事物中表达出来。",
            'lang'=>'cn'
        ] );



    }
}
