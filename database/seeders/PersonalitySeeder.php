<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PersonalityTest;

class PersonalitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PersonalityTest::create([
            'ques_id'   => '1',
            'question'  => "At a party do you: ",
            'ans_a'     => "Interact with many, including strangers",
            'ans_b'     => "Interact with a few, known to you",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '1',
            'question'  => "في الحفلة هل:",
            'ans_a'     => "تتفاعل مع الكثيرين ، بمن فيهم الغرباء",
            'ans_b'     => "تتفاعل مع القليل من الأشخاص المعروفين لك",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '1',
            'question'  => "Lors d'une fête, est-ce que vous :",
            'ans_a'     => "Interagir avec beaucoup, y compris des étrangers",
            'ans_b'     => "Interagissez avec quelques-uns que vous connaissez",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '1',
            'question'  => "Bir partide şunları yaparsınız:",
            'ans_a'     => "Yabancılar da dahil olmak üzere birçok kişiyle etkileşim kurun",
            'ans_b'     => "Tanıdığınız birkaç kişiyle etkileşim kurun",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '1',
            'question'  => "На вечеринке вы:",
            'ans_a'     => "Взаимодействовать со многими, в том числе с незнакомцами",
            'ans_b'     => "Взаимодействуйте с несколькими, известными вам",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '1',
            'question'  => "Machst du auf einer Party:",
            'ans_a'     => "Interagiere mit vielen, auch mit Fremden",
            'ans_b'     => "Interagiere mit einigen, die dir bekannt sind",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '1',
            'question'  => "En una fiesta:",
            'ans_a'     => "Interactuar con muchos, incluidos extraños",
            'ans_b'     => "Interactuar con algunos conocidos por ti",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '1',
            'question'  => "在聚会上，您是否：",
            'ans_a'     => "与许多人互动，包括陌生人",
            'ans_b'     => "与几个你认识的人互动",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '2',
            'question'  => "Are you more: ",
            'ans_a'     => "Realistic than speculative",
            'ans_b'     => "Speculative than realistic",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '2',
            'question'  => "هل انت اكثر:",
            'ans_a'     => "واقعية من التخمين",
            'ans_b'     => "تخميني أكثر من الواقعية",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '2',
            'question'  => "Êtes-vous plutôt :",
            'ans_a'     => "Réaliste que spéculatif",
            'ans_b'     => "Spéculatif que réaliste",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '2',
            'question'  => "daha fazla mısın:",
            'ans_a'     => "Spekülatiften daha gerçekçi",
            'ans_b'     => "Gerçekçi olmaktan çok spekülatif",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '2',
            'question'  => "Вы больше:",
            'ans_a'     => "Реалистично, чем умозрительно",
            'ans_b'     => "Умозрительно, чем реалистично",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '2',
            'question'  => "Bist du mehr:",
            'ans_a'     => "Realistisch als spekulativ",
            'ans_b'     => "Spekulativ statt realistisch",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '2',
            'question'  => "Eres más:",
            'ans_a'     => "Realista que especulativo",
            'ans_b'     => "Especulativo que realista",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '2',
            'question'  => "你更吗：",
            'ans_a'     => "现实多于投机",
            'ans_b'     => "投机大于现实",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '3',
            'question'  => "Is it worse to:",
            'ans_a'     => "Have your 'head in the clouds'",
            'ans_b'     => "Be 'in a rut'",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '3',
            'question'  => "هل من الأسوأ أن:",
            'ans_a'     => "يعيش في الأحلام",
            'ans_b'     => "أكون روتيني",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '3',
            'question'  => "Est-ce pire de :",
            'ans_a'     => 'Avoir la "tête dans les nuages"',
            'ans_b'     => "Être dans une routine'",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '3',
            'question'  => "Daha mı kötü:",
            'ans_a'     => "'kafanız bulutlarda' olsun",
            'ans_b'     => "'Çare içinde' olmak",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '3',
            'question'  => "Хуже:",
            'ans_a'     => "Твоя голова в облаках",
            'ans_b'     => "Быть в колее'",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '3',
            'question'  => "Ist es schlimmer:",
            'ans_a'     => "Haben Sie Ihren 'Kopf in den Wolken'",
            'ans_b'     => "Seien Sie 'in einer Spur'",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '3',
            'question'  => "¿Es peor:",
            'ans_a'     => "Ten tu 'cabeza en las nubes'",
            'ans_b'     => "Estar 'en una rutina'",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '3',
            'question'  => "是否更糟：",
            'ans_a'     => "让你的“头在云端”",
            'ans_b'     => "陷入困境",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '4',
            'question'  => "Are you more impressed by:",
            'ans_a'     => "Principles",
            'ans_b'     => "Emotions",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '4',
            'question'  => "هل أعجبك أكثر بـ:",
            'ans_a'     => "المبادئ",
            'ans_b'     => "العواطف",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '4',
            'question'  => "Êtes-vous plus impressionné par :",
            'ans_a'     => "Des principes",
            'ans_b'     => "Émotions",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '4',
            'question'  => "Daha çok mu etkilendiniz:",
            'ans_a'     => "Prensipler",
            'ans_b'     => "duygular",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '4',
            'question'  => "Вас больше впечатлили:",
            'ans_a'     => "Принципы",
            'ans_b'     => "Эмоции",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '4',
            'question'  => "Sind Sie mehr beeindruckt von:",
            'ans_a'     => "Grundsätze",
            'ans_b'     => "Emotionen",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '4',
            'question'  => "¿Estás más impresionado por:",
            'ans_a'     => "Principios",
            'ans_b'     => "Emociones",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '4',
            'question'  => "你印象更深刻的是：",
            'ans_a'     => "原则",
            'ans_b'     => "情绪",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '5',
            'question'  => "Are more drawn toward the:",
            'ans_a'     => "Convincing",
            'ans_b'     => "Touching",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '5',
            'question'  => "أكثر انجذابًا نحو:",
            'ans_a'     => "الإقناع",
            'ans_b'     => "التأثير",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '5',
            'question'  => "Sont plus attirés par :",
            'ans_a'     => "Convaincant",
            'ans_b'     => "Émouvant",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '5',
            'question'  => "Daha çok şunlara çekilir:",
            'ans_a'     => "İnandırıcı",
            'ans_b'     => "dokunmak",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '5',
            'question'  => "Больше тянутся к:",
            'ans_a'     => "Убедительный",
            'ans_b'     => "Трогательно",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '5',
            'question'  => "Sind mehr angezogen von:",
            'ans_a'     => "Überzeugend",
            'ans_b'     => "Berühren",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '5',
            'question'  => "Están más atraídos por:",
            'ans_a'     => "Convincente",
            'ans_b'     => "Conmovedor",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '5',
            'question'  => "更倾向于：",
            'ans_a'     => "令人信服",
            'ans_b'     => "接触",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '6',
            'question'  => "Do you prefer to work:",
            'ans_a'     => "To deadlines",
            'ans_b'     => "Just “whenever”",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '6',
            'question'  => "هل تفضل العمل:",
            'ans_a'     => "المحدد الوقت",
            'ans_b'     => "في أي وقت",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '6',
            'question'  => "Vous préférez travailler :",
            'ans_a'     => "Aux délais",
            'ans_b'     => 'Juste "à chaque fois"',
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '6',
            'question'  => "Çalışmayı tercih ediyor musunuz:",
            'ans_a'     => "son teslim tarihlerine",
            'ans_b'     => 'Sadece "ne zaman"',
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '6',
            'question'  => "Вы предпочитаете работать:",
            'ans_a'     => "К срокам",
            'ans_b'     => "Просто «когда»",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '6',
            'question'  => "Arbeitest du lieber:",
            'ans_a'     => "Zu den Fristen",
            'ans_b'     => 'Nur „immer“',
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '6',
            'question'  => "Prefieres trabajar:",
            'ans_a'     => "A los plazos",
            'ans_b'     => 'Solo "cuando"',
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '6',
            'question'  => "你喜欢工作吗：",
            'ans_a'     => "到最后期限",
            'ans_b'     => '只是“无论何时”',
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '7',
            'question'  => "Do you tend to choose:",
            'ans_a'     => "Rather carefully",
            'ans_b'     => "Somewhat impulsively",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '7',
            'question'  => "هل تميل إلى الاختيار:",
            'ans_a'     => "بكل بعناية",
            'ans_b'     => "إلى حد ما من الاندفاع",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '7',
            'question'  => "Avez-vous tendance à choisir :",
            'ans_a'     => "Plutôt soigneusement",
            'ans_b'     => "Un peu impulsivement",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '7',
            'question'  => "Şunları seçme eğilimindesiniz:",
            'ans_a'     => "oldukça dikkatli",
            'ans_b'     => "biraz dürtüsel olarak",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '7',
            'question'  => "Вы склонны выбирать:",
            'ans_a'     => "Скорее осторожно",
            'ans_b'     => "Несколько импульсивно",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '7',
            'question'  => "Wählen Sie eher:",
            'ans_a'     => "Eher vorsichtig",
            'ans_b'     => "Etwas impulsiv",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '7',
            'question'  => "Suele elegir:",
            'ans_a'     => "Con bastante cuidado",
            'ans_b'     => "Algo impulsivamente",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '7',
            'question'  => "你是否倾向于选择：",
            'ans_a'     => "比较仔细",
            'ans_b'     => "有点冲动",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '8',
            'question'  => "At parties do you:",
            'ans_a'     => "Stay late, with increasing energy",
            'ans_b'     => "Leave early with decreased energy",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '8',
            'question'  => "في الحفلات هل:",
            'ans_a'     => "ابق متأخرًا ، مع زيادة الطاقة",
            'ans_b'     => "غادر مبكرًا مع انخفاض الطاقة",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '8',
            'question'  => "Lors des fêtes, est-ce que vous :",
            'ans_a'     => "Rester tard, avec une énergie croissante",
            'ans_b'     => "Partir tôt avec une énergie réduite",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '8',
            'question'  => "Partilerde şunları yaparsınız:",
            'ans_a'     => "Artan enerjiyle geç kalın",
            'ans_b'     => "Azaltılmış enerjiyle erken ayrılın",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '8',
            'question'  => "На вечеринках вы:",
            'ans_a'     => "Оставайтесь допоздна, с возрастающей энергией",
            'ans_b'     => "Уходите рано с пониженной энергией",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '8',
            'question'  => "Auf Partys machst du:",
            'ans_a'     => "Bleiben Sie spät, mit zunehmender Energie",
            'ans_b'     => "Gehen Sie früh mit weniger Energie",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '8',
            'question'  => "En las fiestas:",
            'ans_a'     => "Quédate hasta tarde, con energía creciente",
            'ans_b'     => "Sal temprano con energía disminuida",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '8',
            'question'  => "在聚会上，您是否：",
            'ans_a'     => "熬夜，精力充沛",
            'ans_b'     => "提早离开，精力下降",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '9',
            'question'  => "Are you more attracted to:",
            'ans_a'     => "Sensible people",
            'ans_b'     => "Imaginative people",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '9',
            'question'  => "هل تنجذب أكثر إلى:",
            'ans_a'     => "المتعقلون",
            'ans_b'     => "المبدعون",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '9',
            'question'  => "Êtes-vous plus attiré par :",
            'ans_a'     => "Des gens sensés",
            'ans_b'     => "Des gens imaginatifs",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '9',
            'question'  => "Daha çok çekici misiniz:",
            'ans_a'     => "mantıklı insanlar",
            'ans_b'     => "yaratıcı insanlar",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '9',
            'question'  => "Вас больше привлекают:",
            'ans_a'     => "Здравомыслящие люди",
            'ans_b'     => "Творческие люди",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '9',
            'question'  => "Sind Sie mehr angezogen von:",
            'ans_a'     => "Vernünftige Menschen",
            'ans_b'     => "Fantasievolle Menschen",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '9',
            'question'  => "¿Te atrae más:",
            'ans_a'     => "Gente sensata",
            'ans_b'     => "Gente imaginativa",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '9',
            'question'  => "你是否更喜欢：",
            'ans_a'     => "懂事的人",
            'ans_b'     => "富有想象力的人",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '10',
            'question'  => "Are you more interested in: ",
            'ans_a'     => "What is actual",
            'ans_b'     => "What is possible",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '10',
            'question'  => "هل أنت مهتم أكثر بـ:",
            'ans_a'     => "ما هو الفعلي",
            'ans_b'     => "ما هو ممكن",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '10',
            'question'  => "Êtes-vous plus intéressé par :",
            'ans_a'     => "Qu'est-ce qui est réel",
            'ans_b'     => "Ce qui est possible",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '10',
            'question'  => "Daha çok ilgileniyor musunuz:",
            'ans_a'     => "gerçek nedir",
            'ans_b'     => "ne mümkün",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '10',
            'question'  => "Вас больше интересуют:",
            'ans_a'     => "Что актуально",
            'ans_b'     => "Что возможно",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '10',
            'question'  => "Interessieren Sie sich mehr für:",
            'ans_a'     => "Was ist aktuell",
            'ans_b'     => "Was ist möglich",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '10',
            'question'  => "¿Está más interesado en:",
            'ans_a'     => "Que es real",
            'ans_b'     => "Que es posible",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '10',
            'question'  => "您是否更感兴趣：",
            'ans_a'     => "什么是实际",
            'ans_b'     => "什么是可能的",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'cn',
        ]);



       PersonalityTest::create([
            'ques_id'   => '11',
            'question'  => "In judging others are you more swayed by:",
            'ans_a'     => "Laws than circumstances",
            'ans_b'     => "Circumstances than laws",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '11',
            'question'  => "في الحكم على الآخرين ، تتأثر أكثر بما يلي:",
            'ans_a'     => "قوانين أهم من الظروف",
            'ans_b'     => "ظروف أهم من القوانين",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '11',
            'question'  => "En jugeant les autres, êtes-vous plus influencé par :",
            'ans_a'     => "Des lois plus que des circonstances",
            'ans_b'     => "Des circonstances que des lois",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '11',
            'question'  => "Başkalarını yargılarken şunlara daha fazla kafa yorarsınız:",
            'ans_a'     => "Koşullardan çok yasalar",
            'ans_b'     => "Kanunlardan daha fazla şartlar",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '11',
            'question'  => "В оценке других вас больше склоняют:",
            'ans_a'     => "Законы, чем обстоятельства",
            'ans_b'     => "Обстоятельства, чем законы",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '11',
            'question'  => "Wenn Sie andere beurteilen, werden Sie mehr beeinflusst von:",
            'ans_a'     => "Gesetze statt Umstände",
            'ans_b'     => "Umstände als Gesetze",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '11',
            'question'  => "Al juzgar a los demás, te influye más:",
            'ans_a'     => "Leyes que circunstancias",
            'ans_b'     => "Circunstancias que las leyes",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '11',
            'question'  => "在评判他人时，您是否更受以下因素影响：",
            'ans_a'     => "法律胜于环境",
            'ans_b'     => "情况胜于法律",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '12',
            'question'  => "In approaching others is your inclination to be somewhat:",
            'ans_a'     => "Objective",
            'ans_b'     => "Personal",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '12',
            'question'  => "عند الاقتراب من الآخرين ، تميل إلى أن تكون نوعًا ما:",
            'ans_a'     => "موضوعي",
            'ans_b'     => "شخصي",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '12',
            'question'  => "En approchant les autres, vous avez tendance à être un peu :",
            'ans_a'     => "Objectif",
            'ans_b'     => "Personnel",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '12',
            'question'  => "Başkalarına yaklaşırken biraz olma eğiliminiz:",
            'ans_a'     => "Amaç",
            'ans_b'     => "Kişiye özel",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '12',
            'question'  => "Приближаясь к другим, вы склонны быть в некоторой степени:",
            'ans_a'     => "Задача",
            'ans_b'     => "Личное",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '12',
            'question'  => "Bei der Annäherung an andere ist Ihre Neigung, etwas zu sein:",
            'ans_a'     => "Zielsetzung",
            'ans_b'     => "persönlich",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '12',
            'question'  => "Al acercarse a los demás, su inclinación es ser algo:",
            'ans_a'     => "Objetivo",
            'ans_b'     => "Personal",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '12',
            'question'  => "在接近他人时，你的倾向是：",
            'ans_a'     => "客观的",
            'ans_b'     => "个人的",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '13',
            'question'  => "Are you more:",
            'ans_a'     => "Punctual",
            'ans_b'     => "Leisurely",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '13',
            'question'  => "هل انت اكثر:",
            'ans_a'     => "منضبط",
            'ans_b'     => "على مهل",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '13',
            'question'  => "Êtes-vous plutôt :",
            'ans_a'     => "Ponctuelle",
            'ans_b'     => "Tranquille",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '13',
            'question'  => "daha fazla mısın:",
            'ans_a'     => "Dakik",
            'ans_b'     => "sakince",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '13',
            'question'  => "Вы больше:",
            'ans_a'     => "Пунктуальный",
            'ans_b'     => "Неторопливый",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '13',
            'question'  => "Bist du mehr:",
            'ans_a'     => "Pünktlich",
            'ans_b'     => "Gemächlich",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '13',
            'question'  => "Eres más:",
            'ans_a'     => "Puntual",
            'ans_b'     => "Sin prisa",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '13',
            'question'  => "你更吗：",
            'ans_a'     => "准时",
            'ans_b'     => "悠闲",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '14',
            'question'  => "Does it bother you more having things:",
            'ans_a'     => "Incomplete",
            'ans_b'     => "Completed",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '14',
            'question'  => "هل يزعجك المزيد من امتلاك الأشياء:",
            'ans_a'     => "الغير مكتملة",
            'ans_b'     => "المكتملة",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '14',
            'question'  => "Est-ce que ça vous dérange plus d'avoir des choses :",
            'ans_a'     => "Incomplet",
            'ans_b'     => "Complété",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '14',
            'question'  => "Şu şeylere sahip olmak seni daha çok rahatsız ediyor mu?",
            'ans_a'     => "eksik",
            'ans_b'     => "Tamamlanmış",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '14',
            'question'  => "Вас больше беспокоит наличие вещей:",
            'ans_a'     => "Неполный",
            'ans_b'     => "Завершенный",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '14',
            'question'  => "Stört es dich mehr, Dinge zu haben:",
            'ans_a'     => "Unvollständig",
            'ans_b'     => "Vollendet",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '14',
            'question'  => "¿Te molesta más tener cosas:",
            'ans_a'     => "Incompleto",
            'ans_b'     => "Terminado",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '14',
            'question'  => "有东西更烦你吗：",
            'ans_a'     => "不完整",
            'ans_b'     => "完全的",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '15',
            'question'  => "In your social groups do you:",
            'ans_a'     => "Keep abreast of other’s happenings",
            'ans_b'     => "Get behind on the news",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '15',
            'question'  => "في مجموعاتك الاجتماعية هل:",
            'ans_a'     => "أحافظ على قربي من الأحدات",
            'ans_b'     => "الحصول على وراء على الأخبار",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '15',
            'question'  => "Dans vos groupes sociaux, êtes-vous :",
            'ans_a'     => "Se tenir au courant des événements des autres",
            'ans_b'     => "Prendre du retard sur l'actualité",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '15',
            'question'  => "Sosyal gruplarınızda:",
            'ans_a'     => "Başkalarının olaylarından haberdar olun",
            'ans_b'     => "Haberin gerisinde kalın",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '15',
            'question'  => "В своих социальных группах вы:",
            'ans_a'     => "Будьте в курсе событий других",
            'ans_b'     => "Следите за новостями",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '15',
            'question'  => "Tun Sie in Ihren sozialen Gruppen:",
            'ans_a'     => "Bleiben Sie über die Geschehnisse anderer auf dem Laufenden",
            'ans_b'     => "Bleiben Sie in den Nachrichten zurück",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '15',
            'question'  => "En sus grupos sociales:",
            'ans_a'     => "Mantente al tanto de los acontecimientos de otros",
            'ans_b'     => "Atrasarse en las noticias",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '15',
            'question'  => "在您的社交群体中，您是否：",
            'ans_a'     => "随时了解他人的情况",
            'ans_b'     => "落后于新闻",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '16',
            'question'  => "In doing ordinary things are you more likely to:",
            'ans_a'     => "Do it the usual way",
            'ans_b'     => "Do it your own way",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '16',
            'question'  => "عند القيام بالأشياء العادية ، من المرجح أن:",
            'ans_a'     => "افعلها بالطريقة المعتادة",
            'ans_b'     => "افعلها بطريقتك الخاصة",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '16',
            'question'  => "En faisant des choses ordinaires, êtes-vous plus susceptible de :",
            'ans_a'     => "Fais-le comme d'habitude",
            'ans_b'     => "Faites-le à votre façon",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '16',
            'question'  => "Sıradan şeyler yaparken aşağıdakileri yapma olasılığınız daha yüksektir:",
            'ans_a'     => "her zamanki gibi yap",
            'ans_b'     => "kendi yönteminle yap",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '16',
            'question'  => "Занимаясь обычными делами, вы с большей вероятностью:",
            'ans_a'     => "Делай это как обычно",
            'ans_b'     => "Делай по-своему",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '16',
            'question'  => "Wenn Sie gewöhnliche Dinge tun, ist es wahrscheinlicher, dass Sie:",
            'ans_a'     => "Mach es wie gewohnt",
            'ans_b'     => "Mach es auf deine Art",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '16',
            'question'  => "Al hacer cosas ordinarias, es más probable que:",
            'ans_a'     => "Hazlo de la manera habitual",
            'ans_b'     => "Hazlo a tu manera",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '16',
            'question'  => "在做普通的事情时，你更有可能：",
            'ans_a'     => "照常做",
            'ans_b'     => "做你自己的方式",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '17',
            'question'  => "Writers should:",
            'ans_a'     => "Say what they mean and mean what they say",
            'ans_b'     => "Express things more by use of analogy",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '17',
            'question'  => "يجب على الكتاب:",
            'ans_a'     => "يقولون ما يقصدونه ويعنون ما يقولون",
            'ans_b'     => "التعبير عن الأشياء أكثر باستخدام القياس",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '17',
            'question'  => "Les écrivains doivent :",
            'ans_a'     => "Dis ce qu'ils veulent dire et pense ce qu'ils disent",
            'ans_b'     => "Exprimez davantage les choses en utilisant l'analogie",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '17',
            'question'  => "Yazarlar şunları yapmalıdır:",
            'ans_a'     => "Ne demek istediklerini söyle ve ne dediklerini kastet",
            'ans_b'     => "Analoji kullanarak şeyleri daha fazla ifade edin",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '17',
            'question'  => "Писателям следует:",
            'ans_a'     => "Скажите, что они имеют в виду и имеют в виду то, что они говорят",
            'ans_b'     => "Выражайте больше, используя аналогию",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '17',
            'question'  => "Autoren sollten:",
            'ans_a'     => "Sagen Sie, was sie meinen und meinen Sie, was sie sagen",
            'ans_b'     => "Drücke Dinge mehr durch die Verwendung von Analogien aus",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '17',
            'question'  => "Los escritores deben:",
            'ans_a'     => "Decir lo que quieren decir y decir lo que dicen",
            'ans_b'     => "Expresa más las cosas mediante el uso de la analogía.",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '17',
            'question'  => "作家应该：",
            'ans_a'     => "说出他们的意思和他们所说的意思",
            'ans_b'     => "多用类比表达",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '18',
            'question'  => "Which appeals to you more:",
            'ans_a'     => "Consistency of thought",
            'ans_b'     => "Harmonious human relationships",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '18',
            'question'  => "الذي يروق لك أكثر:",
            'ans_a'     => "تناسق افكار",
            'ans_b'     => "علاقة الانسجام ",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '18',
            'question'  => "Ce qui vous interpelle le plus :",
            'ans_a'     => "Cohérence de la pensée",
            'ans_b'     => "Des relations humaines harmonieuses",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '18',
            'question'  => "Hangisi size daha çok hitap ediyor:",
            'ans_a'     => "Düşünce tutarlılığı",
            'ans_b'     => "Uyumlu insan ilişkileri",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '18',
            'question'  => "Что вам больше нравится:",
            'ans_a'     => "Последовательность мысли",
            'ans_b'     => "Гармоничные человеческие отношения",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '18',
            'question'  => "Was dich mehr anspricht:",
            'ans_a'     => "Gedankenfluss",
            'ans_b'     => "Harmonische menschliche Beziehungen",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '18',
            'question'  => "Lo que te atrae más:",
            'ans_a'     => "Consistencia de pensamiento",
            'ans_b'     => "Relaciones humanas armoniosas",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '18',
            'question'  => "哪个更吸引你：",
            'ans_a'     => "思想的一致性",
            'ans_b'     => "和谐的人际关系",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '19',
            'question'  => "Are you more comfortable in making:",
            'ans_a'     => "Logical judgments",
            'ans_b'     => "Value judgments",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '19',
            'question'  => "هل أنت أكثر راحة في صنع:",
            'ans_a'     => "أحكام منطقية",
            'ans_b'     => "أحكام قيمية",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '19',
            'question'  => "Êtes-vous plus à l'aise pour faire :",
            'ans_a'     => "Jugements logiques",
            'ans_b'     => "Jugements de valeur",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '19',
            'question'  => "Şunları yaparken daha rahatsınız:",
            'ans_a'     => "mantıksal yargılar",
            'ans_b'     => "değer yargıları",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '19',
            'question'  => "Вам удобнее делать:",
            'ans_a'     => "Логические суждения",
            'ans_b'     => "Оценочные суждения",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '19',
            'question'  => "Sind Sie bei der Herstellung von:",
            'ans_a'     => "Logische Urteile",
            'ans_b'     => "Werturteile",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '19',
            'question'  => "¿Se siente más cómodo al hacer:",
            'ans_a'     => "Juicios lógicos",
            'ans_b'     => "Juicios de valor",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '19',
            'question'  => "你在制作时更舒服吗：",
            'ans_a'     => "逻辑判断",
            'ans_b'     => "价值判断",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '20',
            'question'  => "Do you want things:",
            'ans_a'     => "Settled and decided",
            'ans_b'     => "Unsettled and undecided",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '20',
            'question'  => "هل تريد اشياء:",
            'ans_a'     => "استقر وقرر",
            'ans_b'     => "غير مستقر و متردد",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '20',
            'question'  => "Voulez-vous des choses :",
            'ans_a'     => "Installé et décidé",
            'ans_b'     => "Inquiet et indécis",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '20',
            'question'  => "şeyler istiyor musun:",
            'ans_a'     => "Yerleşti ve karar verdi",
            'ans_b'     => "Kararsız ve kararsız",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '20',
            'question'  => "Вы хотите вещи:",
            'ans_a'     => "Решился и решил",
            'ans_b'     => "Неурегулированный и нерешительный",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '20',
            'question'  => "Willst du Dinge:",
            'ans_a'     => "Festgelegt und entschieden",
            'ans_b'     => "Verunsichert und unentschlossen",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '20',
            'question'  => "Quieres cosas:",
            'ans_a'     => "Asentado y decidido",
            'ans_b'     => "Inquieto e indeciso",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '20',
            'question'  => "你想要的东西：",
            'ans_a'     => "解决并决定",
            'ans_b'     => "犹豫不决",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'cn',
        ]);



       PersonalityTest::create([
            'ques_id'   => '21',
            'question'  => "Would you say you are more:",
            'ans_a'     => "Serious and determined",
            'ans_b'     => "Easy-going",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '21',
            'question'  => "هل تقول أنك أكثر:",
            'ans_a'     => "جاد وحازم",
            'ans_b'     => "سهل الإختيار",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '21',
            'question'  => "Diriez-vous que vous êtes plus :",
            'ans_a'     => "Sérieux et déterminé",
            'ans_b'     => "Facile à vivre",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '21',
            'question'  => "Daha fazlası olduğunu söyler misin:",
            'ans_a'     => "Ciddi ve kararlı",
            'ans_b'     => "Kolay ve kesintisiz ilerleme",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '21',
            'question'  => "Вы бы сказали, что вы больше:",
            'ans_a'     => "Серьезный и решительный",
            'ans_b'     => "С легким характером",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '21',
            'question'  => "Würden Sie sagen, Sie sind mehr:",
            'ans_a'     => "Ernst und entschlossen",
            'ans_b'     => "Unbeschwerte",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '21',
            'question'  => "Dirías que eres más:",
            'ans_a'     => "Serio y decidido",
            'ans_b'     => "De trato fácil",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '21',
            'question'  => "你会说你更吗：",
            'ans_a'     => "严肃而坚定",
            'ans_b'     => "随和",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '22',
            'question'  => "In phoning do you:",
            'ans_a'     => "Rarely question that it will all be said",
            'ans_b'     => "Rehearse what you’ll say",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '22',
            'question'  => "في الاتصال الهاتفي هل:",
            'ans_a'     => "نادرًا ما يشكك في أن كل ذلك سيقال",
            'ans_b'     => "تدرب على ما ستقوله",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '22',
            'question'  => "En téléphonant :",
            'ans_a'     => "Rarement question que tout sera dit",
            'ans_b'     => "Répétez ce que vous allez dire",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '22',
            'question'  => "Telefon ederken şunları yaparsınız:",
            'ans_a'     => "Nadiren her şeyin söyleneceği sorulur",
            'ans_b'     => "Ne söyleyeceğinizi prova edin",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '22',
            'question'  => "При звонке вы:",
            'ans_a'     => "Редко сомневаюсь, что все будет сказано",
            'ans_b'     => "Репетируйте, что вы скажете",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '22',
            'question'  => "Telefonieren Sie:",
            'ans_a'     => "Fragt sich selten, dass alles gesagt wird",
            'ans_b'     => "Üben Sie, was Sie sagen werden",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '22',
            'question'  => "Al llamar por teléfono:",
            'ans_a'     => "Rara vez cuestiona que todo se dirá",
            'ans_b'     => "Ensaya lo que dirás",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '22',
            'question'  => "在打电话时，您是否：",
            'ans_a'     => "很少质疑这一切都会说出来",
            'ans_b'     => "排练你要说的话",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '23',
            'question'  => "Facts:",
            'ans_a'     => "Speak for themselves",
            'ans_b'     => "Illustrate principles",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '23',
            'question'  => "الحقائق:",
            'ans_a'     => "تتحدث عن أنفسها",
            'ans_b'     => "تظهر مبادئها",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '23',
            'question'  => "Les faits:",
            'ans_a'     => "Parler d'eux-mêmes",
            'ans_b'     => "Illustrer les principes",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '23',
            'question'  => "Gerçekler:",
            'ans_a'     => "kendi adına konuş",
            'ans_b'     => "İlkeleri örnekleyin",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '23',
            'question'  => "Факты:",
            'ans_a'     => "Говорят сами за себя",
            'ans_b'     => "Проиллюстрируйте принципы",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '23',
            'question'  => "Fakten:",
            'ans_a'     => "Sprechen Sie für sich",
            'ans_b'     => "Prinzipien veranschaulichen",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '23',
            'question'  => "Hechos:",
            'ans_a'     => "Hablar por ellos mismos",
            'ans_b'     => "Ilustrar principios",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '23',
            'question'  => "事实：",
            'ans_a'     => "为自己说话",
            'ans_b'     => "说明原则",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '24',
            'question'  => "Are visionaries:",
            'ans_a'     => "somewhat annoying",
            'ans_b'     => "rather fascinating",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '24',
            'question'  => "هم حالمون:",
            'ans_a'     => "مزعج إلى حد ما",
            'ans_b'     => "رائعة نوعا ما",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '24',
            'question'  => "Sont des visionnaires :",
            'ans_a'     => "un peu ennuyeux",
            'ans_b'     => "plutôt fascinant",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '24',
            'question'  => "Vizyonerler:",
            'ans_a'     => "biraz sinir bozucu",
            'ans_b'     => "oldukça büyüleyici",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '24',
            'question'  => "Провидцы:",
            'ans_a'     => "несколько раздражает",
            'ans_b'     => "довольно увлекательно",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '24',
            'question'  => "Sind Visionäre:",
            'ans_a'     => "etwas nervig",
            'ans_b'     => "eher faszinierend",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '24',
            'question'  => "Son visionarios:",
            'ans_a'     => "algo molesto",
            'ans_b'     => "bastante fascinante",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '24',
            'question'  => "是否有远见：",
            'ans_a'     => "有点烦",
            'ans_b'     => "相当迷人",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '25',
            'question'  => "Are you more often:",
            'ans_a'     => "a cool-headed person",
            'ans_b'     => "a warm-hearted person",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '25',
            'question'  => "هل أنت في كثير من الأحيان:",
            'ans_a'     => "شخص هادئ الرأس",
            'ans_b'     => "شخص طيب القلب",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '25',
            'question'  => "Êtes-vous plus souvent :",
            'ans_a'     => "une personne à la tête froide",
            'ans_b'     => "une personne chaleureuse",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '25',
            'question'  => "Daha sık mısınız:",
            'ans_a'     => "soğukkanlı bir insan",
            'ans_b'     => "sıcakkanlı insan",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '25',
            'question'  => "Вы чаще:",
            'ans_a'     => "хладнокровный человек",
            'ans_b'     => "сердечный человек",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '25',
            'question'  => "Sind Sie häufiger:",
            'ans_a'     => "ein besonnener Mensch",
            'ans_b'     => "ein warmherziger Mensch",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '25',
            'question'  => "¿Eres más a menudo:",
            'ans_a'     => "una persona tranquila",
            'ans_b'     => "una persona de buen corazón",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '25',
            'question'  => "你是否更经常：",
            'ans_a'     => "冷静的人",
            'ans_b'     => "一个热心的人",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '26',
            'question'  => "Is it worse to be:",
            'ans_a'     => "unjust",
            'ans_b'     => "merciless",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '26',
            'question'  => "هل هو أسوأ أن تكون:",
            'ans_a'     => "غير عادل",
            'ans_b'     => "بدون رحمة",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '26',
            'question'  => "Est-ce pire d'être :",
            'ans_a'     => "injuste",
            'ans_b'     => "sans merci",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '26',
            'question'  => "Olmak daha mı kötü:",
            'ans_a'     => "haksız",
            'ans_b'     => "acımasız",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '26',
            'question'  => "Хуже быть:",
            'ans_a'     => "несправедливый",
            'ans_b'     => "беспощадный",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '26',
            'question'  => "Ist es schlimmer:",
            'ans_a'     => "ungerecht",
            'ans_b'     => "gnadenlos",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '26',
            'question'  => "¿Es peor ser:",
            'ans_a'     => "injusto",
            'ans_b'     => "despiadado",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '26',
            'question'  => "是否更糟：",
            'ans_a'     => "不公正",
            'ans_b'     => "无情",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '27',
            'question'  => "Should one usually let events occur:",
            'ans_a'     => "by careful selection and choice",
            'ans_b'     => "randomly and by chance",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '27',
            'question'  => "هل ينبغي للمرء عادة السماح للأحداث بالحدوث:",
            'ans_a'     => "عن طريق الاختيار والاختيار الدقيق",
            'ans_b'     => "بشكل عشوائي وعن طريق الصدفة",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '27',
            'question'  => "Faut-il généralement laisser les événements se produire :",
            'ans_a'     => "par une sélection et un choix minutieux",
            'ans_b'     => "au hasard et par hasard",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '27',
            'question'  => "Genellikle olayların gerçekleşmesine izin verilir mi:",
            'ans_a'     => "dikkatli seçim ve seçim ile",
            'ans_b'     => "rastgele ve tesadüfen",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '27',
            'question'  => "Если обычно допускаются события:",
            'ans_a'     => "путем тщательного отбора и выбора",
            'ans_b'     => "случайно и случайно",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '27',
            'question'  => "Sollte man normalerweise Ereignisse eintreten lassen:",
            'ans_a'     => "durch sorgfältige Auswahl und Auswahl",
            'ans_b'     => "zufällig und zufällig",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '27',
            'question'  => "¿Debería uno dejar que los eventos ocurran normalmente?",
            'ans_a'     => "mediante una cuidadosa selección y elección",
            'ans_b'     => "al azar y por casualidad",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '27',
            'question'  => "通常应该让事件发生：",
            'ans_a'     => "通过精心挑选和选择",
            'ans_b'     => "随机和偶然",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '28',
            'question'  => "Do you feel better about:",
            'ans_a'     => "having purchased",
            'ans_b'     => "having the option to buy",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '28',
            'question'  => "هل تشعر بتحسن بشأن:",
            'ans_a'     => "بعد أن اشتريت",
            'ans_b'     => "امتلاك خيار الشراء",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '28',
            'question'  => "Vous sentez-vous mieux :",
            'ans_a'     => "avoir acheté",
            'ans_b'     => "avoir la possibilité d'acheter",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '28',
            'question'  => "Kendinizi daha iyi hissediyor musunuz:",
            'ans_a'     => "satın almış",
            'ans_b'     => "satın alma seçeneğine sahip olmak",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '28',
            'question'  => "Вам лучше по поводу:",
            'ans_a'     => "купив",
            'ans_b'     => "имея возможность купить",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '28',
            'question'  => "Fühlst du dich besser bei:",
            'ans_a'     => "gekauft haben",
            'ans_b'     => "die Kaufoption haben",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '28',
            'question'  => "¿Se siente mejor acerca de:",
            'ans_a'     => "haber comprado",
            'ans_b'     => "tener la opción de comprar",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '28',
            'question'  => "您是否感觉更好：",
            'ans_a'     => "购买了",
            'ans_b'     => "可以选择购买",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '29',
            'question'  => "In company do you:",
            'ans_a'     => "initiate conversation",
            'ans_b'     => "wait to be approached",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '29',
            'question'  => "في الجماعة هل أنت:",
            'ans_a'     => "تبدء المحادثة",
            'ans_b'     => "تنتظر حتى يقترب منك",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '29',
            'question'  => "En compagnie, êtes-vous :",
            'ans_a'     => "initier la conversation",
            'ans_b'     => "attendre d'être approché",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '29',
            'question'  => "Şirkette şunları yaparsınız:",
            'ans_a'     => "konuşmayı başlat",
            'ans_b'     => "yaklaşmayı bekle",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '29',
            'question'  => "В компании вы:",
            'ans_a'     => "начать разговор",
            'ans_b'     => "ждать, чтобы к нему подошли",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '29',
            'question'  => "In Gesellschaft tun Sie:",
            'ans_a'     => "Gespräch einleiten",
            'ans_b'     => "warte darauf, angesprochen zu werden",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '29',
            'question'  => "En compañía usted:",
            'ans_a'     => "iniciar conversación",
            'ans_b'     => "esperar a que se acerquen",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '29',
            'question'  => "在公司你：",
            'ans_a'     => "发起对话",
            'ans_b'     => "等待被接近",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '30',
            'question'  => "Common sense is:",
            'ans_a'     => "rarely questionable",
            'ans_b'     => "frequently questionable",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '30',
            'question'  => "الفطرة السليمة هي:",
            'ans_a'     => "نادرا ما مشكوك فيه",
            'ans_b'     => "كثيرا ما يكون موضع تساؤل",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '30',
            'question'  => "Le bon sens c'est :",
            'ans_a'     => "rarement contestable",
            'ans_b'     => "souvent discutable",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '30',
            'question'  => "Sağduyu şudur:",
            'ans_a'     => "nadiren sorgulanabilir",
            'ans_b'     => "sıklıkla sorgulanabilir",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '30',
            'question'  => "Здравый смысл таков:",
            'ans_a'     => "редко сомнительный",
            'ans_b'     => "часто сомнительный",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '30',
            'question'  => "Gesunder Menschenverstand ist:",
            'ans_a'     => "selten fragwürdig",
            'ans_b'     => "häufig fragwürdig",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '30',
            'question'  => "El sentido común es:",
            'ans_a'     => "rara vez cuestionable",
            'ans_b'     => "frecuentemente cuestionable",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '30',
            'question'  => "常识是：",
            'ans_a'     => "很少有问题",
            'ans_b'     => "经常受到质疑",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'cn',
        ]);



       PersonalityTest::create([
            'ques_id'   => '31',
            'question'  => "Children often do not:",
            'ans_a'     => "make themselves useful enough",
            'ans_b'     => "exercise their fantasy enough",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '31',
            'question'  => "غالبًا ما لا يقوم الأطفال بما يلي:",
            'ans_a'     => "يجعلون أنفسهم مفيدين بما فيه الكفاية",
            'ans_b'     => "ممارسة خيالهم بما فيه الكفاية",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '31',
            'question'  => "Les enfants ne font souvent pas :",
            'ans_a'     => "se rendre suffisamment utile",
            'ans_b'     => "exercer suffisamment leur fantasme",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '31',
            'question'  => "Çocuklar genellikle şunları yapmaz:",
            'ans_a'     => "kendilerini yeterince faydalı kılmak",
            'ans_b'     => "fantezilerini yeterince uygula",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '31',
            'question'  => "Дети часто не делают:",
            'ans_a'     => "сделать себя достаточно полезными",
            'ans_b'     => "достаточно проявить свою фантазию",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '31',
            'question'  => "Kinder tun oft nicht:",
            'ans_a'     => "sich nützlich genug machen",
            'ans_b'     => "ihre Fantasie genug ausüben",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '31',
            'question'  => "Los niños a menudo no:",
            'ans_a'     => "hacerse lo suficientemente útiles",
            'ans_b'     => "ejercitar su fantasía lo suficiente",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '31',
            'question'  => "孩子们通常不会：",
            'ans_a'     => "让自己足够有用",
            'ans_b'     => "充分发挥他们的幻想",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '32',
            'question'  => "In making decisions do you feel more comfortable with:",
            'ans_a'     => "standards",
            'ans_b'     => "feelings",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '32',
            'question'  => "عند اتخاذ القرارات ، تشعر براحة أكبر مع:",
            'ans_a'     => "المعايير",
            'ans_b'     => "المشاعر",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '32',
            'question'  => "Lorsque vous prenez des décisions, vous sentez-vous plus à l'aise avec :",
            'ans_a'     => "normes",
            'ans_b'     => "sentiments",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '32',
            'question'  => "Karar verirken kendinizi daha rahat hissediyor musunuz:",
            'ans_a'     => "standartlar",
            'ans_b'     => "duygular",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '32',
            'question'  => "При принятии решений вам удобнее:",
            'ans_a'     => "стандарты",
            'ans_b'     => "чувства",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '32',
            'question'  => "Wenn Sie Entscheidungen treffen, fühlen Sie sich wohler mit:",
            'ans_a'     => "Standards",
            'ans_b'     => "Gefühle",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '32',
            'question'  => "Al tomar decisiones, se siente más cómodo con:",
            'ans_a'     => "normas",
            'ans_b'     => "sentimientos",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '32',
            'question'  => "在做出决定时，您是否对以下方面感到更自在：",
            'ans_a'     => "标准",
            'ans_b'     => "情怀",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '33',
            'question'  => "Are you more:",
            'ans_a'     => "firm than gentle",
            'ans_b'     => "gentle than firm",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '33',
            'question'  => "هل انت اكثر:",
            'ans_a'     => "حازم أكثر من اللطيف",
            'ans_b'     => "لطيف من الراسخ",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '33',
            'question'  => "Êtes-vous plutôt :",
            'ans_a'     => "ferme que doux",
            'ans_b'     => "doux que ferme",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '33',
            'question'  => "daha fazla mısın:",
            'ans_a'     => "nazikten daha sağlam",
            'ans_b'     => "firmadan daha nazik",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '33',
            'question'  => "Вы больше:",
            'ans_a'     => "твердый, чем нежный",
            'ans_b'     => "нежный, чем твердый",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '33',
            'question'  => "Bist du mehr:",
            'ans_a'     => "fest als sanft",
            'ans_b'     => "sanft als fest",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '33',
            'question'  => "Eres más:",
            'ans_a'     => "firme que suave",
            'ans_b'     => "suave que firme",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '33',
            'question'  => "你更吗：",
            'ans_a'     => "坚定胜于温柔",
            'ans_b'     => "温柔胜于坚定",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '34',
            'question'  => "Which is more admirable:",
            'ans_a'     => "the ability to organize and be methodical",
            'ans_b'     => "the ability to adapt and make do",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '34',
            'question'  => "أيهما أكثر إثارة للإعجاب:",
            'ans_a'     => "القدرة على التنظيم والمنهجية",
            'ans_b'     => "القدرة على التكيف والقيام",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '34',
            'question'  => "Ce qui est plus admirable :",
            'ans_a'     => "la capacité d'organiser et d'être méthodique",
            'ans_b'     => "la capacité de s'adapter et de se débrouiller",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '34',
            'question'  => "Was ist bewundernswerter:",
            'ans_a'     => "die Fähigkeit zu organisieren und methodisch zu sein",
            'ans_b'     => "die Fähigkeit, sich anzupassen und zu tun",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '34',
            'question'  => "Что достойнее восхищения:",
            'ans_a'     => "способность организовывать и быть методичными",
            'ans_b'     => "умение приспосабливаться и довольствоваться",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '34',
            'question'  => "Was ist bewundernswerter:",
            'ans_a'     => "die Fähigkeit zu organisieren und methodisch zu sein",
            'ans_b'     => "die Fähigkeit, sich anzupassen und zu tun",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '34',
            'question'  => "Que es más admirable:",
            'ans_a'     => "la capacidad de organizarse y ser metódico",
            'ans_b'     => "la capacidad de adaptarse y arreglárselas",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '34',
            'question'  => "更令人钦佩的是：",
            'ans_a'     => "组织能力和有条不紊",
            'ans_b'     => "适应和凑合的能力",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '35',
            'question'  => "Do you put more value on:",
            'ans_a'     => "infinite",
            'ans_b'     => "open-minded",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '35',
            'question'  => "هل تضع المزيد من القيمة على:",
            'ans_a'     => "بشكل لانهائي",
            'ans_b'     => "بعقل منفتح",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '35',
            'question'  => "Mettez-vous plus de valeur sur :",
            'ans_a'     => "infini",
            'ans_b'     => "ouvert d'esprit",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '35',
            'question'  => "Daha fazla değer veriyor musunuz:",
            'ans_a'     => "sonsuz",
            'ans_b'     => "açık görüşlü",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '35',
            'question'  => "Вы больше цените:",
            'ans_a'     => "бесконечный",
            'ans_b'     => "открытый",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '35',
            'question'  => "Legen Sie mehr Wert auf:",
            'ans_a'     => "unendlich",
            'ans_b'     => "offen",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '35',
            'question'  => "¿Le da más valor a:",
            'ans_a'     => "infinito",
            'ans_b'     => "mente abierta",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '35',
            'question'  => "您是否更看重：",
            'ans_a'     => "无限的",
            'ans_b'     => "思想开明的",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '36',
            'question'  => "Does new and non-routine interaction with others:",
            'ans_a'     => "stimulate and energize you",
            'ans_b'     => "tax your reserves",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '36',
            'question'  => "هل التفاعل الجديد وغير الروتيني مع الآخرين:",
            'ans_a'     => "يحفزك وينشطك",
            'ans_b'     => "يفرض عليك المزيد من القيود",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '36',
            'question'  => "Est-ce que des interactions nouvelles et inhabituelles avec les autres :",
            'ans_a'     => "vous stimuler et vous dynamiser",
            'ans_b'     => "taxer vos réserves",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '36',
            'question'  => "Başkalarıyla yeni ve rutin olmayan etkileşimde bulunur:",
            'ans_a'     => "sizi uyarmak ve enerji vermek",
            'ans_b'     => "rezervlerinizi vergilendirin",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '36',
            'question'  => "Новое и нестандартное взаимодействие с другими:",
            'ans_a'     => "стимулировать и заряжать вас энергией",
            'ans_b'     => "облагать налогом свои резервы",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '36',
            'question'  => "Führt neue und nicht routinemäßige Interaktionen mit anderen:",
            'ans_a'     => "anregen und energetisieren",
            'ans_b'     => "besteuere deine Rücklagen",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '36',
            'question'  => "Tiene interacción nueva y no rutinaria con otros:",
            'ans_a'     => "estimularte y energizarte",
            'ans_b'     => "grava tus reservas",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '36',
            'question'  => "是否与他人进行新的和非常规的互动：",
            'ans_a'     => "刺激和激励你",
            'ans_b'     => "对你的准备金征税",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '37',
            'question'  => "Are you more frequently:",
            'ans_a'     => "a practical sort of person",
            'ans_b'     => "a fanciful sort of person",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '37',
            'question'  => "هل تعتبر نفسك أكثر:",
            'ans_a'     => "نوع عملي من الأشخاص",
            'ans_b'     => "نوع خيالي من الأشخاص",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '37',
            'question'  => "Êtes-vous plus fréquemment :",
            'ans_a'     => "une sorte de personne pratique",
            'ans_b'     => "une sorte de personne fantaisiste",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '37',
            'question'  => "Daha sık mısınız:",
            'ans_a'     => "pratik bir insan türü",
            'ans_b'     => "hayalperest bir insan türü",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '37',
            'question'  => "Вы чаще:",
            'ans_a'     => "практичный человек",
            'ans_b'     => "причудливый человек",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '37',
            'question'  => "Sind Sie häufiger:",
            'ans_a'     => "ein praktischer Mensch",
            'ans_b'     => "ein fantasievoller Mensch",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '37',
            'question'  => "¿Eres más frecuente:",
            'ans_a'     => "una persona práctica",
            'ans_b'     => "un tipo de persona fantasiosa",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '37',
            'question'  => "您是否更频繁地：",
            'ans_a'     => "实事求是的人",
            'ans_b'     => "异想天开的人",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '38',
            'question'  => "Are you more likely to:",
            'ans_a'     => "see how others are useful",
            'ans_b'     => "see how others see",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '38',
            'question'  => "هل من المرجح أن:",
            'ans_a'     => "انظر كيف يكون الآخرون مفيدون",
            'ans_b'     => "انظر كيف يرى الآخرون",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '38',
            'question'  => "Êtes-vous plus susceptible de :",
            'ans_a'     => "voir comment les autres sont utiles",
            'ans_b'     => "voir comment les autres voient",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '38',
            'question'  => "Şunları yapma olasılığınız daha yüksek:",
            'ans_a'     => "diğerlerinin ne kadar yararlı olduğunu görün",
            'ans_b'     => "başkalarının nasıl gördüğünü görün",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '38',
            'question'  => "Вы с большей вероятностью:",
            'ans_a'     => "посмотреть, чем другие полезны",
            'ans_b'     => "посмотри, как видят другие",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '38',
            'question'  => "Ist es wahrscheinlicher, dass Sie:",
            'ans_a'     => "Sehen Sie, wie andere nützlich sind",
            'ans_b'     => "sehen, wie andere sehen",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '38',
            'question'  => "¿Es más probable que:",
            'ans_a'     => "ver como otros son útiles",
            'ans_b'     => "mira como ven los demás",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '38',
            'question'  => "您是否更有可能：",
            'ans_a'     => "看看其他人是如何有用的",
            'ans_b'     => "看看别人怎么看",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '39',
            'question'  => "Which is more satisfying:",
            'ans_a'     => "to discuss an issue thoroughly",
            'ans_b'     => "to arrive at agreement on an issue",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '39',
            'question'  => "أيهما أكثر إرضاءً:",
            'ans_a'     => "مناقشة قضية بدقة",
            'ans_b'     => "التوصل إلى اتفاق بشأن قضية",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '39',
            'question'  => "Ce qui est plus satisfaisant :",
            'ans_a'     => "discuter d'un problème en profondeur",
            'ans_b'     => "parvenir à un accord sur une question",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '39',
            'question'  => "Hangisi daha tatmin edici:",
            'ans_a'     => "bir konuyu iyice tartışmak",
            'ans_b'     => "bir konuda anlaşmaya varmak",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '39',
            'question'  => "Что более приятно:",
            'ans_a'     => "тщательно обсудить вопрос",
            'ans_b'     => "прийти к соглашению по вопросу",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '39',
            'question'  => "Was ist befriedigender:",
            'ans_a'     => "ein Thema gründlich besprechen",
            'ans_b'     => "eine Einigung über ein Thema erzielen",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '39',
            'question'  => "Cuál es más satisfactorio:",
            'ans_a'     => "discutir un tema a fondo",
            'ans_b'     => "llegar a un acuerdo sobre un tema",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '39',
            'question'  => "哪个更令人满意：",
            'ans_a'     => "彻底讨论一个问题",
            'ans_b'     => "就一个问题达成协议",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '40',
            'question'  => "Which rules you more:",
            'ans_a'     => "your head",
            'ans_b'     => "your heart",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '40',
            'question'  => "ما الذي يحكمك أكثر:",
            'ans_a'     => "رأسك",
            'ans_b'     => "قلبك",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '40',
            'question'  => "Ce qui vous gouverne le plus :",
            'ans_a'     => "ta tête",
            'ans_b'     => "votre cœur",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '40',
            'question'  => "Hangisi sizi daha çok yönetir:",
            'ans_a'     => "kafan",
            'ans_b'     => "kalbin",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '40',
            'question'  => "Какие правила у вас больше:",
            'ans_a'     => "твоя голова",
            'ans_b'     => "твое сердце",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '40',
            'question'  => "Was regiert dich mehr:",
            'ans_a'     => "dein Kopf",
            'ans_b'     => "dein Herz",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '40',
            'question'  => "¿Qué te gobierna más?",
            'ans_a'     => "tu cabeza",
            'ans_b'     => "tu corazón",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '40',
            'question'  => "哪个更能统治你：",
            'ans_a'     => "你的头",
            'ans_b'     => "你的心中",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'cn',
        ]);



       PersonalityTest::create([
            'ques_id'   => '41',
            'question'  => "Are you more comfortable with work that is:",
            'ans_a'     => "contracted",
            'ans_b'     => "done on a casual basis",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '41',
            'question'  => "هل أنت أكثر راحة في العمل الذي:",
            'ans_a'     => "يأخذ شكل التعاقد",
            'ans_b'     => "يتم إجراؤه على أساس غير رسمي",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '41',
            'question'  => "Êtes-vous plus à l'aise avec un travail qui est :",
            'ans_a'     => "contracté",
            'ans_b'     => "fait sur une base occasionnelle",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '41',
            'question'  => "Şu tür işlerde daha rahatsınız:",
            'ans_a'     => "sözleşmeli",
            'ans_b'     => "gündelik olarak yapılır",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '41',
            'question'  => "Вам удобнее работать:",
            'ans_a'     => "контракт",
            'ans_b'     => "сделано на случайной основе",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '41',
            'question'  => "Fühlen Sie sich wohler bei der Arbeit, die ist:",
            'ans_a'     => "Vertrag abgeschlossen",
            'ans_b'     => "nebenbei gemacht",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '41',
            'question'  => "¿Se siente más cómodo con un trabajo que es:",
            'ans_a'     => "contratado",
            'ans_b'     => "hecho de forma casual",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '41',
            'question'  => "您对以下工作是否更满意：",
            'ans_a'     => "签约",
            'ans_b'     => "随意完成",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '42',
            'question'  => "Do you tend to look for:",
            'ans_a'     => "the orderly",
            'ans_b'     => "whatever turns up",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '42',
            'question'  => "هل تميل للبحث عن:",
            'ans_a'     => "المنظم",
            'ans_b'     => "أيا كان ما يظهر",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '42',
            'question'  => "Avez-vous tendance à rechercher :",
            'ans_a'     => "l'ordonné",
            'ans_b'     => "quoi qu'il arrive",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '42',
            'question'  => "Şunları aramaya meyillisiniz:",
            'ans_a'     => "düzenli",
            'ans_b'     => "ne olursa olsun",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '42',
            'question'  => "Вы склонны искать:",
            'ans_a'     => "аккуратный",
            'ans_b'     => "что бы ни случилось",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '42',
            'question'  => "Suchen Sie eher nach:",
            'ans_a'     => "der Ordnungshüter",
            'ans_b'     => "was auch immer auftaucht",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '42',
            'question'  => "Suele buscar:",
            'ans_a'     => "el ordenado",
            'ans_b'     => "lo que sea que surja",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '42',
            'question'  => "您是否倾向于寻找：",
            'ans_a'     => "有秩序的",
            'ans_b'     => "无论出现什么",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '43',
            'question'  => "Do you prefer:",
            'ans_a'     => "many friends with brief contact",
            'ans_b'     => "a few friends with more lengthy contact",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '43',
            'question'  => "هل تفضل:",
            'ans_a'     => "العديد من الأصدقاء مع اتصال قصير",
            'ans_b'     => "عدد قليل من الأصدقاء الذين لديهم اتصال طويل الأمد",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '43',
            'question'  => "Préfères-tu:",
            'ans_a'     => "beaucoup d'amis avec un bref contact",
            'ans_b'     => "quelques amis avec un contact plus long",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '43',
            'question'  => "Tercih eder misin:",
            'ans_a'     => "kısa temaslı birçok arkadaş",
            'ans_b'     => "daha uzun süreli temasa sahip birkaç arkadaş",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '43',
            'question'  => "Ты предпочитаешь:",
            'ans_a'     => "много друзей с коротким контактом",
            'ans_b'     => "несколько друзей с более длительным контактом",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '43',
            'question'  => "Bevorzugen Sie:",
            'ans_a'     => "viele Freunde mit kurzem Kontakt",
            'ans_b'     => "ein paar Freunde mit längerem Kontakt",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '43',
            'question'  => "Prefieres:",
            'ans_a'     => "muchos amigos con breve contacto",
            'ans_b'     => "algunos amigos con contactos más prolongados",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '43',
            'question'  => "你比较喜欢哪个：",
            'ans_a'     => "许多朋友短暂接触",
            'ans_b'     => "几个联系比较久的朋友",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '44',
            'question'  => "Do you go more by:",
            'ans_a'     => "facts",
            'ans_b'     => "principles",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '44',
            'question'  => "هل تذهب أكثر من خلال:",
            'ans_a'     => "حقائق",
            'ans_b'     => "مبادئ",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '44',
            'question'  => "Allez-vous plus par :",
            'ans_a'     => "les faits",
            'ans_b'     => "des principes",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '44',
            'question'  => "Daha fazla mı gidiyorsun:",
            'ans_a'     => "gerçekler",
            'ans_b'     => "prensipler",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '44',
            'question'  => "Вы больше идете по:",
            'ans_a'     => "факты",
            'ans_b'     => "принципы",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '44',
            'question'  => "Gehst du mehr nach:",
            'ans_a'     => "Fakten",
            'ans_b'     => "Prinzipien",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '44',
            'question'  => "Vas más por:",
            'ans_a'     => "hechos",
            'ans_b'     => "principios",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '44',
            'question'  => "您是否通过以下方式进行了更多操作：",
            'ans_a'     => "事实",
            'ans_b'     => "原则",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '45',
            'question'  => "Are you more interested in:",
            'ans_a'     => "production and distribution",
            'ans_b'     => "design and research",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '45',
            'question'  => "هل أنت مهتم أكثر بـ:",
            'ans_a'     => "الإنتاج والتوزيع",
            'ans_b'     => "التصميم والبحث",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '45',
            'question'  => "Êtes-vous plus intéressé par :",
            'ans_a'     => "fabrication et diffusion",
            'ans_b'     => "conception et recherche",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '45',
            'question'  => "Daha çok ilgileniyor musunuz:",
            'ans_a'     => "üretim ve dağıtım",
            'ans_b'     => "tasarım ve araştırma",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '45',
            'question'  => "Вас больше интересуют:",
            'ans_a'     => "производство и распространение",
            'ans_b'     => "дизайн и исследования",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '45',
            'question'  => "Interessieren Sie sich mehr für:",
            'ans_a'     => "Produktion und Vertrieb",
            'ans_b'     => "Design und Forschung",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '45',
            'question'  => "¿Está más interesado en:",
            'ans_a'     => "producción y distribución",
            'ans_b'     => "diseño e investigación",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '45',
            'question'  => "您是否更感兴趣：",
            'ans_a'     => "生产和分销",
            'ans_b'     => "设计和研究",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '46',
            'question'  => "Which is more of a compliment:",
            'ans_a'     => "There is a very logical person",
            'ans_b'     => "There is a very sentimental person",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '46',
            'question'  => "من وهو الأكثر مجاملة:",
            'ans_a'     => "هناك شخص منطقي جدا",
            'ans_b'     => "هناك شخص عاطفي جدا",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '46',
            'question'  => "Ce qui est plutôt un compliment :",
            'ans_a'     => "Il y a une personne très logique",
            'ans_b'     => "Il y a une personne très sentimentale",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '46',
            'question'  => "Hangisi daha çok iltifat:",
            'ans_a'     => "çok mantıklı bir insan var",
            'ans_b'     => "çok duygusal biri var",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '46',
            'question'  => "Что скорее комплимент:",
            'ans_a'     => "Есть очень логичный человек",
            'ans_b'     => "Есть очень сентиментальный человек",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '46',
            'question'  => "Was eher ein Kompliment ist:",
            'ans_a'     => "Es gibt eine sehr logische Person",
            'ans_b'     => "Es gibt eine sehr sentimentale Person",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '46',
            'question'  => "Que es más un cumplido:",
            'ans_a'     => "Hay una persona muy lógica",
            'ans_b'     => "Hay una persona muy sentimental",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '46',
            'question'  => "这更像是一种赞美：",
            'ans_a'     => "有一个很有逻辑的人",
            'ans_b'     => "有一个很感性的人",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '47',
            'question'  => "Do you value in yourself more that you are:",
            'ans_a'     => "unwavering",
            'ans_b'     => "devoted",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '47',
            'question'  => "هل تقدر نفسك أكثر من أنك:",
            'ans_a'     => "لا يتزعزع",
            'ans_b'     => "مخلص",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '47',
            'question'  => "Est-ce que vous valorisez en vous-même plus que vous n'êtes :",
            'ans_a'     => "inébranlable",
            'ans_b'     => "dévoué",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '47',
            'question'  => "Kendinize olduğunuzdan daha çok değer veriyor musunuz:",
            'ans_a'     => "sarsılmaz",
            'ans_b'     => "adanmış",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '47',
            'question'  => "Вы цените в себе больше, чем вы:",
            'ans_a'     => "непоколебимый",
            'ans_b'     => "преданный",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '47',
            'question'  => "Schätzen Sie an sich selbst mehr als Sie sind:",
            'ans_a'     => "unerschütterlich",
            'ans_b'     => "hingebungsvoll",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '47',
            'question'  => "¿Valoras más en ti mismo de lo que eres?",
            'ans_a'     => "inquebrantable",
            'ans_b'     => "devoto",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '47',
            'question'  => "你是否更看重自己：",
            'ans_a'     => "坚定不移",
            'ans_b'     => "专心致志",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '48',
            'question'  => "Do you more often prefer the",
            'ans_a'     => "final and unalterable statement",
            'ans_b'     => "tentative and preliminary statement",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '48',
            'question'  => "هل تفضل في كثير من الأحيان",
            'ans_a'     => "بيان نهائي وغير قابل للتغيير",
            'ans_b'     => "بيان مبدئي وأولي",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '48',
            'question'  => "Préférez-vous plus souvent le",
            'ans_a'     => "déclaration finale et inaltérable",
            'ans_b'     => "déclaration provisoire et préliminaire",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '48',
            'question'  => "daha sık mı tercih edersin",
            'ans_a'     => "nihai ve değiştirilemez ifade",
            'ans_b'     => "geçici ve ön açıklama",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '48',
            'question'  => "Вы чаще предпочитаете",
            'ans_a'     => "окончательное и неизменное заявление",
            'ans_b'     => "предварительное и предварительное заявление",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '48',
            'question'  => "Bevorzugen Sie häufiger die",
            'ans_a'     => "endgültige und unveränderliche Aussage",
            'ans_b'     => "vorläufige und vorläufige Stellungnahme",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '48',
            'question'  => "¿Prefieres más a menudo el",
            'ans_a'     => "declaración final e inalterable",
            'ans_b'     => "declaración tentativa y preliminar",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '48',
            'question'  => "你是否更喜欢",
            'ans_a'     => "最后和不可更改的声明",
            'ans_b'     => "暂定和初步声明",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '49',
            'question'  => "Are you more comfortable:",
            'ans_a'     => "after a decision",
            'ans_b'     => "before a decision",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '49',
            'question'  => "هل أنت أكثر راحة:",
            'ans_a'     => "بعد اتخاذ قرار",
            'ans_b'     => "قبل اتخاذ القرار",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '49',
            'question'  => "Êtes-vous plus à l'aise :",
            'ans_a'     => "après une décision",
            'ans_b'     => "avant une décision",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '49',
            'question'  => "Daha rahat mısın:",
            'ans_a'     => "bir karardan sonra",
            'ans_b'     => "bir karardan önce",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '49',
            'question'  => "Вам удобнее:",
            'ans_a'     => "после решения",
            'ans_b'     => "перед решением",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '49',
            'question'  => "Sind Sie bequemer:",
            'ans_a'     => "nach einer entscheidung",
            'ans_b'     => "vor einer Entscheidung",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '49',
            'question'  => "¿Estás más cómodo?",
            'ans_a'     => "después de una decisión",
            'ans_b'     => "antes de una decisión",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '49',
            'question'  => "你更舒服吗：",
            'ans_a'     => "决定后",
            'ans_b'     => "在做出决定之前",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '50',
            'question'  => "Do you:",
            'ans_a'     => "speak easily and at length with strangers",
            'ans_b'     => "find little to say to strangers",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '50',
            'question'  => "هل أنت :",
            'ans_a'     => "التحدث بسهولة واستفاضة مع الغرباء",
            'ans_b'     => "تجد القليل لتقوله للغرباء",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '50',
            'question'  => "Est-ce que tu:",
            'ans_a'     => "parler facilement et longuement avec des inconnus",
            'ans_b'     => "trouver peu à dire aux étrangers",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '50',
            'question'  => "NS:",
            'ans_a'     => "yabancılarla kolayca ve uzun uzun konuşun",
            'ans_b'     => "yabancılara söyleyecek çok az şey bul",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '50',
            'question'  => "Ты:",
            'ans_a'     => "говорить легко и подробно с незнакомцами",
            'ans_b'     => "найти мало что сказать незнакомцам",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '50',
            'question'  => "Tust du:",
            'ans_a'     => "leicht und ausführlich mit Fremden sprechen",
            'ans_b'     => "finde wenig zu Fremden zu sagen",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '50',
            'question'  => "Vos si:",
            'ans_a'     => "hablar con facilidad y amplitud con extraños",
            'ans_b'     => "encontrar poco que decir a los extraños",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '50',
            'question'  => "你：",
            'ans_a'     => "与陌生人轻松而详细地交谈",
            'ans_b'     => "对陌生人无话可说",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'cn',
        ]);



       PersonalityTest::create([
            'ques_id'   => '51',
            'question'  => "Are you more likely to trust your:",
            'ans_a'     => "experience",
            'ans_b'     => "hunch",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '51',
            'question'  => "هل من المرجح أن تثق في:",
            'ans_a'     => "خبرة",
            'ans_b'     => "الحدس",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '51',
            'question'  => "Êtes-vous plus susceptible de faire confiance à votre :",
            'ans_a'     => "vivre",
            'ans_b'     => "pressentiment",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '51',
            'question'  => "Şunlara güvenme olasılığınız daha mı yüksek:",
            'ans_a'     => "tecrübe etmek",
            'ans_b'     => "kambur",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '51',
            'question'  => "Вы больше доверяете своему:",
            'ans_a'     => "опыт",
            'ans_b'     => "догадываться",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '51',
            'question'  => "Vertrauen Sie eher Ihrem:",
            'ans_a'     => "Erfahrung",
            'ans_b'     => "Ahnung",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '51',
            'question'  => "¿Es más probable que confíe en su:",
            'ans_a'     => "experiencia",
            'ans_b'     => "corazonada",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '51',
            'question'  => "您是否更有可能信任您的：",
            'ans_a'     => "经验",
            'ans_b'     => "直觉",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '52',
            'question'  => "Do you feel:",
            'ans_a'     => "more practical than ingenious",
            'ans_b'     => "more ingenious than practical",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '52',
            'question'  => "هل تشعر:",
            'ans_a'     => "عمليا أكثر من عبقري",
            'ans_b'     => "أكثر عبقريًا من العملي",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '52',
            'question'  => "Vous sentez-vous :",
            'ans_a'     => "plus pratique qu'ingénieux",
            'ans_b'     => "plus ingénieux que pratique",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '52',
            'question'  => "Hissediyor musun:",
            'ans_a'     => "dahiyaneden daha pratik",
            'ans_b'     => "pratikten daha yerli",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '52',
            'question'  => "Ты чувствуешь:",
            'ans_a'     => "практичнее, чем изобретательнее",
            'ans_b'     => "более коренной, чем практичный",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '52',
            'question'  => "Fühlen Sie:",
            'ans_a'     => "praktischer als genial",
            'ans_b'     => "mehr einheimisch als praktisch",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '52',
            'question'  => "Sientes:",
            'ans_a'     => "más práctico que ingenioso",
            'ans_b'     => "más indígena que práctico",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '52',
            'question'  => "你感觉：",
            'ans_a'     => "实用多于巧妙",
            'ans_b'     => "比实际更本土",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '53',
            'question'  => "Which person is more to be complimented – one of:",
            'ans_a'     => "clear reason",
            'ans_b'     => "strong feeling",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '53',
            'question'  => "أي شخص يستحق الإطراء - أحد:",
            'ans_a'     => "سبب واضح",
            'ans_b'     => "احساس قوي",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '53',
            'question'  => "Quelle personne mérite le plus d'être complimentée - l'une des suivantes :",
            'ans_a'     => "raison claire",
            'ans_b'     => "Sentiment fort",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '53',
            'question'  => "Hangi kişiye daha çok iltifat edilir – şunlardan biri:",
            'ans_a'     => "açık sebep",
            'ans_b'     => "güçlü his",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '53',
            'question'  => "Кого из людей следует больше хвалить - одного из:",
            'ans_a'     => "ясная причина",
            'ans_b'     => "сильное чувство",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '53',
            'question'  => "Welche Person ist mehr zu loben – eine von:",
            'ans_a'     => "klarer Grund",
            'ans_b'     => "starkes Gefühl",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '53',
            'question'  => "¿A qué persona hay que felicitar más? Uno de los siguientes:",
            'ans_a'     => "razón clara",
            'ans_b'     => "fuerte sentimiento",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '53',
            'question'  => "哪个人更值得称赞——以下之一：",
            'ans_a'     => "明确的理由",
            'ans_b'     => "强烈的感觉",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '54',
            'question'  => "Are you inclined more to be:",
            'ans_a'     => "fair-minded",
            'ans_b'     => "sympathetic",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '54',
            'question'  => "هل تميل أكثر لتكون:",
            'ans_a'     => "عادل",
            'ans_b'     => "ودي",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '54',
            'question'  => "Êtes-vous plus enclin à être :",
            'ans_a'     => "impartial",
            'ans_b'     => "sympathique",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '54',
            'question'  => "Daha çok olmaya meyilli misiniz:",
            'ans_a'     => "adil fikirli",
            'ans_b'     => "sempatik",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '54',
            'question'  => "Вы больше склонны быть:",
            'ans_a'     => "честный",
            'ans_b'     => "симпатичный",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '54',
            'question'  => "Neigst du eher zu:",
            'ans_a'     => "aufrichtig",
            'ans_b'     => "sympathisch",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '54',
            'question'  => "¿Está más inclinado a ser:",
            'ans_a'     => "imparcial",
            'ans_b'     => "simpático",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '54',
            'question'  => "你是否更倾向于：",
            'ans_a'     => "公正的",
            'ans_b'     => "有同情心的",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '55',
            'question'  => "Is it preferable mostly to:",
            'ans_a'     => "make sure things are arranged",
            'ans_b'     => "just let things happen",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '55',
            'question'  => "هل يفضل في الغالب:",
            'ans_a'     => "تأكد من ترتيب الأشياء",
            'ans_b'     => "فقط دع الأشياء تحدث",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '55',
            'question'  => "Est-il préférable principalement de :",
            'ans_a'     => "s'assurer que les choses sont arrangées",
            'ans_b'     => "laisse les choses arriver",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '55',
            'question'  => "Çoğunlukla aşağıdakiler için tercih edilir:",
            'ans_a'     => "işlerin düzenlendiğinden emin ol",
            'ans_b'     => "sadece olayların olmasına izin ver",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '55',
            'question'  => "В основном ли предпочтительнее:",
            'ans_a'     => "убедитесь, что все устроено",
            'ans_b'     => "просто позволь всему случиться",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '55',
            'question'  => "Ist es meistens vorzuziehen:",
            'ans_a'     => "Sorgen Sie dafür, dass die Dinge arrangiert sind",
            'ans_b'     => "lass die Dinge einfach passieren",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '55',
            'question'  => "¿Es preferible principalmente a:",
            'ans_a'     => "asegúrese de que las cosas estén arregladas",
            'ans_b'     => "solo deja que las cosas sucedan",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '55',
            'question'  => "是否更可取于：",
            'ans_a'     => "确保事情安排妥当",
            'ans_b'     => "就让事情发生吧",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '56',
            'question'  => "In relationships should most things be:",
            'ans_a'     => "re-negotiable",
            'ans_b'     => "random and circumstantial",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '56',
            'question'  => "في العلاقات يجب أن تكون معظم الأشياء:",
            'ans_a'     => "قابلة لإعادة التفاوض",
            'ans_b'     => "عشوائية وظرفية",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '56',
            'question'  => "Dans les relations, la plupart des choses devraient être :",
            'ans_a'     => "renégociable",
            'ans_b'     => "aléatoire et circonstanciel",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '56',
            'question'  => "İlişkilerde çoğu şey şöyle olmalıdır:",
            'ans_a'     => "yeniden pazarlık yapılabilir",
            'ans_b'     => "rastgele ve koşullu",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '56',
            'question'  => "В отношениях должно быть больше всего:",
            'ans_a'     => "оборотный",
            'ans_b'     => "случайный и косвенный",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '56',
            'question'  => "In Beziehungen sollten die meisten Dinge sein:",
            'ans_a'     => "nachverhandelbar",
            'ans_b'     => "zufällig und umständlich",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '56',
            'question'  => "En las relaciones, la mayoría de las cosas deberían ser:",
            'ans_a'     => "renegociable",
            'ans_b'     => "aleatorio y circunstancial",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '56',
            'question'  => "在人际关系中，大多数事情应该是：",
            'ans_a'     => "可重新协商",
            'ans_b'     => "随机的和间接的",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '57',
            'question'  => "When the phone rings do you:",
            'ans_a'     => "hasten to get to it first",
            'ans_b'     => "hope someone else will answer",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '57',
            'question'  => "عندما يرن الهاتف هل:",
            'ans_a'     => "أسرع للوصول إليه أولاً",
            'ans_b'     => "أتمنى أن يجيب شخص آخر",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '57',
            'question'  => "Lorsque le téléphone sonne :",
            'ans_a'     => "hâte-toi d'y arriver en premier",
            'ans_b'     => "j'espère que quelqu'un d'autre répondra",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '57',
            'question'  => "Telefon çaldığında:",
            'ans_a'     => "önce ona ulaşmak için acele et",
            'ans_b'     => "umarım başka biri cevap verir",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '57',
            'question'  => "Когда звонит телефон, вы:",
            'ans_a'     => "спешите добраться до него первым",
            'ans_b'     => "надеюсь, что кто-то еще ответит",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '57',
            'question'  => "Wenn das Telefon klingelt, tun Sie:",
            'ans_a'     => "beeile dich, zuerst dazu zu kommen",
            'ans_b'     => "hoffe jemand anderes antwortet",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '57',
            'question'  => "Cuando suena el teléfono:",
            'ans_a'     => "apresúrate a llegar primero",
            'ans_b'     => "espero que alguien más responda",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '57',
            'question'  => "当电话响起时，您是否：",
            'ans_a'     => "赶快行动起来",
            'ans_b'     => "希望其他人能回答",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '58',
            'question'  => "Do you prize more in yourself:",
            'ans_a'     => "a strong sense of reality",
            'ans_b'     => "a vivid imagination",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '58',
            'question'  => "هل تربح أكثر في نفسك:",
            'ans_a'     => "شعور قوي بالواقع",
            'ans_b'     => "خيال حي",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '58',
            'question'  => "Est-ce que vous accordez plus d'importance à vous-même :",
            'ans_a'     => "un sens aigu de la réalité",
            'ans_b'     => "une imagination débordante",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '58',
            'question'  => "Kendinize daha çok değer veriyor musunuz:",
            'ans_a'     => "güçlü bir gerçeklik duygusu",
            'ans_b'     => "canlı bir hayal gücü",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '58',
            'question'  => "Вы больше цените в себе:",
            'ans_a'     => "сильное чувство реальности",
            'ans_b'     => "яркое воображение",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '58',
            'question'  => "Schätzen Sie mehr an sich selbst:",
            'ans_a'     => "ein starker Realitätssinn",
            'ans_b'     => "eine lebhafte Vorstellung",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '58',
            'question'  => "¿Valoras más en ti mismo?",
            'ans_a'     => "un fuerte sentido de la realidad",
            'ans_b'     => "una vívida imaginación",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '58',
            'question'  => "你是否更看重自己：",
            'ans_a'     => "强烈的现实感",
            'ans_b'     => "生动的想象",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '59',
            'question'  => "Are you drawn more to:",
            'ans_a'     => "fundamentals",
            'ans_b'     => "overtones",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '59',
            'question'  => "هل تنجذب أكثر إلى:",
            'ans_a'     => "الأساسيات",
            'ans_b'     => "الإيحاءات",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '59',
            'question'  => "Êtes-vous plus attiré par :",
            'ans_a'     => "fondamentaux",
            'ans_b'     => "harmoniques",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '59',
            'question'  => "Daha çok ilginizi çekiyor mu:",
            'ans_a'     => "temeller",
            'ans_b'     => "imalar",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '59',
            'question'  => "Вас больше привлекают:",
            'ans_a'     => "основы",
            'ans_b'     => "обертоны",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '59',
            'question'  => "Zieht es dich mehr an:",
            'ans_a'     => "Grundlagen",
            'ans_b'     => "Obertöne",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '59',
            'question'  => "¿Te atrae más:",
            'ans_a'     => "fundamentos",
            'ans_b'     => "matices",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '59',
            'question'  => "你是否更喜欢：",
            'ans_a'     => "基本面",
            'ans_b'     => "泛音",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '60',
            'question'  => "Which seems the greater error: ",
            'ans_a'     => "to be too passionate",
            'ans_b'     => "to be too objective",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '60',
            'question'  => "الذي يبدو الخطأ الأكبر:",
            'ans_a'     => "أن تكون شغوفًا جدًا",
            'ans_b'     => "أن تكون موضوعيًا جدًا",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '60',
            'question'  => "Ce qui semble la plus grande erreur :",
            'ans_a'     => "être trop passionné",
            'ans_b'     => "être trop objectif",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '60',
            'question'  => "Hangisi daha büyük hata gibi görünüyor:",
            'ans_a'     => "çok tutkulu olmak",
            'ans_b'     => "fazla objektif olmak",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '60',
            'question'  => "Что кажется большей ошибкой:",
            'ans_a'     => "быть слишком страстным",
            'ans_b'     => "быть слишком объективным",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '60',
            'question'  => "Was scheint der größere Fehler zu sein:",
            'ans_a'     => "zu leidenschaftlich sein",
            'ans_b'     => "zu objektiv sein",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '60',
            'question'  => "Cuál parece el mayor error:",
            'ans_a'     => "ser demasiado apasionado",
            'ans_b'     => "ser demasiado objetivo",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '60',
            'question'  => "这似乎是更大的错误：",
            'ans_a'     => "过于热情",
            'ans_b'     => "过于客观",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'cn',
        ]);



       PersonalityTest::create([
            'ques_id'   => '61',
            'question'  => "Do you see yourself as basically:",
            'ans_a'     => "hard-headed",
            'ans_b'     => "soft-hearted",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '61',
            'question'  => "هل ترى نفسك بشكل أساسي:",
            'ans_a'     => "متشدد",
            'ans_b'     => "ناعم القلب",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '61',
            'question'  => "Vous considérez-vous comme fondamentalement :",
            'ans_a'     => "têtu",
            'ans_b'     => "au cœur tendre",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '61',
            'question'  => "Kendinizi temelde şu şekilde mi görüyorsunuz:",
            'ans_a'     => "Kalın kafalı",
            'ans_b'     => "yumuşak kalpli",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '61',
            'question'  => "Вы видите себя в основном:",
            'ans_a'     => "упрямый",
            'ans_b'     => "мягкосердечный",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '61',
            'question'  => "Sehen Sie sich grundsätzlich als:",
            'ans_a'     => "dickköpfig",
            'ans_b'     => "weichherzig",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '61',
            'question'  => "¿Te ves a ti mismo básicamente como:",
            'ans_a'     => "cabeza dura",
            'ans_b'     => "corazón blando",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '61',
            'question'  => "你认为自己基本上是：",
            'ans_a'     => "固执的",
            'ans_b'     => "好心的",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '62',
            'question'  => "Which situation appeals to you more:",
            'ans_a'     => "the structured and scheduled",
            'ans_b'     => "the unstructured and unscheduled",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '62',
            'question'  => "أي موقف يروق لك أكثر:",
            'ans_a'     => "منظم و مجدول",
            'ans_b'     => "غير المهيكل وغير المخطط له",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '62',
            'question'  => "Quelle situation vous attire le plus :",
            'ans_a'     => "le structuré et programmé",
            'ans_b'     => "le non structuré et non programmé",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '62',
            'question'  => "Hangi durum size daha çok hitap ediyor:",
            'ans_a'     => "yapılandırılmış ve planlanmış",
            'ans_b'     => "yapılandırılmamış ve programlanmamış",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '62',
            'question'  => "Какая ситуация вам больше нравится:",
            'ans_a'     => "структурированные и запланированные",
            'ans_b'     => "неструктурированные и внеплановые",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '62',
            'question'  => "Welche Situation reizt Sie mehr:",
            'ans_a'     => "die strukturierte und geplante",
            'ans_b'     => "das unstrukturierte und ungeplante",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '62',
            'question'  => "¿Qué situación te atrae más?",
            'ans_a'     => "el estructurado y programado",
            'ans_b'     => "lo no estructurado y no programado",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '62',
            'question'  => "哪种情况更吸引您：",
            'ans_a'     => "结构化和预定的",
            'ans_b'     => "非结构化和计划外的",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '63',
            'question'  => "Are you a person that is more:",
            'ans_a'     => "routinized than whimsical",
            'ans_b'     => "whimsical than routinized",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '63',
            'question'  => "هل انت شخص اكثر:",
            'ans_a'     => "روتينية من غريب الاطوار",
            'ans_b'     => "غريب الاطوار من روتينية",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '63',
            'question'  => "Êtes-vous une personne plus :",
            'ans_a'     => "routinier que fantaisiste",
            'ans_b'     => "fantaisiste que routinier",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '63',
            'question'  => "Daha fazla kişi misiniz:",
            'ans_a'     => "kaprisli olmaktan çok rutin",
            'ans_b'     => "rutinden daha tuhaf",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '63',
            'question'  => "Вы человек, который больше:",
            'ans_a'     => "рутинный, чем прихотливый",
            'ans_b'     => "причудливый, чем рутинный",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '63',
            'question'  => "Bist du ein Mensch, der mehr ist:",
            'ans_a'     => "routiniert als skurril",
            'ans_b'     => "skurril als routinemäßig",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '63',
            'question'  => "¿Eres una persona que es más:",
            'ans_a'     => "rutinario que caprichoso",
            'ans_b'     => "caprichoso que rutinario",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '63',
            'question'  => "你是一个更多的人吗：",
            'ans_a'     => "常规而不是异想天开",
            'ans_b'     => "异想天开比常规化",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '64',
            'question'  => "Are you more inclined to be:",
            'ans_a'     => "easy to approach",
            'ans_b'     => "somewhat reserved",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '64',
            'question'  => "هل أنت أكثر ميلا إلى أن تكون:",
            'ans_a'     => "سهل الاقتراب",
            'ans_b'     => "محجوزا إلى حد ما",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '64',
            'question'  => "Êtes-vous plus enclin à être :",
            'ans_a'     => "facile d'approche",
            'ans_b'     => "Quelque peu réservé",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '64',
            'question'  => "Daha fazla eğilimli misiniz:",
            'ans_a'     => "yaklaşması kolay",
            'ans_b'     => "bir şekilde ayırtılmış",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '64',
            'question'  => "Вы более склонны:",
            'ans_a'     => "легко подойти",
            'ans_b'     => "несколько сдержанный",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '64',
            'question'  => "Sind Sie eher geneigt zu sein:",
            'ans_a'     => "leicht zu nähern",
            'ans_b'     => "etwas reserviert",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '64',
            'question'  => "¿Está más inclinado a ser:",
            'ans_a'     => "fácil de acercarse",
            'ans_b'     => "algo reservado",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '64',
            'question'  => "你是否更倾向于：",
            'ans_a'     => "容易接近",
            'ans_b'     => "有点保留",
            'val_a'     => "E",
            'val_b'     => "I",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '65',
            'question'  => "In writings do you prefer:",
            'ans_a'     => "the more literal",
            'ans_b'     => "the more figurative",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '65',
            'question'  => "في كتابات هل تفضل:",
            'ans_a'     => "الأكثر حرفية",
            'ans_b'     => "الأكثر تجسيدا",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '65',
            'question'  => "Dans les écrits, préférez-vous :",
            'ans_a'     => "le plus littéral",
            'ans_b'     => "le plus figuratif",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '65',
            'question'  => "Yazılarda tercih ettiğiniz:",
            'ans_a'     => "daha gerçek",
            'ans_b'     => "daha figüratif",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '65',
            'question'  => "В произведениях вы предпочитаете:",
            'ans_a'     => "более буквальный",
            'ans_b'     => "более образный",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '65',
            'question'  => "In Schriftform bevorzugen Sie:",
            'ans_a'     => "desto wörtlicher",
            'ans_b'     => "desto figurativer",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '65',
            'question'  => "En escritos prefieres:",
            'ans_a'     => "el más literal",
            'ans_b'     => "el más figurativo",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '65',
            'question'  => "在写作中，您更喜欢：",
            'ans_a'     => "越文字",
            'ans_b'     => "越形象",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '66',
            'question'  => "Is it harder for you to:",
            'ans_a'     => "identify with others",
            'ans_b'     => "utilize others",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '66',
            'question'  => "هل يصعب عليك:",
            'ans_a'     => "التعرف على الآخرين",
            'ans_b'     => "الاستفادة من الآخرين",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '66',
            'question'  => "Est-ce plus difficile pour vous de :",
            'ans_a'     => "s'identifier aux autres",
            'ans_b'     => "utiliser les autres",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '66',
            'question'  => "Senin için daha mı zor:",
            'ans_a'     => "başkalarıyla özdeşleşmek",
            'ans_b'     => "başkalarını kullanmak",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '66',
            'question'  => "Вам труднее:",
            'ans_a'     => "отождествлять себя с другими",
            'ans_b'     => "использовать других",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '66',
            'question'  => "Ist es schwieriger für Sie:",
            'ans_a'     => "sich mit anderen identifizieren",
            'ans_b'     => "andere nutzen",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '66',
            'question'  => "¿Le resulta más difícil:",
            'ans_a'     => "identificarse con los demás",
            'ans_b'     => "utilizar a otros",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '66',
            'question'  => "您是否更难：",
            'ans_a'     => "认同他人",
            'ans_b'     => "利用他人",
            'val_a'     => "S",
            'val_b'     => "N",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '67',
            'question'  => "Which do you wish more for yourself:",
            'ans_a'     => "clarity of reason",
            'ans_b'     => "strength of compassion",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '67',
            'question'  => "أيهما تتمنى أكثر لنفسك:",
            'ans_a'     => "وضوح العقل",
            'ans_b'     => "قوة التعاطف",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '67',
            'question'  => "Lequel souhaitez-vous le plus pour vous-même :",
            'ans_a'     => "clarté de la raison",
            'ans_b'     => "force de compassion",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '67',
            'question'  => "Kendiniz için hangisini daha çok istersiniz:",
            'ans_a'     => "sebebin açıklığı",
            'ans_b'     => "şefkatin gücü",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '67',
            'question'  => "Что вы желаете себе больше:",
            'ans_a'     => "ясность разума",
            'ans_b'     => "сила сострадания",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '67',
            'question'  => "Was wünschst du dir mehr:",
            'ans_a'     => "Klarheit der Vernunft",
            'ans_b'     => "Stärke des Mitgefühls",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '67',
            'question'  => "¿Qué deseas más para ti?",
            'ans_a'     => "claridad de razon",
            'ans_b'     => "fuerza de la compasión",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '67',
            'question'  => "你更希望自己哪一个：",
            'ans_a'     => "道理明晰",
            'ans_b'     => "同情的力量",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '68',
            'question'  => "Which is the greater fault:",
            'ans_a'     => "being indiscriminate",
            'ans_b'     => "being critical",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '68',
            'question'  => "ما هو الخطأ الأكبر:",
            'ans_a'     => "كونها عشوائية",
            'ans_b'     => "كونها حاسمة",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '68',
            'question'  => "Quelle est la plus grande faute :",
            'ans_a'     => "être aveugle",
            'ans_b'     => "être critique",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '68',
            'question'  => "Hangisi daha büyük hata:",
            'ans_a'     => "ayrım gözetmeksizin",
            'ans_b'     => "kritik olmak",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '68',
            'question'  => "Какая большая ошибка:",
            'ans_a'     => "быть неизбирательным",
            'ans_b'     => "быть критичным",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '68',
            'question'  => "Was ist der größere Fehler:",
            'ans_a'     => "wahllos sein",
            'ans_b'     => "kritisch sein",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '68',
            'question'  => "Cuál es la mayor falla:",
            'ans_a'     => "ser indiscriminado",
            'ans_b'     => "siendo crítico",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '68',
            'question'  => "哪个是更大的错误：",
            'ans_a'     => "不分青红皂白",
            'ans_b'     => "批评",
            'val_a'     => "T",
            'val_b'     => "F",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '69',
            'question'  => "Do you prefer the:",
            'ans_a'     => "planned event",
            'ans_b'     => "unplanned event",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '69',
            'question'  => "هل تفضل:",
            'ans_a'     => "حدث مخطط",
            'ans_b'     => "حدث غير مخطط له",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '69',
            'question'  => "Vous préférez :",
            'ans_a'     => "événement prévu",
            'ans_b'     => "événement imprévu",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '69',
            'question'  => "Şunları tercih edersin:",
            'ans_a'     => "planlanmış olay",
            'ans_b'     => "planlanmamış olay",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '69',
            'question'  => "Вы предпочитаете:",
            'ans_a'     => "запланированное мероприятие",
            'ans_b'     => "незапланированное событие",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '69',
            'question'  => "Bevorzugen Sie:",
            'ans_a'     => "geplante Veranstaltung",
            'ans_b'     => "ungeplantes Ereignis",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '69',
            'question'  => "¿Prefieres el:",
            'ans_a'     => "evento planeado",
            'ans_b'     => "evento no planeado",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '69',
            'question'  => "你更喜欢：",
            'ans_a'     => "计划中的事件",
            'ans_b'     => "意外事件",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'cn',
        ]);

       PersonalityTest::create([
            'ques_id'   => '70',
            'question'  => "Do you tend to be more:",
            'ans_a'     => "deliberate than spontaneous",
            'ans_b'     => "spontaneous than deliberate",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'en',
        ]);
        PersonalityTest::create([
            'ques_id'   => '70',
            'question'  => "هل تميل إلى أن تكون أكثر:",
            'ans_a'     => "متعمد أكثر من تلقائية",
            'ans_b'     => "عفوية مما هو متعمد",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ar',
        ]);
       PersonalityTest::create([
            'ques_id'   => '70',
            'question'  => "Avez-vous tendance à être plus :",
            'ans_a'     => "délibéré que spontané",
            'ans_b'     => "spontané que délibéré",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'fr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '70',
            'question'  => "Daha fazla olma eğiliminde misiniz:",
            'ans_a'     => "spontaneden daha kasıtlı",
            'ans_b'     => "kasıtlı olmaktan ziyade kendiliğinden",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'tr',
        ]);
       PersonalityTest::create([
            'ques_id'   => '70',
            'question'  => "Вы склонны к большему:",
            'ans_a'     => "преднамеренный, чем спонтанный",
            'ans_b'     => "спонтанный, чем преднамеренный",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'ru',
        ]);
       PersonalityTest::create([
            'ques_id'   => '70',
            'question'  => "Sind Sie eher:",
            'ans_a'     => "bewusst als spontan",
            'ans_b'     => "spontan als bewusst",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'de',
        ]);
       PersonalityTest::create([
            'ques_id'   => '70',
            'question'  => "¿Tiende a ser más:",
            'ans_a'     => "deliberado que espontáneo",
            'ans_b'     => "espontáneo que deliberado",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'es',
        ]);
       PersonalityTest::create([
            'ques_id'   => '70',
            'question'  => "你是否倾向于更多：",
            'ans_a'     => "故意多于自发",
            'ans_b'     => "自发的而不是故意的",
            'val_a'     => "J",
            'val_b'     => "P",
            'lang'      => 'cn',
        ]);
    }
}
