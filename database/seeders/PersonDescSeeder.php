<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PersonalityDesc;

class PersonDescSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PersonalityDesc::create([
            'person_tag'    => 'INFJ',
            'title'         => "المستشار (المحامي)",
            'desc'          => "يتصف هذا النوع من الشخصيات بأنه حالم ومثالي، ولديه دوما حس من الخيال الإبداعي وإنتاج الافكار الرائعة المختلفة، كما أن لديهم وجهة نظر ورؤية مختلفة للعالم، غالبا لا يتم فهمها من قبل الآخرين، كونها تتسم بالعمق ولا يقبلون بالسطحية في التعامل مع المشكلات، غير أنهم يعتبروا أفراد مهذبون ولطيفون ويهتمون بمن حولهم ويعتبر مساعدة الآخرين هو هدفهمفي الحياة، يعتمدون على حدسهم في النظر للأشياء، ويحبون النظام.",
            'lang'          => 'ar',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INFJ',
            'title'         => "Counsel (lawyer)",
            'desc'          => "This type of personality is characterized as a dreamer and an idealist, and always has a sense of creative imagination and the production of different wonderful ideas, and they have a different view and vision of the world, often not understood by others, as they are deep and do not accept superficiality in dealing with problems, but they They are polite and gentle individuals who care about those around them and consider helping others their goal in life. They rely on their intuition to look at things, and they love order.",
            'lang'          => 'en',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INFJ',
            'title'         => "Conseil (avocat)",
            'desc'          => "Ce type de personnalité est caractérisé comme un rêveur et un idéaliste, et a toujours un sens de l'imagination créatrice et la production de différentes idées merveilleuses, et ils ont une vision et une vision différentes du monde, souvent incomprises par les autres, comme elles le sont. Ce sont des personnes polies et douces qui se soucient de ceux qui les entourent et envisagent d'aider les autres leur objectif dans la vie.Ils s'appuient sur leur intuition pour regarder les choses et ils aiment l'ordre.",
            'lang'          => 'fr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INFJ',
            'title'         => "Danışman (avukat)",
            'desc'          => "Bu kişilik tipi bir hayalperest ve idealist olarak nitelendirilir ve her zaman yaratıcı bir hayal gücü ve farklı harika fikirlerin üretilmesi duygusuna sahiptir ve farklı bir dünya görüşüne ve vizyonuna sahiptirler, genellikle başkaları tarafından olduğu gibi anlaşılmazlar. derin ve sorunlarla uğraşırken yüzeyselliği kabul etmezler ancak çevrelerini önemseyen ve hayattaki amaçlarına başkalarına yardım etmeyi düşünen kibar ve nazik kişilerdir.Sezgilerine güvenirler ve düzeni severler.",
            'lang'          => 'tr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INFJ',
            'title'         => "Советник (юрист)",
            'desc'          => "Этот тип личности характеризуется как мечтатель и идеалист, и всегда обладает чувством творческого воображения и производства различных замечательных идей, и у них другой взгляд и видение мира, часто не понимаемые другими, как они есть. глубокие и не приемлют поверхностности в решении проблем, но они вежливые и нежные люди, которые заботятся о тех, кто их окружает, и считают помощь другим своей целью в жизни. Они полагаются на свою интуицию, чтобы смотреть на вещи, и они любят порядок.",
            'lang'          => 'ru',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INFJ',
            'title'         => "Anwalt (Rechtsanwalt)",
            'desc'          => "Diese Art von Persönlichkeit zeichnet sich als Träumer und Idealist aus und hat immer ein Gespür für kreative Vorstellungskraft und die Produktion verschiedener wunderbarer Ideen, und sie haben eine andere Sicht und Vision der Welt, die von anderen oft nicht verstanden wird, so wie sie sind tief und akzeptieren keine Oberflächlichkeit im Umgang mit Problemen, aber sie sind höfliche und sanfte Menschen, die sich um ihre Umgebung kümmern und es als ihr Lebensziel betrachten, anderen zu helfen.Sie verlassen sich auf ihre Intuition, um Dinge zu betrachten, und sie lieben die Ordnung.",
            'lang'          => 'de',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INFJ',
            'title'         => "Consejero (abogado)",
            'desc'          => "Este tipo de personalidad se caracteriza por ser soñadora e idealista, y siempre tiene un sentido de la imaginación creativa y la producción de diferentes ideas maravillosas, y tienen una visión y visión del mundo diferente, muchas veces no comprendidas por los demás, como son. Son personas amables y amables que se preocupan por los que les rodean y consideran ayudar a los demás como su objetivo en la vida, confían en su intuición para mirar las cosas y aman el orden.",
            'lang'          => 'es',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INFJ',
            'title'         => "法律顾问（律师）",
            'desc'          => "这种性格的特点是梦想家和理想主义者，总是有创造性的想象力和产生不同的美妙想法，他们对世界有不同的看法和视野，往往不被别人理解，因为他们是深沉，不接受肤浅的处理问题，但他们是有礼貌和温柔的人，关心周围的人，并考虑帮助他人实现人生目标。他们依靠直觉看待事物，他们喜欢秩序。",
            'lang'          => 'cn',
        ]);
        

        PersonalityDesc::create([
            'person_tag'    => 'INTJ',
            'title'         => "العقل المدبر (المهندس)",
            'desc'          => "العقل المدبر هو شخص إنطوائي، يميل إلى الهدوء والتحفظ ويبحث عن الراحة في شركائه، يكتفي الأشخاص من هذا النوع بأنفسهم ويفضلون العمل وحدهم، غير أنهم مثاليون في العمل في التخطيط ووضع الاستراتيجيات، ولديهم دوما تساؤل عن سبب حدوث الأشياء، ولا يحيون عدم اليقين، وتستنزفهم العلاقات الاجتماعية، ويقدرون الذكاء والمعرفة، وبالنسبة لمن حولهم قد يكونوا غريبي الأطوار.",
            'lang'          => 'ar',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INTJ',
            'title'         => "mastermind (engineer)",
            'desc'          => "A mastermind is an introverted person who tends to be calm and reserved and seeks comfort in his partners. People of this type are satisfied with themselves and prefer to work alone, but they are perfect at work in planning and strategizing, they always wonder why things happen, they do not live with uncertainty, and it drains them Social relationships, they value intelligence and knowledge, and for those around them they may be eccentric.",
            'lang'          => 'en',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INTJ',
            'title'         => "cerveau (ingénieur)",
            'desc'          => "Un cerveau est une personne introvertie qui a tendance à être calme et réservée et qui cherche du réconfort chez ses partenaires. Les personnes de ce type sont satisfaites d'elles-mêmes et préfèrent travailler seules, mais elles sont parfaites au travail dans la planification et l'élaboration de stratégies, elles se demandent toujours pourquoi les choses arrivent, ils ne vivent pas dans l'incertitude, et cela les épuise.",
            'lang'          => 'fr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INTJ',
            'title'         => "beyni (mühendis)",
            'desc'          => "Bir beyni, sakin ve çekingen olma eğiliminde olan ve ortaklarında rahatlık arayan içe dönük bir kişidir.Bu tür insanlar kendilerinden memnundur ve yalnız çalışmayı tercih ederler, ancak planlama ve strateji oluşturma konusunda işte mükemmeldirler, her zaman neden bir şeylerin neden olduğunu merak ederler. Olursa, belirsizlikle yaşamazlar ve bu onları tüketir Sosyal ilişkiler, zeka ve bilgiye değer verirler ve etraflarındakiler için eksantrik olabilirler.",
            'lang'          => 'tr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INTJ',
            'title'         => "вдохновитель (инженер)",
            'desc'          => "Вдохновитель - это замкнутый человек, который склонен к спокойствию и сдержанности и ищет утешения в своих партнерах.Люди этого типа довольны собой и предпочитают работать в одиночку, но они идеально подходят для работы в планировании и разработке стратегии, они всегда задаются вопросом, почему все происходит. случается, они не живут в условиях неопределенности, и это истощает их. Социальные отношения, они ценят интеллект и знания, а для окружающих они могут быть эксцентричными.",
            'lang'          => 'ru',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INTJ',
            'title'         => "Vordenker (Ingenieur)",
            'desc'          => "Ein Mastermind ist ein introvertierter Mensch, der dazu neigt, ruhig und zurückhaltend zu sein und Trost bei seinen Partnern sucht. Menschen dieser Art sind mit sich selbst zufrieden und arbeiten lieber alleine, aber sie sind perfekt in der Planung und Strategie, sie fragen sich immer, warum Dinge passieren, leben sie nicht mit Unsicherheit, und es entzieht ihnen soziale Beziehungen, sie schätzen Intelligenz und Wissen, und für ihre Umgebung können sie exzentrisch sein.",
            'lang'          => 'de',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INTJ',
            'title'         => "Mente maestra (ingeniero)",
            'desc'          => "Un cerebro es una persona introvertida que tiende a ser tranquila y reservada y busca consuelo en sus parejas. Las personas de este tipo están satisfechas consigo mismas y prefieren trabajar solas, pero son perfectas en el trabajo en la planificación y la elaboración de estrategias, siempre se preguntan por qué las cosas. suceden, no viven con la incertidumbre, y les agota las relaciones sociales, valoran la inteligencia y el conocimiento, y para quienes los rodean pueden ser excéntricos.",
            'lang'          => 'es',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INTJ',
            'title'         => "主谋（工程师）",
            'desc'          => "策划者是一个内向的人，倾向于冷静和矜持，寻求伴侣的安慰。这种人对自己很满意，喜欢独自工作，但他们在计划和谋略方面很擅长工作，他们总是想知道为什么事情发生这种情况时，他们不会生活在不确定性中，这会耗尽他们的社交关系，他们重视智力和知识，而对于周围的人来说，他们可能是古怪的。",
            'lang'          => 'cn',
        ]);


        PersonalityDesc::create([
            'person_tag'    => 'ENFJ',
            'title'         => "المعطاء",
            'desc'          => "تعتبر هذه الشخصية قادرة على التواصل مع الجميع على اختلافهم، غير أنها تملك القدرة على التأثير بهم، وتعتمد دوما على الحدش والمشاعر والعيش في الخيال بصورة أكبر من الواقع، فدوما ما يثيرها المستقبل وتسعى نحو الاكتشاف، والصورة التي يكونهها المحيطون عن هذه الشخصية هي أنهم مخلصون ومؤثرون ولطفاء، ويتمنون دوما الأفضل لهم وللآخرين، ويتم اعتبارهم قادة لهم كاريزما واضحة.",
            'lang'          => 'ar',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENFJ',
            'title'         => "the giver",
            'desc'          => "This character is considered able to communicate with everyone of their differences, but it has the ability to influence them, and always depends on the event and feelings and live in imagination more than reality, as the future always raises it and strives towards discovery, and the image that surrounds this character is that they are loyal They are influential and kind, always wish the best for themselves and others, and are seen as charismatic leaders.",
            'lang'          => 'en',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENFJ',
            'title'         => "le donneur",
            'desc'          => "Ce personnage est considéré comme capable de communiquer avec chacun de ses différences, mais il a la capacité de les influencer, et dépend toujours de l'événement et des sentiments et vit dans l'imagination plus que la réalité, car l'avenir l'élève toujours et tend vers la découverte, et l'image qui entoure ce personnage est qu'ils sont fidèles. Ils sont influents et gentils, souhaitent toujours le meilleur pour eux-mêmes et pour les autres, et sont considérés comme des leaders charismatiques.",
            'lang'          => 'fr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENFJ',
            'title'         => "verici",
            'desc'          => "Bu karakterin herkesle farklılıklarıyla iletişim kurabildiği kabul edilir, ancak onları etkileme yeteneğine sahiptir ve her zaman olaya ve duygulara bağlıdır ve gelecek her zaman onu yükselttiği ve keşfetmeye çalıştığı için hayal gücünde gerçeklikten daha fazla yaşar ve Bu karakteri çevreleyen imaj, sadık olduklarıdır. Etkili ve naziktirler, kendileri ve başkaları için her zaman en iyisini isterler ve karizmatik liderler olarak görülürler.",
            'lang'          => 'tr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENFJ',
            'title'         => "дающий",
            'desc'          => "Этот персонаж считается способным общаться со всеми своими отличиями, но он имеет способность влиять на них и всегда зависит от события и чувств и живет в воображении больше, чем в реальности, поскольку будущее всегда поднимает его и стремится к открытиям, и образ, который окружает этого персонажа, состоит в том, что они лояльны. Они влиятельны и добры, всегда желают всего наилучшего себе и другим и считаются харизматическими лидерами.",
            'lang'          => 'ru',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENFJ',
            'title'         => "der Geber",
            'desc'          => "Dieser Charakter gilt als fähig, mit allen seinen Unterschieden zu kommunizieren, aber er hat die Fähigkeit, sie zu beeinflussen, und hängt immer vom Ereignis und den Gefühlen ab und lebt mehr in der Vorstellung als in der Realität, da die Zukunft sie immer erhebt und nach Entdeckung strebt, und Das Image, das diese Figur umgibt, ist, dass sie loyal sind. Sie sind einflussreich und freundlich, wünschen sich und anderen immer das Beste und werden als charismatische Führer angesehen.",
            'lang'          => 'de',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENFJ',
            'title'         => "el dador",
            'desc'          => "Este personaje se considera capaz de comunicar con todos sus diferencias, pero tiene la capacidad de influir en ellos, y siempre depende del evento y los sentimientos y vive en la imaginación más que en la realidad, ya que el futuro siempre lo plantea y se esfuerza por descubrirlo, y la imagen que envuelve a este personaje es que son leales. Son influyentes y amables, siempre desean lo mejor para ellos y los demás, y son vistos como líderes carismáticos.",
            'lang'          => 'es',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENFJ',
            'title'         => "给予者",
            'desc'          => "这个角色被认为能够与每个人交流他们的不同之处，但它有能力影响他们，总是依赖于事件和感受，生活在想象中而不是现实中，因为未来总是会提高它并努力去发现，和围绕这个角色的形象是他们忠诚他们有影响力和善良，总是希望自己和他人最好，并且被视为有魅力的领导者。",
            'lang'          => 'cn',
        ]);


        PersonalityDesc::create([
            'person_tag'    => 'ISTP',
            'title'         => "الفنان المبدع (الحرفي)",
            'desc'          => "تتسم هذه الشخصية باعتمادها على العقل والمنطق، ولا يمكن توقع ردود فعلها على الإطلاق، ويعتبر الاشخاص من هذا النوع عفويون ويتسمون بالكرم والتفاؤل، ويعتبرون أن الإنصاف والمساواة هما أهم ما يجب تحقيقه، يعملون في الغالب في الميكانيكا والهندسة، كونهم يهوون الفك والتركيب، ويتعلمون من استكشاف الأخطاء وإصلاحها وبالتجربة المباشرة، وقد يجعلهم ذلك جيدون في تقديم التحليل المنطقي.",
            'lang'          => 'ar',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISTP',
            'title'         => "Creative Artist (Craft)",
            'desc'          => "This character is characterized by its reliance on reason and logic, and its reactions cannot be expected at all. People of this type are considered spontaneous, generous and optimistic, and consider that fairness and equality are the most important things to achieve. They work mostly in mechanics and engineering, as they enjoy jaw and installation, and they learn from Troubleshooting and firsthand experience, which can make them good at providing logical analysis.",
            'lang'          => 'en',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISTP',
            'title'         => "Artiste créatif (Artisanat)",
            'desc'          => "Cette personnalité se caractérise par sa confiance en la raison et la logique, et ses réactions ne sont pas du tout prévisibles. Les personnes de ce type sont considérées comme spontanées, généreuses et optimistes, et considèrent que l'équité et l'égalité sont les choses les plus importantes à atteindre. Elles travaillent principalement en mécanique et en ingénierie, car ils apprécient la mâchoire et l'installation, et ils apprennent du dépannage et de l'expérience de première main, ce qui peut les rendre bons pour fournir une analyse logique.",
            'lang'          => 'fr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISTP',
            'title'         => "Yaratıcı Sanatçı (Zanaat)",
            'desc'          => "Bu kişilik, akla ve mantığa bağlı olmasıyla karakterize edilir ve tepkileri hiç beklenemez.Bu tip insanlar kendiliğinden, cömert ve iyimser olarak kabul edilir ve adalet ve eşitliğin ulaşılması gereken en önemli şeyler olduğunu düşünürler.Çoğunlukla çalışırlar. mekanik ve mühendislik alanında uzmandırlar, çünkü çene ve kurulumdan hoşlanırlar ve mantıksal analiz sağlamada kendilerini iyi hale getirebilecek Sorun Giderme ve ilk elden deneyimlerden öğrenirler.",
            'lang'          => 'tr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISTP',
            'title'         => "Креативный художник (Ремесло)",
            'desc'          => "Для этой личности характерно то, что она полагается на разум и логику, и ее реакции нельзя ожидать вообще. Людей этого типа считают спонтанными, щедрыми и оптимистичными и считают, что справедливость и равенство являются наиболее важными вещами, которых нужно достичь. Они в основном работают в области механики и машиностроения, поскольку им нравятся челюсти и установка, и они учатся на устранении неисправностей и на собственном опыте, что может помочь им в проведении логического анализа.",
            'lang'          => 'ru',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISTP',
            'title'         => "Kreativer Künstler (Handwerk)",
            'desc'          => "Diese Persönlichkeit zeichnet sich durch ihr Vertrauen auf Vernunft und Logik aus, und ihre Reaktionen sind überhaupt nicht zu erwarten. Menschen dieser Art gelten als spontan, großzügig und optimistisch und halten Fairness und Gleichberechtigung für das Wichtigste in Mechanik und Ingenieurwesen, da sie Freude an Kiefer und Montage haben und sie lernen aus Fehlersuche und Erfahrungen aus erster Hand, was sie zu guten logischen Analysen machen kann.",
            'lang'          => 'de',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISTP',
            'title'         => "Artista creativo (artesanía)",
            'desc'          => "Esta personalidad se caracteriza por depender de la razón y la lógica, y sus reacciones no pueden esperarse en absoluto. Las personas de este tipo son consideradas espontáneas, generosas y optimistas, y consideran que la justicia y la igualdad son las cosas más importantes a lograr. Trabajan principalmente en mecánica e ingeniería, ya que disfrutan de la mandíbula y la instalación, y aprenden de la resolución de problemas y la experiencia de primera mano, lo que puede hacerlos buenos para proporcionar análisis lógicos.",
            'lang'          => 'es',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISTP',
            'title'         => "创意艺术家（工艺）",
            'desc'          => "这种性格的特点是对理性和逻辑的依赖，其反应完全无法预料。这种人被认为是自发的、慷慨的、乐观的，认为公平和平等是最重要的事情。他们大多工作在机械和工程方面，因为他们喜欢下颚和安装，他们从故障排除和第一手经验中学习，这可以使他们善于提供逻辑分析。",
            'lang'          => 'cn',
        ]);


        PersonalityDesc::create([
            'person_tag'    => 'ESFJ',
            'title'         => "الممول",
            'desc'          => "هذا النوع من الشخصية اجتماعي ومنفتح دون أن يبذل العناء لفعل ذلك، ولديه رغبة دائمة في أن يُسعد الآخرين مما يكسبه شعبية واسعة، ويحبون دون الظهور في الأضواء، فيشتركون في الأنشطة الاجتماعية والرياضية والطلابية، ولديهم قدرة على تنظيم الفعاليات، ويعتبروا أشخاص حماسيين ويحب الآخرين التواجد بجوارهم كونهم يمنحونهم طاقة إيجابية ويهتمون بمن يحبونهم.",
            'lang'          => 'ar',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESFJ',
            'title'         => "The Provider",
            'desc'          => "This type of personality is social and open without making the effort to do so, and he has a constant desire to make others happy, which makes him popular. Next to them being that they give them positive energy and care for those they love.",
            'lang'          => 'en',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESFJ',
            'title'         => "Le fournisseur",
            'desc'          => "Ce type de personnalité est sociable et ouvert sans faire l'effort de le faire, et il a un désir constant de rendre les autres heureux, ce qui le rend populaire, à côté d'eux ils leur donnent une énergie positive et prennent soin de ceux qu'ils aiment.",
            'lang'          => 'fr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESFJ',
            'title'         => "Sağlayan",
            'desc'          => "Bu tür bir kişilik, sosyal ve bunun için çaba sarf etmeden açıktır ve sürekli olarak başkalarını mutlu etme arzusuna sahiptir, bu da onu popüler kılar.Bunun yanında, onlara pozitif enerji vermeleri ve sevdiklerine özen göstermeleridir.",
            'lang'          => 'tr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESFJ',
            'title'         => "Провайдер",
            'desc'          => "Этот тип личности является социальным и открытым, не прилагая к этому никаких усилий, и у него есть постоянное желание сделать других счастливыми, что делает его популярным. Кроме того, они дают им положительную энергию и заботятся о тех, кого любят.",
            'lang'          => 'ru',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESFJ',
            'title'         => "Der Provider",
            'desc'          => "Diese Art von Persönlichkeit ist sozial und offen, ohne sich die Mühe zu machen, und er hat den ständigen Wunsch, andere glücklich zu machen, was ihn beliebt macht, außerdem gibt er ihnen positive Energie und kümmert sich um die, die sie lieben.",
            'lang'          => 'de',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESFJ',
            'title'         => "El proveedor",
            'desc'          => "Este tipo de personalidad es sociable y abierta sin esforzarse en ello, y tiene un deseo constante de hacer felices a los demás, lo que lo hace popular, junto a ellos que le brindan energía positiva y cariño por sus seres queridos.",
            'lang'          => 'es',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESFJ',
            'title'         => "提供者",
            'desc'          => "这种性格是社交和开放的，不努力这样做，他总是渴望让别人快乐，这让他很受欢迎。在他们旁边，他们会给他们正能量，关心他们所爱的人。",
            'lang'          => 'cn',
        ]);



        PersonalityDesc::create([
            'person_tag'    => 'INFP',
            'title'         => "المثالي",
            'desc'          => "يفضل هذا النوع من الشخصية أن يقضوا الوقت بمفردهم في أماكن هادئة دون صخب، فهم إنطوائين ولا يحبون التحدث عن أنفسهم  ومتحفظين، غير أنهم أيضا لدبهم ميل لمراقبة الإشارات التي ترسلها لهم الحياة ويعتبرونها علامات على فهم معنى أعمق متعلق بهم، ويجعلهم ذلك يغرقون كثيرا في خيالهم وأحلام اليقظة، ومأساتهم أنهم ينشدون الكمال والمثالية.",
            'lang'          => 'ar',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INFP',
            'title'         => "The Idealist",
            'desc'          => "This type of personality prefers to spend time alone in quiet places without hustle, they are introverted and do not like to talk about themselves and are reserved, but they also have a tendency to watch the signals that life sends them and consider them signs of understanding a deeper meaning related to them, and this makes them sink a lot in their imagination And daydreams, and their tragedy that they seek perfection and idealism.",
            'lang'          => 'en',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INFP',
            'title'         => "L'idéaliste",
            'desc'          => "Ce type de personnalité préfère passer du temps seul dans des endroits calmes sans agitation, ils sont introvertis et n'aiment pas parler d'eux-mêmes et sont réservés, mais ils ont aussi tendance à regarder les signaux que la vie leur envoie et à les considérer comme des signes de compréhension un sens plus profond qui leur est lié, et cela les fait beaucoup sombrer dans leur imagination Et leurs rêveries, et leur tragédie qu'ils recherchent la perfection et l'idéalisme.",
            'lang'          => 'fr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INFP',
            'title'         => "idealist",
            'desc'          => "Bu kişilik tipi, sessiz ve sakin yerlerde acele etmeden yalnız vakit geçirmeyi tercih eder, içe dönüktür ve kendileri hakkında konuşmayı sevmezler ve çekingendirler, ancak aynı zamanda hayatın kendilerine gönderdiği sinyalleri izleme ve onları anlayış işaretleri olarak görme eğilimleri vardır. onlarla ilgili daha derin bir anlam ve bu onların hayallerinde ve hayallerinde ve mükemmellik ve idealizm aradıkları trajedilerinde çok fazla batmalarına neden olur.",
            'lang'          => 'tr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INFP',
            'title'         => "Идеалист",
            'desc'          => "Этот тип личности предпочитает проводить время в одиночестве в тихих местах без суеты, они интроверты, не любят говорить о себе и замкнуты, но они также имеют тенденцию следить за сигналами, которые им посылает жизнь, и рассматривать их как признаки понимания. более глубокий смысл, связанный с ними, и это заставляет их сильно погружаться в их воображение И мечты, и их трагедию, что они ищут совершенства и идеализма.",
            'lang'          => 'ru',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INFP',
            'title'         => "Der Idealist",
            'desc'          => "Diese Art von Persönlichkeit verbringt am liebsten Zeit alleine an ruhigen Orten ohne Hektik, sie sind introvertiert und sprechen nicht gerne über sich selbst und sind zurückhaltend, aber sie haben auch eine Tendenz, die Signale, die das Leben ihnen sendet, zu beobachten und sie als Zeichen des Verständnisses zu betrachten eine tiefere Bedeutung, die mit ihnen verbunden ist, und das lässt sie sehr in ihrer Vorstellungskraft und ihren Tagträumen und ihrer Tragödie versinken, dass sie nach Perfektion und Idealismus streben.",
            'lang'          => 'de',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INFP',
            'title'         => "El idealista",
            'desc'          => "Este tipo de personalidad prefiere pasar tiempo a solas en lugares tranquilos sin prisas, son introvertidos y no les gusta hablar de sí mismos y son reservados, pero también tienen tendencia a estar atentos a las señales que les envía la vida y considerarlas señales de comprensión. un significado más profundo relacionado con ellos, y esto los hace hundirse mucho en su imaginación y ensoñaciones, y su tragedia que buscan la perfección y el idealismo.",
            'lang'          => 'es',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INFP',
            'title'         => "理想主义者",
            'desc'          => "这种性格的人喜欢在安静的地方独处而不喧嚣，性格内向，不喜欢谈论自己，内向，但也有观察生活发出的信号并认为是理解的迹象。更深的含义与他们有关，这让他们沉沦在他们的想象和白日梦中，以及他们追求完美和理想主义的悲剧中。",
            'lang'          => 'cn',
        ]);



        PersonalityDesc::create([
            'person_tag'    => 'ESFP',
            'title'         => "المؤدي أو المسلي",
            'desc'          => "تتميز هذه الشخصية بأنها مرحة ومسلية وتحب الظهور والترفيه، ويعتبر الأفراد مستكشفون يستمتعون بالتعلم ولديهم مهارات قوية تمكنهم من التعامل مع الآخرين، وعادة ما يكونوا  كريمين وودودين ومتعاطفين مع من حولهم، ويون اهتماما كبيرا بعائلاتهم وأصدقائهم، ويفضلون الاعتماد على الحظوظ والفرص لا التخطيط، ويمكنهم طلب المسعدة ممن حولهم ببساطة وسهولة.",
            'lang'          => 'ar',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESFP',
            'title'         => "performer or entertainer",
            'desc'          => "This character is characterized by being cheerful, entertaining, and loves to show off and entertain. Individuals are considered explorers who enjoy learning and have strong interpersonal skills. They are usually generous, friendly, and sympathetic to those around them. They pay great attention to their families and friends, and prefer to rely on fortunes and opportunities rather than planning, and they can ask Masada those around them simply and easily.",
            'lang'          => 'en',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESFP',
            'title'         => "interprète ou artiste",
            'desc'          => "Ce personnage se caractérise par être gai, divertissant et aime se montrer et divertir. Les individus sont considérés comme des explorateurs qui aiment apprendre et ont de solides compétences interpersonnelles. Ils sont généralement généreux, amicaux et sympathiques avec ceux qui les entourent. Ils accordent une grande attention à leurs familles et leurs amis, et préfèrent compter sur la fortune et les opportunités plutôt que sur la planification, et ils peuvent demander à Massada à ceux qui les entourent simplement et facilement.",
            'lang'          => 'fr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESFP',
            'title'         => "icracı veya şovmen",
            'desc'          => "Bu karakter, neşeli, eğlenceli ve gösteriş ve eğlendirmeyi seven kişilerle karakterize edilir.Kişiler, öğrenmeyi seven ve güçlü kişilerarası becerilere sahip kaşifler olarak kabul edilir.Genellikle cömert, arkadaş canlısı ve çevrelerine karşı sempatiktirler.Öğrenmeye büyük önem verirler. aileleri ve arkadaşları, planlama yerine servet ve fırsatlara güvenmeyi tercih ederler ve Masada'ya çevrelerindekilere basit ve kolay bir şekilde sorabilirler.",
            'lang'          => 'tr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESFP',
            'title'         => "исполнитель или артист",
            'desc'          => "Этот персонаж веселый, веселый, любит красоваться и развлекаться. Люди считаются исследователями, которым нравится учиться и которые обладают сильными навыками межличностного общения. Обычно они щедры, дружелюбны и сочувствуют окружающим. Они уделяют большое внимание окружающим. своим семьям и друзьям, и предпочитают полагаться на судьбу и возможности, а не на планирование, и они могут просто и легко спросить Масаду у окружающих.",
            'lang'          => 'ru',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESFP',
            'title'         => "Darsteller oder Entertainer",
            'desc'          => "Dieser Charakter zeichnet sich dadurch aus, dass er fröhlich und unterhaltsam ist und es liebt, anzugeben und zu unterhalten. Personen gelten als Entdecker, die gerne lernen und über starke zwischenmenschliche Fähigkeiten verfügen. Sie sind normalerweise großzügig, freundlich und mitfühlend mit ihren Mitmenschen. Sie schenken ihnen große Aufmerksamkeit ihre Familien und Freunde und verlassen sich lieber auf Vermögen und Gelegenheiten als auf Planung, und sie können Masada einfach und unkompliziert fragen.",
            'lang'          => 'de',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESFP',
            'title'         => "artista o animador",
            'desc'          => "Este personaje se caracteriza por ser alegre, entretenido y le encanta presumir y entretener. Los individuos son considerados exploradores que disfrutan aprendiendo y tienen fuertes habilidades interpersonales. Suelen ser generosos, amables y comprensivos con quienes los rodean. Prestan gran atención a sus familias y amigos, y prefieren confiar en las fortunas y las oportunidades en lugar de la planificación, y pueden preguntarle a Masada a quienes los rodean de manera simple y sencilla.",
            'lang'          => 'es',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESFP',
            'title'         => "表演者或艺人",
            'desc'          => "这个性格特点是开朗，有趣，喜欢炫耀和娱乐。个人被认为是喜欢学习的探索者，人际交往能力强。他们通常大方，友好，同情周围的人。他们非常关注他们的家人和朋友，更喜欢依靠运气和机会而不是计划，他们可以简单轻松地询问周围的马萨达。",
            'lang'          => 'cn',
        ]);



        PersonalityDesc::create([
            'person_tag'    => 'ENFP',
            'title'         => "البطل",
            'desc'          => "الشخصية التي تملك روح حرة ولديها إدراك خاص بذاتها، غير أنها منفتحة وتتسم بالقيادية، فهذه الشخصية تمتلك أسلوبها الخاص وغالبا ما يكون لها مظهر وأفعال وعادات وحتى أفكار لا تشترك فيها مع آخرين، ولا يحبون أن يضعهم المحيطين داخل إطار محدد، ويتمتعون بحدس قوي، ولكنهم يملون بسرعة ولا يمكنهم متابعة شيء حتى النهاية، ويتجنبون العمل في الوظائف التي تتطلب أداء مهام مفصلة وروتينية.",
            'lang'          => 'ar',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENFP',
            'title'         => "The Champion",
            'desc'          => "The character who has a free spirit and has a special awareness of himself, but is open and characterized by leadership, this character has his own style and often have appearance, actions, habits and even ideas that they do not share with others, and they do not like to be placed within a specific framework, and they have strong intuition, but they They get bored quickly and can't follow anything through, and they avoid jobs that require detailed and routine tasks.",
            'lang'          => 'en',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENFP',
            'title'         => "Le champion",
            'desc'          => "Le personnage qui a un esprit libre et une conscience particulière de lui-même, mais qui est ouvert et caractérisé par le leadership, ce personnage a son propre style et a souvent une apparence, des actions, des habitudes et même des idées qu'il ne partage pas avec les autres, et ils n'aiment pas être placés dans un cadre précis, et ont une forte intuition, mais ils s'ennuient vite et ne peuvent rien suivre, et ils évitent les travaux qui nécessitent des tâches détaillées et routinières.",
            'lang'          => 'fr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENFP',
            'title'         => "Şampiyon",
            'desc'          => "Özgür bir ruha sahip ve kendisi hakkında özel bir farkındalığı olan ancak açık ve liderlikle karakterize olan karakter, bu karakterin kendine has bir tarzı vardır ve çoğu zaman başkalarıyla paylaşmadıkları görünüşe, eylemlere, alışkanlıklara ve hatta fikirlere sahiptir ve Belirli bir çerçeveye oturtulmaktan hoşlanmazlar ve sezgileri kuvvetlidir, ancak çabuk sıkılırlar ve hiçbir şeyi takip edemezler, detaylı ve rutin işler gerektiren işlerden kaçınırlar.",
            'lang'          => 'tr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENFP',
            'title'         => "Чемпион",
            'desc'          => "Персонаж, обладающий свободным духом и особым образом осознающий себя, но открытый и обладающий лидерскими качествами, этот персонаж имеет свой собственный стиль и часто имеет внешний вид, действия, привычки и даже идеи, которые они не разделяют с другими, и они не любят, когда их помещают в определенные рамки, и у них сильная интуиция, но им быстро становится скучно и они не могут ничего выполнить, и они избегают работы, требующей подробных и рутинных задач.",
            'lang'          => 'ru',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENFP',
            'title'         => "Der Meister",
            'desc'          => "Der Charakter, der einen freien Geist hat und ein besonderes Selbstbewusstsein hat, aber offen und von Führung geprägt ist, dieser Charakter hat seinen eigenen Stil und hat oft Aussehen, Handlungen, Gewohnheiten und sogar Ideen, die sie nicht mit anderen teilen, und sie Sie lassen sich nicht gerne in einen bestimmten Rahmen einordnen und haben eine starke Intuition, aber sie langweilen sich schnell, können nichts durchziehen und meiden Jobs, die Detail- und Routineaufgaben erfordern.",
            'lang'          => 'de',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENFP',
            'title'         => "El campeón",
            'desc'          => "El personaje que tiene un espíritu libre y tiene una especial conciencia de sí mismo, pero es abierto y caracterizado por el liderazgo, este personaje tiene un estilo propio y muchas veces tiene apariencia, acciones, hábitos e incluso ideas que no comparte con los demás, y no les gusta que los coloquen en un marco específico, y tienen una fuerte intuición, pero se aburren rápidamente y no pueden seguir nada, y evitan trabajos que requieren tareas detalladas y rutinarias.",
            'lang'          => 'es',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENFP',
            'title'         => "冠军",
            'desc'          => "具有自由精神，对自己有特殊认识，但开放且具有领导力的角色，这个角色有自己的风格，经常有他们不与他人分享的外表、行为、习惯甚至想法，并且他们不喜欢被置于一个特定的框架内，他们有很强的直觉，但他们很快就会厌倦，什么也做不完，他们会回避需要详细和常规任务的工作。",
            'lang'          => 'cn',
        ]);



        PersonalityDesc::create([
            'person_tag'    => 'ESTP',
            'title'         => "رائد الأعمال",
            'desc'          => "تتمكن هذه الشخصية من التفاعل الاجتماعي الجيد، وتستمد قوتها من المشاعر والعواطف، وتمتلك القدرة على التفكير المنطقي، وتتمتع بروح الفكاهة، كما أنها شخصية مهتمة بإصلاح الأخطاء وعدم البقاء دون عمل، وتقديم البديل دوما، ولا يحبون البيئات شديدة التنظيم، كما يفضلون الحركة المستمرة والعمل.",
            'lang'          => 'ar',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESTP',
            'title'         => "Entrepreneur",
            'desc'          => "This character is able to have good social interaction, derives its strength from feelings and emotions, has the ability to think logically, has a sense of humor, and is interested in fixing mistakes and not staying without work, and always providing an alternative, and they do not like highly organized environments, and they prefer constant movement and work .",
            'lang'          => 'en',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESTP',
            'title'         => "Entrepreneur",
            'desc'          => "Ce personnage est capable d'avoir une bonne interaction sociale, tire sa force des sentiments et des émotions, a la capacité de penser logiquement, a le sens de l'humour et est intéressé à corriger les erreurs et à ne pas rester sans travail, et à toujours proposer une alternative, et ils n'aiment pas les environnements très organisés et préfèrent les mouvements et le travail constants.",
            'lang'          => 'fr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESTP',
            'title'         => "Girişimci",
            'desc'          => "İyi bir sosyal etkileşime sahip olan, gücünü duygu ve duygulardan alan, mantıklı düşünebilen, espri anlayışı olan, hataları düzeltmek ve işsiz kalmamakla ilgilenen ve her zaman bir alternatif sunan bu karakter, son derece organize ortamlardan hoşlanmazlar ve sürekli hareket etmeyi ve çalışmayı tercih ederler.",
            'lang'          => 'tr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESTP',
            'title'         => "Предприниматель",
            'desc'          => "Этот персонаж способен на хорошее социальное взаимодействие, черпает силу в чувствах и эмоциях, имеет способность мыслить логически, обладает чувством юмора и заинтересован в исправлении ошибок и не оставаться без работы, и всегда предоставляет альтернативу, и им не нравится высокоорганизованная среда, они предпочитают постоянное движение и работу.",
            'lang'          => 'ru',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESTP',
            'title'         => "Unternehmer",
            'desc'          => "Dieser Charakter ist in der Lage, gute soziale Interaktionen zu führen, bezieht seine Stärke aus Gefühlen und Emotionen, hat die Fähigkeit, logisch zu denken, hat einen Sinn für Humor und ist daran interessiert, Fehler zu beheben und nicht ohne Arbeit zu bleiben und immer eine Alternative zu bieten, und sie mögen keine stark organisierten Umgebungen und bevorzugen ständige Bewegung und Arbeit.",
            'lang'          => 'de',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESTP',
            'title'         => "Empresario",
            'desc'          => "Este personaje es capaz de tener una buena interacción social, deriva su fuerza de los sentimientos y emociones, tiene la capacidad de pensar con lógica, tiene sentido del humor y está interesado en corregir errores y no quedarse sin trabajo, y brindar siempre una alternativa, y no les gustan los entornos muy organizados y prefieren el movimiento y el trabajo constantes.",
            'lang'          => 'es',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESTP',
            'title'         => "企业家",
            'desc'          => "这个角色能够有良好的社交互动，从感觉和情绪中汲取力量，有逻辑思考的能力，有幽默感，有兴趣纠正错误而不是无所事事，并且总是提供替代方案，并且他们不喜欢高度组织化的环境，他们更喜欢不断的运动和工作。",
            'lang'          => 'cn',
        ]);



        PersonalityDesc::create([
            'person_tag'    => 'ESTJ',
            'title'         => "المشرف",
            'desc'          => "الأشخاص من هذا النوع يكونون على قدر كبير من الالتزام بالتقاليد واتباع النظام، ويتعمدون دوما فعل ما هو صحيح ليكونون مقبولون من المجتمع، يمكن اعتبارهم المواطنين الصالحين، ويجأ إليه كثيرون من أجل طلب التوجيه والمشورة، ويحبون هم دوما تقديم المساعدة، وغالبا ما يكونون متحمسين للدفاع عن القيم التقليدية.",
            'lang'          => 'ar',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESTJ',
            'title'         => "The Supervisor",
            'desc'          => "People of this type are very traditional and orderly, always deliberately do what is right to be accepted by society, can be considered good citizens, many turn to for guidance and advice, always like to help, and are often enthusiastic in defense about traditional values.",
            'lang'          => 'en',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESTJ',
            'title'         => "Le surveillant",
            'desc'          => "Les personnes de ce type sont très traditionnelles et ordonnées, font toujours délibérément ce qui est juste pour être acceptées par la société, peuvent être considérées comme de bons citoyens, beaucoup se tournent vers pour obtenir des conseils et des conseils, aiment toujours aider et sont souvent enthousiastes à l'idée de défendre les valeurs traditionnelles .",
            'lang'          => 'fr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESTJ',
            'title'         => "Denetçi",
            'desc'          => "Bu tür insanlar çok geleneksel ve düzenlidir, toplum tarafından kabul edilmek için her zaman bilinçli olarak doğru olanı yaparlar, iyi vatandaşlar olarak kabul edilebilirler, birçoğu rehberlik ve tavsiye için başvurur, her zaman yardım etmeyi sever ve genellikle geleneksel değerleri savunmada heveslidir. .",
            'lang'          => 'tr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESTJ',
            'title'         => "Наблюдатель",
            'desc'          => "Люди этого типа очень традиционны и упорядочены, всегда сознательно делают то, что правильно для общества, могут считаться хорошими гражданами, многие обращаются за советом и советом, всегда любят помогать и часто с энтузиазмом защищают традиционные ценности. .",
            'lang'          => 'ru',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESTJ',
            'title'         => "Der Vorgesetzte",
            'desc'          => "Menschen dieses Typs sind sehr traditionell und geordnet, tun immer bewusst das, was für eine gesellschaftliche Akzeptanz das Richtige ist, gelten als gute Bürger, wenden sich für viele an Rat und Orientierung, helfen immer gerne und verteidigen oft mit Begeisterung traditionelle Werte .",
            'lang'          => 'de',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESTJ',
            'title'         => "El supervisor",
            'desc'          => "Las personas de este tipo son muy tradicionales y ordenadas, siempre hacen deliberadamente lo que es correcto para ser aceptadas por la sociedad, pueden ser consideradas buenas ciudadanas, muchas buscan orientación y consejo, siempre les gusta ayudar y a menudo se muestran entusiastas en la defensa de los valores tradicionales.",
            'lang'          => 'es',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ESTJ',
            'title'         => "主管",
            'desc'          => "这类人非常传统、有秩序，总是刻意为社会所接受，可以算是好公民，很多会求助于指点和建议，总是乐于助人，对传统价值观往往热衷于捍卫.",
            'lang'          => 'cn',
        ]);



        PersonalityDesc::create([
            'person_tag'    => 'ENTJ',
            'title'         => "القائد",
            'desc'          => "يستمتع الأشخاص من هذا النوع بتولي المسئولية، ويعتبرون التحديات والعقبات فرصة يطورون ها من أنفسهم وتعد حافز لهم ليكونوا أفضل، ولديهم صفة القيادية بالفطرة وكأنها موهبة، فلا يتهربون من اتخاذ القرارات، ودوما فاعلين في مجريات الأمور التي تحدث لهم، كونهم لا يفضلون أن يبقوا دون تأثير، ولذلك فهم منفتحون على التواجد مع الناس، ولكن يصعب عليهم رؤية الأمور من خارج منظورهم الخاص.",
            'lang'          => 'ar',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENTJ',
            'title'         => "The Commander",
            'desc'          => "People of this type enjoy taking responsibility, and consider challenges and obstacles as an opportunity to develop themselves and are an incentive for them to be better. Impact, so they are open to being with people, but it is difficult for them to see things from outside their own perspective.",
            'lang'          => 'en',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENTJ',
            'title'         => "Le commandant",
            'desc'          => "Les personnes de ce type aiment prendre leurs responsabilités et considèrent les défis et les obstacles comme une opportunité de se développer et sont une incitation à être meilleur. Impact, ils sont donc ouverts à être avec les gens, mais il leur est difficile de voir les choses de l'extérieur de leur propre point de vue.",
            'lang'          => 'fr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENTJ',
            'title'         => "Komutan",
            'desc'          => "Bu tip insanlar sorumluluk almaktan hoşlanırlar ve zorlukları ve engelleri kendilerini geliştirmek için bir fırsat ve daha iyi olmaları için bir teşvik olarak görürler. Etki, bu nedenle insanlarla birlikte olmaya açıktırlar, ancak olaylara kendi bakış açılarının dışından bakmaları zordur.",
            'lang'          => 'tr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENTJ',
            'title'         => "Командир",
            'desc'          => "Людям этого типа нравится брать на себя ответственность, и они рассматривают проблемы и препятствия как возможность развиваться и являются для них стимулом стать лучше. Воздействие, поэтому они открыты для общения с людьми, но им трудно смотреть на вещи со стороны.",
            'lang'          => 'ru',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENTJ',
            'title'         => "Der Kommandant",
            'desc'          => "Menschen dieses Typs übernehmen gerne Verantwortung und sehen Herausforderungen und Hindernisse als Chance, sich weiterzuentwickeln und sind Ansporn, besser zu werden. Impact, also sind sie offen dafür, mit Menschen zusammen zu sein, aber es ist schwierig für sie, die Dinge außerhalb ihrer eigenen Perspektive zu sehen.",
            'lang'          => 'de',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENTJ',
            'title'         => "El comandante",
            'desc'          => "Las personas de este tipo disfrutan asumiendo responsabilidades, y consideran los desafíos y obstáculos como una oportunidad para desarrollarse y son un incentivo para ser mejores. Impacto, por lo que están abiertos a estar con la gente, pero les resulta difícil ver las cosas desde fuera de su propia perspectiva.",
            'lang'          => 'es',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENTJ',
            'title'         => "指挥官",
            'desc'          => "这种类型的人喜欢承担责任，并将挑战和障碍视为自我发展的机会，是他们变得更好的动力。 Impact，所以他们愿意与人相处，但他们很难从自己的角度之外看待事物。",
            'lang'          => 'cn',
        ]);



         PersonalityDesc::create([
            'person_tag'    => 'INTP',
            'title'         => "المفكر",
            'desc'          => "يعتبر هذا النوع من الشخصية هو ما ينسب له الفضل في عديد من الاكتشافات العلمية على مدار التاريخ، ويتم اعتبارهم الشخصيات الأكثر منطقية على الإطلاق، كما أن لديهم قدرة على قراءة الناس والتقاط التناقضات، كما أن لديهم قدرة كبيرة على الإبداع والايتكار وتقديم فكر حيوي مختلف، من الممكن أن يكونوا مصطرين ومزاجيين فشخصياتهم معقدة، ويمتلكون موهبة تحليل الأفكار بطريقة مبتكرة.",
            'lang'          => 'ar',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INTP',
            'title'         => "the thinker",
            'desc'          => "This type of personality is what is credited with many scientific discoveries throughout history, and they are considered the most logical characters at all, and they have the ability to read people and pick up on contradictions, and they have a great ability to creativity and innovation and present different vital thought, They can be stubborn and moody, have complex personalities, and have a gift for analyzing ideas in an innovative way.",
            'lang'          => 'en',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INTP',
            'title'         => "le penseur",
            'desc'          => "Ce type de personnalité est ce qui est crédité de nombreuses découvertes scientifiques à travers l'histoire, et ils sont considérés comme les personnages les plus logiques du tout, et ils ont la capacité de lire les gens et de relever les contradictions, et ils ont une grande capacité de créativité et d'innovation. et présentent différentes pensées vitales. Ils peuvent être têtus et de mauvaise humeur, avoir des personnalités complexes et avoir un don pour analyser les idées de manière innovante.",
            'lang'          => 'fr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INTP',
            'title'         => "düşünür",
            'desc'          => "Bu tür kişilik, tarih boyunca birçok bilimsel keşifle anılan şeydir ve en mantıklı karakterler olarak kabul edilirler ve insanları okuma ve çelişkileri anlama yeteneklerine sahiptirler ve yaratıcılık ve yenilik konusunda büyük yetenekleri vardır. ve farklı yaşamsal düşünceler sunarlar, İnatçı ve huysuz olabilirler, karmaşık kişiliklere sahip olabilirler ve fikirleri yenilikçi bir şekilde analiz etme yeteneklerine sahip olabilirler.",
            'lang'          => 'tr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INTP',
            'title'         => "мыслитель",
            'desc'          => "Этому типу личности приписывают многие научные открытия на протяжении всей истории, и они считаются наиболее логичными персонажами, и у них есть способность читать людей и улавливать противоречия, а также они обладают большими способностями к творчеству и новаторству. они могут быть упрямыми и капризными, иметь сложный характер и обладать даром новаторски анализировать идеи.",
            'lang'          => 'ru',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INTP',
            'title'         => "der Denker",
            'desc'          => "Dieser Art von Persönlichkeit werden im Laufe der Geschichte viele wissenschaftliche Entdeckungen zugeschrieben, und sie gelten als die logischsten Charaktere überhaupt, und sie haben die Fähigkeit, Menschen zu lesen und Widersprüche aufzugreifen, und sie haben eine große Fähigkeit zu Kreativität und Innovation Sie können stur und launisch sein, komplexe Persönlichkeiten haben und die Gabe haben, Ideen auf innovative Weise zu analysieren.",
            'lang'          => 'de',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INTP',
            'title'         => "el pensador",
            'desc'          => "A este tipo de personalidad se le atribuyen muchos descubrimientos científicos a lo largo de la historia, y son considerados los personajes más lógicos en absoluto, tienen la capacidad de leer a la gente y captar contradicciones, y tienen una gran capacidad de creatividad e innovación. y presentar un pensamiento vital diferente. Pueden ser tercos y malhumorados, tener personalidades complejas y tener un don para analizar ideas de manera innovadora.",
            'lang'          => 'es',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'INTP',
            'title'         => "思想家",
            'desc'          => "这种性格被认为是历史上许多科学发现的功劳，被认为是最符合逻辑的性格，有识人、识矛盾的能力，有极强的创造力和创新能力。并提出不同的重要思想，他们可能固执和喜怒无常，具有复杂的个性，并具有以创新方式分析想法的天赋。",
            'lang'          => 'cn',
        ]);


         PersonalityDesc::create([
            'person_tag'    => 'ISFJ',
            'title'         => "المربي",
            'desc'          => "الأشخاص الطيبين دافئو القلب هم من ينتمون لهذه الشخصية، التي تعتبر معطاءة وتدعم المحيطين والمحبين بكل حماس ودون أنانية، غير أنهم قادرون على الانسجام والتعاون وتقدير مشاعر الآخرين، وتميل طبيعتهم إلى العمل الجاد والدقة لدرجة الكمال، ويفعلون دوما ما بوسعهم ليفوا بالجميل، ويتميزون بالكرم.",
            'lang'          => 'ar',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISFJ',
            'title'         => "The Nurturer",
            'desc'          => "Kind and warm-hearted people belong to this character, which is considered giving and supports those around and lovers with enthusiasm and without selfishness. However, they are able to harmonize, cooperate and appreciate the feelings of others, and their nature tends to work hard and accuracy to the point of perfection, and they always do what they can to fulfill the beautiful, and they are distinguished by generosity.",
            'lang'          => 'en',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISFJ',
            'title'         => "Le nourricier",
            'desc'          => "Des personnes gentilles et chaleureuses appartiennent à ce personnage, qui est considéré comme généreux et soutient ceux qui l'entourent et les amoureux avec enthousiasme et sans égoïsme. Cependant, ils sont capables d'harmoniser, de coopérer et d'apprécier les sentiments des autres, et leur nature a tendance à travailler dur et avec précision jusqu'à la perfection, et ils font toujours ce qu'ils peuvent pour accomplir le beau, et ils se distinguent par la générosité.",
            'lang'          => 'fr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISFJ',
            'title'         => "bakıcı",
            'desc'          => "Sevecen ve sımsıcak insanlar, fedakarlık olarak kabul edilen ve etrafındakilere ve sevenlere coşkuyla ve bencillik göstermeden destek olan bu karaktere aittir. Bununla birlikte, uyum sağlayabilir, işbirliği yapabilir ve başkalarının duygularını takdir edebilirler ve doğaları mükemmellik noktasına kadar sıkı ve doğruluk eğilimindedir ve her zaman güzeli gerçekleştirmek için ellerinden geleni yaparlar ve cömertlikleriyle ayırt edilirler.",
            'lang'          => 'tr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISFJ',
            'title'         => "Воспитатель",
            'desc'          => "К этому персонажу относятся добрые и отзывчивые люди, который считается дающим и поддерживает окружающих и влюбленных с энтузиазмом и без эгоизма. Тем не менее, они способны гармонизировать, сотрудничать и ценить чувства других, и их природа имеет тенденцию усердно и аккуратно работать до совершенства, и они всегда делают все возможное, чтобы воплотить прекрасное, и их отличает щедрость.",
            'lang'          => 'ru',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISFJ',
            'title'         => "Der Ernährer",
            'desc'          => "Zu diesem Charakter gehören freundliche und warmherzige Menschen, die als gebend gelten und ihre Mitmenschen und Liebenden mit Enthusiasmus und ohne Egoismus unterstützen. Sie sind jedoch in der Lage, die Gefühle anderer zu harmonisieren, zu kooperieren und zu schätzen, und ihre Natur neigt dazu, hart und genau bis zur Perfektion zu arbeiten, und sie tun immer alles, um das Schöne zu erfüllen, und sie zeichnen sich durch Großzügigkeit aus.",
            'lang'          => 'de',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISFJ',
            'title'         => "El criador",
            'desc'          => "A este personaje pertenecen personas amables y de buen corazón, que se considera generoso y apoya a los que le rodean y a los amantes con entusiasmo y sin egoísmo. Sin embargo, son capaces de armonizar, cooperar y apreciar los sentimientos de los demás, y su naturaleza tiende a trabajar duro y con precisión hasta el punto de la perfección, y siempre hacen lo que pueden para realizar lo bello, y se distinguen por la generosidad.",
            'lang'          => 'es',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISFJ',
            'title'         => "养育者",
            'desc'          => "善良热心的人属于这种性格，被认为是给予和支持周围的人和爱人，热情而不自私。然而，他们能够协调、合作和欣赏他人的感受，他们的天性倾向于努力和准确到完美的地步，他们总是尽其所能来实现美丽，他们以慷慨着称。",
            'lang'          => 'cn',
        ]);



         PersonalityDesc::create([
            'person_tag'    => 'ENTP',
            'title'         => "الحالم (المحاور)",
            'desc'          => "الشخصية التي يمتعها الخوض في المناقشات العميقة، تارة تدلل بالحجج والمنطق، وأخرى تلقي بهم عرض الحائط لتدلل أن لا قيمة لهم، وتعتبر من الشخصيات النادرة، فهم منفتحون لكن لا تجذبهم الاحادايث الصغيرة العابرة، ولا يزدهرون في التجمعات الكبيرة، ويعدون أذكياء بصورة كبيرة ولديهم معرفة، ويمكنهم التشكيك في كل شيء.",
            'lang'          => 'ar',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENTP',
            'title'         => "The dreamer (interlocutor)",
            'desc'          => "The character who enjoys engaging in deep discussions, sometimes pampered with arguments and logic, and others throw them away to show that they are worthless, and is considered one of the rare characters, they are open but not attracted by small, fleeting talk, and do not thrive in large gatherings, and they are highly intelligent and have knowledge And they can question everything.",
            'lang'          => 'en',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENTP',
            'title'         => "rêveur (interlocuteur)",
            'desc'          => "Le personnage qui aime s'engager dans des discussions approfondies, parfois choyé avec des arguments et de la logique, et d'autres les jettent pour montrer qu'ils ne valent rien, et est considéré comme l'un des rares personnages, ils sont ouverts mais pas attirés par les petites paroles éphémères, et ne prospèrent pas dans les grands rassemblements, et ils sont très intelligents et ont des connaissances Et ils peuvent tout remettre en question.",
            'lang'          => 'fr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENTP',
            'title'         => "hayalperest (muhatap)",
            'desc'          => "Derin tartışmalara girmekten hoşlanan, bazen argümanlar ve mantıkla şımartılan ve diğerleri, değersiz olduklarını göstermek için onları bir kenara atan ve nadir karakterlerden biri olarak kabul edilen karakter, açıktır, ancak kısa, kısa konuşmalardan etkilenmezler ve Kalabalık ortamlarda gelişmezler, son derece zekidirler ve bilgiye sahiptirler ve her şeyi sorgulayabilirler.",
            'lang'          => 'tr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENTP',
            'title'         => "мечтатель (собеседник)",
            'desc'          => "Персонаж, который любит вести глубокие дискуссии, иногда балуется аргументами и логикой, а другие отбрасывают их, чтобы показать, что они бесполезны, и считается одним из редких персонажей, они открыты, но их не привлекают мелкие, мимолетные разговоры и не преуспевают в больших собраниях, они очень умны и обладают знаниями. И они могут подвергать сомнению все.",
            'lang'          => 'ru',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENTP',
            'title'         => "Träumer (Gesprächspartner)",
            'desc'          => "Der Charakter, der gerne tiefgründige Diskussionen führt, manchmal mit Argumenten und Logik verwöhnt wird, und andere sie wegwerfen, um zu zeigen, dass er wertlos ist und als einer der seltenen Charaktere gilt, er ist offen, aber nicht von kleinen, flüchtigen Gesprächen angezogen, und gedeihen nicht in großen Versammlungen, und sie sind hochintelligent und haben Wissen Und sie können alles in Frage stellen.",
            'lang'          => 'de',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENTP',
            'title'         => "soñador (interlocutor)",
            'desc'          => "El personaje que disfruta de participar en discusiones profundas, a veces mimado con argumentos y lógica, y otros los desecha para demostrar que no valen nada, y es considerado uno de los personajes raros, es abierto pero no atraído por conversaciones pequeñas y fugaces, y no prosperan en grandes reuniones, son muy inteligentes y tienen conocimiento y pueden cuestionar todo.",
            'lang'          => 'es',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ENTP',
            'title'         => "梦想家（对话者）",
            'desc'          => "喜欢深入讨论的角色，有时喜欢争论和逻辑，被别人扔掉以表明他们一文不值，被认为是罕见的角色之一，他们开放但不会被小而短暂的谈话所吸引，并且不在大型聚会中茁壮成长，他们智商高，知识渊博，可以质疑一切。",
            'lang'          => 'cn',
        ]);



         PersonalityDesc::create([
            'person_tag'    => 'ISFP',
            'title'         => "الفنان",
            'desc'          => "غير أن نمط هذه الشخصية محب للفن ويمكن وصفه بالفنان، فهم أيضا مغامرون ويحبون دوما امتلاك الفرصة لعيش كل لحظة على أكمل وجه، ويفضلون تجربة كل جديد واكتشاف أنفسهم، قد يكونوا إنطوائيين ولكنهم ودودين وكريمين مع الأشخاص المقربين منهم، ويحبون مقابلة أشخاص جدد والتعرف عليهم، وتعتبر هذه الشخصية له روح مبدعة وخلاقة ونشطة، وتميل إلى الإيجابية.",
            'lang'          => 'ar',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISFP',
            'title'         => "The Composer",
            'desc'          => "However, the style of this character is art-loving and can be described as an artist. They are also adventurous and always love having the opportunity to live every moment to the fullest. They prefer to try everything new and discover themselves. They may be introverted, but they are friendly and generous with the people close to them, and they love to meet new people and get to know them, This personality is considered a creative, creative and active spirit, and tends to be positive.",
            'lang'          => 'en',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISFP',
            'title'         => "Le compositeur",
            'desc'          => "Cependant, le style de ce personnage est amateur d'art et peut être décrit comme un artiste. Ils sont également aventureux et aiment toujours avoir l'opportunité de vivre pleinement chaque instant. Ils préfèrent tout essayer de nouveau et se découvrir. Ils peuvent être introvertis, mais ils sont amicaux et généreux avec leurs proches, et ils aiment rencontrer de nouvelles personnes et apprendre à les connaître. Cette personnalité est considérée comme un esprit créatif, créatif et actif, et a tendance à être positive.",
            'lang'          => 'fr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISFP',
            'title'         => "besteci",
            'desc'          => "Ancak bu karakterin tarzı sanatsever ve sanatçı olarak nitelendirilebilir. Aynı zamanda maceracıdırlar ve her anı dolu dolu yaşama fırsatına sahip olmayı her zaman severler. Yeni olan her şeyi denemeyi ve kendilerini keşfetmeyi tercih ederler. İçine kapanık olabilirler, ancak yakınlarına karşı arkadaş canlısı ve cömerttirler ve yeni insanlarla tanışmayı ve onları tanımayı severler. Bu kişilik yaratıcı, yaratıcı ve aktif bir ruh olarak kabul edilir ve olumlu olma eğilimindedir.",
            'lang'          => 'tr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISFP',
            'title'         => "Композитор",
            'desc'          => "Однако стиль этого персонажа - искусство, и его можно охарактеризовать как художника. Они также любят приключения и всегда любят иметь возможность прожить каждое мгновение в полной мере. Они предпочитают пробовать все новое и открывать для себя себя. Они могут быть интровертами, но они дружелюбны и щедры по отношению к близким им людям, и они любят знакомиться с новыми людьми и узнавать их. Эта личность считается творческой, творческой и активной душой и имеет тенденцию быть позитивной.",
            'lang'          => 'ru',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISFP',
            'title'         => "Der Komponist",
            'desc'          => "Der Stil dieser Figur ist jedoch kunstliebend und kann als Künstler bezeichnet werden. Sie sind auch abenteuerlustig und lieben es, jeden Moment in vollen Zügen zu genießen. Sie probieren lieber alles neu aus und entdecken sich selbst. Sie mögen introvertiert sein, aber sie sind freundlich und großzügig mit den Menschen, die ihnen nahe stehen, und sie lieben es, neue Leute zu treffen und kennenzulernen. Diese Persönlichkeit gilt als kreativer, kreativer und aktiver Geist und neigt dazu, positiv zu sein.",
            'lang'          => 'de',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISFP',
            'title'         => "El compositor",
            'desc'          => "Sin embargo, el estilo de este personaje es amante del arte y puede describirse como un artista. También son aventureros y siempre les encanta tener la oportunidad de vivir cada momento al máximo. Prefieren probar todo lo nuevo y descubrirse a sí mismos. Puede que sean introvertidos, pero son amigables y generosos con las personas cercanas a ellos, y les encanta conocer gente nueva y conocerla. Esta personalidad se considera un espíritu creativo, creativo y activo, y tiende a ser positivo.",
            'lang'          => 'es',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISFP',
            'title'         => "作曲家",
            'desc'          => "不过这个人物的画风是爱好艺术的，可以用艺术家来形容。他们也喜欢冒险，总是喜欢有机会尽情享受每一刻。他们更喜欢尝试新事物并发现自己。他们可能性格内向，但对亲近的人友好而慷慨，他们喜欢结识新朋友并结识他们，这种性格被认为是富有创造力、创造性和积极性的精神，并且往往是积极的。",
            'lang'          => 'cn',
        ]);



         PersonalityDesc::create([
            'person_tag'    => 'ISTJ',
            'title'         => "المراقب (اللوجيستي)",
            'desc'          => "في التعامل الأول مع هذه الشخصية، سيكون الانطباع هو الشعور بالخوف منهم، كونهم جادين ورسميين مع الأشخاص غير المقربين منهم، وهم أكثر من يُساء فهمهم، ويفضلون العمل الجاد والمسئولية الاجتماعية، كما يتسمون بالتحقظ والهدوء والاستقامة، ويمزجون بين الاستشعار والتفكير والإنطوائية والحكم، ويعتبروا منطقيون وحكيمون ويميلون إلى الحقائق، لديهم دقة وصبر تجعلهم مناسبين في كثير من الوظائف.",
            'lang'          => 'ar',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISTJ',
            'title'         => "The Inspector",
            'desc'          => "In the first interaction with this character, the impression will be to feel fear of them, as they are serious and official with people who are not close to them, and they are the most misunderstood, and prefer hard work and social responsibility, as they are vigilant, calm and straight, and they mix between sensing, thinking, introversion and judgment, and they are considered logical. They are wise and tend to the facts, they have the accuracy and patience that make them suitable for many jobs.",
            'lang'          => 'en',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISTJ',
            'title'         => "L'inspecteur",
            'desc'          => "Dans la première interaction avec ce personnage, l'impression sera d'avoir peur d'eux, car ils sont sérieux et officiels avec des personnes qui ne sont pas proches d'eux, et ils sont les plus incompris, et préfèrent le travail acharné et la responsabilité sociale, car ils sont vigilants, calmes et droits, et ils mélangent entre sentir, penser, introversion et jugement, et ils sont considérés comme logiques. Ils sont sages et attentifs aux faits, ils ont la précision et la patience qui les rendent aptes à de nombreux emplois.",
            'lang'          => 'fr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISTJ',
            'title'         => "Müfettiş",
            'desc'          => "Bu karakterle ilk etkileşimde, kendilerine yakın olmayan insanlarla ciddi ve resmi oldukları ve en çok yanlış anlaşıldıkları için onlardan korku hissetmek ve çok çalışmayı ve sosyal sorumluluğu tercih etmeleri gibi bir izlenim olacaktır. uyanık, sakin ve dürüsttürler ve algılama, düşünme, içe dönüklük ve yargılama arasında karışırlar ve mantıklı olarak kabul edilirler. Akıllıdırlar ve gerçeklere eğilimlidirler, onları birçok işe uygun hale getiren doğruluk ve sabra sahiptirler.",
            'lang'          => 'tr',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISTJ',
            'title'         => "Инспектор",
            'desc'          => "При первом взаимодействии с этим персонажем создается впечатление, что вы испытываете страх перед ними, поскольку они серьезны и официальны с людьми, которые им не близки, и их больше всего неправильно понимают, и они предпочитают тяжелую работу и социальную ответственность, поскольку они бдительны, спокойны и прямолинейны, они сочетают в себе ощущения, мышление, интроверсию и суждение, и их считают логичными. Они мудры и склонны к фактам, у них есть точность и терпение, которые делают их подходящими для многих работ.",
            'lang'          => 'ru',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISTJ',
            'title'         => "Der Inspektor",
            'desc'          => "Bei der ersten Interaktion mit dieser Figur wird der Eindruck entstehen, Angst vor ihnen zu haben, da sie mit Menschen, die ihnen nicht nahe stehen, ernst und offiziell sind, und sie werden am meisten missverstanden und bevorzugen harte Arbeit und soziale Verantwortung, da sie sind wachsam, ruhig und aufrichtig, und sie mischen sich zwischen Fühlen, Denken, Introvertiertheit und Urteilsvermögen und gelten als logisch. Sie sind weise und orientieren sich an den Tatsachen, sie haben die Genauigkeit und Geduld, die sie für viele Aufgaben geeignet machen.",
            'lang'          => 'de',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISTJ',
            'title'         => "El inspector",
            'desc'          => "En la primera interacción con este personaje, la impresión será sentir miedo por ellos, ya que son serios y oficiales con personas que no son cercanas a ellos, y son los más incomprendidos, y prefieren el trabajo duro y la responsabilidad social, ya que son vigilantes, tranquilos y rectos, y se mezclan entre sentir, pensar, introversión y juicio, y se consideran lógicos. Son sabios y atienden los hechos, tienen la precisión y la paciencia que los hacen aptos para muchos trabajos.",
            'lang'          => 'es',
        ]);
        PersonalityDesc::create([
            'person_tag'    => 'ISTJ',
            'title'         => "检查员",
            'desc'          => "与这个角色的第一次互动，给人的印象是害怕他们，因为他们对待不亲近的人是认真的和官方的，他们最容易被误解，更喜欢努力工作和承担社会责任，因为他们警惕、冷静、直率，他们混合了感觉、思考、内向和判断，他们被认为是合乎逻辑的。他们很聪明，倾向于事实，他们的准确性和耐心使他们适合许多工作。",
            'lang'          => 'cn',
        ]);



    }
}
