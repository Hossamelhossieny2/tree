<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShareUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('share_users', function (Blueprint $table) {
            $table->id();
            $table->foreignId('share_id');
            $table->integer('share_type');
            $table->foreignId('share_user');
            $table->foreignId('piece_id');
            $table->integer('family_id');
            $table->integer('family_2')->nullable();
            $table->foreignId('user_id')->nullable();
            $table->string('share_title');
            $table->string('share_desc');
            $table->string('share_ids');
            $table->string('piece_name_ar');
            $table->string('piece_name_en');
            $table->string('share_image');
            $table->date('share_pay_time');
            $table->integer('is_payed')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('share_users');
    }
}
