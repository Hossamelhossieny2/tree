<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPersonalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_personalities', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->integer('E')->default(0);
            $table->integer('I')->default(0);
            $table->integer('S')->default(0);
            $table->integer('N')->default(0);
            $table->integer('T')->default(0);
            $table->integer('F')->default(0);
            $table->integer('J')->default(0);
            $table->integer('P')->default(0);
            $table->integer('last_ques')->default(0);
            $table->string('person')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_personalities');
    }
}
