<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFamilycalendarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('familycalendars', function (Blueprint $table) {
           $table->increments('id');
           $table->foreignId('family_id')->nullable();
           $table->foreignId('user_id')->nullable();
            $table->string('event_name');
            $table->string('event_location');
            $table->date('event_date');
            $table->string('event_time');
            $table->string('event_timezone');
            $table->text('event_description')->nullable();
            $table->string('event_per');
            $table->string('event_privacy');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('familycalendars');
    }
}
