<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountryEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country_events', function (Blueprint $table) {
            $table->id();
            $table->foreignId('country_id');
            $table->string('country_iso');
            $table->string('title_en');
            $table->string('title_ar');
            $table->string('title_fr');
            $table->string('title_tr');
            $table->string('title_ru');
            $table->string('title_de');
            $table->string('title_es');
            $table->string('title_cn');
            $table->string('type');
            $table->string('image')->nullable();
            $table->date('start_date');
            $table->date('end_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country_events');
    }
}
