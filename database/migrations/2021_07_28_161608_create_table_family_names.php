<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableFamilyNames extends Migration
{
    public function up()
    {
        Schema::create('family_names', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->integer('members')->default(0)->nullable();
            $table->integer('male')->default(0)->nullable();
            $table->integer('female')->default(0)->nullable();
            $table->integer('live')->default(0)->nullable();
            $table->integer('dead')->default(0)->nullable();
            $table->integer('married')->default(0)->nullable();
            $table->integer('divorced')->default(0)->nullable();
            $table->integer('widow')->default(0)->nullable();
            $table->text('fam_cache')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('family_names');
    }
}
