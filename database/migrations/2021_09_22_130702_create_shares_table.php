<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSharesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shares', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->integer('type');
            $table->integer('family_id');
            $table->integer('family_2')->nullable();
            $table->integer('family_m')->nullable();
            $table->integer('total')->nullable();
            $table->enum('privacy', ['public','private'])->default('public');
            $table->integer('period');
            $table->enum('unit', ['Day','Week','Month'])->default('Month');
            $table->integer('member_count')->default(0);
            $table->integer('piece')->nullable();
            $table->string('piece_name')->nullable();
            $table->date('start_pay_time')->nullable();
            $table->boolean('is_started')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shares');
    }
}
