<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserWifeTable extends Migration
{
    public function up()
    {
        Schema::create('user_wife', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable();
            $table->foreignId('wife_id')->nullable();
            $table->boolean('still')->nullable();
            $table->date('marriage_start_date')->nullable();
            $table->date('marriage_end_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('user_wife');
    }
}
