<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHelpVediosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('help_vedios', function (Blueprint $table) {
            $table->id();
            $table->integer('cat')->nullable();
            $table->string('cat_title')->nullable();
            $table->integer('sub');
            $table->string('title')->nullable();
            $table->string('video')->nullable();
            $table->string('image')->nullable();
            $table->string('lenght')->nullable();
            $table->string('lang');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('help_vedios');
    }
}
