<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalityTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personality_tests', function (Blueprint $table) {
            $table->id();
            $table->string('ques_id');
            $table->string('question');
            $table->string('ans_a');
            $table->string('ans_b');
            $table->string('val_a');
            $table->string('val_b');
            $table->string('lang');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personality_tests');
    }
}
