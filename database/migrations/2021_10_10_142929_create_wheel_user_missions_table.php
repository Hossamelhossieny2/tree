<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWheelUserMissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wheel_user_missions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('type');
            $table->integer('ques');
            $table->integer('quarter');
            $table->date('end_date');
            $table->integer('is_done')->default(0);
            $table->date('done_day')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wheel_user_missions');
    }
}
