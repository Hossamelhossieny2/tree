<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->id();
            $table->string('iso',10);
            $table->string('iso3',10)->nullable();
            $table->string('name');
            $table->string('nicename');
            $table->smallInteger('numcode')->nullable();
            $table->smallInteger('phonecode');
            $table->decimal('lat',12,8)->nullable();
            $table->decimal('lon',12,8)->nullable();
            $table->integer('value')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
