<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'هذه البيانات لا تطابق سجلاتنا',
    'password' => 'كلمة المرور المدخلة خطأ',
    'throttle' => 'محاولات دخول عديدة خاطئة . يرجي المحاولة لاحقا بعد :seconds ثانية.',

];
