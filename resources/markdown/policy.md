# Privacy Policy

At SELAA, we collect the data necessary to show you the data you want to interact with others such as birthdays, marriage dates, dates of death and other personal data and we understand that protecting your personal data is of paramount importance to us. Please take a moment to review this statement carefully.
في SELAA ، نقوم بجمع البيانات اللازمة لتظهر لك البيانات التي تريد التفاعل مع الآخرين مثل أعياد الميلاد وتواريخ الزواج وتواريخ الوفاة وغيرها من البيانات الشخصية ، ونحن نتفهم أن حماية بياناتك الشخصية ذات أهمية قصوى بالنسبة لنا. من فضلك خذ لحظة لمراجعة هذا البيان بعناية.

Why does SELAA process your personal data? لماذا تعالج SELAA بياناتك الشخصية؟
By opening a section heading below, we will explain the categories of personal data we collect and process, as well as the reasons why we do so, such as providing you with services when you visit our website, making new member additions, and using it to provide you with various information that is useful to you. You will also find information about our legal basis for processing your data and our data sources.
من خلال فتح عنوان قسم أدناه ، سنشرح فئات البيانات الشخصية التي نجمعها ونعالجها ، بالإضافة إلى أسباب قيامنا بذلك ، مثل تزويدك بالخدمات عند زيارة موقعنا على الويب ، وإجراء إضافات أعضاء جدد ، واستخدامها لتزويدك بمعلومات متنوعة مفيدة لك. ستجد أيضًا معلومات حول أساسنا القانوني لمعالجة بياناتك ومصادر بياناتنا.

DATA SHARING AND DISCLOSURES : تبادل البيانات والإفصاح عنها
Sharing personal data تلاحق البيانات الشخصية
SELAA does not sell or rent your personal information, nor does it share your personal data with any of our service providers in any way.
SELAA لا تبيع أو تؤجر معلوماتك الشخصية ، ولا تشارك بياناتك الشخصية مع أي من مزودي الخدمة لدينا بأي شكل من الأشكال.

Like most companies, SELAA uses service providers for purposes such as:
مثل معظم الشركات ، تستخدم SELAA موفري الخدمة لأغراض مثل:

providing and improving our online service platform; توفير منصة الخدمة عبر الإنترنت وتحسينها ؛
sensure your personal data is protected with appropriate safeguards in accordance with applicable privacy laws. We also use industry standard data protection measures to safeguard all international transfers of personal data through data protection agreements with our service providers.
إحساس أن بياناتك الشخصية محمية بضمانات مناسبة وفقًا لقوانين الخصوصية المعمول بها. نستخدم أيضًا معايير حماية البيانات القياسية في الصناعة لحماية جميع عمليات النقل الدولية للبيانات الشخصية من خلال اتفاقيات حماية البيانات مع مزودي الخدمة لدينا.

Personal Data Disclosures : الإفصاح عن البيانات الشخصية
We also reserve the right to disclose personal information under certain specific circumstances, including:
نحتفظ أيضًا بالحق في الكشف عن المعلومات الشخصية في ظل ظروف معينة محددة ، بما في ذلك:

When we have your express consent to do so;
When it is reasonably necessary for our legitimate interests in conducting our business, such as in the event a merger, acquisition, or sale;
To protect SELAA's legal rights and property; and
To comply with the law or law enforcement.
Otherwise, your personal information is never shared with any individual or other organization.
عندما نحصل على موافقتك الصريحة على القيام بذلك ؛
عندما يكون ذلك ضروريًا بشكل معقول لمصالحنا المشروعة في إدارة أعمالنا ، كما هو الحال في حالة الاندماج أو الاستحواذ أو البيع ؛
لحماية حقوق وممتلكات SELAA القانونية ؛ و
للامتثال للقانون أو تطبيق القانون.
خلاف ذلك ، لا يتم مشاركة معلوماتك الشخصية مع أي فرد أو منظمة أخرى.

SAFEGUARDING YOUR DATA : الحفاظ على بياناتك
SELAA uses technical and organizational safeguards to keep your data safe and secure. Where appropriate, these safeguards include measures such as anonymization or pseudonymization of personal data, strict access control, and the use of encryption to protect the data we process.
تستخدم SELAA الضمانات التقنية والتنظيمية للحفاظ على بياناتك آمنة ومأمونة. عند الاقتضاء ، تشمل هذه الضمانات تدابير مثل إخفاء الهوية أو إخفاء الهوية للبيانات الشخصية ، والتحكم الصارم في الوصول ، واستخدام التشفير لحماية البيانات التي نعالجها.

We also ensure that our staff receives adequate training to ensure personal data is processed only in accordance with our internal policies, consistent with our obligations under applicable law. We also limit access to your sensitive personal data to personnel that have specifically been granted such access.
نحن نضمن أيضًا تلقي موظفينا تدريبًا مناسبًا لضمان معالجة البيانات الشخصية فقط وفقًا لسياساتنا الداخلية ، بما يتوافق مع التزاماتنا بموجب القانون المعمول به. كما نقصر الوصول إلى بياناتك الشخصية الحساسة على الأفراد الذين تم منحهم مثل هذا الوصول على وجه التحديد.

Online services that we provide, such as the SELAA online store and SELAA on the Web, protect your personal data in-transit using encryption and other security measures. We also regularly test our service, systems, and other assets for possible security vulnerabilities.
تحمي الخدمات التي نقدمها عبر الإنترنت ، مثل متجر SELAA عبر الإنترنت و SELAA على الويب ، بياناتك الشخصية أثناء النقل باستخدام التشفير وإجراءات الأمان الأخرى. نقوم أيضًا باختبار خدماتنا وأنظمتنا والأصول الأخرى بانتظام بحثًا عن نقاط ضعف أمنية محتملة.

We update the SELAA app and the ring firmware regularly. We recommend that you make sure that you always have the latest app and firmware versions installed in order to maximize protection of your data.
نقوم بتحديث تطبيق SELAA والبرامج الثابتة على الحلقة بانتظام. نوصيك بالتأكد من تثبيت أحدث إصدارات التطبيقات والبرامج الثابتة لديك دائمًا لزيادة حماية بياناتك.

data retention : الاحتفاظ بالبيانات
The period of retention of your personal data generally depends on how active your and your family's SELAA account is. Your personal data will be deleted when it is no longer needed for the purpose for which it was originally collected, unless we have a legal obligation to keep the data for a longer period of time. For example, your personal data is stored for as long as you are registered in a particular family and the members are still active even if you are not active.
تعتمد فترة الاحتفاظ ببياناتك الشخصية بشكل عام على مدى نشاط حساب SELAA الخاص بك وعائلتك. سيتم حذف بياناتك الشخصية عندما لا تكون هناك حاجة إليها للغرض الذي تم جمعها من أجله في الأصل ، ما لم يكن لدينا التزام قانوني بالاحتفاظ بالبيانات لفترة زمنية أطول. على سبيل المثال ، يتم تخزين بياناتك الشخصية طالما أنك مسجّل في عائلة معينة ولا يزال الأعضاء نشطين حتى لو لم تكن نشطًا.

SELAA also has legal obligations to retain certain personal data for a specific period of time, such as for tax purposes. These required retention periods may include, for example, accounting and tax requirements, legal claims, or for any other legal purposes. Please note that obligatory retention periods for personal data vary based on the relevant law.
تتحمل SELAA أيضًا التزامات قانونية بالاحتفاظ ببعض البيانات الشخصية لفترة زمنية محددة ، مثل الأغراض الضريبية. قد تشمل فترات الاستبقاء المطلوبة ، على سبيل المثال ، متطلبات المحاسبة والضرائب ، والمطالبات القانونية ، أو لأي أغراض قانونية أخرى. يرجى ملاحظة أن فترات الاحتفاظ الإلزامية بالبيانات الشخصية تختلف بناءً على القانون ذي الصلة.

If you wish, you may request deletion of your SELAA account by contacting dataprotection@SELAA.social
إذا كنت ترغب في ذلك ، يمكنك طلب حذف حساب SELAA الخاص بك عن طريق الاتصال بـ dataprotection@SELAA.social

USE OF COOKIES : استخدام ملفات تعريف الارتباط
We use cookies and various other technologies to collect and store analytics and other information when customers use our site, as well as for personalization and advertising purposes. The cookies we use include both first party and third party cookies.
نحن نستخدم ملفات تعريف الارتباط والعديد من التقنيات الأخرى لجمع وتخزين التحليلات والمعلومات الأخرى عندما يستخدم العملاء موقعنا ، وكذلك لأغراض التخصيص والإعلان. تشمل ملفات تعريف الارتباط التي نستخدمها ملفات تعريف ارتباط الطرف الأول والطرف الثالث.

Cookies are small text files sent and saved on your device that allows us to identify visitors of our websites and facilitate the use of our site and to create aggregate information of our visitors. This helps us to improve our service and better serve our customers, and will not harm your device or files. We use cookies to tailor our site and the information we provide in accordance with the individual interests of our customers. Cookies are also used for tracking your browsing habits and for targeting and optimizing advertising, both on our site as well as on other sites you may visit. We also use cookies for integrating our social media accounts on our website.
ملفات تعريف الارتباط هي ملفات نصية صغيرة يتم إرسالها وحفظها على جهازك والتي تتيح لنا التعرف على زوار مواقعنا الإلكترونية وتسهيل استخدام موقعنا وإنشاء معلومات مجمعة عن زوارنا. يساعدنا هذا على تحسين خدماتنا وخدمة عملائنا بشكل أفضل ، ولن يضر جهازك أو ملفاتك. نحن نستخدم ملفات تعريف الارتباط لتصميم موقعنا والمعلومات التي نقدمها وفقًا للمصالح الفردية لعملائنا. تُستخدم ملفات تعريف الارتباط أيضًا لتتبع عادات التصفح الخاصة بك ولاستهداف الإعلانات وتحسينها ، سواء على موقعنا أو على المواقع الأخرى التي قد تزورها. نستخدم أيضًا ملفات تعريف الارتباط لدمج حسابات وسائل التواصل الاجتماعي الخاصة بنا على موقعنا.

Please see our Cookie Policy for more information on SELAA's use of cookies, and how you can set your cookie preferences.
يرجى الاطلاع على سياسة ملفات تعريف الارتباط الخاصة بنا للحصول على مزيد من المعلومات حول استخدام SELAA لملفات تعريف الارتباط ، وكيف يمكنك تعيين تفضيلات ملفات تعريف الارتباط الخاصة بك.

YOUR RIGHTS AS A DATA SUBJECT : حقوقك كموضوع بيانات
Whenever SELAA processes your data, you have certain rights that enable you to control how your personal data is being processed. This section provides you with information about each of those rights. If you wish to exercise your rights as a data subject, please contact dataprotection@SELAA.social with your request to do so.
عندما تقوم SELAA بمعالجة بياناتك ، يكون لديك بعض الحقوق التي تمكنك من التحكم في كيفية معالجة بياناتك الشخصية. يوفر لك هذا القسم معلومات حول كل من هذه الحقوق. إذا كنت ترغب في ممارسة حقوقك كصاحب بيانات ، فيرجى الاتصال بـ dataprotection@SELAA.social مع طلبك للقيام بذلك.

Right to access data : الحق في الوصول إلى البيانات
You have the right to know what personal data is processed about you. You may contact us to request access to the personal data we have collected about you, and we will confirm whether we are processing your data, and provide you with information about the personal data we have collected and processed about you.
لديك الحق في معرفة البيانات الشخصية التي تتم معالجتها عنك. يمكنك الاتصال بنا لطلب الوصول إلى البيانات الشخصية التي جمعناها عنك ، وسوف نؤكد ما إذا كنا نعالج بياناتك ، ونزودك بمعلومات حول البيانات الشخصية التي قمنا بجمعها ومعالجتها عنك.

Right to erasure : الحق في المحو
You have the right to request the deletion of your personal data in certain circumstances. We will comply with such requests unless we have a valid legal basis or legal obligation to preserve the data.
As you can delete your account by your self .
لديك الحق في طلب حذف بياناتك الشخصية في ظروف معينة. سنلتزم بهذه الطلبات ما لم يكن لدينا أساس قانوني صالح أو التزام قانوني بالحفاظ على البيانات.
كما يمكنك حذف حسابك بنفسك.

Right to rectification (of inaccurate data) : الحق في التصحيح (للبيانات غير الدقيقة)
You have the right to request correction of any incorrect or incomplete personal data we have stored about you.
يحق لك طلب تصحيح أي بيانات شخصية غير صحيحة أو غير كاملة قمنا بتخزينها عنك.

Please note that you can correct and update some of your basic information via the SELAA App(soon) and via SELAA on the Web.
يرجى ملاحظة أنه يمكنك تصحيح بعض معلوماتك الأساسية وتحديثها عبر تطبيق SELAA (قريبًا) وعبر SELAA على الويب.

Right to data portability : الحق في نقل البيانات
You have the right to request receipt of the personal data you have provided to us in a structured and commonly used format. The right to data portability only applies when we process your personal data for certain reasons, such as by contract or by your consent.
لديك الحق في طلب استلام البيانات الشخصية التي قدمتها إلينا بتنسيق منظم وشائع الاستخدام. ينطبق الحق في نقل البيانات فقط عندما نعالج بياناتك الشخصية لأسباب معينة ، مثل العقد أو بموافقتك.

Please note that SELAA on the Web provides you with the ability to export your own data.
يرجى ملاحظة أن SELAA على الويب توفر لك القدرة على تصدير بياناتك الخاصة.

Right to object to processing : الحق في الاعتراض على المعالجة
You have the right to object to the processing of your personal data under certain circumstances. In the event that we do not have legitimate grounds to continue processing such personal data, we will no longer process your personal data after we have received and verified your objection. You also have the right to object processing of your personal data for direct marketing purposes at any time.
يحق لك الاعتراض على معالجة بياناتك الشخصية في ظل ظروف معينة. في حالة عدم وجود أسباب مشروعة لمواصلة معالجة هذه البيانات الشخصية ، فلن نعالج بياناتك الشخصية بعد استلامنا اعتراضك والتحقق منه. يحق لك أيضًا الاعتراض على معالجة بياناتك الشخصية لأغراض التسويق المباشر في أي وقت.

Right to restrict processing : الحق في تقييد المعالجة
You have the right to request that we restrict processing of your personal data under certain circumstances. For example, if you contest the accuracy of your data, you can make a restriction request that we do not process your data until SELAA has verified the accuracy of your data.
لديك الحق في طلب تقييد معالجة بياناتك الشخصية في ظل ظروف معينة. على سبيل المثال ، إذا كنت تعترض على دقة بياناتك ، فيمكنك تقديم طلب تقييد بعدم قيامنا بمعالجة بياناتك حتى تتحقق SELAA من دقة بياناتك.

Right to withdraw consent : الحق في سحب الموافقة
If we have requested your consent in order to process your personal data, you have the right to withdraw your consent for such processing at any time. It should be noted, however, that withdrawing your consent may lead to issues or restrictions on your ability to fully utilize SELAA services.
إذا طلبنا موافقتك من أجل معالجة بياناتك الشخصية ، فيحق لك سحب موافقتك على هذه المعالجة في أي وقت. ومع ذلك ، تجدر الإشارة إلى أن سحب موافقتك قد يؤدي إلى مشكلات أو قيود على قدرتك على الاستفادة الكاملة من خدمات SELAA.

Please note that you can always unsubscribe from receiving our newsletter and other marketing emails by using the ‘Unsubscribe'-link provided in the emails you receive from us if we make it (later).
يرجى ملاحظة أنه يمكنك دائمًا إلغاء الاشتراك من تلقي رسائلنا الإخبارية ورسائل البريد الإلكتروني التسويقية الأخرى باستخدام رابط "إلغاء الاشتراك" الموجود في رسائل البريد الإلكتروني التي تتلقاها منا إذا قمنا بذلك (لاحقًا).

SELAA strives to address your privacy concerns. If you have contacted SELAA about your issue and are still unhappy with our response, subject to applicable law, you may contact your local supervisory authority regarding your issue. However, we urge you to first contact us at dataprotection@SELAA.social so that we can more quickly resolve your issue before escalating the issue.
تسعى SELAA جاهدة لمعالجة مخاوف الخصوصية الخاصة بك. إذا اتصلت بـ SELAA بشأن مشكلتك وما زلت غير راضٍ عن ردنا ، وفقًا للقانون المعمول به ، يمكنك الاتصال بالسلطة الإشرافية المحلية بخصوص مشكلتك. ومع ذلك ، نحثك على الاتصال بنا أولاً على dataprotection@SELAA.social حتى نتمكن من حل مشكلتك بسرعة أكبر قبل تصعيد المشكلة.

Please read SELAA's EGPC Privacy Notice if you are a resident of Egypt to read more about your rights under EGYPT law.
يرجى قراءة إشعار خصوصية الهيئة المصرية العامة للبترول (SELAA) إذا كنت مقيمًا في مصر لقراءة المزيد عن حقوقك بموجب قانون مصر.

Data Protection Officer: dataprotection@SELAA.social

CHANGES TO THIS PRIVACY NOTICE : التغييرات في إشعار الخصوصية هذا
This Privacy Statement is effective as of September 15, 2021. We reserve the right to update this Policy from time to time at our sole discretion, but if we do, we'll let you know about any material changes either by notifying you on the website or by sending you an email or push notification. If you keep using SELAA services after a change, your continued use means that you accept any such changes.
يسري بيان الخصوصية هذا اعتبارًا من 15 سبتمبر 2021. نحتفظ بالحق في تحديث هذه السياسة من وقت لآخر وفقًا لتقديرنا الخاص ، ولكن إذا فعلنا ذلك ، فسنعلمك بأي تغييرات جوهرية إما عن طريق إخطارك على موقع الويب أو عن طريق إرسال بريد إلكتروني إليك أو إشعار الدفع. إذا واصلت استخدام خدمات SELAA بعد التغيير ، فإن استمرار استخدامك يعني أنك تقبل أي تغييرات من هذا القبيل.