# Terms of Service

Last update: Sep 10, 2021
آخر تحديث : سبتمبر ١٠.٢٠٢١

Welcome and thank you for your interest in SELAA! You are reviewing these Terms of Use because you are using, interfering with or otherwise using, our Application ("SELAA Services"), which allow you to add and communicate with all of your family members on the SELAA website at SELAA.social (the "Website") .
مرحبًا بك وشكرًا على اهتمامك بـ SELAA! أنت تراجع شروط الاستخدام هذه لأنك تستخدم تطبيقنا ، أو تتداخل أو تستخدم ، ("خدمات SELAA") ، والتي تتيح لك إضافة جميع أفراد عائلتك والتواصل معهم علي موقع SELAA على SELAA.social ("الموقع الإلكتروني")

THESE TERMS OF USE ARE A LEGAL AGREEMENT BETWEEN YOU AND SELAA. PLEASE READ THEM THROUGH CAREFULLY BEFORE USING ANY SELAA SERVICES. By using any SELAA Services you signify your agreement to these Terms of Use (the “Terms of Use”). If you do not agree to these Terms of Use, you may not use the SELAA Services .
تعتبر شروط الاستخدام هذه اتفاقية قانونية بينك وبين SELAA. يرجى قراءتها بعناية قبل استخدام أي من خدمات SELAA. باستخدام أي من خدمات SELAA ، فإنك تشير إلى موافقتك على شروط الاستخدام هذه ("شروط الاستخدام"). إذا كنت لا توافق على شروط الاستخدام هذه ، فلا يجوز لك استخدام خدمات SELAA.

The SELAA Services have been developed and are owned by TRIANGLE company which is referred to as SELAA, we, us or our in these Terms of Use. You are referred to as “you” or “your” in these Terms of Use.
تم تطوير خدمات SELAA وهي مملوكة لشركة TRIANGLE والتي يشار إليها باسم SELAA أو نحن أو نحن أو لدينا في شروط الاستخدام هذه. يُشار إليك باسم "أنت" أو "ملكك" في شروط الاستخدام هذه.

SUPPORT AND QUESTIONS : الدعم والأسئلة
If you have any questions about the installation or use of the SELAA Services or have technical difficulties, you will find FAQs on our Website at SELAA.social or you may e-mail us at support@selaa.social
إذا كانت لديك أي أسئلة حول تثبيت أو استخدام خدمات SELAA أو كنت تواجه صعوبات فنية ، فستجد الأسئلة الشائعة على موقعنا على SELAA.social أو يمكنك مراسلتنا عبر البريد الإلكتروني على support@selaa.social

CHANGES TO SERVICES AND TERMS : التغييرات في الخدمات والشروط
We may change or discontinue, in whole or in part, the SELAA Services at any time without notice. You acknowledge that SELAA is not liable to you or to any third party for any such action.
يجوز لنا تغيير خدمات SELAA أو إيقافها ، كليًا أو جزئيًا ، في أي وقت دون إشعار. أنت تقر بأن SELAA ليست مسؤولة تجاهك أو تجاه أي طرف ثالث عن أي إجراء من هذا القبيل.

We may modify these Terms of Use from time to time. The most current Terms of Use will be available on the SELAA home page and the date of the latest update is indicated at the top of these Terms of Use. We may communicate major changes with a special notice on the SELAA Services or by email. You accept such modified Terms of Use by continuing to use of the SELAA Services.
يجوز لنا تعديل شروط الاستخدام هذه من وقت لآخر. ستكون أحدث شروط الاستخدام متاحة على الصفحة الرئيسية لـ SELAA ويتم الإشارة إلى تاريخ آخر تحديث في الجزء العلوي من شروط الاستخدام هذه. قد نقوم بإبلاغ التغييرات الرئيسية بإشعار خاص على خدمات SELAA أو عن طريق البريد الإلكتروني. أنت تقبل شروط الاستخدام المعدلة من خلال الاستمرار في استخدام خدمات SELAA.

REQUIREMENTS FOR USE OF THE SELAA SERVICES : متطلبات استخدام خدمات SELAA
You represent and warrant that you are aged 18 years or older and have the legal authority to accept these Terms of Use on your own behalf or any party you represent. Please also refer to the Precautions in these Terms of Use.
أنت تقر وتضمن أنك تبلغ من العمر 18 عامًا أو أكثر وأن لديك السلطة القانونية لقبول شروط الاستخدام هذه نيابةً عنك أو عن أي طرف تمثّله. يرجى أيضًا الرجوع إلى الاحتياطات الواردة في شروط الاستخدام هذه.

The full SELAA experience requires the SELAA Product, Internet access to the Mobile App(soon) and Website, and other required software (if any). Periodic updates to any of these elements may be required for improved performance, and the performance of the whole may be affected by the performance of any of these elements. You are responsible for obtaining your own Internet access and mobile device to access the SELAA Services.
تتطلب تجربة SELAA الكاملة منتج SELAA والوصول إلى الإنترنت لتطبيق الهاتف المحمول (قريبًا) والموقع الإلكتروني والبرامج الأخرى المطلوبة (إن وجدت). قد تكون التحديثات الدورية لأي من هذه العناصر مطلوبة لتحسين الأداء ، وقد يتأثر أداء الكل بأداء أي من هذه العناصر. أنت مسؤول عن الحصول على وصولك إلى الإنترنت وجهازك المحمول للوصول إلى خدمات SELAA.

PRECAUTIONS : احتياطات
Beware of excessive sitting in front of Silah services provided to you as a way of family rapprochement in an entertaining way to connect you with your extended family.
احذر من الجلوس المفرط أمام خدمات صلة المقدمة لك كوسيلة للتقارب العائلي بطريقة مسلية لربطك بأسرتك الممتدة.

Therefore, we are not responsible for any damages or health problems that may occur to you from long periods of sitting in front of the recreational services provided to you.
لذلك ، نحن غير مسؤولين عن أي أضرار أو مشاكل صحية قد تحدث لك من فترات طويلة من الجلوس أمام الخدمات الترفيهية المقدمة لك.

CORPORATE CONTACT INFO : 
معلومات الاتصال بالشركة
Mailing Address: Mostkbal city 18u, floor 4, cairo , EGYPT

Phone: +20-100-1191955 (Please note calls to this number cannot reach customer service. For questions on support, orders, returns or product information, please submit a support request to more quickly have your needs handled).
(الرجاء ملاحظة أن المكالمات على هذا الرقم لا يمكن أن تصل إلى خدمة العملاء. للأسئلة حول الدعم أو الطلبات أو المرتجعات أو معلومات المنتج ، يرجى إرسال طلب دعم لتتم معالجة احتياجاتك بسرعة أكبر).

PRIVACY POLICY : سياسة الخصوصية
The SELAA Services are subject to the SELAA Privacy Policy, which can be found at SELAA.social/privacy-policy
تخضع خدمات SELAA لسياسة خصوصية SELAA ، والتي يمكن العثور عليها في SELAA.social/privacy-policyLICY

LIMITED LICENSE AND OWNERSHIP OF THE SELAARING SERVICES : الترخيص المحدود والملكية للخدمات الذاتية
You are hereby granted a non-exclusive, revocable and nontransferable license to use the SELAA Services in accordance with these Terms of Use.
يتم منحك بموجب هذا ترخيصًا غير حصري وقابل للإلغاء وغير قابل للنقل لاستخدام خدمات SELAA وفقًا لشروط الاستخدام هذه.

SELAA or its licensors own all title and rights to the SELAA Services, including, but is not limited, to all copyrights, trademarks, know-how and other intellectual property rights included therein. You may not reproduce (whether by linking, framing or any other method), transfer, distribute, store, modify, decompile, disassemble, or create derivative works of, publicly display, or commercially exploit any part of the SELAA Services except as necessary to display, download or print (without modification) for your own non-commercial use. All rights in the SELAA Services not expressly granted to you by SELAA are retained by SELAA and its licensors. Violation of any of these Terms of Use gives SELAA sole discretion to refuse or terminate access to the SELAA Services effective immediately. In such an event you are obliged to immediately destroy any copies you have made of any portion of the SELAA Services.
تمتلك SELAA أو المرخصون التابعون لها جميع حقوق الملكية والحقوق لخدمات SELAA ، بما في ذلك ، على سبيل المثال لا الحصر ، جميع حقوق النشر والعلامات التجارية والمعرفة وحقوق الملكية الفكرية الأخرى المدرجة فيها. لا يجوز لك إعادة إنتاج (سواء عن طريق الربط أو التأطير أو أي طريقة أخرى) ، أو نقل ، أو توزيع ، أو تخزين ، أو تعديل ، أو فك ، أو تفكيك ، أو إنشاء أعمال مشتقة ، أو عرض علني ، أو استغلال تجاري لأي جزء من خدمات SELAA باستثناء ما هو ضروري العرض أو التنزيل أو الطباعة (بدون تعديل) لاستخدامك غير التجاري. تحتفظ SELAA ومرخصوها بجميع الحقوق في خدمات SELAA غير الممنوحة لك صراحةً من قبل SELAA. يمنح انتهاك أي من شروط الاستخدام هذه السلطة التقديرية لسلاع لرفض أو إنهاء الوصول إلى خدمات SELAA سارية المفعول على الفور. في مثل هذه الحالة ، أنت ملزم بإتلاف أي نسخ قمت بعملها من أي جزء من خدمات SELAA على الفور.

You may propose to SELAA modifications or improvements to all or any part of the SELAA Services. By choosing to disclose such proposal to SELAA, you hereby grant SELAA all title and ownership and intellectual property rights to such proposal.
يمكنك اقتراح تعديلات أو تحسينات على SELAA لكل أو أي جزء من خدمات SELAA. باختيار الإفصاح عن هذا الاقتراح إلى SELAA ، فإنك تمنح بموجبه SELAA جميع حقوق الملكية والملكية وحقوق الملكية الفكرية لمثل هذا الاقتراح.

INDEMNITY : تعويض
You agree to defend, indemnify and hold SELAA and its partners, affiliates, service providers, licensors, officers, directors, employees and agents harmless from and against any claims, actions or demands, including but not limited to reasonable legal and accounting fees, alleging or resulting from: (a) your violation of these Terms of Use; or (b) your violation of SELAA’s intellectual property rights, any third party rights or any applicable law when using the SELAA Services.
أنت توافق على الدفاع عن SELAA وشركائها والشركات التابعة لها ومقدمي الخدمات والمرخصين والمسؤولين والمديرين والموظفين والوكلاء وتعويضهم وحمايتهم من أي ضرر من أو ضد أي مطالبات أو إجراءات أو مطالب ، بما في ذلك على سبيل المثال لا الحصر ، الرسوم القانونية والمحاسبية المعقولة ، بدعوى أو ناتجًا عن: (أ) انتهاكك لشروط الاستخدام هذه ؛ أو (ب) انتهاكك لحقوق الملكية الفكرية لشركة SELAA أو أي حقوق لجهة خارجية أو أي قانون معمول به عند استخدام خدمات SELAA.

LOCAL LAW AND EXPORT CONTROL : القانون المحلي ومراقبة الصادرات
In relation to your purchase and use of the SELAA Services, you agree to comply with the laws of your local jurisdiction as well as with any export restrictions of the Egypt and certain other countries on products or information.
فيما يتعلق بشرائك واستخدامك لخدمات SELAA ، فإنك توافق على الامتثال لقوانين ولايتك القضائية المحلية وكذلك مع أي قيود تصدير خاصة بمصر وبعض البلدان الأخرى على المنتجات أو المعلومات.

THIRD PARTY SERVICES : خدمات الطرف الثالث
To increase value to our users, we may provide links or references to third party websites or services within the SELAA Services. We have no control of such websites or services and do not assume any responsibility or liability for any damage or loss of any kind for or due to their content, functionality, or practices. If you decide to access these third party websites or services, you do so at your own risk. SELAA suggests that before using these third party websites or services you read their terms of use and privacy policies (if any).
لزيادة القيمة لمستخدمينا ، قد نقدم روابط أو مراجع لمواقع أو خدمات طرف ثالث ضمن خدمات SELAA. ليس لدينا سيطرة على هذه المواقع أو الخدمات ولا نتحمل أي مسؤولية أو التزام عن أي ضرر أو خسارة من أي نوع بسبب أو بسبب محتواها أو وظائفها أو ممارساتها. إذا قررت الوصول إلى مواقع أو خدمات الطرف الثالث ، فإنك تقوم بذلك على مسؤوليتك الخاصة. تقترح SELAA أنه قبل استخدام مواقع الويب أو الخدمات الخاصة بهذه الجهات الخارجية ، يجب عليك قراءة شروط الاستخدام وسياسات الخصوصية الخاصة بهم (إن وجدت).

CONTENT AND WARRANTY DISCLAIMER : إخلاء المسؤولية عن المحتوى والضمان
EXCEPT FOR THE LIMITED WARRANTY FOR THE SELAA PRODUCT SET FORTH BELOW, THE SELAA SERVICES ARE PROVIDED BY SELAA AND ITS AFFILIATES “AS IS.”
باستثناء الضمان المحدود لمنتج SELAA المحدد أدناه ، يتم توفير خدمات SELAA من قبل SELAA والشركات التابعة لها "كما هي".
NEITHER SELAA NOR ITS PARTNERS, SUPPLIERS, OR AFFILIATES MAKE ANY REPRESENTATIONS OR WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED, AS TO THE OPERATION OF THE SELAA SERVICES (INCLUDING THE SELAA PRODUCTS), ITS CONTENTS, OR ANY INFORMATION MADE AVAILABLE BY OR THROUGH THE SELAA SERVICES. IN ADDITION, SELAA AND ITS PARTNERS, SUPPLIERS AND AFFILIATES DISCLAIM ALL WARRANTIES WITH RESPECT TO THE SELAA SERVICES (INCLUDING THE SELAA PRODUCTS), EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, TITLE, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. FURTHERMORE, SELAA DOES NOT WARRANT THAT USE OF THE SELAA SERVICES WILL BE UNINTERRUPTED, AVAILABLE AT ANY TIME OR FROM ANY PARTICULAR LOCATION, SECURE OR ERROR-FREE, THAT DEFECTS WILL BE CORRECTED, OR THAT THE SELAA SERVICES (INCLUDING ANY SELAA PRODUCT) ARE FREE OF VIRUSES OR OTHER POTENTIALLY HARMFUL COMPONENTS.
لا تقدم SELAA ولا شركاؤها أو موردوها أو الشركات التابعة لها أي إقرارات أو ضمانات من أي نوع ، صريحة أو ضمنية ، فيما يتعلق بتشغيل خدمات SELAA (بما في ذلك منتجات SELAA) ، أو محتوياتها ، أو أي معلومات تقدمها خدمات سيلا. بالإضافة إلى ذلك ، تخلي SELAA وشركاؤها وموردوها والشركات التابعة لها مسؤوليتها عن جميع الضمانات المتعلقة بخدمات SELAA (بما في ذلك منتجات SELAA) ، صريحة أو ضمنية ، بما في ذلك على سبيل المثال لا الحصر الضمانات الضمنية والضمانات الضمنية والضمانات عدم الانتهاك. علاوة على ذلك ، لا تضمن SELAA أن استخدام خدمات SELA سيكون بلا انقطاع أو متاحًا في أي وقت أو من أي موقع محدد أو آمن أو خالي من الأخطاء ، وسيتم تصحيح العيوب ، أو أن منتجات SELAUD هي منتجات محددة خالية من الفيروسات أو غيرها من المكونات الضارة المحتملة.

Limited and unconditional warranty from both parties, the owner of the program and the user party against programming defects and data storage, which may result in the loss or loss of your data or the deletion of your account from the site without intentional, and this may occur due to an update or emergency change of databases or codes used in the programming language .
ضمان محدود وغير مشروط من كلا الطرفين ، مالك البرنامج وطرف المستخدم ضد عيوب البرمجة وتخزين البيانات ، مما قد يؤدي إلى ضياع أو ضياع بياناتك أو حذف حسابك من الموقع دون قصد ، وهذا قد تحدث بسبب تحديث أو تغيير طارئ لقواعد البيانات أو الرموز المستخدمة في لغة البرمجة.

Limitation of Liability : تحديد المسؤولية

IN NO EVENT WILL TRIANGLE company OR ANY OF ITS AFFILIATES BE LIABLE FOR DIRECT, INDIRECT, INCIDENTAL, PUNITIVE OR CONSEQUENTIAL DAMAGES (INCLUDING WITHOUT LIMITATION THOSE RESULTING FROM LOST PROFITS, LOST DATA OR BUSINESS INTERRUPTION) ARISING OUT OF THE USE, INABILITY TO USE, OR THE RESULTS OF USE OF THE SELAA SERVICES, WHETHER SUCH DAMAGES ARE BASED ON WARRANTY, CONTRACT, TORT OR ANY OTHER LEGAL THEORY AND WHETHER OR NOT SELAA HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. THE AGGREGATE LIABILITY OF SELAA, ARISING FROM OR RELATING TO THE SELAA SERVICES (REGARDLESS OF THE FORM OF ACTION OR CLAIM, E.G. CONTRACT, WARRANTY, TORT, STRICT LIABILITY, NEGLIGENCE, OR ANY OTHER LEGAL THEORY) IS LIMITED TO $100. SELAA’S AFFILIATES, PROVIDERS AND PARTNERS HAVE NO LIABILITY WHATSOEVER ARISING FROM THE WEBSITE. In some locations applicable law may not allow certain of the limitations described above, in which case such limitations will apply to the maximum extent allowed by such applicable law.
لن تتحمل أي شركة تراي أنجل أو أي من الشركات التابعة لها بأي حال من الأحوال المسؤولية عن الأضرار المباشرة أو غير المباشرة أو العرضية أو العقابية أو التبعية (بما في ذلك على سبيل المثال لا الحصر ، تلك الناتجة عن الأرباح المفقودة أو البيانات المفقودة أو توقف العمل) أو نتائج استخدام خدمات SELAA ، سواء كانت هذه الأضرار مستندة إلى الضمان أو العقد أو الضرر أو أي نظرية قانونية أخرى وسواء تم إخطار SELAA بإمكانية حدوث مثل هذه الأضرار أم لا. إن المسؤولية الإجمالية لسلالة SELAA ، الناشئة عن خدمات SELAA أو المتعلقة بها (بغض النظر عن شكل الدعوى أو الدعوى ، على سبيل المثال ، العقد أو الضمان أو الضرر أو المسؤولية الصارمة أو الإهمال أو أي قانون قانوني آخر بالنسبة للنظرية) محدود. لا تتحمل الشركات التابعة والمزودون والشركاء لسلالة أي مسؤولية تنشأ عن الموقع الإلكتروني. في بعض المواقع ، قد لا يسمح القانون المعمول به ببعض القيود الموضحة أعلاه ، وفي هذه الحالة سيتم تطبيق هذه القيود إلى أقصى حد يسمح به هذا القانون المعمول به.

TRADEMARKS AND PATENTS : العلامات التجارية وبراءات الاختراع
SELAA logo is trademark or registered trademark of Triangle company Patented and patents pending. © SELAA 2021-2022. All rights reserved. You are not granted, by implication or otherwise, any license or right to use any marks appearing on SELAA Services. One or more patents owned by SELAA (or patents licensed from third parties) apply to the SELAA Products and to the features and services accessible via the SELAA Services maintained by SELAA.
شعار SELAA هو علامة تجارية أو علامة تجارية مسجلة لشركة Triangle المسجلة وبراءات الاختراع المعلقة. © SELAA 2021-2022. كل الحقوق محفوظة. لا يتم منحك ، بشكل ضمني أو غير ذلك ، أي ترخيص أو حق لاستخدام أي علامات تظهر في خدمات SELAA. تنطبق واحدة أو أكثر من براءات الاختراع المملوكة لشركة SELAA (أو براءات الاختراع المرخصة من جهات خارجية) على منتجات SELAA والميزات والخدمات التي يمكن الوصول إليها عبر خدمات SELAA التي تحتفظ بها SELAA.

GOVERNING LAW AND DISPUTES : القانون الحاكم والنزاعات
The United Nations Convention on Contracts for the International Sale of Goods shall not apply to these terms of Use or your purchase of any element of the SELAA Services.
لا تنطبق اتفاقية الأمم المتحدة بشأن عقود البيع الدولي للبضائع على شروط الاستخدام هذه أو شرائك لأي عنصر من خدمات SELAA.

If you are a resident of the Arab Republic of Egypt, these Terms of Use are governed by and construed in accordance with the laws of the Arab Republic of Egypt, without regard to principles of conflict of laws in any jurisdiction. If you reside in any other country, these Terms of Use shall be governed by and construed in accordance with the laws of the United States of America, without regard to principles of conflict of laws in any jurisdiction.
إذا كنت مقيمًا في جمهورية مصر العربية ، فإن شروط الاستخدام هذه تحكمها وتفسر وفقًا لقوانين جمهورية مصر العربية ، بغض النظر عن مبادئ تنازع القوانين في أي ولاية قضائية. إذا كنت تقيم في أي بلد آخر ، فإن شروط الاستخدام هذه تخضع لقوانين الولايات المتحدة الأمريكية وتفسر وفقًا لها ، بغض النظر عن مبادئ تنازع القوانين في أي ولاية قضائية.

The Parties shall use their best efforts to settle any disputes that may arise between them through friendly negotiations and which arise out of or in connection with these Terms of Use; the existence, validity, termination and interpretation of any term of this Agreement; and disputes related to your use of the SELAA Services. If the parties fail to reach an amicable settlement, either party may refer such dispute to binding arbitration. If you are a resident of the Arab Republic of Egypt, (a) the arbitration will be conducted in accordance with the International Arbitration Rules of the American Arbitration Association (“AAA”) in effect at the time of the arbitration (“AAA Arbitration Rules”); (b) the International Arbitration Association shall be the appointing authority and responsible for administering any arbitration under this Agreement in accordance with the Arbitration Rules of the International Arbitration Association;
يبذل الطرفان قصارى جهدهما لتسوية أي نزاعات قد تنشأ بينهما من خلال مفاوضات ودية والتي تنشأ عن أو فيما يتعلق بشروط الاستخدام هذه ؛ وجود وصلاحية وإنهاء وتفسير أي بند من بنود هذه الاتفاقية ؛ والنزاعات المتعلقة باستخدامك لخدمات SELAA. إذا فشل الطرفان في التوصل إلى تسوية ودية ، يجوز لأي من الطرفين إحالة هذا النزاع إلى التحكيم الملزم. إذا كنت مقيمًا في جمهورية مصر العربية ، (أ) سيتم إجراء التحكيم وفقًا لقواعد التحكيم الدولية لجمعية التحكيم الأمريكية ("AAA") السارية وقت التحكيم ("قواعد التحكيم AAA" ") ؛ (ب) يجب أن تكون مؤسسة التحكيم الدولية هي سلطة التعيين والمسؤولة عن إدارة أي تحكيم بموجب هذه الاتفاقية وفقًا لقواعد التحكيم الخاصة برابطة التحكيم الدولية ؛

If you reside in any other country or territory, (1) the arbitration will be held in accordance with the Arbitration Rules of the International Chamber of Commerce of Finland (“ICC”) in effect at the time of the arbitration (the “ICC Arbitration Rules”); (2) the ICC shall be the appointing authority and responsible for administering any arbitration hereunder in accordance with the ICC Arbitration Rules; and (3) the place of arbitration shall be in Helsinki, Finland.
إذا كنت تقيم في أي بلد أو إقليم آخر ، (1) سيتم إجراء التحكيم وفقًا لقواعد التحكيم الخاصة بغرفة التجارة الدولية لفنلندا ("ICC") السارية وقت التحكيم ("تحكيم غرفة التجارة الدولية" قواعد")؛ (2) يجب أن تكون المحكمة الجنائية الدولية هي سلطة التعيين والمسؤولة عن إدارة أي تحكيم بموجب هذه الاتفاقية وفقًا لقواعد التحكيم الخاصة بغرفة التجارة الدولية ؛ و (3) يجب أن يكون مكان التحكيم في هلسنكي ، فنلندا.

In either case, the arbitration shall be conducted by a single arbitrator who shall be a professional, legal or otherwise, but shall not be, or have previously been associated with either party (the “Arbitrator”). The arbitral award shall be final, binding and non-appealable. The Arbitrator’s award must be reasoned and issued in writing within thirty (30) days of the hearing, unless otherwise agreed to by SELAA and you.
في كلتا الحالتين ، يجب إجراء التحكيم بواسطة محكم واحد يجب أن يكون محترفًا أو قانونيًا أو غير ذلك ، ولكن لا يجوز أن يكون أو كان مرتبطًا سابقًا بأي من الطرفين ("المحكم"). يجب أن يكون قرار التحكيم نهائيًا وملزمًا وغير قابل للاستئناف. يجب أن يكون قرار المحكم مسببًا ويتم إصداره كتابيًا في غضون ثلاثين (30) يومًا من جلسة الاستماع ، ما لم يتم الاتفاق على خلاف ذلك من قبل SELAA وأنت.

Notwithstanding the foregoing, in recognition of the irreparable harm that a breach by you of SELAA’s intellectual property rights would cause, SELAA may seek an injunction against such violation or breach in a court of competent jurisdiction.
على الرغم مما سبق ذكره ، واعترافاً بالضرر الذي لا يمكن إصلاحه والذي قد يتسبب فيه خرقك لحقوق الملكية الفكرية لشركة SELAA ، قد تطلب SELAA أمرًا قضائيًا ضد هذا الانتهاك أو الانتهاك في محكمة ذات اختصاص قضائي.

GENERAL : عام
Arabic language shall govern all documents, notices, and interpretations of these Terms of Use. Sections titled Precautions, Indemnity, Content and Warranty Disclaimer, Limitation of Liability and Governing Law and Disputes shall survive and remain in effect after your license to use the SELAA Services has terminated for any or no reason.
تحكم اللغة العربية جميع المستندات والإشعارات والتفسيرات الخاصة بشروط الاستخدام هذه. تظل الأقسام المعنونة الاحتياطات والتعويض وإخلاء المسؤولية عن المحتوى والضمان وحدود المسؤولية والقانون الحاكم والنزاعات سارية المفعول بعد إنهاء ترخيصك لاستخدام خدمات SELAA لأي سبب أو بدون سبب.

SELAA’s failure to exercise or enforce any right or provision of these Terms of Use will not constitute a waiver of such right or provision unless acknowledged and agreed to by SELAA in writing. These Terms of Use constitute the entire agreement between you and SELAA with respect to the subject matter herein and supersede any and all prior or contemporaneous oral or written agreements. You may not assign this agreement to any other party and any attempt to do so is void.
لن يشكل إخفاق SELAA في ممارسة أو إنفاذ أي حق أو حكم من شروط الاستخدام هذه تنازلاً عن هذا الحق أو الحكم ما لم يتم الاعتراف به والموافقة عليه من قبل SELAA كتابةً. تشكل شروط الاستخدام هذه الاتفاقية الكاملة بينك وبين SELAA فيما يتعلق بالموضوع الوارد هنا وتحل محل أي وجميع الاتفاقيات الشفوية أو المكتوبة السابقة أو المعاصرة. لا يجوز لك التنازل عن هذه الاتفاقية لأي طرف آخر وأي محاولة للقيام بذلك تعتبر باطلة.

TRIANGLE for it soulution ,Mostakbal city , EGYPT

SELAA's country of origin is Egypt. This could mean that your payment for any subsequent service could be subject to a local transaction if a credit card was used. Please check more information with the bank that issued your card.
بلد منشأ صلة هو مصر. قد يعني هذا أن دفعتك مقابل أي خدمة لاحقة يمكن أن تخضع لمعاملة محلية إذا تم استخدام بطاقة ائتمان. يرجى التحقق من مزيد من المعلومات مع البنك الذي أصدر بطاقتك.


©2021 TRIANGLE for it soulution All Rights Reserved.
SELAA are trademarks of TRIANGLE for it soulution and may not be used without permission.
SELAA هي علامات تجارية لشركة TRIANGLE لذلك لا يجوز استخدامها بدون إذن.
