<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <livewire:family-statistics  />
            </div>
        </div>
    </div>

    @push('scripts2')
    <!-- JS-libs and init for charts -->
        <script src="{{ asset('js/libs/moment.min.js') }}"></script>
        <script src="{{ asset('js/libs/jquery.appear.min.js') }}"></script>
        <script src="{{ asset('js/libs/Chart.min.js') }}"></script>
        <script src="{{ asset('js/libs/chartjs-plugin-deferred.min.js') }}"></script>
        <script src="{{ asset('js/libs/circle-progress.min.js') }}"></script>
        <script src="{{ asset('js/libs/loader.min.js') }}"></script>
        <script src="{{ asset('js/libs/run-chart.min.js') }}"></script>
    <!-- ... end JS-libs and init for charts -->
    

        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.7/raphael.min.js" charset="utf-8"></script>
        <script src="{{ asset('js/map/jquery.mapael.js') }}" charset="utf-8"></script>
        <script src="{{ asset('js/map/world_countries.js') }}" charset="utf-8"></script>

    @endpush
</x-app-layout>
