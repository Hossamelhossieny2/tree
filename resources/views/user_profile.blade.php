<article class="hentry post has-post-thumbnail thumb-full-width">
                  
                    <div class="post__author author vcard inline-items">
                        <img loading="lazy" src="{{ asset('img/default/system.png') }}" width="36" height="36" alt="author">

                        <div class="author-date">
                            <a class="h6 post__author-name fn" href="#">see</a> user
                            <a href="#">profile</a>
                            <div class="post__date">
                                <time class="published" id="update_time" datetime="2017-03-24T18:18">
                                    12 hours ago
                                </time>
                            </div>
                        </div>
                    </div>
                    <div class="post-thumb">
                        
                        <img loading="lazy" src="{{ asset('img/default/profile.png') }}" alt="photo" width="771" height="100" style="height:50px">
                       

                        <h2 class="h1 post-title" id="user_name">
                            {{ $this_user->name }} {{ (isset($this_user->father->name)) ? $this_user->father->name : " " }} {{ (isset($this_user->family->title)) ? $this_user->family->title : " " }}
                            </h2>
                        <div class="overlay"></div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-2 col-md-2">
                                <a href="#">{{ __('living_country') }} :</a>
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <img src="{{ asset('img/flag/'.strtolower($this_user->country->iso).'.svg') }}" width="16" id="user_flag">
                                 <span id="user_country">{{ $this_user->country->nicename }}</span>
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <a href="#">{{ __('birthday') }} :</a>
                            </div>
                            <div class="col-lg-2 col-md-2">
                                @if(app()->getLocale() == 'ar')
                                <span id="user_birth">{{ arabic_w2e(\Carbon\Carbon::parse($this_user->birth_date)->format('Y/m/d'))  }}</span>
                                @else
                                <span id="user_birth">{{ \Carbon\Carbon::parse($this_user->birth_date)->format('Y/m/d') ?? __('not_specified') }}</span>
                                @endif
                                <br>
                                <span id="user_birth_place">{{ $this_user->profile->birth_place ?? __('not_specified') }}</span>
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <a href="#">{{ __('profession') }} :</a>
                                
    
                                <?php 
                                if(!empty($this_user->profile->profession)){

                                    switch ($this_user->profile->profession)
                                        {
                                            case 'Agriculture':
                                            $profession =  __('Agriculture') ;
                                            break;
                                        case 'Architecture':
                                            $profession =  __('Architecture') ;
                                            break;
                                        case 'Arts':
                                            $profession =  __('Arts') ;
                                            break;
                                        case 'Business':
                                            $profession =  __('Business') ;
                                            break;
                                        case 'Education':
                                            $profession =  __('Education') ;
                                            break;
                                        case 'Government':
                                            $profession =  __('Government') ;
                                            break;
                                        case 'Health':
                                            $profession =  __('Health') ;
                                            break;
                                        case 'Information':
                                            $profession =  __('Information') ;
                                            break;
                                        case 'Law':
                                            $profession =  __('Law') ;
                                            break;
                                        case 'Marketing':
                                            $profession =  __('Marketing') ;
                                            break;
                                        case 'Science':
                                            $profession =  __('Science') ;
                                            break;
                                        case 'without':
                                            $profession =  __('without') ;
                                            break;
                                        case 'null':
                                            $profession =  __('not_specified') ;
                                            break;
                                        }

                                    }else{
                                        $profession =  __('not_specified') ;
                                    }
                                ?>
                                <span id="user_profission">{{ $profession }}</span>
                            </div>
                            <div class="col-lg-2 col-md-2">
                                @if($this_user->profile_photo_path)
                                <img id="user_pic" loading="lazy" src="{{ asset($this_user->profile_photo_path)}}" alt="Hossam" width="124" height="124" style="height:100px;width:100px">
                                @else
                                <img id="user_pic" loading="lazy" src="{{ asset('img/default/user.png')}}" alt="Hossam" width="60" height="60" style="height:60px;width:60px">
                                @endif
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-2 col-md-2">
                                <a href="#">{{ __('mobile_number') }} :</a>
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <span id="user_phone">{{ ($this_user->phone_number) ? $this_user->country->phonecode.substr($this_user->phone_number,1) : __('not_specified')}}</span>
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <a href="#">{{ __('gender') }} :</a> <span id="user_gender">{{ $this_user->gender }}</span>
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <a href="#">{{ __('status') }} :</a> <span id="user_status">{{ (count($this_user->wives)>0 || count($this_user->husbands)>0 ) ? __('married') : __('single')  }}</span>
                            </div>
                            <div class="col-lg-2 col-md-2" id="user_religion">{{ (!empty($this_user->profile->religion)) ? __($this_user->profile->religion) : __('not_specified') }}
                            </div>
                            <div class="col-lg-2 col-md-2" id="user_plotical">{{ (!empty($this_user->profile->politic)) ? __($this_user->profile->politic) : __('not_specified') }}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-2 col-md-2">
                                <a href="#">{{ __('about_me2') }} :</a>
                            </div>
                            <div class="col-lg-10 col-md-10" id="user_bio">{{ $this_user->profile->bio ?? __('not_specified') }}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-2 col-md-2">
                                <a href="#">{{ __('email') }} :</a>
                            </div>
                            <div class="col-lg-4 col-md-4" id="user_email">{{ $this_user->email ?? __('not_specified') }}
                            </div>
                            
                            <div class="col-lg-2 col-md-2">
                                <a href="#">{{ __('website') }} :</a>
                            </div>
                            <div class="col-lg-4 col-md-4" id="user_website">{{ $this_user->profile->website ?? __('not_specified') }}
                            </div>

                            
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-2 col-md-2">
                                <a href="#">{{ __('facebook') }} :</a>
                            </div>
                            <div class="col-lg-4 col-md-4" id="user_facebook">
                                @if($this_user->profile->facebook)
                                    https://www.facebook.com/{{ $this_user->profile->facebook}}
                                @else
                                    {{ __('not_specified') }}
                                @endif
                            </div>

                            <div class="col-lg-2 col-md-2">
                                <a href="#">{{ __('youtube') }} :</a>
                            </div>
                            <div class="col-lg-4 col-md-4" id="user_youtube">
                                 @if($this_user->profile->youtube)
                                    https://www.youtube.com/user/{{ $this_user->profile->youtube}}/featured
                                @else
                                    {{ __('not_specified') }}
                                @endif
                            </div>

                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-2 col-md-2">
                                <a href="#">{{ __('instgram') }} :</a>
                            </div>
                            <div class="col-lg-4 col-md-4" id="user_instagram">
                                @if($this_user->profile->instagram)
                                    https://www.instagram.com/{{ $this_user->profile->instagram}}/
                                @else
                                    {{ __('not_specified') }}
                                @endif
                            </div>

                            <div class="col-lg-2 col-md-2">
                                <a href="#">{{ __('ticktock') }} :</a>
                            </div>
                            <div class="col-lg-4 col-md-4" id="user_tiktok">
                                @if($this_user->profile->ticktok)
                                    https://www.tiktok.com/{{ $this_user->profile->ticktok}}/
                                @else
                                    {{ __('not_specified') }}
                                @endif
                            </div>
                        </div>
                        <hr>
                        <div class="row">

                            <div class="col-lg-2 col-md-2">
                                <a href="#">{{ __('twitter') }} :</a>
                            </div>
                            <div class="col-lg-4 col-md-4" id="user_tiktok">{{ $this_user->profile->twitter ?? __('not_specified') }}
                            </div>

                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-2 col-md-2">
                                <a href="#">{{ __('hobbies') }} :</a>
                            </div>
                            <div class="col-lg-4 col-md-4" id="user_hoppies">{{ $this_user->profile->hobby ?? __('not_specified') }}
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <a href="#">{{ __('favourite_music_bands') }} :</a>
                            </div>
                            <div class="col-lg-4 col-md-4" id="user_music">{{ $this_user->profile->music ?? __('not_specified') }}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-2 col-md-2">
                                <a href="#">{{ __('favourite_tv_shows') }} :</a>
                            </div>
                            <div class="col-lg-4 col-md-4" id="user_tv">{{ $this_user->profile->tv ?? __('not_specified') }}
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <a href="#">{{ __('favourite_books') }} :</a>
                            </div>
                            <div class="col-lg-4 col-md-4" id="user_books">{{ $this_user->profile->books ?? __('not_specified') }}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-2 col-md-2">
                                <a href="#">{{ __('favourite_movies') }} :</a>
                            </div>
                            <div class="col-lg-4 col-md-4" id="user_movies">{{ $this_user->profile->movies ?? __('not_specified') }}
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <a href="#">{{ __('favourite_writers') }} :</a>
                            </div>
                            <div class="col-lg-4 col-md-4" id="user_writer">{{ $this_user->profile->writer ?? __('not_specified') }}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-2 col-md-2">
                                <a href="#">{{ __('favourite_games') }} :</a>
                            </div>
                            <div class="col-lg-4 col-md-4" id="user_games">{{ $this_user->profile->games ?? __('not_specified') }}
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <a href="#">{{ __('other_interests') }} :</a>
                            </div>
                            <div class="col-lg-4 col-md-4" id="user_other">{{ $this_user->profile->other ?? __('not_specified') }}
                            </div>
                        </div>
                        <hr>
                            <a href="#">{{ __('education_and_traning') }}</a>
                        <div id="user_edu">
                                @foreach($this_user->study as $study)
                                    @if($study->is_job == 0)
                                        <div class="row">
                                            <div class="col-lg-1 col-md-1">
                                                --
                                            </div>
                                            <div class="col-lg-3 col-md-3">
                                                {{$study->place}}
                                            </div>
                                            <div class="col-lg-2 col-md-2">
                                                {{$study->start.' : '.$study->end}}
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                {{$study->specialty}}
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                        </div>
                        <hr>
                        <a href="#">{{ __('employement_and_job') }}</a>
                            
                        <div id="user_job">
                            @foreach($this_user->study as $study)
                                    @if($study->is_job == 1)
                                        <div class="row">
                                            <div class="col-lg-1 col-md-1">
                                                --
                                            </div>
                                            <div class="col-lg-3 col-md-3">
                                                {{$study->place}}
                                            </div>
                                            <div class="col-lg-2 col-md-2">
                                                {{$study->start.' : '.$study->end}}
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                {{$study->specialty}}
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                        </div>

                    </div>

                </article>