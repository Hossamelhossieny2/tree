
<x-app-layout>

<!-- Top Header-Profile -->

<div class="container">
    <div class="row">
        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="ui-block">
                <div class="top-header">
                    <div class="top-header-thumb">

                    </div>
                    <div class="profile-section">
                        
                        <x-profile_links  :act="2"/>

                        <div class="control-block-button">
                            <a href="#" class="btn btn-control bg-blue">
                                <svg class="olymp-happy-face-icon"><use xlink:href="#olymp-happy-face-icon"></use></svg>
                            </a>

                            <a href="#" class="btn btn-control bg-purple">
                                <svg class="olymp-chat---messages-icon"><use xlink:href="#olymp-chat---messages-icon"></use></svg>
                            </a>

                            <div class="btn btn-control bg-primary more">
                                <svg class="olymp-settings-icon"><use xlink:href="#olymp-settings-icon"></use></svg>

                                <ul class="more-dropdown more-with-triangle triangle-bottom-right">
                                    <!-- <li>
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#update-header-photo">Update Profile Photo</a>
                                    </li>
                                    <li>
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#update-header-photo">Update Header Photo</a>
                                    </li> -->
                                    <li>
                                        <a href="#">{{ __('account_setting') }}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <x-top_header_profile />
                    
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ... end Top Header-Profile -->


<!-- Your Account Personal Information -->

<div class="container">
    <div class="row">
        <div class="col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">
            <div class="ui-block">
                <div class="ui-block-title">
                    <h6 class="title">{{ __('account_setting') }}</h6>
                </div>
                <div class="ui-block-content">

                    
                    <!-- Personal Account Settings Form -->
                    
                    
                        <div class="container">
                        <div id="profile_section" class="row">
                            
                            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                @if (Laravel\Fortify\Features::canUpdateProfileInformation())
                                    @livewire('profile.update-profile-information-form')

                                    <x-jet-section-border />
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div id="password_section" class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                @if (Laravel\Fortify\Features::enabled(Laravel\Fortify\Features::updatePasswords()))
                                    <div class="mt-10 sm:mt-0">
                                        @livewire('profile.update-password-form')
                                    </div>

                                    <x-jet-section-border />
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div id="twofactor_section" class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                @if (Laravel\Fortify\Features::canManageTwoFactorAuthentication())
                                    <div class="mt-10 sm:mt-0">
                                        @livewire('profile.two-factor-authentication-form')
                                    </div>

                                    <x-jet-section-border />
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div id="session_section" class="mt-10 sm:mt-0">
                                    @livewire('profile.logout-other-browser-sessions-form')
                                </div>

                                @if (Laravel\Jetstream\Jetstream::hasAccountDeletionFeatures())
                                    <x-jet-section-border />

                                    <div class="mt-10 sm:mt-0">
                                        @livewire('profile.delete-user-form')
                                    </div>
                                @endif
                            </div>

                        </div>
                        </div>
                    
                    
                    <!-- ... end Personal Account Settings Form  -->

                </div>
            </div>
        </div>

        <div class="col col-xl-3 order-xl-1 col-lg-3 order-lg-1 col-md-12 order-md-2 col-sm-12 col-12 responsive-display-none">
            <div class="ui-block">

                <!-- Your Profile  -->
                
                <div class="your-profile">
                    <div class="ui-block-title ui-block-title-small">
                        <h6 class="title">{{ __('edit_profile') }}</h6>
                    </div>

                    <div class="accordion" id="accordionExample">
                        <div class="accordion-item">
                            <h6 class="accordion-header" id="headingOne">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    {{ __('account_setting') }}
                                    <svg class="olymp-dropdown-arrow-icon"><use xlink:href="#olymp-dropdown-arrow-icon"></use></svg>
                                </button>
                            </h6>
                            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <ul class="your-profile-menu">
                                        <li>
                                            <a href="#profile_section">{{ __('account_setting') }}</a>
                                        </li>
                                        <li>
                                            <a href="#password_section">{{ __('change_password') }}</a>
                                        </li>
                                        <li>
                                            <a href="#session_section" class="active">{{ __('browser_sessions') }}</a>
                                        </li>
                                        <li>
                                            <a href="#twofactor_section">{{ __('two_factor_auth') }}</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                
                </div>
                
                <!-- ... end Your Profile  -->

            </div>
        </div>
    </div>
</div>

<!-- ... end Your Account Personal Information -->


<!-- Window-popup Update Header Photo -->

<div class="modal fade" id="update-header-photo" tabindex="-1" role="dialog" aria-labelledby="update-header-photo" aria-hidden="true">
    <div class="modal-dialog window-popup update-header-photo" role="document">
        <div class="modal-content">
            <a href="#" class="close icon-close" data-bs-dismiss="modal" aria-label="Close">
                <svg class="olymp-close-icon"><use xlink:href="#olymp-close-icon"></use></svg>
            </a>

            <div class="modal-header">
                <h6 class="title">{{ __('update_header_photo') }}</h6>
            </div>

            <div class="modal-body">
                <a href="#" class="upload-photo-item">
                <svg class="olymp-computer-icon"><use xlink:href="#olymp-computer-icon"></use></svg>

                <h6>{{ __('upload_photo') }}</h6>
                <span>{{ __('browse_photo') }}.</span>
            </a>

                <a href="#" class="upload-photo-item" data-bs-toggle="modal" data-bs-target="#choose-from-my-photo">

            <svg class="olymp-photos-icon"><use xlink:href="#olymp-photos-icon"></use></svg>

            <h6>{{ __('choose_photo') }}</h6>
            <span>{{ __('uploaded_photo') }}</span>
        </a>
            </div>
        </div>
    </div>
</div>


<!-- ... end Window-popup Update Header Photo -->

<!-- Window-popup Choose from my Photo -->

<div class="modal fade" id="choose-from-my-photo" tabindex="-1" role="dialog" aria-labelledby="choose-from-my-photo" aria-hidden="true">
    <div class="modal-dialog window-popup choose-from-my-photo" role="document">

        <div class="modal-content">
            <a href="#" class="close icon-close" data-bs-dismiss="modal" aria-label="Close">
                <svg class="olymp-close-icon"><use xlink:href="#olymp-close-icon"></use></svg>
            </a>
            <div class="modal-header">
                <h6 class="title">{{ __('choose_photo') }}</h6>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-bs-toggle="tab" href="#home" role="tab" aria-expanded="true">
                            <svg class="olymp-photos-icon"><use xlink:href="#olymp-photos-icon"></use></svg>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="tab" href="#profile" role="tab" aria-expanded="false">
                            <svg class="olymp-albums-icon"><use xlink:href="#olymp-albums-icon"></use></svg>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="modal-body">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane fade active show" id="home" role="tabpanel" aria-expanded="true">

                        <div class="choose-photo-item">
                            <div class="radio">
                                <label class="custom-radio">
                                    <img loading="lazy" src="img/choose-photo1.webp" alt="photo" width="247" height="166">
                                    <input type="radio" name="optionsRadios">
                                </label>
                            </div>
                        </div>
                        <div class="choose-photo-item">
                            <div class="radio">
                                <label class="custom-radio">
                                    <img loading="lazy" src="img/choose-photo2.webp" alt="photo" width="247" height="166">
                                    <input type="radio" name="optionsRadios">
                                </label>
                            </div>
                        </div>
                        <div class="choose-photo-item">
                            <div class="radio">
                                <label class="custom-radio">
                                    <img loading="lazy" src="img/choose-photo3.webp" alt="photo" width="247" height="166">
                                    <input type="radio" name="optionsRadios">
                                </label>
                            </div>
                        </div>

                        <div class="choose-photo-item">
                            <div class="radio">
                                <label class="custom-radio">
                                    <img loading="lazy" src="img/choose-photo4.webp" alt="photo" width="247" height="166">
                                    <input type="radio" name="optionsRadios">
                                </label>
                            </div>
                        </div>
                        <div class="choose-photo-item">
                            <div class="radio">
                                <label class="custom-radio">
                                    <img loading="lazy" src="img/choose-photo5.webp" alt="photo" width="247" height="166">
                                    <input type="radio" name="optionsRadios">
                                </label>
                            </div>
                        </div>
                        <div class="choose-photo-item">
                            <div class="radio">
                                <label class="custom-radio">
                                    <img loading="lazy" src="img/choose-photo6.webp" alt="photo" width="247" height="166">
                                    <input type="radio" name="optionsRadios">
                                </label>
                            </div>
                        </div>

                        <div class="choose-photo-item">
                            <div class="radio">
                                <label class="custom-radio">
                                    <img loading="lazy" src="img/choose-photo7.webp" alt="photo" width="247" height="166">
                                    <input type="radio" name="optionsRadios">
                                </label>
                            </div>
                        </div>
                        <div class="choose-photo-item">
                            <div class="radio">
                                <label class="custom-radio">
                                    <img loading="lazy" src="img/choose-photo8.webp" alt="photo" width="247" height="166">
                                    <input type="radio" name="optionsRadios">
                                </label>
                            </div>
                        </div>
                        <div class="choose-photo-item">
                            <div class="radio">
                                <label class="custom-radio">
                                    <img loading="lazy" src="img/choose-photo9.webp" alt="photo" width="247" height="166">
                                    <input type="radio" name="optionsRadios">
                                </label>
                            </div>
                        </div>


                        <a href="#" class="btn btn-secondary btn-lg btn--half-width">Cancel</a>
                        <a href="#" class="btn btn-primary btn-lg btn--half-width">Confirm Photo</a>

                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-expanded="false">

                        <div class="choose-photo-item">
                            <figure>
                                <img loading="lazy" src="img/choose-photo10.webp" alt="photo" width="225" height="180">
                                <figcaption>
                                    <a href="#">South America Vacations</a>
                                    <span>Last Added: 2 hours ago</span>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="choose-photo-item">
                            <figure>
                                <img loading="lazy" src="img/choose-photo11.webp" alt="photo" width="225" height="180">
                                <figcaption>
                                    <a href="#">Photoshoot Summer 2016</a>
                                    <span>Last Added: 5 weeks ago</span>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="choose-photo-item">
                            <figure>
                                <img loading="lazy" src="img/choose-photo12.webp" alt="photo" width="225" height="180">
                                <figcaption>
                                    <a href="#">Amazing Street Food</a>
                                    <span>Last Added: 6 mins ago</span>
                                </figcaption>
                            </figure>
                        </div>

                        <div class="choose-photo-item">
                            <figure>
                                <img loading="lazy" src="img/choose-photo13.webp" alt="photo" width="224" height="179">
                                <figcaption>
                                    <a href="#">Graffity & Street Art</a>
                                    <span>Last Added: 16 hours ago</span>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="choose-photo-item">
                            <figure>
                                <img loading="lazy" src="img/choose-photo14.webp" alt="photo" width="225" height="180">
                                <figcaption>
                                    <a href="#">Amazing Landscapes</a>
                                    <span>Last Added: 13 mins ago</span>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="choose-photo-item">
                            <figure>
                                <img loading="lazy" src="img/choose-photo15.webp" alt="photo" width="225" height="180">
                                <figcaption>
                                    <a href="#">The Majestic Canyon</a>
                                    <span>Last Added: 57 mins ago</span>
                                </figcaption>
                            </figure>
                        </div>


                        <a href="#" class="btn btn-secondary btn-lg btn--half-width">Cancel</a>
                        <a href="#" class="btn btn-primary btn-lg disabled btn--half-width">Confirm Photo</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- ... end Window-popup Choose from my Photo -->


<a class="back-to-top" href="#">
    <svg class="back-icon" width="14" height="18"><use xlink:href="#olymp-back-to-top"></use></svg>
</a>


</x-app-layout>
