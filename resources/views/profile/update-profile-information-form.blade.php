<x-jet-form-section submit="updateProfileInformation">
    <x-slot name="title">
        {{ __('profile_information') }}
    </x-slot>

    <x-slot name="description">
        {{ __('update_acc_info') }}
    </x-slot>

    <x-jet-validation-errors class="mb-4" />

    <x-slot name="form">
        <!-- Profile Photo -->
        @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
            <div x-data="{photoName: null, photoPreview: null}" class="col-span-6 sm:col-span-4">
                <!-- Profile Photo File Input -->
                <input type="file" class="hidden"
                            wire:model="photo"
                            x-ref="photo"
                            x-on:change="
                                    photoName = $refs.photo.files[0].name;
                                    const reader = new FileReader();
                                    reader.onload = (e) => {
                                        photoPreview = e.target.result;
                                    };
                                    reader.readAsDataURL($refs.photo.files[0]);
                            " />

                <x-jet-label for="photo" value="{{ __('photo') }}" />

                <!-- Current Profile Photo -->
                <div class="mt-2" x-show="! photoPreview">
                    @if($this->user->profile_photo_path)
                    <img src="{{ asset($this->user->profile_photo_path) }}" alt="{{ $this->user->name }}" class="rounded-full h-20 w-20 object-cover">
                    @else
                    <img src="{{ asset('img/default/user.png') }}" alt="{{ $this->user->name }}" class="rounded-full h-20 w-20 object-cover">
                    @endif
                </div>

                <!-- New Profile Photo Preview -->
                <div class="mt-2" x-show="photoPreview">
                    <span class="block rounded-full w-20 h-20"
                          x-bind:style="'background-size: cover; background-repeat: no-repeat; background-position: center center; background-image: url(\'' + photoPreview + '\');'">
                    </span>
                </div>

                <x-jet-secondary-button class="mt-2 mr-2" type="button" x-on:click.prevent="$refs.photo.click()">
                    {{ __('select_new_photo') }}
                </x-jet-secondary-button>

                @if ($this->user->profile_photo_path)
                    <x-jet-secondary-button type="button" class="mt-2" wire:click="deleteProfilePhoto">
                        {{ __('remove_photo') }}
                    </x-jet-secondary-button>
                @endif

                <x-jet-input-error for="photo" class="mt-2" />
            </div>
        @endif
        
            <div class="col-span-6 sm:col-span-4">
                <!-- Name -->
                <div class="form-group label-floating">
                    <label for="name" class="control-label">{{ __('name') }}</label>
                    <input id="name" class="form-control" placeholder="" type="text" value="{{ Auth::user()->name }}" wire:model.defer="state.name" autocomplete="name">
                    <x-jet-input-error for="name" class="mt-2" />

                </div>

                <!-- Email -->
                <div class="form-group label-floating">
                    <label for="email" class="control-label">{{ __('email') }}</label>
                    <input id="email" class="form-control" placeholder="" type="email" value="{{ Auth::user()->email }}" wire:model.defer="state.email">
                </div>
                <x-jet-input-error for="email" class="mt-2" />
            </div>

            <!-- Phone number -->
            <div class="col-span-6 sm:col-span-4">
                <div class="form-group label-floating is-empty">
                    <label for="Phone_number" class="control-label">{{ __('phone_number') }}</label>
                    <input id="Phone_number" class="form-control" placeholder="" value="{{ Auth::user()->Phone_number }}" type="text" wire:model.defer="state.phone_number">
                    <x-jet-input-error for="Phone_number" class="mt-2" />
                </div>
            </div>
            <?php $countries = \App\Models\Country::all();?>
            <div class="col-span-6 sm:col-span-4">
                <div class="form-group label-floating is-select">
                    <label class="control-label">{{ __('living_country') }}</label>
                    <select wire:model.defer="state.country_id" class="form-control">
                     <option value="">{{ __('select_living_country') }} .. </option>
                        @foreach($countries as $country)
                        <option value="{{ $country->id }}" @if(auth()->user()->country_id == $country->id) selected="selected" @endif>{{ $country->nicename }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <!-- Birth date -->
            <div class="col-span-6 sm:col-span-4">
            <div class="form-group date-time-picker label-floating">
                <label for="birth_date" class="control-label">{{ __('birth_date') }}</label>
                <input class="form-control" type="date" id="birth_date" wire:model.defer="state.birth_date" value="{!! Auth::user()->birth_date ? Auth::user()->birth_date->format('Y-m-d') : '' !!}" max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                <span class="input-group-addon">
            </span>
            <x-jet-input-error for="birth_date" class="mt-2" />
            </div>
            </div>


    </x-slot>

    <x-slot name="actions">
        <x-jet-action-message class="mr-3" on="saved">
            {{ __('saved') }}
        </x-jet-action-message>

        <x-jet-button wire:loading.attr="disabled" wire:target="photo"  class="btn btn-primary btn-lg full-width">
            {{ __('save_only') }}
        </x-jet-button>
    </x-slot>
</x-jet-form-section>

