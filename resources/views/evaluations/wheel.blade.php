<x-app-layout>
     <!-- Top Header-Profile -->

<div class="container">
    <div class="row">
        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="ui-block">
                <div class="top-header top-header-favorit">
                    <div class="top-header-thumb" style="background-image:url(../img/top-header8.webp);">

                        <div class="top-header-author">
                            <div class="author-thumb">
                                <img loading="lazy" src="{{ asset('img/default/wheel.webp') }}" alt="author" width="120" height="120">
                            </div>
                            <div class="author-content">
                                <a href="#" class="h3 author-name">{{ __('wheel_life') }}</a>
                                <div class="country">{{ __('wheel_life_desc') }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="profile-section with-social-menu-tabs">
                        <div class="row">
                            <div class="col col-xl-8 m-auto col-lg-8 col-md-12">

                                <ul class="nav nav-tabs social-menu-tabs" id="social-menu-tabs" role="tablist">
                                    @foreach($wheel_types as $type)
                                        <?php
                                            $ua = strtolower($_SERVER['HTTP_USER_AGENT']);
                                            $isMob = is_numeric(strpos($ua, "mobile"));
                                        ?>
                                        @if($isMob == FALSE)
                                        <li class="nav-item" role="presentation">
                                        @else
                                        <li class="nav-item" role="presentation" style="width:25%">
                                        @endif
                                            <a class="h6 nav-link @if($type->type_id == 1) active @endif" id="index-tab{{$type->type_id}}" data-bs-toggle="tab" href="#side{{$type->type_id}}" role="tab" aria-controls="side{{$type->type_id}}" aria-selected="true">
                                                {{$type->title}} 
                                            </a>
                                        </li>
                                    @endforeach

                                </ul>
                            </div>
                        </div>

                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ... end Top Header-Profile -->

    @foreach ($errors->all() as $error)
        <li style="list-style: none;"><div class="alert alert-danger">{{ $error }}</div></li>
    @endforeach

    @if(Session::has('success'))
        <div class="alert alert-success h3 text-center" id="added">
            {{ Session::get('success') }}
        </div>
    @endif

<div class="container">
    <div class="row">
        <div class="col-12">
    <div class="accordion bg-grey" id="accordionE">
  
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingT">
      <button class="accordion-button collapsed text-white" type="button" data-bs-toggle="collapse" data-bs-target="#collapseT" aria-expanded="false" aria-controls="collapseT"  style="background-image: url({{ asset('img/default/clickhere.png') }});background-repeat: no-repeat;">
        {{__('see_help_vid')}}
      </button>
      
    </h2>

    <div id="collapseT" class="accordion-collapse collapse" aria-labelledby="headingT" data-bs-parent="#accordionE">
      <div class="accordion-body">
        <ul class="widget w-last-video">
            @foreach($VidHelp as $vid)
                <li>
                   <h6 style="color:#39A9FF">{{ $vid->title }}</h6>
                    <a href="{{asset($vid->video)}}" class="play-video play-video--small">
                        <svg class="olymp-play-icon">
                            <use xlink:href="#olymp-play-icon"></use>
                        </svg>
                    </a>
                    <img loading="lazy" src="{{ asset($vid->image) }}" alt="video" width="272" height="181">
                    <div class="video-content">
                        <div class="title">{{ $vid->title }}</div>
                        <time class="published" datetime="2017-03-24T18:18">{{ $vid->lenght }}</time>
                    </div>
                    <div class="overlay"></div>
                </li>
                <li> </li>
            @endforeach
            </div>
      </div>
    </div>
  </div>
</div>

    </div>
    </div>
</div>
<hr>
<div class="container">
    <div class="row">

@if(isset($MyinitWheel))
        <div class="col col-6">
            <div class="ui-block responsive-flex">
                <div class="ui-block-title">
                   <h3>{{__('your_init_chart')}}</h3>
                </div>
                <div class="ui-block-content">
                    <div class="chart-js radar-chart">
                        <canvas id="wheel_chart" height="200"></canvas>
                    </div>
                </div>
            </div>
            
        </div>

        <div class="col col-6">
            <div class="tab-content">
                @for($i=1;$i<9;$i++)
                 <?php 
                    $t='t'.$i;
                    $percent =  $MyinitWheel->$t*5 .' % ';
                    if($MyinitWheel->$t <= 10){
                        $color = 'red';
                        $msg = __('must_start');
                    }elseif($MyinitWheel->$t > 10 && $MyinitWheel->$t <= 15){
                        $color = 'orange';
                        $msg = __('some_iterest');
                    }elseif($MyinitWheel->$t >= 16){
                        $color = 'green';
                        $msg = __('good_enought');
                    }

                ?>
                <div class="tab-pane fade @if($i == 1) show active @endif" id="side{{$i}}" role="tabpanel" aria-labelledby="index-tab{{$i}}">
                    <div class="ui-block responsive-flex">
                        
                        <div class="ui-block-title">
                          <h3 style="color:{{$color}}"> {{__('aspect')}} {{$wheel_types[$i-1]->title}}</h3> <h4 style="color:{{$color}}">{{$percent}}</h4>
                        </div>

                        <div class="ui-block-content">
                           <div class="birthday-item inline-items badges">
                    <div class="author-thumb">
                        <img loading="lazy" src="{{ asset('img/badge1.webp') }}" alt="author" width="38" height="38">
                        <div class="label-avatar" style="background:{{$color}}!important">{{$MyinitWheel->$t/2}}</div>
                    </div>
                    <div class="birthday-author-name">
                        <a href="#" class="h4 author-name">{{__('result')}}</a>
                        <div class="h5 birthday-date">{{$msg}}</div>
                    </div>
                
                    <div class="skills-item">
                        <div class="skills-item-meter">
                            <span class="skills-item-meter-active skills-animate" style="width: {{$MyinitWheel->$t*5}}%; opacity: 1;background:{{$color}}!important"></span>
                        </div>
                    </div>
                
                </div>
                @if($color != 'green')
                <hr>
                           <div>
                            @if($color == 'orange')
                               <h3>{{__('you_can_enhance2')}}</h3>
                            @else
                                <h3>{{__('you_can_enhance')}}</h3>
                            @endif
                               <ul>
                                @if(isset($WheelMiss[$i]))
                                   @foreach($WheelMiss[$i] as $ques)
                                   <li><input id="drag-{{$i}}-{{$ques->q_id}}" draggable="true" ondragstart="drag(event)" class="form-control" type="text" name="enhance" value="{{$ques->title}}" disabled="" readonly=""></li>
                                   @endforeach
                               @endif
                               </ul>
                           </div>
                @endif
                        </div>

                    </div>
                </div>
                @endfor
            </div>
        </div>

@else
    

        <div class="col col-12">
            <div class="row">
                @if(app()->getLocale() == 'ar')
                <div class="col col-12">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/hu5e7KiKtkw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                @elseif(app()->getLocale() == 'en')
                <div class="col col-12">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/AoeUm-B5utE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                 @elseif(app()->getLocale() == 'fr')
                <div class="col col-12">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/OqR4y104DC0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                @elseif(app()->getLocale() == 'tr')
                <div class="col col-12">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/0faO28h9GOM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                @elseif(app()->getLocale() == 'de')
                <div class="col col-12">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/HWk-GwTOaJk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                @elseif(app()->getLocale() == 'ru')
                <div class="col col-12">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/zuq-Nss6d-k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                @elseif(app()->getLocale() == 'es')
                <div class="col col-12">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/R681IqRCYMg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                @elseif(app()->getLocale() == 'cn')
                <div class="col col-12">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/ZxEytwpZF2w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                @endif
            </div>
        </div>

        <div class="col col-12">
            <div class="ui-block responsive-flex">
                <div class="ui-block-title text-center">
                   <h3>{{__('your_init_chart')}}</h3>
                </div>
                <div class="ui-block-content">
                    <div class="tab-content">
                        @for($i=0;$i<8;$i++)
                        <div class="tab-pane fade @if($i == 0) show active @endif" id="side{{$i+1}}" role="tabpanel" aria-labelledby="index-tab{{$i+1}}">
                            <div class="ui-block responsive-flex">
                                
                                <div class="ui-block-title h4 bg-blue text-center">
                                    <h4 style="color: white">{{ __('aspect') }} {{ $wheel_types[$i]->title }}</h4>
                                </div>
                                <div class="ui-block-content">
                                    <?php
                                    $type = 't'.$wheel_types[$i]->type_id;
                                    ?>
                                    @if(isset($Wheelnotcomplete->$type) && $Wheelnotcomplete->$type)
                                        <div class="crumina-module crumina-heading align-center">
                                            <div class="heading-sup-title">{{ __('great_job') }}</div>
                                            <h2 class="heading-title">{{ __('eval_done') }}</h2>
                                            <p class="heading-text">{{ __('eval_next') }}</p>
                                        </div>
                                    @else
                                        <form method="POST" action="{{ route('send_ques',app()->getLocale()) }}">
                                            @csrf
                                            <input type="hidden" name="wheel_type" value="{{$wheel_types[$i]->type_id}}"/>
                                        <table>
                                        @foreach($wheel_types[$i]['ques'] as $ques)
                                            <tr id="tr-{{$wheel_types[$i]->type_id}}-{{$ques->q_id}}" style="background-color: #f6efef;font-size: 16px;">
                                                <td width="50%"><span>{{arabic_w2e($ques->q_id)}} - {{$ques->title}}</span></td>
                                                <td width="15%">{{ __('nop') }} <input type="radio" name="ans-{{$ques->q_id}}" value="0" required="" /></td>
                                                <td width="30%">{{ __('sometimes') }} <input type="radio" name="ans-{{$ques->q_id}}" value="1" /></td>
                                                <td width="20%">{{ __('yeb') }} <input type="radio" name="ans-{{$ques->q_id}}" value="2" /></td>
                                            </tr>
                                            <tr><td colspan="4"> <hr> </td></tr>
                                        @endforeach
                                        <tr><td colspan="3"></td><td><button class="btn btn-blue btn-lg" type="submit">{{ __('next') }}</button> </td></tr>
                                        </table>
                                        
                                        </form>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endfor
                    
                </div>
                </div>
            </div>
        </div>
@endif
    </div>
</div>


@if(isset($MyinitWheel))


<div class="container">
    <div class="row">
        <div class="col-12 text-center text-white h3">
             <div class="ui-block responsive-flex">
               <div class="ui-block-title bg-grey">
                    {{ __('your_paln') }}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-3">
            <div class="ui-block responsive-flex">
               <div class="ui-block-title bg-purple">
                    <h4 style="color:white;"> {{ __('first_quarter') }}</h4>
                </div>
                <div class="h5 ui-block-content text-center" style="border: dashed gray;" id="div1" ondrop="drop(event,missionDiv1)" ondragover="allowDrop(event)">
                    {{ \Carbon\carbon::now()->format('F') .' -> '.\Carbon\carbon::now()->addMonth(3)->format('F Y') }}
                    
                </div>
            </div>
        </div>

         <div class="col-3">
             <div class="ui-block responsive-flex">
               <div class="ui-block-title bg-purple">
                    <h4 style="color:white;"> {{ __('second_quarter') }}</h4>
                </div>
                <div class="h5 ui-block-content text-center" style="border: dashed gray;" id="div2" ondrop="drop(event,missionDiv2)" ondragover="allowDrop(event)">
                    {{ \Carbon\carbon::now()->addMonth(3)->format('F') .' -> '.\Carbon\carbon::now()->addMonth(6)->format('F Y') }}
                </div>
            </div>
        </div>

         <div class="col-3">
             <div class="ui-block responsive-flex">
               <div class="ui-block-title bg-purple">
                    <h4 style="color:white;"> {{ __('third_quarter') }}</h4>
                </div>
                <div class="h5 ui-block-content text-center" style="border: dashed gray;" id="div3" ondrop="drop(event,missionDiv3)" ondragover="allowDrop(event)">
                    {{ \Carbon\carbon::now()->addMonth(6)->format('F') .' -> '.\Carbon\carbon::now()->addMonth(9)->format('F Y') }}
                </div>
            </div>
        </div>

         <div class="col-3">
             <div class="ui-block responsive-flex">
               <div class="ui-block-title bg-purple">
                    <h4 style="color:white;"> {{ __('forth_quarter') }}</h4>
                </div>
                <div class="h5 ui-block-content text-center" style="border: dashed gray;" id="div4" ondrop="drop(event,missionDiv4)" ondragover="allowDrop(event)">
                    {{ \Carbon\carbon::now()->addMonth(9)->format('F') .' -> '.\Carbon\carbon::now()->addMonth(12)->format('F Y') }}
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="ui-block responsive-flex">
                <div class="ui-block-content" id="missionDiv1">
                @if(isset($MyMissons[1]))
                    @foreach($MyMissons[1] as $mision)
                        <input class="form-control" type="text"  value="{{$mision->title}}"  readonly="" />
                    @endforeach
                @endif
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="ui-block responsive-flex">
                <div class="ui-block-content" id="missionDiv2">
                @if(isset($MyMissons[2]))
                    @foreach($MyMissons[2] as $mision)
                        <input class="form-control" type="text"  value="{{$mision->title}}" readonly=""/>
                    @endforeach
                @endif
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="ui-block responsive-flex">
                <div class="ui-block-content" id="missionDiv3">
                @if(isset($MyMissons[3]))
                    @foreach($MyMissons[3] as $mision)
                        <input class="form-control" type="text"  value="{{$mision->title}}" readonly=""/>
                    @endforeach
                @endif
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="ui-block responsive-flex">
                <div class="ui-block-content" id="missionDiv4">
                @if(isset($MyMissons[4]))
                    @foreach($MyMissons[4] as $mision)
                        <input class="form-control" type="text"  value="{{$mision->title}}" readonly=""/>
                    @endforeach
                @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@if(isset($MyinitWheel))
    @push('scripts')
    <!-- JS-libs and init for charts -->
        <script src="{{ asset('js/libs/Chart.min.js') }}"></script>
        <script src="{{ asset('js/libs/loader.min.js') }}"></script>
    <!-- ... end JS-libs and init for charts -->

    <script type="text/javascript">
       
        var ctx3 = document.getElementById("wheel_chart").getContext("2d");

        var data3 = {
            labels: [
            <?php
            foreach($wheel_types as $type){
                echo '"'.$type->title.'",';
            }
            ?>
            ],
            datasets: [
                {
                    label: "{{__('init_chart')}}",
                    backgroundColor: "rgba(255, 99, 132, 0.2)",
                    borderColor: "rgba(251, 0, 0, 0.6)",
                    pointBackgroundColor: "rgba(251, 174, 28, 0.6)",
                    pointBorderColor: "#fff",
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: "rgba(251, 174, 28, 0.6)",


                    data: [
                    <?php
                    for($i=1;$i<9;$i++){
                        $t='t'.$i;
                        echo $MyinitWheel->$t.',';
                    }
                    ?>
                    ]
                }
            ]
        };
        var radarChart = new Chart(ctx3, {
            type: "radar",
            data: data3,
            options: {
                    scale: {
                        ticks: {
                            beginAtZero: true,
                            fontFamily: "Poppins",
                            
                        },
                        gridLines: {
                            color: "rgba(135,135,135,0)",
                        },
                        pointLabels:{
                            fontFamily: "Poppins",
                            fontColor:"#878787"
                        },
                    },
                    
                    animation: {
                        duration:   3000
                    },
                    responsive: true,
                    legend: {
                            labels: {
                            fontFamily: "Poppins",
                            fontColor:"#878787"
                            }
                        },
                        elements: {
                            arc: {
                                borderWidth: 1
                            }
                        },
                        tooltip: {
                        backgroundColor:'rgba(33,33,33,1)',
                        cornerRadius:0,
                        footerFontFamily:"'Poppins'"
                    }
            }

        });


    </script>
@endpush
@endif


@if(!isset($needHelp))
@push('modals')
<!-- Faqs Popup -->

<div class="modal fade" id="faqs-popup" tabindex="-1" role="dialog" aria-labelledby="faqs-popup" aria-hidden="true">
    <div class="modal-dialog window-popup faqs-popup" role="document">
        <div class="modal-content">
            <a href="#" class="close icon-close" data-bs-dismiss="modal" aria-label="Close">
                <svg class="olymp-close-icon">
                    <use xlink:href="#olymp-close-icon"></use>
                </svg>
            </a>

            <div class="modal-header">
                <h4 class="title" id="faqs-title">{{__('welcome_in').__('your_init_chart')}}</h4>
            </div>

            <div class="modal-body">

                <div class="accordion" id="accordionExample">
                    
                    @foreach($FaqsHelp as $faq)
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="heading{{$faq->id}}">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{$faq->id}}" aria-expanded="true" aria-controls="collapse{{$faq->id}}">
                                <span class="c-green">- </span> {{$faq->title}}
                            </button>
                        </h2>
                        <div id="collapse{{$faq->id}}" class="accordion-collapse collapse @if($faq->id == $FaqsHelp[0]->id) show @endif" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                {{$faq->desc}}
                            </div>
                        </div>
                    </div>
                    @endforeach
                    
                </div>
                <hr>
                <a data-bs-dismiss="modal" class="close btn btn-blue btn-lg">{{__('dismiss')}}</a>
                <a data-bs-dismiss="modal" class="close btn btn-lg btn-primary" onclick="removeHelp({{$faq->topic_id}})">{{__('dont_show')}}</a>
            </div>
        </div>
    </div>
</div>

<!-- ... end Faqs Popup -->
@endpush
@endif

@push('scripts')
@if(!isset($needHelp))
<script type="text/javascript">
    $( document ).ready(function() {
        
        $('#faqs-popup').modal('show');
    });        

    function removeHelp(argument) {
  
  var topic = argument;

    $.ajax({
            url: "{{ route('remove_help',app()->getLocale()) }}",
            type:"POST",
            data:{
              Topic:topic,
              _token: $('meta[name="csrf-token"]').attr('content')
            },
            success:function(response){
              console.log(response);
              if(response) {
                $('.success').text(response.success);
                $("#helpCloseButton").click();
              }
            },
       });
    }
</script>
@endif
<script type="text/javascript">
function allowDrop(ev) {
  ev.preventDefault();
}

function drag(ev) {
  ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev,div) {
  ev.preventDefault();
  var toDiv = div.id;
  var data = ev.dataTransfer.getData("text");
  var mission = document.getElementById(toDiv);

  mission.appendChild(document.getElementById(data));
  //console.log(data);

$.ajax({
        url: "{{ route('add_mission',app()->getLocale()) }}",
        type:"POST",
        data:{
          Chap:data,
          ToDiv:toDiv,
          _token: $('meta[name="csrf-token"]').attr('content')
        },
        success:function(response){
          console.log(response);
          if(response) {
            $('.success').text(response.success);
            
          }
        },
   });

 

}
</script>

@if(\Session::get('type'))
<script type="text/javascript">
$( document ).ready(function() {
setTimeout(function() {
    $('#added').fadeOut('fast');
}, 3000);
var type = {{ \Session::get('type') }};
var nextType = type + 1;
if(nextType < 9){
    $("#side1").removeClass( "active" );
    $("#side1").removeClass( "show" );
    $("#index-tab1").removeClass( "active" );

    var SideId = $("#index-tab"+type);
    var SideIdNext = $("#index-tab"+nextType);

    var SideIdC = $("#side"+type);
    var SideIdNextC = $("#side"+nextType);

    SideId.removeClass( "active" );
    SideIdNext.addClass( "active" );

    SideIdC.removeClass( "active" );
    SideIdC.removeClass( "show" );

    SideIdNextC.addClass( "active" );
    SideIdNextC.addClass( "show" );

    console.log("#side"+ nextType);
}
});
</script>
@endif
@endpush
</x-app-layout>

