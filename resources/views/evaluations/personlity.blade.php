<x-app-layout>
     <!-- Top Header-Profile -->

<div class="container">
    <div class="row">
        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="ui-block">
                <div class="top-header top-header-favorit">
                    <div class="top-header-thumb" style="background-image:url(../img/top-header8.webp);">

                        <div class="top-header-author">
                            <div class="author-thumb">
                                <img loading="lazy" src="{{ asset('img/default/personality.webp') }}" alt="author" width="120" height="120">
                            </div>
                            <div class="author-content">
                                <a href="#" class="h3 author-name">{{ __('personality') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ... end Top Header-Profile -->

    @foreach ($errors->all() as $error)
        <li style="list-style: none;"><div class="alert alert-danger">{{ $error }}</div></li>
    @endforeach

    @if(Session::has('success'))
        <div class="alert alert-success h3 text-center" id="added">
            {{ Session::get('success') }}
        </div>
    @endif

@if(isset($VidHelp))
<div class="container">
    <div class="row">
        <div class="col-12">
    <div class="accordion bg-grey" id="accordionE">
  
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingT">
      <button class="accordion-button collapsed text-white" type="button" data-bs-toggle="collapse" data-bs-target="#collapseT" aria-expanded="false" aria-controls="collapseT"  style="background-image: url({{ asset('img/default/clickhere.png') }});background-repeat: no-repeat;">
        {{__('see_help_vid')}}
      </button>
      
    </h2>

    <div id="collapseT" class="accordion-collapse collapse" aria-labelledby="headingT" data-bs-parent="#accordionE">
      <div class="accordion-body">
        <ul class="widget w-last-video">

            @foreach($VidHelp as $vid)
                <li>
                   <h6 style="color:#39A9FF">{{ $vid->title }}</h6>
                    <a href="{{asset($vid->video)}}" class="play-video play-video--small">
                        <svg class="olymp-play-icon">
                            <use xlink:href="#olymp-play-icon"></use>
                        </svg>
                    </a>
                    <img loading="lazy" src="{{ asset($vid->image) }}" alt="video" width="272" height="181">
                    <div class="video-content">
                        <div class="title">{{ $vid->title }}</div>
                        <time class="published" datetime="2017-03-24T18:18">{{ $vid->lenght }}</time>
                    </div>
                    <div class="overlay"></div>
                </li>
                <li> </li>
            @endforeach
            </div>
      </div>
    </div>
  </div>
</div>

    </div>
    </div>
</div>
<hr>
@endif
<div class="container">
    <div class="row">


        <!-- <div class="col col-12">
            <div class="row">
                <div class="col col-12">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/hu5e7KiKtkw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div> -->
        @if(isset($test))
        <div class="ui-block">
            <div class="ui-block-title bg-blue">
                <h6 class="title">{{__('personality')}}</h6>
                <div class="skills-item">
                    <div class="skills-item-info">
                        <span class="skills-item-title">({{$test->ques_id}}/70)</span>
                        <span class="skills-item-count"><span class="count-animate" data-speed="1000" data-refresh-interval="50" data-to="46" data-from="0"></span><span class="units">{{intval($test->ques_id*100/70)}}%</span></span>
                    </div>
                    <div class="skills-item-meter">
                        <span class="skills-item-meter-active bg-purple" style="width: {{intval($test->ques_id*100/70)}}%"></span>
                    </div>
                </div>
            </div>
            <div class="ui-block-content">
                <!-- W-Pool -->
            <form method="POST" action="{{ route('person_ques',app()->getLocale()) }}">
                @csrf
                <ul class="widget w-pool">
                    <li>
                        <p style="font-size: 20px;font-weight: 500">{{arabic_w2e($test->ques_id)}} - {{$test->question}} </p>
                    </li>
                    <hr>
                    
                    <input type="hidden" name="question" value="{{$test->ques_id}}" />
                    <li>
                        <div class="skills-item">
                            <div class="skills-item-info">
                                <span class="skills-item-title">
                                    <span class="radio">
                                        <label>
                                            <input type="radio" name="ans" value="{{ $test->val_a }}" required="" />
                                            {{ $test->ans_a }}
                                        </label>
                                    </span>
                                </span>
                               
                            </div>
                
                        </div>
                    </li>
                    
                    <li>
                        <div class="skills-item">
                            <div class="skills-item-info">
                                <span class="skills-item-title">
                                    <span class="radio">
                                        <label>
                                            <input type="radio" name="ans" value="{{ $test->val_b }}" required="" />
                                            {{ $test->ans_b }}
                                        </label>
                                    </span>
                                </span>
                               
                            </div>
                
                        </div>
                    </li>
                </ul>
                <!-- .. end W-Pool -->
                <button type="submit" class="btn btn-md-2 btn-blue custom-color c-grey full-width text-white">{{ __('next') }}</button>
            </form>
            </div>
        </div>
        @endif

        @if(isset($test_res))
        <div class="ui-block">
            <div class="ui-block-title bg-blue">
                <h3 class="title text-white text-center">{{ __('wheel_life_desc') .'( '.$test_res->person.' )'}}</h3>
            </div>
            <div class="ui-block-content">
                <div class="post-video">
                            <!-- <div class="video-thumb">
                                <img loading="lazy" src="{{asset('img/default/'.$test_res->person.'.png')}}" alt="photo" width="197" height="194">
                            </div> -->
                    
                            <div class="video-content">
                                <!-- <a href="#" class="h4 title"></a> -->
                                {!! $test_res->intro !!}
                                <!-- <a href="#" class="link-site">YOUTUBE.COM</a> -->
                            </div>
                        </div>
            
            </div>

            <div class="ui-block-content">
                <div class="post-video">
                    <div class="video-content" style="direction: ltr;text-align: left">
                        <!-- <a href="#" class="h4 title"></a> -->
                        {!! $test_res_full->streng !!}
                        <!-- <a href="#" class="link-site">YOUTUBE.COM</a> -->
                    </div>
                </div>
            </div>

            <div class="ui-block-content">
                <div class="post-video">
                    <div class="video-content" style="direction: ltr;text-align: left">
                        <!-- <a href="#" class="h4 title"></a> -->
                        {!! $test_res_full->relation !!}
                        <!-- <a href="#" class="link-site">YOUTUBE.COM</a> -->
                    </div>
                </div>
            </div>

            <div class="ui-block-content">
                <div class="post-video">
                    <div class="video-content" style="direction: ltr;text-align: left">
                        <!-- <a href="#" class="h4 title"></a> -->
                        {!! $test_res_full->friend !!}
                        <!-- <a href="#" class="link-site">YOUTUBE.COM</a> -->
                    </div>
                </div>
            </div>

             <div class="ui-block-content">
                <div class="post-video">
                    <div class="video-content" style="direction: ltr;text-align: left">
                        <!-- <a href="#" class="h4 title"></a> -->
                        {!! $test_res_full->parent !!}
                        <!-- <a href="#" class="link-site">YOUTUBE.COM</a> -->
                    </div>
                </div>
            </div>

            <div class="ui-block-content">
                <div class="post-video">
                    <div class="video-content" style="direction: ltr;text-align: left">
                        <!-- <a href="#" class="h4 title"></a> -->
                        {!! $test_res_full->career !!}
                        <!-- <a href="#" class="link-site">YOUTUBE.COM</a> -->
                    </div>
                </div>
            </div>

            <div class="ui-block-content">
                <div class="post-video">
                    <div class="video-content" style="direction: ltr;text-align: left">
                        <!-- <a href="#" class="h4 title"></a> -->
                        {!! $test_res_full->habit !!}
                        <!-- <a href="#" class="link-site">YOUTUBE.COM</a> -->
                    </div>
                </div>
            </div>

            <div class="ui-block-content">
                <div class="post-video">
                    <div class="video-content" style="direction: ltr;text-align: left">
                        <!-- <a href="#" class="h4 title"></a> -->
                        {!! $test_res_full->conclos !!}
                        <!-- <a href="#" class="link-site">YOUTUBE.COM</a> -->
                    </div>
                </div>
            </div>

        </div>
        @endif

    </div>
</div>


@if(!isset($needHelp))
@push('modals')
<!-- Faqs Popup -->

<div class="modal fade" id="faqs-popup" tabindex="-1" role="dialog" aria-labelledby="faqs-popup" aria-hidden="true">
    <div class="modal-dialog window-popup faqs-popup" role="document">
        <div class="modal-content">
            <a href="#" class="close icon-close" data-bs-dismiss="modal" aria-label="Close">
                <svg class="olymp-close-icon">
                    <use xlink:href="#olymp-close-icon"></use>
                </svg>
            </a>

            <div class="modal-header">
                <h4 class="title" id="faqs-title">{{__('welcome_in').__('personality')}}</h4>
            </div>

            <div class="modal-body">

                <div class="accordion" id="accordionExample">
                    
                    @foreach($FaqsHelp as $faq)
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="heading{{$faq->id}}">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{$faq->id}}" aria-expanded="true" aria-controls="collapse{{$faq->id}}">
                                <span class="c-green">- </span> {{$faq->title}}
                            </button>
                        </h2>
                        <div id="collapse{{$faq->id}}" class="accordion-collapse collapse @if($faq->id == $FaqsHelp[0]->id) show @endif" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                {{$faq->desc}}
                            </div>
                        </div>
                    </div>
                    @endforeach
                    
                </div>
                <hr>
                <a data-bs-dismiss="modal" class="close btn btn-blue btn-lg">{{__('dismiss')}}</a>
                <a data-bs-dismiss="modal" class="close btn btn-lg btn-primary" onclick="removeHelp({{$faq->topic_id}})">{{__('dont_show')}}</a>
            </div>
        </div>
    </div>
</div>

<!-- ... end Faqs Popup -->
@endpush
@endif

@push('scripts')
@if(!isset($needHelp))
<script type="text/javascript">
    $( document ).ready(function() {
        
        $('#faqs-popup').modal('show');
    });        

    function removeHelp(argument) {
  
  var topic = argument;

    $.ajax({
            url: "{{ route('remove_help',app()->getLocale()) }}",
            type:"POST",
            data:{
              Topic:topic,
              _token: $('meta[name="csrf-token"]').attr('content')
            },
            success:function(response){
              console.log(response);
              if(response) {
                $('.success').text(response.success);
                $("#helpCloseButton").click();
              }
            },
       });
    }
</script>
@endif
@endpush
</x-app-layout>

