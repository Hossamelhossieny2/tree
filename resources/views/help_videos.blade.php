<x-app-layout>
<!-- Main Header Groups -->

<!-- Main Header BlogV1 -->

<div class="main-header">
    <div class="content-bg-wrap bg-account"></div>
    <div class="container">
        <div class="row">
            <div class="col col-lg-8 m-auto col-md-8 col-sm-12 col-12">
                <div class="main-header-content">
                    <h1>{{ __('help_videos') }}</h1>
                    
                </div>
            </div>
        </div>
    </div>

   
</div>


<div class="container">
    <div class="row">
    
    @foreach($videos as $vid)
        <div class="col col-3">
           <div class="ui-block"  style="background-image: url({{ asset('img/pattern/p13.png')}})">
                <div class="ui-block-title">
                    <h6 class="title text-white">{{ $vid->cat_title}}</h6>
                </div>
                <div class="ui-block-content">

                    <!-- W-Latest-Video -->
                    
                    <ul class="widget w-last-video">
                        @foreach($vid['subs'] as $sub)
                        <li>
                            <h6 style="color:#39A9FF">{{ $sub->title }}</h6>
                            <a href="{{asset($sub->video)}}" class="play-video play-video--small">
                                <svg class="olymp-play-icon">
                                    <use xlink:href="#olymp-play-icon"></use>
                                </svg>
                            </a>
                            <img loading="lazy" src="{{ asset($sub->image) }}" alt="video" width="272" height="181">
                            <div class="video-content">
                                <div class="title">{{ $sub->title }}</div>
                                <time class="published" datetime="2017-03-24T18:18">{{ $sub->lenght }}</time>
                            </div>
                            <div class="overlay"></div>
                        </li>
                        <hr style="color:yellow;">
                        @endforeach
                        <span class="text-white">{{ __('video_count').' : '.arabic_w2e(count($vid['subs'])) }}</span>
                    </ul>
                    
                    <!-- .. end W-Latest-Video -->
                </div>
            </div>
        </div>
    @endforeach
        
    </div>

</div>

</x-app-layout>