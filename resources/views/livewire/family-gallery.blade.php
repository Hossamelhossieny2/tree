<div  style="background-image: url({{ asset('img/pattern/p6.png')}})">

<div class="container">
    <div class="row">
        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="ui-block">
                <div class="top-header top-header-favorit">
                    <div class="top-header-thumb">

                        <div class="top-header-author">
                            <div class="author-thumb">
                                @if(auth()->user()->profile_photo_path)
                                    <img loading="lazy" src="{{ asset(auth()->user()->profile_photo_path) }}"  width="120" height="120" alt="{{ $post->user->name ?? '' }}">
                                @else
                                    <img loading="lazy" src="{{ asset('img/default/user_'.auth()->user()->gender.'.png')}}"  width="120" height="120" alt="{{ $post->user->name ?? '' }}">
                                @endif
                            </div>
                            <div class="author-content">
                                <a href="#" class="h3 author-name">{{ __('family_gallery') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="ui-block responsive-flex">
                <div class="ui-block-title">
                    <div class="block-btn align-right">
                        <a href="{{ route('show-gallery',app()->getLocale())}}" class="btn btn-md-2 btn-primary">{{ __("add_photos") }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="container">
    <div class="row">
        <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="clients-grid">

                @if(count($galleries)>0)
                <ul class="cat-list-bg-style align-center sorting-menu">
                    <li class="cat-list__item active" data-filter="*"><a href="#" class="">{{__('all_tags')}}</a></li>
                            <?php $uniques = array(); ?>
                            @foreach($galleries as $key => $gallery)
                                @foreach($gallery->tags as $tag)
                                    <?php $uniques[$tag->name] = $tag;?>
                                @endforeach
                            @endforeach
                            
                            @foreach($uniques as $tag)
                            <li class="cat-list__item" data-filter=".{{ $tag->name }}"><a href="#" class="">{{ $tag->name }}</a></li>
                            @endforeach
                </ul>
                @else
                <ul class="cat-list-bg-style align-center sorting-menu">
                    <li class="alert alert-danger h4">{{__('no_gal')}}</li>
                </ul>
                @endif

                <div class="row sorting-container" id="clients-grid-1" data-layout="masonry">


            @if(!empty($galleries))
                @foreach($galleries as $key => $gallery)
                    <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 sorting-item @foreach($gallery->tags as $tag){{ $tag->name }} @endforeach">

                        <div class="ui-block">
                            
                            <article class="hentry post has-post-thumbnail">
                            
                                <div class="post__author author vcard inline-items">
                                    @if(!empty($gallery->user_add->profile_photo_path))
                                    <img loading="lazy" src="{{ asset($gallery->user_add->profile_photo_path) }}" alt="author" width="42" height="42">
                                    @else
                                    <img loading="lazy" src="{{ asset('img/default/user.png') }}" alt="author" width="42" height="42">
                                    @endif
                                    <div class="author-date">
                                        <a class="h6 post__author-name fn" href="#">{{$gallery->user_add->name}}</a>
                                        <div class="post__date">
                                            <time class="published" datetime="2004-07-24T18:18">
                                                {{ $gallery->created_at }}
                                            </time>
                                        </div>
                                    </div>
                                    @if($gallery->user_add->id == auth()->user()->id)
                                    <div class="more"><svg class="olymp-three-dots-icon"><use xlink:href="#olymp-three-dots-icon"></use></svg>
                                        <ul class="more-dropdown">
                                            <li>
                                                <a href="#">Delete Pic</a>
                                            </li>
                                        </ul>
                                    </div>
                                    @endif
                                </div>

                                <h3>{{ $gallery->title_name }}</h3>
                                <p>{{ $gallery->content }}</p>
                            
                                <div class="post-thumb">
                                    <img loading="lazy" src="{{ asset('uploads/gallery_uploads/'.$gallery->pic) }}" alt="photo" width="518" height="762">
                                    @if($gallery->public == '1')
                                    <a href="#" class="post-category bg-breez">public</a>
                                    @else
                                    <a href="#" class="post-category bg-purple">private</a>
                                    @endif
                                </div>
                            
                            <div class="comments-shared">
                                <span class="btn">Tag:</span>
                                @foreach($gallery->tags as $tag)
                                <li class="cat-list__item" data-filter=".{{ $tag->name }}"><a href="#" class="">{{ $tag->name }}</a></li>
                                @endforeach
                            </div>
                            </article>
                        </div>
                    </div>
                @endforeach
            @endif
                    
                </div>

            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-4 text-center">
    @if ($galleries->hasPages())
        <nav role="navigation" aria-label="Pagination Navigation" class="flex justify-between">
            <span>
                {{-- Previous Page Link --}}
                @if ($galleries->onFirstPage())
                    <a class="relative inline-flex items-center px-4 py-2 text-sm font-medium text-white bg-primary border border-gray-300 leading-5 rounded-md hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150" href="{{$galleries->links()->elements[0][1]}}">
                        {!! __('pagination.first') !!}
                    </a>
                @else
                    <a class="relative inline-flex items-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 leading-5 rounded-md hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150" href="{{$galleries->links()->elements[0][1]}}">
                        {!! __('pagination.previous') !!}
                    </a>
                @endif
            </span>
            <?php $a=0;?>
            @foreach($galleries->links()->elements[0] as $pp)
            <?php $a+=1;?>
                <a class="relative inline-flex items-center px-4 py-2 text-sm font-medium text-white bg-primary border border-gray-300 leading-5 rounded-md hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150" href="{{$pp}}">{{arabic_w2e($a)}}</a>
            @endforeach
            <span>
                {{-- Next Page Link --}}
                @if ($galleries->hasMorePages())
                    <a class="relative inline-flex items-center px-4 py-2 text-sm font-medium text-white bg-primary border border-gray-300 leading-5 rounded-md hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150" href="{{$galleries->links()->elements[0][$a]}}">
                        {!! __('pagination.last') !!}
                    </a>
                @else
                    <span class="relative inline-flex items-center px-4 py-2 text-sm font-medium text-gray-500 bg-white border border-gray-300 cursor-default leading-5 rounded-md">
                        {!! __('pagination.next') !!}
                    </span>
                @endif
            </span>
        </nav>
    @endif
</div>
    </div>
</div>


</div>
