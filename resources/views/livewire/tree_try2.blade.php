<div style="margin-top:0 !important;background-image: url({{ asset('img/pattern/p13.png')}})">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    
    <!-- Main Header Groups -->

<div class="main-header">
    <div class="content-bg-wrap bg-group" style="background-image: asset('img/bg-group.png');"></div>
    <div class="container">
        <div class="row">
            <div class="col col-lg-8 m-auto col-md-8 col-sm-12 col-12">
                <div class="main-header-content">
                    <h1>{{ __('tree_builder') }}</h1>
                    <p> 
                    </p>
                </div>
            </div>
        </div>
    </div>

    <img loading="lazy" class="img-bottom" src="{{ asset('img/group-bottom.webp') }}" alt="friends" width="1087" height="148">
</div>

<!-- ... end Main Header Groups -->
    

    <div class="container">
        <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="ui-block responsive-flex">
                    <div class="ui-block-title text-center">
                        <div class="h6 title">{{ ( __('tree_builder') . strtoupper($user->getFamilyName()) ?? '' ) }}</div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>


<div id="tree" class="mx-auto my-5"  >
<div class="white rgba-white-strong rounded">
    <div class="text-center">

         <!-- Button trigger modal -->
            @if (session()->has('message'))
                <div class="alert alert-success text-center h4">
                    {{ session('message') }}
                </div>
            @endif

    </div>

    <div class="tree mx-auto my-5">
    <div class="white rgba-white-strong rounded">
    <ul>
      <li>
        @if(isset($father->id))
        <a href="{{ route('tree_view',[app()->getLocale(),$father->id]) }}">
            @if(isset($father->profile_photo_path))
            <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto" src="{{ asset($father->profile_photo_path) }}" alt="" />
            @else
            <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto" src="{{ url('img/default/user_male_1.png')}}" alt="" />
            @endif
            <br><span class="p-1 text-green">{{ $father->name }}</span>
        </a>
        @else
        <a data-bs-toggle="modal" data-bs-target="#userAdd" wire:click="setRelation('father')">
            <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto" src="{{ url('img/default/user_male_1.png')}}" alt="" />
            <br><span class="p-1 text-green">{{ __('add_father') }}</span>
        </a>
        @endif
        <!-- grandmother location -->
        <br>
        @if(isset($mother->id))
        <a href="{{ route('tree_view',[app()->getLocale(),$mother->id]) }}">
            @if(isset($mother->profile_photo_path))
            <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto" src="{{ asset($mother->profile_photo_path) }}" alt="" />
            @else
            <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto" src="{{ url('img/default/user_male_1.png')}}" alt="" />
            @endif
            <br><span class="p-1 text-green">{{ $mother->name }}</span>
        </a>
        @else
        <a data-bs-toggle="modal" data-bs-target="#userAdd" wire:click="setRelation('mother')">
            <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto" src="{{ url('img/default/user_female_1.png')}}" alt="" />
            <br><span class="p-1 text-red">{{ __('add_mother') }}</span>
        </a>
        @endif

            <ul>
                    @if(isset($sisters))
                    @foreach($sisters as $sister)
                    <li>
                        <a  href="{{ route('tree_view',[app()->getLocale(),$sister->id]) }}">
                            @if(isset($sister->profile_photo_path))
                            <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto" src="{{ asset($sister->profile_photo_path) }}" alt="" />
                            @else
                            <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto" src="{{ url('img/default/user_female.png')}}" alt="" />
                            @endif
                            <br><span class="p-1 text-red">{{ $sister->name }}</span>
                        </a>
                    </li>
                    @endforeach
                    @endif
                <li>
                    <a data-bs-toggle="modal" data-bs-target="#userAdd" wire:click="setRelation('sister')">
                        <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto" src="{{ url('img/default/user_female.png')}}" alt="" />
                        <br><span class="p-1 text-red">{{ __('add_sister') }}</span>
                    </a>
                </li>
                
               


                <!-- my Location start -->
                <li>
                   
                   <a>
                            @if($user->profile_photo_path)
                                <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto" src="{{ URL::asset($user->profile_photo_path) }}" alt="" />
                            @else
                                <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto bg-white" src="{{ url('img/default/user_'.$user->gender.'.png')}}" alt="" />
                            @endif
                            <br>
                            <span class="text-white">Me</span>
                        </a>
                    
                        <ul>
                            <li>
                                <a data-bs-toggle="modal" data-bs-target="#userAdd" wire:click="setRelation('wife')">
                                <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto" src="{{ url('img/default/user_female.png')}}" alt="" />
                                <br><span class="p-1 text-red">{{ __('add_wife') }}</span>
                                </a>
                            </li> 

                            @if(isset($wives))
                            @foreach($wives as $wife)
                            <li>
                                <a  href="{{ route('tree_view',[app()->getLocale(),$wife['userDet']->id]) }}">
                                 @if($wife['userDet']->profile_photo_path)
                                <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto" src="{{ URL::asset($wife['userDet']->profile_photo_path) }}" alt="" />
                                @else
                                <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto bg-white" src="{{ url('img/default/user_'.$wife['userDet']->gender.'.png')}}" alt="" />
                                @endif
                                <br><span class="p-1 text-red">{{ $wife['userDet']->name }}</span>
                                </a>
                            </li> 
                            @endforeach
                            @endif
                        </ul>
                </li>
                <!-- my Location end -->
                <li>
                    <a data-bs-toggle="modal" data-bs-target="#userAdd" wire:click="setRelation('brother')">
                        <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto" src="{{ url('img/default/user_male.png')}}" alt="" />
                        <br><span class="p-1 text-red">{{ __('add_brother') }}</span>
                    </a>
                </li>
                @if(isset($brothers))
                @foreach($brothers as $brother)
                <li>
                    <a href="{{ route('tree_view',[app()->getLocale(),$brother->id]) }}">
                        @if(isset($brother->profile_photo_path))
                        <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto" src="{{ asset($brother->profile_photo_path) }}" alt="" />
                        @else
                        <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto" src="{{ url('img/default/user_female.png')}}" alt="" />
                        @endif
                        <br><span class="p-1 text-red">{{ $brother->name }}</span>
                    </a>
                </li>
                @endforeach
                @endif

            </ul>
        </li>
    </ul>
    </div>
    </div>
</div>
</div>

 <!--    Father Modal start  -->
    <div wire:ignore.self class="modal fade" id="userAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
                <form wire:submit.prevent="store" enctype="multipart/form-data">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('add_father') }}</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close" id="AddCloseButton">
                            <span aria-hidden="true close-btn">×</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            @if ($addedPic)
                                {{ __('photo_preview') }}:
                                <img src="{{ $addedPic->temporaryUrl() }}">
                            @else
                                <img src="{{ asset('img/default/user_male.png') }}" width="50%">
                            @endif
                            <label for="exampleFormControlInput1">{{ __('add_pic') }}</label>
                            <input type="file" wire:model="addedPic" >
                            @error('editPic') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlInput1">{{ __('father_name') }}</label>
                            <input type="text" wire:model="addedName" class="form-control" placeholder="{{ __('insert').' '.__('father_name') }}" >
                            @error('addedName') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlInput1">{{ __('father_email') }}</label>
                            <input type="email" wire:model="addedEmail" class="form-control" placeholder="{{ __('insert').' '.__('father_email') }}" >
                            @error('addedEmail') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('living_country') }}</label>
                            <select wire:model="addedCountry" class="form-control">
                                <option value="">{{ __('select_living_country') }} .. </option>
                                @foreach($countries as $country)
                                <option value="{{ $country->id }}" >{{ $country->nicename }}</option>
                                @endforeach
                            </select>
                            @error('addedCountry') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('mobile_number') }}</label>
                            <input type="number" wire:model.defer="addedPhone" class="form-control" placeholder="{{ __('mobile_number').' '.__('insert') }} (01 ..... )" >
                            @error('addedPhone') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                         <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('birthday') }}</label>
                            <input class="form-control" type="date" wire:model.defer="addedBirth" id="exampleFormControlInput5" value="old('editBirth')" max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('leave_date') }}</label>
                            <input class="form-control" type="date" wire:model.defer="addedDeath" id="exampleFormControlInput5" value="{{$editDeath ?? '' }}" max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                        </div>
                        
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close-btn" data-bs-dismiss="modal">{{ __('cancel') }}</button>
                        <button type="submit" class="btn btn-primary close-modal">{{ __('save_changes') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--    Father Modal end  --> 
    <script type="text/javascript">
        document.addEventListener('livewire:load', function () {
            @this.on('added', () => {
                $("#AddCloseButton").click();
            });
            
        });
    </script>
</div>
