<div>
    <!-- Top Header-Profile -->

<div class="container">
    <div class="row">
        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="ui-block">
                <div class="top-header">
                    <div class="top-header-thumb">

                    </div>
                    <div class="profile-section">
                        
                        <x-profile_links  :act="5"/>

                        <div class="control-block-button">
                            <a href="#" class="btn btn-control bg-blue">
                                <svg class="olymp-happy-face-icon"><use xlink:href="#olymp-happy-face-icon"></use></svg>
                            </a>

                            <a href="#" class="btn btn-control bg-purple">
                                <svg class="olymp-chat---messages-icon"><use xlink:href="#olymp-chat---messages-icon"></use></svg>
                            </a>

                            <div class="btn btn-control bg-primary more">
                                <svg class="olymp-settings-icon"><use xlink:href="#olymp-settings-icon"></use></svg>

                                <ul class="more-dropdown more-with-triangle triangle-bottom-right">
                                    <li>
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#update-header-photo">Update Profile Photo</a>
                                    </li>
                                    <li>
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#update-header-photo">Update Header Photo</a>
                                    </li>
                                    <li>
                                        <a href="#">Account Settings</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <x-top_header_profile />

                </div>
            </div>
        </div>
    </div>
</div>

<!-- ... end Top Header-Profile -->

<div class="container">
    <div class="row">
        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="ui-block responsive-flex">
                <div class="ui-block-title">
                    <div class="h6 title">{{ __("my_gallery") }}</div>

                    <div class="block-btn align-right">

                        <a href="{{ route('show-gallery',app()->getLocale())}}" class="btn btn-md-2 btn-primary">{{ __("add_photos") }}</a>
                    </div>

                    <ul class="nav nav-tabs photo-gallery" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" data-bs-toggle="tab" href="#photo-page" role="tab">
                                <svg class="olymp-photos-icon"><use xlink:href="#olymp-photos-icon"></use></svg>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link active" data-bs-toggle="tab" href="#album-page" role="tab">
                                <svg class="olymp-albums-icon"><use xlink:href="#olymp-albums-icon"></use></svg>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-12">
    <div class="accordion bg-grey" id="accordionE">
  
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingT">
      <button class="accordion-button collapsed text-white" type="button" data-bs-toggle="collapse" data-bs-target="#collapseT" aria-expanded="false" aria-controls="collapseT"  style="background-image: url({{ asset('img/default/clickhere.png') }});background-repeat: no-repeat;">
        {{__('see_help_vid')}}
      </button>
      
    </h2>

    <div id="collapseT" class="accordion-collapse collapse" aria-labelledby="headingT" data-bs-parent="#accordionE">
      <div class="accordion-body">
        <ul class="widget w-last-video">
            @foreach($VidHelp as $vid)
                <li>
                   <h6 style="color:#39A9FF">{{ $vid->title }}</h6>
                    <a href="{{asset($vid->video)}}" class="play-video play-video--small">
                        <svg class="olymp-play-icon">
                            <use xlink:href="#olymp-play-icon"></use>
                        </svg>
                    </a>
                    <img loading="lazy" src="{{ asset($vid->image) }}" alt="video" width="272" height="181">
                    <div class="video-content">
                        <div class="title">{{ $vid->title }}</div>
                        <time class="published" datetime="2017-03-24T18:18">{{ $vid->lenght }}</time>
                    </div>
                    <div class="overlay"></div>
                </li>
                <li> </li>
            @endforeach
            </div>
      </div>
    </div>
  </div>
</div>

    </div>
    </div>

    <hr>

<div class="container">
    <div class="row">
        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane fade" id="photo-page" role="tabpanel">

                    <div class="photo-album-wrapper">

                        @foreach($userGal as $gal)
                        <div class="photo-item col-4-width">
                            <img loading="lazy" src="{{ asset('uploads/thumbnails/'.$gal->pic) }}" alt="photo" width="332" height="284">
                            
                            <div class="overlay overlay-dark"></div>
                            
                            <a href="#" class="post-add-icon inline-items">
                                <svg class="olymp-heart-icon"><use xlink:href="#olymp-heart-icon"></use></svg>
                                <span>15</span>
                            </a>
                            <a href="#" data-bs-toggle="modal" data-bs-target="#open-photo-popup-v2" class="  full-block"></a>
                            <div class="content">
                                <a href="#" class="h6 title">{{ $gal->title_name }}</a>
                                <time class="published" datetime="2017-03-24T18:18">{{ \Carbon\Carbon::parse($gal->created_at)->shortRelativeDiffForHumans() }}</time>
                            </div>
                        </div>
                        @endforeach

                    </div>

                </div>

                <div class="tab-pane fade active show" id="album-page" role="tabpanel">

                    <div class="photo-album-wrapper">

                        @foreach($userGal as $gal)
                        <div class="photo-album-item-wrap col-4-width">
                            
                            
                            <div class="photo-album-item">
                                <div class="photo-item">
                                    <img loading="lazy" src="{{ asset('uploads/thumbnails/'.$gal->pic) }}" alt="photo" width="332" height="284">
                                    
                                    <div class="overlay overlay-dark"></div>
                                    
                                    <a href="#" class="more">
                                    <div class="more"><svg class="olymp-three-dots-icon"><use xlink:href="#olymp-three-dots-icon"></use></svg>
                                        <ul class="more-dropdown">
                                            <li>
                                                <a href="#">{{ __("del_this_pic") }}</a>
                                            </li>
                                        </ul>
                                    </div></a>
                                    @if($gal->public == '1')
                                    <a href="#" class="post-category bg-breez">public</a>
                                    @else
                                    <a href="#" class="post-category bg-purple">private</a>
                                    @endif
                                    
                                </div>
                            
                                <div class="content">

                                    <a href="#" class="title h5">{{ $gal->title_name }}</a>
                                    <span class="sub-title">{{__('added_since')}}: {{ \Carbon\Carbon::parse($gal->created_at)->shortRelativeDiffForHumans() }}</span>
                            
                                </div>
                            
                            </div>
                        </div>
                        @endforeach
                        

                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
</div>
