<div>

<!-- Top Header-Profile -->

<div class="container">
    <div class="row">
        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="ui-block">
                <div class="top-header">
                    <div class="top-header-thumb">

                    </div>
                    <div class="profile-section">
                        
                        <x-profile_links  :act="4"/>

                        <div class="control-block-button">
                            <a href="#" class="btn btn-control bg-blue">
                                <svg class="olymp-happy-face-icon"><use xlink:href="#olymp-happy-face-icon"></use></svg>
                            </a>

                            <a href="#" class="btn btn-control bg-purple">
                                <svg class="olymp-chat---messages-icon"><use xlink:href="#olymp-chat---messages-icon"></use></svg>
                            </a>

                            <div class="btn btn-control bg-primary more">
                                <svg class="olymp-settings-icon"><use xlink:href="#olymp-settings-icon"></use></svg>

                                <ul class="more-dropdown more-with-triangle triangle-bottom-right">
                                    <li>
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#update-header-photo">Update Profile Photo</a>
                                    </li>
                                    <li>
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#update-header-photo">Update Header Photo</a>
                                    </li>
                                    <li>
                                        <a href="#">Account Settings</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <x-top_header_profile />

                </div>
            </div>
        </div>
    </div>
</div>

<!-- ... end Top Header-Profile -->



    <!-- Your Account Personal Information -->

<div class="container">
    <div class="row">

        @foreach($Msgs as $msg)
        <div class="col col-xl-4 order-xl-4 col-lg-4 order-lg-4 col-md-6 order-md-1 col-sm-12 col-4">
            <div class="ui-block">
                <div class="ui-block-title">
                    <h6 class="title">Chat / Messages (#{{ $msg->id }}) with {{ ($msg['user_id'] != auth()->user()->id) ? auth()->user()->name : $msg['from_user']['name'] }} </h6>
                    <a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="#olymp-three-dots-icon"></use></svg></a>
                </div>

                <div class="row">
                    <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">


                        <!-- Chat Field -->
                        
                        <div class="chat-field">
                            <div class="mCustomScrollbar" data-mcs-theme="dark" id="gotLastChat-{{ $msg->id }}">
                                <ul class="notification-list chat-message chat-message-field">
                                    @if($msg['user_id'] == auth()->user()->id)
                                    <li>
                                        <div class="author-thumb">
                                            @if(!empty(auth()->user()->profile_photo_path))
                                            <img loading="lazy" src="{{ asset(auth()->user()->profile_photo_path) }}" alt="{{ auth()->user()->name }}" width="36" height="36">
                                            @else
                                            <img loading="lazy" src="{{ asset('img/default/user_'.auth()->user()->gender.'.png') }}" alt="{{ auth()->user()->name }}" width="36" height="36">
                                            @endif
                                        </div>
                                        <div class="notification-event">
                                            <div class="event-info-wrap">
                                                <a href="#" class="h6 notification-friend">{{ auth()->user()->name }}</a>
                                                <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">{{ \Carbon\Carbon::parse($msg['created_at'])->shortRelativeDiffForHumans() }}</time></span>
                                            </div>
                                            <span class="chat-message-item-me popup-chat">{{ $msg['body'] }}</span>
                                        </div>
                                    </li>
                                    @else
                                    <li>
                                        <div class="notification-event">
                                            <div class="event-info-wrap">
                                                <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">{{ \Carbon\Carbon::parse($msg['created_at'])->shortRelativeDiffForHumans() }}</time></span>
                                                <a href="#" class="h6 notification-friend">{{ $msg['user']['name'] }}</a>
                                            </div>
                                            <span class="chat-message-item popup-chat" style="padding:13px;border-radius: 10px">{{ $msg['body'] }}</span>
                                        </div>
                                        <div class="author-thumb">
                                             @if(!empty($msg['user']['profile_photo_path']))
                                            <img loading="lazy" src="{{ asset($msg['user']['profile_photo_path']) }}" alt="author" width="36" height="36">
                                            @else
                                            <img loading="lazy" src="{{ asset('img/default/user_'.$msg['user']['gender'].'.png') }}" alt="author" width="36" height="36">
                                            @endif
                                        </div>
                                    </li>
                                    @endif
                                    
                                    @foreach($msg['repliess'] as $replay)
                                    
                                    @if($replay['user_id'] == auth()->user()->id)
                                    <li>
                                        <div class="author-thumb">
                                            @if(!empty(auth()->user()->profile_photo_path))
                                            <img loading="lazy" src="{{ asset(auth()->user()->profile_photo_path) }}" alt="{{ auth()->user()->name }}" width="36" height="36">
                                            @else
                                            <img loading="lazy" src="{{ asset('img/default/user_'.auth()->user()->gender.'.png') }}" alt="{{ auth()->user()->name }}" width="36" height="36">
                                            @endif
                                        </div>
                                        <div class="notification-event">
                                            <div class="event-info-wrap">
                                                <a href="#" class="h6 notification-friend">{{ auth()->user()->name }}</a>
                                                <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">{{ \Carbon\Carbon::parse($replay['created_at'])->shortRelativeDiffForHumans() }}</time></span>
                                            </div>
                                            <span class="chat-message-item-me popup-chat">{{ $replay['body'] }}</span>
                                        </div>
                                    </li>
                                    @else
                                    <li>
                                        <div class="notification-event">
                                            <div class="event-info-wrap">
                                                <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">{{ \Carbon\Carbon::parse($replay['created_at'])->shortRelativeDiffForHumans() }}</time></span>
                                                <a href="#" class="h6 notification-friend">{{$msg['user']['name']}}</a>
                                            </div>
                                            <span class="chat-message-item popup-chat" style="padding:13px;border-radius: 10px">{{ $replay['body'] }}</span>
                                        </div>
                                        <div class="author-thumb">
                                             @if(!empty($msg['user']['profile_photo_path']))
                                            <img loading="lazy" src="{{ asset($msg['user']['profile_photo_path']) }}" alt="author" width="36" height="36">
                                            @else
                                            <img loading="lazy" src="{{ asset('img/default/user_'.$msg['user']['gender'].'.png') }}" alt="author" width="36" height="36">
                                            @endif
                                        </div>
                                    </li>
                                    @endif
                                    @endforeach

                                    <script type="text/javascript">
                                        $('#gotLastChat-{{ $msg->id }}').animate({scrollTop: $('#gotLastChat-{{ $msg->id }}').prop("scrollHeight")}, 2000);
                                          
                                    </script>
                                    
                                </ul>
                            </div>
                        
                        </div>
                        
                        <!-- ... end Chat Field -->

                    </div>
                </div>

            </div>

         
        </div>
        @endforeach
    </div>
</div>

<!-- ... end Your Account Personal Information -->


</div>


