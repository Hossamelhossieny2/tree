<div>
   
<!-- Top Header-Profile -->

<div class="container">
    <div class="row">
        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="ui-block">
                <div class="top-header">
                    <div class="top-header-thumb">

                    </div>
                    <div class="profile-section">
                        
                        <x-profile_links :act="6"/>

                        <div class="control-block-button">
                            <a href="#" class="btn btn-control bg-blue">
                                <svg class="olymp-happy-face-icon"><use xlink:href="#olymp-happy-face-icon"></use></svg>
                            </a>

                            <a href="#" class="btn btn-control bg-purple">
                                <svg class="olymp-chat---messages-icon"><use xlink:href="#olymp-chat---messages-icon"></use></svg>
                            </a>

                            <div class="btn btn-control bg-primary more">
                                <svg class="olymp-settings-icon"><use xlink:href="#olymp-settings-icon"></use></svg>

                                <ul class="more-dropdown more-with-triangle triangle-bottom-right">
                                    <li>
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#update-header-photo">Update Profile Photo</a>
                                    </li>
                                    <li>
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#update-header-photo">Update Header Photo</a>
                                    </li>
                                    <li>
                                        <a href="#">Account Settings</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <x-top_header_profile />

                </div>
            </div>
        </div>
    </div>
</div>

<!-- ... end Top Header-Profile -->

<!-- Main Content Birthday -->

<div class="container">
    <div class="row">

        <div class="col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
            <div class="ui-block bg-blue">
                <div class="ui-block-title">
                    <h6 class="title text-white">{{ __('father_fam') }}</h6>
                    <div class="btn btn-control bg-primary text-white pt-2">
                        {{arabic_w2e(count($FatherFam))}}
                    </div>
                </div>
            </div>
            @if(!empty($FatherFam))
            @foreach($FatherFam as $member)
            <div class="ui-block">
                <div class="birthday-item inline-items">
                    <div class="author-thumb">
                        @if($member->profile_photo_path)
                            <img loading="lazy" src="{{ asset($member->profile_photo_path) }}" alt="author" width="42" height="42">
                        @else
                            <img loading="lazy" src="{{ asset('img/default/user_'.$member->gender.'.png') }}" alt="author" width="42" height="42">
                        @endif
                    </div>
                    <div class="birthday-author-name">
                        <img src="{{ asset('img/flag/'.strtolower($member->country->iso.'.svg'))}}" width="16px" />
                        <a href="#" class="h6 author-name">{{ $member->name }} 
                            @if(!empty($member->father->name))
                                {{ $member->father->name }}
                            @endif 
                            @if(!empty($member->family->title) && !empty($member->father->name) && $member->family->title != $member->father->name)
                                {{ $member->family->title }}
                            @endif
                            <br/> 
                                @if(count($member->husbands)>0 && $member->gender == 'female') <font color="#37A9FF">'{{__('wife_of')}} {{$member->husbands[0]->name}}'</font> 
                                @endif 
                            </a>
                            <?php if(!empty($member->phone_number)){
                                $mobile = arabic_w2e('00'.$member->country->phonecode).arabic_w2e(substr($member->phone_number,1));
                            }else{
                                $mobile = __('mobile_number_null');
                            };
                            ?>

                        <div class="birthday-date h4">{{ $mobile  }}</div>
                    </div>
                    <a href="#{{ $member->id }}" class="btn btn-xm bg-blue" onclick="Profile({{$member->id}})">{{ __('view_profile') }}</a>
                </div>
            </div>
            @endforeach
            @endif

        </div>
        
         <div class="col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
            <div class="ui-block bg-purple">
                <div class="ui-block-title">
                    <h6 class="title text-white">{{ __('mother_fam') }}</h6>
                    <div class="btn btn-control bg-primary text-white pt-2">
                        {{ ($MotherFam) ? arabic_w2e(count($MotherFam)) : arabic_w2e(0) }}
                    </div>
                </div>
            </div>

            @if(!empty($MotherFam))
            @foreach($MotherFam as $member)

            <div class="ui-block">
                <div class="birthday-item inline-items">
                    <div class="author-thumb">
                        @if($member->profile_photo_path)
                            <img loading="lazy" src="{{ asset($member->profile_photo_path) }}" alt="author" width="42" height="42">
                        @else
                            <img loading="lazy" src="{{ asset('img/default/user_'.$member->gender.'.png') }}" alt="author" width="42" height="42">
                        @endif
                    </div>
                    <div class="birthday-author-name">
                        <img src="{{ asset('img/flag/'.strtolower($member->country->iso.'.svg'))}}" width="16px" />
                        <a href="#" class="h6 author-name">{{ $member->name }} 
                            @if(!empty($member->father->name))
                                {{ $member->father->name }}
                            @endif 
                            @if(!empty($member->family->title) && !empty($member->father->name) && $member->family->title != $member->father->name)
                                {{ $member->family->title }}
                            @endif
                            <br/> 
                                @if(count($member->husbands)>0 && $member->gender == 'female') <font color="#37A9FF">'{{__('wife_of')}} {{$member->husbands[0]->name}}'</font> 
                                @endif 
                            </a>
                         <?php if(!empty($member->phone_number)){
                                $mobile = arabic_w2e('00'.$member->country->phonecode).arabic_w2e(substr($member->phone_number,1));
                            }else{
                                $mobile = __('mobile_number_null');
                            };
                            ?>

                        <div class="birthday-date h4">{{ $mobile  }}</div>
                    </div>
                    <a href="#{{ $member->id }}" class="btn btn-xm bg-purple" onclick="Profile({{$member->id}})">{{ __('view_profile') }}</a>
                </div>
            </div>
            @endforeach
            @endif

        </div>

        <div class="col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
            <div class="ui-block bg-purple">
                <div class="ui-block-title">
                    <h6 class="title text-white">{{ __('wife_fam') }}</h6>
                    <div class="btn btn-control bg-primary text-white pt-2">
                        {{ ($WifeFam) ? arabic_w2e(count($WifeFam)) : arabic_w2e(0) }}
                    </div>
                </div>
            </div>

            @if(!empty($WifeFam))
            @foreach($WifeFam as $member)

            <div class="ui-block">
                <div class="birthday-item inline-items">
                    <div class="author-thumb">
                        @if($member->profile_photo_path)
                            <img loading="lazy" src="{{ asset($member->profile_photo_path) }}" alt="author" width="42" height="42">
                        @else
                            <img loading="lazy" src="{{ asset('img/default/user_'.$member->gender.'.png') }}" alt="author" width="42" height="42">
                        @endif
                    </div>
                    <div class="birthday-author-name">
                        <img src="{{ asset('img/flag/'.strtolower($member->country->iso.'.svg'))}}" width="16px" />
                        <a href="#" class="h6 author-name">{{ $member->name }} 
                            @if(!empty($member->father->name))
                                {{ $member->father->name }}
                            @endif 
                            @if(!empty($member->family->title) && !empty($member->father->name) && $member->family->title != $member->father->name)
                                {{ $member->family->title }}
                            @endif
                            <br/> 
                                @if(count($member->husbands)>0 && $member->gender == 'female') <font color="#37A9FF">'{{__('wife_of')}} {{$member->husbands[0]->name}}'</font> 
                                @endif 
                            </a>
                         <?php if(!empty($member->phone_number)){
                                $mobile = arabic_w2e('00'.$member->country->phonecode).arabic_w2e(substr($member->phone_number,1));
                            }else{
                                $mobile = __('mobile_number_null');
                            };
                            ?>

                        <div class="birthday-date h4">{{ $mobile  }}</div>
                    </div>
                    <a href="#{{ $member->id }}" class="btn btn-xm bg-purple" onclick="Profile({{$member->id}})">{{ __('view_profile') }}</a>
                </div>
            </div>
            @endforeach
            @endif

        </div>

<hr>

        <div class="col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
            <div class="ui-block bg-success">
                <div class="ui-block-title">
                    <h6 class="title text-white">{{ __('follow_list') }}</h6>
                    <div class="btn btn-control bg-primary text-white pt-2">
                        {{arabic_w2e(count($FollowFam))}}
                    </div>
                </div>
            </div>
            @if(!empty($FollowFam))
            @foreach($FollowFam as $member)
            <div class="ui-block">
                <div class="birthday-item inline-items">
                    <div class="author-thumb">
                        @if($member->user->profile_photo_path)
                            <img loading="lazy" src="{{ asset($member->user->profile_photo_path) }}" alt="author" width="42" height="42">
                        @else
                            <img loading="lazy" src="{{ asset('img/default/user_'.$member->user->gender.'.png') }}" alt="author" width="42" height="42">
                        @endif
                    </div>
                    <div class="birthday-author-name">
                        <img src="{{ asset('img/flag/'.strtolower($member->user->country->iso.'.svg'))}}" width="16px" />
                        <a href="#" class="h6 author-name">{{ $member->user->name }} 
                            @if(!empty($member->father->name))
                                {{ $member->father->name }}
                            @endif 
                            @if(!empty($member->family->title) && !empty($member->father->name) && $member->family->title != $member->father->name)
                                {{ $member->family->title }}
                            @endif
                            <br/> 
                                @if($member->gender == 'female' && count($member->user->husbands)>0) <font color="#37A9FF">'{{__('wife_of')}} {{$member->husbands[0]->name}}'</font> 
                                @endif 
                            </a>
                        <div class="birthday-date">{{ $member->phone_number ?? __('mobile_number_null')  }}</div>
                    </div>
                    <a href="#{{ $member->id }}" class="btn btn-xm bg-success" onclick="Profile({{$member->id}})">{{ __('view_profile') }}</a>
                    <a href="#{{ $member->id }}" class="btn btn-xm bg-danger" id="userFamily-{{$member->user->id}}" wire:click="unFollow({{$member->user->id}})">{{ __('unfollow_user') }}</a>
                           
                </div>
            </div>
            @endforeach
            @endif

        </div>

    </div>
</div>

<!-- ... end Main Content Birthday -->

<!-- Window-popup Blog Post Popup -->

<script type="text/javascript">

    function Profile(userid){
        $('#user_profile_details').html('');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
            $.ajax({
               type:'POST',
               url:"{{ route('getUserProfile',app()->getLocale()) }}",
               data:{user:userid},
               success:function(data){
                  $('#user_profile_details').append(data)
                  $('#user-profile-popup').modal('show');
                  }
        });
    }

    function Follow(userid){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
            $.ajax({
               type:'POST',
               url:"{{ route('followUser.post',app()->getLocale()) }}",
               data:{user:userid},
               success:function(data){
                  console.log(data);
                  // $('#userFamily-'+userid).modal('show');
                  }
        });
    }

    function unFollow(userid){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
            $.ajax({
               type:'POST',
               url:"{{ route('unfollowUser.post',app()->getLocale()) }}",
               data:{user:userid},
               success:function(data){
                  console.log(data);
                  // $('#userFamily-'+userid).modal('show');
                  }
        });
    }
</script>

<div class="modal fade" id="user-profile-popup" tabindex="-1" role="dialog" aria-labelledby="blog-post-popup" aria-hidden="true">
    <div class="modal-dialog window-popup blog-post-popup" role="document">
        <div class="modal-content" style="background-image:url({{ asset('img/pattern/p7.png')}})">
            <a href="#" class="close icon-close" data-bs-dismiss="modal" aria-label="Close">
                <svg class="olymp-close-icon">
                    <use xlink:href="#olymp-close-icon"></use>
                </svg>
            </a>
            <div class="modal-body" id="user_profile_details">
            </div>
        </div>
    </div>
</div>

<!-- ... end Window-popup Blog Post Popup -->

</div>
