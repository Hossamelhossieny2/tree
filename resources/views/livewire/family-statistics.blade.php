<div style="background-image: url({{ asset('img/pattern/p17.png') }})">

    <style type="text/css">
        .mapael .map {
            position: relative;
        }

        .mapael .mapTooltip {
            position: absolute;
            background-color: #fff;
            moz-opacity: 0.70;
            opacity: 0.70;
            filter: alpha(opacity=70);
            border-radius: 10px;
            padding: 10px;
            z-index: 1000;
            max-width: 200px;
            display: none;
            color: #343434;
        }

        .mapael .areaLegend {
            margin-bottom: 20px;
        }

        /* The switch - the box around the slider */
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        /* Hide default HTML checkbox */
        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        /* The slider */
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #2196F3;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>

    <!-- Main Header Groups -->

    <div class="main-header main-header-weather">
        <div class="content-bg-wrap bg-weather"></div>

        <div class="date-and-place">
            <div class="date">{{ date('l, M dS') }}</div>
            <div class="place">{{ $weathard['location']['name'] }}, {{ $weathard['location']['country'] }}</div>
        </div>

        <div class="wethear-update">
            {{ __('updated') }}: {{ $weathard['current']['last_updated'] }}
            <svg class="olymp-weather-refresh-icon">
                <use xlink:href="#olymp-weather-refresh-icon"></use>
            </svg>
        </div>
        <div class="container">
            <div class="row">
                <div class="m-auto col-lg-4 col-md-8 col-sm-12 col-12">
                    <div class="wethear-content">
                        <div class="wethear-now">
                            <img src="{{ $weathard['current']['condition']['icon'] }}" />

                            <div class="temperature-sensor">{{ arabic_w2e($weathard['current']['temp_c']) }} C°</div>
                            <div class="max-min-temperature">
                                <span>{{ $weathard['current']['condition']['text'] }}</span>
                            </div>
                        </div>


                        <div class="climate">{{ $weathard['location']['region'] }}</div>

                        <div class="wethear-now-description" style="font-size:10px !important">
                            <div>
                                <svg class="olymp-weather-thermometer-icon icon">
                                    <use xlink:href="#olymp-weather-thermometer-icon"></use>
                                </svg>
                                <div>{{ __('humidity') }}</div>
                                <span>{{ arabic_w2e($weathard['current']['humidity']) }} %</span>
                            </div>

                            <div>
                                <svg class="olymp-weather-rain-drops-icon icon">
                                    <use xlink:href="#olymp-weather-rain-drops-icon"></use>
                                </svg>
                                <div>{{ __('chance_of_rain') }}</div>
                                <span>{{ arabic_w2e($weathard['current']['cloud']) }} %</span>
                            </div>

                            <div>
                                <svg class="olymp-weather-wind-icon-header icon">
                                    <use xlink:href="#olymp-weather-wind-icon-header"></use>
                                </svg>
                                <div>{{ __('wind_speed') }}</div>
                                <span>{{ arabic_w2e($weathard['current']['wind_mph']) }} MPH</span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <img class="img-bottom" src="{{ asset('img/weather-bottom.webp') }}" alt="friends" width="1124"
            height="310">
    </div>

    <!-- ... end Main Header Groups -->

    <div class="container">
        <div class="row">

            <div class="mb-3 col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                <div class="mb-0 ui-block h-100 bg-smoke">
                    <div class="ui-block-title">
                        <div class="h6 title">{{ __('family_statistics') }}</div>
                    </div>
                    <div class="ui-block-content" id="family_numbers">
                        <div class="swiper-container" data-slide="fade">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="statistics-slide">
                                        <div class="count-stat" data-swiper-parallax="-500">
                                            {{ arabic_w2e($maleCount + $femaleCount) }}</div>
                                        <div class="title" data-swiper-parallax="-100">
                                            <span class="c-primary">{{ __('members') }}</span> {{ __('added') }}
                                        </div>
                                        <div class="sub-title" data-swiper-parallax="-100">{{ __('member_word') }}
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="statistics-slide">
                                        <div class="count-stat" data-swiper-parallax="-500">
                                            {{ arabic_w2e($familyS['gallery']) }}</div>
                                        <div class="title" data-swiper-parallax="-100">
                                            <span class="c-primary">{{ __('picture') }}</span> {{ __('added') }}
                                        </div>
                                        <div class="sub-title" data-swiper-parallax="-100">{{ __('picture_word') }}
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="statistics-slide">
                                        <div class="count-stat" data-swiper-parallax="-500">
                                            {{ arabic_w2e($familyS['post']) }}</div>
                                        <div class="title" data-swiper-parallax="-100">
                                            <span class="c-primary">{{ __('posts') }}</span> {{ __('added') }}
                                        </div>
                                        <div class="sub-title" data-swiper-parallax="-100"> {{ __('post_word') }}</div>
                                    </div>
                                </div>

                            </div>

                            <!-- If we need pagination -->
                            <div class="swiper-pagination pagination-blue"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mb-3 col col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                <div class="mb-0 ui-block h-100 bg-smoke">
                    <div class="ui-block-title">
                        <div class="h6 title">{{ __('worldwide_stat') }}</div>
                    </div>

                    <div class="ui-block-content">
                        <div class="world-statistics">
                            <div class="world-statistics-img">
                                <div class="mapcontainer">
                                    <div class="map">
                                        <span>{{ __('map_missing') }}</span>
                                    </div>

                                </div>
                            </div>

                            <ul class="country-statistics">
                                @foreach ($user_map as $key => $val)
                                    <li>
                                        <img loading=lazy src="{{ asset('img/flag/' . strtolower($key) . '.svg') }}"
                                            alt="flag" width="16" height="11">
                                        <span class="country">{{ $key }}</span>
                                        <span class="count-stat">{{ arabic_w2e($val) }}</span>
                                    </li>
                                @endforeach
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (!empty($wheel['MyinitWheel']))
        <?php
        $part1 = intval((($wheel['MyinitWheel']->t1 + $wheel['MyinitWheel']->t2 + $wheel['MyinitWheel']->t3 + $wheel['MyinitWheel']->t4) * 5) / 4);
        $part2 = intval((($wheel['MyinitWheel']->t5 + $wheel['MyinitWheel']->t6 + $wheel['MyinitWheel']->t5 + $wheel['MyinitWheel']->t8) * 5) / 4);
        $app_parts = intval(($part1 + $part2) / 2);
        ?>
        <div class="container">
            <div class="row">
                <div class="mb-3 col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                    <div class="mb-0 ui-block h-100 bg-smoke">
                        <div class="ui-block-title">
                            <h3>{{ __('your_init_chart') }} ({{ arabic_w2e($app_parts) }}%)</h3>
                        </div>
                        <div class="ui-block-content">
                            <div class="chart-js radar-chart">
                                <canvas id="wheel_chart" height="200"></canvas>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mb-3 col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                    <div class="mb-0 ui-block h-100 bg-smoke">
                        <div class="ui-block-title">
                            <div class="h5 title">{{ __('private_victory') }} ({{ arabic_w2e($part1) }}%)</div>
                            <a href="#" class="more">
                                <svg class="olymp-three-dots-icon">
                                    <use xlink:href="#olymp-three-dots-icon"></use>
                                </svg>
                            </a>
                        </div>

                        <div class="ui-block-content">
                            <div class="skills-item">
                                <div class="skills-item-info">
                                    <span class="skills-item-title">{{ __('aspect') }}
                                        {{ $wheel['wheel_types'][0]->title }}</span>
                                    <span class="skills-item-count"><span class="count-animate" data-speed="1000"
                                            data-refresh-interval="50" data-to="62" data-from="0"></span><span
                                            class="units">{{ $wheel['MyinitWheel']->t1 * 5 }}%</span></span>
                                </div>
                                <div class="skills-item-meter">
                                    <span
                                        class="skills-item-meter-active 
                                @if ($wheel['MyinitWheel']->t1 <= 10) bg-primary
                                @elseif($wheel['MyinitWheel']->t1 > 10 && $wheel['MyinitWheel']->t1 <= 15)
                                bg-yellow
                                @elseif($wheel['MyinitWheel']->t1 >= 16)
                                bg-breez @endif
                                "
                                        style="width: {{ $wheel['MyinitWheel']->t1 * 5 }}%"></span>
                                </div>
                            </div>

                            <div class="skills-item">
                                <div class="skills-item-info">
                                    <span class="skills-item-title">{{ __('aspect') }}
                                        {{ $wheel['wheel_types'][1]->title }}</span>
                                    <span class="skills-item-count"><span class="count-animate" data-speed="1000"
                                            data-refresh-interval="50" data-to="46" data-from="0"></span><span
                                            class="units">{{ $wheel['MyinitWheel']->t2 * 5 }}%</span></span>
                                </div>
                                <div class="skills-item-meter">
                                    <span
                                        class="skills-item-meter-active 
                                @if ($wheel['MyinitWheel']->t2 <= 10) bg-primary
                                @elseif($wheel['MyinitWheel']->t2 > 10 && $wheel['MyinitWheel']->t2 <= 15)
                                bg-yellow
                                @elseif($wheel['MyinitWheel']->t2 >= 16)
                                bg-breez @endif
                                "
                                        style="width: {{ $wheel['MyinitWheel']->t2 * 5 }}%"></span>
                                </div>
                            </div>

                            <div class="skills-item">
                                <div class="skills-item-info">
                                    <span class="skills-item-title">{{ __('aspect') }}
                                        {{ $wheel['wheel_types'][2]->title }}</span>
                                    <span class="skills-item-count"><span class="count-animate" data-speed="1000"
                                            data-refresh-interval="50" data-to="79" data-from="0"></span><span
                                            class="units">{{ $wheel['MyinitWheel']->t3 * 5 }}%</span></span>
                                </div>
                                <div class="skills-item-meter">
                                    <span
                                        class="skills-item-meter-active 
                                @if ($wheel['MyinitWheel']->t3 <= 10) bg-primary
                                @elseif($wheel['MyinitWheel']->t3 > 10 && $wheel['MyinitWheel']->t3 <= 15)
                                bg-yellow
                                @elseif($wheel['MyinitWheel']->t3 >= 16)
                                bg-breez @endif
                                "
                                        style="width: {{ $wheel['MyinitWheel']->t3 * 5 }}%"></span>
                                </div>
                            </div>

                            <div class="skills-item">
                                <div class="skills-item-info">
                                    <span class="skills-item-title">{{ __('aspect') }}
                                        {{ $wheel['wheel_types'][3]->title }}</span>
                                    <span class="skills-item-count"><span class="count-animate" data-speed="1000"
                                            data-refresh-interval="50" data-to="34" data-from="0"></span><span
                                            class="units">{{ $wheel['MyinitWheel']->t4 * 5 }}%</span></span>
                                </div>
                                <div class="skills-item-meter">
                                    <span
                                        class="skills-item-meter-active 
                                @if ($wheel['MyinitWheel']->t4 <= 10) bg-primary
                                @elseif($wheel['MyinitWheel']->t4 > 10 && $wheel['MyinitWheel']->t4 <= 15)
                                bg-yellow
                                @elseif($wheel['MyinitWheel']->t4 >= 16)
                                bg-breez @endif
                                "
                                        style="width: {{ $wheel['MyinitWheel']->t4 * 5 }}%"></span>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="mb-3 col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                    <div class="mb-0 ui-block h-100 bg-smoke">
                        <div class="ui-block-title">
                            <div class="h5 title">{{ __('public_victory') }} ({{ arabic_w2e($part2) }}%)</div>
                            <a href="#" class="more">
                                <svg class="olymp-three-dots-icon">
                                    <use xlink:href="#olymp-three-dots-icon"></use>
                                </svg>
                            </a>
                        </div>

                        <div class="ui-block-content">
                            <div class="skills-item">
                                <div class="skills-item-info">
                                    <span class="skills-item-title">{{ __('aspect') }}
                                        {{ $wheel['wheel_types'][4]->title }}</span>
                                    <span class="skills-item-count"><span class="count-animate" data-speed="1000"
                                            data-refresh-interval="50" data-to="62" data-from="0"></span><span
                                            class="units">{{ $wheel['MyinitWheel']->t5 * 5 }}%</span></span>
                                </div>
                                <div class="skills-item-meter">
                                    <span
                                        class="skills-item-meter-active 
                                @if ($wheel['MyinitWheel']->t5 <= 10) bg-primary
                                @elseif($wheel['MyinitWheel']->t5 > 10 && $wheel['MyinitWheel']->t5 <= 15)
                                bg-yellow
                                @elseif($wheel['MyinitWheel']->t5 >= 16)
                                bg-breez @endif
                                "
                                        style="width: {{ $wheel['MyinitWheel']->t5 * 5 }}%"></span>
                                </div>
                            </div>

                            <div class="skills-item">
                                <div class="skills-item-info">
                                    <span class="skills-item-title">{{ __('aspect') }}
                                        {{ $wheel['wheel_types'][5]->title }}</span>
                                    <span class="skills-item-count"><span class="count-animate" data-speed="1000"
                                            data-refresh-interval="50" data-to="46" data-from="0"></span><span
                                            class="units">{{ $wheel['MyinitWheel']->t6 * 5 }}%</span></span>
                                </div>
                                <div class="skills-item-meter">
                                    <span
                                        class="skills-item-meter-active 
                                @if ($wheel['MyinitWheel']->t6 <= 10) bg-primary
                                @elseif($wheel['MyinitWheel']->t6 > 10 && $wheel['MyinitWheel']->t6 <= 15)
                                bg-yellow
                                @elseif($wheel['MyinitWheel']->t6 >= 16)
                                bg-breez @endif
                                "
                                        style="width: {{ $wheel['MyinitWheel']->t6 * 5 }}%"></span>
                                </div>
                            </div>

                            <div class="skills-item">
                                <div class="skills-item-info">
                                    <span class="skills-item-title">{{ __('aspect') }}
                                        {{ $wheel['wheel_types'][6]->title }}</span>
                                    <span class="skills-item-count"><span class="count-animate" data-speed="1000"
                                            data-refresh-interval="50" data-to="79" data-from="0"></span><span
                                            class="units">{{ $wheel['MyinitWheel']->t7 * 5 }}%</span></span>
                                </div>
                                <div class="skills-item-meter">
                                    <span
                                        class="skills-item-meter-active 
                                @if ($wheel['MyinitWheel']->t7 <= 10) bg-primary
                                @elseif($wheel['MyinitWheel']->t7 > 10 && $wheel['MyinitWheel']->t7 <= 15)
                                bg-yellow
                                @elseif($wheel['MyinitWheel']->t7 >= 16)
                                bg-breez @endif
                                "
                                        style="width: {{ $wheel['MyinitWheel']->t7 * 5 }}%"></span>
                                </div>
                            </div>

                            <div class="skills-item">
                                <div class="skills-item-info">
                                    <span class="skills-item-title">{{ __('aspect') }}
                                        {{ $wheel['wheel_types'][7]->title }}</span>
                                    <span class="skills-item-count"><span class="count-animate" data-speed="1000"
                                            data-refresh-interval="50" data-to="95" data-from="0"></span><span
                                            class="units">{{ $wheel['MyinitWheel']->t8 * 5 }}%</span></span>
                                </div>
                                <div class="skills-item-meter">
                                    <span
                                        class="skills-item-meter-active 
                                @if ($wheel['MyinitWheel']->t8 <= 10) bg-primary
                                @elseif($wheel['MyinitWheel']->t8 > 10 && $wheel['MyinitWheel']->t8 <= 15)
                                bg-yellow
                                @elseif($wheel['MyinitWheel']->t8 >= 16)
                                bg-breez @endif
                                "
                                        style="width: {{ $wheel['MyinitWheel']->t8 * 5 }}%"></span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @endif

    @if (!empty($wheel['myMision']) && !empty($wheel['MyinitWheel']))

        <div class="container">
            <div class="row">
                <div class="mb-3 col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="mb-0 ui-block h-100 bg-smoke">
                        <div class="ui-block-title">
                            @if ($quarter != 0)
                                <h5 class="title">{{ __('mission_end') }} {{ $quarter }}</h5>
                            @else
                                <h5 class="title">{{ __('mission_must_add') }}</h5> <a
                                    href="{{ route('wheel_view', app()->getLocale()) }}">{{ __('your_paln') }}</a>
                            @endif
                        </div>

                        <ol class="widget w-playlist">
                            @foreach ($wheel['myMision'] as $wh)
                                @php
                                    $pub_date = $wh->created_at->format('Y-m-d');
                                    $datework = \Carbon\Carbon::createFromDate($pub_date);

                                    $now = \Carbon\Carbon::now();
                                    $testdate = $datework->diffInDays($now);
                                @endphp
                                <li>
                                    <label class="switch">
                                        @if ($wh->done_day != date('Y-m-d'))
                                            <input type="checkbox" id="mark{{ $wh->id }}"
                                                onclick="markAsDone({{ $wh->id }})">
                                        @else
                                            <input type="checkbox" checked="">
                                        @endif
                                        <span class="slider"></span>
                                    </label>
                                    <div class="playlist-thumb">
                                        <img loading="lazy" src="{{ asset('img/default/wheel.webp') }}"
                                            alt="thumb-composition" width="34" height="34">
                                    </div>

                                    <div class="composition">
                                        <a href="#" class="composition-name">{{ $wh->quesWithType->title }}</a>
                                        <a href="#"
                                            class="composition-author">{{ __('aspect') . ' ' . $wheel['wheel_types'][$wh->type - 1]->title }}
                                            (
                                            {{ arabic_w2e($testdate) . ' ' . __('days') . ' / ' . __('done') . ' ' . arabic_w2e($wh->is_done) }}
                                            )</a>
                                    </div>

                                    <div class="composition-time">
                                        @if ($wh->done_day != date('Y-m-d'))
                                            <img src="{{ asset('img/default/notdone.png') }}" width="130">
                                        @else
                                            <img src="{{ asset('img/default/done.png') }}" width="130">
                                        @endif
                                    </div>
                                </li>
                            @endforeach
                        </ol>


                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="container">
        <div class="row">
            <div class="mb-3 col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                <div class="mb-0 ui-block h-100 bg-smoke">
                    <div class="ui-block-title">
                        <div class="h6 title">{{ __('posts') }}</div>
                    </div>
                    <div class="ui-block-content">
                        <div class="chart-with-statistic">
                            <ul class="statistics-list-count">
                                <li>
                                    <div class="points">
                                        <span>
                                            <span class="statistics-point bg-purple"></span>
                                            {{ __('picture') }}
                                        </span>
                                    </div>
                                    <div class="count-stat">{{ arabic_w2e($familyS['gallery']) }}</div>
                                </li>
                                <li>
                                    <div class="points">
                                        <span>
                                            <span class="statistics-point bg-yellow"></span>
                                            {{ __('personal_posts') }}
                                        </span>
                                    </div>
                                    <div class="count-stat">{{ arabic_w2e($familyS['post_without_pic']) }}</div>
                                </li>
                                <li>
                                    <div class="points">
                                        <span>
                                            <span class="statistics-point bg-breez"></span>
                                            {{ __('multimedia_posts') }}
                                        </span>
                                    </div>
                                    <div class="count-stat">{{ arabic_w2e($familyS['post_with_pic']) }}</div>
                                </li>
                                <li>
                                    <div class="points">
                                        <span>
                                            <span class="statistics-point bg-primary"></span>
                                            {{ __('event_posts') }}
                                        </span>
                                    </div>
                                    <div class="count-stat">{{ arabic_w2e($familyS['post_event']) }}</div>
                                </li>
                            </ul>

                            <!---------------------------------------- PIE-COLOR-CHART ------------------------------------>

                            <div class="chart-js chart-js-pie-color">
                                <canvas id="postChart" width="185" height="185"></canvas>
                                <div class="general-statistics">{{ arabic_w2e($familyS['total']) }}
                                    <span>{{ __('post') }}</span>
                                </div>
                            </div>

                            <!--
                            JS libraries for PIE-COLOR-CHART:
                            js/libs/Chart.min.js
                            js/libs/chartjs-plugin-deferred.min.js
                            js/libs/loader.min.js
                         -->


                            <!-- JS-init for PIE-COLOR-CHART: js/libs/run-chart.min.js -->

                            <!--------------------------------- ... end PIE-COLOR-CHART ----------------------------------->

                        </div>
                    </div>
                </div>
            </div>

            <div class="mb-3 col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                <div class="mb-0 ui-block h-100 bg-smoke">
                    <div class="ui-block-title">
                        <div class="h6 title">{{ __('age_dist') }}</div>
                    </div>

                    <div class="ui-block-content">
                        <div class="skills-item">
                            <div class="skills-item-info">
                                <span class="skills-item-title">{{ __('age_0') }}</span>
                                <span class="skills-item-count"><span class="count-animate" data-speed="1000"
                                        data-refresh-interval="50" data-to="{{ intval(($age[5] / $age['total']) * 100) }}"
                                        data-from="0"></span><span class="units">{{ arabic_w2e($age[0]) }}
                                        ({{ arabic_w2e(intval(($age[0] / $age['total']) * 100)) }}%)</span></span>
                            </div>
                            <div class="skills-item-meter">
                                <span class="skills-item-meter-active bg-purple"
                                    style="width: {{ intval(($age[0] / $age['total']) * 100) }}%"></span>
                            </div>
                        </div>

                        <div class="skills-item">
                            <div class="skills-item-info">
                                <span class="skills-item-title">{{ __('age_1') }}</span>
                                <span class="skills-item-count"><span class="count-animate" data-speed="1000"
                                        data-refresh-interval="50" data-to="{{ intval(($age[5] / $age['total']) * 100) }}"
                                        data-from="0"></span><span class="units">{{ arabic_w2e($age[1]) }}
                                        ({{ arabic_w2e(intval(($age[1] / $age['total']) * 100)) }}%)</span></span>
                            </div>
                            <div class="skills-item-meter">
                                <span class="skills-item-meter-active bg-purple"
                                    style="width: {{ intval(($age[1] / $age['total']) * 100) }}%"></span>
                            </div>
                        </div>

                        <div class="skills-item">
                            <div class="skills-item-info">
                                <span class="skills-item-title">{{ __('age_2') }}</span>
                                <span class="skills-item-count"><span class="count-animate" data-speed="1000"
                                        data-refresh-interval="50" data-to="{{ intval(($age[5] / $age['total']) * 100) }}"
                                        data-from="0"></span><span class="units">{{ arabic_w2e($age[2]) }}
                                        ({{ arabic_w2e(intval(($age[2] / $age['total']) * 100)) }}%)</span></span>
                            </div>
                            <div class="skills-item-meter">
                                <span class="skills-item-meter-active bg-purple"
                                    style="width: {{ intval(($age[2] / $age['total']) * 100) }}%"></span>
                            </div>
                        </div>

                        <div class="skills-item">
                            <div class="skills-item-info">
                                <span class="skills-item-title">{{ __('age_3') }}</span>
                                <span class="skills-item-count"><span class="count-animate" data-speed="1000"
                                        data-refresh-interval="50" data-to="{{ intval(($age[5] / $age['total']) * 100) }}"
                                        data-from="0"></span><span class="units">{{ arabic_w2e($age[3]) }}
                                        ({{ arabic_w2e(intval(($age[3] / $age['total']) * 100)) }}%)</span></span>
                            </div>
                            <div class="skills-item-meter">
                                <span class="skills-item-meter-active bg-purple"
                                    style="width: {{ intval(($age[3] / $age['total']) * 100) }}%"></span>
                            </div>
                        </div>

                        <div class="skills-item">
                            <div class="skills-item-info">
                                <span class="skills-item-title">{{ __('age_4') }}</span>
                                <span class="skills-item-count"><span class="count-animate" data-speed="1000"
                                        data-refresh-interval="50" data-to="{{ intval(($age[5] / $age['total']) * 100) }}"
                                        data-from="0"></span><span class="units">{{ arabic_w2e($age[4]) }}
                                        ({{ arabic_w2e(intval(($age[4] / $age['total']) * 100)) }}%)</span></span>
                            </div>
                            <div class="skills-item-meter">
                                <span class="skills-item-meter-active bg-purple"
                                    style="width: {{ intval(($age[4] / $age['total']) * 100) }}%"></span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="mb-3 col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">

                <div class="mb-0 ui-block h-100 bg-smoke">
                    <div class="ui-block-title">
                        <div class="h6 title">{{ $age['null'] }} {{ __('person') }} {{ __('without_age') }} (
                            @if ($age['null'] != 0)
                                {{ 100 - intval(($age['total'] / $age['null']) * 100) }}%
                            @else
                                0%
                            @endif)</div>
                    </div>
                    <div class="ui-block-content">
                        <div class="skills-item">
                            <div class="skills-item-info">
                                <span class="skills-item-title">{{ __('age_5') }}</span>
                                <span class="skills-item-count"><span class="count-animate" data-speed="1000"
                                        data-refresh-interval="50" data-to="{{ intval(($age[5] / $age['total']) * 100) }}"
                                        data-from="0"></span><span class="units">{{ arabic_w2e($age[5]) }}
                                        ({{ arabic_w2e(intval(($age[5] / $age['total']) * 100)) }}%)</span></span>
                            </div>
                            <div class="skills-item-meter">
                                <span class="skills-item-meter-active bg-purple"
                                    style="width: {{ intval(($age[5] / $age['total']) * 100) }}%"></span>
                            </div>
                        </div>

                        <div class="skills-item">
                            <div class="skills-item-info">
                                <span class="skills-item-title">{{ __('age_6') }}</span>
                                <span class="skills-item-count"><span class="count-animate" data-speed="1000"
                                        data-refresh-interval="50" data-to="{{ intval(($age[5] / $age['total']) * 100) }}"
                                        data-from="0"></span><span class="units">{{ arabic_w2e($age[6]) }}
                                        ({{ arabic_w2e(intval(($age[6] / $age['total']) * 100)) }}%)</span></span>
                            </div>
                            <div class="skills-item-meter">
                                <span class="skills-item-meter-active bg-purple"
                                    style="width: {{ intval(($age[6] / $age['total']) * 100) }}%"></span>
                            </div>
                        </div>

                        <div class="skills-item">
                            <div class="skills-item-info">
                                <span class="skills-item-title">{{ __('age_7') }}</span>
                                <span class="skills-item-count"><span class="count-animate" data-speed="1000"
                                        data-refresh-interval="50" data-to="{{ intval(($age[5] / $age['total']) * 100) }}"
                                        data-from="0"></span><span class="units">{{ arabic_w2e($age[7]) }}
                                        ({{ arabic_w2e(intval(($age[7] / $age['total']) * 100)) }}%)</span></span>
                            </div>
                            <div class="skills-item-meter">
                                <span class="skills-item-meter-active bg-purple"
                                    style="width: {{ intval(($age[7] / $age['total']) * 100) }}%"></span>
                            </div>
                        </div>

                        <div class="skills-item">
                            <div class="skills-item-info">
                                <span class="skills-item-title">{{ __('age_8') }}</span>
                                <span class="skills-item-count"><span class="count-animate" data-speed="1000"
                                        data-refresh-interval="50" data-to="{{ intval(($age[5] / $age['total']) * 100) }}"
                                        data-from="0"></span><span class="units">{{ arabic_w2e($age[8]) }}
                                        ({{ arabic_w2e(intval(($age[8] / $age['total']) * 100)) }}%)</span></span>
                            </div>
                            <div class="skills-item-meter">
                                <span class="skills-item-meter-active bg-purple"
                                    style="width: {{ intval(($age[8] / $age['total']) * 100) }}%"></span>
                            </div>
                        </div>

                        <div class="skills-item">
                            <div class="skills-item-info">
                                <span class="skills-item-title">{{ __('age_9') }}</span>
                                <span class="skills-item-count"><span class="count-animate" data-speed="1000"
                                        data-refresh-interval="50" data-to="{{ intval(($age[5] / $age['total']) * 100) }}"
                                        data-from="0"></span><span class="units">{{ arabic_w2e($age[9]) }}
                                        ({{ arabic_w2e(intval(($age[9] / $age['total']) * 100)) }}%)</span></span>
                            </div>
                            <div class="skills-item-meter">
                                <span class="skills-item-meter-active bg-purple"
                                    style="width: {{ intval(($age[9] / $age['total']) * 100) }}%"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="mb-3 col col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                <div class="mb-0 ui-block h-100 bg-smoke">
                    <div class="ui-block-title">
                        <div class="h6 title">{{ __('relationship_status') }}</div>
                    </div>
                    <div class="ui-block-content">

                        <!------------------------------------------- RADAR-CHART ----------------------------------------->

                        <div class="chart-js chart-radar">
                            <canvas height="200" width="300" id="familiesChart"></canvas>
                            <table class="dataTable" border="0">
                                <tbody>
                                    <tr>
                                        <td width="30px"><span class="color" style="background:#3FA5EF"></span>
                                        </td>
                                        <td>{{ __('single') }}</td>
                                        <td>{{ arabic_w2e($single) }}</td>
                                        <td width="30px"><span class="color" style="background:#E58694"></span>
                                        </td>
                                        <td>{{ __('married') }}</td>
                                        <td>{{ arabic_w2e($married) }}</td>
                                    </tr>
                                    <tr>
                                        <td width="30px"><span class="color" style="background:#BFBFBF"></span>
                                        </td>
                                        <td>{{ __('widowed') }}</td>
                                        <td>{{ arabic_w2e($widowed) }}</td>
                                        <td width="30px"><span class="color" style="background:#87E6EB"></span>
                                        </td>
                                        <td>{{ __('divorced') }}</td>
                                        <td>{{ arabic_w2e($divorced) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <!--
                            JS libraries for RADAR-CHART:
                            js/libs/Chart.min.js
                            js/libs/chartjs-plugin-deferred.min.js
                            js/libs/loader.min.js
                         -->

                        <!-- JS-init for RADAR-CHART: js/libs/run-chart.min.js -->

                        <!------------------------------------ ... end RADAR-CHART ---------------------------------------->

                    </div>
                </div>
            </div>
            <div class="mb-3 col col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                <div class="mb-0 ui-block h-100 bg-smoke">
                    <div class="ui-block-title">
                        <div class="h6 title">{{ __('gender_chart') }}</div>
                    </div>
                    <div class="ui-block-content">

                        <!------------------------------------------- RADAR-CHART ----------------------------------------->

                        <div class="chart-js chart-radar">
                            <canvas height="200" width="300" id="genderChart"></canvas>
                            <table class="dataTable" border="0">
                                <tbody>
                                    <tr>
                                        <td width="30px"><span class="color" style="background:#39A9FF"></span>
                                        </td>
                                        <td>{{ __('male') }}</td>
                                        <td>{{ arabic_w2e($maleCount) }}</td>
                                    </tr>
                                    <tr>
                                        <td width="30px"><span class="color" style="background:#FF5E3B"></span>
                                        </td>
                                        <td>{{ __('female') }}</td>
                                        <td>{{ arabic_w2e($femaleCount) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <!--
                            JS libraries for RADAR-CHART:
                            js/libs/Chart.min.js
                            js/libs/chartjs-plugin-deferred.min.js
                            js/libs/loader.min.js
                         -->

                        <!-- JS-init for RADAR-CHART: js/libs/run-chart.min.js -->

                        <!------------------------------------ ... end RADAR-CHART ---------------------------------------->

                    </div>
                </div>
            </div>
            <div class="mb-3 col col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                <div class="mb-0 ui-block h-100 bg-smoke">
                    <div class="ui-block-title">
                        <div class="h6 title">{{ __('livings') }}</div>
                    </div>

                    <div class="ui-block-content">

                        <!------------------------------------------- RADAR-CHART ----------------------------------------->

                        <div class="chart-js chart-radar">
                            <canvas height="200" width="300" id="livingChart"></canvas>
                            <table class="dataTable" border="0">
                                <tbody>
                                    <tr>
                                        <td width="30px"><span class="color" style="background:#F67E3F"></span>
                                        </td>
                                        <td>{{ __('living') }}</td>
                                        <td>{{ arabic_w2e($lived) }}</td>
                                    </tr>
                                    <tr>
                                        <td width="30px"><span class="color" style="background:#8C4CA0"></span>
                                        </td>
                                        <td>{{ __('departed') }}</td>
                                        <td>{{ arabic_w2e($dead) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <!--
                            JS libraries for RADAR-CHART:
                            js/libs/Chart.min.js
                            js/libs/chartjs-plugin-deferred.min.js
                            js/libs/loader.min.js
                         -->

                        <!-- JS-init for RADAR-CHART: js/libs/run-chart.min.js -->

                        <!------------------------------------ ... end RADAR-CHART ---------------------------------------->

                    </div>
                </div>
            </div>
            <div class="mb-3 col col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                <div class="mb-0 ui-block h-100 bg-smoke">
                    <div class="ui-block-title">
                        <div class="h6 title">{{ __('fam_dist') }}</div>
                    </div>
                    <div class="ui-block-content">

                        <!------------------------------------------- RADAR-CHART ----------------------------------------->

                        <div class="chart-js chart-radar">
                            <canvas height="200" width="300" id="relationChart"></canvas>
                            <table class="dataTable" border="0">
                                <tbody>
                                    <tr>
                                        <td width="30px"><span class="color" style="background:#3581C4"></span>
                                        </td>
                                        <td>{{ __('father_fam') }}</td>
                                        <td>{{ arabic_w2e($fatherFamily) }}</td>
                                    </tr>
                                    <tr>
                                        <td width="30px"><span class="color" style="background:#F76367"></span>
                                        </td>
                                        <td>{{ __('mother_fam') }}</td>
                                        <td>{{ arabic_w2e($motherFamily) }}</td>
                                    </tr>
                                    <tr>
                                        <td width="30px"><span class="color" style="background:#00BA37"></span>
                                        </td>
                                        <td>{{ __('wife_fam') }}</td>
                                        <td>{{ arabic_w2e($wifeFamily) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <!--
                            JS libraries for RADAR-CHART:
                            js/libs/Chart.min.js
                            js/libs/chartjs-plugin-deferred.min.js
                            js/libs/loader.min.js
                         -->

                        <!-- JS-init for RADAR-CHART: js/libs/run-chart.min.js -->

                        <!------------------------------------ ... end RADAR-CHART ---------------------------------------->

                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="container">
        <div class="row">
            @if ($oldestMale)
                <div class="col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12">
                    <div class="ui-block bg-smoke">
                        <div class="ui-block-content">
                            <ul class="statistics-list-count">
                                <li>
                                    <div class="points">
                                        <span>
                                            {{ __('oldest_living_m') }}
                                        </span>
                                    </div>
                                    <div class="count-stat">
                                        <figcaption>
                                            @if ($oldestMale->profile_photo_path)
                                                <img class="w-16 h-16 mx-auto border-4 border-gray-400 rounded-full"
                                                    src="{{ asset($oldestMale->profile_photo_path) }}"
                                                    alt="{{ $oldestMale->name ?? '' }}" width="50" />
                                            @else
                                                <img class="w-16 h-16 mx-auto border-4 border-gray-400 rounded-full"
                                                    src="{{ asset('img/default/user_male_1.png') }}"
                                                    alt="{{ $oldestMale->name ?? '' }}" width="50" />
                                            @endif
                                            <div><strong>{{ __('age') }}:
                                                    {{ arabic_w2e($oldestMale->getTextualAge()) }} </strong></div>
                                            <div><a
                                                    href="{{ url('tree_view' . '/' . $oldestMale->id) }}">{{ $oldestMale->name }}</a>
                                            </div>
                                            <div><small class="c-primary">
                                                    @if ($oldestMale->id == $user->id)
                                                        {{ __('this_is_u') }}
                                                    @endif
                                                </small>
                                                <small><br> {{ __('born') }}:
                                                    {{ arabic_w2e(date_format($oldestMale->birth_date, 'Y')) }}</small>
                                            </div>
                                        </figcaption>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            @endif
            @if ($oldestFemale)
                <div class="col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12">
                    <div class="ui-block bg-smoke">
                        <div class="ui-block-content">
                            <ul class="statistics-list-count">
                                <li>
                                    <div class="points">
                                        <span>
                                            {{ __('oldest_living_f') }}
                                        </span>
                                    </div>
                                    <div class="count-stat">
                                        <figcaption>
                                            @if ($oldestFemale->profile_photo_path)
                                                <img class="w-16 h-16 mx-auto border-4 border-gray-400 rounded-full"
                                                    src="{{ asset($oldestFemale->profile_photo_path) }}"
                                                    alt="{{ $oldestFemale->name ?? '' }}" width="50" />
                                            @else
                                                <img class="w-16 h-16 mx-auto border-4 border-gray-400 rounded-full"
                                                    src="{{ asset('img/default/user_female_1.png') }}"
                                                    alt="{{ $oldestFemale->name ?? '' }}" width="50" />
                                            @endif
                                            <div><strong>{{ __('age') }}:
                                                    {{ arabic_w2e($oldestFemale->getTextualAge()) }} </strong></div>
                                            <div><a
                                                    href="{{ url('tree_view' . '/' . $oldestFemale->id) }}">{{ $oldestFemale->name }}</a>
                                            </div>
                                            <div><small class="c-primary">
                                                    @if ($oldestFemale->id == $user->id)
                                                        {{ __('this_is_u') }}
                                                    @endif
                                                </small>
                                                <small><br> {{ __('born') }}:
                                                    {{ arabic_w2e(date_format($oldestFemale->birth_date, 'Y')) }}</small>
                                            </div>
                                        </figcaption>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            @endif
            @if ($youngestMale)
                <div class="col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12">
                    <div class="ui-block bg-smoke">
                        <div class="ui-block-content">
                            <ul class="statistics-list-count">
                                <li>
                                    <div class="points">
                                        <span>
                                            {{ __('youngest_living_m') }}
                                        </span>
                                    </div>
                                    <div class="count-stat">
                                        <figcaption>
                                            @if ($youngestMale->profile_photo_path)
                                                <img class="w-16 h-16 mx-auto border-4 border-gray-400 rounded-full"
                                                    src="{{ asset($youngestMale->profile_photo_path) }}"
                                                    alt="{{ $youngestMale->name ?? '' }}" width="50" />
                                            @else
                                                <img class="w-16 h-16 mx-auto border-4 border-gray-400 rounded-full"
                                                    src="{{ asset('img/default/user_male_3.png') }}"
                                                    alt="{{ $youngestMale->name ?? '' }}" width="50" />
                                            @endif

                                            <div><strong>{{ __('age') }}:
                                                    {{ arabic_w2e($youngestMale->getTextualAge()) }}</strong></div>
                                            <div><a
                                                    href="{{ url('tree_view' . '/' . $youngestMale->id) }}">{{ $youngestMale->name }}</a>
                                            </div>
                                            <div><small class="c-primary">
                                                    @if ($youngestMale->id == $user->id)
                                                        {{ __('this_is_u') }}
                                                    @endif
                                                </small>
                                                <small><br> {{ __('born') }}:
                                                    {{ arabic_w2e(date_format($youngestMale->birth_date, 'Y')) }}</small>
                                            </div>
                                        </figcaption>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            @endif
            @if ($youngestFemale)
                <div class="col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12">
                    <div class="ui-block bg-smoke">
                        <div class="ui-block-content">
                            <ul class="statistics-list-count">
                                <li>
                                    <div class="points">
                                        <span>
                                            {{ __('youngest_living_f') }}
                                        </span>
                                    </div>
                                    <div class="count-stat">
                                        <figcaption>
                                            @if ($youngestFemale->profile_photo_path)
                                                <img class="w-16 h-16 mx-auto border-4 border-gray-400 rounded-full"
                                                    src="{{ asset($youngestFemale->profile_photo_path) }}"
                                                    alt="{{ $youngestFemale->name ?? '' }}" width="50" />
                                            @else
                                                <img class="w-16 h-16 mx-auto border-4 border-gray-400 rounded-full"
                                                    src="{{ asset('img/default/user_female_3.png') }}"
                                                    alt="{{ $youngestFemale->name ?? '' }}" width="50" />
                                            @endif
                                            <div><strong>{{ __('age') }}:
                                                    {{ arabic_w2e($youngestFemale->getTextualAge()) }}</strong></div>
                                            <div><a
                                                    href="{{ url('tree_view' . '/' . $youngestFemale->id) }}">{{ $youngestFemale->name }}</a>
                                            </div>
                                            <div><small class="c-primary">
                                                    @if ($youngestFemale->id == $user->id)
                                                        {{ __('this_is_u') }}
                                                    @endif
                                                </small>
                                                <small><br> {{ __('born') }}:
                                                    {{ arabic_w2e(date_format($youngestFemale->birth_date, 'Y')) }}</small>
                                            </div>
                                        </figcaption>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>

    <hr class="divider">
</div>
</div>


@if (!isset($needHelp))
    @push('modals')
        <!-- Faqs Popup -->

        <div class="modal fade" id="faqs-popup" tabindex="-1" role="dialog" aria-labelledby="faqs-popup"
            aria-hidden="true">
            <div class="modal-dialog window-popup faqs-popup" role="document">
                <div class="modal-content">
                    <a href="#" class="close icon-close" data-bs-dismiss="modal" aria-label="Close">
                        <svg class="olymp-close-icon">
                            <use xlink:href="#olymp-close-icon"></use>
                        </svg>
                    </a>

                    <div class="modal-header">
                        <h4 class="title" id="faqs-title">{{ __('welcome_in') . __('family_statistics') }}</h4>
                    </div>

                    <div class="modal-body">

                        <div class="accordion" id="accordionExample">

                            @foreach ($FaqsHelp as $faq)
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="heading{{ $faq->id }}">
                                        <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#collapse{{ $faq->id }}" aria-expanded="true"
                                            aria-controls="collapse{{ $faq->id }}">
                                            <span class="c-green">- </span> {{ $faq->title }}
                                        </button>
                                    </h2>
                                    <div id="collapse{{ $faq->id }}"
                                        class="accordion-collapse collapse @if ($faq->id == $FaqsHelp[0]->id) show @endif"
                                        aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            {{ $faq->desc }}
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                        <hr>
                        <a data-bs-dismiss="modal" class="close btn btn-blue btn-lg">{{ __('dismiss') }}</a>
                        <a data-bs-dismiss="modal" class="close btn btn-lg btn-primary"
                            onclick="removeHelp({{ $faq->topic_id }})">{{ __('dont_show') }}</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- ... end Faqs Popup -->
    @endpush

    @push('scripts')
        <script type="text/javascript">
            $(document).ready(function() {

                $('#faqs-popup').modal('show');
            });

            function removeHelp(argument) {

                var topic = argument;

                $.ajax({
                    url: "{{ route('remove_help', app()->getLocale()) }}",
                    type: "POST",
                    data: {
                        Topic: topic,
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(response) {
                        console.log(response);
                        if (response) {
                            $('.success').text(response.success);
                            $("#helpCloseButton").click();
                        }
                    },
                });
            }
        </script>
    @endpush
@endif

<!-- include Chart script -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js" charset="utf-8"></script>

<script>
    // Gender Pie Chart
    var genderData = [{
            value: {{ $maleCount }},
            color: "#39A9FF",
            highlight: "#000",
            label: "{{ __('male') }}"
        },
        {
            value: {{ $femaleCount }},
            color: "#FF5E3B",
            highlight: "#000",
            label: "{{ __('female') }}"
        }
    ]
    var genderContext = document.getElementById('genderChart').getContext('2d');
    var genderChart = new Chart(genderContext).Pie(genderData);

    //  Living vs. Deceased Pie Chart
    var livingData = [{
            value: {{ $lived }},
            color: "#F67E3F",
            highlight: "#000",
            label: "{{ __('living') }}"
        },
        {
            value: {{ $dead }},
            color: "#8C4CA0",
            highlight: "#000",
            label: "{{ __('departed') }}"
        }
    ]
    var context = document.getElementById('livingChart').getContext('2d');
    var livingChart = new Chart(context).Pie(livingData);


    //  Relationship Pie Chart
    var relationData = [{
            value: {{ $single }},
            color: "#3FA5EF",
            highlight: "#000",
            label: "{{ __('single') }}"
        },
        {
            value: {{ $married }},
            color: "#E58694",
            highlight: "#000",
            label: "{{ __('married') }}"
        },
        {
            value: {{ $widowed }},
            color: "#BFBFBF",
            highlight: "#000",
            label: "{{ __('widowed') }}"
        },
        {
            value: {{ $divorced }},
            color: "#87E6EB",
            highlight: "#000",
            label: "{{ __('divorced') }}"
        }
    ]
    var context = document.getElementById('familiesChart').getContext('2d');
    var relationChart = new Chart(context).Pie(relationData);

    //  families Pie Chart
    var relationData = [{
            value: {{ $fatherFamily }},
            color: "#3581C4",
            highlight: "#000",
            label: "{{ __('father_fam') }}"
        },
        {
            value: {{ $motherFamily }},
            color: "#F76367",
            highlight: "#000",
            label: "{{ __('mother_fam') }}"
        },
        {
            value: {{ $wifeFamily }},
            color: "#00BA37",
            highlight: "#000",
            label: "{{ __('wife_fam') }}"
        }
    ]
    var context = document.getElementById('relationChart').getContext('2d');
    var relationChart = new Chart(context).Pie(relationData);

    var doughnutData = [{
            value: {{ $familyS['gallery'] }},
            color: "#7C59C2",
            highlight: "#000",
            label: "{{ __('picture') }}"
        },
        {
            value: {{ $familyS['post_without_pic'] }},
            color: "#FFDC1A",
            highlight: "#000",
            label: "{{ __('personal_posts') }}"
        },
        {
            value: {{ $familyS['post_with_pic'] }},
            color: "#04DDC1",
            highlight: "#000",
            label: "{{ __('multimedia_posts') }}"
        },
        {
            value: {{ $familyS['post_event'] }},
            color: "#FE5E39",
            highlight: "#000",
            label: "{{ __('event_posts') }}"
        }

    ];
    var context = document.getElementById('postChart').getContext('2d');
    var postChart = new Chart(context).Doughnut(doughnutData);
</script>

<script type="text/javascript">
    $(function() {
        $(".mapcontainer").mapael({
            map: {
                name: "world_countries",
                defaultArea: {
                    attrs: {
                        stroke: "#fff",
                        "stroke-width": 1
                    }
                }
            },
            legend: {
                area: {
                    mode: "horizontal",
                    title: "Countries population",
                    labelAttrs: {
                        "font-size": 12
                    },
                    marginLeft: 5,
                    marginLeftLabel: 5,
                    slices: [{
                            max: 5,
                            attrs: {
                                fill: "#6aafe1"
                            },
                            label: "< 5 members"
                        },
                        {
                            min: 5,
                            max: 10,
                            attrs: {
                                fill: "#459bd9"
                            },
                            label: "> 5 members and < 10 millions"
                        },
                        {
                            min: 10,
                            max: 30,
                            attrs: {
                                fill: "#2579b5"
                            },
                            label: "> 10 members and < 50 millions"
                        },
                        {
                            min: 30,
                            attrs: {
                                fill: "#1a527b"
                            },
                            label: "> 50 members"
                        }
                    ]
                },
                plot: {
                    mode: "horizontal",
                    title: "Cities population",
                    labelAttrs: {
                        "font-size": 12
                    },
                    marginLeft: 5,
                    marginLeftLabel: 5,
                    slices: [{
                            max: 50,
                            attrs: {
                                fill: "#00FD00"
                            },
                            attrsHover: {
                                transform: "s1.5",
                                "stroke-width": 1
                            },
                            label: "< 500 000",
                            size: 10
                        },
                        {
                            min: 50,
                            max: 100,
                            attrs: {
                                fill: "#FF9E00"
                            },
                            attrsHover: {
                                transform: "s1.5",
                                "stroke-width": 1
                            },
                            label: "> 500 000 and 1 million",
                            size: 20
                        },
                        {
                            min: 100,
                            attrs: {
                                fill: "#FF0000"
                            },
                            attrsHover: {
                                transform: "s1.5",
                                "stroke-width": 1
                            },
                            label: "> 1 million",
                            size: 30
                        }
                    ]
                }
            },
            plots: {
                <?php foreach($user_map as $key=>$val){
                        $countryy = \App\Models\Country::where('iso',$key)->first();
                        ?> '{{ $key }}': {
                    latitude: {{ $countryy->lat }},
                    longitude: {{ $countryy->lon }},
                    value: {{ $val }},
                    tooltip: {
                        content: "{{ $countryy->nicename }}<br />{{ __('members') }}: {{ $val }}"
                    }
                },
                <?php }?>
            },
            areas: {
                <?php $countries = \App\Models\Country::all();
                        foreach($countries as $country){
                    ?> "{{ $country->iso }}": {
                    "value": "{{ $country->value ?? '0' }}",
                    "attrs": {
                        "href": "#"
                    },
                    "tooltip": {
                        "content": "<span style=\"font-weight:bold;\">{{ $country->nicename }}<\/span><br \/>{{ __('population') }} : {{ $country->value ?? '0' }}"
                    }
                },
                <?php }?>
            }
        });
    });

    function markAsDone(e) {
        var markId = e;
        $.ajax({
            url: "{{ route('mark_mission_done', app()->getLocale()) }}",
            type: "POST",
            data: {
                mission: markId,
                _token: $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response) {
                console.log(response);
                if (response) {
                    $('.success').text(response.success);
                    $('#mark' + markId).attr('checked');
                }
            },
        });
    }
</script>

@if (!empty($wheel['MyinitWheel']))
    @push('scripts')
        <script type="text/javascript">
            var ctx3 = document.getElementById("wheel_chart").getContext("2d");

            var data3 = {
                labels: [
                    <?php
                    foreach ($wheel['wheel_types'] as $type) {
                        echo '"' . __('aspect') . ' ' . $type->title . '",';
                    }
                    ?>
                ],
                datasets: [{
                    label: "{{ __('init_chart') }}",
                    backgroundColor: "rgba(255, 99, 132, 0.2)",
                    borderColor: "rgba(251, 0, 0, 0.6)",
                    pointBackgroundColor: "rgba(251, 174, 28, 0.6)",
                    pointBorderColor: "#fff",
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: "rgba(251, 174, 28, 0.6)",


                    data: [
                        <?php
                        for ($i = 1; $i < 9; $i++) {
                            $t = 't' . $i;
                            echo $wheel['MyinitWheel']->$t . ',';
                        }
                        ?>
                    ]
                }]
            };
            var radarChart = new Chart(ctx3, {
                type: "radar",
                data: data3,
                options: {
                    scale: {
                        ticks: {
                            beginAtZero: true,
                            fontFamily: "Poppins",

                        },
                        gridLines: {
                            color: "rgba(135,135,135,0)",
                        },
                        pointLabels: {
                            fontFamily: "Poppins",
                            fontColor: "#878787"
                        },
                    },

                    animation: {
                        duration: 3000
                    },
                    responsive: true,
                    legend: {
                        labels: {
                            fontFamily: "Poppins",
                            fontColor: "#878787"
                        }
                    },
                    elements: {
                        arc: {
                            borderWidth: 1
                        }
                    },
                    tooltip: {
                        backgroundColor: 'rgba(33,33,33,1)',
                        cornerRadius: 0,
                        footerFontFamily: "'Poppins'"
                    }
                }
            });
        </script>
    @endpush
@endif


</div>
