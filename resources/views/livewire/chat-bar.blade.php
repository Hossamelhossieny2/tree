<div>
    <!-- <div wire:poll.5000ms> -->
    <!-- Fixed Sidebar Right -->

<div class="fixed-sidebar right">
    <div class="fixed-sidebar-right sidebar--small" id="sidebar-right">

        <div class="mCustomScrollbar" data-mcs-theme="dark">
            <ul class="chat-users">
                @if($famUsers)
                @foreach($famUsers as $chat)

                @if(!isset($chat['death_date']))
                <li class="inline-items @if($chat->id != auth()->id()) js-chat-open @endif" data-user="{{ $chat->id }}" data-route="{{ route('get.user-chat',[$chat]) }}">
                    <div class="author-thumb">
                         @if(!empty($chat->profile_photo_path))
                        <img loading="lazy" alt="author" src="{{ asset($chat->profile_photo_path )}}" class="avatar" width="34" height="34" title="{{ $chat->name}}">
                        @else
                        <img loading="lazy" alt="author" src="{{ asset('img/default/user_'.$chat->gender.'.png') }}" class="avatar" width="34" height="34" title="{{ $chat->name}} {{$chat->father->name ?? ' '}}">
                        @endif
                        @if(!empty($chat['user_status']['user_id']))
                            <span class="icon-status online"></span>
                        @else
                            <span class="icon-status disconected"></span>
                        @endif
                    </div>
                </li>
                @endif
                @endforeach
                @endif
            </ul>
        </div>

        <div class="search-friend inline-items">
            <a href="#" class="js-sidebar-open">
                <svg class="olymp-menu-icon"><use xlink:href="#olymp-menu-icon"></use></svg>
            </a>
        </div>

        <a href="#" class="olympus-chat inline-items">
            <svg class="olymp-chat---messages-icon"><use xlink:href="#olymp-chat---messages-icon"></use></svg>
        </a>

    </div>

    <div class="fixed-sidebar-right sidebar--large" id="sidebar-right-1">

        <div class="mCustomScrollbar" data-mcs-theme="dark">

            <div class="ui-block-title ui-block-title-small">
                <a href="#" class="title">Close Friends</a>
                <a href="{{ route('profile.show', app()->getLocale()) }}">Settings</a>
            </div>

            <ul class="chat-users">
                @if($famUsers)
                @foreach($famUsers as $chat)
                @if(!isset($chat['death_date']))
                <li class="inline-items @if($chat->id != auth()->id()) js-chat-open @endif" data-user="{{ $chat->id }}" data-route="{{ route('get.user-chat',[$chat]) }}">

                    <div class="author-thumb">
                        @if(!empty($chat->profile_photo_path))
                        <img loading="lazy" alt="author" src="{{ asset($chat->profile_photo_path )}}" class="avatar" width="34" height="34">
                        @else
                        <img loading="lazy" alt="author" src="{{ asset('img/default/user_'.$chat->gender.'.png') }}" class="avatar" width="34" height="34">
                        @endif
                        @if(!empty($chat['user_status']['user_id']))
                            <span class="icon-status online"></span>
                        @else
                            <span class="icon-status disconected"></span>
                        @endif
                    </div>

                    <div class="author-status">
                        <a data-bs-toggle="modal" data-bs-target="#userMsg" wire:click="updateUserInfo({{ $chat->id }})" class="h6 author-name">{{$chat->name}} {{$chat->father->name ?? ' '}}</a>
                        @if(!empty($chat['user_status']['user_id']))
                            <span class="status">ONLINE</span>
                        @else
                            <span class="status">OFFLINE</span>
                        @endif
                    </div>

                </li>
                @endif
                @endforeach
                @endif
            </ul>

        </div>

        <div class="search-friend inline-items">
            <form class="form-group" >
                <input class="form-control" placeholder="Search Friends..." value="" type="text">
            </form>

            <a href="{{ route('profile.show', app()->getLocale()) }}" class="settings">
                <svg class="olymp-settings-icon"><use xlink:href="#olymp-settings-icon"></use></svg>
            </a>

            <a href="#" class="js-sidebar-open">
                <svg class="olymp-close-icon"><use xlink:href="#olymp-close-icon"></use></svg>
            </a>
        </div>

        <a href="#" class="olympus-chat inline-items">

            <h6 class="olympus-chat-title">{{ __('big_family') }}</h6>
            <svg class="olymp-chat---messages-icon"><use xlink:href="#olymp-chat---messages-icon"></use></svg>
        </a>
    </div>

</div>

<!-- ... end Fixed Sidebar Right-Responsive -->

<!-- Window-popup-CHAT for responsive min-width: 768px -->

<div class="ui-block popup-chat popup-chat-responsive" id="Msg_Block" tabindex="-1" role="dialog" aria-labelledby="popup-chat-responsive" aria-hidden="true">

    <div class="modal-content">
        <div class="modal-header">
            <span class="icon-status online"></span>
            <h6 class="title" id="chat-title">Chat User</h6>
            <input type="hidden" id="chat-user" value="0">
            <input type="hidden" id="current-user" value="{{auth()->id()}}">
            <input type="hidden" id="chat-Msg-id" value="0">
            <div class="more">
                <svg class="olymp-three-dots-icon"><use xlink:href="#olymp-three-dots-icon"></use></svg>
                <svg class="olymp-little-delete js-chat-open"><use xlink:href="#olymp-little-delete"></use></svg>
            </div>
        </div>
        <div class="modal-body" id="allMsgCont">
            <div class="mCustomScrollbar" id="gotLast">
                <ul class="notification-list chat-message chat-message-field" id="Msgs">
                </ul>
            </div>
                <div class="form-group">
                    <textarea class="form-control" placeholder="Press enter to post..." id="MsgContent"></textarea>
                    <div class="add-options-message">
                        
                        <!-- <div class="options-message smile-block">

                            <svg class="olymp-happy-sticker-icon"><use xlink:href="#olymp-happy-sticker-icon"></use></svg>

                            <ul class="more-dropdown more-with-triangle triangle-bottom-right">
                                <li>
                                    <a href="#">
                                        <img loading="lazy" src="{{asset('img/icon-chat1.webp')}}" alt="icon" width="20" height="20">
                                    </a>
                                </li>

                            </ul>
                        </div> -->
                    </div>
                    <a onClick="sendMsgNow()" class="form-control btn btn-secondary" id="send_msg">send</a>
                </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    function sendMsgNow(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
            var MsgBody = document.getElementById("MsgContent").value;
            var MsgToUser = document.getElementById("chat-user").value;
            var MsgId = document.getElementById("chat-Msg-id").value;
            $.ajax({
               type:'POST',
               url:"{{ route('add_user_msg') }}",
               data:{body:MsgBody,touser:MsgToUser,msgid:MsgId},
               success:function(data){
                  //alert(data.success);
                  var today = new Date();
                    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                    var dateTime = date+' '+time;

                  var ul = document.getElementById("Msgs");
                    var li = document.createElement("li");
                    var  picDiv = document.createElement("div");
                          picDiv.setAttribute("class","author-thumb");
                          picDiv.setAttribute("id","author-"+MsgId);
                    var   pic = document.createElement("img");
                          pic.setAttribute("loading","lazy");
                          pic.setAttribute("class","mCS_img_loaded");
                          pic.setAttribute("src", data.peofile_photo_path);
                          pic.setAttribute("width","28");
                          pic.setAttribute("height","28");
                          pic.setAttribute("alt","author"+data.name);
                          picDiv.appendChild(pic);
                    var  firstDiv = document.createElement("div");
                         firstDiv.setAttribute("class","notification-event");
                        if(data.id == MsgToUser){
                          firstDiv.setAttribute("class","popup-chat pull-right");
                            }
                          
                          firstDiv.setAttribute("id","notification-"+MsgId);
                    var  secondDiv = document.createElement("span");
                    if(data.id != MsgToUser){
                          secondDiv.setAttribute("class","chat-message-item-me");
                      }else{
                        secondDiv.setAttribute("class","chat-message-item");
                      }
                          secondDiv.setAttribute("id","chat-message-"+MsgId);
                          secondDiv.innerHTML=MsgBody;
                          firstDiv.appendChild(secondDiv);
                    var  thirdDiv = document.createElement("span");
                          thirdDiv.setAttribute("class","chat-message-item");
                          thirdDiv.setAttribute("id","chat-message-"+MsgId);
                          thirdDiv.innerHTML=dateTime;
                          firstDiv.appendChild(thirdDiv);
                          
                          if(data.id != MsgToUser){
                            li.appendChild(picDiv);
                            li.appendChild(firstDiv);
                          }else{
                            li.appendChild(firstDiv);
                            li.appendChild(picDiv);
                          }

                          li.appendChild(firstDiv);
                          ul.appendChild(li);
                          
                          $('#gotLast').animate({scrollTop: $('#gotLast').prop("scrollHeight")}, 500);

                    document.getElementById("MsgContent").value = "";
                    var element = document.getElementById('allMsgCont');
        element.scrollTop = element.scrollHeight;
               }
      
        });
    }
</script>
<!-- ... end Window-popup-CHAT for responsive min-width: 768px -->



</div>
