<div>

    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

    <!-- Main Header Groups -->

    <div class="main-header">
        <div class="content-bg-wrap bg-group" style="background-image: {{ asset('img/bg-group.png') }};"></div>
        <div class="container">
            <div class="row">
                <div class="col col-lg-8 m-auto col-md-8 col-sm-12 col-12">
                    <div class="main-header-content">
                        <h1>{{ __('tree_builder') }}</h1>
                        <p>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <img loading="lazy" class="img-bottom" src="{{ asset('img/group-bottom.webp') }}" alt="friends" width="1087"
            height="148">
    </div>

    <!-- ... end Main Header Groups -->

    <div class="container">
        <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="ui-block responsive-flex">
                    <div class="ui-block-title text-center">
                        <div class="h6 title">{{ __('tree_builder') . strtoupper($user->getFamilyName()) ?? '' }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="accordion bg-grey" id="accordionE">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingT">
                            <button class="accordion-button collapsed text-white" type="button"
                                data-bs-toggle="collapse" data-bs-target="#collapseT" aria-expanded="false"
                                aria-controls="collapseT"
                                style="background-image: url({{ asset('img/default/clickhere.png') }});background-repeat: no-repeat;">
                                {{ __('see_help_vid') }}
                            </button>
                        </h2>

                        <div id="collapseT" class="accordion-collapse collapse" aria-labelledby="headingT"
                            data-bs-parent="#accordionE">
                            <div class="accordion-body">
                                <ul class="widget w-last-video">
                                    @foreach ($VidHelp as $vid)
                                        <li>
                                            <h6 style="color:#39A9FF">{{ $vid->title }}</h6>
                                            <a href="{{ asset($vid->video) }}" class="play-video play-video--small">
                                                <svg class="olymp-play-icon">
                                                    <use xlink:href="#olymp-play-icon"></use>
                                                </svg>
                                            </a>
                                            <img loading="lazy" src="{{ asset($vid->image) }}" alt="video"
                                                width="272" height="181">
                                            <div class="video-content">
                                                <div class="title">{{ $vid->title }}</div>
                                                <time class="published"
                                                    datetime="2017-03-24T18:18">{{ $vid->lenght }}</time>
                                            </div>
                                            <div class="overlay"></div>
                                        </li>
                                        <li> </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <div id="tree" class="mx-auto my-5">
        <div class="white rgba-white-strong rounded">
            <div class="py-12 text-center">
                <!-- Button trigger modal -->
                @if (session()->has('message'))
                    <div id="mydiv" class="alert alert-success text-center h4">
                        {{ session('message') }}
                    </div>
                @endif

                @if ($user->mother && $user->father)
                    <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#sisterAdd">
                        <i class="fas fa-user-plus"></i> <br />{{ __('add_sister') }} -> {{ $user->name }}
                    </button>
                @else
                    <button type="button" class="btn btn-danger">
                        <i class="fad fa-ban"></i> {{ __('sister') }} -> {{ $user->name }}<br />
                        {{ __('add_father_mother') }}
                    </button>
                @endif



                @if ($user->mother && $user->father)
                    <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#brotherAdd">
                        <i class="fas fa-user-plus"></i> <br />{{ __('add_brother') }} -> {{ $user->name }}
                    </button>
                @else
                    <button type="button" class="btn btn-danger">
                        <i class="fad fa-ban"></i> {{ __('brother') }} -> {{ $user->name }}<br />
                        {{ __('add_father_mother') }}
                    </button>
                @endif

                @if ($user->gender == 'male')
                    <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#WifeAdd">
                        <i class="fas fa-user-plus"></i> <br />{{ __('add_wife') }} -> {{ $user->name }}
                    </button>
                @else
                    @if ($user->husbands()->count() == $user->divorcedHusbands->count())
                        <button type="button" class="btn btn-success" data-bs-toggle="modal"
                            data-bs-target="#addHusband" wire:click="$set('isEx',false)">
                            <i class="fas fa-user-plus"></i> <br />{{ __('add_husband') }} -> {{ $user->name }}
                        </button>
                    @endif
                @endif
            </div>

            <div class="tree mx-auto my-5" id="treee">
                <div class="white rgba-white-strong rounded">
                    <ul>
                        <li>
                            <!-- grandmother location -->
                            @if ($user->mother)
                                <div class="card flex-row flex-wrap w-100"
                                    style=" border: solid 1px #000;border-radius: 5px;">

                                    <div class="card-header border-0">
                                        @if ($user->mother->profile_photo_path)
                                            <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto rounded-circle"
                                                src="{{ asset($user->mother->profile_photo_path) }}"
                                                alt="{{ $user->mother->name ?? '' }}" />
                                        @else
                                            <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto"
                                                src="{{ asset('img/default/user_female_1.png') }}"
                                                alt="add mother pic" />
                                        @endif

                                        <!-- edit user / upload pic -->
                                        <div class="overlay">
                                            <!-- edit btn -->
                                            <a class="btn btn-success" data-bs-toggle="modal"
                                                data-bs-target="#userEdit"
                                                wire:click="updateUserInfo({{ $user->mother->id }})"
                                                title="{{ __('edit_user') }}"><i class="fa fa-pencil"></i></a>
                                            <!-- add pic btn -->
                                            <a class="btn btn-secondary" data-bs-toggle="modal"
                                                data-bs-target="#userPic"
                                                wire:click="updateUserInfo({{ $user->mother->id }})"
                                                class="icon pull-left" title="{{ __('add_pic') }}"><i
                                                    class="fa fa-camera-retro" aria-hidden="true"></i></a>
                                            <!-- edit marriage -->
                                            <a class="btn btn-purple" data-bs-toggle="modal"
                                                data-bs-target="#marEdit"
                                                wire:click="updateMarInfo({{ $user->mother->id }},{{ $user->father->id }})"
                                                title="{{ __('mar_user') }}"><i class="fa fa-clock"></i></a>
                                            <!-- delete user btn -->
                                            <a class="btn btn-danger"
                                                onclick="confirm(' this User will deleted permanently ?')"
                                                wire:click="deleteUser({{ $user->mother->id }})"
                                                class="icon pull-left" title="{{ __('del_user') }}">
                                                <i class="fa fa-trash"></i></a>
                                            <!-- <a href="whatsapp://send?text=The text to share!" data-action="share/whatsapp/share">Share via Whatsapp</a> -->

                                            <!-- <a href="https://wa.me/00201001191955/?text=try%20this">dd</a> -->
                                        </div>
                                        <!-- edit user / upload pic end -->

                                        @if ($user->mother->death_date)
                                            <span class="badge badge-die">{{ __('departed') }}</span>
                                        @else
                                            <span class="badge badge-alive">{{ __('alive') }}</span>
                                        @endif

                                    </div>
                                    <div class="card-block px-2 bg-gray-100 w-50">
                                        <h4 class="card-title font-semibold">{{ $user->mother->name ?? '' }}</h4>
                                        <p class="card-text">
                                            @if ($user->mother->getTextualAge())
                                                @if ($user->mother->death_date)
                                                    {{ $user->mother->getDieAge() }}
                                                @else
                                                    {{ $user->mother->getTextualAge() }}
                                                @endif
                                            @else
                                                {{ __('age_null') }}
                                            @endif
                                        </p>
                                        <a href="{{ route('tree_view', [app()->getLocale(), $user->mother->id]) }}"
                                            class="btn btn-primary">{{ __('buttons') }}</a>
                                    </div>

                                    <div class="w-100">
                                        @if ($user->mother->death_date)
                                            {{ $user->mother->getDieSince() }}
                                        @else
                                            {{ $user->mother->phone_number ?? __('mobile_number_null') }}
                                        @endif
                                    </div>
                                    <div class="card-footer w-100 text-muted">
                                        @if ($marriage_time)
                                            {{ __('marriage_since') }} {{ $marriage_time }} {{ __('years') }}
                                        @else
                                            {{ __('unknown_marrige_time') }}
                                        @endif
                                    </div>
                                    <div class="w-100">
                                        <?php if ($user->mother->phone_number) {
                                            $number = $user->mother->country->phonecode . (int) $user->mother->phone_number;
                                        } else {
                                            $number = '0';
                                        }
                                        ?>
                                        @if ($number != '0')
                                            <a target="_blank"
                                                href="https://web.whatsapp.com/send?phone={{ $number }}&text={{ __('try_site') }}">
                                                <img src="{{ asset('img/default/whatsapp.jpg') }}"
                                                    title="Invite via WhatsApp" style="width: 20px;" /></a>
                                        @else
                                            <img src="{{ asset('img/default/nowhatsapp.jpg') }}"
                                                title="Invite via WhatsApp" style="width: 20px;" />
                                        @endif
                                    </div>
                                </div>
                            @else
                                @if ($user->father)
                                    <div class="card flex-row flex-wrap bg-danger">
                                        <div class="card-header border-0">
                                            <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto"
                                                src="{{ url('img/default/user_female_1.png') }}" alt="" />
                                        </div>
                                        <div class="card-block px-2 bg-gray-100 w-50">
                                            <h4 class="card-title">{{ __('add_mother') }}</h4>
                                            <p class="card-text">{{ __('missing_data') }}</p>
                                            <button type="button" class="btn btn-success" data-bs-toggle="modal"
                                                data-bs-target="#addMotherModal"
                                                wire:click="setParentGenderAndLabel('female')">{{ __('press_here') }}</button>
                                        </div>
                                        <div class="w-100"></div>
                                        <div class="card-footer w-100 text-muted">
                                            {{ __('unknown_marrige_time') }}
                                        </div>
                                    </div>
                                @else
                                    <div class="card flex-row flex-wrap bg-danger">
                                        <div class="card-header border-0">
                                            <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto"
                                                src="{{ url('img/default/user_female_1.png') }}" alt=""
                                                style="background-color:white" />
                                        </div>
                                        <div class="card-block px-2 bg-gray-100 w-50">
                                            <h4 class="card-title">{{ __('add_mother') }}</h4>
                                            <p class="card-text">{{ __('missing_data') }}</p>
                                            <button type="button" class="btn btn-danger" data-bs-toggle="modal"
                                                data-bs-target="#addFatherModal"
                                                wire:click="setParentGenderAndLabel('male')">{{ __('father_first') }}</button>
                                        </div>
                                    </div>
                                @endif
                            @endif

                            @if ($user->father)
                                <!-- grand father location -->
                                <div class="card flex-row flex-wrap"
                                    style=" border: solid 1px #000;border-radius: 5px;">
                                    <div class="card-header border-0">
                                        @if ($user->father->profile_photo_path)
                                            <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto rounded-circle"
                                                src="{{ asset($user->father->profile_photo_path) }}"
                                                alt="{{ $user->father->name ?? '' }}" />
                                        @else
                                            <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto"
                                                src="{{ url('img/default/user_male_1.png') }}"
                                                alt="add father pic" />
                                        @endif

                                        <!-- edit user / upload pic -->
                                        <div class="overlay">
                                            <!-- edit btn -->
                                            <a class="btn btn-success" data-bs-toggle="modal"
                                                data-bs-target="#userEdit"
                                                wire:click="updateUserInfo({{ $user->father->id }})"
                                                title="{{ __('edit_user') }}"><i class="fa fa-pencil fa-lg"></i></a>
                                            <!-- add pic btn -->
                                            <a class="btn btn-secondary" data-bs-toggle="modal"
                                                data-bs-target="#userPic"
                                                wire:click="updateUserInfo({{ $user->father->id }})"
                                                class="icon pull-left" title="{{ __('add_pic') }}"><i
                                                    class="fa fa-camera-retro" aria-hidden="true"></i></a>
                                            <!-- delete user btn -->
                                            <a class="btn btn-danger"
                                                onclick="confirm(' this User will deleted permanently ?')"
                                                wire:click="deleteUser({{ $user->father->id }})"
                                                class="icon pull-left" title="{{ __('del_user') }}">
                                                <i class="fa fa-trash"></i></a>
                                        </div>
                                        <!-- edit user / upload pic end -->

                                        @if ($user->father->death_date)
                                            <span class="badge badge-die">{{ __('departed') }}</span>
                                        @else
                                            <span class="badge badge-alive">{{ __('alive') }}</span>
                                        @endif
                                    </div>
                                    <div class="card-block px-2 bg-gray-100 w-50">
                                        <h4 class="card-title font-semibold">{{ $user->father->name ?? '' }}</h4>
                                        <p class="card-text">
                                            @if ($user->father->getTextualAge())
                                                @if ($user->father->death_date)
                                                    {{ $user->father->getDieAge() }}
                                                @else
                                                    {{ $user->father->getTextualAge() }}
                                                @endif
                                            @else
                                                {{ __('age_null') }}
                                            @endif
                                        </p>
                                        <a href="{{ route('tree_view', [app()->getLocale(), $user->father->id]) }}"
                                            class="btn btn-primary">{{ __('buttons') }}</a>
                                    </div>
                                    <div class="w-100">
                                        @if ($user->father->death_date)
                                            {{ $user->father->getDieSince() }}
                                        @else
                                            {{ $user->father->phone_number ?? __('mobile_number_null') }}
                                        @endif
                                    </div>
                                    <div class="card-footer w-100 text-muted">
                                        {{ __('married') . ' ' . $user->father->wives()->count() . ' ' . __('wife') }}
                                    </div>
                                    <div class="w-100">
                                        <?php if ($user->father->phone_number) {
                                            $number = $user->father->country->phonecode . (int) $user->father->phone_number;
                                        } else {
                                            $number = '0';
                                        }
                                        ?>
                                        @if ($number != '0')
                                            <a target="_blank"
                                                href="https://web.whatsapp.com/send?phone={{ $number }}&text={{ __('try_site') }}">
                                                <img src="{{ asset('img/default/whatsapp.jpg') }}"
                                                    title="Invite via WhatsApp" style="width: 20px;" /></a>
                                        @else
                                            <img src="{{ asset('img/default/nowhatsapp.jpg') }}"
                                                title="Invite via WhatsApp" style="width: 20px;" />
                                        @endif
                                    </div>
                                </div>
                            @else
                                <div class="card flex-row flex-wrap bg-danger">
                                    <div class="card-header border-0">
                                        <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto"
                                            src="{{ url('img/default/user_male_1.png') }}" alt=""
                                            style="background-color:white" />
                                    </div>
                                    <div class="card-block px-2 bg-gray-100 w-50">
                                        <h4 class="card-title">{{ __('add_father') }}</h4>
                                        <p class="card-text">{{ __('missing_data') }}</p>
                                        <button type="button" class="btn btn-success" data-bs-toggle="modal"
                                            data-bs-target="#addFatherModal"
                                            wire:click="setParentGenderAndLabel('male')">{{ __('press_here') }}</button>
                                    </div>
                                </div>
                            @endif
                            <ul>
                                @if (!empty($sisters))
                                    <!-- sister location start -->
                                    @foreach ($sisters as $sister)
                                        <li>
                                            <div class="card flex-row flex-wrap"
                                                style=" border: solid 1px #000;border-radius: 5px;">
                                                <div class="card-header border-0">
                                                    @if ($sister->profile_photo_path)
                                                        <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto rounded-circle"
                                                            src="{{ asset($sister->profile_photo_path) }}"
                                                            alt="" />
                                                    @else
                                                        <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto"
                                                            src="{{ url('img/default/user_female.png') }}"
                                                            alt="" />
                                                    @endif

                                                    <!-- edit user / upload pic -->
                                                    <div class="overlay">
                                                        <!-- edit btn -->
                                                        <a class="btn btn-success" data-bs-toggle="modal"
                                                            data-bs-target="#userEdit"
                                                            wire:click="updateUserInfo({{ $sister->id }})"
                                                            title="{{ __('edit_user') }}"><i
                                                                class="fa fa-pencil fa-lg"></i></a>
                                                        <!-- add pic btn -->
                                                        <a class="btn btn-secondary" data-bs-toggle="modal"
                                                            data-bs-target="#userPic"
                                                            wire:click="updateUserInfo({{ $sister->id }})"
                                                            class="icon pull-left" title="{{ __('add_pic') }}"><i
                                                                class="fa fa-camera-retro" aria-hidden="true"></i></a>
                                                        <!-- delete user btn -->
                                                        @if ($sister->husbands()->count() == 0)
                                                            <a class="btn btn-danger"
                                                                onclick="confirm(' this User will deleted permanently ?')"
                                                                wire:click="deleteUser({{ $sister->id }})"
                                                                class="icon pull-left" title="{{ __('del_user') }}">
                                                                <i class="fa fa-trash"></i></a>
                                                        @endif
                                                    </div>
                                                    <!-- edit user / upload pic end -->

                                                    @if ($sister->death_date)
                                                        <span class="badge badge-die">{{ __('departed') }}</span>
                                                    @else
                                                        <span class="badge badge-alive">{{ __('alive') }}</span>
                                                    @endif
                                                </div>
                                                <div class="card-block px-2 bg-gray-100 w-50">
                                                    <h4 class="card-title font-semibold">{{ $sister->name ?? '' }}
                                                    </h4>
                                                    <p class="card-text">
                                                        @if ($sister->getTextualAge())
                                                            @if ($sister->death_date)
                                                                {{ $sister->getDieAge() }}
                                                            @else
                                                                {{ $sister->getTextualAge() }}
                                                            @endif
                                                        @else
                                                            {{ __('age_null') }}
                                                        @endif
                                                    </p>
                                                    <a href="{{ route('tree_view', [app()->getLocale(), $sister->id]) }}"
                                                        class="btn btn-primary">{{ __('buttons') }}</a>
                                                </div>
                                                <div class="w-100">
                                                    @if ($sister->death_date)
                                                        {{ $sister->getDieSince() }}
                                                    @else
                                                        {{ $sister->phone_number ?? __('mobile_number_null') }}
                                                    @endif
                                                </div>
                                                <div class="card-footer w-100 text-muted">
                                                    {{ __('marriedf') . ' ' . $sister->husbands()->count() . ' ' . __('husband') }}
                                                </div>
                                                <div class="w-100">
                                                    <?php if ($sister->phone_number) {
                                                        $number = $sister->country->phonecode . (int) $sister->phone_number;
                                                    } else {
                                                        $number = '0';
                                                    }
                                                    ?>
                                                    @if ($number != '0')
                                                        <a target="_blank"
                                                            href="https://web.whatsapp.com/send?phone={{ $number }}&text={{ __('try_site') }}">
                                                            <img src="{{ asset('img/default/whatsapp.jpg') }}"
                                                                title="Invite via WhatsApp"
                                                                style="width: 20px;" /></a>
                                                    @else
                                                        <img src="{{ asset('img/default/nowhatsapp.jpg') }}"
                                                            title="Invite via WhatsApp" style="width: 20px;" />
                                                    @endif
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                    <!-- sister location start -->
                                @endif


                                <!-- my Location start -->
                                <li>
                                    <div class="card flex-row flex-wrap me" id="myPlace"
                                        style=" border: solid 1px #000;border-radius: 5px;">
                                        <div class="card-header border-0">
                                            @if ($user->profile_photo_path)
                                                <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto mr-3 rounded-circle"
                                                    src="{{ asset($user->profile_photo_path) }}" alt="" />
                                            @else
                                                <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto"
                                                    src="{{ url('img/default/user_' . $user->gender . '.png') }}"
                                                    alt="" />
                                            @endif

                                            <!-- edit user / upload pic -->
                                            <div class="overlay">
                                                <!-- edit btn -->
                                                <a class="btn btn-success" data-bs-toggle="modal"
                                                    data-bs-target="#userEdit"
                                                    wire:click="updateUserInfo({{ $user->id }})"
                                                    title="{{ __('edit_user') }}"><i
                                                        class="fa fa-pencil fa-lg"></i></a>
                                                <!-- add pic btn -->
                                                <a class="btn btn-secondary" data-bs-toggle="modal"
                                                    data-bs-target="#userPic"
                                                    wire:click="updateUserInfo({{ $user->id }})"
                                                    class="icon pull-left" title="{{ __('add_pic') }}"><i
                                                        class="fa fa-camera-retro" aria-hidden="true"></i></a>
                                            </div>
                                            <!-- edit user / upload pic end -->

                                            @if ($user->death_date)
                                                <span class="badge badge-die">{{ __('departed') }}</span>
                                            @else
                                                <span class="badge badge-alive">{{ __('alive') }}</span>
                                            @endif

                                        </div>
                                        <div class="card-block px-2 bg-gray-100 w-50">
                                            <h4 class="card-title font-semibold">{{ $user->name ?? '' }}</h4>
                                            <img id='fag'
                                                src="{{ asset('img/flag/' . strtolower($user->country->iso) . '.svg') }}"
                                                style="width: 20px!important;">
                                            <p class="card-text">
                                                @if ($user->getTextualAge())
                                                    @if ($user->death_date)
                                                        {{ $user->getDieAge() }}
                                                    @else
                                                        {{ $user->getTextualAge() }}
                                                    @endif
                                                @else
                                                    {{ __('age_null') }}
                                                @endif
                                            </p>
                                            <a class="btn btn-secondary">{{ strtoupper($user->getFamilyName()) }}</a>
                                        </div>
                                        <div class="w-100">
                                            @if ($user->death_date)
                                                {{ $user->getDieSince() }}
                                            @else
                                                {{ $user->phone_number ?? __('mobile_number_null') }}
                                            @endif
                                        </div>
                                        <div class="w-100">
                                            {{ $user->getFamilyCount() }}
                                        </div>
                                        <div class="w-100">
                                            <?php if ($user->phone_number) {
                                                $number = $user->country->phonecode . (int) $user->phone_number;
                                            } else {
                                                $number = '0';
                                            }
                                            ?>
                                            @if ($number != '0')
                                                <a target="_blank"
                                                    href="https://web.whatsapp.com/send?phone={{ $number }}&text={{ __('try_site') }}">
                                                    <img src="{{ asset('img/default/whatsapp.jpg') }}"
                                                        title="Invite via WhatsApp" style="width: 20px;" /></a>
                                            @else
                                                <img src="{{ asset('img/default/nowhatsapp.jpg') }}"
                                                    title="Invite via WhatsApp" style="width: 20px;" />
                                            @endif
                                        </div>
                                    </div>
                                    <ul>
                                        @if ($user->gender == 'male')
                                            @foreach ($user->wives as $wife)
                                                <!-- my wivies start-->
                                                <li>
                                                    <div class="card flex-row flex-wrap"
                                                        style=" border: solid 1px #000;border-radius: 5px;">
                                                        <div class="card-header border-0">
                                                            @if ($wife->profile_photo_path)
                                                                <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto rounded-circle"
                                                                    src="{{ asset($wife->profile_photo_path) }}"
                                                                    alt="" />
                                                            @else
                                                                <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto"
                                                                    src="{{ url('img/default/user_female.png') }}"
                                                                    alt="" />
                                                            @endif

                                                            <!-- edit user / upload pic -->
                                                            <div class="overlay">
                                                                <!-- edit btn -->
                                                                <a class="btn btn-success" data-bs-toggle="modal"
                                                                    data-bs-target="#userEdit"
                                                                    wire:click="updateUserInfo({{ $wife->id }})"
                                                                    title="{{ __('edit_user') }}"><i
                                                                        class="fa fa-pencil fa-lg"></i></a>
                                                                <!-- add pic btn -->
                                                                <a class="btn btn-secondary" data-bs-toggle="modal"
                                                                    data-bs-target="#userPic"
                                                                    wire:click="updateUserInfo({{ $wife->id }})"
                                                                    class="icon pull-left"
                                                                    title="{{ __('add_pic') }}"><i
                                                                        class="fa fa-camera-retro"
                                                                        aria-hidden="true"></i></a>
                                                                <!-- edit marriage -->
                                                                <a class="btn btn-purple" data-bs-toggle="modal"
                                                                    data-bs-target="#marEdit"
                                                                    wire:click="updateMarInfo({{ $wife->id }},{{ $user->id }})"
                                                                    title="{{ __('mar_user') }}"><i
                                                                        class="fa fa-clock"></i></a>
                                                                <!-- delete user btn -->
                                                                @if (empty($wife->wife_children))
                                                                    <a class="btn btn-danger"
                                                                        onclick="confirm(' this User will deleted permanently ?')"
                                                                        wire:click="deleteUser({{ $wife->id }})"
                                                                        class="icon pull-left"
                                                                        title="{{ __('del_user') }}">
                                                                        <i class="fa fa-trash"></i></a>
                                                                @endif
                                                            </div>
                                                            <!-- edit user / upload pic end -->

                                                            @if ($wife->death_date)
                                                                <span
                                                                    class="badge badge-die">{{ __('departed') }}</span>
                                                            @else
                                                                <span
                                                                    class="badge badge-alive">{{ __('alive') }}</span>
                                                            @endif
                                                        </div>
                                                        <div class="card-block px-2 bg-gray-100 w-50">
                                                            <h4 class="card-title font-semibold">
                                                                {{ $wife->name ?? '' }}</h4>
                                                            <p class="card-text">
                                                                @if ($wife->getTextualAge())
                                                                    @if ($wife->death_date)
                                                                        {{ $wife->getDieAge() }}
                                                                    @else
                                                                        {{ $wife->getTextualAge() }}
                                                                    @endif
                                                                @else
                                                                    {{ __('age_null') }}
                                                                @endif
                                                            </p>
                                                            <a href="{{ route('tree_view', [app()->getLocale(), $wife->id]) }}"
                                                                class="btn btn-primary">{{ __('buttons') }}</a>
                                                        </div>
                                                        <div class="w-100 {{ $user->gender }}">
                                                            @if ($wife->death_date)
                                                                {{ $wife->getDieSince() }}
                                                            @else
                                                                {{ $wife->phone_number ?? __('mobile_number_null') }}
                                                            @endif
                                                        </div>
                                                        <div
                                                            class="card-footer w-100 text-muted {{ !$user->isAlreadyMarriedWife($wife) ? 'ex' : 'female' }}">
                                                            @if (!$user->isAlreadyMarriedWife($wife))
                                                                {{ __('marrige_stay') }}
                                                            @else
                                                                {{ __('marriedf') }}
                                                            @endif
                                                            {{ $wife->marriage->getMarriageTextualDate() }}
                                                        </div>
                                                        <div class="w-100">
                                                            <?php if ($wife->phone_number) {
                                                                $number = $wife->country->phonecode . (int) $wife->phone_number;
                                                            } else {
                                                                $number = '0';
                                                            }
                                                            ?>
                                                            @if ($number != '0')
                                                                <a target="_blank"
                                                                    href="https://web.whatsapp.com/send?phone={{ $number }}&text={{ __('try_site') }}">
                                                                    <img src="{{ asset('img/default/whatsapp.jpg') }}"
                                                                        title="Invite via WhatsApp"
                                                                        style="width: 20px;" /></a>
                                                            @else
                                                                <img src="{{ asset('img/default/nowhatsapp.jpg') }}"
                                                                    title="Invite via WhatsApp"
                                                                    style="width: 20px;" />
                                                            @endif
                                                        </div>
                                                        <div class="card-footer w-100 text-muted">
                                                            <button type="button" class="btn btn-success"
                                                                data-bs-toggle="modal" data-bs-target="#childAdd"
                                                                wire:click="setFatherAndMotherForNewChild({{ $wife->id }},{{ $wife->marriage->user_id }})">
                                                                {{ __('add_child') }}</button>
                                                        </div>
                                                    </div>
                                                    <ul>
                                                        @if (!empty($wife->wife_children))
                                                            @foreach ($wife->wife_children as $child)
                                                                <!-- my childrens start -->
                                                                <li>
                                                                    <div class="card flex-row flex-wrap"
                                                                        style=" border: solid 1px #000;border-radius: 5px;">
                                                                        <div class="card-header border-0">
                                                                            @if ($child->profile_photo_path)
                                                                                <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto rounded-circle"
                                                                                    src="{{ asset($child->profile_photo_path) }}"
                                                                                    alt="" />
                                                                            @else
                                                                                <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto"
                                                                                    src="{{ url('img/default/user_' . $child->gender . '_3.png') }}"
                                                                                    alt="" />
                                                                            @endif

                                                                            <!-- edit user / upload pic -->
                                                                            <div class="overlay">
                                                                                <!-- edit btn -->
                                                                                <a class="btn btn-success"
                                                                                    data-bs-toggle="modal"
                                                                                    data-bs-target="#userEdit"
                                                                                    wire:click="updateUserInfo({{ $child->id }})"
                                                                                    title="{{ __('edit_user') }}"><i
                                                                                        class="fa fa-pencil fa-lg"></i></a>
                                                                                <!-- add pic btn -->
                                                                                <a class="btn btn-secondary"
                                                                                    data-bs-toggle="modal"
                                                                                    data-bs-target="#userPic"
                                                                                    wire:click="updateUserInfo({{ $child->id }})"
                                                                                    class="icon pull-left"
                                                                                    title="{{ __('add_pic') }}"><i
                                                                                        class="fa fa-camera-retro"
                                                                                        aria-hidden="true"></i></a>
                                                                                <!-- delete user btn -->
                                                                                @if ($child->wives()->count() == 0 && $child->husbands()->count() == 0)
                                                                                    <a class="btn btn-danger"
                                                                                        onclick="confirm(' this User will deleted permanently ?')"
                                                                                        wire:click="deleteUser({{ $child->id }})"
                                                                                        class="icon pull-left"
                                                                                        title="{{ __('del_user') }}">
                                                                                        <i class="fa fa-trash"></i></a>
                                                                                @endif
                                                                            </div>
                                                                            <!-- edit user / upload pic end -->

                                                                            @if ($child->death_date)
                                                                                <span
                                                                                    class="badge badge-die">{{ __('departed') }}</span>
                                                                            @else
                                                                                <span
                                                                                    class="badge badge-alive">{{ __('alive') }}</span>
                                                                            @endif
                                                                        </div>
                                                                        <div class="card-block px-2 bg-gray-100 w-50">
                                                                            <h4 class="card-title font-semibold">
                                                                                {{ $child->name ?? '' }}</h4>
                                                                            <p class="card-text">
                                                                                @if ($child->getTextualAge())
                                                                                    @if ($child->death_date)
                                                                                        {{ $child->getDieAge() }}
                                                                                    @else
                                                                                        {{ $child->getTextualAge() }}
                                                                                    @endif
                                                                                @else
                                                                                    {{ __('age_null') }}
                                                                                @endif
                                                                            </p>
                                                                            <a href="{{ route('tree_view', [app()->getLocale(), $child->id]) }}"
                                                                                class="btn btn-primary">{{ __('buttons') }}</a>
                                                                        </div>
                                                                        <div class="w-100 {{ $child->gender }}">
                                                                            @if ($child->death_date)
                                                                                {{ $child->getDieSince() }}
                                                                            @else
                                                                                {{ $child->phone_number ?? __('mobile_number_null') }}
                                                                            @endif
                                                                        </div>
                                                                        <div
                                                                            class="card-footer w-100 text-muted {{ $child->gender }}">
                                                                            @if ($child->gender == 'male')
                                                                                {{ 'married ' . $child->wives()->count() . \Illuminate\Support\Str::plural(' Wife', $child->wives()->count()) }}
                                                                            @elseif($child->gender == 'female')
                                                                                {{ 'married ' . $child->husbands()->count() . \Illuminate\Support\Str::plural(' Husband', $child->husbands()->count()) }}
                                                                            @endif
                                                                        </div>
                                                                        <div class="w-100">
                                                                            <?php if ($child->phone_number) {
                                                                                $number = $child->country->phonecode . (int) $child->phone_number;
                                                                            } else {
                                                                                $number = '0';
                                                                            }
                                                                            ?>
                                                                            @if ($number != '0')
                                                                                <a target="_blank"
                                                                                    href="https://web.whatsapp.com/send?phone={{ $number }}&text={{ __('try_site') }}">
                                                                                    <img src="{{ asset('img/default/whatsapp.jpg') }}"
                                                                                        title="Invite via WhatsApp"
                                                                                        style="width: 20px;" /></a>
                                                                            @else
                                                                                <img src="{{ asset('img/default/nowhatsapp.jpg') }}"
                                                                                    title="Invite via WhatsApp"
                                                                                    style="width: 20px;" />
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <!-- my childrens end -->
                                                            @endforeach
                                                        @endif
                                                    </ul>
                                                </li>
                                            @endforeach
                                        @elseif($user->gender == 'female')
                                            @foreach ($user->husbands as $husband)
                                                <!-- my wivies start-->
                                                <li>
                                                    <div class="card flex-row flex-wrap"
                                                        style=" border: solid 1px #000;border-radius: 5px;">
                                                        <div class="card-header border-0">
                                                            @if ($husband->profile_photo_path)
                                                                <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto rounded-circle"
                                                                    src="{{ asset($husband->profile_photo_path) }}"
                                                                    alt="" />
                                                            @else
                                                                <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto"
                                                                    src="{{ url('img/default/user_male.png') }}"
                                                                    alt="" />
                                                            @endif

                                                            <!-- edit user / upload pic -->
                                                            <div class="overlay">
                                                                <!-- edit btn -->
                                                                <a class="btn btn-success" data-bs-toggle="modal"
                                                                    data-bs-target="#userEdit"
                                                                    wire:click="updateUserInfo({{ $husband->id }})"
                                                                    title="{{ __('edit_user') }}"><i
                                                                        class="fa fa-pencil fa-lg"></i></a>
                                                                <!-- add pic btn -->
                                                                <a class="btn btn-secondary" data-bs-toggle="modal"
                                                                    data-bs-target="#userPic"
                                                                    wire:click="updateUserInfo({{ $husband->id }})"
                                                                    class="icon pull-left"
                                                                    title="{{ __('add_pic') }}"><i
                                                                        class="fa fa-camera-retro"
                                                                        aria-hidden="true"></i></a>
                                                                <!-- edit marriage -->
                                                                <a class="btn btn-purple" data-bs-toggle="modal"
                                                                    data-bs-target="#marEdit"
                                                                    wire:click="updateMarInfo({{ $user->id }},{{ $husband->id }})"
                                                                    title="{{ __('mar_user') }}"><i
                                                                        class="fa fa-clock"></i></a>
                                                                <!-- delete user btn -->
                                                                @if ($user->wifeChildrenFromHusband($husband)->isEmpty())
                                                                    <a class="btn btn-danger"
                                                                        onclick="confirm(' this User will deleted permanently ?')"
                                                                        wire:click="deleteUser({{ $husband->id }})"
                                                                        class="icon pull-left"
                                                                        title="{{ __('del_user') }}">
                                                                        <i class="fa fa-trash"></i></a>
                                                                @endif
                                                            </div>
                                                            <!-- edit user / upload pic end -->

                                                            @if ($husband->death_date)
                                                                <span
                                                                    class="badge badge-die">{{ __('departed') }}</span>
                                                            @else
                                                                <span
                                                                    class="badge badge-alive">{{ __('alive') }}</span>
                                                            @endif
                                                        </div>
                                                        <div class="card-block px-2 bg-gray-100 w-50">
                                                            <h4 class="card-title font-semibold">
                                                                {{ $husband->name ?? '' }}</h4>
                                                            <p class="card-text">
                                                                @if ($husband->getTextualAge())
                                                                    @if ($husband->death_date)
                                                                        {{ $husband->getDieAge() }}
                                                                    @else
                                                                        {{ $husband->getTextualAge() }}
                                                                    @endif
                                                                @else
                                                                    {{ __('age_null') }}
                                                                @endif
                                                            </p>

                                                            <a href="{{ route('tree_view', [app()->getLocale(), $husband->id]) }}"
                                                                class="btn btn-primary">{{ __('buttons') }}</a>
                                                        </div>
                                                        <div class="w-100 {{ $husband->gender }}">
                                                            @if ($husband->death_date)
                                                                {{ $husband->getDieSince() }}
                                                            @else
                                                                {{ $husband->phone_number ?? __('mobile_number_null') }}
                                                            @endif
                                                        </div>
                                                        <div
                                                            class="card-footer w-100 text-muted {{ !$user->isAlreadyMarriedToHusband($husband) ? 'ex' : 'male' }}">
                                                            @if (!$user->isAlreadyMarriedToHusband($husband))
                                                                {{ __('marrige_stay') }}
                                                            @else
                                                                {{ __('married') }}
                                                            @endif
                                                            {{ $husband->husband->getMarriageTextualDate() }}
                                                        </div>
                                                        <div class="w-100">
                                                            <?php if ($husband->phone_number) {
                                                                $number = $husband->country->phonecode . (int) $husband->phone_number;
                                                            } else {
                                                                $number = '0';
                                                            }
                                                            ?>
                                                            @if ($number != '0')
                                                                <a target="_blank"
                                                                    href="https://web.whatsapp.com/send?phone={{ $number }}&text={{ __('try_site') }}">
                                                                    <img src="{{ asset('img/default/whatsapp.jpg') }}"
                                                                        title="Invite via WhatsApp"
                                                                        style="width: 20px;" /></a>
                                                            @else
                                                                <img src="{{ asset('img/default/nowhatsapp.jpg') }}"
                                                                    title="Invite via WhatsApp"
                                                                    style="width: 20px;" />
                                                            @endif
                                                        </div>

                                                        <div class="card-footer w-100 text-muted">
                                                            <button type="button" class="btn btn-success"
                                                                data-bs-toggle="modal" data-bs-target="#childAdd"
                                                                wire:click="setFatherAndMotherForNewChild({{ $husband->husband->wife_id }},{{ $husband->id }})">
                                                                {{ __('add_child') }}</button>
                                                        </div>
                                                    </div>
                                                    <ul>
                                                        @if (!empty($user->wifeChildrenFromHusband($husband)->isNotEmpty()))
                                                            @foreach ($user->wifeChildrenFromHusband($husband) as $child)
                                                                <!-- my childrens start -->
                                                                <li>
                                                                    <div class="card flex-row flex-wrap"
                                                                        style=" border: solid 1px #000;border-radius: 5px;">
                                                                        <div class="card-header border-0">
                                                                            @if ($child->profile_photo_path)
                                                                                <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto rounded-circle"
                                                                                    src="{{ asset($child->profile_photo_path) }}"
                                                                                    alt="" />
                                                                            @else
                                                                                <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto"
                                                                                    src="{{ url('img/default/user_' . $child->gender . '_3.png') }}"
                                                                                    alt="" />
                                                                            @endif

                                                                            <!-- edit user / upload pic -->
                                                                            <div class="overlay">
                                                                                <!-- edit btn -->
                                                                                <a class="btn btn-success"
                                                                                    data-bs-toggle="modal"
                                                                                    data-bs-target="#userEdit"
                                                                                    wire:click="updateUserInfo({{ $child->id }})"
                                                                                    title="{{ __('edit_user') }}"><i
                                                                                        class="fa fa-pencil fa-lg"></i></a>
                                                                                <!-- add pic btn -->
                                                                                <a class="btn btn-secondary"
                                                                                    data-bs-toggle="modal"
                                                                                    data-bs-target="#userPic"
                                                                                    wire:click="updateUserInfo({{ $child->id }})"
                                                                                    class="icon pull-left"
                                                                                    title="{{ __('add_pic') }}"><i
                                                                                        class="fa fa-camera-retro"
                                                                                        aria-hidden="true"></i></a>
                                                                                <!-- delete user btn -->
                                                                                @if ($child->wives()->count() == 0 && $child->husbands()->count() == 0)
                                                                                    <a class="btn btn-danger"
                                                                                        onclick="confirm(' this User will deleted permanently ?')"
                                                                                        wire:click="deleteUser({{ $child->id }})"
                                                                                        class="icon pull-left"
                                                                                        title="{{ __('del_user') }}">
                                                                                        <i class="fa fa-trash"></i></a>
                                                                                @endif
                                                                            </div>
                                                                            <!-- edit user / upload pic end -->

                                                                            @if ($child->death_date)
                                                                                <span
                                                                                    class="badge badge-die">{{ __('departed') }}</span>
                                                                            @else
                                                                                <span
                                                                                    class="badge badge-alive">{{ __('alive') }}</span>
                                                                            @endif
                                                                        </div>
                                                                        <div class="card-block px-2 bg-gray-100 w-50">
                                                                            <h4 class="card-title font-semibold">
                                                                                {{ $child->name ?? '' }}</h4>
                                                                            <p class="card-text">
                                                                                @if ($child->getTextualAge())
                                                                                    @if ($child->death_date)
                                                                                        {{ $child->getDieAge() }}
                                                                                    @else
                                                                                        {{ $child->getTextualAge() }}
                                                                                    @endif
                                                                                @else
                                                                                    {{ __('age_null') }}
                                                                                @endif
                                                                            </p>
                                                                            <a href="{{ route('tree_view', [app()->getLocale(), $child->id]) }}"
                                                                                class="btn btn-primary">{{ __('buttons') }}</a>
                                                                        </div>
                                                                        <div class="w-100 {{ $child->gender }}">
                                                                            @if ($child->death_date)
                                                                                {{ $child->getDieSince() }}
                                                                            @else
                                                                                {{ $child->phone_number ?? __('mobile_number_null') }}
                                                                            @endif
                                                                        </div>
                                                                        <div
                                                                            class="card-footer w-100 text-muted {{ $child->gender }}">
                                                                            @if ($child->gender == 'male')
                                                                                {{ 'married ' . $child->wives()->count() . \Illuminate\Support\Str::plural(' Wife', $child->wives()->count()) }}
                                                                            @elseif($child->gender == 'female')
                                                                                {{ 'married ' . $child->husbands()->count() . \Illuminate\Support\Str::plural(' Husband', $child->husbands()->count()) }}
                                                                            @endif
                                                                        </div>
                                                                        <div class="w-100">
                                                                            <?php if ($child->phone_number) {
                                                                                $number = $child->country->phonecode . (int) $child->phone_number;
                                                                            } else {
                                                                                $number = '0';
                                                                            }
                                                                            ?>
                                                                            @if ($number != '0')
                                                                                <a target="_blank"
                                                                                    href="https://web.whatsapp.com/send?phone={{ $number }}&text={{ __('try_site') }}">
                                                                                    <img src="{{ asset('img/default/whatsapp.jpg') }}"
                                                                                        title="Invite via WhatsApp"
                                                                                        style="width: 20px;" /></a>
                                                                            @else
                                                                                <img src="{{ asset('img/default/nowhatsapp.jpg') }}"
                                                                                    title="Invite via WhatsApp"
                                                                                    style="width: 20px;" />
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <!-- my childrens end -->
                                                            @endforeach
                                                        @endif
                                                    </ul>
                                                </li>
                                            @endforeach
                                        @endif
                                        <!-- my wivies end-->

                                    </ul>
                                </li>
                                <!-- my Location end -->

                                @if (!empty($brothers))
                                    <!-- brothers location start-->
                                    @foreach ($brothers as $brother)
                                        <li>
                                            <div class="card flex-row flex-wrap"
                                                style=" border: solid 1px #000;border-radius: 5px;">
                                                <div class="card-header border-0">
                                                    @if ($brother->profile_photo_path)
                                                        <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto rounded-circle"
                                                            src="{{ asset($brother->profile_photo_path) }}"
                                                            alt="" />
                                                    @else
                                                        <img class="rounded-full border-4 border-gray-400 h-26 w-16 mx-auto"
                                                            src="{{ url('img/default/user_male.png') }}"
                                                            alt="" />
                                                    @endif

                                                    <!-- edit user / upload pic -->
                                                    <div class="overlay">
                                                        <!-- edit btn -->
                                                        <a class="btn btn-success" data-bs-toggle="modal"
                                                            data-bs-target="#userEdit"
                                                            wire:click="updateUserInfo({{ $brother->id }})"
                                                            title="{{ __('edit_user') }}"><i
                                                                class="fa fa-pencil fa-lg"></i></a>
                                                        <!-- add pic btn -->
                                                        <a class="btn btn-secondary" data-bs-toggle="modal"
                                                            data-bs-target="#userPic"
                                                            wire:click="updateUserInfo({{ $brother->id }})"
                                                            class="icon pull-left" title="{{ __('add_pic') }}"><i
                                                                class="fa fa-camera-retro" aria-hidden="true"></i></a>
                                                        <!-- delete user btn -->
                                                        @if ($brother->wives()->count() == 0)
                                                            <a class="btn btn-danger"
                                                                onclick="confirm(' this User will deleted permanently ?')"
                                                                wire:click="deleteUser({{ $brother->id }})"
                                                                class="icon pull-left" title="{{ __('del_user') }}">
                                                                <i class="fa fa-trash"></i></a>
                                                        @endif
                                                    </div>
                                                    <!-- edit user / upload pic end -->

                                                    @if ($brother->death_date)
                                                        <span class="badge badge-die">{{ __('departed') }}</span>
                                                    @else
                                                        <span class="badge badge-alive">{{ __('alive') }}</span>
                                                    @endif
                                                </div>
                                                <div class="card-block px-2 bg-gray-100 w-50">
                                                    <h4 class="card-title font-semibold">{{ $brother->name ?? '' }}
                                                    </h4>
                                                    <p class="card-text">
                                                        @if ($brother->getTextualAge())
                                                            @if ($brother->death_date)
                                                                {{ $brother->getDieAge() }}
                                                            @else
                                                                {{ $brother->getTextualAge() }}
                                                            @endif
                                                        @else
                                                            {{ __('age_null') }}
                                                        @endif
                                                    </p>
                                                    <a href="{{ route('tree_view', [app()->getLocale(), $brother->id]) }}"
                                                        class="btn btn-primary">{{ __('buttons') }}</a>
                                                </div>
                                                <div class="w-100">
                                                    @if ($brother->death_date)
                                                        {{ $brother->getDieSince() }}
                                                    @else
                                                        {{ $brother->phone_number ?? __('mobile_number_null') }}
                                                    @endif
                                                </div>
                                                <div class="card-footer w-100 text-muted">
                                                    {{ __('married') . ' ' . $brother->wives()->count() . ' ' . __('wife') }}
                                                </div>
                                                <div class="w-100">
                                                    <?php if ($brother->phone_number) {
                                                        $number = $brother->country->phonecode . (int) $brother->phone_number;
                                                    } else {
                                                        $number = '0';
                                                    }
                                                    ?>
                                                    @if ($number != '0')
                                                        <a target="_blank"
                                                            href="https://web.whatsapp.com/send?phone={{ $number }}&text={{ __('try_site') }}">
                                                            <img src="{{ asset('img/default/whatsapp.jpg') }}"
                                                                title="Invite via WhatsApp"
                                                                style="width: 20px;" /></a>
                                                    @else
                                                        <img src="{{ asset('img/default/nowhatsapp.jpg') }}"
                                                            title="Invite via WhatsApp" style="width: 20px;" />
                                                    @endif
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                    <!-- brothers location end-->
                                @endif

                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!--    Father Modal start  -->
    <div wire:ignore.self class="modal fade" id="addFatherModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form wire:submit.prevent="addParents('male')" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('add_father') }}</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"
                            id="fatherAddCloseButton">
                            <span aria-hidden="true close-btn">×</span>
                        </button>
                    </div>

                    <div class="modal-body">

                        <div class="form-group">
                            <label for="exampleFormControlInput1">{{ __('father_name') }}</label> <span
                                class="text-red">* - {{ __('req') }} - *</span>
                            <input type="text" wire:model.defer="name" class="form-control"
                                placeholder="{{ __('insert') . ' ' . __('father_name') }}" required>
                            @error('name')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('living_country') }}</label> <span
                                class="text-red">* - {{ __('req') }} - *</span>
                            <select wire:model.defer="live" class="form-control">
                                <x-country_foreach :countries="$countries" />
                            </select>
                            @error('live')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">{{ __('email') }}</label> <span
                                class="text-green">* - {{ __('req2') }} - *</span>
                            <input type="text" wire:model.defer="email" class="form-control"
                                placeholder="Enter email">
                            @error('email')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('mobile_number') }}</label>
                            <input type="number" wire:model.defer="phone" class="form-control"
                                placeholder="{{ __('mobile_number') . ' ' . __('insert') }} (01 ..... )">
                            @error('phone')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('birthday') }}</label>
                            <input class="form-control" type="date" wire:model.defer="birth"
                                id="exampleFormControlInput5"
                                max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('leave_date') }}</label>
                            <input class="form-control" type="date" wire:model.defer="death"
                                id="exampleFormControlInput5"
                                max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close-btn"
                            data-bs-dismiss="modal">{{ __('cancel') }}</button>
                        <button type="submit" class="btn btn-primary close-modal">{{ __('save_changes') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--    Father Modal end  -->

    <!--     Mother Modal start -->
    <div wire:ignore.self class="modal fade" id="addMotherModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form wire:submit.prevent="addParents('female')" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('add_mother') }}</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"
                            id="motherAddCloseButton">
                            <span aria-hidden="true close-btn">×</span>
                        </button>
                    </div>

                    <div class="modal-body">

                        <div class="form-group">
                            <label for="exampleFormControlInput1">{{ __('mother_name') }}</label> <span
                                class="text-red">* - {{ __('req') }} - *</span>
                            <input type="text" wire:model.defer="name" class="form-control"
                                placeholder="{{ __('insert') . ' ' . __('mother_name') }}" required>
                            @error('parent_name')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('living_country') }}</label> <span
                                class="text-red">* - {{ __('req') }} - *</span>
                            <select wire:model.defer="live" class="form-control">
                                <x-country_foreach :countries="$countries" />
                            </select>
                            @error('parent_live')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">{{ __('email') }}</label>
                            <input type="text" wire:model.defer="email" class="form-control"
                                placeholder="Enter email">
                            @error('email')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('phone_number') }}</label> <span
                                class="text-green">* - {{ __('req2') }} - *</span>
                            <input type="number" wire:model.defer="phone" class="form-control"
                                placeholder="{{ __('insert') . ' ' . __('phone_number') }} (01 ..... )">
                            @error('parent_phone')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('marrige_time') }}</label>
                            <input type="date" wire:model.defer="marriage_start" class="form-control"
                                placeholder="pick a date" max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                            @error('parent_marriage_started_at')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('birthday') }}</label>
                            <input class="form-control" type="date" wire:model.defer="birth"
                                id="exampleFormControlInput5"
                                max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('leave_date') }}</label>
                            <input class="form-control" type="date" wire:model.defer="death"
                                id="exampleFormControlInput5"
                                max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close-btn"
                            data-bs-dismiss="modal">{{ __('cancel') }}</button>
                        <button type="submit" class="btn btn-primary close-modal">{{ __('save_changes') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--     Mother Modal end -->

    <!--     brother Modal start -->
    <div wire:ignore.self class="modal fade" id="brotherAdd" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form wire:submit.prevent="addSibling('male')" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('add_brother') }}</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"
                            id="brotherAddCloseButton">
                            <span aria-hidden="true close-btn">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('select_mother') }}</label> <span
                                class="text-red">* - {{ __('req') }} - *</span>
                            <select wire:model.defer="mother" class="form-control" required>
                                <option>{{ __('select_mother') }} .. </option>
                                @if ($user->fatherHasWives())
                                    @foreach ($user->father->wives as $fw)
                                        <option value="{{ $fw->id }}">{{ $fw->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                            @error('brother_mother')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlInput1">{{ __('brother_name') }}</label> <span
                                class="text-red">* - {{ __('req') }} - *</span>
                            <input type="text" wire:model.defer="name" class="form-control"
                                placeholder="{{ __('insert') . ' ' . __('brother_name') }}" required>
                            @error('name')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('living_country') }}</label> <span
                                class="text-red">* - {{ __('req') }} - *</span>
                            <select type="number" wire:model.defer="live" class="form-control">
                                <x-country_foreach :countries="$countries" />
                            </select>
                            @error('live')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">{{ __('email') }}</label> <span
                                class="text-green">* - {{ __('req2') }} - *</span>
                            <input type="text" wire:model.defer="email" class="form-control"
                                placeholder="Enter email">
                            @error('email')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('mobile_number') }}</label>
                            <input type="number" wire:model.defer="phone" class="form-control"
                                placeholder="{{ __('insert') . ' ' . __('mobile_number') }} (01 ..... )">
                            @error('phone')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('birthday') }}</label>
                            <input class="form-control" type="date" wire:model.defer="birth"
                                id="exampleFormControlInput5"
                                max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('leave_date') }}</label>
                            <input class="form-control" type="date" wire:model.defer="death"
                                id="exampleFormControlInput5"
                                max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close-btn"
                            data-bs-dismiss="modal">{{ __('cancel') }}</button>
                        <button type="submit" class="btn btn-primary close-modal">{{ __('save_changes') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--     brother Modal end -->

    <!--     sister Modal start -->
    <div wire:ignore.self class="modal fade" id="sisterAdd" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form wire:submit.prevent="addSibling('female')" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('add_sister') }}</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"
                            id="sisterAddCloseButton">
                            <span aria-hidden="true close-btn">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('select_mother') }}</label> <span
                                class="text-red">* - {{ __('req') }} - *</span>
                            <select wire:model.defer="mother" class="form-control" required>
                                <option>{{ __('select_mother') }} .. </option>
                                @if ($user->fatherHasWives())
                                    @foreach ($user->father->wives as $fw)
                                        <option value="{{ $fw->id }}">{{ $fw->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                            @error('mother')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlInput1">{{ __('sister_name') }}</label> <span
                                class="text-red">* - {{ __('req') }} - *</span>
                            <input type="text" wire:model.defer="name" class="form-control"
                                placeholder="{{ __('insert') . ' ' . __('sister_name') }}" required>
                            @error('name')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('living_country') }}</label> <span
                                class="text-red">* - {{ __('req') }} - *</span>
                            <select type="number" wire:model.defer="live" class="form-control">
                                <x-country_foreach :countries="$countries" />
                            </select>
                            @error('live')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">{{ __('email') }}</label> <span
                                class="text-green">* - {{ __('req2') }} - *</span>
                            <input type="text" wire:model.defer="email" class="form-control"
                                placeholder="Enter email">
                            @error('email')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('mobile_number') }}</label>
                            <input type="number" wire:model.defer="phone" class="form-control"
                                placeholder="{{ __('insert') . ' ' . __('mobile_number') }} (01 ..... )">
                            @error('brother_phone')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('birthday') }}</label>
                            <input class="form-control" type="date" wire:model.defer="birth"
                                id="exampleFormControlInput5"
                                max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('leave_date') }}</label>
                            <input class="form-control" type="date" wire:model.defer="death"
                                id="exampleFormControlInput5"
                                max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close-btn"
                            data-bs-dismiss="modal">{{ __('cancel') }}</button>
                        <button type="submit" class="btn btn-primary close-modal">{{ __('save_changes') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--     sister Modal end -->

    <!--     wife Modal start -->
    <div wire:ignore.self class="modal fade" id="WifeAdd" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form wire:submit.prevent="addWife" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('add_wife') }}</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"
                            id="WifeAddCloseButton">
                            <span aria-hidden="true close-btn">×</span>
                        </button>
                    </div>

                    <div class="modal-body">

                        <div class="form-group">
                            <label for="exampleFormControlInput1">{{ __('wife_name') }}</label> <span
                                class="text-red">* - {{ __('req') }} - *</span>
                            <input type="text" wire:model.defer="name" class="form-control"
                                placeholder="{{ __('insert') . ' ' . __('wife_name') }}" required>
                            @error('name')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('living_country') }}</label> <span
                                class="text-red">* - {{ __('req') }} - *</span>
                            <select type="number" wire:model.defer="live" class="form-control">
                                <x-country_foreach :countries="$countries" />
                            </select>
                            @error('live')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">{{ __('email') }}</label> <span
                                class="text-green">* - {{ __('req2') }} - *</span>
                            <input type="text" wire:model.defer="email" class="form-control"
                                placeholder="Enter email">
                            @error('email')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('mobile_number') }}</label>
                            <input type="number" wire:model.defer="phone" class="form-control"
                                placeholder="{{ __('insert') . ' ' . __('mobile_number') }} (01 ..... )">
                            @error('phone')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('marrige_time') }}</label>
                            <input type="date" wire:model.defer="marriage_start" class="form-control"
                                placeholder="pick a date" max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                            @error('marriage_start')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('marrige_time_end') }}</label>
                            <input type="date" wire:model.defer="marriage_end" class="form-control"
                                placeholder="pick a date" max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('birthday') }}</label>
                            <input class="form-control" type="date" wire:model.defer="birth"
                                id="exampleFormControlInput5"
                                max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('leave_date') }}</label>
                            <input class="form-control" type="date" wire:model.defer="death"
                                id="exampleFormControlInput5"
                                max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close-btn"
                            data-bs-dismiss="modal">{{ __('cancel') }}</button>
                        <button type="submit" class="btn btn-primary close-modal">{{ __('save_changes') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--     wife Modal end -->

    <!--     child Modal start -->
    <div wire:ignore.self class="modal fade" id="childAdd" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form wire:submit.prevent="addChild" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('add_child') }}</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"
                            id="childAddCloseButton">
                            <span aria-hidden="true close-btn">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleFormControlInput1">{{ __('child_name') }}</label>
                            <input type="text" wire:model.defer="name" class="form-control"
                                placeholder="{{ __('insert') . ' ' . __('child_name') }}" required>
                            @error('name')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('select_gender') }}</label> <span
                                class="text-red">* - {{ __('req') }} - *</span>
                            <select wire:model.defer="gender" class="form-control">
                                <option value="">{{ __('select_gender') }}</option>
                                <option value="male">{{ __('male') }}</option>
                                <option value="female">{{ __('female') }}</option>
                            </select>
                            @error('gender')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('living_country') }}</label> <span
                                class="text-red">* - {{ __('req') }} - *</span>
                            <select type="number" wire:model.defer="live" class="form-control">
                                <x-country_foreach :countries="$countries" />
                            </select>
                            @error('live')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">{{ __('email') }}</label> <span
                                class="text-green">* - {{ __('req2') }} - *</span>
                            <input type="text" wire:model.defer="email" class="form-control"
                                placeholder="Enter email">
                            @error('email')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('mobile_number') }}</label>
                            <input type="number" wire:model.defer="phone" class="form-control"
                                placeholder="{{ __('insert') . ' ' . __('mobile_number') }} (01 ..... )">
                            @error('phone')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('birthday') }}</label>
                            <input class="form-control" type="date" wire:model.defer="birth"
                                id="exampleFormControlInput5"
                                max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('leave_date') }}</label>
                            <input class="form-control" type="date" wire:model.defer="death"
                                id="exampleFormControlInput5"
                                max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close-btn"
                            data-bs-dismiss="modal">{{ __('cancel') }}</button>
                        <button type="submit"
                            class="btn btn-primary close-modal">{{ __('save_changes') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--     child Modal end -->

    <!--     husband Modal start -->
    <div wire:ignore.self class="modal fade" id="addHusband" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form wire:submit.prevent="addHusband" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('husband_name') }}</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"
                            id="husbandAddCloseButton">
                            <span aria-hidden="true close-btn">×</span>
                        </button>
                    </div>

                    <div class="modal-body">

                        <div class="form-group">
                            <label for="exampleFormControlInput1">{{ __('husband_name') }}</label> <span
                                class="text-red">* - {{ __('req') }} - *</span>
                            <input type="text" wire:model.defer="name" class="form-control"
                                placeholder="{{ __('insert') . ' ' . __('husband_name') }}" required>
                            @error('name')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('living_country') }}</label> <span
                                class="text-red">* - {{ __('req') }} - *</span>
                            <select type="number" wire:model.defer="live" class="form-control">
                                <x-country_foreach :countries="$countries" />
                            </select>
                            @error('live')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">{{ __('email') }}</label> <span
                                class="text-green">* - {{ __('req2') }} - *</span>
                            <input type="text" wire:model.defer="email" class="form-control"
                                placeholder="Enter email">
                            @error('email')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('mobile_number') }}</label>
                            <input type="number" wire:model.defer="phone" class="form-control"
                                placeholder="{{ __('insert') . ' ' . __('mobile_number') }} (01 ..... )">
                            @error('phone')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('marrige_time') }}</label>
                            <input type="date" wire:model.defer="marriage_start" class="form-control"
                                placeholder="pick a date" max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('marrige_end') }}</label>
                            <input type="date" wire:model.defer="marriage_end" class="form-control"
                                placeholder="photok a date"
                                max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('birthday') }}</label>
                            <input class="form-control" type="date" wire:model.defer="birth"
                                id="exampleFormControlInput5"
                                max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('leave_date') }}</label>
                            <input class="form-control" type="date" wire:model.defer="death"
                                id="exampleFormControlInput5"
                                max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close-btn"
                            data-bs-dismiss="modal">{{ __('cancel') }}</button>
                        <button type="submit"
                            class="btn btn-primary close-modal">{{ __('save_changes') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--     husband Modal end -->

    <!--     user edit Modal start -->
    <div wire:ignore.self class="modal fade" id="userEdit" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form wire:submit.prevent="userEdit" method="post">
                <div class="modal-content" style="background-color:#FEF6F3">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('edit') }} {{ $editName ?? '' }}</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"
                            id="userEditCloseButton">
                            <span aria-hidden="true close-btn">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleFormControlInput1">{{ __('name') }}</label>
                            <input type="text" wire:model.defer="editName" class="form-control"
                                placeholder="{{ __('insert') . ' ' . __('name') }}" required>
                            @error('editName')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">{{ __('email') }}</label>
                            <input type="text" wire:model.defer="editEmail" class="form-control"
                                placeholder="Enter email">
                            @error('email')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('select_gender') }}</label>
                            <select wire:model.defer="editGender" class="form-control" required>
                                <option value="">{{ __('select_gender') }}</option>
                                <option value="male">{{ __('male') }}</option>
                                <option value="female">{{ __('female') }}</option>
                            </select>
                            @error('editGender')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('living_country') }}</label>
                            <select wire:model.defer="editLive" class="form-control">
                                <option value="">{{ __('select_living_country') }} .. </option>
                                @foreach ($countries as $country)
                                    <option value="{{ $country->id }}"
                                        @if ($editLive == $country->id) selected="selected" @endif>
                                        {{ $country->nicename }}</option>
                                @endforeach
                            </select>
                            @error('editlive')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('mobile_number') }}</label>
                            <input type="number" wire:model.defer="editPhone" class="form-control"
                                placeholder="{{ __('insert') . ' ' . __('mobile_number') }} (01 ..... )">
                            @error('editPhone')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('birthday') }}</label>
                            <input class="form-control" type="date" wire:model.defer="editBirth"
                                id="exampleFormControlInput5"
                                max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('leave_date') }}</label>
                            <input class="form-control" type="date" wire:model.defer="editDeath"
                                id="exampleFormControlInput5"
                                max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close-btn"
                            data-bs-dismiss="modal">{{ __('cancel') }}</button>
                        <button type="submit"
                            class="btn btn-primary close-modal">{{ __('save_changes') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--     user edit Modal end -->

    <!--     user edit Modal start -->
    <div wire:ignore.self class="modal fade" id="marEdit" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form wire:submit.prevent="marEdit" method="post">
                <div class="modal-content" style="background-color:#FFFED9;">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('mar_user') }} {{ $editName ?? '' }}</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"
                            id="marEditCloseButton">
                            <span aria-hidden="true close-btn">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('marrige_time') }}</label>
                            <input type="date" wire:model.defer="marriage_start_edit" class="form-control"
                                placeholder="pick a date" max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlInput2">{{ __('marrige_end') }}</label>
                            <input type="date" wire:model.defer="marriage_end_edit" class="form-control"
                                placeholder="photok a date"
                                max="{{ \Carbon\Carbon::yesterday()->toDateString() }}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close-btn"
                            data-bs-dismiss="modal">{{ __('cancel') }}</button>
                        <button type="submit"
                            class="btn btn-primary close-modal">{{ __('save_changes') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--     user edit Modal end -->

    <!--     user edit Modal start -->
    <div wire:ignore.self class="modal fade" id="userPic" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form wire:submit.prevent="editPic" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('change') }} {{ $editUser->name ?? '' }}
                            {{ __('pic') }}</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"
                            id="userPicCloseButton">
                            <span aria-hidden="true close-btn">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            @if ($editPic)
                                {{ __('photo_preview') }}:
                                <img src="{{ $editPic->temporaryUrl() }}">
                            @endif
                            <label for="exampleFormControlInput1">{{ __('name') }}</label>
                            <input type="file" wire:model="editPic" required="">
                            @error('editPic')
                                <span class="text-danger error">{{ $message }}</span>
                            @enderror
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close-btn"
                            data-bs-dismiss="modal">{{ __('cancel') }}</button>
                        <button type="submit"
                            class="btn btn-primary close-modal">{{ __('save_changes') }}</button>
                    </div>

                </div>
            </form>
        </div>
    </div>

    <!--     user edit Modal end -->

    <script type="text/javascript">
        document.addEventListener('livewire:load', function() {
            @this.on('Father_added', () => {
                $("#fatherAddCloseButton").click();
            });
            @this.on('Mother_added', () => {
                $("#motherAddCloseButton").click();
            });
            @this.on('Sibling_added', () => {
                $("#brotherAddCloseButton").click();
                $("#sisterAddCloseButton").click();
            });
            @this.on('wife_added', () => {
                $("#WifeAddCloseButton").click();
            });
            @this.on('child_added', () => {
                $("#childAddCloseButton").click();
            });
            @this.on('husband_added', () => {
                $("#husbandAddCloseButton").click();
            });
            @this.on('User_modified', () => {
                $("#userEditCloseButton").click();
            });
            @this.on('marr_modified', () => {
                $("#marEditCloseButton").click();
            });
            @this.on('image_uploaded', () => {
                $("#userPicCloseButton").click();
            });
        });
    </script>

    @if (!isset($needHelp))
        @push('modals')
            <!-- Faqs Popup -->

            <div class="modal fade" id="faqs-popup" tabindex="-1" role="dialog" aria-labelledby="faqs-popup"
                aria-hidden="true">
                <div class="modal-dialog window-popup faqs-popup" role="document">
                    <div class="modal-content">
                        <a href="#" class="close icon-close" data-bs-dismiss="modal" aria-label="Close">
                            <svg class="olymp-close-icon">
                                <use xlink:href="#olymp-close-icon"></use>
                            </svg>
                        </a>

                        <div class="modal-header">
                            <h4 class="title" id="faqs-title">{{ __('welcome_in') . __('tree_builder') }}</h4>
                        </div>

                        <div class="modal-body">

                            <div class="accordion" id="accordionExample">

                                @foreach ($FaqsHelp as $faq)
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="heading{{ $faq->id }}">
                                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                data-bs-target="#collapse{{ $faq->id }}" aria-expanded="true"
                                                aria-controls="collapse{{ $faq->id }}">
                                                <span class="c-green">- </span> {{ $faq->title }}
                                            </button>
                                        </h2>
                                        <div id="collapse{{ $faq->id }}"
                                            class="accordion-collapse collapse @if ($faq->id == $FaqsHelp[0]->id) show @endif"
                                            aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                            <div class="accordion-body">
                                                {{ $faq->desc }}
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                            <hr>
                            <a data-bs-dismiss="modal" class="close btn btn-blue btn-lg">{{ __('dismiss') }}</a>
                            <a data-bs-dismiss="modal" class="close btn btn-lg btn-primary"
                                onclick="removeHelp({{ $faq->topic_id }})">{{ __('dont_show') }}</a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- ... end Faqs Popup -->
        @endpush

        @push('scripts')
            <script type="text/javascript">
                $(document).ready(function() {

                    $('#faqs-popup').modal('show');

                });

                function removeHelp(argument) {

                    var topic = argument;

                    $.ajax({
                        url: "{{ route('remove_help', app()->getLocale()) }}",
                        type: "POST",
                        data: {
                            Topic: topic,
                            _token: $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(response) {
                            console.log(response);
                            if (response) {
                                $('.success').text(response.success);
                                $("#helpCloseButton").click();
                            }
                        },
                    });
                }
            </script>
        @endpush
    @endif

</div>
