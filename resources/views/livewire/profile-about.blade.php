<div>

    <!-- Top Header-Profile -->

    <div class="container">
        <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="ui-block">
                    <div class="top-header">
                        <div class="top-header-thumb">

                        </div>
                        <div class="profile-section">

                            <x-profile_links :act="3" />

                            <div class="control-block-button">
                                <a href="#" class="btn btn-control bg-blue">
                                    <svg class="olymp-happy-face-icon">
                                        <use xlink:href="#olymp-happy-face-icon"></use>
                                    </svg>
                                </a>

                                <a href="#" class="btn btn-control bg-purple">
                                    <svg class="olymp-chat---messages-icon">
                                        <use xlink:href="#olymp-chat---messages-icon"></use>
                                    </svg>
                                </a>

                                <div class="btn btn-control bg-primary more">
                                    <svg class="olymp-settings-icon">
                                        <use xlink:href="#olymp-settings-icon"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>

                        <x-top_header_profile />

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ... end Top Header-Profile -->


    <div class="container">
        <div class="row">
            <div class="col col-xl-8 order-xl-2 col-lg-8 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">
                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">{{ __('hobbies_and_interests') }}</h6>
                        <a href="#" class="more"><svg class="olymp-three-dots-icon">
                                <use xlink:href="#olymp-three-dots-icon"></use>
                            </svg></a>
                    </div>
                    <form wire:submit.prevent="storeMyInfo" method="post" class="comment-form">
                        <div class="ui-block-content">
                            <div class="row">
                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12 mb-3 mb-md-0">


                                    <!-- W-Personal-Info -->

                                    <ul class="widget w-personal-info item-block">
                                        <li>
                                            <span class="title">{{ __('hobbies') }}:</span>
                                            <textarea class="form-control" wire:model.defer="hobby" placeholder="{{ __('unknown') }}"></textarea>
                                        </li>
                                        <li>
                                            <span class="title">{{ __('favourite_tv_shows') }}:</span>
                                            <textarea class="form-control" wire:model.defer="tv" placeholder="{{ __('unknown') }}"></textarea>
                                        </li>
                                        <li>
                                            <span class="title">{{ __('favourite_movies') }}:</span>
                                            <textarea class="form-control" wire:model.defer="movies" placeholder="{{ __('unknown') }}"></textarea>
                                        </li>
                                        <li>
                                            <span class="title">{{ __('favourite_games') }}:</span>
                                            <textarea class="form-control" wire:model.defer="games" placeholder="{{ __('unknown') }}"></textarea>
                                        </li>
                                    </ul>

                                    <!-- ... end W-Personal-Info -->
                                </div>
                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">


                                    <!-- W-Personal-Info -->

                                    <ul class="widget w-personal-info item-block">
                                        <li>
                                            <span class="title">{{ __('favourite_music_bands') }}:</span>
                                            <textarea class="form-control" wire:model.defer="music" placeholder="{{ __('unknown') }}"></textarea>
                                        </li>
                                        <li>
                                            <span class="title">{{ __('favourite_books') }}:</span>
                                            <textarea class="form-control" wire:model.defer="books" placeholder="{{ __('unknown') }}"></textarea>
                                        </li>
                                        <li>
                                            <span class="title">{{ __('favourite_writers') }}:</span>
                                            <textarea class="form-control" wire:model.defer="writer" placeholder="{{ __('unknown') }}"></textarea>
                                        </li>
                                        <li>
                                            <span class="title">{{ __('other_interests') }}:</span>
                                            <textarea class="form-control" wire:model.defer="other" placeholder="{{ __('unknown') }}"></textarea>
                                        </li>
                                    </ul>

                                    <!-- ... end W-Personal-Info -->
                                </div>
                                <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <button type="submit" class="btn btn-blue btn-lg full-width">Publish</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="ui-block">

                                <div class="ui-block-title">
                                    <h6 class="title">{{ __('education_and_traning') }}</h6>
                                    <a href="#" class="more"><svg class="olymp-three-dots-icon">
                                            <use xlink:href="#olymp-three-dots-icon"></use>
                                        </svg></a>
                                </div>
                                <div class="ui-block-content">
                                    <div class="row">
                                        <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
                                            <!-- W-Personal-Info -->
                                            <ul class="widget w-personal-info item-block">
                                                @if (count($UserEdu) != 0)
                                                    @foreach ($UserEdu as $user)
                                                        @if ($user->is_job == 0)
                                                            <li>
                                                                <span class="title">{{ $user->place }}</span>
                                                                <span class="date">{{ $user->start }} -
                                                                    {{ $user->end }}</span>
                                                                <span class="text">{{ $user->specialty }}</span>
                                                            </li>
                                                            <li>
                                                                <hr>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <li>
                                                        <span class="title">{{ __('unknown_education_state') }}</span>
                                                    </li>

                                                @endif
                                                <form wire:submit.prevent="storeMyStudy(0)" method="post"
                                                    class="comment-form">
                                                    <li>
                                                        <label>study place</label>
                                                        <input class="form-control" type="text"
                                                            wire:model.defer="Eplace"
                                                            placeholder="study at place for example ( univirsty . college .... ) "
                                                            required="" />
                                                        <label>study subjects</label>
                                                        <input class="form-control" type="text"
                                                            wire:model.defer="Estudy"
                                                            placeholder="all subject you study their ...  "
                                                            required="" />
                                                        <label>start from year</label>
                                                        <input class="form-control col-lg-4" type="text"
                                                            wire:model.defer="Estart"
                                                            placeholder="start study from ... for example (2014)"
                                                            required="" />
                                                        <label>finish year</label>
                                                        <input class="form-control " type="text"
                                                            wire:model.defer="Eend"
                                                            placeholder="finish study at ... for example (2015)"
                                                            required="" />
                                                        <button type="submit"
                                                            class="btn btn-blue btn-lg full-width">Publish</button>
                                                    </li>
                                                </form>
                                            </ul>
                                            <!-- ... end W-Personal-Info -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="ui-block">

                                <div class="ui-block-title">
                                    <h6 class="title">{{ __('employement_and_job') }}</h6>
                                    <a href="#" class="more"><svg class="olymp-three-dots-icon">
                                            <use xlink:href="#olymp-three-dots-icon"></use>
                                        </svg></a>
                                </div>
                                <div class="ui-block-content">
                                    <div class="row">
                                        <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
                                            <!-- W-Personal-Info -->
                                            <ul class="widget w-personal-info item-block">
                                                @if (count($UserEdu) != 0)
                                                    @foreach ($UserEdu as $user)
                                                        @if ($user->is_job == 1)
                                                            <li>
                                                                <span class="title">{{ $user->place }}</span>
                                                                <span class="date">{{ $user->start }} -
                                                                    {{ $user->end }}</span>
                                                                <span class="text">{{ $user->specialty }}</span>
                                                            </li>
                                                            <li>
                                                                <hr>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <li>
                                                        <span
                                                            class="title">{{ __('unknown_employement_state') }}</span>
                                                    </li>
                                                @endif
                                                <form wire:submit.prevent="storeMyStudy(1)" method="post"
                                                    class="comment-form">
                                                    <li>
                                                        <label>work place</label>
                                                        <input class="form-control" type="text"
                                                            wire:model.defer="Eplace"
                                                            placeholder="work at place for example ( company . college .... ) "
                                                            required="" />
                                                        <label>work specialty</label>
                                                        <input class="form-control" type="text"
                                                            wire:model.defer="Estudy"
                                                            placeholder="all subject you work their ...  "
                                                            required="" />
                                                        <label>join year</label>
                                                        <input class="form-control col-lg-4" type="text"
                                                            wire:model.defer="Estart"
                                                            placeholder="start job from ... for example (2014)"
                                                            required="" />
                                                        <label>leave year</label>
                                                        <input class="form-control " type="text"
                                                            wire:model.defer="Eend"
                                                            placeholder="leave job at ... for example (2015)"
                                                            required="" />
                                                        <button type="submit"
                                                            class="btn btn-blue btn-lg full-width">Publish</button>
                                                    </li>
                                                </form>
                                            </ul>
                                            <!-- ... end W-Personal-Info -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>


            </div>


            <div class="col col-xl-4 order-xl-1 col-lg-4 order-lg-1 col-md-12 order-md-2 col-sm-12 col-12">
                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">{{ __('personal_info') }}</h6>
                        <a href="#" class="more"><svg class="olymp-three-dots-icon">
                                <use xlink:href="#olymp-three-dots-icon"></use>
                            </svg></a>
                    </div>
                    <form wire:submit.prevent="storeMyInfo" method="post" class="comment-form">
                        <div class="ui-block-content">


                            <!-- W-Personal-Info -->

                            <ul class="widget w-personal-info">
                                <li>
                                    <span class="title">{{ __('about_me2') }}:</span>
                                    <textarea class="form-control" wire:model.defer="bio" placeholder="{{ __('unknown') }}"></textarea>
                                </li>
                                <li>
                                    <span class="title">{{ __('birthday') }}:</span>
                                    <span class="text">
                                        @if (!empty(auth()->user()->birth_date))
                                            {{ auth()->user()->birth_date->format('l d, M Y') }}
                                        @else
                                            __('unknown')
                                        @endif
                                    </span>
                                </li>
                                <li>
                                    <span class="title">{{ __('birth_place') }}:</span>
                                    <textarea class="form-control" wire:model.defer="birth_place" placeholder="{{ __('unknown') }}"></textarea>
                                </li>
                                <li>
                                    <span class="title">{{ __('lives_in') }}:</span>
                                    <img src="{{ asset('img/flag/' . strtolower(auth()->user()->country->iso . '.svg')) }}"
                                        width="16px" /> {{ auth()->user()->country->nicename }}
                                </li>
                                <li>
                                    <span class="title">{{ __('occupation') }}:</span>
                                    <select wire:model.defer="profession" class="form-control col-lg-4">
                                        <option value="">select profession ..</option>
                                        <option value="Agriculture">Agriculture, Food, and Natural Resources</option>
                                        <option value="Architecture">Architecture and Construction</option>
                                        <option value="Arts">Arts, Audio/Video Technology, and Communication</option>
                                        <option value="Business">Business and Finance</option>
                                        <option value="Education">Education and Training</option>
                                        <option value="Government">Government and Public Administration</option>
                                        <option value="Health">Health Science</option>
                                        <option value="Information">Information Technology</option>
                                        <option value="Law">Law, Public Safety, Corrections, and Security</option>
                                        <option value="Marketing">Marketing directions</option>
                                        <option value="Science">Science, Technology, Engineering, and Math</option>
                                        <option value="without">without profession .. </option>
                                    </select>
                                </li>
                                <li>
                                    <span class="title">{{ __('joined') }}:</span>
                                    <span class="text">{{ auth()->user()->created_at->format('l d, M Y') }}</span>
                                </li>
                                <li>
                                    <span class="title">{{ __('gender') }}:</span>
                                    <span class="text">{{ auth()->user()->gender }}</span>
                                </li>
                                <li>
                                    <span class="title">{{ __('status') }}:</span>
                                    <span class="text"> </span>
                                </li>
                                <li>
                                    <span class="title">{{ __('email') }}:</span>
                                    <a href="#" class="text">{{ auth()->user()->email }}</a>
                                </li>
                                <li>
                                    <span class="title">{{ __('website') }}:</span>
                                    <textarea class="form-control" wire:model.defer="website" placeholder="{{ __('website') . ' ' . __('unknown') }}"></textarea>
                                </li>
                                <li>
                                    <span class="title">{{ __('phone_number') }}:</span>
                                    <span class="text"><img
                                            src="{{ asset('img/flag/' . strtolower(auth()->user()->country->iso . '.svg')) }}"
                                            width="16px" />
                                        {{ '00' . auth()->user()->country->phonecode . substr(auth()->user()->phone_number, 1) }}</span>
                                </li>
                                <li>
                                    <span class="title">{{ __('religious_belifs') }}:</span>
                                    <select wire:model.defer="religion" class="form-control col-lg-4">
                                        <option value="">{{ __('select_religion') }} ..</option>
                                        <option value="islam">{{ __('islam') }}</option>
                                        <option value="christian">{{ __('christian') }}</option>
                                        <option value="jewish">{{ __('jewish') }}</option>
                                        <option value="othr">{{ __('othr') }}</option>
                                    </select>
                                </li>
                                <li>
                                    <span class="title">{{ __('political_incline') }}:</span>
                                    <select wire:model.defer="politic" class="form-control col-lg-4">
                                        <option value="">{{ __('select_plotical') }} ..</option>
                                        <option value="non">{{ __('non') }}</option>
                                        <option value="unspecifid">{{ __('unspecifid') }}</option>
                                        <option value="moderate">{{ __('moderate') }}</option>
                                        <option value="hardened">{{ __('hardened') }}</option>
                                    </select>
                                </li>
                            </ul>

                            <!-- ... end W-Personal-Info -->
                            <!-- W-Socials -->

                            <div class="widget w-socials">
                                <h6 class="title">{{ __('other_social_networks') }}:</h6>
                                <svg width="16" height="16">
                                    <use xlink:href="#olymp-facebook-icon"></use>
                                </svg>
                                {{ __('facebook') }}
                                <textarea class="form-control" wire:model.defer="facebook" placeholder="https://www.facebook.com/ ..... "></textarea>

                                <svg width="16" height="16">
                                    <use xlink:href="#olymp-youtube-icon"></use>
                                </svg>
                                {{ __('youtube') }}
                                <textarea class="form-control" wire:model.defer="youtube" placeholder="https://youtube.com/user/ ..... "></textarea>

                                <svg width="16" height="16">
                                    <use xlink:href="#olymp-twitter-icon"></use>
                                </svg>
                                {{ __('twitter') }}
                                <textarea class="form-control" wire:model.defer="twitter" placeholder="https://twitter.com/@ ..... "></textarea>

                                <svg width="16" height="16">
                                    <use xlink:href="#olymp-instagram-icon"></use>
                                </svg>
                                {{ __('instgram') }}
                                <textarea class="form-control" wire:model.defer="instagram" placeholder="https://www.instagram.com/ ....."></textarea>

                                <svg width="16" height="16">
                                    <use xlink:href="#olymp-happy-sticker-icon"></use>
                                </svg>
                                {{ __('ticktock') }}
                                <textarea class="form-control" wire:model.defer="ticktok" placeholder="https://www.tiktok.com/@ ..... "></textarea>


                            </div>

                            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <button type="submit" class="btn btn-blue btn-lg full-width">Publish</button>
                            </div>
                            <!-- ... end W-Socials -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
