<div>
     <!-- Top Header-Profile -->

<div class="container">
    <div class="row">
        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="ui-block">
                <div class="top-header">
                    <div class="top-header-thumb">

                    </div>
                    <div class="profile-section">
                        
                        <x-profile_links  :act="1"/>

                        <div class="control-block-button">
                            <a href="#" class="btn btn-control bg-blue">
                                <svg class="olymp-happy-face-icon"><use xlink:href="#olymp-happy-face-icon"></use></svg>
                            </a>

                            <a href="#" class="btn btn-control bg-purple">
                                <svg class="olymp-chat---messages-icon"><use xlink:href="#olymp-chat---messages-icon"></use></svg>
                            </a>

                            <div class="btn btn-control bg-primary more">
                                <svg class="olymp-settings-icon"><use xlink:href="#olymp-settings-icon"></use></svg>

                                <ul class="more-dropdown more-with-triangle triangle-bottom-right">
                                    <li>
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#update-header-photo">Update Profile Photo</a>
                                    </li>
                                    <li>
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#update-header-photo">Update Header Photo</a>
                                    </li>
                                    <li>
                                        <a href="#">Account Settings</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <x-top_header_profile />

                </div>
            </div>
        </div>
    </div>
</div>

<!-- ... end Top Header-Profile -->

    <div class="container">
            <div class="row">
                <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="ui-block">


                        <!-- Today Events -->

                        <div class="today-events calendar">
                            <div class="today-events-thumb">
                                <div class="date">
                                    <div class="day-number">{{ date('d') }}</div>
                                    <div class="day-week">{{ date('l') }}</div>
                                    <div class="month-year">{{ date('M , Y') }}</div>
                                </div>
                            </div>

                            <div class="list">
                                <div class="accordion day-event" id="accordionExample" data-month="12" data-day="2">

                                   @foreach($actLogs as $actLog)
                                   @if($actLog->created_at->isToday())
                                    <div class="accordion-item">
                                        <div class="accordion-header" id="headingTwo{{ $actLog->id }}">
                                            <div class="event-time">
                                                <time datetime="2004-07-24T18:18">{{ $actLog->created_at->format('h:i A') }}</time>
                                            </div>
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo{{ $actLog->id }}" aria-expanded="false" aria-controls="collapseTwo{{ $actLog->id }}">
                                                {{ $actLog->log_name }}
                                                <svg width="8" height="8">
                                                    <use xlink:href="#olymp-dropdown-arrow-icon"></use>
                                                </svg>
                                                <span class="event-status-icon completed" data-toggle="tooltip" data-placement="top" data-original-title="COMPLETED">
                                                                                <svg class="olymp-checked-calendar-icon"><use xlink:href="#olymp-checked-calendar-icon"></use></svg>
                                                                            </span>
                                            </button>
                                        </div>
                                        <div id="collapseTwo{{ $actLog->id }}" class="accordion-collapse collapse" aria-labelledby="headingTwo{{ $actLog->id }}" data-bs-parent="#accordionExample">
                                            <div class="accordion-body">
                                                {{ $actLog->description }}
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    @endif
                                    @endforeach

                                </div>

                            </div>
                        </div>

                        <!-- ... end Today Events -->
                    </div>
                </div>
                <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="ui-block">


                       

                        <div class="ui-block-title ui-block-title-small">
                            <h6 class="title">all lifetime events for you </h6>
                        </div>

                        <div class="today-events calendar">
                            <div class="list">
                                <div class="accordion day-event" id="accordionExample2">

                                @foreach($actLogs as $actLog)
                                
                                    <div class="accordion-item">
                                        <div class="accordion-header" id="headingOne{{ $actLog->id }}">
                                            <div class="event-time">
                                                <time class="published" datetime="{{ $actLog->created_at }}">{{ $actLog->created_at }}</time>
                                            </div>
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne{{ $actLog->id }}" aria-expanded="false" aria-controls="collapseOne{{ $actLog->id }}">
                                                {{ $actLog->log_name }}
                                                <svg width="8" height="8">
                                                    <use xlink:href="#olymp-dropdown-arrow-icon"></use>
                                                </svg>
                                                <span class="event-status-icon" data-bs-toggle="modal" data-bs-target="#public-event">
                                                    <svg class="olymp-calendar-icon" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="UNCOMPLETED"><use xlink:href="#olymp-calendar-icon"></use></svg>
                                                </span>
                                            </button>
                                        </div>
                                        <div id="collapseOne{{ $actLog->id }}" class="accordion-collapse collapse" aria-labelledby="headingOne{{ $actLog->id }}" data-bs-parent="#accordionExample{{ $actLog->id }}">
                                            <div class="accordion-body">
                                                {{ $actLog->description }}
                                                <div class="place inline-items">
                                                    <svg class="olymp-add-a-place-icon">
                                                        <use xlink:href="#olymp-add-a-place-icon"></use>
                                                    </svg>
                                                    <span>Daydreamz Agency</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   
                               <hr>
                                @endforeach
  </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
</div>
