<div style="background-image: url({{ asset('img/pattern/p1.png')}})">
@push('styles')
    <link rel="stylesheet" href="https://cdn.plyr.io/3.7.8/plyr.css" />
<style>
    #player {
        width: 100% !important;
        height: auto !important;
        max-width: 100%;
        max-height: 100%;
        object-fit: cover;
    }
    .modal-backdrop {
        /* z-index: 1999; */
    }
    .fancybox-slide>* {
        max-height: 100vh !important;
    }

</style>
@endpush


 @if (session()->has('message'))
    <div class="alert alert-success text-center">
        {{ session('message') }}
    </div>
@endif

 <!-- Main Header Groups -->

<div class="main-header">
    <div class="content-bg-wrap bg-group" style="background-image: asset('img/bg-group.png');"></div>
    <div class="container">
        <div class="row">
            <div class="col col-lg-8 m-auto col-md-8 col-sm-12 col-12">
                <div class="main-header-content">
                    <h1>{{ __('family_posts') }}</h1>
                    <p> 
                    </p>
                </div>
            </div>
        </div>
    </div>

    <img loading="lazy" class="img-bottom" src="{{ asset('img/group-bottom.webp') }}" alt="friends" width="1087" height="148">
</div>

<!-- ... end Main Header Groups -->

<div class="container">
        <div class="row">
            
        <div class="col col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">

            <div class="ui-block">
                <div class="ui-block-title bg-blue">
                    <h6 class="title c-white">{{ __('create_new_post') }}</h6>
                </div>
                <div class="ui-block-content">

                    <form wire:submit.prevent="addPost" method="post" class="comment-form">

                        <div class="row">
                            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

                                @if($postPic != 'null')
                                <div class="post-thumb">
                                    <img loading="lazy" src="{{asset($postPic)}}" alt="author" width="768" height="441">
                                </div>
                                @endif

                                @if($youtubeLink != '')
                                <img loading="lazy" src="https://i.ytimg.com/vi/{{$youtubeLink}}/hqdefault.jpg" alt="author" width="768" height="441">
                                @endif

                                <div class="form-group label-floating">
                                    <label class="control-label">{{ __('what_r_u_thinking') }}</label>
                                    <input class="form-control" type="text" wire:model.defer="postBody" placeholder="" />
                                    <div class="add-options-message">
                                        <a href="#" class="options-message" data-bs-toggle="modal" data-bs-target="#update-header-photo">
                                            <svg class="olymp-camera-icon">
                                                <use xlink:href="#olymp-camera-icon"></use>
                                            </svg>
                                        </a>
                                         <a href="#" class="options-message" data-bs-toggle="modal" data-bs-target="#addYoutube">
                                            <svg class="olymp-music-play-icon-big">
                                                <use xlink:href="#olymp-music-play-icon-big"></use>
                                            </svg>
                                        </a>
                                    </div>
                                </div>

                            </div>

                            <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <div class="form-group label-floating is-select">
                                    <label class="control-label">{{ __('post_privacy') }}</label>
                                    <select wire:model.defer="postType" class="form-select">
                                        <option value="1">{{ __('post_public') }}</option>
                                        <option value="2">{{ __('post_private') }}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <button type="submit" class="btn btn-blue btn-lg full-width">{{ __('publish') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>

        <div class="col col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
            <div class="ui-block">
                
                <!-- W-Birthsday-Alert -->
                
                <div class="widget w-birthday-alert" style="border-radius: 5px 5px 0 0;">
                
                    <div class="content">
                        <div class="author-thumb">
                            <i class="ai aries"></i>
                        </div>
                        <span>horoScope date </span>
                        <a href="#" class="h4 title">horoScope sunsign Today</a>
                        
                        <p>horoscope text</p>


                    </div>
                </div>
                
                <!-- ... end W-Birthsday-Alert -->          
            </div>
        </div>
    </div>
        <!-- Main Content LEFT Post Versions -->
        <!-- top 1 shoe trophy icon like 1 show heart icon -->
    <div class="row">
       @foreach($posts as $post)
                <div class="col col-12">
                    <div class="ui-block">
                        <x-one_post :post="$post" :user="$user" :top="0" :like="0"></x-one_post>
                    </div>
                </div>
        @endforeach
        <!-- ... end Main Content Post Versions -->
    </div>
</div>


<!-- Window-popup Update Header Photo -->

<div wire:ignore.self class="modal fade" id="update-header-photo" tabindex="-1" role="dialog" aria-labelledby="update-header-photo" aria-hidden="true">
    <div class="modal-dialog window-popup update-header-photo" role="document">
        <div class="modal-content">
            <a href="#" class="close icon-close" data-bs-dismiss="modal" aria-label="Close">
                <svg class="olymp-close-icon"><use xlink:href="#olymp-close-icon"></use></svg>
            </a>

            <div class="modal-header">
                <h6 class="title">{{ __('add_post_pic') }}</h6>
            </div>

            <div class="modal-body">

                <a href="#" class="upload-photo-item" data-bs-toggle="modal" data-bs-target="#addPic">
                <svg class="olymp-computer-icon"><use xlink:href="#olymp-computer-icon"></use></svg>

                <h6>{{ __('upload_pic') }}</h6>
                <span>{{ __('browse_your_computer') }}.</span>


            </a>

                <a href="#" class="upload-photo-item" data-bs-toggle="modal" data-bs-target="#choose-from-my-photo">

            <svg class="olymp-photos-icon"><use xlink:href="#olymp-photos-icon"></use></svg>

            <h6>{{ __('choose_from_my_photos') }}</h6>
            <span>{{ __('chosse_from_app_gal') }}</span>
        </a>
            </div>
        </div>
    </div>
</div>


<!-- ... end Window-popup Update Header Photo -->

<!-- Window-popup Choose from my Photo -->

<div wire:ignore.self class="modal fade" id="choose-from-my-photo" tabindex="-1" role="dialog" aria-labelledby="choose-from-my-photo" aria-hidden="true">
    <div class="modal-dialog window-popup choose-from-my-photo" role="document">

        <div class="modal-content">
            <a href="#" class="close icon-close" data-bs-dismiss="modal" aria-label="Close">
                <svg class="olymp-close-icon"><use xlink:href="#olymp-close-icon"></use></svg>
            </a>
            <div class="modal-header">
                <h6 class="title">{{ __('chosse_from_app_gal') }}</h6>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-bs-toggle="tab" href="#home" role="tab" aria-expanded="true">
                            <svg class="olymp-photos-icon"><use xlink:href="#olymp-photos-icon"></use></svg>
                        </a>
                    </li>

                </ul>
            </div>

            <div class="modal-body">
                <!-- Tab panes -->
                <div class="tab-content">
                    <form wire:submit.prevent="addPostPic" method="post">
                        <div class="tab-pane fade active show" id="home" role="tabpanel" aria-expanded="true">
                            @for($i=0;$i<15;$i++)
                            <div class="choose-photo-item">
                                <div class="radio">
                                    <label class="custom-radio">
                                        <img loading="lazy" src="{{ url('img/default/event'.$i.'.png')}}" alt="photo" width="247" height="166">
                                        <input type="radio" name="picss" wire:model.defer="postPic" value="{{ url('img/default/event'.$i.'.png')}}">
                                    </label>
                                </div>
                            </div>
                            @endfor

                            <button type="button" class="btn btn-secondary btn-lg btn--half-width" data-bs-dismiss="modal">{{ __('cancel') }}</button>
                            <button type="submit" id="selectPhoto" class="btn btn-primary btn-lg btn--half-width">{{ __('select_photo') }}</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    </div>
</div>

<!-- ... end Window-popup Choose from my Photo -->


<!--     pic upload Modal start -->
    <div wire:ignore.self class="modal fade" id="addPic" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form wire:submit.prevent="imageUploadPost">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('upload_pic') }}</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true close-btn">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input wire:model="postPic" type="file">
                            @error('postPic') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close-btn" data-bs-dismiss="modal">{{ __('cancel') }}</button>
                        <button type="submit" class="btn btn-primary close-modal">{{ __('save_changes') }}</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
    <!--     pic upload Modal end -->

    <!--     youtube Modal start -->
    <div wire:ignore.self class="modal fade" id="addYoutube" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form wire:submit.prevent="youtubePost">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('add_youtube_link') }}</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true close-btn">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group label-floating">
                                    <label class="control-label">{{ __('add_youtube_link') }}</label>
                            <input class="form-control" wire:model="youtubeLink" type="test">
                            @error('youtubeLink') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close-btn" data-bs-dismiss="modal">{{ __('cancel') }}</button>
                        <button type="submit" class="btn btn-primary close-modal">{{ __('save_changes') }}</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
    <!--     youtube Modal end -->
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-4 text-center">
    @if ($posts->hasPages())
        <nav role="navigation" aria-label="Pagination Navigation" class="flex justify-between">
            <span>
                {{-- Previous Page Link --}}
                @if ($posts->onFirstPage())
                    <a class="relative inline-flex items-center px-4 py-2 text-sm font-medium text-white bg-primary border border-gray-300 leading-5 rounded-md hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150" href="{{$posts->links()->elements[0][1]}}">
                        {!! __('pagination.first') !!}
                    </a>
                @else
                    <a class="relative inline-flex items-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 leading-5 rounded-md hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150" href="{{$posts->links()->elements[0][1]}}">
                        {!! __('pagination.previous') !!}
                    </a>
                @endif
            </span>
            <?php $a=0;?>
            @foreach($posts->links()->elements[0] as $pp)
            <?php $a+=1;?>
                <a class="relative inline-flex items-center px-4 py-2 text-sm font-medium text-white bg-primary border border-gray-300 leading-5 rounded-md hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150" href="{{$pp}}">{{arabic_w2e($a)}}</a>
            @endforeach
            <span>
                {{-- Next Page Link --}}
                @if ($posts->hasMorePages())
                    <a class="relative inline-flex items-center px-4 py-2 text-sm font-medium text-white bg-primary border border-gray-300 leading-5 rounded-md hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150" href="{{$posts->links()->elements[0][$a]}}">
                        {!! __('pagination.last') !!}
                    </a>
                @else
                    <span class="relative inline-flex items-center px-4 py-2 text-sm font-medium text-gray-500 bg-white border border-gray-300 cursor-default leading-5 rounded-md">
                        {!! __('pagination.next') !!}
                    </span>
                @endif
            </span>
        </nav>
    @endif
</div>
    </div>
</div>


<script type="text/javascript">

    document.addEventListener('livewire:load', function () {
            @this.on('pic_added', () => {
                $('.modal').modal('hide').data('bs.modal', null);
                $('.modal').remove();
                $('.modal-backdrop').remove();
                $('body').removeClass('modal-open');
                $('body').removeAttr('style');
            });
            
        })
</script>
@push('scripts')
<script src="https://cdn.plyr.io/3.7.8/plyr.polyfilled.js"></script>
<script>
    document.addEventListener('DOMContentLoaded', () => {
        const player = new Plyr('#player');
    });
</script>
@endpush
</div>