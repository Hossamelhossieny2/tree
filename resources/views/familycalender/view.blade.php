<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <!-- Main Header Events -->

<div class="main-header">
    <div class="content-bg-wrap bg-events"></div>
    <div class="container">
        <div class="row">
            <div class="col col-lg-8 m-auto col-md-8 col-sm-12 col-12">
                <div class="main-header-content">
                    <h1>{{ __('create_and_manage_events') }}</h1>
                    <p>{{ __('event_banner') }}</p>
                </div>
            </div>
        </div>
    </div>

    <img loading="lazy" class="img-bottom" src="{{ asset('img/event-bottom.png') }}" alt="friends" width="1169" height="146">
</div>

<!-- ... end Main Header Events -->


<div class="container">
    <div class="row">
        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="ui-block responsive-flex">
                <div class="ui-block-title">
                    <ul class="nav nav-tabs calendar-events-tabs" id="calendar-events-tabs" role="tablist">
                        
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" id="events-tab" data-bs-toggle="tab" href="#events" role="tab" aria-controls="home" aria-selected="true">
                                {{ __('calendar_and_events') }}
                            </a>
                        </li>

                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="members-tab" data-bs-toggle="tab" href="#members" role="tab" aria-controls="home" aria-selected="false">
                                {{ __('members_and_events') }} <span class="items-round-little bg-blue">{{ count($events) }}</span>
                            </a>
                        </li>

                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="notifications-tab" data-bs-toggle="tab" href="#notifications" role="tab" aria-controls="home" aria-selected="false">
                                {{ __('event_join_interest') }} <span class="items-round-little bg-breez">{{ count($event_joins) }}</span>
                            </a>
                        </li>

                        

                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>

@if (\Session::has('success'))
    <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
@endif

<!-- Tab panes -->
<div class="tab-content" id="calendar-events-tabs-content"  style="background-image: url( {{ asset('img/pattern/p20.png') }} )">
    <div class="tab-pane fade show active" id="events" role="tabpanel" aria-labelledby="events-tab">
        <div class="container">
            <div class="row">
                <div class="col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                    <div class="ui-block">


                        <!-- Today Events -->

                        <div class="today-events calendar">
                            <div class="today-events-thumb">
                                <div class="date">
                                    <div class="day-number">{{ date('d') }}</div>
                                    <div class="day-week">{{ date('l') }}</div>
                                    <div class="month-year">{{ date('M , Y') }}</div>
                                </div>
                            </div>

                            <div class="list">
                                <div class="control-block-button">
                                    <a href="#" class="btn btn-control bg-breez" data-bs-toggle="modal" data-bs-target="#create-event">
                                        <svg class="olymp-plus-icon">
                                            <use xlink:href="#olymp-plus-icon"></use>
                                        </svg>
                                    </a>
                                </div>

                                <div class="today-events calendar">
                            <div class="list">

                                <div class="control-block-button">

                                    <a href="#" class="btn btn-control bg-breez" data-bs-toggle="modal" data-bs-target="#create-event">
                                        <svg class="olymp-plus-icon">
                                            <use xlink:href="#olymp-plus-icon"></use>
                                        </svg>
                                    </a>

                                </div>

                                <div class="accordion day-event" id="accordionExample2">
                                @if($today_events)
                                    @foreach($today_events as $today)
                                    <div class="accordion-item">
                                        <div class="accordion-header" id="headingOne{{ $today['id'] }}">
                                            <div class="event-time">
                                                <time class="published" datetime="2017-03-24T18:18">00:00 / all day</time>
                                                <div class="more">
                                                    <svg class="olymp-three-dots-icon">
                                                        <use xlink:href="#olymp-three-dots-icon"></use>
                                                    </svg>
                                                    <ul class="more-dropdown">
                                                        <li>
                                                            <a href="#">{{ __('mark_as_completed') }}</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">{{ __('delete_event') }}</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne{{ $today['id'] }}" aria-expanded="true" aria-controls="collapseOne{{ $today['id'] }}">
                                                {{ $today['title'] }}
                                                <svg width="8" height="8">
                                                    <use xlink:href="#olymp-dropdown-arrow-icon"></use>
                                                </svg>
                                                <span class="event-status-icon" data-bs-toggle="modal" data-bs-target="#public-event">
                                                    <svg class="olymp-calendar-icon" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="UNCOMPLETED"><use xlink:href="#olymp-calendar-icon"></use></svg>
                                                </span>
                                            </button>
                                        </div>
                                        <div id="collapseOne{{ $today['id'] }}" class="accordion-collapse collapse @if($today_events[0]['id'] == $today['id'])show @endif" aria-labelledby="headingOne{{ $today['id'] }}" data-bs-parent="#accordionExample2">
                                            <div class="accordion-body">
                                                {{ __('for_more_details') }} .... <a onclick="{{ $today['url'] }}">{{ __('click_here') }}</a>
                                                <div class="place inline-items">
                                                    <div class="author-thumb">
                                                        @if(!empty($today['user_img']))
                                                        <img loading="lazy" src="{{ asset($today['user_img']) }}" alt="author" width="28" height="28">
                                                        @else
                                                        <img loading="lazy" src="{{ asset('img/default/user.png') }}" alt="author" width="28" height="28">
                                                        @endif
                                                    </div>
                                                    <span>{{ $today['user'] }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                            @else
                            <div class="accordion-item">
                                        <div class="accordion-header" id="headingOne0">
                                            <div class="event-time">
                                                <time class="published" datetime="2017-03-24T18:18">{{ __('today') }}</time>
                                            </div>
                                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne0" aria-expanded="true" aria-controls="collapseOne0">
                                                {{ __('there_is_no_events_today') }}
                                                <span class="event-status-icon" data-bs-toggle="modal" data-bs-target="#public-event">
                                                    <svg class="olymp-calendar-icon" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="UNCOMPLETED"><use xlink:href="#olymp-calendar-icon"></use></svg>
                                                </span>
                                            </button>
                                        </div>
                                        <div id="collapseOne0" class="accordion-collapse collapse show" aria-labelledby="headingOne0" data-bs-parent="#accordionExample2">
                                            <div class="accordion-body">
                                                <div class="place inline-items">
                                                    <div class="author-thumb">
                                                        
                                                        <img loading="lazy" src="{{ asset('img/default/system.png') }}" alt="author" width="28" height="28">
                                                        
                                                    </div>
                                                    <span>{{ __('system_robot') }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            @endif
                                </div>

                            </div>

                        </div>

                            </div>
                        </div>

                        <!-- ... end Today Events -->
                    </div>
                </div>
                <div class="col col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                    <div class="ui-block">

                        <!------------------------------------- Full Calendar ----------------------------------------->

                        <div id="calendar" class="crumina-full-calendar">


                        </div>

                        <!-- JS library for Full Calendar -->
                        <script src="{{ asset('js/libs/fullcalendar.min.js') }}"></script>
                        <!-- ...end JS library for Full Calendar -->
                        
                        <!-- JS-init for Full Calendar -->
                        <script>

                            /* -----------------------------
                                 * FullCalendar Init
                                 * Script file: fullcalendar.min.js
                                 * https://fullcalendar.io/
                                 https://fullcalendar.io/docs/event-object
                            * ---------------------------*/

                            /* get event details from table familyCalendar with event id */
                            function showDetails(id){
                                var link = location.protocol;
                                
                                $.ajax('/get_calender/'+id, {
                                    type: 'get',  // http method
                                    data: { myData: id },  // data to submit
                                    success: function (data, status, xhr) {
                                        var created = new Date(data['created_at']).toISOString().slice(0, 10);

                                        $('#modal-title').html(data['event_name']);
                                        $('#modal-time').html('Created at : '+created);
                                        $('#modal-user').html(data['user']);
                                        $('#modal-event-public').attr('src',link+'/'+'img/event-def.png');
                                        if(data['img']){
                                            $('#modal-user-img').attr('src',link+'/'+data['img']);
                                        }
                                        $('#modal-location').html(data['loc']);
                                        $('.modal-user-date').html(data['ev-d']);
                                        $('.modal-user-time').html(data['ev-t']);
                                        $('.modal-user-timez').html(data['ev-z']);
                                        $('#modal-desc').html(data['desc']);
                                        $('#modal-per').html(data['per']);
                                        $('#modal-event-p').html(data['ev']);
                                        if(data['ev'] == 'public'){

                                            $('#modal-event-public').show();

                                            $('#modal-event-private').hide();
                                            $('#modal-event-birth').hide();
                                            $('#modal-event-death').hide();
                                            $('#modal-event-wedding').hide();
                                        }else if(data['ev'] == 'private'){
                                            $('#modal-event-private').show();
                                            $('#modal-event-public').hide();
                                            $('#modal-event-birth').hide();
                                            $('#modal-event-death').hide();
                                            $('#modal-event-wedding').hide();
                                        }


                                        $('#private-event').modal('show');
                                    },
                                    error: function (jqXhr, textStatus, errorMessage) {
                                            $('p').append('Error' + errorMessage);
                                    }
                                });
                            }

                            function showNames(id,name){
                                $.ajax('/get_calender_date/'+id, {
                                    type: 'get',  // http method
                                    data: { myData: id },  // data to submit
                                    success: function (data, status, xhr) {
                                        if(name == 1){
                                            var anv = ' {{ __('birth_anv') }}';
                                            var dat = data['birth_date'];
                                        }else if(name == 2){
                                            var anv = ' {{ __('death_anv') }}';
                                            var dat = data['death_date'];
                                        }
                                        var link = location.protocol;
                                        var created = new Date(data['created_at']).toISOString().slice(0, 10);

                                        $('#modal-title').html(data['name'] + anv);
                                        $('#modal-time').html('{{ __('created_at') }}'+created);
                                        $('#modal-user').html('{{ __('system_robot') }}');
                                        console.log(data);
                                        if(data['profile_photo_path']){
                                            $('#modal-user-img').attr('src',link+'/'+data['profile_photo_path']);
                                        }
                                        $('#modal-location').html('{{ __('not_specified') }}');
                                        $('.modal-user-date').html(dat);
                                        $('.modal-user-time').html(' ');
                                        $('.modal-user-timez').html('{{ __('local_time') }}');
                                        $('#modal-desc').html(data['name'] + anv );
                                        $('#modal-per').html('{{ __('nor') }}');
                                        $('#modal-event-p').html('{{ __('pub') }}');
                                        
                                        if(name == 1){
                                            $('#modal-event-birth').show();
                                            $('#modal-event-death').hide();
                                            $('#modal-event-wedding').hide();
                                            $('#modal-event-public').hide();
                                            $('#modal-event-private').hide();
                                        }else if(name == 2){
                                            $('#modal-event-birth').hide();
                                            $('#modal-event-death').show();
                                            $('#modal-event-wedding').hide();
                                            $('#modal-event-public').hide();
                                            $('#modal-event-private').hide();
                                        }
                                        if(data['name']){
                                            $('#joinButtom').show();
                                        }else{
                                            $('#joinButtom').hide();
                                        }
                                        $('#private-event').modal('show');
                                    },
                                    error: function (jqXhr, textStatus, errorMessage) {
                                            $('p').append('Error' + errorMessage);
                                    }
                                });
                            }

                            function showCountry(id){
                                var link = location.protocol;
                                $.ajax('/get_calender_country/'+id, {
                                    type: 'get',  // http method
                                    data: { myData: id },  // data to submit
                                    success: function (data, status, xhr) {

                                        var pathArray = window.location.pathname.split('/');
                                        var secondLevelLocation = pathArray[1];
                                        var tit = 'title_'+pathArray[1];
                                        var created = new Date(data['created_at']).toISOString().slice(0, 10);

                                        $('#modal-title').html(data[tit]);
                                        $('#modal-time').html('{{ __('created_at') }}'+created);
                                        $('#modal-user').html(data['user']);
                                        if(data['image']){
                                            $('#modal-user-img').attr('src',data['img']);
                                            $('#modal-event-public').attr('src',data['image']);
                                            $('#event-flag').attr('src',link+'/img/flag/'+data['country_iso']+'.svg');
                                            console.log(data['image']);
                                        }
                                        $('#modal-location').html(data['country_iso']);
                                        $('.modal-user-date').html(data['start_date']);
                                        $('.modal-user-time').html('00:00');
                                        $('.modal-user-timez').html(data['country_iso']);
                                        $('#modal-desc').html('{{ __('country_vac') }}');
                                        $('#modal-per').html('{{ __('nor') }}');
                                        $('#modal-event-p').html('{{ __('pub') }}');
                                            
                                            $('#event-flag').show();
                                            $('#modal-event-public').show();
                                            $('#modal-event-private').hide();
                                            $('#modal-event-birth').hide();
                                            $('#modal-event-death').hide();
                                            $('#modal-event-wedding').hide();


                                        $('#private-event').modal('show');
                                    },
                                    error: function (jqXhr, textStatus, errorMessage) {
                                            $('p').append('Error' + errorMessage);
                                    }
                                });
                            }

                            function showMarriage(id,wife,kind){
                                var link = location.protocol;
                                $('#modal-event-public').attr('src',link+'/'+'img/event-def.png');
                                $.ajax('/get_calender_marriage/'+id+'/'+wife+'/'+kind, {
                                    type: 'get',  // http method
                                    data: { myData: id,wife,kind },  // data to submit
                                    success: function (data, status, xhr) {
                                        var created = new Date(data['created_at']).toISOString().slice(0, 10);

                                        $('#modal-title').html('{{ __('marr_anv') }}');
                                        $('#modal-time').html('{{ __('created_at') }}'+created);
                                        $('#modal-user').html(data['user']);
                                        if(data['img']){
                                            $('#modal-user-img').attr('src',data['img']);
                                        }
                                        $('#modal-location').html('{{ __('not_specified') }}');
                                        $('.modal-user-date').html(data['start']);
                                        $('.modal-user-time').html('00:00');
                                        $('.modal-user-timez').html('CAI');
                                        if(data['end']){
                                            $('#modal-desc').html(data['husband']+' marriage with '+data['wife']+' Anniversary But dont forget it ended on '+data['end']);
                                        }else{
                                            $('#modal-desc').html(data['husband']+' marriage with '+data['wife']+' Anniversary and still Happy ');
                                        }
                                        
                                        $('#modal-per').html('{{ __('nor') }}');
                                        $('#modal-event-p').html('{{ __('pub') }}');
                                        $('#modal-event-wedding').show();
                                        $('#modal-event-public').hide();
                                        $('#modal-event-private').hide();
                                        $('#modal-event-birth').hide();
                                            $('#modal-event-death').hide();
                                        
                                        $('#private-event').modal('show');
                                    },
                                    error: function (jqXhr, textStatus, errorMessage) {
                                            $('p').append('Error' + errorMessage);
                                    }
                                });
                            }
                            

                            fullCalendar = function () {
                                var calendarEl = document.getElementById('calendar');

                                var calendar = new FullCalendar.Calendar(calendarEl, {
                                    plugins: ['interaction', 'dayGrid', 'timeGrid'],
                                    defaultView: 'dayGridMonth',
                                    defaultDate: '{{ date("Y-m-d")}}',
                                    header: {
                                        left: 'prev',
                                        center: 'title',
                                        right: 'next,dayGridMonth,timeGridWeek,timeGridDay'
                                    },
                                    buttonText: {
                                        month: ' ',
                                        week: ' ',
                                        day: ' '
                                    },
                                    buttonIcons: {
                                        prev: 'fas fa-arrow-left',
                                        next: 'fas fa-arrow-right'
                                    },

                                    eventClick: function (info) {
                                        var url = info.event.url;
                                        var is_modal = url.match(/^modal\:(#[-\w]+)$/);
                                        if (!is_modal) {
                                            return;
                                        }

                                        info.jsEvent.preventDefault();
                                        var modal = is_modal[1];

                                        $(modal).modal('show');
                                    },
                                    events: [
                                    <?php foreach ($events as $event) {
                                        echo "{";
                                        echo "title: '".$event['title']."',";
                                        echo "start: '".$event['start']."',";
                                        if(!empty($event['end'])){
                                        echo "end: '".$event['end']."',";
                                        }
                                        echo "url: '".$event['url']."',";
                                        echo "textColor: '".$event['txt']."',";
                                        echo "backgroundColor: '".$event['bg']."',";
                                        echo "allDay: '".$event['day']."',";
                                        echo "borderColor: 'green'";
                                        echo "},";

                                    } ?>
                                        
                                    ],
                                });

                                calendar.render();
                            };

                            document.addEventListener("DOMContentLoaded", function () {
                                fullCalendar();
                            });

                        </script>
                        <!-- ... end JS-init for Full Calendar -->

                        <!------------------------------------- ... end Full Calendar ----------------------------------------->


                        
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="tab-pane fade" id="notifications" role="tabpanel" aria-labelledby="notifications-tab">
        <div class="container">
            <div class="row">
                <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="ui-block">


                        <table class="event-item-table event-item-table-fixed-width">

                            <thead>

                            <tr>

                                <th class="author">
                                    {{ __('notification') }}
                                </th>

                                <th class="location">
                                    {{ __('place') }}
                                </th>

                                <th class="upcoming">
                                    {{ __('date') }}
                                </th>

                                <th class="description">
                                    {{ __('description') }}
                                </th>

                                <th class="users">
                                    {{ __('assistants') }}
                                </th>

                                <th class="add-event">

                                </th>
                            </tr>

                            </thead>

                            <tbody>
                                @foreach($event_joins as $event_join)
                                    <tr class="event-item">
                                        <td class="author">
                                            <div class="event-author inline-items">
                                                <div class="author-thumb">
                                                    <img loading="lazy" src="{{ asset($event_join->events->userEvents->profile_photo_path) }}" width="34" height="34" alt="author">
                                                </div>
                                                <div class="author-date">
                                                    <a href="#" class="author-name h6">{{ $event_join->events->userEvents->name }}</a> 
                                                    {{ __('add_this_event') }} <br> <a onclick="javascript:showDetails({{$event_join->events->id}})">{{ $event_join->events->event_name }}</a>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="location">
                                            <div class="place inline-items">
                                                <svg class="olymp-add-a-place-icon">
                                                    <use xlink:href="#olymp-add-a-place-icon"></use>
                                                </svg>
                                                <span>{{ $event_join->events->event_location }}</span>
                                            </div>
                                        </td>
                                        <td class="upcoming">
                                            <div class="date-event inline-items align-left">
                                                <svg class="olymp-small-calendar-icon">
                                                    <use xlink:href="#olymp-small-calendar-icon"></use>
                                                </svg>

                                                <span class="month">{{ $event_join->events->event_date.' '.$event_join->events->event_time.' '.$event_join->events->event_timezone }}</span>

                                            </div>
                                        </td>
                                        <td class="description">
                                            <p class="description">{{ $event_join->events->event_description }}</p>
                                        </td>
                                        <td class="users">
                                             <div class="author-thumb">
                                                @if(!empty($event_join->user_join->profile_photo_path))
                                                <img loading="lazy" src="{{ asset($event_join->user_join->profile_photo_path) }}" width="34" height="34" alt="author">
                                                @else
                                                <img loading="lazy" src="{{ asset('img/default/user_'.$event_join->user_join->gender.'.png') }}" width="34" height="34" alt="author">
                                                @endif
                                            </div>
                                            <span>{{ $event_join->user_join->name }}</span><br>On :
                                            <span>{{ $event_join->user_join->created_at }}</span>

                                        </td>
                                        <td class="add-event">
                                             @if($event_join->confirm == 'join')
                                                <a class="btn btn-blue btn-sm">Confirmed Join</a>
                                            @elseif($event_join->confirm == 'maybe')
                                                <a class="btn btn-breez btn-sm">Not Confirmed yet</a>
                                            @elseif($event_join->confirm == 'interest')
                                                <a class="btn btn-sm btn-border-think custom-color c-grey">{{ __('just_interest') }}</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            <tr class="event-item"><td class="text-center" colspan="6">{{ __('no_more_events') }} </td></tr>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="tab-pane fade" id="members" role="tabpanel" aria-labelledby="members-tab">
        <div class="container">
    <div class="row">

        @for($i=1;$i<13;$i++)

        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="ui-block" style="background-color:#04DDC0;">
                <div class="ui-block-title">
                    <h6 class="title" >{{\Carbon\Carbon::createFromFormat('m',$i)->format('F')}}</h6>
                </div>
            </div>
        </div>
        
            @foreach($events as $event)

            @if(\Carbon\Carbon::createFromFormat('Y-m-d', $event['start'])->format('m') == $i)
            <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="ui-block">
                    <!-- Birthday Item -->

                    <div class="birthday-item inline-items">

                        
                            @if($event['type'] != "Coutnry vacation")
                            <div class="author-thumb">
                                @if(!empty($event['pic']))
                                <img loading="lazy" src="{{ asset($event['pic']) }}" alt="author" width="42" height="42">
                                @else
                                <img loading="lazy" src="{{ asset('img/default/user.png') }}" alt="author" width="42" height="42">
                                @endif
                            </div>
                            @else
                                <img loading="lazy" src="{{ asset(strtolower($event['pic'])) }}" alt="author" width="35" height="35">
                            @endif
                        
                        @if(!empty($event['pic2']))
                        <div class="author-thumb">
                            <img loading="lazy" src="{{ asset($event['pic2']) }}" alt="author" width="42" height="42">
                        </div>
                        @endif
                        <div class="birthday-author-name">
                            <a href="#" class="h6 author-name">{{$event['title']}} </a>
                            <div class="birthday-date">{{$event['start']}}</div>
                        </div>
                        <a class="btn" style="background-color: {{ $event['bg'] }};color: {{ $event['txt'] }}" >{{$event['type']}}</a>
                    </div>
                    <!-- ... end Birthday Item -->
                </div>
            </div>
            @endif
            @endforeach

@endfor

        

    </div>
</div>
    </div>
</div>



<!-- Window-popup Event  -->

<div class="modal fade" id="private-event" tabindex="-1" role="dialog" aria-labelledby="private-event" aria-hidden="true">
    <div class="modal-dialog window-popup event-private-public public-event" role="document">
        <div class="modal-content">
            <a href="#" class="close icon-close" data-bs-dismiss="modal" aria-label="Close">
                <svg class="olymp-close-icon">
                    <use xlink:href="#olymp-close-icon"></use>
                </svg>
            </a>
            <div class="modal-body">
                <article class="hentry post has-post-thumbnail thumb-full-width private-event">

                    <div class="private-event-head inline-items">
                        <img loading="lazy" src="{{ asset('img/default/system.png') }}" alt="author" width="40" height="40">

                        <div class="author-date">
                            <p class="h3 event-title" id="modal-title">default tilte</p>
                            <div class="event__date">
                                <p class="published" id="modal-time">default time</p>
                                
                            </div>
                        </div>
                    </div>
                    <div class="post-thumb">
                        <img loading="lazy" id="modal-event-public" src="{{ asset('img/event-def.png') }}" alt="photo" width="769" height="100" style="display: none;">
                        <img loading="lazy" id="modal-event-private" src="{{ asset('img/event-def2.png') }}" alt="photo" width="769" height="100" style="display: none;">
                        <img loading="lazy" id="modal-event-birth" src="{{ asset('img/event_birth.png') }}" alt="photo" width="769" height="100" style="display: none;">
                        <img loading="lazy" id="modal-event-death" src="{{ asset('img/event_death.png') }}" alt="photo" width="769" height="100" style="display: none;">
                        <img loading="lazy" id="modal-event-wedding" src="{{ asset('img/event_wedding.png') }}" alt="photo" width="769" height="100" style="display: none;">
                    </div>
                    <div class="row">
                        <div class="col col-lg-7 col-md-7 col-sm-12 col-12">
                            <div class="post__author author vcard inline-items">
                                <img loading="lazy" id="modal-user-img" src="{{ asset('img/default/system.png') }}" alt="author" width="42" height="42">

                                <div class="author-date">
                                    <a class="h6 post__author-name fn" id="modal-user" href="#">{{ __('system_robot') }}</a> {{ __('create_event') }}
                                    <div class="post__date">
                                        <!-- <time class="published" datetime="2017-03-24T18:18">
                                            March 11 at 9:52pm
                                        </time> -->
                                        <span class="modal-user-timez">C</span> <span class="modal-user-date">1</span> <span class="modal-user-time">1</span>
                                    </div>
                                </div>

                            </div>

                            <p id="modal-desc">
                                We’ll be playing in the Gotham Bar in May. Come and have a great time with us! Entry: $12
                            </p>
                        </div>
                        <div class="col col-lg-5 col-md-5 col-sm-12 col-12">
                            <div class="event-description">

                                <h6 class="event-description-title" id="modal-event-p">Public </h6> <h6>{{ __('event') }}</h6>

                                <div class="place inline-items">
                                    <svg class="olymp-add-a-place-icon">
                                        <use xlink:href="#olymp-add-a-place-icon"></use>
                                    </svg>
                                    <span id="modal-location">Gotham Bar</span>
                                     <img loading="lazy" id="event-flag" src="" width="50" style="display: none;">
                                </div>

                                <div class="place inline-items">
                                    <svg class="olymp-add-a-place-icon">
                                        <use xlink:href="#olymp-add-a-place-icon"></use>
                                    </svg>
                                    <span class="modal-user-timez">C</span> <span class="modal-user-date">1</span> <span class="modal-user-time">1</span>
                                </div>

                                <!-- <a href="#" class="btn btn-breez btn-sm full-width">Invite Friends</a> -->

                                <a href="#" id="joinButtom" data-bs-toggle="modal" data-bs-target="#join-event" class="btn btn-blue btn-sm full-width">{{ __('join_this_event_add_interst') }}</a>
                            </div>
                        </div>
                    </div>


                </article>

                
            </div>
        </div>
    </div>
</div>

<!-- ... end Window-popup Event  -->


<!-- Window-popup Create Event -->

<div class="modal fade" id="create-event" tabindex="-1" role="dialog" aria-labelledby="create-event" aria-hidden="true">
    <div class="modal-dialog window-popup create-event" role="document">
        <div class="modal-content">
            <a href="#" class="close icon-close" data-bs-dismiss="modal" aria-label="Close">
                <svg class="olymp-close-icon">
                    <use xlink:href="#olymp-close-icon"></use>
                </svg>
            </a>
            <div class="modal-header">
                <h6 class="title">{{ __('create_an_event') }}</h6>
            </div>
            <form class="form-group" action="{{ route('add_event', app()->getLocale()) }}" method="post">
                @csrf
            <div class="modal-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="form-group label-floating">
                    <label class="control-label">{{ __('event_name') }}</label>
                    <input name="event_title" class="form-control" placeholder="" value="" type="text" required="">
                </div>

                <div class="form-group label-floating is-empty">
                    <label class="control-label">{{ __('event_location') }}</label>
                    <input name="event_place" class="form-control" placeholder="" value="" type="text" required="">
                </div>

                <div class="form-group date-time-picker label-floating">
                    <label class="control-label">{{ __('event_date') }}</label>
                    <input type="date" name="event_day" value="{{ date('Y-m-d') }}" min="{{ \Carbon\Carbon::today()->toDateString() }}" required="">
                    <span class="input-group-addon">
                    <svg class="olymp-calendar-icon icon"><use xlink:href="#olymp-calendar-icon"></use></svg>
                </span>
                </div>

                <div class="row">
                    <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="form-group label-floating">
                            <label class="control-label">{{ __('event_time') }}</label>
                            <input name="event_time" class="form-control" placeholder="" value="09:00" type="time" required="">
                        </div>
                    </div>
                    <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="form-group label-floating is-select">
                            <label class="control-label">{{ __('timezone') }}</label>
                            <select name="event_timezone" class="form-select">
                                <option value="CAI">CAI (UTC+2)</option>
                                <option value="KSA">KSA (UTC+3)</option>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <textarea name="event_desc" class="form-control" placeholder="Event Description" required=""></textarea>
                </div>

                <div class="form-group label-floating is-select">
                    <label class="control-label">{{ __('event_priority') }}</label>
                    <select name="event_per" class="form-select">
                        <option value="hight">{{ __('hight_priority') }}</option>
                        <option value="normal">{{ __('normal_priority') }}</option>
                        <option value="low">{{ __('low_priority') }}</option>
                    </select>
                </div>

                <div class="form-group label-floating is-select">
                    <label class="control-label">{{ __('event_privacy') }}</label>
                    <select name="event_privacy" class="form-select">
                        <option value="public">{{ __('public_event') }}</option>
                        <option value="private">{{ __('private_event') }}</option>
                    </select>
                </div>
                
                <button type="submit" class="btn btn-breez btn-lg full-width">{{ __('create_event') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- ... end Window-popup Create Event -->

<!-- Window-popup Join Event -->

<div class="modal fade" id="join-event" tabindex="-1" role="dialog" aria-labelledby="join-event" aria-hidden="true">
    <div class="modal-dialog window-popup join-event" role="document">
        <div class="modal-content">
            <a href="#" class="close icon-close" data-bs-dismiss="modal" aria-label="Close">
                <svg class="olymp-close-icon">
                    <use xlink:href="#olymp-close-icon"></use>
                </svg>
            </a>
            <div class="modal-header">
                <h6 class="title">{{ __('join_an_event') }}</h6>
            </div>
            <form class="form-group" action="{{ route('join_event', app()->getLocale()) }}" method="post">
                @csrf
            <div class="modal-body">

                <input type="hidden" name="event_id" value="1">

                <div class="form-group label-floating is-select">
                    <label class="control-label">{{ __('event_response') }}</label>
                    <select name="event_confirm" class="form-select">
                        <option value="join">{{ __('sure') }}</option>
                        <option value="maybe">{{ __('maybe') }}</option>
                        <option value="interest">{{ __('just') }}</option>
                    </select>
                </div>

                <button type="submit" class="btn btn-breez btn-lg full-width">{{ __('join_this_event') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- ... end Window-popup Join Event -->

@if(!isset($needHelp))
@push('modals')
<!-- Faqs Popup -->

<div class="modal fade" id="faqs-popup" tabindex="-1" role="dialog" aria-labelledby="faqs-popup" aria-hidden="true">
    <div class="modal-dialog window-popup faqs-popup" role="document">
        <div class="modal-content">
            <a href="#" class="close icon-close" data-bs-dismiss="modal" aria-label="Close">
                <svg class="olymp-close-icon">
                    <use xlink:href="#olymp-close-icon"></use>
                </svg>
            </a>

            <div class="modal-header">
                <h4 class="title" id="faqs-title">{{__('welcome_in').__('calendar_and_events')}}</h4>
            </div>

            <div class="modal-body">

                <div class="accordion" id="accordionExample">
                    
                    @foreach($FaqsHelp as $faq)
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="heading{{$faq->id}}">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{$faq->id}}" aria-expanded="true" aria-controls="collapse{{$faq->id}}">
                                <span class="c-green">- </span> {{$faq->title}}
                            </button>
                        </h2>
                        <div id="collapse{{$faq->id}}" class="accordion-collapse collapse @if($faq->id == $FaqsHelp[0]->id) show @endif" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                {{$faq->desc}}
                            </div>
                        </div>
                    </div>
                    @endforeach
                    
                </div>
                <hr>
                <a data-bs-dismiss="modal" class="close btn btn-blue btn-lg">{{__('dismiss')}}</a>
                <a data-bs-dismiss="modal" class="close btn btn-lg btn-primary" onclick="removeHelp({{$faq->topic_id}})">{{__('dont_show')}}</a>
            </div>
        </div>
    </div>
</div>

<!-- ... end Faqs Popup -->
@endpush

@push('scripts')
<script type="text/javascript">
    $( document ).ready(function() {
        
        $('#faqs-popup').modal('show');
    });        

    function removeHelp(argument) {
  
  var topic = argument;

    $.ajax({
            url: "{{ route('remove_help',app()->getLocale()) }}",
            type:"POST",
            data:{
              Topic:topic,
              _token: $('meta[name="csrf-token"]').attr('content')
            },
            success:function(response){
              //console.log(response);
              if(response) {
                $('.success').text(response.success);
                $("#helpCloseButton").click();
              }
            },
       });
    }
</script>
@endpush

@endif

</x-app-layout>

