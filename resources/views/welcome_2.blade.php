<!DOCTYPE html>
<html lang="en">
<head>

    <title>Main Landing Page</title>

    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">


    <!-- Theme Font -->
    <link rel="preload" type="text/css" href="{{ asset('css/theme-font.min.css') }}" as="style">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('Bootstrap/dist/css/bootstrap.css') }}">

    <!-- Main Styles CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.min.css') }}">

    <!-- Main RTL CSS -->
    <!--<link rel="stylesheet" type="text/css" href="css/rtl.min.css">-->
<style type="text/css">
    article.accordion
{
  display: block;
  width: 43em;
  height: 63em;
  margin: 0 auto;
  background-color: #666;
  overflow: auto;
  border-radius: 5px;
  box-shadow: 0 3px 3px rgba(0,0,0,0.3);
}
article.accordion section
{
  position: relative;
  display: block;
  float: left;
  width: 5em;
  height: 30em;
  margin: 0.5em 0 0.5em 0.5em;
  color: #333;
  background-color: #333;
  overflow: hidden;
  border-radius: 3px;
}
article.accordion section h2
{
  position: absolute;
  font-size: 3em;
  font-weight: bold;
  width: 12em;
  height: 2em;
  top: 12em;
  left: 0;
  text-indent: 1em;
  padding: 0;
  margin: 0;
  color: #ddd;
  -webkit-transform-origin: 0 0;
  -moz-transform-origin: 0 0;
  -ms-transform-origin: 0 0;
  -o-transform-origin: 0 0;
  transform-origin: 0 0;
  -webkit-transform: rotate(-90deg);
  -moz-transform: rotate(-90deg);
  -ms-transform: rotate(-90deg);
  -o-transform: rotate(-90deg);
  transform: rotate(-90deg);
}
article.accordion section h2 a
{
  display: block;
  width: 100%;
  line-height: 2em;
  text-decoration: none;
  color: inherit;
  outline: 0 none;
}
article.accordion section:target
{
  width: 25em;
  height: 30em;
  padding: 0 1em;
  color: #333;
  background-color: #fff;
}
article.accordion section:target h2
{
  position: static;
  font-size: 1.3em;
  text-indent: 0;
  color: #333;
  -webkit-transform: rotate(0deg);
  -moz-transform: rotate(0deg);
  -ms-transform: rotate(0deg);
  -o-transform: rotate(0deg);
  transform: rotate(0deg);
}
article.accordion section,
article.accordion section h2
{
  -webkit-transition: all 1s ease;
  -moz-transition: all 1s ease;
  -ms-transition: all 1s ease;
  -o-transition: all 1s ease;
  transition: all 1s ease;
}

.btn-container{
  display: inline-block;
  margin: 70px auto 0;
  font-size: 0;
  border: 1px solid #fff;
}

label{
  position: relative; 
  cursor: pointer;
  font-size: 12px;
  display: inline-block;
  text-transform: uppercase;
  letter-spacing: 5px;  
  width: 200px;
  padding: 13px 0;
  transition: all 0.3s ease;  
}

#pizza + label:before{
  content: "";
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 100%;
  z-index: -1;
  background-color: #fff;
  transition: all 0.3s ease;
}

#pizza:checked + label:before{
  left: 0;
}

#pizza:checked + label,
#pasta:checked + label{
  color: #000;
  background-color: #fff;
}

</style>

</head>

<body class="content-bg-wrap bg-landing">


  <article class="accordion content-bg-wrap bg-landing">
  <section id="acc1">
    <h2><a href="#acc1">{{__('tree_builder')}}</a></h2>
    <hr>
    <dl style="text-align: left;font-size: 18px">
        <li>{{__('complete_social_network')}}</li>
        <li>{{ $tree->desc }}</li>
    </dl>
  </section>
  <section id="acc2">
    <h2><a href="#acc2">{{__('your_init_chart')}}</a></h2>
    <hr>
    <dl style="text-align: left;font-size: 18px">
        <li>{{__('your_init_chart')}}</li>
        <li>{{ $tree1->desc }}</li>
    </dl>
  </section>
  <section id="acc3">
    <h2><a href="#acc3">{{__('personality')}}</a></h2>
    <hr>
    <dl style="text-align: left;font-size: 18px">
        <li>{{ $tree2->desc }}</li>
    </dl>
  </section>
   

  <section id="acc4">
    <h2><a href="#acc4">.</a></h2>
    
        <a href="{{ route('webfamily', ['ar']) }}">
                <img loading="lazy" src="{{ asset('img/default/ar.png') }}" title="اللغة العربية" width="34" height="34">
        </a>
        <a href="{{ route('webfamily', ['en']) }}">
                <img loading="lazy" src="{{ asset('img/default/en.png') }}" title="English language" width="34" height="34">
        </a>
        <a href="{{ route('webfamily', ['fr']) }}">
                <img loading="lazy" src="{{ asset('img/default/fr.png') }}" title="langue française" width="34" height="34">
        </a>
        <a href="{{ route('webfamily', ['tr']) }}">
                <img loading="lazy" src="{{ asset('img/default/tr.png') }}" title="Türk Dili" width="34" height="34">
        </a>
        <a href="{{ route('webfamily', ['ru']) }}">
                <img loading="lazy" src="{{ asset('img/default/ru.png') }}" title="русский язык" width="34" height="34">
        </a>
        <a href="{{ route('webfamily', ['de']) }}">
                <img loading="lazy" src="{{ asset('img/default/de.png') }}" title="deutsche Sprache" width="34" height="34">
        </a>
        <a href="{{ route('webfamily', ['es']) }}">
                <img loading="lazy" src="{{ asset('img/default/es.png') }}" title="lengua española" width="34" height="34">
        </a>
        <a href="{{ route('webfamily', ['cn']) }}">
                <img loading="lazy" src="{{ asset('img/default/cn.png') }}" title="中文" width="34" height="34">
        </a>
    
  </section>
  <div class="container background-color">
        <div class="row display-flex">
            <div class="col col-12">
                <div class="ui-block">
                 <x-jet-validation-errors class="mb-4" />
            @if (session('status'))
                <div class="mb-4 font-medium text-sm text-green-600">
                    {{ session('status') }}
                </div>
            @endif

             <form class="form-control" method="POST" action="{{ route('HomeLogin',app()->getLocale()) }}">
                @csrf
                        <div class="btn-container" style="
                            background: #000;
                              color: #fff;  
                              text-align: center;
                              font-weight: 100;
                              font-family: sans-serif;">
                          <input type="radio" name="user" id="pizza" value="old" checked hidden/>
                          <label for="pizza">old user</label>  
                          <input type="radio" name="user" id="pasta" value="new" hidden/>
                          <label for="pasta">new user</label> 
                        </div>

                        <div class="form-group label-floating">
                            <label class="control-label">Email</label>
                            <input class="form-control" type="email" name="email" :value="old('email')" required autofocus />
                        </div>
                   
                        <div class="form-group label-floating">
                            <label class="control-label">Password</label>
                            <input class="form-control" type="password" name="password" required autocomplete="current-password" />
                        </div>
                        
                        <div class="form-group">
                          <select class="form-control" name="part">
                              <option value="tree_view">{{__('tree_builder')}}</option>
                              <option value="wheel_view">{{__('wheel_life')}}</option>
                              <option value="personality_view">{{__('personality')}}</option>
                          </select>
                        </div>
                        
                    <input type="hidden" name="country" value="{{$user_country}}" />
                    <button class="btn btn-secondary full-width">{{__('login')}} / {{__('register')}}</button>
                                
                </form>

                </div>
            </div>
        </div>
    </div>
</article>





<a class="back-to-top" href="#">
    <svg class="back-icon" width="14" height="18"><use xlink:href="#olymp-back-to-top"></use></svg>
</a>



</body>
</html>