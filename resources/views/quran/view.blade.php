<x-app-layout>



<div class="container">
    <div class="row">

        <div class="col-12">

        	<article class="hentry blog-post blog-post-v3">
                	<div class="post-content" style="background-image: url( http://localhost/tree/public/img/pattern/pattern5.png )">
                
                        <div class="author-date">
                            
                            <a class="h6 post__author-name fn" href="#">آخر دخول للحفظ</a>
                            <div class="post__date">
                                <time class="published" datetime="2017-03-24T18:18">
                                    22-10-2020
                                </time>
                            </div>
                        </div>
                
                        <a href="#" class="h3 post-title">الدخول للحفظ</a>
                        <p>{{arabic_w2e(count($memos))}} سورة</p>
                
                        <div class="post-additional-info inline-items">
                
                            <div class="friends-harmonic-wrap">
                               
                                <div class="names-people-likes">
                                
                                </div>
                            </div>
                
                            <div class="comments-shared">
                                <a href="{{ route('quran.memo',[app()->getLocale()]) }}" class="text-white post-add-icon inline-items btn btn-lg btn-purple full-width">
                                   أكمل الحفظ
                                </a>
                            </div>
                
                        </div>
                    </div>

                    <div class="post-thumb">
                        <img loading="lazy" src="{{ asset('img/quran_face.jpg')}}" alt="photo" width="370" height="261">
                    </div>
                
                    <div class="post-content" style="background-image: url( http://localhost/tree/public/img/pattern/pattern5.png )">
                
                        <div class="author-date">
                            
                            <a class="h6 post__author-name fn" href="#">آخر دخول للمراجعة</a>
                            <div class="post__date">
                                <time class="published" datetime="2017-03-24T18:18">
                                    ---
                                </time>
                            </div>
                        </div>
                
                        <a href="#" class="h3 post-title">الدخول للمراجعة</a>
                        <p>٠ سورة</p>
                
                        <div class="post-additional-info inline-items">
                
                            <div class="friends-harmonic-wrap">
                               
                                <div class="names-people-likes">
                                
                                </div>
                            </div>
                
                            <div class="comments-shareds">
                                <a href="new['link']" class="text-white btn btn-lg btn-purple full-width">
                                   راجع ماتم حفظه
                                </a>
                            </div>
                
                        </div>
                    </div>
                
                </article>

        </div>

        
    </div>
</div>
<div class="clear"><br></div>
<div class="container">
	<div class="row">

		@for($i=114;$i>0;$i-=1)
		<div class="col-2">
			<div class="ui-block" style="border: 1px solid #000;text-align:center">
				
				<div class="birthday-item inline-items bg-grey">
					<!--  -->
					<div class="birthday-author-name">
						<a href="#" class="text-white h6 author-name">{{App\Models\QuranSurah::find($i)->arabic}}</a>
						<div class="birthday-date">رقم {{arabic_w2e($i)}} - آياتها {{arabic_w2e(App\Models\QuranSurah::find($i)->ayah)}}</div>
					</div>
				</div>
				<div class="author-thumb">
						@if(isset($memos[$i]) && $memos->last()[0]['sura'] != $i)
						<img loading="lazy" src="{{ asset('img/default/done.png') }}" alt="author" width="100" height="42">
						@else
							@if($memos->last()[0]['sura'] == $i)
							<img loading="lazy" src="{{ asset('img/default/inprogress.png') }}" alt="author" width="100" height="42">
							@else
						 	<img loading="lazy" src="{{ asset('img/default/notdone.png') }}" alt="author" width="100" height="42">
						 	@endif
						 @endif 
					</div>
			</div>
		</div>
		@endfor
		
	</div>
</div>

</x-app-layout>