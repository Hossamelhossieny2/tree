<x-app-layout>
@push('styles')
<link href="https://db.onlinewebfonts.com/c/a565b17e58e287300f1cd79645ae1770?family=KFGQPC+HAFS+Uthmanic+Script+Bold"
            rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
            integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/quran.css') }}" />
    <style>
      .LineAya{
        font-family: "KFGQPC HAFS Uthmanic Script Bold";
        /* font-size: 18px;
        line-height: 0.8; */
      }
    </style>
@endpush
<div class="container">
    <div class="row" style="background-color: #d9ecf6;">

        <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">

        	<div class="quran_content"> 
                <div class="ReadAya">
                    <p class="page_head" style="padding-right: 60px;">{{ $rightPagee['juzR'] }}
                        @if(isset($rightPagee['finishR']))
                        -( {{ $leftPagee['hezbL'] }} )-
                        @endif
                    </p> 

                    <ul> 
                            @for($i=1;$i<16;$i++)
                                {!! $rightPagee[$i] !!}
                            @endfor
                    </ul> 
                    <p class="page_foot">{{ $rightPagee['numberR'] }}</p> 
                </div> 
            </div> 
        </div>
        <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
            <div class="quran_content"> 
                <div class="ReadAya">
                    <p class="page_head" style="padding-right: 350px;">{{ $leftPagee['juzL'] }}
                         @if(isset($leftPagee['finishL']))
                        -( {{ $leftPagee['hezbL'] }} )-
                        @endif
                    </p>
                    <ul> 
                            @for($i=1;$i<16;$i++)
                                {!! $leftPagee[$i] !!}
                            @endfor
                    </ul> 
                    <p class="page_foot">{{ $leftPagee['numberL'] }}</p> 
                </div>  
            </div> 

        </div>

        
    </div>
</div>
<div class="clear"><br></div>
@if(!empty($meta['last_aya']))
<div class="container">
    <div class="row" style="background-image: url( http://localhost/tree/public/img/pattern/pattern5.png );padding: 20px">
        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <button type="button" class="btn btn-purple btn-lg full-width" data-toggle="modal" data-target="#mashary">
              <i class="fa fa-play" aria-hidden="true"></i> إستماع وتفسير
            </button>
        </div>
        <div class="row">
            <div class="col col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <button type="button" class="btn btn-primary btn-lg full-width" data-toggle="modal" data-target="#hussary">
                  <i class="fa fa-play" aria-hidden="true"></i> (الحصري)
                </button>
            </div>
            <div class="col col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <button type="button" class="btn btn-primary btn-lg full-width" data-toggle="modal" data-target="#khalafi">
                  <i class="fa fa-play" aria-hidden="true"></i> (ماهر المعيقلي)
                </button>
            </div>
            <div class="col col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <button type="button" class="btn btn-primary btn-lg full-width" data-toggle="modal" data-target="#minshawy">
                  <i class="fa fa-play" aria-hidden="true"></i> (عبد الباسط)
                </button>
            </div>
            <div class="col col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <button type="button" class="btn btn-primary btn-lg full-width" data-toggle="modal" data-target="#tafseer4">
                  <i class="fa fa-eye" aria-hidden="true"></i> الطبري
                </button>
            </div>
            <div class="col col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <button type="button" class="btn btn-primary btn-lg full-width" data-toggle="modal" data-target="#tafseer2">
                  <i class="fa fa-eye" aria-hidden="true"></i> ابن كثير
                </button>
            </div>
            <div class="col col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <button type="button" class="btn btn-primary btn-lg full-width" data-toggle="modal" data-target="#tafseer3">
                  <i class="fa fa-eye" aria-hidden="true"></i> الجلالين
                </button>
            </div>
        </div>


    </div>
</div>
@endif
<!-- Modal -->
        <div class="modal fade" id="mashary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="massryLabel">{{$meta['last_aya']}}</h4>
                  </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close1" style="display: none;"><span aria-hidden="true">&times;</span></button>
              <div class="modal-body" style="text-align:right;">
                <video controls id="video1" style="width: 100%; height: auto; margin:0 auto; frameborder:0;">
                  <!-- <source id="link1" src="https://dl.salamquran.com/ayat/afasy-murattal-192/{{$meta['last']}}.mp3" type="video/mp4"> -->
                    <source id="link1" src="https://cdn.islamic.network/quran/audio/64/ar.alafasy/{{$meta['last_aya_global']}}.mp3" type="video/mp4">
                  Your browser doesn't support HTML5 video tag.
                </video>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="khalafi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="massryLabel">{{$meta['last_aya']}}</h4>
                  </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close2" style="display: none;"><span aria-hidden="true">&times;</span></button>
              <div class="modal-body" style="text-align:right;">
                <video controls id="video2" style="width: 100%; height: auto; margin:0 auto; frameborder:0;">
                  <source id="link2" src="https://cdn.islamic.network/quran/audio/64/ar.mahermuaiqly/{{$meta['last_aya_global']}}.mp3" type="video/mp4">
                  Your browser doesn't support HTML5 video tag.
                </video>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="hussary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="massryLabel">{{$meta['last_aya']}}</h4>
                  </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close3" style="display: none;"><span aria-hidden="true">&times;</span></button>
              <div class="modal-body" style="text-align:right;">
                <video controls id="video3" style="width: 100%; height: auto; margin:0 auto; frameborder:0;">
                  <source id="link3" src="https://cdn.islamic.network/quran/audio/64/ar.husarymujawwad/{{$meta['last_aya_global']}}.mp3" type="video/mp4">
                  Your browser doesn't support HTML5 video tag.
                </video>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="minshawy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="massryLabel">{{$meta['last_aya']}}</h4>
                  </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close4" style="display: none;"><span aria-hidden="true">&times;</span></button>
              <div class="modal-body" style="text-align:right;">
                <video controls id="video4" style="width: 100%; height: auto; margin:0 auto; frameborder:0;">
                  <source id="link4" src="https://cdn.islamic.network/quran/audio/64/ar.abdulsamad/{{$meta['last_aya_global']}}.mp3" type="video/mp4">
                  Your browser doesn't support HTML5 video tag.
                </video>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="tafseer1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">{{$meta['last_aya']}}</h4>
              </div>
              <div class="modal-body" style="text-align:right;font-size: 20px;">
                {{$meta['tafseer1']}}
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-lg" data-dismiss="modal">إغلاق</button>
                <a class="btn btn-success btn-lg" href="{{ route('quran.next',[app()->getLocale(),$meta['last'].$meta['last_page']]) }}" style="display:non"> 
                 <i class="fa fa-thumbs-up" aria-hidden="true"></i> تم الحفظ
                </a>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="tafseer2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel2">تفسير ابن كثير</h4>
              </div>
              <div class="modal-body" style="text-align:right;">
               {{$meta['tafseer2']}}
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="tafseer4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel2">تفسير الطبري</h4>
              </div>
              <div class="modal-body" style="text-align:right;">
                {{$meta['tafseer4']}}
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="tafseer3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel3">تفسير الجلالين</h4>
              </div>
              <div class="modal-body" style="text-align:right;">
                {{$meta['tafseer3']}}
              </div>
            </div>
          </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script type="text/javascript">
            
            $('#mashary').on('shown.bs.modal', function () {
              $('#video1')[0].play();
            })

            $('#khalafi').on('shown.bs.modal', function () {
              $('#video2')[0].play();
            })

            $('#hussary').on('shown.bs.modal', function () {
              $('#video3')[0].play();
            })

            $('#minshawy').on('shown.bs.modal', function () {
              $('#video4')[0].play();
            })

            $('#mashary').on('hidden.bs.modal', function () {
              $('#video1')[0].pause();
            })

            $('#khalafi').on('hidden.bs.modal', function () {
              $('#video2')[0].pause();
            })

            $('#hussary').on('hidden.bs.modal', function () {
              $('#video3')[0].pause();
            })

            $('#minshawy').on('hidden.bs.modal', function () {
              $('#video4')[0].pause();
            })
            

            $("#video1").on("ended", function() {
              $('#close1').click();
              $("#tafseer1").modal('show');
            });

            $("#video2").on("ended", function() {
              $('#close2').click();
            });

            $("#video3").on("ended", function() {
              $('#close3').click();
            });

            $("#video4").on("ended", function() {
              $('#close4').click();
            });
        </script>
</x-app-layout>