<x-app-layout>
    <!-- Top Header-Profile -->

<div class="container">
    <div class="row">
        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="ui-block">
                <div class="top-header">
                    <div class="top-header-thumb">

                    </div>
                    <div class="profile-section">
                        
                        <x-profile_links  :act="7"/>

                        <div class="control-block-button">
                            <a href="#" class="btn btn-control bg-blue">
                                <svg class="olymp-happy-face-icon"><use xlink:href="#olymp-happy-face-icon"></use></svg>
                            </a>

                            <a href="#" class="btn btn-control bg-purple">
                                <svg class="olymp-chat---messages-icon"><use xlink:href="#olymp-chat---messages-icon"></use></svg>
                            </a>

                            <div class="btn btn-control bg-primary more">
                                <svg class="olymp-settings-icon"><use xlink:href="#olymp-settings-icon"></use></svg>

                                <ul class="more-dropdown more-with-triangle triangle-bottom-right">
                                    <li>
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#update-header-photo">Update Profile Photo</a>
                                    </li>
                                    <li>
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#update-header-photo">Update Header Photo</a>
                                    </li>
                                    <li>
                                        <a href="#">Account Settings</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <x-top_header_profile />

                </div>
            </div>
        </div>
    </div>
</div>

<!-- ... end Top Header-Profile -->



   <div class="container">
	<div class="row">
		<div class="col-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">{{ __('notifications') }}</h6> 
				</div>

				
				<!-- Notification List -->
				
				<ul class="notification-list">
					
					@foreach($all_not as $not)
					<?php 
						$data = json_decode($not->data);
						$thisUser = $not->notifiable_type::find($data->from);
					 ?>
					<li @if(empty($not->read_at)) class="un-read" @endif>
						<div class="author-thumb">
							@if(!empty($thisUser->profile_photo_path))
                            <img loading="lazy" src="{{ asset($thisUser->profile_photo_path) }}" alt="author" width="42" height="42">
                            @else
                            <img loading="lazy" src="{{ asset('img/user_'.$thisUser->gender.'.png') }}" alt="author" width="42" height="42">
                            @endif
						</div>
						<div class="notification-event">
							<a href="#" class="h6 notification-friend">{{__('from')}} : </a> {{ $thisUser->name }} <br><a href="#" class="notification-link">{{ $data->title }}</a>
							<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">{{ \Carbon\Carbon::parse($not->created_at)->diffForHumans() }}</time></span>
						<span>
							{{ $data->desc }} .
						</span>
						</div>
						
						<span class="notification-icon">
							<svg class="olymp-comments-post-icon"><use xlink:href="#olymp-comments-post-icon"></use></svg>
						</span>
						
					</li>
				
					@endforeach

					
				</ul>
				
				<!-- ... end Notification List -->

			</div>

			

			<!-- Pagination -->
			
				
						{{ $all_not->links() }}
					
			
			<!-- ... end Pagination -->

		</div>

		
	</div>
</div>

</x-app-layout>

