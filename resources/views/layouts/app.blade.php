<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('fav.ico/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('fav.ico/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('fav.ico/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('fav.ico/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('fav.ico/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('fav.ico/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('fav.ico/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('fav.ico/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('fav.ico/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('fav.ico/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('fav.ico/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('fav.ico/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('fav.ico/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('fav.ico/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('fav.ico/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">
    
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="noindex" />
    <meta name="keywords" content="{{ __('keywords') }}">
    <meta name="description" content="{{ __('complete_social_network') }}">
    <!-- Theme Font -->
    <meta property="og:title" content="{{ __('triangle_social_network') }}">
    <meta property="og:description" content="{{ __('complete_social_network') }}">
    <meta property="og:image" content="https://selaa.social/public/img/default/icon-flv.png">
    <meta property="og:url" content="https://selaa.social/public/ar/webfamily">
    <meta name="twitter:card" content="https://selaa.social/public/img/default/icon-flv.png">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/theme-font.min.css') }}" as="style">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <!-- Styles tailwind -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('Bootstrap/dist/css/bootstrap.css') }}">
    <!-- Main Styles CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    @if(app()->getlocale() == 'ar' || app()->getlocale() == 'cn')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/rtl.css') }}">
    @endif
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style_'.app()->getlocale().'.css') }}">
    
    @stack('styles')
    
    @livewireStyles
    
     <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</head>

<body class="page-has-left-panels page-has-right-panels">

<x-preloader ></x-preloader>

<x-nav_bar ></x-nav_bar>

<livewire:chat-bar />

<x-top_bar ></x-top_bar>

<div class="header-spacer header-spacer-small"></div>
    
                {{ $slot }}

<a class="back-to-top" href="#">
    <svg class="back-icon" width="14" height="18"><use xlink:href="#olymp-back-to-top"></use></svg>
</a>

<!-- JS Scripts -->
<script src="{{ asset('js/jQuery/jquery-3.6.0.min.js') }}"></script>
<!-- <script src="{{ asset('js/jQuery/jquery-3.5.1.min.js') }}"></script> -->
<script src="{{ asset('js/libs/jquery.mousewheel.min.js') }}"></script>
<script src="{{ asset('js/libs/perfect-scrollbar.min.js') }}"></script>
<script src="{{ asset('js/libs/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ asset('js/libs/material.min.js') }}"></script>
<script src="{{ asset('js/libs/selectize.min.js') }}"></script>
<script src="{{ asset('js/libs/swiper.jquery.min.js') }}"></script>
<script src="{{ asset('js/libs/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('js/libs/ajax-pagination.min.js') }}"></script>
<script src="{{ asset('js/libs/jquery.magnific-popup.min.js') }}"></script>

@stack('scripts2')

<script src="{{ asset('js/main.js') }}"></script>
<script src="{{ asset('js/libs-init/libs-init.js') }}"></script>

<script src="{{ asset('Bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>

<!-- SVG icons loader -->
<script src="{{ asset('js/svg-loader.js') }}"></script>
<!-- /SVG icons loader -->
<!-- <script src="{{ asset('js/jQuery/jquery.custom.min.js') }}"></script> -->

        @stack('modals')

        @stack('scripts')

        @livewireScripts
</body>
</html>

