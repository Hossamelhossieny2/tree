<x-app-layout>
     <!-- Top Header-Profile -->

<div class="container">
    <div class="row">
        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="ui-block">
                <div class="top-header">
                    <div class="top-header-thumb">

                    </div>
                    <div class="profile-section">
                        
                        <x-profile_links  :act="1"/>

                        <div class="control-block-button">
                            <a href="#" class="btn btn-control bg-blue">
                                <svg class="olymp-happy-face-icon"><use xlink:href="#olymp-happy-face-icon"></use></svg>
                            </a>

                            <a href="#" class="btn btn-control bg-purple">
                                <svg class="olymp-chat---messages-icon"><use xlink:href="#olymp-chat---messages-icon"></use></svg>
                            </a>

                            <div class="btn btn-control bg-primary more">
                                <svg class="olymp-settings-icon"><use xlink:href="#olymp-settings-icon"></use></svg>

                                <ul class="more-dropdown more-with-triangle triangle-bottom-right">
                                    <li>
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#update-header-photo">Update Profile Photo</a>
                                    </li>
                                    <li>
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#update-header-photo">Update Header Photo</a>
                                    </li>
                                    <li>
                                        <a href="#">Account Settings</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <x-top_header_profile />

                </div>
            </div>
        </div>
    </div>
</div>

<!-- ... end Top Header-Profile -->

    @foreach ($errors->all() as $error)
        <li style="list-style: none;"><div class="alert alert-danger">{{ $error }}</div></li>
    @endforeach

    @if(Session::has('success'))
        <div class="alert alert-success h3 text-center">
            {{ Session::get('success') }}
        </div>
    @endif


    <div class="container">
            <div class="row">
                <div class="col col-xl-9 order-xl-6 col-lg-12 order-lg-1 col-sm-12 col-12">

                     <div class="ui-block">
                <div class="ui-block-title bg-blue">
                    <h6 class="title c-white">{{ __('create_new_share') }}</h6>
                </div>
                <div class="ui-block-content">

                    <form method="post" action="{{ route('add_share_from_user',app()->getLocale()) }}" >
                        @csrf
                        <div class="row">
                            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                               
                                <div class="form-group label-floating">
                                    <label class="control-label">{{ __('share_title') }}</label>
                                    <input class="form-control" type="text" name="piece_name" placeholder="" required="" />
                                </div>

                            </div>

                            

                            <div class="col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                <div class="form-group label-floating is-select">
                                    <label class="control-label">{{ __('share_type') }}</label>
                                    <select name="share_type" onchange="showDiv(this)" class="form-select" required="">
                                        <option value="">{{ __('select_type') }}</option>
                                        @foreach($share_types as $type)
                                        <option value="{{ $type->type_id }}">{{ $type->title.': '.$type->desc }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">{{ __('share_start_date') }}</label>
                                        <input type="date" min="{{ date('Y-m-d') }}" name="start_date" class="form-control" />
                                </div>
                            </div>
                            <div class="col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                <div class="form-group label-floating is-select">
                                    <label class="control-label">{{ __('post_privacy') }}</label>
                                    <select name="post_privacy" class="form-select">
                                        <option value="public">{{ __('post_public') }}</option>
                                        <option value="private">{{ __('post_privat') }}</option>
                                    </select>
                                </div>
                            </div>


                            <div class="col col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                               
                                <div class="form-group label-floating">
                                    <label class="control-label">{{ __('share_period') }}</label>
                                    <input class="form-control" type="number" name="share_period" placeholder="" required="" />
                                </div>
                                
                            </div>

                            <div class="col col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                                <div class="form-group label-floating is-select">
                                    <label class="control-label">{{ __('share_unit') }}</label>
                                    <select name="share_unit" class="form-select" required="">
                                        <option value="">{{ __('select_unit') }}</option>
                                        <option value="Day">{{ __('Day') }}</option>
                                        <option value="Week">{{ __('Week') }}</option>
                                        <option value="Month">{{ __('Month') }}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                               
                                <div class="form-group label-floating">
                                    <label class="control-label">{{ __('share_count') }}</label>
                                    <input class="form-control" type="number" name="share_count" placeholder="" required="" />
                                </div>

                            </div>

                            <div class="col col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                               
                                <div class="form-group label-floating">
                                    <label class="control-label">{{ __('total_price') }}</label>
                                    <input class="form-control" type="number" name="share_total" placeholder="" required="" />
                                </div>

                            </div>

                            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <button type="submit" class="btn btn-blue btn-lg full-width">{{ __('publish') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

                    <div id="newsfeed-items-grid">

                        
                        @foreach($my_shares as $share)


                        <?php $nam = 'piece_name_'.app()->getLocale();$users_for_this_share[$share['share']['id']] = 0;?>

                        
                        <div class="ui-block"  style="background-image: url({{ asset('img/pattern/p6.png')}});border:1px dotted #000;">

                            <!-- Post -->

                            <article class="hentry post video">

                                <div class="post__author author vcard inline-items">
                                    @if(auth()->user()->profile_photo_path)
                                    <img loading="lazy" src="{{ asset(auth()->user()->profile_photo_path) }}" alt="author" width="42" height="42">
                                    @else
                                    <img loading="lazy" src="{{ asset('img\default\user_'.auth()->user()->gender.'.png') }}" alt="author" width="42" height="42" style="background-color: gray;">
                                    @endif

                                    <div class="author-date">
                                        <a class="h6 post__author-name fn" href="#">{{ auth()->user()->name.' '. __('you_offer') }} </a>
                                        <a href="#">{{ __('this_share') }}</a>
                                        <div class="post__date">
                                            <time class="published" datetime="{{ $share['share']['created_at'] }}">
                                                {{ $share['share']['created_at'] }}
                                            </time>
                                        </div>
                                    </div>

                                    

                                </div>

                                <div class="post-video">

                                    <div class="video-thumb">
                                        <img loading="lazy" src="{{ asset($share['share']['desc']['image']) }}" alt="photo" width="205" height="194">
                                        
                                    </div>

                                    <div class="video-content">
                                        <a href="{{ route('user_shares',[app()->getLocale(),$share['share']['desc']['type_id'],$share['share']['desc']['id']]) }}" class="h4 title" style="color:{{ $share['share']['desc']['color'] }}">{{ $share['share']['desc']['title'] }}</a>

                                        <p style="font-size:16px">{{ $share['share']['piece_name'] }}</p>
                                        <a class="btn btn-sm btn-bg-secondary">{{ __('start_time') }} : {{$share['share']['start_pay_time']}}</a>

                                    </div>
                                </div>
                                <h4>
                                    {{ $share['share']['desc']['desc'] }}
                                 <br>
                                 {{ $share['share']['user_share'][0]['share_desc'] }}
                                </h4>


                                <div class="post-additional-info inline-items">
                                    <div class="container">
                                    <div class="row">
                                        <?php $total =0;$a[$share['share']['id']]=0;$month_sum[$share['share']['id']]=0;?>
                                        
                                        @foreach($share['user_piece'] as $unit=>$pays)
                                        <div class="col-12 text-center text-white h4" style="background-color:{{ $share['share']['desc']['color'] }}">
                                            {{ $unit }}<p class="btn btn-sm">{{$share['share']['desc']['share_pay_time']}}</p>
                                        </div>
                                        <?php $y=1;?>
                                            @foreach($pays as $pay)


@if(!isset($pay['user']['id']))
<div class="col-2 btn m-2 " style="background-color:lightgrey;">
    <img class="rounded-full border-2" src="{{ asset('img/default/user.png') }}"  style="width: 40px;height: 40px;background-color:lightgrey;" title="empty user" />
        <br>
        <br>
         {{ $share['share']['user_share'][0][$nam] }} ({{ arabic_w2e($y) }})<br>
        <br>
    <div>
        <a class="btn btn-sm" style="background-color:grey;">{{ __('empty') }}</a>
    </div>
</div>

@else
<?php 
$users_for_this_share[$share['share']['id']] += 1;
$a[$share['share']['id']] +=1;
if($pay['paid']['pay_status'] == '1' && $pay['paid']['confirm'] == '1'){
            $month_sum[$share['share']['id']] = $month_sum[$share['share']['id']] + $pay['paid']['amount'];
            };
?>

<div class="col-2 btn btn-blue m-2">
    <img class="rounded-full border-2" src="{{ asset(($pay['user']['profile_photo_path']) ? $pay['user']['profile_photo_path'] : 'img/default/user_'.$pay['user']['gender'].'.png') }}"  style="width: 40px;height: 40px;background-color:#fff" title="{{$pay['user']['name']}}"/><br>
        <a>{{$pay['user']['name']}}</a><br>
        
        
        {{ $share['share']['user_share'][0][$nam] }} ({{ arabic_w2e($y) }})  - {{ ($pay['paid']['amount'] > 0) ? arabic_w2e(number_format(ceil($pay['paid']['amount']))) : arabic_w2e(ceil($pay['paid']['amount']))}}<br>
        {{$pay['paid']['pay_time']}}<br>

        
    <div id="part{{$pay['paid']['id']}}">
        @if($pay['paid']['share_type'] != 3 && $pay['paid']['pay_status'] == 0)
        <a class="btn btn-sm btn-danger">{{__('not_paid')}}</a>
        @elseif($pay['paid']['share_type'] == 3 && $pay['paid']['pay_status'] == 0)
        <a class="btn btn-sm btn-danger">{{__('not_read')}}</a>
        @elseif($pay['paid']['pay_status'] == 1 && $pay['paid']['confirm'] == 0)
            @if($share['share']['is_started'] != 0)
            <a onclick="confirmPayment({{$pay['paid']['id']}})" class="btn btn-sm btn-purple">{{__('ask_confirm')}}</a>
            @else
            <a class="btn btn-sm btn-purple">{{__('ask_confirm_but')}}</a>
            @endif
        @elseif($pay['paid']['share_type'] == 3 && $pay['paid']['pay_status'] == 1 && $pay['paid']['confirm'] == 1)
        <a class="btn btn-sm btn-success">{{__('read')}}</a>
       @elseif($pay['paid']['share_type'] != 3 && $pay['paid']['pay_status'] == 1 && $pay['paid']['confirm'] == 1)
        <a class="btn btn-sm btn-success">{{__('paid')}}</a>
        @endif
    </div>
    
</div>
@endif
<?php $y+=1;?>

                                            @endforeach

                                            
                                            @if($share['share']['is_started'] != 0)
        <hr>
        <div class="btn btn-lg" style="background-color:black">{{ __('collect_amount') }} ( {{ $unit }} )   = {{arabic_w2e($month_sum[$share['share']['id']])}}</div>
        <?php $total = $total + $month_sum[$share['share']['id']];?>
        <?php $month_sum[$share['share']['id']] = 0; ?>
        @endif
                                        @endforeach

                                       

        @if($share['share']['is_started'] != 0)
         <div class="btn btn-lg" style="background-color:black;color: #65BAFF">{{ __('total_collected') }}   = {{arabic_w2e($total)}}</div>
         @endif

                                    @if(auth()->id() == $share['share']['user']['id'])
                                        @if($share['share']['is_started'] == 0)
                                            @if(intval($users_for_this_share[$share['share']['id']]/$share['share']['period']) != $share['share']['member_count'])
                                            <a class="btn btn-sm bg-primary">{{ __('cant_start') }}</a>
                                            @else
                                                <a href="{{ route('start_this_share',[app()->getLocale(),$share['share']['id']]) }}" class="btn btn-sm bg-success">{{ __('start_now') }}</a>
                                            @endif
                                            <a href="{{ route('remove_this_share',[app()->getLocale(),$share['share']['id']]) }}" class="btn btn-sm bg-danger">{{ __('remove_this') }}</a>
                                        @endif
                                    @endif
                                    </div>
                                    
                                    </div>


                                </div>


                            </article>

                            </div>
                        
                        @endforeach
                        
                    </div>
                    <a id="load-more-button" href="#" class="btn btn-control btn-more" data-load-link="items-to-load.html" data-container="newsfeed-items-grid">
                        <svg class="olymp-three-dots-icon">
                            <use xlink:href="#olymp-three-dots-icon"></use>
                        </svg>
                    </a>

                </div>

                <div class="col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="ui-block">
                        <div class="ui-block-title">
                            <h6 class="title">{{ __('my_all_shares') }}</h6>
                            <a href="#" class="more">
                                <svg class="olymp-three-dots-icon">
                                    <use xlink:href="#olymp-three-dots-icon"></use>
                                </svg>
                            </a>
                        </div>
                        <div class="ui-block-content">
                            <!-- W-Pool -->

                            <ul class="widget w-pool">
                                <li>
                                    <p>{{ __('all_shares') }} .. {{ __('big_family') }} </p>
                                </li>
                                @foreach($my_shares as $share)
                                <li>
                                    <div class="skills-item">
                                        
                                        <div class="skills-item-info">
                                            <span class="skills-item-title">
                                                <span class="radio">
                                                    <label>
                                                        {{ $share['share']['desc']['title'] }}: {{ $share['share']['piece_name'] }}
                                                    </label>
                                                </span>
                                            </span>
                                            <span class="skills-item-count">
                                                <span class="count-animate" data-speed="1000" data-refresh-interval="50" data-to="62" data-from="0"></span>
                                                <span class="units">{{ intval($a[$share['share']['id']]/$share['share']['member_count']) }}%</span>
                                            </span>
                                        </div>
                                        <div class="skills-item-meter">
                                            <span class="skills-item-meter-active " style="width: {{ intval($a[$share['share']['id']]/$share['share']['period']) }}%;background-color:{{ $share['share']['desc']['color'] }}"></span>
                                        </div>

                                        <div class="counter-friends">{{ $a[$share['share']['id']]/$share['share']['period'] }} / {{$share['share']['member_count']}} {{ __('member_join') }}</div>

                                        <ul class="friends-harmonic">
                                             @foreach($share['share']['user_share'] as $part)
                                             @if(isset($part['user']))
                                            <li>
                                                <a href="#">
                                                    @if(isset($part['user']) && !empty($part['user']['profile_photo_path']))
                                                    <img loading="lazy" src="{{ asset($part['user']['profile_photo_path']) }}" alt="author" width="28" height="28">
                                                    @endif
                                                </a>
                                            </li>
                                            @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                </li>
                                <hr>
                                @endforeach
                            </ul>

                            <!-- .. end W-Pool -->
                            
                        </div>
                    </div>

                   
                </div>
               
            </div>
        </div>

        <script type="text/javascript">

            function confirmPayment(partId){
                if (confirm('Are You sure to Select This Part? it will be payed Now')) {
                // alert(partId);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                    $.ajax({
                       type:'POST',
                       url:"{{ route('user_shares_confirm',app()->getLocale()) }}",
                       data:{part:partId},
                       success:function(data){
                          //console.log(data);
                          $('#part'+partId).html(data);
                       }
              
                });
            }
        }
        </script>

</x-app-layout>
