<x-app-layout>
<div style="background-image: url({{ asset('img/pattern/p1.png')}})">

 @if (session()->has('message'))
    <div class="alert alert-success text-center">
        {{ session('message') }}
    </div>
@endif


<!-- Main Header Groups -->

<div class="main-header">
    <div class="content-bg-wrap bg-group" style="background-image: asset('img/bg-group.png');"></div>
    <div class="container">
        <div class="row">
            <div class="col col-lg-8 m-auto col-md-8 col-sm-12 col-12">
                <div class="main-header-content">
                    <h1>{{ __('all_shares') }}</h1>
                    <p> @foreach($shares_titles as $tit)
                        - {{$tit->title}} - 
                        @endforeach
                    </p>
                </div>
            </div>
        </div>
    </div>

    <img loading="lazy" class="img-bottom" src="{{ asset('img/group-bottom.webp') }}" alt="friends" width="1087" height="148">
</div>

<!-- ... end Main Header Groups -->


<div class="container">
    <div class="row">
        
        <div class="col col-xl-12 col-12">
            <div class="ui-block">
                <div class="ui-block-content">
                    
                    <div class="row">
                        <div class="col col-lg-3 col-md-6 col-sm-12 col-12">
                            <a href="{{ route('user_shares',[app()->getLocale(),0,0]) }}" class="btn btn-purple btn-lg btn-light-bg text-white"  style="width: 100%">{{ __('all_types') }}</a>
                        </div>
                        @foreach($shares_titles as $tit)
                        <div class="col col-lg-3 col-md-3 col-sm-12 col-12">
                            <a href="{{ route('user_shares',[app()->getLocale(),$tit->type_id,0]) }}" class="btn btn-lg text-white" style="background-color: {{$tit->color}};width: 100%">{{$tit->title}}</a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="container">
    <div class="row">
        <div class="col-12">
    <div class="accordion bg-grey" id="accordionE">
  
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingT">
      <button class="accordion-button collapsed text-white" type="button" data-bs-toggle="collapse" data-bs-target="#collapseT" aria-expanded="false" aria-controls="collapseT"  style="background-image: url({{ asset('img/default/clickhere.png') }});background-repeat: no-repeat;">
        {{__('see_help_vid')}}
      </button>
      
    </h2>

    <div id="collapseT" class="accordion-collapse collapse" aria-labelledby="headingT" data-bs-parent="#accordionE">
      <div class="accordion-body">
        <ul class="widget w-last-video">
            @foreach($VidHelp as $vid)
                <li>
                   <h6 style="color:#39A9FF">{{ $vid->title }}</h6>
                    <a href="{{asset($vid->video)}}" class="play-video play-video--small">
                        <svg class="olymp-play-icon">
                            <use xlink:href="#olymp-play-icon"></use>
                        </svg>
                    </a>
                    <img loading="lazy" src="{{ asset($vid->image) }}" alt="video" width="272" height="181">
                    <div class="video-content">
                        <div class="title">{{ $vid->title }}</div>
                        <time class="published" datetime="2017-03-24T18:18">{{ $vid->lenght }}</time>
                    </div>
                    <div class="overlay"></div>
                </li>
                <li> </li>
            @endforeach
            </div>
      </div>
    </div>
  </div>
</div>

    </div>
    </div>

    <hr>

<div class="container">
    <div class="row">


        @if(isset($one_share))
        <div class="col-12">
            <div class="ui-block"  style="background-image: url({{ asset('img/pattern/p2.png')}});border-radius:25px;border:1px dotted #000;">
                    <article class="hentry post video">
                    
                        <div class="post__author author vcard inline-items">
                                @if($one_share['user']['profile_photo_path'])
                                <img loading="lazy" src="{{ asset($one_share['user']['profile_photo_path']) }}" alt="author" width="42" height="42">
                                @else
                                <img loading="lazy" src="{{ asset('img\default\user_'.$one_share['user']['gender'].'.png') }}" alt="author" width="42" height="42" style="background-color: gray;">
                                @endif
                    
                            <div class="author-date">
                                <a class="h6 post__author-name fn" href="#">{{ $one_share['user']['name'] }}</a> {{__('you_offer')}} <a href="#">{{ __('this_share') }}</a>
                                <div class="post__date">
                                    <time class="published" datetime="2004-07-24T18:18">
                                        {{ $one_share->created_at }}
                                    </time>
                                </div>
                            </div>
                        </div>
                        <a class="btn btn-sm bg-blue">{{ __('start_time') }} : {{$one_share->start_pay_time}}</a>
                        <p><a href="#">{{ $one_share['desc']['title'] }}</a>: {{ $one_share['desc']['desc'] }}</p>
                    
                        <div class="post-video">
                            <div class="video-thumb">
                                <img loading="lazy" src="{{ asset($one_share['desc']['image'])}}" alt="photo" width="197" height="194">
                                </a>
                            </div>
                    
                            <div class="video-content">
                                <a href="#" class="h4 title">
                                    {{$one_share['user_share'][0]['share_desc']}}
                                </a>
                                <p>{{ $one_share->piece_name }}</p>
                            </div>
                        </div>
                    </article>
                </div>
            </div>

@foreach($one_share['user_share'] as $part)

        <div class="col-3 text-center">
            <div class="ui-block" style="background-color:lightgrey;">
                
                <!-- W-Create-Fav-Page -->
                @if(!isset($part['user']))
                <div class="widget w-create-fav-page" style="background-color:{{ $one_share['desc']['color'] }};text-decoration: double underline; font-weight: 400;margin-bottom: 0;" id="part{{$part->id}}">
                    <div class="icons-block text-white h3" style="margin-bottom: 0;">
                        <?php $name = 'piece_name_'.app()->getLocale();?>

                        {{ ($one_share->period>1) ? $part->$name : __('part') }} ({{ (app()->getLocale() == 'ar') ? arabic_w2e($part->piece_id) : $part->piece_id }})
                        <br>
                        <a class="btn btn-lg">
                            @if($part->share_type  == 2)
                            {{$part->share_pay_time}}
                            @else
                            {{$one_share->start_pay_time}}
                            @endif
                        </a>
                        <br><span class="btn btn-lg">{{$part->share_piece}}</span>
                        <div><img src="{{asset('img/default/notselected.png')}}"></div>
                    </div>

                    <div class="content">
                        <h3 class="text-white">{{ $part->share_title }}</h3>
                        <span class="title">{{ $part->share_desc }}</span>
                    </div>

                    <br>
                        <button id="{{$part->id}}" onClick="getThisShare({{$part->id}})" class="btn btn-lg btn-light-bg text-white" title="join me" style="background-color:#000;">
                            {{__('pick_this')}}
                        </button>
                </div>
                @else

                <div class="widget w-create-fav-page h3" style="background-color:lightgrey;text-decoration: double underline; font-weight: 400;margin-bottom: 0">
                    <div class="icons-block text-gray h3" style="margin-bottom: 16px;">
                        <?php $name = 'piece_name_'.app()->getLocale();?>
                        {{ ($one_share->period>1) ? $part->$name : __('part') }} ({{ (app()->getLocale() == 'ar') ? arabic_w2e($part->piece_id) : $part->piece_id }})
                        <br><a class="btn btn-lg">{{$part->share_pay_time}}</a>
                    </div>
                    <div><img src="{{asset('img/default/selected.png')}}"></div>
                   
                        @if($part['user']['profile_photo_path'])
                        <img loading="lazy" src="{{ asset($part['user']['profile_photo_path']) }}" alt="author" width="92" height="92">
                        @else
                        <img loading="lazy" src="{{ asset('img\default\user_'.$part['user']['gender'].'.png') }}" alt="author" width="92" height="92" style="background-color: gray;">
                        @endif
                    
                    <br/>

                    <button class="btn btn-md-2 btn-border-think btn-transparent c-grey" style="font-weight: 400;font-size: 20px;">{{ $part['user']['name'] }}<div class="ripple-container"></div>
                    </button>

                </div>
                @endif
                </div>

        </div>
        @endforeach

        @endif

@if(count($shares)>0)
@foreach($shares as $share)

        <div class="col col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12">
            <div class="ui-block"  style="background-image: url({{ asset('img/pattern/p2.png')}});border:1px dotted #000;">

                
                <!-- Friend Item -->
                
                <div class="friend-item">
                    <div class="friend-header-thumb">
                        <img loading="lazy" src="{{ asset( $share['desc']['image'] ) }}" alt="friend" width="318" height="122">
                    </div>
                
                    <div class="friend-item-content">
                
                        <div class="more">
                            <svg class="olymp-three-dots-icon"><use xlink:href="#olymp-three-dots-icon"></use></svg>
                            <ul class="more-dropdown">
                                <li>
                                    <a href="#">{{ $share['user']['name'] }}</a>
                                </li>
                                <li>
                                    <a href="#">{{ ($share['user']['phone_number']) ? $share['user']['phone_number'] : __('mobile_number_null') }}</a>
                                </li>
                            </ul>
                        </div>
                        <div class="friend-avatar">
                            <div class="author-thumb">
                                @if($share['user']['profile_photo_path']))
                                <img loading="lazy" src="{{ asset($share['user']['profile_photo_path']) }}" alt="author" width="92" height="92">
                                @else
                                <img loading="lazy" src="{{ asset('img\default\user_'.$share['user']['gender'].'.png') }}" alt="author" width="92" height="92" style="background-color: gray;">
                                @endif
                            </div>
                            <div class="author-content">
                                <a href="#" class="h5 author-name">{{ $share['desc']['title'] }}</a>
                                <div class="country">{{ $share->piece_name }}</div>
                            </div>
                        </div>
                
                        <div class="swiper-container swiper-swiper-unique-id-0 initialized swiper-container-horizontal" data-slide="fade" id="swiper-unique-id-0">

                            <div class="swiper-wrapper" style="width: 1076px; transform: translate3d(-269px, 0px, 0px); transition-duration: 0ms;">
                                <div class="swiper-slide swiper-slide-active" data-swiper-slide-index="0" style="width: 269px;">
                                    <div class="friend-count" data-swiper-parallax="-500" style="transform: translate3d(0px, 0px, 0px); transition-duration: 0ms;">
                                        <a href="#" class="friend-count-item">
                                            <div class="h6">{{arabic_w2e($share->period)}}</div>
                                            <div class="title">{{__($share->unit)}}</div>
                                        </a>
                                        <a href="#" class="friend-count-item">
                                            <div class="h6">{{ arabic_w2e(number_format($share->member_count)) }}</div>
                                            <?php $name = 'piece_name_'.app()->getLocale();?>
                                            <div class="title">{{ __('members') }}</div>
                                        </a>
                                        <a href="#" class="friend-count-item">
                                            <div class="h6">{{ arabic_w2e(number_format($share->total)) }}</div>
                                            <div class="title">{{ __('total') }}</div>
                                        </a>
                                    </div>
                                    <div class="friend-since" data-swiper-parallax="-100" style="transform: translate3d(100px, 0px, 0px); transition-duration: 0ms;">
                                        <span>{{ __('start_time') }}:</span>
                                        <div class="h6">{{ $share->start_pay_time }}</div>
                                    </div>
                                    <div class="control-block-button" data-swiper-parallax="-100" style="transform: translate3d(0px, 0px, 0px); transition-duration: 0ms;">
                                        <a  class="btn btn-control bg-blue">
                                            {{ arabic_w2e(number_format($share->piece)) }}
                                            
                                        </a>

                                    </div>
                                    <br>
                                    <a href="{{ route('user_shares',[app()->getLocale(),$section,$share->id]) }}" class="btn btn-lg btn-primary">{{ __('more') }}</a>
                                </div>
                            </div>
                
                        </div>
                    </div>
                </div>
                
                <!-- ... end Friend Item -->
            </div>
        </div>
@endforeach

@endif
    </div>
</div>


<script type="text/javascript">

    function getThisShare(partId){
        if (confirm('Are You sure to Select This Part? it will be disabled for others')) {
        // alert(partId);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
            $.ajax({
               type:'POST',
               url:"{{ route('user_shares_select',app()->getLocale()) }}",
               data:{part:partId},
               success:function(data){
                  //console.log(data);
                  $('#part'+partId).html(data);
                  $('#part'+partId).css('background-color','lightgrey');
               }
      
        });
    }
}
</script>

@if(!isset($needHelp))
@push('modals')
<!-- Faqs Popup -->

<div class="modal fade" id="faqs-popup" tabindex="-1" role="dialog" aria-labelledby="faqs-popup" aria-hidden="true">
    <div class="modal-dialog window-popup faqs-popup" role="document">
        <div class="modal-content">
            <a href="#" class="close icon-close" data-bs-dismiss="modal" aria-label="Close">
                <svg class="olymp-close-icon">
                    <use xlink:href="#olymp-close-icon"></use>
                </svg>
            </a>

            <div class="modal-header">
                <h4 class="title" id="faqs-title">{{__('welcome_in').__('all_shares')}}</h4>
            </div>

            <div class="modal-body">

                <div class="accordion" id="accordionExample">
                    
                    @foreach($FaqsHelp as $faq)
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="heading{{$faq->id}}">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{$faq->id}}" aria-expanded="true" aria-controls="collapse{{$faq->id}}">
                                <span class="c-green">- </span> {{$faq->title}}
                            </button>
                        </h2>
                        <div id="collapse{{$faq->id}}" class="accordion-collapse collapse @if($faq->id == $FaqsHelp[0]->id) show @endif" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                {{$faq->desc}}
                            </div>
                        </div>
                    </div>
                    @endforeach
                    
                </div>
                <hr>
                <a data-bs-dismiss="modal" class="close btn btn-blue btn-lg">{{__('dismiss')}}</a>
                <a data-bs-dismiss="modal" class="close btn btn-lg btn-primary" onclick="removeHelp({{$faq->topic_id}})">{{__('dont_show')}}</a>
            </div>
        </div>
    </div>
</div>

<!-- ... end Faqs Popup -->
@endpush

@push('scripts')
<script type="text/javascript">
    $( document ).ready(function() {
        
        $('#faqs-popup').modal('show');
    });        

    function removeHelp(argument) {
  
  var topic = argument;

    $.ajax({
            url: "{{ route('remove_help',app()->getLocale()) }}",
            type:"POST",
            data:{
              Topic:topic,
              _token: $('meta[name="csrf-token"]').attr('content')
            },
            success:function(response){
              console.log(response);
              if(response) {
                $('.success').text(response.success);
                $("#helpCloseButton").click();
              }
            },
       });
    }
</script>
@endpush

@endif

</x-app-layout>