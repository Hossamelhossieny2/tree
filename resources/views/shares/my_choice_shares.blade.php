<x-app-layout>
     <!-- Top Header-Profile -->

<div class="container">
    <div class="row">
        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="ui-block">
                <div class="top-header">
                    <div class="top-header-thumb">

                    </div>
                    <div class="profile-section">
                        
                        <x-profile_links  :act="2"/>

                        <div class="control-block-button">
                            <a href="#" class="btn btn-control bg-blue">
                                <svg class="olymp-happy-face-icon"><use xlink:href="#olymp-happy-face-icon"></use></svg>
                            </a>

                            <a href="#" class="btn btn-control bg-purple">
                                <svg class="olymp-chat---messages-icon"><use xlink:href="#olymp-chat---messages-icon"></use></svg>
                            </a>

                            <div class="btn btn-control bg-primary more">
                                <svg class="olymp-settings-icon"><use xlink:href="#olymp-settings-icon"></use></svg>

                                <ul class="more-dropdown more-with-triangle triangle-bottom-right">
                                    <li>
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#update-header-photo">Update Profile Photo</a>
                                    </li>
                                    <li>
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#update-header-photo">Update Header Photo</a>
                                    </li>
                                    <li>
                                        <a href="#">Account Settings</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <x-top_header_profile />

                </div>
            </div>
        </div>
    </div>
</div>

<!-- ... end Top Header-Profile -->

    @foreach ($errors->all() as $error)
        <li style="list-style: none;"><div class="alert alert-danger">{{ $error }}</div></li>
    @endforeach

    @if(Session::has('success'))
        <div class="alert alert-success h3 text-center">
            {{ Session::get('success') }}
        </div>
    @endif

    <div class="container">
                    <div id="newsfeed-items-grid">

                        @foreach($shares as $share)
                        <?php $a =0;?>
                        <div class="ui-block"  style="background-image: url({{ asset('img/pattern/p6.png')}});border:1px dotted #000;">

                            <!-- Post -->

                            <article class="hentry post video">

                                <div class="post__author author vcard inline-items">
                                    @if($share['share_user_id']['profile_photo_path'])
                                    <img loading="lazy" src="{{ asset($share['share_user_id']['profile_photo_path']) }}" alt="author" width="42" height="42">
                                    @else
                                    <img loading="lazy" src="{{ asset('img\default\user_'.$share['share_user_id']['gender'].'.png') }}" alt="author" width="42" height="42" style="background-color: gray;">
                                    @endif

                                    <div class="author-date">
                                        <a class="h6 post__author-name fn" href="#">{{ $share['share_user_id']['name'].' '. __('you_offer') }} </a>
                                        <a href="#">{{ __('this_share') }}</a>
                                        <div class="post__date">
                                            <time class="published" datetime="2017-03-24T18:18">
                                                {{ $share->created_at }}
                                            </time>
                                        </div>
                                    </div>

                                    

                                </div>

                                <div class="post-video">

                                    <div class="video-thumb">
                                        <img loading="lazy" src="{{ asset($share->share_image) }}" alt="photo" width="205" height="194">
                                        
                                    </div>

                                    <div class="video-content">
                                        <a href="{{ route('user_shares',[app()->getLocale(),$share->type,$share->id]) }}" class="h4 title" style="color:gray;">{{ $share->share_desc }}</a>
                                        <?php $name = 'piece_name_'.app()->getLocale()?>
                                        <a class="btn btn-sm btn-bg-secondary">{{ $share->$name }}</a>
                                        <a class="btn btn-sm btn-bg-secondary">{{ arabic_w2e(number_format($share['share']['piece'])) }}</a>
                                        <a class="btn btn-sm btn-bg-secondary">{{ $share['share']['start_pay_time'] }}</a>
                                       @if($share->share_type != 3)
                                        <a class="btn btn-sm btn-success" style="color:#000;">confirmed payment : {{ arabic_w2e(number_format($share->is_payed)) }}</a>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="post-additional-info inline-items">
                                    <div class="container">
                                    <div class="row">
                                   <?php $cut = $share['share']['period']; $a=0;?>

                                        @foreach($my_shares[$share->share_id] as $one)
                                        <?php $a+=1;?>
                                        <div class="col-2" id="part{{$one->id}}">
                                            <button class="btn" style="background-color:black;">
                                                {{ arabic_w2e(number_format($one->amount)) }} - {{ $share->$name }} ( {{ arabic_w2e($one->piece_id) }} )
                                                <a class="btn btn-sm">{{$one->pay_time}}</a>
                                                @if($one->pay_status == 1 && $one->confirm == 1)
                                                <div><img src="{{asset('img/default/done.png')}}"></div>
                                                @elseif($one->pay_status == 1 && $one->confirm == 0)
                                                <div><a><img src="{{asset('img/default/inprogress.png')}}"></a></div>
                                                @elseif($one->pay_status == 0)
                                                @if($share->type != 3)
                                                <div><a class="btn btn-sm btn-blue" onClick="doneThisShare({{$one->id}})">{{ ($one->share_type == 3) ? 'Read Compleated' : 'Pay Now' }}</a></div>
                                                @endif
                                                @endif
                                                
                                                </button>
                                        </div>
                                           <?php if($a / $cut === intval($a / $cut)){?>
                                            <hr>

                                        <?php }?>
                                        @endforeach
                                    
                                    </div>
                                    </div>


                                </div>


                            </article>

                            </div>
                        @endforeach
                    </div>
                    <a id="load-more-button" href="#" class="btn btn-control btn-more" data-load-link="items-to-load.html" data-container="newsfeed-items-grid">
                        <svg class="olymp-three-dots-icon">
                            <use xlink:href="#olymp-three-dots-icon"></use>
                        </svg>
                    </a>

                </div>

               
            </div>
        </div>
<script type="text/javascript">

    function doneThisShare(partId){
        if (confirm('Are You sure to Select This Part? it will be payed Now')) {
        // alert(partId);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
            $.ajax({
               type:'POST',
               url:"{{ route('user_shares_done',app()->getLocale()) }}",
               data:{part:partId},
               success:function(data){
                  //console.log(data);
                  $('#part'+partId).html(data);
               }
      
        });
    }
}
</script>

</x-app-layout>
