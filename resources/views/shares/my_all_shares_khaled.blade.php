<x-app-layout>
     <!-- Top Header-Profile -->

<div class="container">
    <div class="row">
        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="ui-block">
                <div class="top-header">
                    <div class="top-header-thumb">

                    </div>
                    <div class="profile-section">
                        
                        <x-profile_links  :act="1"/>

                        <div class="control-block-button">
                            <a href="#" class="btn btn-control bg-blue">
                                <svg class="olymp-happy-face-icon"><use xlink:href="#olymp-happy-face-icon"></use></svg>
                            </a>

                            <a href="#" class="btn btn-control bg-purple">
                                <svg class="olymp-chat---messages-icon"><use xlink:href="#olymp-chat---messages-icon"></use></svg>
                            </a>

                            <div class="btn btn-control bg-primary more">
                                <svg class="olymp-settings-icon"><use xlink:href="#olymp-settings-icon"></use></svg>

                                <ul class="more-dropdown more-with-triangle triangle-bottom-right">
                                    <li>
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#update-header-photo">Update Profile Photo</a>
                                    </li>
                                    <li>
                                        <a href="#" data-bs-toggle="modal" data-bs-target="#update-header-photo">Update Header Photo</a>
                                    </li>
                                    <li>
                                        <a href="#">Account Settings</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <x-top_header_profile />

                </div>
            </div>
        </div>
    </div>
</div>

<!-- ... end Top Header-Profile -->

    @foreach ($errors->all() as $error)
        <li style="list-style: none;"><div class="alert alert-danger">{{ $error }}</div></li>
    @endforeach

    @if(Session::has('success'))
        <div class="alert alert-success h3 text-center">
            {{ Session::get('success') }}
        </div>
    @endif


    <div class="container">
            <div class="row">
                <div class="col col-xl-9 order-xl-6 col-lg-12 order-lg-1 col-sm-12 col-12">

                     <div class="ui-block">
                <div class="ui-block-title bg-blue">
                    <h6 class="title c-white">{{ __('create_new_share') }}</h6>
                </div>
                <div class="ui-block-content">

                    <form method="post" action="{{ route('add_share_from_user',app()->getLocale()) }}" >
                        @csrf
                        <div class="row">
                            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                               
                                <div class="form-group label-floating">
                                    <label class="control-label">{{ __('share_title') }}</label>
                                    <input class="form-control" type="text" name="piece_name" placeholder="" required="" />
                                </div>

                            </div>

                            

                            <div class="col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                <div class="form-group label-floating is-select">
                                    <label class="control-label">{{ __('share_type') }}</label>
                                    <select name="share_type" onchange="showDiv(this)" class="form-select" required="">
                                        <option value="">{{ __('select_type') }}</option>
                                        @foreach($share_types as $type)
                                        <option value="{{ $type->type_id }}">{{ $type->title.': '.$type->desc }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">{{ __('share_start_date') }}</label>
                                        <input type="date" min="{{ date('Y-m-d') }}" name="start_date" class="form-control" />
                                </div>
                            </div>
                            <div class="col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                <div class="form-group label-floating is-select">
                                    <label class="control-label">{{ __('post_privacy') }}</label>
                                    <select name="post_privacy" class="form-select">
                                        <option value="public">{{ __('post_public') }}</option>
                                        <!-- <option value="private">{{ __('post_private') }}</option> -->
                                    </select>
                                </div>
                            </div>


                            <div class="col col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                               
                                <div class="form-group label-floating">
                                    <label class="control-label">{{ __('share_period') }}</label>
                                    <input class="form-control" type="number" name="share_period" placeholder="" required="" />
                                </div>
                                
                            </div>

                            <div class="col col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                                <div class="form-group label-floating is-select">
                                    <label class="control-label">{{ __('share_unit') }}</label>
                                    <select name="share_unit" class="form-select" required="">
                                        <option value="">{{ __('select_unit') }}</option>
                                        <option value="Day">{{ __('Day') }}</option>
                                        <option value="Week">{{ __('Week') }}</option>
                                        <option value="Month">{{ __('Month') }}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                               
                                <div class="form-group label-floating">
                                    <label class="control-label">{{ __('share_count') }}</label>
                                    <input class="form-control" type="number" name="share_count" placeholder="" required="" />
                                </div>

                            </div>

                            <div class="col col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                               
                                <div class="form-group label-floating">
                                    <label class="control-label">{{ __('total_price') }}</label>
                                    <input class="form-control" type="number" name="share_total" placeholder="" required="" />
                                </div>

                            </div>

                            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <button type="submit" class="btn btn-blue btn-lg full-width">{{ __('publish') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

                    <div id="newsfeed-items-grid">

                        
                        @foreach($my_shares as $share)


                       
                        <div class="ui-block"  style="background-image: url({{ asset('img/pattern/p6.png')}});border:1px dotted #000;">

                            <!-- Post -->

                            <article class="hentry post video">

                                <div class="post__author author vcard inline-items">
                                    @if($share['share']['user']['profile_photo_path'])
                                    <img loading="lazy" src="{{ asset($share['share']['user']['profile_photo_path']) }}" alt="author" width="42" height="42">
                                    @else
                                    <img loading="lazy" src="{{ asset('img\default\user_'.$share['share']['user']['gender'].'.png') }}" alt="author" width="42" height="42" style="background-color: gray;">
                                    @endif

                                    <div class="author-date">
                                        <a class="h6 post__author-name fn" href="#">{{ $share['share']['user']['name'].' '. __('you_offer') }} </a>
                                        <a href="#">{{ __('this_share') }}</a>
                                        <div class="post__date">
                                            <time class="published" datetime="2017-03-24T18:18">
                                                {{ $share['share']['created_at'] }}
                                            </time>
                                        </div>
                                    </div>

                                    

                                </div>

                                <div class="post-video">

                                    <div class="video-thumb">
                                        <img loading="lazy" src="{{ asset($share['share']['desc']['image']) }}" alt="photo" width="205" height="194">
                                        
                                    </div>

                                    <div class="video-content">
                                        <a href="{{ route('user_shares',[app()->getLocale(),$share['share']['type'],$share['share']['id']]) }}" class="h4 title" style="color:{{ $share['share']['desc']['color'] }}">{{ $share['share']['desc']['title'] }}</a>

                                        <p style="font-size:16px">{{ $share['share']->piece_name }}</p>
                                        <a class="btn btn-sm btn-bg-secondary">{{ __('start_time') }} : {{$share['share']->start_pay_time}}</a>

                                    </div>
                                </div>
                                <h4>
                                    {{ $share['share']['desc']['desc'] }}
                                 
                                </h4>


                                <div class="post-additional-info inline-items">
                                    <div class="container">
                                    <div class="row">
                                        <?php $total =0;?>
                                       
@foreach($share['user_piece'] as $key=>$val)
       
       <div class="col-12 text-center text-white h4" style="background-color:{{ $share['share']['desc']['color'] }}">
         {{ $key }} <p class="btn btn-sm">{{$share['share']->share_pay_time}}</p>
    </div>

    @foreach($val as $one_share)
@dump($one_share['paid'])
        @if($one_share['user'] != "no name")
        
        <div class="col-2 btn m-2 " style="background-color:lightblue;">
            <img class="rounded-full border-2" src="{{ asset(($one_share['user']['profile_photo_path']) ? $one_share['user']['profile_photo_path'] : 'img/default/user_'.$one_share['user']['gender'].'.png') }}"  style="width: 40px;height: 40px;background-color:lightgrey;" title="{{$one_share['user']['name']}}" /><br>
            {{ $one_share['paid']['piece_id'] }}<br>
            {{ $one_share['paid']['pay_time'] }}
            <div>
                <a class="btn btn-sm" style="background-color:grey;">not selected</a>
            </div>
        </div>
        @else
        <div class="col-2 btn m-2 " style="background-color:lightgrey;">
            <img class="rounded-full border-2" src="{{ asset('img/default/user.png') }}"  style="width: 40px;height: 40px;background-color:lightgrey;" title="empty user" /><br>
           empty
            <div>
                <a class="btn btn-sm" style="background-color:grey;">not selected</a>
            </div>
        </div>
        @endif
    @endforeach

@endforeach                                
                                    </div>
                                    
                                    </div>


                                </div>


                            </article>

                            </div>
                        
                        @endforeach
                        
                    </div>
                    <a id="load-more-button" href="#" class="btn btn-control btn-more" data-load-link="items-to-load.html" data-container="newsfeed-items-grid">
                        <svg class="olymp-three-dots-icon">
                            <use xlink:href="#olymp-three-dots-icon"></use>
                        </svg>
                    </a>

                </div>

               side
               
            </div>
        </div>

        <script type="text/javascript">

            function confirmPayment(partId){
                if (confirm('Are You sure to Select This Part? it will be payed Now')) {
                // alert(partId);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                    $.ajax({
                       type:'POST',
                       url:"{{ route('user_shares_confirm',app()->getLocale()) }}",
                       data:{part:partId},
                       success:function(data){
                          //console.log(data);
                          $('#part'+partId).html(data);
                       }
              
                });
            }
        }
        </script>

</x-app-layout>
