<x-app-layout>


<div class="stunning-header bg-primary-opacity">

    <div class="header-spacer--standard"></div>

    <div class="stunning-header-content">
        <h1 class="stunning-header-title">{{__('contact') }}</h1>
        <ul class="breadcrumbs">
            <li class="breadcrumbs-item">
                <a href="#">{{__('home') }}</a>
                <span class="icon breadcrumbs-custom">/</span>
            </li>
            <li class="breadcrumbs-item active">
                <span>{{__('contact') }}</span>
            </li>
        </ul>
    </div>

    <div class="content-bg-wrap stunning-header-bg1"></div>
</div>

<!-- End Stunning header -->

@foreach ($errors->all() as $error)
    <li style="list-style: none;"><div class="alert alert-danger">{{ $error }}</div></li>
@endforeach

@if(Session::has('success'))
    <div class="alert alert-success text-center h4">
      {{ Session::get('success') }}
    </div>
@endif

<section class="mt-0">
    <div class="section">

        <!----------------------------------------------- Map --------------------------------------------------------->

        <div id="map" style="height: 480px"></div>

        <!-- JS library for Map Leaflet -->
        <script src="{{ asset('js/libs/leaflet.min.js') }}"></script>
        <script src="{{ asset('js/libs/MarkerClusterGroup.min.js') }}"></script>
        <!-- ...end JS library for Map Leaflet -->

        <!-- JS-init for Map -->
        <script>

            /* -----------------------
                * Create the map
                * https://leafletjs.com/
            * --------------------- */

            maps = {
                maps: {
                    mapUSA: {
                        config: {
                            id: 'map',
                            map: {
                                center: new L.LatLng(30.148512552984826, 31.540720662709866),
                                zoom: 12,
                                maxZoom: 18,
                                layers: new L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {
                                    maxZoom: 16,
                                    attribution: ''
                                })
                            },
                            icon: {
                                iconSize  : [36, 54],
                                iconAnchor: [22, 94],
                                className : 'icon-map'
                            }
                        },
                        markers: [
                            {
                                coords: [30.148512552984826, 31.540720662709866],
                                icon: '/map-marker.png'
                            }
                        ]
                    }
                },
                init: function () {
                    var _this = this;

                    for (var key in this.maps) {
                        var data = this.maps[key];

                        if (!data.config || !data.markers) {
                            continue;
                        }

                        if (!document.getElementById(data.config.id)) {
                            continue;
                        }

                        var map = new L.map(data.config.id, data.config.map);
                        var cluster = L.markerClusterGroup({
                            iconCreateFunction: function (cluster) {
                                var childCount = cluster.getChildCount();
                                var config = data.config.cluster;
                                return new L.DivIcon({
                                    html: '<div><span>' + childCount + '</span></div>',
                                    className: 'marker-cluster marker-cluster-' + key,
                                    iconSize: new L.Point(config.iconSize[0], config.iconSize[1])
                                });
                            }
                        });
                        data.markers.forEach(function (item) {
                            data.config.icon['iconUrl'] = '{{asset("/img/")}}' + item.icon;
                            var icon = L.icon(data.config.icon);

                            var marker = L.marker(item.coords, {icon: icon});
                            cluster.addLayer(marker);
                        });

                        map.addLayer(cluster);
                        this.disableScroll(jQuery("#" + data.config.id), map);
                    }
                },
                disableScroll: function ($map, map) {
                    map.scrollWheelZoom.disable();

                    $map.bind('mousewheel DOMMouseScroll', function (event) {
                        event.stopPropagation();
                        if (event.ctrlKey == true) {
                            event.preventDefault();
                            map.scrollWheelZoom.enable();
                            setTimeout(function () {
                                map.scrollWheelZoom.disable();
                            }, 1000);
                        } else {
                            map.scrollWheelZoom.disable();
                        }
                    });
                }
            };

            document.addEventListener("DOMContentLoaded", function() {
                maps.init();
            });
        </script>

        <!-- ... end JS-init for Map -->

        <!------------------------------------------- ... end Map ----------------------------------------------------->

    </div>
</section>


<section class="medium-padding120">
    <div class="container">
        <div class="row">
            <div class="col col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">

                
                <!-- Contact Item -->
                
                <div class="contact-item-wrap">
                    <h3 class="contact-title">SELAA HQ</h3>
                    <div class="contact-item">
                        <a href="#">MOSTAKBAL CITY,14 road 1, EGY</a>
                    </div>
                    <div class="contact-item">
                        <h6 class="sub-title">General Inquiries</h6>
                        <a href="mailto:">hqinquiries@selaa.com</a>
                    </div>
                </div>
                
                <!-- ... end Contact Item -->
            </div>

            <div class="col col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">

                
                <!-- Contact Item -->
                
                <div class="contact-item-wrap">
                    <h3 class="contact-title">Press and Media</h3>
                    <div class="contact-item">
                        <h6 class="sub-title">Jenny Stevens</h6>
                        <a href="mailto:">jennymedia@selaa.com</a>
                    </div>
                    <div class="contact-item">
                        <h6 class="sub-title">Skype</h6>
                        <a href="#">Stevens Press</a>
                    </div>
                </div>
                
                <!-- ... end Contact Item -->

            </div>
            <div class="col col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">

                
                <!-- Contact Item -->
                
                <div class="contact-item-wrap">
                    <h3 class="contact-title">Business Chat</h3>
                    <div class="contact-item">
                        <h6 class="sub-title">Hossam Elhossieny</h6>
                        <a href="mailto:">hossamelhossieny@selaa.com</a>
                    </div>
                    <div class="contact-item">
                        <h6 class="sub-title">Skype</h6>
                        <a href="#">Hossam Elhossieny</a>
                    </div>
                </div>
                
                <!-- ... end Contact Item -->

            </div>
            <div class="col col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">

                
                <!-- Contact Item -->
                
                <div class="contact-item-wrap">
                    <h3 class="contact-title">Human Resources</h3>
                    <div class="contact-item">
                        <h6 class="sub-title">Stella Karenson</h6>
                        <a href="mailto:">stellahhrr@olympus.com</a>
                    </div>
                    <div class="contact-item">
                        <h6 class="sub-title">Skype</h6>
                        <a href="#">Karenson HHRR</a>
                    </div>
                </div>
                
                <!-- ... end Contact Item -->

            </div>
        </div>
    </div>
</section>



<section class="medium-padding120 bg-body contact-form-animation">
    <div class="container">
        <div class="row">
            <div class="col col-xl-10 col-lg-10 col-md-12 col-sm-12  m-auto">

                
                <!-- Contacts Form -->
                
                <div class="contact-form-wrap">
                    <div class="contact-form-thumb">
                        <h2 class="title">SEND US <span>A RAVEN</span></h2>
                        <p>Do you have general questions about SELAA Social Network? Send us a raven and we’ll answer you as fast as we can!</p>
                        <img loading="lazy" src="{{ asset('img/crew.webp') }}" alt="crew" class="crew" data-aos="fade-right" data-aos-delay="200" data-aos-duration="500" width="104" height="119">
                    </div>
                    <form class="contact-form" method="post" action="{{route('send_msg',app()->getLocale())}}">
                        @csrf
                        <div class="row">
                            <div class="col col-12 col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">First Name</label>
                                    <input class="form-control" placeholder="first name" type="text" name="first_name">
                                </div>
                            </div>
                            <div class="col col-12 col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Last Name</label>
                                    <input class="form-control" placeholder="last name" type="text" name="last_name">
                                </div>
                            </div>
                            <div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Your Email</label>
                                    <input class="form-control" placeholder="input your email" type="email" name="user_email">
                                </div>

                                <div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Your phone</label>
                                    <input class="form-control" placeholder="input your mobile" type="number" name="user_mobile">
                                </div>
                
                                <div class="form-group label-floating is-select">
                                    <label class="control-label">Inquiry Subject</label>
                                    <select class="form-select" name="about">
                                        <option value="account">SELAA Account Inquiries</option>
                                        <option value="business">SELAA Business Inquiries</option>
                                        <option value="faq">SELAA FAQ Inquiries</option>
                                        <option value="other">SELAA other Inquiries</option>
                                    </select>
                                </div>
                
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Your Message" name="message"></textarea>
                                </div>
                
                                <button class="btn btn-purple btn-lg full-width">Send Message</button>
                            </div>
                        </div>
                    </form>
                </div>
                
                <!-- ... end Contacts Form -->

            </div>
        </div>
    </div>

    <div class="half-height-bg bg-white"></div>
</section>


</x-app-layout>