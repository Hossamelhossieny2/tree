<x-app-layout>
<!-- Main Header Groups -->

<!-- Main Header BlogV1 -->

<div class="main-header">
    <div class="content-bg-wrap bg-account"></div>
    <div class="container">
        <div class="row">
            <div class="col col-lg-8 m-auto col-md-8 col-sm-12 col-12">
                <div class="main-header-content">
                    <h1>{{ __('read_news') }}</h1>
                    
                </div>
            </div>
        </div>
    </div>

   
</div>

<!-- ... end Main BlogV1 -->


<!-- <div class="container">
    <div class="row">
        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="ui-block responsive-flex1200">
                <div class="ui-block-title">
                    
                    <div class="w-select">
                        <div class="title">Filter By:</div>
                        <fieldset class="form-group">
                            <select class="form-select">
                                <option value="NU">All Categories</option>
                                <option value="NU">Favourite</option>
                                <option value="NU">Likes</option>
                            </select>
                        </fieldset>
                    </div>

                    <div class="w-select">
                        <fieldset class="form-group">
                            <select class="form-select">
                                <option value="NU">Date (Descending)</option>
                                <option value="NU">Date (Ascending)</option>
                            </select>
                        </fieldset>
                    </div>

                    <a href="#" data-bs-toggle="modal" data-bs-target="#create-photo-album" class="btn btn-primary btn-md-2">Filter</a>

                    <form class="w-search">
                        <div class="form-group with-button">
                            <input class="form-control" type="text" placeholder="Search Blog Posts......">
                            <button>
                                <svg class="olymp-magnifying-glass-icon"><use xlink:href="#olymp-magnifying-glass-icon"></use></svg>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->

<div class="container">
    <div class="row">

        <div class="col col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
            @foreach($news as $new)
            
            <div class="ui-block">

                <!-- Post -->
                
                <article class="hentry blog-post blog-post-v3">
                
                    <div class="post-thumb">
                        @if($new['image'])
                        <img loading="lazy" src="{{$new['image']}}" alt="photo" width="370" height="261">
                        @else
                        <img loading="lazy" src="{{ asset('img/default/icon-flv.png')}}" alt="photo" width="370" height="261">
                        @endif
                        <a href="#" class="post-category bg-blue-light">{{$new['source']}}</a>
                    </div>
                
                    <div class="post-content">
                
                        <div class="author-date">
                            by
                            <a class="h6 post__author-name fn" href="#">{{$new['source']}}</a>
                            <div class="post__date">
                                <time class="published" datetime="2017-03-24T18:18">
                                    {{$new['start_date']}}
                                </time>
                            </div>
                        </div>
                
                        <a href="#" class="h3 post-title">{{$new['title']}} </a>
                        <p>{{$new['desc']}}</p>
                
                        <div class="post-additional-info inline-items">
                
                            <div class="friends-harmonic-wrap">
                               
                                <div class="names-people-likes">
                                
                                </div>
                            </div>
                
                            <div class="comments-shared">
                                <a target="_blank" href="{{$new['link']}}" class="post-add-icon inline-items btn btn-lg btn-primary text-white">
                                   {{__('see_on_org')}}
                                </a>
                            </div>
                
                        </div>
                    </div>
                
                </article>
                
                <!-- ... end Post -->

            </div>

           @endforeach

            
            <!-- Pagination -->
            
            <nav aria-label="Page navigation">

                <!-- <ul class="pagination justify-content-center">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1">Previous</a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1<div class="ripple-container"><div class="ripple ripple-on ripple-out" style="left: -10.3833px; top: -16.8333px; background-color: rgb(255, 255, 255); transform: scale(16.7857);"></div></div></a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">...</a></li>
                    <li class="page-item"><a class="page-link" href="#">12</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#">Next</a>
                    </li>
                </ul> -->
            </nav>
            
            <!-- ... end Pagination -->

        </div>

        <div class="col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
            <aside>

                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">{{__('man_says')}}</h6>
                    </div>
                </div>

                @for($i=0;$i< count($news)-6;$i++)
                
                <div class="ui-block">

                    <!-- Post -->
                    
                    <article class="hentry blog-post blog-post-v3 featured-post-item">
                    
                        <div class="post-thumb">
                            <img loading="lazy" src="{{ ($events[$i]['link'] == 'x') ? asset($events[$i]['image']) : $events[$i]['image'] }}" alt="photo" width="368" height="180">
                            <a href="#" class="post-category bg-purple">{{ $events[$i]['title'] }}</a>
                        </div>
                    
                        <div class="post-content">
                    
                            <a href="#" class="h4 post-title">{{ $events[$i]['desc'] }}</a>
                            
                        </div>
                    
                    </article>
                    
                    <!-- ... end Post -->

                </div>
                @endfor

            </aside>
        </div>

    </div>

</div>

</x-app-layout>