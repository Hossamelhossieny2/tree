<x-app-layout>
    <!-- Top Header-Profile -->
@push('styles')
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" rel="stylesheet" />
<style>
.html {
    direction: rtl;
}

.bootstrap-tagsinput .tag {
    margin-right: 2px;
    color: #ffffff;
    background: #2196f3;
    padding: 3px 7px;
    border-radius: 3px;
}

.bootstrap-tagsinput {
    width: 100%;
}
</style>
@endpush

<div class="container" style="padding-top: 50px">
    @if(Session::has('success'))
    <div class="alert alert-success">
        {{ Session::get('success') }}
        @php
        Session::forget('success');
        @endphp
    </div>
    @endif
    <div class="row">
        <form action="{{ route('create-gallery',app()->getLocale()) }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="mb-3">
                <input type="text" class="form-control" name="title_name" placeholder="{{__('pic_title')}}">
                @if ($errors->has('title_name'))
                <span class="text-danger">{{ $errors->first('title_name') }}</span>
                @endif
            </div>

            <div class="mb-3">
                <input type="file" name="pic" id="pic" required="" onchange="previewImage(event)">
                @if ($errors->has('pic'))
                <span class="text-danger">{{ $errors->first('pic') }}</span>
                @endif
                <img id="pic-preview" src="#" alt="Image Preview" style="display: none; margin-top: 10px; max-width: 100%;">
            </div>

            <div class="mb-3">
                <textarea class="form-control" name="content" placeholder="{{__('pic_desc')}}"></textarea>
                @if ($errors->has('content'))
                <span class="text-danger">{{ $errors->first('content') }}</span>
                @endif
            </div>

            <div class="mb-3">
                <div class="form-group label-floating is-select">
                    <label class="control-label">{{ __('post_privacy') }}</label>
                    <select name="public" class="form-select">
                        <option value="1">{{ __('post_public') }} ({{__('gall_1')}} - {{__('gall_2')}})</option>
                        <option value="0">{{ __('post_private') }} ({{__('gall_1')}})</option>
                    </select>
                </div>
            </div>

            <div class="mb-3">
                <p class="text-white">{{__('pic_tags')}}</p>
                <input class="form-control" type="text" id="tags" data-role="tagsinput" name="tags" placeholder="{{__('enter_tags')}}">
                @if ($errors->has('tags'))
                <span class="text-danger">{{ $errors->first('tags') }}</span>
                @endif
            </div>

            <div class="d-grid">
                <button class="btn btn-info btn-submit">{{__('publish')}}</button>
            </div>
        </form>
    </div>
</div>

@push('scripts2')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
<script>
    function previewImage(event) {
        var reader = new FileReader();
        reader.onload = function() {
            var output = document.getElementById('pic-preview');
            output.src = reader.result;
            output.style.display = 'block';
        }
        reader.readAsDataURL(event.target.files[0]);
    }
</script>
@endpush
</x-app-layout>