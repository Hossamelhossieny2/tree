<x-app-layout>
<!-- Main Header Groups -->

<div class="main-header">
    <div class="content-bg-wrap bg-group" style="background-image: url('../img/family_bg2.webp');"></div>
    <div class="container">
        <div class="row">
            <div class="col col-lg-8 m-auto col-md-8 col-sm-12 col-12">
                <div class="main-header-content">
                    <h1>{{ $tree_name }} Family Page</h1>
                </div>
            </div>
        </div>
    </div>
    
</div>

<!-- ... end Main Header Groups -->		

<div class="container">
        <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="ui-block responsive-flex">
                    <div class="ui-block-title">
                        <div class="h6 title">{{ $tree_name }} Tree Print</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <input name="slider" type="range" value="50" min="25" id="zoomer" name="slider" oninput="deepdive()">
                <!-- <label for="slider" id="zoomer_label">50</label> -->
            </div>
        </div>
    </div>

	<x-tree_member :person="$person" :tree="$tree" :first="1" />
@push('scripts')
<script src="{{ asset('js/libs-uncompressed/perfect-scrollbar.js') }}"></script>
<script type="text/javascript">

    $(document).ready(function () {

        const input = document.getElementById('zoomer');
        const treeP = document.getElementById('pepole-tree');
        const label = document.getElementById('zoomer_label');
        var zoomlevel = 100;

        input.addEventListener("input", (event) => {
          const value = Number(input.value) / 100;
          input.style.setProperty("--thumb-rotate", `${value * 720}deg`);
        } , true);

    var topUserSearch = $('.js-user-search');

    if (topUserSearch.length) {
        topUserSearch.selectize({
            delimiter: ',',
            persist: false,
            maxItems: 2,
            valueField: 'name',
            labelField: 'name',
            searchField: ['name'],
            options: [
            <?php foreach($famUsers as $chat){
                if(!empty($chat->profile_photo_path)){
                    $pic = asset($chat->profile_photo_path);
                }else{
                    $pic = asset('img/default/user_'.$chat->gender.'.png');
                }
                ?>
                {
                    image: '{{ $pic }}',
                    name: '{{ $chat->name}}',
                    message: '12 Friends in Common',
                    icon: 'olymp-happy-face-icon'
                },
            <?php }?>
                
            ],
            render: {
                option: function (item, escape) {
                    return '<div class="inline-items">' +
                        (item.image ? '<div class="author-thumb"><img src="' + escape(item.image) + '" alt="avatar"></div>' : '') +
                        '<div class="notification-event">' +
                        (item.name ? '<span class="h6 notification-friend"></a>' + escape(item.name) + '</span>' : '') +
                        (item.message ? '<span class="chat-message-item">' + escape(item.message) + '</span>' : '') +
                        '</div>' +
                        (item.icon ? '<span class="notification-icon"><svg class="' + escape(item.icon) + '"><use xlink:href="#' + escape(item.icon) + '"></use></svg></span>' : '') +
                        '</div>';
                },
                item: function (item, escape) {
                    var label = item.name;
                    return '<div>' +
                        '<span class="label">' + escape(label) + '</span>' +
                        '</div>';
                }
            }
        });
    }
});

(function ($) {
    $.fn.inlineStyle = function (prop) {
         var styles = this.attr("style"),
             value;
         styles && styles.split(";").forEach(function (e) {
             var style = e.split(":");
             if ($.trim(style[0]) === prop) {
                 value = style[1];           
             }                    
         });   
         return value;
    };
}(jQuery));
var zoomer = document.getElementById('zoomer');
var hubblecontainer = document.getElementById('hubble-container');

if (hubblecontainer) {
    hubblecontainer.style.webkitTransform = "scale(0.5)";
    hubblecontainer.style.backgroundSize = "cover";
    var checkDiv = setInterval(testDiv, 1000);
}

function testDiv(){
    var attr = $("#hubble-container").inlineStyle("transform");
    //console.log(attr);
    if(attr){
        centerDiv();
        clearInterval(checkDiv);
        setTimeout(function(){ 
            window.scrollTo({
              top: 0,          
            }); 
         }, 500);
                 
    }
}

function centerDiv(){
document.getElementById('hubble-container').scrollIntoView({
            behavior: 'auto',
            block: 'center',
            inline: 'center'
        });

        var marginTop = parseInt(400)-600;     
        $('#hubble-container').css('margin-top',marginTop);
       
}

function deepdive(){ 
    zoomlevel = zoomer.valueAsNumber/100;
    
    var marginTop = parseInt(zoomer.valueAsNumber*8)-600;     
    $('#hubble-container').css('margin-top',marginTop);
    hubblecontainer.style.webkitTransform = "scale("+zoomlevel+")";
    hubblecontainer.style.transform = "scale("+zoomlevel+")";
}




</script>
@endpush
</x-app-layout>