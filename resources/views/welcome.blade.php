<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('fav.ico/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('fav.ico/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('fav.ico/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('fav.ico/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('fav.ico/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('fav.ico/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('fav.ico/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('fav.ico/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('fav.ico/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('fav.ico/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('fav.ico/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('fav.ico/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('fav.ico/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('fav.ico/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('fav.ico/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">

    <title>{{ __('triangle_social_network') }} - {{__('home')}}</title>

    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="robots" content="noindex" />
    <meta name="keywords" content="{{ __('keywords') }}">
    <meta name="description" content="{{ __('complete_social_network') }}">

    <meta property="og:title" content="{{ __('triangle_social_network') }}">
    <meta property="og:description" content="{{ __('complete_social_network') }}">
    <meta property="og:image" content="https://selaa.social/public/img/default/icon-flv.png">
    <meta property="og:url" content="https://selaa.social/public/ar/webfamily">
    <meta name="twitter:card" content="https://selaa.social/public/img/default/icon-flv.png">

    <!-- Theme Font -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/theme-font.min.css') }}" as="style">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('Bootstrap/dist/css/bootstrap.css') }}">

    <!-- Main Styles CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.min.css') }}">

    

    @if(app()->getlocale() == 'ar' || app()->getlocale() == 'cn')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/rtl.css') }}">
    <link href="{{ asset('css/bs5-intro-tour-ar.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/tree-rtl.css') }}">
    @else
    <link href="{{ asset('css/bs5-intro-tour.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/tree-ltr.css') }}">
    @endif
    
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
        .more-dropdown {
            position: relative;
            display: inline-block;
        }

        .more-dropdown ul {
            display: none;
            position: absolute;
            background-color: #f9f9f9;
            min-width: 160px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
        }

        .more-dropdown:hover ul {
            display: block;
        }

        .more-dropdown ul li {
            color: black !important;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }

        .more-dropdown ul li:hover {
            background-color: #000;
            color: black;
        }
    </style>
</head>
<body class="body-bg-white">

<!-- Preloader -->

<div id="hellopreloader">
    <div class="preloader">
        <svg width="45" height="45" stroke="#fff">
            <g fill="none" fill-rule="evenodd" stroke-width="2" transform="translate(1 1)">
                <circle cx="22" cy="22" r="6" stroke="none">
                    <animate attributeName="r" begin="1.5s" calcMode="linear" dur="3s" repeatCount="indefinite" values="6;22"/>
                    <animate attributeName="stroke-opacity" begin="1.5s" calcMode="linear" dur="3s" repeatCount="indefinite" values="1;0"/>
                    <animate attributeName="stroke-width" begin="1.5s" calcMode="linear" dur="3s" repeatCount="indefinite" values="2;0"/>
                </circle>
                <circle cx="22" cy="22" r="6" stroke="none">
                    <animate attributeName="r" begin="3s" calcMode="linear" dur="3s" repeatCount="indefinite" values="6;22"/>
                    <animate attributeName="stroke-opacity" begin="3s" calcMode="linear" dur="3s" repeatCount="indefinite" values="1;0"/>
                    <animate attributeName="stroke-width" begin="3s" calcMode="linear" dur="3s" repeatCount="indefinite" values="2;0"/>
                </circle>
                <circle cx="22" cy="22" r="8">
                    <animate attributeName="r" begin="0s" calcMode="linear" dur="1.5s" repeatCount="indefinite" values="6;1;2;3;4;5;6"/>
                </circle>
            </g>
        </svg>

        <div class="text">Loading ...</div>
    </div>
</div>

<!-- ... end Preloader -->
<div class="main-header main-header-fullwidth main-header-has-header-standard">

    
    <!-- Header Standard Landing  -->
    
    <div class="header--standard header--standard-landing" id="header--standard">
        <div class="container">
            <div class="header--standard-wrap">
    
                <a href="#" class="logo">
                    <div class="img-wrap">
                        <img loading="lazy" src="{{ asset('img/default/tree_logo_small_white.png') }}" alt="Olympus" width="34" height="34">
                        <img loading="lazy" src="{{ asset('img/default/tree_logo_small_orange.png') }}" width="34" height="34" alt="Olympus" class="logo-colored">
                    </div>
                    <div class="title-block">
                        <h6 class="logo-title">selaa</h6>
                        <div class="sub-title">{{ __('triangle_social_network') }}</div>
                    </div>
                </a>
                
                <a href="#" class="open-responsive-menu js-open-responsive-menu">
                    <svg class="olymp-menu-icon"><use xlink:href="#olymp-menu-icon"></use></svg>
                </a>
                <!-- <img src="{{ asset('img/default/help.png') }}" id="helpMe" width="60"> -->
                <div class="nav nav-pills nav1 header-menu">
                    <div class="mCustomScrollbar">
                        <ul>
                            <li class="nav-item">
                                <a href="#homepage" class="nav-link">{{ __('home') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#why_us">{{ __('why_join') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#feature">{{ __('triangle_family_app') }}</a>
                            </li>
                            <li class="nav-item">
                                <a href="#news" class="nav-link">{{ __('our_last_news') }}</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('terms.show') }}" class="nav-link">{{ __('terms') }}</a>
                            </li>
                            
                            <li class="nav-item">
                                <a href="{{ route('policy.show') }}" class="nav-link">{{ __('privacy') }}</a>
                            </li>
                            <li class="shoping-cart more">
                                <a href="#" class="nav-link">
                                    <img loading="lazy" src="{{ asset('img/default/'.app()->getLocale().'.png') }}" alt="language flag" width="80">
                                </a>
                                <div class="more-dropdown">
                                    <ul>
                                        <li @if (app()->getLocale() == "ar") class="bg-smoke" @endif class="text-center">
                                            <a href="{{ route('webfamily', ['ar']) }}">
                                                <div class="author-thumb">
                                                    <img loading="lazy" src="{{ asset('img/default/ar.png') }}" alt="arabic" width="34" height="34">
                                                </div>
                                                <div class="un-read">
                                                    اللغة العربية
                                                </div>
                                            </a>
                                        </li>
                                       <li @if (app()->getLocale() == "en") class="bg-smoke" @endif class="text-center">
                                            <a href="{{ route('webfamily', ['en']) }}">
                                                <div class="author-thumb">
                                                    <img loading="lazy" src="{{ asset('img/default/en.png') }}" alt="english" width="34" height="34">
                                                </div>
                                                <div class="un-read">
                                                    English language
                                                </div>
                                            </a>
                                        </li>
                                        <li @if (app()->getLocale() == "fr") class="bg-smoke" @endif class="text-center">
                                            <a href="{{ route('webfamily', ['fr']) }}">
                                                <div class="author-thumb">
                                                    <img loading="lazy" src="{{ asset('img/default/fr.png') }}" alt="frensh" width="34" height="34">
                                                </div>
                                                <div class="un-read">
                                                    langue française
                                                </div>
                                            </a>
                                        </li>
                                        <li @if (app()->getLocale() == "tr") class="bg-smoke" @endif class="text-center">
                                            <a href="{{ route('webfamily', ['tr']) }}">
                                                <div class="author-thumb">
                                                    <img loading="lazy" src="{{ asset('img/default/tr.png') }}" alt="turkisk" width="34" height="34">
                                                </div>
                                                <div class="un-read">
                                                    Türk Dili
                                                </div>
                                            </a>
                                        </li>
                                        <li @if (app()->getLocale() == "ru") class="bg-smoke" @endif class="text-center">
                                            <a href="{{ route('webfamily', ['ru']) }}">
                                                <div class="author-thumb">
                                                    <img loading="lazy" src="{{ asset('img/default/ru.png') }}" alt="russian" width="34" height="34">
                                                </div>
                                                <div class="un-read">
                                                    русский язык
                                                </div>
                                            </a>
                                        </li>
                                        <li @if (app()->getLocale() == "de") class="bg-smoke" @endif class="text-center">
                                            <a href="{{ route('webfamily', ['de']) }}">
                                                <div class="author-thumb">
                                                    <img loading="lazy" src="{{ asset('img/default/de.png') }}" alt="german" width="34" height="34">
                                                </div>
                                                <div class="un-read">
                                                    deutsche Sprache
                                                </div>
                                            </a>
                                        </li>
                                        <li @if (app()->getLocale() == "es") class="bg-smoke" @endif class="text-center">
                                            <a href="{{ route('webfamily', ['es']) }}">
                                                <div class="author-thumb">
                                                    <img loading="lazy" src="{{ asset('img/default/es.png') }}" alt="spanish" width="34" height="34">
                                                </div>
                                                <div class="un-read">
                                                    lengua española
                                                </div>
                                            </a>
                                        </li>
                                        <li @if (app()->getLocale() == "cn") class="bg-smoke" @endif class="text-center">
                                            <a href="{{ route('webfamily', ['cn']) }}">
                                                <div class="author-thumb">
                                                    <img loading="lazy" src="{{ asset('img/default/cn.png') }}" alt="chinease" width="34" height="34">
                                                </div>
                                                <div class="un-read">
                                                    中文
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="close-responsive-menu js-close-responsive-menu">
                                <svg class="olymp-close-icon"><use xlink:href="#olymp-close-icon"></use></svg>
                            </li>
                            <li class="nav-item js-expanded-menu">
                                <a href="#" class="nav-link">
                                    <svg class="olymp-menu-icon"><use xlink:href="#olymp-menu-icon"></use></svg>
                                    <svg class="olymp-close-icon"><use xlink:href="#olymp-close-icon"></use></svg>
                                </a>
                            </li>
                            
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
    
    <!-- ... end Header Standard Landing  -->
    <div class="header-spacer--standard"></div>

    <div class="content-bg-wrap bg-landing"></div>

    <div class="container">
        <div class="row display-flex">
            <div class="col col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">
                <div class="landing-content">

                    <h1>{{ __('complete_social_network') }}</h1>
                    
                    <p>{{ __('site_word') }}</p>
                    
                </div>
            </div>

            <div class="col col-xl-5 ms-auto col-lg-6 col-md-12 col-sm-12 col-12">
                <x-jet-validation-errors class="mb-4 bg-smoke" style="color: red;font-size: 16px;"/>
                
                @auth
                    <form method="POST" action="{{ route('logout', app()->getLocale()) }}">
                        @csrf
                        <a class="btn btn-md btn-border c-white" href="{{ route('logout', app()->getLocale()) }}" onclick="event.preventDefault(); this.closest('form').submit();">
                            <i class="fa fa-sign-out"></i>{{ __('logout') }}
                        </a>
                    </form>
                    
                @else
                <h3>{{__('register_till_now')}} {{arabic_w2e(number_format($family+11111))}} {{__('family')}}</h3>
                <br>
                    <a href="#" class="btn btn-lg btn-border c-white col-6" data-bs-toggle="modal" data-bs-target="#selaa-login" style="float: left">{{ __('login') }}</a>

                    <a href="#" class="btn btn-lg btn-border c-white col-6" data-bs-toggle="modal" data-bs-target="#selaa-register" style="float: left">{{ __('register') }}</a>
                @endauth

            </div>

        </div>
    </div>

    <img loading="lazy" class="img-bottom" src="{{ asset('img/group-bottom.webp') }}" alt="friends" width="1087" height="148">
    <img loading="lazy" class="img-rocket" src="{{ asset('img/rocket.webp') }}" alt="rocket" width="97" height="96">
</div>

<!-- Clients Block -->

<section class="crumina-module crumina-clients">
    <div class="container">
        <div class="row">
            @include('components.profit_tree')
        </div>
    </div>
</section>

<!-- ... end Clients Block -->


<!-- Section Img Scale Animation -->

<section class="align-center pt80 section-move-bg-top img-scale-animation" id="homepage">
    <div class="container">
        <div class="row">
            <div class="m-auto col col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12">
                <img class="main-img" src="{{ asset('img/scale1.webp') }}" alt="screen" data-aos="zoom-in-down" data-aos-delay="200" data-aos-duration="500" width="760" height="450">
            </div>
        </div>

        <img class="first-img1" alt="img" src="{{ asset('img/scale2.webp') }}" data-aos="zoom-in-down" data-aos-delay="700" data-aos-duration="500" width="140" height="187">
        <img class="second-img1" alt="img" src="{{ asset('img/scale3.webp') }}" data-aos="zoom-in-down" data-aos-delay="1000" data-aos-duration="500" width="242" height="125">
        <img class="third-img1" alt="img" src="{{ asset('img/scale4.webp') }}" data-aos="zoom-in-down" data-aos-delay="1300" data-aos-duration="500" width="391" height="134">
    </div>
    <div class="content-bg-wrap bg-section2"></div>
</section>

<!-- ... end Section Img Scale Animation -->

<section class="medium-padding120" id="why_us" style="background-image: url( {{ asset('img/pattern/p1.png') }} )">
    <div class="container">
        <div class="row">
            <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <img loading="lazy" src="{{ asset('img/default/icon-flv.png') }}" alt="screen" width="538" height="289">
            </div>

            <div class="m-auto col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                <div class="crumina-module crumina-heading">
                    <h2 class="heading-title">{{ __('why_join') }} <span class="c-primary">{{ __('triangle_social_network') }}</span></h2>
                    <p class="heading-text">{{ __('cause_join') }}</p>
                    <p class="heading-text">{{ __('cause_join2') }}</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="medium-padding120">
    <div class="container">
        <div class="row">
            <div class="m-auto col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                <div class="crumina-module crumina-heading">
                    <h2 class="heading-title">{{ __('connect_family_mem') }} <span class="c-primary">{{ __('all_over_theworld') }}</span></h2>
                    <p class="heading-text">{{ __('cause_connect') }}</p>
                    <p class="heading-text">{{ __('cause_connect2') }}</p>
                </div>
            </div>

            <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <img loading="lazy" src="{{ asset('img/image1.webp') }}" alt="screen" width="579" height="334">
            </div>
        </div>
    </div>
</section>


<section class="medium-padding120"  style="background-image: url( {{ asset('img/pattern/p3.png') }} )">
    <div class="container">
        <div class="row">
            <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <img loading="lazy" src="{{ asset('img/image2.webp') }}" alt="screen" width="588" height="262">
            </div>

            <div class="m-auto col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                <div class="crumina-module crumina-heading">
                    <h2 class="heading-title">{{ __('search_all') }} <span class="c-primary">{{ __('family_cv') }}</span></h2>
                    <p class="heading-text">{{ __('cause_cv') }}</p>
                    <p class="heading-text">{{ __('cause_cv2') }}</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="medium-padding120 bg-users">
    <div class="container">
        <div class="row">
            <div class="m-auto col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                <div class="crumina-module crumina-heading">
                    <h2 class="text-white heading-title">{{ __('share_thoughts') }} <span class="c-primary">{{ __('big_family') }}</span></h2>
                    <p class="text-white heading-text">{{ __('cause_share') }}</p>
                </div>
            </div>

            <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <img loading="lazy" src="{{ asset('img/image3.webp') }}" alt="screen" width="548" height="351">
            </div>
        </div>
    </div>
</section>



<!-- Planer Animation -->

<section class="medium-padding120 bg-section3 background-cover planer-animation">
    <div class="container">
        <div class="row mb60">
            <div class="m-auto col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                <div class="crumina-module crumina-heading align-center">
                    <div class="heading-sup-title">{{ __('triangle_social_network') }}</div>
                    <h2 class="h1 heading-title">{{ __('community_reviews') }}</h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="swiper-container pagination-bottom" data-show-items="3">
                <div class="swiper-wrapper">
                    
                    @foreach($tests as $test)
                    <div class="ui-block swiper-slide">

                        
                        <!-- Testimonial Item -->
                        
                        <div class="crumina-module crumina-testimonial-item">
                            <div class="testimonial-header-thumb" style="background-image:url({{asset('img/flag/'.$test->iso.'.svg')}}); "></div>
                        
                            <div class="testimonial-item-content">
                        
                                <div class="author-thumb">
                                    <img loading="lazy" src="{{ asset($test->image) }}" alt="author" width="92" height="92">
                                </div>
                        
                                <h3 class="testimonial-title">{{$test->title}}</h3>
                        
                                <ul class="rait-stars">
                                    @for($i=0;$i<$test->rate;$i++)
                                    <li>
                                        <svg class="star-icon" width="10" height="10"><use xlink:href="#olymp-star-full"></use></svg>
                                    </li>
                                    @endfor
                                    
                                </ul>
                        
                                <p class="testimonial-message">{{$test->desc}}</p>
                        
                                <div class="author-content">
                                    <a href="#" class="h6 author-name">{{$test->user_name}}</a>
                                    <div class="country">{{$test->place}}</div>
                                </div>
                            </div>
                        </div>
                        
                        <!-- ... end Testimonial Item -->
                    </div>
                    @endforeach
                </div>

                <div class="swiper-pagination"></div>
            </div>
        </div>
    </div>

    <img loading="lazy" src="{{ asset('img/planer.webp') }}" alt="planer" class="planer" data-aos="fade-right" data-aos-delay="200" data-aos-duration="500" width="356" height="140">
</section>

<!-- ... end Section Planer Animation -->

<section class="medium-padding120" id="feature">
    <div class="container">
        <div class="row">
            <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <img loading="lazy" src="{{ asset('img/image4.webp') }}" alt="screen" width="597" height="601">
            </div>

            <div class="m-auto col col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">
                <div class="crumina-module crumina-heading">
                    <h2 class="h1 heading-title">{{ __('release_your_power') }} <span class="c-primary">{{ __('triangle_family_app') }}!</span></h2>
                    <p class="heading-text">{{ __('cause_app') }}</p>
                </div>

                
                <ul class="list--styled">
                    <li>
                        <svg width="20" height="20"><use xlink:href="#olymp-check-circle"></use></svg>
                        {{ __('soon') }}
                    </li>
                    <li>
                        <svg width="20" height="20"><use xlink:href="#olymp-check-circle"></use></svg>
                        {{ __('soon') }}
                    </li>
                </ul>

                <a href="#" class="btn btn-market">
                    <img class="icon" src="{{ asset('svg-icons/apple-logotype.svg') }}" alt="app store">
                    <div class="text">
                        <span class="sup-title">{{ __('not_aval') }}</span>
                        <span class="title">App Store</span>
                    </div>
                </a>

                <a href="#" class="btn btn-market">
                    <img class="icon" src="{{ asset('svg-icons/google-play.svg') }}" alt="google">
                    <div class="text">
                        <span class="sup-title">{{ __('not_aval') }}</span>
                        <span class="title">Google Play</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>


<!-- Section Subscribe Animation -->

<section class="medium-padding100 subscribe-animation bg-users">
    <div class="container">
        <div class="row">
            <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="crumina-module crumina-heading c-white custom-color">
                    <h2 class="h1 heading-title">{{ __('triangle_newsletter') }}</h2>
                    <p class="heading-text">{{ __('subscribe_for') }}</p>
                </div>

                
                <!-- Subscribe Form  -->
                
                <form class="form-inline subscribe-form" method="post">
                    <div class="form-group label-floating is-empty">
                        <label class="control-label">{{ __('enter_your_email') }}</label>
                        <input class="bg-white form-control" placeholder="" type="email" autocomplete="email">
                    </div>
                
                    <button class="btn btn-blue btn-lg">{{ __('send') }}</button>
                </form>
                
                <!-- ... end Subscribe Form  -->

            </div>
        </div>

        <img loading="lazy" src="{{ asset('img/paper-plane.webp') }}" alt="plane" class="plane" data-aos="fade-right" data-aos-delay="200" data-aos-duration="700" width="387" height="316">
    </div>
</section>

<!-- ... end Section Subscribe Animation -->
<section class="medium-padding120"  id="news" style="background-image: url( {{ asset('img/pattern/p10.png') }} )">
    <div class="container">
        <div class="row mb60">
            <div class="m-auto col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                <div class="crumina-module crumina-heading align-center">
                    <h2 class="h1 heading-title">{{ __('our_last_news') }}</h2>
                </div>
            </div>
        </div>

        <div class="row">
            @foreach($news as $new)
            <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">

                <!-- Post -->
                
                <article class="hentry blog-post">
                
                    <div class="post-thumb">
                        <img loading="lazy" src="{{ asset($new->image) }}" alt="photo" width="370" height="261">
                    </div>
                
                    <div class="post-content">

                        <a href="#" class="post-category bg-blue-light">{{ $new->tag }}</a>
                        <a href="#" class="h4 post-title">{{ $new->title }} </a>
                        <p>{{ $new->desc }}</p>
                
                        <div class="author-date">
                            <img src="{{asset('img/flag/'.strtolower($new->country).'.svg')}}" width="15" /> by
                            <a class="h6 post__author-name fn" href="#">Site administrator</a>
                            <div class="post__date">
                                <time class="published" datetime="{{ date('Y-m-d')}}">
                                    - 8 hours ago
                                </time>
                            </div>
                        </div>
                
                       
                    </div>
                
                </article>
                
                <!-- ... end Post -->
            </div>
            @endforeach
        </div>
    </div>
</section>


<!-- Section Call To Action Animation -->

<section class="align-right pt160 pb80 section-move-bg call-to-action-animation">
    <div class="container">
        <div class="row">
            <div class="m-auto col col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12">
                <a href="#" class="btn btn-primary btn-lg"  data-bs-toggle="modal" data-bs-target="#registration-login-form-popup">{{ __('ask_to_join') }}</a>
            </div>
        </div>
    </div>
    <img class="first-img" alt="guy" src="{{ asset('img/guy.webp') }}" data-aos="fade-up" data-aos-delay="200" data-aos-duration="500" width="297" height="254">
    <img class="second-img" alt="rocket" src="{{ asset('img/rocket1.webp') }}" data-aos="fade-up" data-aos-delay="500" data-aos-duration="500">
    <div class="content-bg-wrap bg-section1"></div>
</section>

<!-- ... end Section Call To Action Animation -->

<div class="modal fade" id="selaa-login" tabindex="-1" role="dialog" aria-labelledby="selaa-login" aria-hidden="true">
    <div class="modal-dialog window-popup restore-password-popup" role="document">
        <div class="modal-content">
            <a href="#" class="close icon-close" data-bs-dismiss="modal" aria-label="Close">
                <svg class="olymp-close-icon"><use xlink:href="#olymp-close-icon"></use></svg>
            </a>

            <div class="modal-header">
                <h6 class="title">{{ __('login_to_you_account') }}</h6>
            </div>

            <div class="modal-body">
                <form class="content" method="POST" action="{{ route('HomeLogin', app()->getLocale()) }}">
                    @csrf
                    <div class="row">
                        <div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <x-jet-validation-errors class="mb-4" />

                            <div>
                                <x-jet-label for="login" class="control-label" value="{{ __('email') }} / {{ __('mobile_number') }}" />
                                <x-jet-input id="login" class="form-control" type="text" name="login" :value="old('email')" autocomplete="phone_number" required autofocus />
                            </div>

                            <div class="mt-4">
                                <x-jet-label for="password" class="control-label" value="{{ __('password') }}" />
                                <x-jet-input class="form-control" type="password" name="password" required autocomplete="current-password" />
                            </div>



                            <div class="remember">

                                <div class="checkbox">
                                    <label for="remember_me">
                                        <x-jet-checkbox name="remember" />
                                        <span>{{ __('remember_me') }}</span>
                                    </label>
                                </div>
                                @if (Route::has('password.request'))
                                    <a href="{{ route('password.request', app()->getLocale()) }}" class="forgot" data-bs-toggle="modal" data-bs-target="#restore-password">{{ __('forgot_your_password') }}</a>
                                @endif
                            </div>

                            <x-jet-button class="btn btn-lg btn-primary full-width">
                                {{ __('Login_to_you_account') }}
                            </x-jet-button>

                            <!-- <div class="or"></div>

                            <a href="#" class="btn btn-lg bg-facebook full-width btn-icon-left"><svg width="16" height="16"><use xlink:href="#olymp-facebook-icon"></use></svg>{{ __('login_with_facebook') }}</a>

                            <a href="#" class="btn btn-lg bg-twitter full-width btn-icon-left"><svg width="16" height="16"><use xlink:href="#olymp-twitter-icon"></use></svg>{{ __('login_with_twitter') }}</a> -->


                            <!-- <p>Don’t you have an account? <a href="#">{{ __('ask_to_join') }}</a> it’s really simple and you can start enjoing all the benefits!</p> -->
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="selaa-register" tabindex="-1" role="dialog" aria-labelledby="selaa-register" aria-hidden="true">
    <div class="modal-dialog window-popup restore-password-popup" role="document">
        <div class="modal-content">
            <a href="#" class="close icon-close" data-bs-dismiss="modal" aria-label="Close">
                <svg class="olymp-close-icon"><use xlink:href="#olymp-close-icon"></use></svg>
            </a>

            <div class="modal-header">
                <h6 class="title">{{ __('register_to') }}</h6>
            </div>

            <div class="modal-body">
                <form class="content" method="POST" action="{{ route('HomeRegister', app()->getLocale()) }}">
                @csrf
                <div class="row">
                    
                    <div class="col-12">
                        <div class="form-group label-floating">
                            <label class="control-label">{{ __('email') }}</label>
                            <x-jet-input class="form-control" type="email" name="email" :value="old('email')" autocomplete="email" required />
                        </div>
                    </div>

                    <div class="col col-6 col-xl-6 col-lg-6 col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                            <label class="control-label">{{ __('lives_in') }}</label>
                            <select class="form-select" name="country" :value="old('country')">
                                @foreach($countries as $country)
                                    <option value="{{ $country->id }}" {{ ($country->id == $user_country) ? 'selected' : '' }}>{{ $country->nicename }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="col col-6 col-xl-6 col-lg-6 col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                            <label class="control-label">{{ __('gender') }}</label>
                            <select class="form-select" name="gender">
                                <option value="male">{{ __('male') }}</option>
                                <option value="female">{{ __('female') }}</option>
                            </select>
                        </div>
                    </div>

                    <div class="col col-6 col-xl-6 col-lg-6 col-md-6 col-sm-12">
                        
                        <div class="form-group label-floating">
                            <label class="control-label">{{ __('Password') }}</label>
                            <x-jet-input class="form-control" type="password" name="password" required autocomplete="new-password" />
                        </div>
                    </div>
                    <div class="col col-6 col-xl-6 col-lg-6 col-md-6 col-sm-12">
                       <div class="form-group label-floating">
                            <label class="control-label">{{ __('Confirm Password') }}</label>
                            <x-jet-input class="form-control" type="password" name="password_confirmation" required autocomplete="new-password" />
                        </div>
                    </div>

                    <div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group label-floating">
                             <label class="control-label">{{ __('firstLogTo') }}</label>
                              <select class="form-group" name="part">
                                  <option value="tree_view">{{__('tree_builder')}}</option>
                                  <option value="wheel_view">{{__('wheel_life')}}</option>
                                  <option value="personality_view">{{__('personality')}}</option>
                              </select>
                            </div>
                        </div>

                        <div class="remember">
                            @if (Laravel\Jetstream\Jetstream::hasTermsAndPrivacyPolicyFeature())
                                <div class="mt-4">
                                    <x-jet-label for="terms">
                                        <div class="flex items-center">
                                            <x-jet-checkbox name="terms" />

                                            <div class="ml-2">
                                                @if(app()->getLocale() == 'ar')
                                                {!! __('أوافق علي  :terms_of_service و :privacy_policy', [
                                                        'terms_of_service' => '<a target="_blank" href="'.route('terms.show').'" class="text-sm text-gray-600 underline hover:text-gray-900">'.__('terms').'</a>',
                                                        'privacy_policy' => '<a target="_blank" href="'.route('policy.show').'" class="text-sm text-gray-600 underline hover:text-gray-900">'.__('privacy').'</a>',
                                                ]) !!}
                                                @else
                                                {!! __('I agree to the :terms_of_service and :privacy_policy', [
                                                        'terms_of_service' => '<a target="_blank" href="'.route('terms.show').'" class="text-sm text-gray-600 underline hover:text-gray-900">'.__('terms').'</a>',
                                                        'privacy_policy' => '<a target="_blank" href="'.route('policy.show').'" class="text-sm text-gray-600 underline hover:text-gray-900">'.__('privacy').'</a>',
                                                ]) !!}
                                                @endif
                                            </div>
                                        </div>
                                    </x-jet-label>
                                </div>
                            @endif
                        </div>

                        <x-jet-button class="btn btn-purple btn-lg full-width">
                            {{ __('register') }}
                        </x-jet-button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Window-popup Restore Password -->

<div class="modal fade" id="restore-password" tabindex="-1" role="dialog" aria-labelledby="restore-password" aria-hidden="true">
    <div class="modal-dialog window-popup restore-password-popup" role="document">
        <div class="modal-content">
            <a href="#" class="close icon-close" data-bs-dismiss="modal" aria-label="Close">
                <svg class="olymp-close-icon"><use xlink:href="#olymp-close-icon"></use></svg>
            </a>

            <div class="modal-header">
                <h6 class="title">Restore your Password</h6>
            </div>

            <div class="modal-body">
                <form  method="get">
                    <p>Enter your email and click the send code button. You’ll receive a code in your email. Please use that
                        code below to change the old password for a new one.
                    </p>
                    <div class="form-group label-floating">
                        <label class="control-label">Your Email</label>
                        <input class="form-control" placeholder="" type="email" value="james-spiegel@yourmail.com" autocomplete="email">
                    </div>
                    <button class="btn btn-purple btn-lg full-width">Send me the Code</button>
                    <div class="form-group label-floating">
                        <label class="control-label">Enter the Code</label>
                        <input class="form-control" placeholder="" type="text" value="" autocomplete="code">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Your New Password</label>
                        <input class="form-control" placeholder="" type="password" value="olympus" autocomplete="new-password" >
                    </div>
                    <button class="btn btn-primary btn-lg full-width">Change your Password!</button>
                </form>

            </div>
        </div>
    </div>
</div>

<!-- ... end Window-popup Restore Password -->


<!-- Window Popup Main Search -->

<div class="modal fade" id="main-popup-search" tabindex="-1" role="dialog" aria-labelledby="main-popup-search" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered window-popup main-popup-search" role="document">
        <div class="modal-content">
            <a href="#" class="close icon-close" data-bs-dismiss="modal" aria-label="Close">
                <svg class="olymp-close-icon"><use xlink:href="#olymp-close-icon"></use></svg>
            </a>
            <div class="modal-body">
                <form class="form-inline search-form" method="post">
                    <div class="form-group label-floating">
                        <label class="control-label">What are you looking for?</label>
                        <input class="bg-white form-control" placeholder="" type="text" value="" autocomplete="">
                    </div>

                    <button class="btn btn-purple btn-lg">Search</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- ... end Window Popup Main Search -->

<!-- Footer Full Width -->

<div class="footer footer-full-width" id="footer">
    <div class="container">
        <div class="row">
            <div class="col col-lg-8 col-md-8 col-sm-12 col-12">

                
                <!-- Widget About -->
                
                <div class="widget w-about">
                
                    <a href="#" class="logo">
                        <div class="img-wrap">
                            <img loading="lazy" src="{{ asset('img/default/tree_logo_small_orange.png') }}" alt="Olympus" width="60" height="60">
                        </div>
                        <div class="title-block">
                            <h6 class="logo-title">SELAA</h6>
                            <div class="sub-title">{{ __('triangle_social_network') }}</div>
                        </div>
                    </a>
                    <p>{{ __('site_word') }}</p>
                    <ul class="socials">
                        <li>
                            <a href="#">
                                <svg width="16" height="16"><use xlink:href="#olymp-facebook-square"></use></svg>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <svg width="16" height="16"><use xlink:href="#olymp-twitter-icon"></use></svg>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <svg width="16" height="16"><use xlink:href="#olymp-youtube-icon"></use></svg>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <svg width="16" height="16"><use xlink:href="#olymp-google-plus-g-icon"></use></svg>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <svg width="16" height="16"><use xlink:href="#olymp-instagram-icon"></use></svg>
                            </a>
                        </li>
                    </ul>
                </div>
                
                <!-- ... end Widget About -->

            </div>

            
            <div class="col col-lg-4 col-md-4 col-sm-12 col-12">

                
                <div class="widget w-list">
                    <h6 class="title">{{ __('triangle_social_network') }}</h6>
                    <ul>
                        <li>
                            <a href="{{ route('policy.show')}}">{{ __('privacy') }}</a>
                        </li>
                        <li>
                            <a href="{{ route('terms.show')}}">{{ __('terms') }}</a>
                        </li>
                    </ul>
                </div>

            </div>

            <div class="col col-lg-12 col-md-12 col-sm-12 col-12">

                
                <!-- SUB Footer -->
                
                <div class="sub-footer-copyright">
                    <span>
                        Copyright <a target="_blank" href="http://www.triangle4it.com/">Triangle 4 it soulutions</a> All Rights Reserved 2021
                    </span>
                </div>
                
                <!-- ... end SUB Footer -->

            </div>
        </div>
    </div>
</div>

<!-- ... end Footer Full Width -->





<a class="back-to-top" href="#">
    <svg class="back-icon" width="14" height="18"><use xlink:href="#olymp-back-to-top"></use></svg>
</a>



<!-- JS Scripts -->
<script src="{{ asset('js/jQuery/jquery-3.5.1.min.js') }}"></script>

<script src="{{ asset('js/libs/jquery.mousewheel.min.js') }}"></script>
<!-- <script src="{{ asset('js/libs/perfect-scrollbar.min.js') }}"></script> -->
<script src="{{ asset('js/libs/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ asset('js/libs/material.min.js') }}"></script>
<script src="{{ asset('js/libs/selectize.min.js') }}"></script>
<script src="{{ asset('js/libs/swiper.jquery.min.js') }}"></script>
<script src="{{ asset('js/libs/moment.min.js') }}"></script>
<script src="{{ asset('js/libs/daterangepicker.min.js') }}"></script>
<!-- <script src="{{ asset('js/libs/isotope.pkgd.min.js') }}"></script> -->
<!-- <script src="{{ asset('js/libs/ajax-pagination.min.js') }}"></script> -->
<!-- <script src="{{ asset('js/libs/jquery.magnific-popup.min.js') }}"></script> -->
<script src="{{ asset('js/libs/aos.min.js') }}"></script>

<script src="{{ asset('js/main.js') }}"></script>
<script src="{{ asset('js/libs-init/libs-init.js') }}"></script>

<script src="{{ asset('Bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
<script type="text/javascript">
    
function checkLoginState() {
  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });
}

$(function () {
    $('.genealogy-tree ul').hide();
    $('.genealogy-tree>ul').show();
    $('.genealogy-tree ul.active').show();
    $('.genealogy-tree li').on('click', function (e) {
        var children = $(this).find('> ul');
        if (children.is(":visible")) children.hide('fast').removeClass('active');
        else children.show('fast').addClass('active');
        e.stopPropagation();
    });
});
</script>
<!-- SVG icons loader -->
<script src="{{ asset('js/svg-loader.js') }}"></script>
<!-- /SVG icons loader -->
<!-- <script src="{{ asset('assets/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/bs5-intro-tour.js') }}"></script>
    <script>
        var steps = [{
            title: "{{ __('welcome') }}",
            content: "<p>{{ __('welcome_help') }}</p>"
        }, {
            id: "login-btn",
            content: "<p>{{ __('login_help') }}</p>"
        }, {
            id: "register-btn",
            content: "<p>{{ __('register_help') }}</p>"
        }];
        var tour = new Tour(steps, {
            // Uncomment to change button translations
            // translations: {
            //     next: "Dalej",
            //     previous: "Wróć",
            //     finish: "Zakończ"
            // }
        });


        document.getElementById("helpMe").addEventListener("click", function() {
            tour.show();
        });
    </script> -->
</body>
</html>