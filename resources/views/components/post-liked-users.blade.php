<div>
    <ul class="friends-harmonic">
        @foreach($liked_users as $liked_user)
        <li>
            <a href="#">
                <img loading="lazy" src="{{ asset($liked_user->user_photo) }}" alt="friend" width="28" height="28">
            </a>
        </li>
        @endforeach
    </ul>

    <div class="names-people-likes">
        {{ isset($liked_users[0]) ? $liked_users[0]->user_name : '' }}
        {{ isset($liked_users[1]) ? ',' . $liked_users[1]->user_name : '' }}
        @if($post->likes_count - 2 > 0)<br>{{ $post->likes_count - 2 }} more liked this @endif
    </div>
</div>
