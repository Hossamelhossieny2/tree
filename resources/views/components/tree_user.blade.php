<div class="member-view-box">
    <div class="member-image">
        @if(!empty($user['image']))
        <img src="{{ $user['image'] }}" alt="Member">
        @endif

        @if(!empty($user['name']))
        <div class="member-details">
            <h3>{{ $user['name'] }}</h3>
        </div>
        @endif
    </div>
</div>