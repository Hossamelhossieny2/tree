
<div class="card2 card-has-bg click-col">
    <div class="card-img-overlay d-flex flex-column ">
        <div class="card-body text-white">
           <small><i class="far fa-clock"></i> {{ $child->birth_date ?? 'unknown birth' }}</small>
        </div>
        <div class="card-footer" style="border: 0!important;height: 122px;background-color: transparent;!important;">
            <div class="media" style="border: 0!important;">
                    @if($child->profile_photo_path)
                      <img class="mr-3 rounded-circle border-2" src="{{ asset($child->profile_photo_path) }}" alt="Generic placeholder image" style="width:60px;height:60px;">
                      @else
                    <img class="mr-3 rounded-circle border-2" src="{{ asset('img/default/user_'.$child->gender.'.png') }}" alt="Generic placeholder image" style="width:60px;height:60px;">
                      @endif

                <div class="media-body">
                    <h4 class="my-0 d-block">{{$child->name}}</h4>
                     <small>
                        @if(!empty($child->death_date))
                            <span class="badge badge-die">departed</span>
                        @else
                            <span class="badge badge-alive">alive</span>
                        @endif
                        <br><img id='fag' src="{{ asset('img/flag/'.strtolower($child->iso).'.svg')}}" style="width: 40px!important;">
                     </small>
                </div>
            </div>
        </div>
        <small class="pt-4 text-white">@if(!empty($child->phone_number)) <a href="tel:{{ '00'.$child->phonecode.''.substr($child->phone_number,1) }}"><i class="far fa-phone"></i></a> @endif @if(!empty($child->phone_number)){{ '+'.$child->phonecode.''.substr($child->phone_number,1) }} @else mobile null @endif</small>
    </div>
</div>

