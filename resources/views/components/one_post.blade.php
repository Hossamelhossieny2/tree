<div>
    <!-- Comments -->
    <ul class="comments-list">
        <article class="hentry post">

            <div class="post__author author vcard inline-items">
                @if($post->user->profile_photo_path)
                    <img loading="lazy" src="{{ asset($post->user->profile_photo_path) }}" width="36" height="36" alt="{{ $post->user->name ?? '' }}">
                @else
                    <img loading="lazy" src="{{ asset('img/default/user_'.$post->user->gender.'.png')}}" width="36" height="36" alt="{{ $post->user->name ?? '' }}">
                @endif
                <div class="author-date">
                    <a class="h6 post__author-name fn" href="#">{{ $post->user->name ?? '  ' }}</a>
                    <div class="post__date">
                        <time class="published" datetime="{{$post->created_at}}">
                            <?php echo \Carbon\Carbon::parse($post->created_at)->shortRelativeDiffForHumans() ?>
                        </time>
                    </div>
                </div>

                <div class="more">
                    <img src="{{ asset('img/flag/'.strtolower($post->user->country->iso.'.svg'))}}" width="16px" />
                </div>

            </div>

            @if($post->post_pic != 'null' && $post->post_vid == '')
                <div class="post-thumb">
                    {{-- <img loading="lazy" src="{{ asset('uploads/gallery_uploads/'.$post->post_pic) }}" alt="author" width="768" height="441"> --}}
                    <img loading="lazy" src="{{ asset($post->post_pic) }}" alt="author" style="width:30% !important"/>
                </div>
            @endif

            @if($post->post_vid != '')
                <div class="post-thumb">
                    <div id="player" data-plyr-provider="youtube" data-plyr-embed-id="{{ $post->post_vid }}" style="width: 100%;"></div>
                    {{-- <iframe width="100%" height="600" src="https://www.youtube.com/embed/{{$post->post_vid}}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> --}}
                </div>
            @endif

            <p>{{ $post->post }}</p>

            <div class="post-additional-info inline-items">
                <a target="_blank" href="https://web.whatsapp.com/send?phone=201001191955&text={{__('try_site')}}">
                        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/WhatsApp_logo-color-vertical.svg/2048px-WhatsApp_logo-color-vertical.svg.png" title="Invite via WhatsApp" width="40" /></a>

                <a href="#" class="post-add-icon inline-items" wire:click="toggleLikePost({{ $post->id }})">
                    <svg class="olymp-heart-icon">
                        <use xlink:href="#olymp-heart-icon"></use>
                    </svg>
                    <span>{{ $post->likes_count }}</span>
                </a>

                <x-post-liked-users :post="$post" />

                <div class="comments-shared">
                    <a href="#" class="post-add-icon inline-items">
                        <svg class="olymp-speech-balloon-icon">
                            <use xlink:href="#olymp-speech-balloon-icon"></use>
                        </svg>
                        <span>{{ $post->comments_count }}</span>
                    </a>

                </div>


            </div>

            
            <div class="control-block-button post-control-button">
                @if($top == '1')
                <a href="#" class="btn btn-control featured-post">
                    <svg class="olymp-trophy-icon">
                        <use xlink:href="#olymp-trophy-icon"></use>
                    </svg>
                </a>
                @endif

                @if($like == '1')
                <a href="#" class="btn btn-control">
                    <svg class="olymp-like-post-icon">
                        <use xlink:href="#olymp-like-post-icon"></use>
                    </svg>
                </a>
                @endif
            </div>
            

        </article>
        <li class="comment-item has-children">
            <ul class="children">
                @foreach($post->comments as $comment)
                    <li class="comment-item">
                        <div class="post__author author vcard inline-items">
                            @if($comment->user->profile_photo_path)
                                <img loading="lazy" src="{{ asset($comment->user->profile_photo_path) }}" alt="{{$comment->user->name}} pic" width="36" height="36">
                            @else
                                <img loading="lazy" src="{{ asset('img/default/user_'.$comment->user->gender.'.png')}}" alt="{{$comment->user->name}} pic" width="36" height="36">
                            @endif

                            <div class="author-date">
                                <a class="h6 post__author-name fn" href="#">{{$comment->user->name}}</a>
                                <div class="post__date">
                                    <time class="published" datetime="{{$comment->created_at}}">
                                        <?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($comment->created_at))->diffForHumans() ?>
                                    </time>
                                </div>
                            </div>

                            <a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="#olymp-three-dots-icon"></use></svg></a>

                        </div>

                        <p>{{$comment->comment}}</p>

                        <a href="#" class="post-add-icon inline-items">
                            <svg class="olymp-heart-icon"><use xlink:href="#olymp-heart-icon"></use></svg>
                            <span>2</span>
                        </a>
                        <a href="#" class="reply">Reply</a>
                    </li>
                @endforeach
            </ul>
        </li>
    </ul>
    <!-- Comment Form  -->
    <form class="comment-form" wire:submit.prevent="addComment({{$post->id}})" method="post">
        <div class="post__author author vcard inline-items">
            @if($user->profile_photo_path)
                <img loading="lazy" src="{{ asset($user->profile_photo_path) }}" width="36" height="36" alt="{{ $user->name ?? '' }}">
            @else
                <img loading="lazy" src="{{ asset('img/default/user_'.$user->gender.'.png')}}" width="36" height="36" alt="{{ $user->name ?? '' }}">
            @endif
            <div class="form-group with-icon-right ">
                <textarea class="form-control" wire:model.defer="commentBody" placeholder="{{__('replay')}} ..." required=""></textarea>
            
            </div>
        </div>
        <button class="btn btn-md-2 btn-primary">{{__('post_replay')}}</button>
    </form>
    <!-- ... end Comment Form  -->
</div>
