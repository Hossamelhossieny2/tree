<div class="top-header-author">
    <a href="#" class="author-thumb">
        @if(Auth::user()->profile_photo_path)
            <img loading="lazy" src="{{ asset(Auth::user()->profile_photo_path) }}" alt="{{ Auth::user()->name ?? '' }}" width="124" height="124"/>
        @else
            <img loading="lazy" src="{{ asset('img/default/user_'.Auth::user()->gender.'.png')}}" alt="add mother pic" width="124" height="124"/>
        @endif
    </a>
    <div class="author-content">
        <a href="#" class="h4 author-name">{{ Auth::user()->name }}</a>
        <div class="country"><img src="{{ asset('img/flag/'.strtolower(auth()->user()->country->iso.'.svg'))}}" width="16px" /> {{ auth()->user()->country->name }}</div>
    </div>
</div>