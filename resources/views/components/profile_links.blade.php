<div class="row">
    <div class="col col-lg-5 col-md-5 col-sm-12 col-12">
        <ul class="profile-menu">
            <li>
                <a href="{{ route('member_view', app()->getLocale()) }}" @if($act==6)class="active"@endif>{{ __('members') }}</a>
            </li>
            <li>
                <a href="{{ route('my_all_shares',[ app()->getLocale(),0]) }}" @if($act==1)class="active"@endif>{{ __('my_all_shares') }}</a>
            </li>
            <li>
                <a href="{{ route('my_choice_shares', app()->getLocale()) }}" @if($act==2)class="active"@endif>{{ __('my_choice_shares') }}</a>
            </li>
        </ul>
    </div>
    <div class="col col-lg-5 ms-auto col-md-5 col-sm-12 col-12">
        <ul class="profile-menu">
            <li>
                <a href="{{ route('my_photos', app()->getLocale())}}" @if($act==5)class="active"@endif>{{ __('photos') }}</a>
            </li>
            <li>
                <a href="{{ route('not_view', app()->getLocale())}}" @if($act==7)class="active"@endif>{{ __('notifications') }} (<font @if(count($Notif) > 0) color="red" @endif>{{ arabic_w2e(count($Notif)) }}</font>)</a>
            </li>
            <li>
                <a href="{{ route('msg_view', app()->getLocale()) }}" @if($act==4)class="active"@endif>{{ __('messages') }} (<font @if(count($ChatMsgTo) > 0) color="red" @endif>{{ arabic_w2e(count($ChatMsgTo)) }}</font>)</a>
            </li>
        </ul>
    </div>
</div>