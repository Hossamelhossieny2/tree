
<!-- Main Header Groups -->

<div class="main-header">
    <div class="content-bg-wrap bg-group" style="background-image: asset('img/bg-group.png');"></div>
    <div class="container">
        <div class="row">
            <div class="col col-lg-8 m-auto col-md-8 col-sm-12 col-12">
                <div class="main-header-content">
                    <h1>Stats and Analytics</h1>
                    <p>Welcome to your stats and analytics dashboard! Here you’l see all your profile stats like: visits,
     new friends, average comments, likes, social media reach, annual graphs, and much more!</p>
                </div>
            </div>
        </div>
    </div>

    <img loading="lazy" class="img-bottom" src="{{ asset('img/group-bottom.webp') }}" alt="friends" width="1087" height="148">
</div>

<!-- ... end Main Header Groups -->

