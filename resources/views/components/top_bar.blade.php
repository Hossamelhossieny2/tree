<header class="header" id="site-header">

    <div class="page-title">
        <h6>{{ __('big_family') }}</h6>
    </div>

    <div class="header-content-wrapper">

        <div class="control-block">

            <div class="control-icon more has-items">
                <img loading="lazy" src="{{ asset('img/default/'.app()->getLocale().'.png') }}" alt="language flag" width="90">

                <div class="more-dropdown more-with-triangle triangle-top-center" >
                    <ul>
                       <li @if (app()->getLocale() == "ar") class="bg-smoke" @endif class="text-center">
                            <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), ['ar']) }}">
                                <div class="author-thumb">
                                    <img loading="lazy" src="{{ asset('img/default/ar.png') }}" alt="language flag" width="34" height="34">
                                </div>
                                <div class="h4 un-read">
                                    اللغة العربية
                                </div>
                            </a>
                        </li>
                       <li @if (app()->getLocale() == "en") class="bg-smoke" @endif class="text-center">
                            <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), ['en']) }}">
                                <div class="author-thumb">
                                    <img loading="lazy" src="{{ asset('img/default/en.png') }}" alt="language flag" width="34" height="34">
                                </div>
                                <div class="h4 un-read">
                                    English language
                                </div>
                            </a>
                        </li>
                        <li @if (app()->getLocale() == "fr") class="bg-smoke" @endif class="text-center">
                            <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), ['fr']) }}">
                                <div class="author-thumb">
                                    <img loading="lazy" src="{{ asset('img/default/fr.png') }}" alt="language flag" width="34" height="34">
                                </div>
                                <div class="h4 un-read">
                                    langue française
                                </div>
                            </a>
                        </li>
                        <li @if (app()->getLocale() == "tr") class="bg-smoke" @endif class="text-center">
                            <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), ['tr']) }}">
                                <div class="author-thumb">
                                    <img loading="lazy" src="{{ asset('img/default/tr.png') }}" alt="language flag" width="34" height="34">
                                </div>
                                <div class="h4 un-read">
                                    Türk Dili
                                </div>
                            </a>
                        </li>
                        <li @if (app()->getLocale() == "ru") class="bg-smoke" @endif class="text-center">
                            <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), ['ru']) }}">
                                <div class="author-thumb">
                                    <img loading="lazy" src="{{ asset('img/default/ru.png') }}" alt="language flag" width="34" height="34">
                                </div>
                                <div class="h4 un-read">
                                    русский язык
                                </div>
                            </a>
                        </li>
                        <li @if (app()->getLocale() == "de") class="bg-smoke" @endif class="text-center">
                            <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), ['de']) }}">
                                <div class="author-thumb">
                                    <img loading="lazy" src="{{ asset('img/default/de.png') }}" alt="language flag" width="34" height="34">
                                </div>
                                <div class="h4 un-read">
                                    deutsche Sprache
                                </div>
                            </a>
                        </li>
                        <li @if (app()->getLocale() == "es") class="bg-smoke" @endif class="text-center">
                            <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), ['es']) }}">
                                <div class="author-thumb">
                                    <img loading="lazy" src="{{ asset('img/default/es.png') }}" alt="language flag" width="34" height="34">
                                </div>
                                <div class="h4 un-read">
                                    lengua española
                                </div>
                            </a>
                        </li>
                        <li @if (app()->getLocale() == "cn") class="bg-smoke" @endif class="text-center">
                            <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), ['cn']) }}">
                                <div class="author-thumb">
                                    <img loading="lazy" src="{{ asset('img/default/cn.png') }}" alt="language flag" width="34" height="34">
                                </div>
                                <div class="h4 un-read">
                                    中文
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                
            </div>

            <div class="control-icon more has-items">
                <svg class="olymp-chat---messages-icon"><use xlink:href="#olymp-chat---messages-icon"></use></svg>
                @if(count($ChatMsgTo) > 0)
                <div class="label-avatar bg-primary">{{ count($ChatMsgTo) }}</div>
                @endif
                <div class="more-dropdown more-with-triangle triangle-top-center">
                    <div class="ui-block-title ui-block-title-small">
                        <h6 class="title">{{ __('chat_msg') }}</h6>
                        <a href="{{ route('msg_view',app()->getLocale()) }}">{{ __('view_all_chats') }}</a>
                    </div>

                    <div class="mCustomScrollbar" data-mcs-theme="dark">
                        <ul class="notification-list chat-message">
                            @foreach($ChatMsgTo as $cm)

                            <li class="message-unread">
                                <div class="author-thumb">
                                    <img loading="lazy" src="img/avatar59-sm.webp" alt="author" width="34" height="34">
                                </div>
                                <div class="notification-event">
                                    <a href="#" class="h6 notification-friend">{{__('from')}} : {{ $cm['user']['name'] }}</a>
                                    <span class="chat-message-item">{{ $cm['body'] }} ...</span>
                                    <span class="notification-date"><time class="entry-date updated" datetime="{{ $cm->created_at }}">{{ \Carbon\Carbon::parse($cm->created_at)->shortRelativeDiffForHumans() }}</time></span>
                                </div>
                                <span class="notification-icon">
                                    <svg class="olymp-chat---messages-icon"><use xlink:href="#olymp-chat---messages-icon"></use></svg>
                                </span>
                                <div class="more">
                                    <svg class="olymp-three-dots-icon"><use xlink:href="#olymp-three-dots-icon"></use></svg>
                                </div>
                            </li>
                            @endforeach
                            
                        </ul>
                    </div>
                    @if(count($ChatMsgTo) > 0)
                    <a href="{{ route('msg_view',app()->getLocale() ) }}" class="view-all bg-purple">{{ __('view_all_msg') }}</a>
                    @else
                    <a href="#" class="view-all bg-purple">{{__('no_message')}}</a>
                    @endif
                </div>
            </div>

            <div class="control-icon more has-items">
                <svg class="olymp-thunder-icon"><use xlink:href="#olymp-thunder-icon"></use></svg>
                @if(count($Notif) > 0)
                <div class="label-avatar bg-primary">{{ count($Notif) }}</div>
                @endif

               

                <div class="more-dropdown more-with-triangle triangle-top-center">
                    <div class="ui-block-title ui-block-title-small">
                        <h6 class="title">{{ __('notifications') }}</h6>
                        <a href="{{ route('not_view',app()->getLocale()) }}">{{ __('view_all_not') }}</a>
                    </div>

                     <div class="mCustomScrollbar" data-mcs-theme="dark">
                        <ul class="notification-list chat-message">
                            @foreach($Notif as $cm)

                            <?php
                                $data = json_decode($cm['data']);
                                $thisUser = $cm->notifiable_type::find($data->from);
                            ?>
                            <li class="message-unread">
                                <a href="{{ route('user_shares',[app()->getLocale(),'x',$data->share_id]) }}">
                                <div class="author-thumb">
                                    @if(!empty($thisUser->profile_photo_path))
                                    <img loading="lazy" src="{{ asset($thisUser->profile_photo_path) }}" alt="{{ $thisUser->name ?? '' }}" width="34" height="34">
                                    @else
                                    <img loading="lazy" src="{{ asset('img/user_'.$thisUser->gender.'.png') }}" alt="{{ $thisUser->name ?? '' }}" width="34" height="34">
                                    @endif
                                </div>
                                <div class="notification-event">
                                    <a href="{{ route('user_shares',[app()->getLocale(),'x',$data->share_id]) }}" class="h6 notification-friend">{{__('from')}} : ( {{ $thisUser->name }} ) {{ $data->title }} </a>
                                    <span class="chat-message-item">{{ $data->desc }} ...</span>
                                    <span class="notification-date"><time class="entry-date updated" datetime="{{ $cm->created_at }}">{{ \Carbon\Carbon::parse($cm->created_at)->shortRelativeDiffForHumans() }}</time></span>
                                </div>
                            </a>
                            </li>

                            @endforeach
                            
                        </ul>
                    </div>
                    @if(count($Notif) > 0)
                    <a href="{{ route('not_view',app()->getLocale() ) }}" class="view-all bg-purple">{{ __('view_all_not') }}</a>
                    @else
                    <a href="#" class="view-all bg-purple">{{__('no_not')}}</a>
                    @endif
                </div>
            </div>

            <div class="author-page author vcard inline-items more">
                <div class="author-thumb">
                    @if(Auth::user()->profile_photo_path)
                    <img alt="author" src="{{ asset(Auth::user()->profile_photo_path) }}" width="36" height="36" class="avatar" alt="{{ $thisUser->name ?? '' }}" />
                    @else
                    <img alt="author" src="{{ asset('img/default/user_'.Auth::user()->gender.'.png') }}" width="36" height="36" style="background-color: white;" class="avatar" alt="{{ $thisUser->name ?? '' }}" />
                    @endif
                    <span class="icon-status online"></span>
                    <div class="more-dropdown more-with-triangle">
                        <div class="mCustomScrollbar" data-mcs-theme="dark">
                            <div class="ui-block-title ui-block-title-small">
                                <h6 class="title">{{ __('your_account') }}</h6>
                            </div>

                            <ul class="account-settings">
                                <li>
                                    <a href="{{ route('profile.show', app()->getLocale()) }}">
                                        <i class="far fa-id-badge fa-2x"></i>
                                        <span style="padding:0 20px;">{{ __('my_links') }}</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('my_all_shares',[ app()->getLocale(),0]) }}">
                                        <i class="far fa-bullhorn fa-2x"></i>
                                        <span style="padding:0 20px;">{{ __('my_all_shares') }}</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('my_choice_shares',[ app()->getLocale(),0]) }}">
                                        <i class="fas fa-bullhorn fa-2x"></i>
                                        <span style="padding:0 20px;">{{ __('my_choice_shares') }}</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('about_me', app()->getLocale()) }}">
                                        <i class="far fa-meh-rolling-eyes fa-2x"></i>
                                        <span style="padding:0 20px;">{{ __('about_me') }}</span>
                                    </a>
                                </li>
                                <li>
                                    <form method="POST" action="{{ route('logout', app()->getLocale()) }}">
                                       @csrf
                                        <a href="{{ route('logout', app()->getLocale()) }}" onclick="event.preventDefault(); this.closest('form').submit();">
                                        <i class="fas fa-sign-out-alt fa-2x"></i>
                                        <span style="padding:0 20px;">{{ __('logout') }}</span>
                                        </a> 
                                    </form>
                                </li>
                            </ul>

                            <div class="ui-block-title ui-block-title-small">
                                <li>
                                    <a href="{{ route('msg_view',app()->getLocale()) }}">
                                        <svg class="olymp-chat---messages-icon">
                                            <use xlink:href="#olymp-chat---messages-icon"></use>
                                        </svg>
                                        {{ __('chat_msg') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('not_view',app()->getLocale()) }}">
                                        <svg class="olymp-thunder-icon"><use xlink:href="#olymp-thunder-icon"></use></svg>
                                        {{ __('notifications') }}
                                    </a>
                                </li>
                            </div>

                            <ul>
                                <li>
                                    <a href="{{ route('terms.show') }}">
                                        <span>{{ __('terms') }}</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('policy.show') }}">
                                        <span>{{ __('privacy') }}</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('contact_us',app()->getLocale()) }}">
                                        <span>{{ __('contact') }}</span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
                <a href="#" class="author-name fn">
                    <div class="author-title">
                        {{ Auth::user()->name }} <svg class="olymp-dropdown-arrow-icon"><use xlink:href="#olymp-dropdown-arrow-icon"></use></svg>
                    </div>
                    <span class="author-subtitle">{{ __('triangle_family_app') }}</span>
                </a>
            </div>

        </div>
    
    </div>
    
</header>

<!-- ... end Header-BP -->

<!-- Responsive Header-BP -->

<header class="header header-responsive" id="site-header-responsive">

    <div class="header-content-wrapper">
        <ul class="nav nav-tabs mobile-notification-tabs" id="mobile-notification-tabs" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="request-tab" data-bs-toggle="tab" href="#request" role="tab" aria-controls="request" aria-selected="false">
                    <div class="control-icon has-items">
                        <img loading="lazy" src="{{ asset('img/default/'.app()->getLocale().'.png') }}" alt="language flag" width="60">
                    </div>
                </a>
            </li>

            <li class="nav-item" role="presentation">
                <a class="nav-link" id="chat-tab" data-bs-toggle="tab" href="#chat" role="tab" aria-controls="chat" aria-selected="false">
                    <div class="control-icon has-items">
                        <svg class="olymp-chat---messages-icon">
                            <use xlink:href="#olymp-chat---messages-icon"></use>
                        </svg>
                        @if(count($ChatMsgTo) > 0)
                            <div class="label-avatar bg-purple">{{ count($ChatMsgTo) }}</div>
                        @endif
                    </div>
                </a>
            </li>
        </ul>
    </div>

    <!-- Tab panes -->
    <div class="tab-content tab-content-responsive">

        <div class="tab-pane fade" id="request" role="tabpanel" aria-labelledby="request-tab">

            <div class="mCustomScrollbar" data-mcs-theme="dark">
                <ul>
                       <li @if (app()->getLocale() == "ar") class="bg-smoke" @endif class="text-center">
                            <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), ['ar']) }}">
                                <div class="author-thumb">
                                    <img loading="lazy" src="{{ asset('img/default/ar.png') }}" alt="language flag" width="34" height="34">
                                </div>
                                <div class="h4 un-read">
                                    اللغة العربية
                                </div>
                            </a>
                        </li>
                       <li @if (app()->getLocale() == "en") class="bg-smoke" @endif class="text-center">
                            <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), ['en']) }}">
                                <div class="author-thumb">
                                    <img loading="lazy" src="{{ asset('img/default/en.png') }}" alt="language flag" width="34" height="34">
                                </div>
                                <div class="h4 un-read">
                                    English language
                                </div>
                            </a>
                        </li>
                        <li @if (app()->getLocale() == "fr") class="bg-smoke" @endif class="text-center">
                            <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), ['fr']) }}">
                                <div class="author-thumb">
                                    <img loading="lazy" src="{{ asset('img/default/fr.png') }}" alt="language flag" width="34" height="34">
                                </div>
                                <div class="h4 un-read">
                                    langue française
                                </div>
                            </a>
                        </li>
                        <li @if (app()->getLocale() == "tr") class="bg-smoke" @endif class="text-center">
                            <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), ['tr']) }}">
                                <div class="author-thumb">
                                    <img loading="lazy" src="{{ asset('img/default/tr.png') }}" alt="language flag" width="34" height="34">
                                </div>
                                <div class="h4 un-read">
                                    Türk Dili
                                </div>
                            </a>
                        </li>
                        <li @if (app()->getLocale() == "ru") class="bg-smoke" @endif class="text-center">
                            <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), ['ru']) }}">
                                <div class="author-thumb">
                                    <img loading="lazy" src="{{ asset('img/default/ru.png') }}" alt="language flag" width="34" height="34">
                                </div>
                                <div class="h4 un-read">
                                    русский язык
                                </div>
                            </a>
                        </li>
                        <li @if (app()->getLocale() == "de") class="bg-smoke" @endif class="text-center">
                            <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), ['de']) }}">
                                <div class="author-thumb">
                                    <img loading="lazy" src="{{ asset('img/default/de.png') }}" alt="language flag" width="34" height="34">
                                </div>
                                <div class="h4 un-read">
                                    deutsche Sprache
                                </div>
                            </a>
                        </li>
                        <li @if (app()->getLocale() == "es") class="bg-smoke" @endif class="text-center">
                            <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), ['es']) }}">
                                <div class="author-thumb">
                                    <img loading="lazy" src="{{ asset('img/default/es.png') }}" alt="language flag" width="34" height="34">
                                </div>
                                <div class="h4 un-read">
                                    lengua española
                                </div>
                            </a>
                        </li>
                        <li @if (app()->getLocale() == "cn") class="bg-smoke" @endif class="text-center">
                            <a href="{{ route(\Illuminate\Support\Facades\Route::currentRouteName(), ['cn']) }}">
                                <div class="author-thumb">
                                    <img loading="lazy" src="{{ asset('img/default/cn.png') }}" alt="language flag" width="34" height="34">
                                </div>
                                <div class="h4 un-read">
                                    中文
                                </div>
                            </a>
                        </li>
                    </ul>
            </div>

        </div>

        <div class="tab-pane fade" id="chat" role="tabpanel" aria-labelledby="chat-tab">

            <div class="mCustomScrollbar" data-mcs-theme="dark">
                <div class="ui-block-title ui-block-title-small">
                    <h6 class="title">{{ __('chat_msg') }}</h6>
                    <a href="{{ route('msg_view',app()->getLocale()) }}">{{ __('view_all_chats') }}</a>
                </div>

                <ul class="notification-list chat-message">
                     @foreach($ChatMsgTo as $cm)
                    <li class="message-unread">
                        <div class="author-thumb">
                            <img loading="lazy" src="img/avatar59-sm.webp" alt="author" width="34" height="34">
                        </div>
                        <div class="notification-event">
                            <a href="#" class="h6 notification-friend">From : {{ $cm['user']['name'] }}</a>
                            <span class="chat-message-item">{{ $cm['body'] }} ...</span>
                            <span class="notification-date"><time class="entry-date updated" datetime="{{ $cm->created_at }}">{{ \Carbon\Carbon::parse($cm->created_at)->shortRelativeDiffForHumans() }}</time></span>
                        </div>
                        <span class="notification-icon">
                            <svg class="olymp-chat---messages-icon"><use xlink:href="#olymp-chat---messages-icon"></use></svg>
                        </span>
                        <div class="more">
                            <svg class="olymp-three-dots-icon"><use xlink:href="#olymp-three-dots-icon"></use></svg>
                        </div>
                    </li>
                    @endforeach
                </ul>

                <a href="{{ route('msg_view',app()->getLocale()) }}">{{ __('view_all_chats') }}</a>
            </div>

        </div>

    </div>
    <!-- ... end  Tab panes -->

</header>

