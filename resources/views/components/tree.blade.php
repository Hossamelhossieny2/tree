<div class="tree" id="FamilyTreeDiv">
    <ul>

        <li>
            {{--                father / mother location--}}
            <div>
                @if(!empty($mother))
                    <a href="{{url('/'.$mother->id)}}">
                        <span class="female">{{$mother->name}}</span>
                    </a>
                @else
                    <span class="female">Add Mother</span>
                @endif


                <span class="spacer"></span>

                @if(!empty($father))
                    <a href="{{url('/'.$father->id)}}">
                        <span class="male">{{$father->name}}</span>
                    </a>
                @else
                    <span class="male">Add Father</span>
                @endif

            </div>


            <ul>


                {{--                    sister location--}}
                @if(!empty($sisters))
                    @foreach($sisters as $sister)
                        <li>
                            <div>
                                <a href="{{url('/'.$sister->id)}}">
                                    <span class="female">{{$sister->name}}</span>
                                </a>
                            </div>
                        </li>
                    @endforeach
                @else
                    <li>
                        <div>
                            <span class="female">Add sister</span>
                        </div>
                    </li>
                @endif





                <li>
                    {{--                    my location--}}
                    <div>
                        <span class="me">{{$treeData['user']->name}}</span>
                    </div>

                    <ul>

                        {{--                            wife/husband location with children--}}

                        {{--                                    if wife or husbund--}}
                        @if($treeData['user']->gender == 'male')
                            @if(!empty($marriage))
                                @foreach($marriage as $wife)
                                    <li>
                                        <div>
                                            <a href="{{url('/'.$wife->id)}}">
                                                <span class="female">{{$wife->name}} </span>
                                            </a>
                                        </div>
                                        <ul>
                                            @if(!empty($wife->wife_children))
                                                @foreach($wife->wife_children as $child)
                                                    <li>
                                                        <div>
                                                            <a href="{{url('/'.$child->id)}}">
                                                                <span class="{{$child->gender}}">{{$child->name}}</span>
                                                            </a>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            @else
                                                <li>
                                                    <div>
                                                        <span class="male">add child</span>
                                                    </div>
                                                </li>
                                            @endif
                                        </ul>
                                    </li>
                                @endforeach
                            @else
                                <li>
                                    <div>
                                        <span class="female">add wife </span>
                                    </div>
                                </li>
                            @endif
                        @else
                            @if(!empty($marriage))
                                <li>
                                    <div>
                                        <a href="{{url('/'.$marriage->id)}}">
                                            <span class="male">{{$marriage->name}} </span>
                                        </a>
                                    </div>
                                    <ul>
                                        @if(!empty($marriage->husband_children))
                                            @foreach($marriage->husband_children as $child)
                                                <li>
                                                    <div>
                                                        <a href="{{url('/'.$child->id)}}">
                                                            <span class="{{$child->gender}}">{{$child->name}}</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            @endforeach
                                        @else
                                            <li>
                                                <div>
                                                    <span class="male">add child</span>
                                                </div>
                                            </li>
                                        @endif
                                    </ul>
                                </li>
                                @endif
                                @endif


                                </li>

                    </ul>

                </li>



                {{--                    brother location--}}
                @if(!empty($brothers))

                    @foreach($brothers as $brother)
                        <li>
                            <div>
                                <a href="{{url('/'.$brother->id)}}">
                                    <span class="male">{{$brother->name}}</span>
                                </a>
                            </div>
                        </li>
                    @endforeach

                @else
                    <li>
                        <div>
                            <span class="male">Add brother</span>
                        </div>
                    </li>
                @endif


            </ul>
        </li>

    </ul>

</div>
