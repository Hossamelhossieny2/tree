

<div class="fixed-sidebar left">
    <div class="fixed-sidebar-left sidebar--small" id="sidebar-left">

        <a href="#" class="logo js-sidebar-open">
            <div class="img-wrap">
                <img loading="lazy" src="{{ asset('img/default/tree_logo_small_white.png') }}" alt="Olympus" width="34" height="34">
            </div>
        </a>

        <div class="mCustomScrollbar" data-mcs-theme="dark">
            <ul class="left-menu">
                <li>
                    <a href="#" class="js-sidebar-open">
                        <i class="fas fa-expand-arrows-alt fa-2x"></i>
                    </a>
                </li>
                <li>
                     <a href="{{ route('family_dashboard', app()->getLocale()) }}" :active="request()->routeIs('family_dashboard', app()->getLocale())" title="{{ __('family_dashboard') }}">
                        <i class="fad fa-tachometer-alt-slow fa-2x"></i>
                    </a>
                </li>
                <li>
                    <a href="{{ route('family_posts', app()->getLocale()) }}" :active="request()->routeIs('family_posts', app()->getLocale())" title="{{ __('family_posts') }}">
                        <i class="fa fa-paper-plane fa-2x"></i>
                    </a>
                </li>
                <li>
                    <a href="{{ route('tree_view', app()->getLocale()) }}" :active="request()->routeIs('tree_view', app()->getLocale())" title="{{ __('create_tree') }}">
                        <i class="fab fa-pagelines fa-2x"></i>
                    </a>
                </li>
                <li>
                    <a href="{{ route('family_calender', app()->getLocale()) }}" :active="request()->routeIs('family_calender', app()->getLocale())" title="{{ __('family_calender') }}" >
                        <i class="far fa-calendar-alt fa-2x"></i>
                    </a>
                </li>
                <li>
                    <a href="{{ route('gallery_view', app()->getLocale()) }}" :active="request()->routeIs('gallery_view', app()->getLocale())" title="{{ __('family_gallery') }}">
                        <i class="fas fa-camera-retro fa-2x"></i>
                    </a>
                </li>

                 <li>
                    <a href="{{ route('family_print_father', app()->getLocale()) }}" :active="request()->routeIs('family_print_father', app()->getLocale())" title="{{ __('family_father_print') }}">
                        <i class="fas fa-address-card fa-2x"></i>
                    </a>
                </li>
                <li>
                    <a href="{{ route('family_print_mother', app()->getLocale()) }}" :active="request()->routeIs('family_print_mother', app()->getLocale())" title="{{ __('family_mother_print') }}">
                        <i class="far fa-address-card fa-2x"></i>
                    </a>
                </li>
                <li>
                    <a href="{{ route('user_shares',[app()->getLocale(),0,0]) }}" :active="request()->routeIs('user_shares', app()->getLocale())" title="{{ __('user_shares') }}">
                        <i class="far fa-users fa-2x"></i>
                    </a>
                </li>
                <li>
                    <a href="{{ route('wheel_view', app()->getLocale()) }}" :active="request()->routeIs('wheel_view', app()->getLocale())"  title="{{ __('your_init_chart') }}">
                         <i class="far fa-snowflake fa-2x"></i>
                    </a>
                </li>
                <li>
                    <a href="{{ route('personality_view', app()->getLocale()) }}" :active="request()->routeIs('personality_view', app()->getLocale())" title="{{ __('personality') }}">
                        <i class="fab fa-jenkins fa-2x"></i>
                        
                    </a>
                </li>
                 <li>
                    <a href="{{ route('quran.index', app()->getLocale()) }}" :active="request()->routeIs('quran.index', app()->getLocale())" title="{{ __('quran_memorize') }}">
                        <i class="fal fa-quran fa-2x"></i>
                        
                    </a>
                </li>
                <li>
                    <a href="{{ route('read_news', app()->getLocale()) }}" :active="request()->routeIs('read_news', app()->getLocale())"  title="{{ __('read_news') }}">
                         <i class="fa fa-newspaper fa-2x"></i>
                    </a>
                </li>

                 <li>
                    <a href="{{ route('get_help', app()->getLocale()) }}" :active="request()->routeIs('get_help', app()->getLocale())"  title="{{ __('help_videos') }}">
                         <i class="fa fa-question-circle fa-2x"></i>
                    </a>
                </li>
                
            </ul>
        </div>
    </div>

    <div class="fixed-sidebar-left sidebar--large" id="sidebar-left-1">
        <a href="#" class="logo js-sidebar-open">
            <div class="img-wrap">
                <img loading="lazy" src="{{ asset('img/default/tree_logo_small_white.png') }}" alt="{{ __('triangle_social_network') }}" width="34" height="34">
            </div>
            <div class="title-block">
                <h6 class="logo-title">{{ __('triangle_social_network') }}</h6>
            </div>
        </a>

        <div class="mCustomScrollbar" data-mcs-theme="dark">
            <ul class="left-menu">
                <li>
                    <a href="#" class="js-sidebar-open">
                        <i class="fas fa-compress-arrows-alt fa-2x"></i>
                        <span class="left-menu-title" style="padding:0 20px;">{{ __('collapse_Menu') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('family_dashboard', app()->getLocale()) }}" :active="request()->routeIs('family_dashboard', app()->getLocale())">
                        <i class="fad fa-tachometer-alt-slow fa-2x"></i>
                        <span class="left-menu-title" style="padding:0 20px;">{{ __('family_dashboard') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('family_posts', app()->getLocale()) }}" :active="request()->routeIs('family_posts', app()->getLocale())">
                        <i class="fa fa-paper-plane fa-2x"></i>
                        <span class="left-menu-title" style="padding:0 20px;">{{ __('family_posts') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('tree_view', app()->getLocale()) }}" :active="request()->routeIs('tree_view', app()->getLocale())">
                        <i class="fab fa-pagelines fa-2x"></i>
                        <span class="left-menu-title" style="padding:0 20px;">{{ __('create_tree') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('family_calender', app()->getLocale()) }}" :active="request()->routeIs('family_calender', app()->getLocale())">
                        <i class="far fa-calendar-alt fa-2x"></i>
                        <span class="left-menu-title" style="padding:0 20px;">{{ __('family_calender') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('gallery_view', app()->getLocale()) }}" :active="request()->routeIs('gallery_view', app()->getLocale())">
                        <i class="fas fa-camera-retro fa-2x"></i>
                        <span class="left-menu-title" style="padding:0 20px;">{{ __('family_gallery') }}</span>
                    </a>
                </li>
                 <li>
                    <a href="{{ route('family_print_father', app()->getLocale()) }}">
                        <i class="fas fa-address-card fa-2x"></i>
                        <span class="left-menu-title" style="padding:0 20px;">{{ __('family_father_print') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('family_print_mother', app()->getLocale()) }}">
                        <i class="far fa-address-card fa-2x"></i>
                        <span class="left-menu-title" style="padding:0 20px;">{{ __('family_mother_print') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('user_shares',[app()->getLocale(),0,0]) }}">
                        <i class="far fa-users fa-2x"></i>
                        <span class="left-menu-title" style="padding:0 20px;">{{ __('user_shares') }}</span>
                    </a>
                </li>
                        
                <li>
                    <a href="{{ route('wheel_view', app()->getLocale()) }}" :active="request()->routeIs('wheel_view', app()->getLocale())">
                        <i class="far fa-snowflake fa-2x"></i>
                        <span class="left-menu-title" style="padding:0 20px;">{{__('your_init_chart')}}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('personality_view', app()->getLocale()) }}" :active="request()->routeIs('personality_view', app()->getLocale())">
                        <i class="fab fa-jenkins fa-2x"></i>
                        <span class="left-menu-title" style="padding:0 20px;">{{ __('personality') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('quran.index', app()->getLocale()) }}" :active="request()->routeIs('quran.index', app()->getLocale())">
                        <i class="fal fa-quran fa-2x"></i>
                        <span class="left-menu-title" style="padding:0 20px;">{{ __('quran_memorize') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('read_news', app()->getLocale()) }}" :active="request()->routeIs('read_news', app()->getLocale())"  title="{{ __('read_news') }}">
                         <i class="fa fa-newspaper fa-2x"></i>
                         <span class="left-menu-title" style="padding:0 20px;">{{__('read_news')}}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('get_help', app()->getLocale()) }}" :active="request()->routeIs('get_help', app()->getLocale())"  title="{{ __('help_videos') }}">
                         <i class="fa fa-question-circle fa-2x"></i>
                         <span class="left-menu-title" style="padding:0 20px;">{{__('help_videos')}}</span>
                    </a>
                </li>

            </ul>

            <div class="profile-completion">
            <?php 
            $user_profile_info = DB::table('user_infos')->where('user_id',auth()->id())->first();
            $user_profile_edu = DB::table('user_education')->where('user_id',auth()->id())->count();
            $rat = 0;

                if(!empty($user_profile_info->bio)){$rat += 5;}
                if(!empty($user_profile_info->hoppy)){$rat += 5;}
                if(!empty($user_profile_info->tv)){$rat += 5;}
                if(!empty($user_profile_info->movies)){$rat += 5;}
                if(!empty($user_profile_info->games)){$rat += 5;}
                if(!empty($user_profile_info->music)){$rat += 5;}
                if(!empty($user_profile_info->books)){$rat += 5;}
                if(!empty($user_profile_info->writer)){$rat += 5;}
                if(!empty($user_profile_info->bith_place)){$rat += 5;}
                if(!empty($user_profile_info->lives_in)){$rat += 5;}
                if(!empty($user_profile_info->website)){$rat += 5;}
                if(!empty($user_profile_info->facebook)){$rat += 5;}
                if(!empty($user_profile_info->twitter)){$rat += 5;}
                if(!empty($user_profile_info->instgram)){$rat += 5;}
                if(!empty($user_profile_info->ticktok)){$rat += 5;}
                if(!empty(auth()->user()->email)){$rat += 5;}
                if(!empty(auth()->user()->birth_date)){$rat += 5;}
                if(!empty(auth()->user()->profile_photo_path)){$rat += 5;}
                if($user_profile_edu > 2){$rat += 10;}
            ?>
                <div class="skills-item">
                    <div class="skills-item-info">
                        <span class="skills-item-title">{{ __('profile_completion') }}</span>
                        <span class="skills-item-count"><span class="count-animate" data-speed="1000" data-refresh-interval="50" data-to="{{$rat}}" data-from="0"></span><span class="units">{{$rat}}%</span></span>
                    </div>
                    <div class="skills-item-meter">
                        <span class="skills-item-meter-active bg-primary" style="width: {{$rat}}%"></span>
                    </div>
                </div>


                <span>{{ __('complete') }} <a href="#">{{ __('your_profile') }}</a> {{ __('pepole_know_you') }}</span>

            </div>
        </div>
    </div>
</div>

<!-- ... end Fixed Sidebar Left -->


<!-- Fixed Sidebar Left -->

<div class="fixed-sidebar left fixed-sidebar-responsive">

    <div class="fixed-sidebar-left sidebar--small" id="sidebar-left-responsive">
        <a href="#" class="logo js-sidebar-open">
            <img loading="lazy" src="{{ asset('img/default/tree_logo_small_white.png') }}" alt="{{ __('triangle_social_network') }}" width="34" height="34">
        </a>

    </div>

    <div class="fixed-sidebar-left sidebar--large" id="sidebar-left-1-responsive">
        <a href="#" class="logo js-sidebar-open">
            <div class="img-wrap">
                <img loading="lazy" src="{{ asset('img/default/tree_logo_small_white.png') }}" alt="{{ __('triangle_social_network') }}" width="34" height="34">
            </div>
            <div class="title-block">
                <h6 class="logo-title">{{ __('triangle_social_network') }}</h6>
            </div>
        </a>

        <div class="mCustomScrollbar" data-mcs-theme="dark">

            <div class="control-block">
                <div class="author-page author vcard inline-items">
                    <div class="author-thumb">
                        @if(Auth::user()->profile_photo_path)
                    <img alt="author" src="{{ asset(Auth::user()->profile_photo_path) }}" width="36" height="36" class="avatar">
                    @else
                    <img alt="author" src="{{ asset('img/default/user_'.Auth::user()->gender.'.png') }}" width="36" height="36" style="background-color: white;" class="avatar">
                    @endif
                        <span class="icon-status online"></span>
                    </div>
                    <a href="#" class="author-name fn">
                        <div class="author-title">
                            {{ Auth::user()->name }} <svg class="olymp-dropdown-arrow-icon"><use xlink:href="#olymp-dropdown-arrow-icon"></use></svg>
                        </div>
                        <span class="author-subtitle">SPACE COWBOY</span>
                    </a>
                </div>
            </div>

            <ul class="left-menu">
                <li>
                    <a href="#" class="js-sidebar-open">
                        <i class="fas fa-arrows-alt-h fa-2x"></i>
                        <span class="left-menu-title"  style="padding:0 20px;">{{ __('collapse_Menu') }}</span>
                    </a>
                </li>
                
                <li>
                    <a href="{{ route('family_dashboard', app()->getLocale()) }}" :active="request()->routeIs('family_dashboard', app()->getLocale())">
                        <i class="fad fa-tachometer-alt-slow fa-2x"></i>
                        <span class="left-menu-title"  style="padding:0 20px;">{{ __('family_dashboard') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('family_posts', app()->getLocale()) }}" :active="request()->routeIs('family_posts', app()->getLocale())">
                        <i class="fa fa-paper-plane fa-2x"></i>
                        <span class="left-menu-title"  style="padding:0 20px;">{{ __('family_posts') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('tree_view', app()->getLocale()) }}" :active="request()->routeIs('tree_view', app()->getLocale())">
                        <i class="fab fa-pagelines fa-2x"></i>
                        <span class="left-menu-title"  style="padding:0 20px;">{{ __('create_tree') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('family_calender', app()->getLocale()) }}" :active="request()->routeIs('family_calender', app()->getLocale())">
                        <i class="far fa-calendar-alt fa-2x"></i>
                        <span class="left-menu-title"  style="padding:0 20px;">{{ __('family_calender') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('gallery_view', app()->getLocale()) }}" :active="request()->routeIs('gallery_view', app()->getLocale())">
                        <i class="fas fa-camera-retro fa-2x"></i>
                        <span class="left-menu-title"  style="padding:0 20px;">{{ __('family_gallery') }}</span>
                    </a>
                </li>
                 <li>
                    <a href="{{ route('family_print_father', app()->getLocale()) }}" :active="request()->routeIs('family_print_father', app()->getLocale())">
                        <i class="fas fa-address-card fa-2x"></i>
                        <span class="left-menu-title"  style="padding:0 20px;">{{ __('family_father_print') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('family_print_mother', app()->getLocale()) }}" :active="request()->routeIs('family_print_mother', app()->getLocale())">
                        <i class="far fa-address-card fa-2x"></i>
                        <span class="left-menu-title"  style="padding:0 20px;">{{ __('family_mother_print') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('user_shares',[app()->getLocale(),0,0]) }}" :active="request()->routeIs('user_shares', app()->getLocale())">
                        <i class="far fa-users fa-2x"></i>
                        <span class="left-menu-title"  style="padding:0 20px;">{{ __('user_shares') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('wheel_view', app()->getLocale()) }}" :active="request()->routeIs('wheel_view', app()->getLocale())">
                        <i class="far fa-snowflake fa-2x"></i>
                        <span class="left-menu-title"  style="padding:0 20px;">{{ __('your_init_chart') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('personality_view', app()->getLocale()) }}" :active="request()->routeIs('personality_view', app()->getLocale())">
                        <i class="fab fa-jenkins fa-2x"></i>
                        <span class="left-menu-title" style="padding:0 20px;">{{ __('personality') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('quran.index', app()->getLocale()) }}" :active="request()->routeIs('quran.index', app()->getLocale())">
                        <i class="fal fa-quran fa-2x"></i>
                        <span class="left-menu-title" style="padding:0 20px;">{{ __('quran_memorize') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('read_news', app()->getLocale()) }}" :active="request()->routeIs('read_news', app()->getLocale())">
                        <i class="fa fa-newspaper fa-2x"></i>
                        <span class="left-menu-title"  style="padding:0 20px;">{{ __('read_news') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('get_help', app()->getLocale()) }}" :active="request()->routeIs('get_help', app()->getLocale())">
                         <i class="fa fa-question-circle fa-2x"></i>
                         <span class="left-menu-title" style="padding:0 20px;">{{__('help_videos')}}</span>
                    </a>
                </li>
            </ul>

            <div class="ui-block-title ui-block-title-small">
                <h6 class="title">{{ __('your_account') }}</h6>
            </div>

            <ul class="account-settings">
                <li>
                    <a href="{{ route('profile.show', app()->getLocale()) }}">
                        <i class="far fa-id-badge fa-2x"></i>
                        <span style="padding:0 20px;">{{ __('profile') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('my_all_shares', app()->getLocale()) }}">
                        <i class="far fa-bullhorn fa-2x"></i>
                        <span style="padding:0 20px;">{{ __('my_all_shares') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('about_me', app()->getLocale()) }}">
                        <i class="far fa-meh-rolling-eyes fa-2x"></i>
                        <span style="padding:0 20px;">{{ __('about_me') }}</span>
                    </a>
                </li>
                <li>
                    <form method="POST" action="{{ route('logout', app()->getLocale()) }}">
                       @csrf
                        <a href="{{ route('logout', app()->getLocale()) }}" onclick="event.preventDefault(); this.closest('form').submit();">
                       <i class="fas fa-sign-out-alt fa-2x"></i>
                       <span style="padding:0 20px;">{{ __('logout') }}</span>
                        </a> 
                    </form>
                </li>
            </ul>

            <div class="ui-block-title ui-block-title-small">
                <h6 class="title">{{ __('about_site') }}</h6>
            </div>

            <ul class="about-olympus">
                <li>
                    <a href="{{ route('terms.show') }}">
                        <i class="fab fa-readme"></i>
                        <span style="padding:0 20px;">{{ __('terms') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('policy.show') }}">
                        <i class="fas fa-user-secret"></i>
                        <span style="padding:0 20px;">{{ __('privacy') }}</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fas fa-file-signature"></i>
                        <span style="padding:0 20px;">{{ __('contact') }}</span>
                    </a>
                </li>
            </ul>

        </div>
    </div>
</div>

