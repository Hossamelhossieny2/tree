<div id="tree" class="mx-auto my-5" style="margin-top:0 !important;width: 100%;" >
<div class="white rgba-white-strong rounded">

@if(!empty($first))

    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
           {{strtoupper($person->name)}} Tree
        </h2>
    </x-slot>


<div class="tree" style="background-image: url( {{ asset('img/pattern/pattern5.png') }} )">
<div class="py-12 text-center" id="hubble-container"> 
@endif

@if(!empty($first))
<ul class="tree" id="pepole-tree">
	<li style="padding:0 20px">
		<div class="card2 card-has-bg click-col" id="gf">
            <div class="card-img-overlay d-flex flex-column ">
                <div class="card-body text-white">
                   <small><i class="far fa-clock"></i> {{ $person->birth_date ?? 'unknown birth' }}</small>
                </div>
                <div class="card-footer" style="border: 0!important;height: 122px;background-color: transparent;!important;">
                    <div class="media" style="border: 0!important;">
                            @if($person->profile_photo_path)
                              <img class="mr-3 rounded-circle border-2" src="{{ asset($person->profile_photo_path) }}" alt="Generic placeholder image" style="width:60px;height:60px;">
                              @else
                            <img class="mr-3 rounded-circle border-2" src="{{ asset('img/default/user_'.$person->gender.'.png') }}" alt="Generic placeholder image" style="width:60px;height:60px;">
                              @endif
                        <div class="media-body">
                            <h4 class="my-0 d-block">{{ $person->name }}</h4>
                             <small>
                                @if(!empty($person->death_date))
                                    <span class="badge badge-die">departed</span>
                                @else
                                    <span class="badge badge-alive">alive</span>
                                @endif
                                <br><img id='fag' src="{{ asset('img/flag/'.strtolower($person->iso).'.svg')}}" style="width: 40px!important;">
                             </small>
                        </div>
                    </div>
                </div>
                <small class="pt-4 text-white">@if(!empty($person->phone_number)) <a href="tel:{{ '00'.$person->phonecode.''.substr($person->phone_number,1) }}"><i class="far fa-phone"></i></a> @endif @if(!empty($person->phone_number)){{ '+'.$person->phonecode.''.substr($person->phone_number,1) }} @else mobile null @endif</small>
            </div>
        </div>

@endif
@if(!empty($tree[$person->id]['marriges']))
		
		@if(!empty($first))
	    	
	    @else
	    	<ul>
    	@endif

        @foreach($tree[$person->id]['marriges'] as $marrige)
        @if(!empty($first))
	    	
	    @else
	    	<li style="padding:0 20px;">
    	@endif
           
           		<x-wife :wife="$marrige" ></x-wife>

                @if(!empty($tree[$person->id]['children'][$marrige->marrige_id]))
                    <ul>
                    @foreach($tree[$person->id]['children'][$marrige->marrige_id] as $child)
                        <li>
                        	<x-child :child="$child" ></x-child>
                        
                        	<x-tree_member :person="$child" :tree="$tree" ></x-tree_member>                        	
                        </li>
                    @endforeach
                    </ul>
                @endif
            </li>
        @endforeach
	</ul>
@endif

@if(!empty($first))
</div>
</div>
@endif

</div>
</div>
