<!DOCTYPE html>
<html lang="en">
<head>

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Required meta tags always come first -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<!-- Theme Font -->
	<link rel="preload" type="text/css" href="{{ asset('css/theme-font.min.css') }}" as="style">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" type="text/css" href="{{ asset('Bootstrap/dist/css/bootstrap.css') }}">

	<!-- Main Styles CSS -->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/main.min.css') }}">

	<!-- Main RTL CSS -->
	<!--<link rel="stylesheet" type="text/css" href="css/rtl.min.css">-->

</head>
<body class="body-bg-white">


<!-- Preloader -->

<div id="hellopreloader">
	<div class="preloader">
		<svg width="45" height="45" stroke="#fff">
			<g fill="none" fill-rule="evenodd" stroke-width="2" transform="translate(1 1)">
				<circle cx="22" cy="22" r="6" stroke="none">
					<animate attributeName="r" begin="1.5s" calcMode="linear" dur="3s" repeatCount="indefinite" values="6;22"/>
					<animate attributeName="stroke-opacity" begin="1.5s" calcMode="linear" dur="3s" repeatCount="indefinite" values="1;0"/>
					<animate attributeName="stroke-width" begin="1.5s" calcMode="linear" dur="3s" repeatCount="indefinite" values="2;0"/>
				</circle>
				<circle cx="22" cy="22" r="6" stroke="none">
					<animate attributeName="r" begin="3s" calcMode="linear" dur="3s" repeatCount="indefinite" values="6;22"/>
					<animate attributeName="stroke-opacity" begin="3s" calcMode="linear" dur="3s" repeatCount="indefinite" values="1;0"/>
					<animate attributeName="stroke-width" begin="3s" calcMode="linear" dur="3s" repeatCount="indefinite" values="2;0"/>
				</circle>
				<circle cx="22" cy="22" r="8">
					<animate attributeName="r" begin="0s" calcMode="linear" dur="1.5s" repeatCount="indefinite" values="6;1;2;3;4;5;6"/>
				</circle>
			</g>
		</svg>

		<div class="text">Loading ...</div>
	</div>
</div>

<!-- ... end Preloader -->

<!-- Stunning header -->

<div class="stunning-header bg-primary-opacity">

	
	<!-- Header Standard Landing  -->
	
	<div class="header--standard header--standard-landing" id="header--standard">
		<div class="container">
			<div class="header--standard-wrap">
	
				<a href="{{ route('family_dashboard',app()->getlocale()) }}" class="logo">
					<div class="img-wrap">
						<img loading="lazy" src="{{ asset('img/logo.webp') }}" alt="Olympus" width="34" height="34">
						<img loading="lazy" src="{{ asset('img/logo-colored-small.webp') }}" width="34" height="34" alt="Olympus" class="logo-colored">
					</div>
					<div class="title-block">
						<h6 class="logo-title">SELAA</h6>
						<div class="sub-title">SOCIAL NETWORK</div>
					</div>
				</a>
	
				<a href="#" class="open-responsive-menu js-open-responsive-menu">
					<svg class="olymp-menu-icon"><use xlink:href="#olymp-menu-icon"></use></svg>
				</a>
				
			</div>
		</div>
	</div>
	
	<!-- ... end Header Standard Landing  -->
	<div class="header-spacer--standard"></div>

	<div class="stunning-header-content">
		<h1 class="stunning-header-title">{{ $error }} Error Page</h1>
		<ul class="breadcrumbs">
			<li class="breadcrumbs-item">
				<a href="#">Home</a>
				<span class="icon breadcrumbs-custom">/</span>
			</li>
			<li class="breadcrumbs-item active">
				<span>{{ $error }} Error</span>
			</li>
		</ul>
	</div>

	<div class="content-bg-wrap stunning-header-bg1"></div>
</div>

<!-- End Stunning header -->


<section class="medium-padding120">
	<div class="container">
		<div class="row">
			<div class="col col-xl-6 m-auto col-lg-6 col-md-12 col-sm-12 col-12">
				<div class="page-404-content">
					<img loading="lazy" src="{{ asset('img/404.webp') }}" alt="photo" width="636" height="304">
					<div class="crumina-module crumina-heading align-center">
						<h2 class="h1 heading-title">An <span class="c-primary">error</span> destected !...</h2>
						<h3 class="heading-text c-primary">{{ $desc }}</h3>
					</div>

					<a href="{{ route('family_dashboard',app()->getlocale()) }}" class="btn btn-primary btn-lg">Go to Homepage</a>
				</div>
			</div>
		</div>
	</div>
</section>




<!-- Footer Full Width -->

<div class="footer footer-full-width" id="footer">
	<div class="sub-footer-copyright">
		<span>
			Copyright <a href="{{ route('family_dashboard',app()->getlocale()) }}">Olympus Buddypress + WP</a> All Rights Reserved 2017
		</span>
	</div>
</div>

<!-- ... end Footer Full Width -->


<a class="back-to-top" href="#">
	<svg class="back-icon" width="14" height="18"><use xlink:href="#olymp-back-to-top"></use></svg>
</a>


<!-- JS Scripts -->
<script src="{{ asset('js/jQuery/jquery-3.5.1.min.js') }}"></script>

<script src="{{ asset('js/libs/jquery.mousewheel.min.js') }}"></script>
<script src="{{ asset('js/libs/perfect-scrollbar.min.js') }}"></script>
<script src="{{ asset('js/libs/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ asset('js/libs/material.min.js') }}"></script>
<script src="{{ asset('js/libs/selectize.min.js') }}"></script>

<script src="{{ asset('js/libs/moment.min.js') }}"></script>
<script src="{{ asset('js/libs/daterangepicker.min.js') }}"></script>
<script src="{{ asset('js/libs/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('js/libs/ajax-pagination.min.js') }}"></script>
<script src="{{ asset('js/libs/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('js/libs/aos.min.js') }}"></script>

<script src="{{ asset('js/main.js') }}"></script>
<script src="{{ asset('js/libs-init/libs-init.js') }}"></script>

<script src="{{ asset('Bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>

<!-- SVG icons loader -->
<script src="{{ asset('js/svg-loader.js') }}"></script>
<!-- /SVG icons loader -->

</body>
</html>