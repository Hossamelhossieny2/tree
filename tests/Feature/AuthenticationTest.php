<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\FamilyName;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;

class AuthenticationTest extends TestCase
{
    use RefreshDatabase,WithFaker;

    public function test_login_screen_can_be_rendered()
    {
        $response = $this->get('/login');

        $response->assertStatus(200);
    }


    public function test_users_can_register()
    {
        $user = [
            'name' => $this->faker->firstName(),
            'email' => $this->faker->safeEmail(),
            'gender' => $this->faker->randomElement(['male', 'female']),
            'phone_number' => '010'.rand('11111111','99999999'),
            'birth_date' => rand(1940,2021).'-'.rand(1,12).'-'.rand(1,31),
            'password' => '123123123', // password
            'password_confirmation' => '123123123', // password
        ];

        $response = $this->post(route('register'), $user)
            ->assertSessionDoesntHaveErrors();

        unset($user['password_confirmation']);
        unset($user['password']);

        $this->assertDatabaseHas(User::class,$user);
        $this->assertDatabaseHas(FamilyName::class,[
            'title' => $user['name']
        ]);

        $this->assertAuthenticated();

        $response->assertRedirect(route('family_dashboard',['en']));
    }


    public function test_users_can_authenticate_using_the_login_screen()
    {
        $user = User::factory()->create();

        $response = $this->post(route('login', app()->getLocale()), [
            'phone_number' => $user->phone_number,
            'password' => '123123123',
        ]);

        $this->assertAuthenticated();
        $response->assertRedirect(route('family_dashboard',['en']));
    }

    public function test_users_can_not_authenticate_with_invalid_password()
    {
        $user = User::factory()->create();

        $this->post('/login', [
            'phone_number' => $user->phone_number,
            'password' => 'wrong-password',
        ]);

        $this->assertGuest();
    }
}
