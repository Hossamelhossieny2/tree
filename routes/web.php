<?php

use App\Http\Controllers\ChatController;
use App\Http\Livewire\Tree;
use App\Http\Livewire\MessageLive;
use App\Http\Livewire\FamilyGallery;
use App\Http\Controllers\HomeController;
use App\Http\Livewire\Posts;
use App\Http\Livewire\MyMembers;
use App\Http\Livewire\ProfileAbout;
use App\Models\User;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FatherFamilyPrintController;
use App\Http\Controllers\MotherFamilyPrintController;
use App\Http\Controllers\FamilyCalendarController;
use App\Http\Controllers\GalleryController;
use App\Http\Livewire\MyTimeline;
use App\Http\Livewire\MyPhotos;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\Auth\FacebookController;
use App\Http\Controllers\ShareModuleController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\WheelController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\HelpVideosController;
use App\Http\Controllers\ContactusController;
use App\Http\Controllers\PersonalityTestController;
use App\Http\Controllers\QuranController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(app()->getLocale().'/webfamily');
});



Route::get('show-user-location-data', [LocationController::class, 'index']);
Route::post('/get/user-chat/{user}',[ChatController::class,'getChat'])->name('get.user-chat');
Route::post('/add_user_msg', [ChatController::class,'add_user_msg'])->name('add_user_msg');


Route::get('/get_calender/{id}', [FamilyCalendarController::class,'show'])->name('get_event');
Route::get('/get_calender_country/{id}', [FamilyCalendarController::class,'show_country'])->name('get_event_country');
Route::get('/get_calender_date/{id}', [FamilyCalendarController::class,'show_date'])->name('get_event_date');
Route::get('/get_calender_marriage/{hus}/{wife}/{type}', [FamilyCalendarController::class,'show_marriage'])->name('get_event_marriage');

Route::get('facebook', function () {
    return view('facebook');
});
Route::get('auth/facebook', [FacebookController::class,'redirectToFacebook']);
Route::get('auth/facebook/callback', [FacebookController::class,'handleFacebookCallback']);


Route::group([
  'prefix' => '{locale}', 
  'where' => ['locale' => '[a-zA-Z]{2}'], 
  'middleware' => 'setlocale'], function() {

Route::get('/contact_us', [ContactusController::class,'index'])->name('contact_us');
Route::post('/send_msg', [ContactusController::class,'send_msg'])->name('send_msg');
Route::get('/webfamily', [HomeController::class,'webfamily'])->name('webfamily');
Route::post('/HomeLogin', [HomeController::class,'SelaaLogin'])->name('HomeLogin');
Route::post('/HomeRegister', [HomeController::class,'SelaaRegister'])->name('HomeRegister');
Route::post('/getUserProfile', [HomeController::class,'get_user_with_info'])->name('getUserProfile');
Route::post('/followuser', [MyMembers::class,'follow_user'])->name('followUser.post');
Route::post('/unfollowuser', [MyMembers::class,'unfollow_user'])->name('unfollowUser.post');
    Route::get('/terms', [HomeController::class,'terms'])->name('terms');
    Route::get('/policy', [HomeController::class,'policy'])->name('policy');

    

Route::middleware('auth')->group(function(){
    
    Route::get('/family_dashboard', [HomeController::class,'index'])->name('family_dashboard');

    Route::get('/tree_view/{user?}', Tree::class)->name('tree_view')->middleware('has-family-ids-session');
    Route::get('/gallery_view', FamilyGallery::class)->name('gallery_view')->middleware('has-family-ids-session');
    
    Route::get('/family_calender', [FamilyCalendarController::class,'index'])->name('family_calender')->middleware('has-family-ids-session');;

    Route::get('/family_posts', Posts::class)->name('family_posts')->middleware('has-family-ids-session');;
    Route::get('/family_print_father', [FatherFamilyPrintController::class,'familyTree'])->name('family_print_father');
    Route::get('/family_print_mother', [MotherFamilyPrintController::class,'familyTree'])->name('family_print_mother');
    
    Route::post('/add_event', [FamilyCalendarController::class,'store'])->name('add_event');
    Route::post('/join_event', [FamilyCalendarController::class,'join_event'])->name('join_event');

    Route::get('/show-gallery', [GalleryController::class, 'index'])->name('show-gallery');
    Route::post('/create-gallery', [GalleryController::class, 'store'])->name('create-gallery');

    Route::get('/msg_view', MessageLive::class)->name('msg_view');
    Route::get('/member_view', MyMembers::class)->name('member_view');
    Route::get('/about_me', ProfileAbout::class)->name('about_me');
    Route::get('/time_line', MyTimeline::class)->name('time_line');
    Route::get('/my_photos', MyPhotos::class)->name('my_photos');

    Route::get('my_all_shares/{partId?}', [ShareModuleController::class,'my_all_shares'])->name('my_all_shares');
    Route::get('my_choice_shares', [ShareModuleController::class,'my_choice_shares'])->name('my_choice_shares');
    Route::get('/user_shares/{id?}/{shareid?}', [ShareModuleController::class,'index'])->name('user_shares')->middleware('has-family-ids-session');
    Route::post('/user_shares_select', [ShareModuleController::class,'user_shares_select'])->name('user_shares_select');
    Route::post('/user_shares_done', [ShareModuleController::class,'user_shares_done'])->name('user_shares_done');
    Route::post('/user_shares_confirm', [ShareModuleController::class,'user_shares_confirm'])->name('user_shares_confirm');
    Route::post('/add_share_from_user', [ShareModuleController::class,'add_share_from_user'])->name('add_share_from_user');
    Route::get('/start_this_share/{shareid?}', [ShareModuleController::class,'start_this_share'])->name('start_this_share');
    Route::get('/remove_this_share/{shareid?}', [ShareModuleController::class,'remove_this_share'])->name('remove_this_share');

    Route::get('/remove_this_share/{shareid?}', [ShareModuleController::class,'remove_this_share'])->name('remove_this_share');

    Route::get('not_view', [ShareModuleController::class, 'not_view'])->name('not_view');

    Route::get('wheel_view', [WheelController::class, 'index'])->name('wheel_view');
    Route::get('personality_view', [PersonalityTestController::class, 'index'])->name('personality_view');
    Route::post('send_ques', [WheelController::class, 'send_ques'])->name('send_ques');
    Route::post('add_mission', [WheelController::class, 'add_mission'])->name('add_mission');
    Route::post('remove_help', [WheelController::class, 'remove_help'])->name('remove_help');
    Route::post('mark_mission_done', [WheelController::class, 'mark_mission_done'])->name('mark_mission_done');

    Route::get('read_news', [NewsController::class,'index'])->name('read_news');
    Route::get('get_help', [HelpVideosController::class,'index'])->name('get_help');

    Route::post('person_ques', [PersonalityTestController::class, 'person_ques'])->name('person_ques');

    Route::get('quran_index', [QuranController::class, 'index'])->name('quran.index');
    Route::get('quran_review', [QuranController::class, 'review'])->name('quran.review');
    Route::get('quran_memorize/{sura?}/{page?}', [QuranController::class, 'memo'])->name('quran.memo');
    Route::get('next_aya/{ayasura}',[QuranController::class,'next'])->name('quran.next');
});

});
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/todo', function () {
    return view('todo');
})->name('dashboard-todo');



